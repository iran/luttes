.. index::
   ! Nous sommes leux voix

.. _nous_sommes_leurs_voix_2019_03_10:

=====================================================================================================================================
2019-03-10 Il y’a quelques semaines, j’ai rencontré à New York Masih Alinejad, la fondatrice du mouvement #MyStealthyFreedom
=====================================================================================================================================

- https://twitter.com/AntonStruve/status/1104803384132022274?s=20&t=xTbSB0NLsyQmWY-tBGfLUg


Il y’a quelques semaines, j’ai rencontré à New York Masih Alinejad, la
fondatrice du mouvement #MyStealthyFreedom

Voici le message qu’elle adresse à toutes celles et ceux qui soutiennent
la lutte des iraniennes contre le voile obligatoire (1ère partie) ⤵️
