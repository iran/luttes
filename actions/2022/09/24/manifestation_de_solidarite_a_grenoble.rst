
.. index::
   pair: Mahsa Jina Amini; Grenoble
   pair: Iraniennes ; Ukrainiennes
   pair: Chant; Iranien


.. _iran_grenoble_2022_09_24:

==========================================================================================================================================================================================================================================================
♀️✊ 1er rassemblement samedi 24 septembre 2022 à Grenoble place Félix Poulat à 14h30 **rassemblement de solidarité avec les femmes en Iran et en hommage à Mahsa Jina Amini, #Grenoble #JinaMahsaAmini #MasahAmini #Masah_Amini #OpIran #مهساامینی**
==========================================================================================================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 24 septembre 2022 cela fait donc 1 semaine 1 jour
=> 8e jour = on est dans la 2e semaine de la révolution, N°semaine:38 (2022)



- :ref:`mahsa_jina_amini`
- :ref:`slogans`
- https://www.grenoble.fr/association/129060/69-ligue-des-droits-de-l-homme-et-du-citoyen.htm


.. #Grenoble #NousSommesLeursVoix #EndIranRegime #IranProtests #LetUsTalk #WhiteWednesdays #TwitterKurds #zhinaAmini #JinaMahsaAmini #MasahAmini #MasahAmini #Masah_Amini  #OpIran #مهساامینی


Rassemblement de solidarité avec les femmes en Iran et en hommage à Mahsa Jina Amini
========================================================================================

Samedi 24 septembre 2022 place Félix Poulat à 14h30 rassemblement de
solidarité avec les femmes en Iran et en hommage à Mahsa Jina Amini

A l'appel de nombeuses Iraniennes et Iraniens de Grenoble
Avec le soutien de la `Ligue des Droits de l'Homme de Grenoble métropole <https://www.grenoble.fr/association/129060/69-ligue-des-droits-de-l-homme-et-du-citoyen.htm>`_


.. figure:: images/appel_a_solidarite.png
   :align: center
   :width: 300

.. figure:: images/appel_au_rassemblement.png
   :align: center
   :width: 300

Masah Amini, #مهساامینی
-----------------------

.. figure:: images/mahsa_amini_hopital.png
   :align: center
   :width: 600


.. figure:: images/mahsa_amini_en_persan.png
   :align: center
   :width: 300

   Masah Amini, #مهساامینی, :ref:`mahsa_jina_amini`


Manifestation à Grenoble, place Félix Poulat
---------------------------------------------

.. figure:: images//manifestants__et_tram.png
   :align: center


.. figure:: images/avec_les_ukrainiens_2.png
   :align: center

   Solidarité entre ukrainiens/ukrainiennes et iraniens/iraniennes



.. figure:: images/une_personne_avec_dessin_marjane_rose.png
   :align: center
   :width: 600


Mort à la république islamique, #MasahAmini
-----------------------------------------------

.. figure:: images/manifestantes__et_zoya_rose.png
   :align: center

   Mort à la république islamique, #MasahAmini

.. figure:: images/manifestantes__et_zoya_rose_2.png
   :align: center


Morte pour une mèche de cheveux en Iran
-------------------------------------------

.. figure:: images/morte_pour_une_meche_de_cheveux.png
   :align: center



Soyons avec les femmes iraniennes
------------------------------------

.. figure:: images/soyons_avec_les_femmes_iraniennes.png
   :align: center
   :width: 600



.. _femme_vie_liberte_2022_09_24:

Femme (Zan, زن), Vie (Zendegi, زندگی), Liberté (Âzâdi, آزادی)
--------------------------------------------------------------

- :ref:`zan_zendegi_azadi`

.. figure:: images/tissu_avec_3_mots.png
   :align: center
   :width: 600

   Femme (Zan, زن), Vie (Zendegi, زندگی), Liberté (Âzâdi, آزادی)



Femme, Vie, Liberté #MahsaAmini, #OpIran
------------------------------------------

.. figure:: images/femme_vie_liberte.png
   :align: center
   :width: 300

   Femme, Vie, Liberté #MahsaAmini, #OpIran



Morte pour ses cheveux en Iran, Femme (Zan, زن), Vie (Zendegi, زندگی), Liberté (Âzâdi, آزادی)
---------------------------------------------------------------------------------------------

.. figure:: images/femme_vie_liberte_plus_mahsa_rose.png
   :align: center

   Morte pour ses cheveux en Iran, Femme (Zan, زن), Vie (Zendegi, زندگی), Liberté (Âzâdi, آزادی)



.. figure:: images/sur_le_mur_4_images_mahsa.png
   :align: center


.. figure:: images/sur_le_mur_4_images_mahsa_dessin_marjane.png
   :align: center


.. figure:: images/sur_le_mur_8_images_plus_rose.png
   :align: center

.. figure:: images/manifestants__devant_eglise.png
   :align: center


.. figure:: images/sur_le_mur_9_images.png
   :align: center


Quelques événements du 19 septembre au 26 septembre 2022
=============================================================================

- :ref:`appel_de_hawzhin_azeez`


Solidarité entre ukrainiens/ukrainiennes et iraniens/iraniennes
====================================================================

.. figure:: images/avec_les_ukrainiens.png
   :align: center

   Solidarité entre ukrainiens/ukrainiennes et iraniens/iraniennes


.. figure:: images/avec_les_ukrainiens_2.png
   :align: center

   Solidarité entre ukrainiens/ukrainiennes et iraniens/iraniennes


Compte-rendu
=============

- https://www.francebleu.fr/infos/societe/grenoble-150-personnes-manifestent-contre-la-repression-en-iran-1664037189

Liens
=====

- :ref:`iran_grenoble_2022_10_01`
- https://travailleur-alpin.fr/2022/09/27/plus-de-150-personnes-se-sont-rassemblees-a-grenoble-contre-la-repression-en-iran/
- https://www.francebleu.fr/infos/societe/grenoble-150-personnes-manifestent-contre-la-repression-en-iran-1664037189
