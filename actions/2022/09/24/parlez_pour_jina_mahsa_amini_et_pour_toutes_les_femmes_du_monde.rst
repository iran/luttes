.. index::
   pair: Appel ; Hawzhin Azeez
   ! Appel de Hawzhin Azeez


.. _appel_de_hawzhin_azeez:

=======================================================================================================
2022-09-24 **Appel de Hawzhin Azeez**, Parlez pour Jina Mahsa Amini et pour toutes les femmes du monde
=======================================================================================================

- https://kurdistan-au-feminin.fr/2022/09/24/parlez-pour-jina-mahsa-amini-et-pour-toutes-les-femmes-du-monde/


Introduction
==============

L’activiste kurde, Hawzhin Azeez appelle chaque femme et homme à élever
la voix pour les femmes et la planète que le système capitaliste phallocrate
détruit depuis des siècles.

Voici l’appel de Hawzhin Azeez
================================

Ne pensez-vous pas que c’est une coïncidence si **l’une des plus grandes
révolutions que le monde ait connues s’est élevée contre les valeurs
fascistes et patriarcales au Rojava, dans le nord de la Syrie ?**

Nous avons vu les brillantes YPJ (Unités de protection des femmes) se lever
pour écraser DAECH et leur haine et leur rage ?

Ne pensez-vous pas que c’est une coïncidence si ce sont encore les Kurdes
qui mènent un autre soulèvement féministe en Iran ?
Que la mort d’une femme kurde innocente a conduit à un cri mondial de « Jin, Jiyan, Azadi » (Femmes, Vie, Liberté) ?

Ne pensez pas que seuls l’Iran et d’autres régimes islamiques « arriérés »
sont les seuls à s’opposer aux droits et à la liberté des femmes.

En Amérique, « Roe and Wade » [arrêt historique rendu par la Cour suprême
des États-Unis en 1973 sur la question de la constitutionnalité des lois
qui criminalisent ou restreignent l’accès à l’avortement] était renversé.

En Australie, une femme par semaine est assassinée par son partenaire
actuel ou ancien.

Au Canada, des milliers de femmes et de filles autochtones sont portées disparues.

Dans toutes les nations « civilisées » d’Europe, les femmes souffrent en
silence de la violence domestique, de la violence sexuelle, de la violence
psychologique, du contrôle économique, de la pauvreté, de la marginalisation
institutionnelle, etc.

Ces mêmes régimes ont sanctionné et accueilli à nouveau les talibans avec
leur silence honteux afin que davantage d’hommes puissent dire aux femmes
qu’ils ne peuvent pas étudier, travailler, vivre et respirer.

Les femmes arméniennes sont décapitées, démembrées et violées à cause du
même silence. La Palestine continue de brûler.

**Pendant ce temps, la beauté de notre planète est détruite, notre merveilleuse
diversité de faune et de flores assassinée pour le profit**.

Combien de temps **la masculinité toxique** sans entraves et l’alliance impie
du capitalisme continueront – elles à assassiner, réduire au silence et
effacer tout ce qui est féminin, tout ce qui est beau, tout ce qui est irremplaçable ?

**Combien de temps avant que le monde ne considère les femmes comme des êtres
humains, comme l’autre moitié de lui-même, la part digne d’un égal amour,
justice et humanité, d’existence et de protection ?**

:ref:`Zhina Mahsa Amini <mahsa_jina_amini>`, c’est nous toutes.

Elle est vous et moi. Elle est chaque femme. Chaque féminin sacré qui a
été nié, déraciné et assassiné.

**Le 21ème siècle est le siècle de la libération de la femme !**

Assez ! Prêtez votre voix, votre puissance et votre amour aux peuples
d’Iran qui protestent.

Pas parce que vous détestez l’islam et que vous voyez cela comme une opportunité
d’effacer davantage de diversité. **Mais parce que votre humanité est déplacée
vers un endroit au-delà de l’apathie, au-delà des intérêts égoïstes et
de l’hyper individualité, vers un endroit où le courage et l’espoir
s’unissent ; un endroit où l’amour se retrouve enfin**.

Parlez pour :ref:`Zhina [prénom kurde de Mahsa Amini] <mahsa_jina_amini>`

Parlez pour toutes les femmes du monde. Parlez pour l’humanité, pour la
féminité, pour vous-même, pour votre mère, votre sœur, votre fille et
l’étranger de l’autre côté de la route. Parlez l’un pour l’autre.

*Aux hommes je dis que nous avons plus que jamais besoin de vous !*

**Le monde vous offre tant d’occasions de vous élever contre l’injustice.
Pourquoi vous ne le prenez pas ?!**

