.. index::
   pair: Mahsa Jina Amini; Trocadéro


.. _trocadero_2022_09_25:

=======================================================================================================================================================================================================
2022-09-25 Trocadéro, Paris 16h Soutien aux Iraniennes et Iraniens qui se battent pour la liberté et donnent leur vie pour ce combat. #Iran #MahsaAmini #JinaMahsaAmini #Hadis_Najafi  #HadisNajafi
=======================================================================================================================================================================================================

.. -:ref:`mahsa_jina_amini`

Annonce par https://twitter.com/FaridVahiid
===============================================

.. figure:: images/appel_trocadero.png
   :align: center

   https://twitter.com/FaridVahiid/status/1573981617730854913?s=20&t=YTw8zeAOjrUUuezB2y418w


L’une des images fortes de cette manifestation
===================================================

Les manifestants en colère devant l’ambassade d’Iran à Paris face aux
policiers. #Iran #MahsaAmini

.. figure:: images/image_forte.png
   :align: center

   https://twitter.com/RemyBuisine/status/1574091561930989578?s=20&t=asHI2JvltGppdqCSJP4hew
