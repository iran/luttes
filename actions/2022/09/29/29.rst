

=====================
2022-09-29
=====================



Iran : « Femmes, vie, liberté, c’est un projet politique »
============================================================

- https://www.mediapart.fr/journal/international/290922/iran-femmes-vie-liberte-c-est-un-projet-politique
- https://www.youtube.com/watch?v=KKVqOaOL_YA

Alors que des milliers de personnes bravent la répression et manifestent
en Iran depuis une semaine, le régime des mollahs est-il menacé ?

Nous analysons ce soulèvement exceptionnel impulsé par des femmes et
qui transcende les classes sociales avec nos invité·es.

.. figure:: images/video_mediapart.png
   :align: center
