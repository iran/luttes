
.. index::
   pair: Voix ; Iran
   pair: Grenoble ; Portons la voix de l'Iran
   pair: Grenoble ; FemmeVieLiberte


.. figure:: femme_vie_liberte_portons_la_voix_de_l_iran.png
   :align: center
   :width: 600

.. figure:: images/manifs_1er_octobre.png
   :align: center
   :width: 600


.. _iran_grenoble_2022_10_01:

=======================================================================================================================================================================================================================================================================
♀️✊ 2e rassemblement samedi 1er octobre 2022 à Grenoble place Félix Poulat à 14h30 **Femme, Vie, Liberté #mahsa_amini 📣  Portons la voix de l'Iran, rassemblement de solidarité avec les femmes Iraniennes #Grenoble #NousSommesLeursVoix #OpIran #مهساامینی** 📣
=======================================================================================================================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 01 octobre 2022 cela fait donc 2 semaines 1 jour
=> 15e jour = on est dans la 3e semaine de la révolution, N°semaine:39 (2022)



- :ref:`mahsa_jina_amini`
- :ref:`slogans`
- https://www.grenoble.fr/association/129060/69-ligue-des-droits-de-l-homme-et-du-citoyen.htm


.. ♀️️✊ #Grenoble #mahsa_amini #NousSommesLeursVoix #EndIranRegime #IranProtests
.. #LetUsTalk #WhiteWednesdays
.. - #TwitterKurds #zhinaAmini #JinaMahsaAmini #MasahAmini #MasahAmini #Masah_Amini  #OpIran #مهساامینی
.. ♀️✊

Femme, Vie, Liberté #mahsa_amini, Portons la voix de l'Iran, rassemblement de solidarité avec les femmes iraniennes
==========================================================================================================================

Samedi 1er octobre 2022 place Félix Poulat à 14h30 rassemblement de
solidarité avec les femmes iraniennes.

A l'appel de nombeuses Iraniennes et Iraniens de Grenoble
Avec le soutien de la `Ligue des Droits de l'Homme de Grenoble métropole <https://www.grenoble.fr/association/129060/69-ligue-des-droits-de-l-homme-et-du-citoyen.htm>`_


Femme, Vie, Liberté (#FemmeVieLiberte #ZanZendegiÂzâdi #JinJiyanAzadî #WomanLifeFreedom)
=========================================================================================

- :ref:`zan_zendegi_azadi`

.. figure:: images/zan_zendegi_azadi.png
   :align: center
   :width: 300

- #FemmeVieLiberte (Français)
- #ZanZendegiÂzâdi => Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan)
- #JinJiyanAzadî (Kurde)
- #WomanLifeFreedom (Anglais)


📣 Portons la voix de l'Iran 📣
==================================

Le cri de l’Iran : “Parlez de nous !” https://kedistan.net/2022/09/28/iran-parlez-de-nous/

S’il est une demande des femmes, des jeunes et de tous ceux qui descendent
dans la rue contre le régime en Iran, c’est bien celle-là.
**Parlez de nous, ne laissez pas le silence s’installer avec la répression**.

#IranProtests #MahsaAmini #IranRevolution


.. figure:: images/parlez_de_nous.png
   :align: center

   https://twitter.com/KEDISTAN/status/1575116876023316484?s=20&t=_6nVPvVENk-ljxt03niV7A, https://kedistan.net/2022/09/28/iran-parlez-de-nous/



Liens
======

- :ref:`iran_grenoble_2022_09_24`


Annonces du rassemblement par Ici-Grenoble
----------------------------------------------

Ici-grenoble
+++++++++++++

.. figure:: ../../../../images/ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org

Merci à `Ici-Grenoble <https://www.ici-grenoble.org>`_ :) pour l'annonce du rassemblement.

- https://www.ici-grenoble.org/evenement/rassemblement-de-solidarite-avec-les-femmes-iraniennes-femme-vie-liberte-mahsa-amini


.. figure:: images/annonce_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/rassemblement-de-solidarite-avec-les-femmes-iraniennes-femme-vie-liberte-mahsa-amini

.. figure:: images/annonce_ici_grenoble_en_une.png
   :align: center


Le tamis
++++++++++

- https://www.le-tamis.info/evenement/femme-vie-liberte-mahsa_amini-portons-la-voix-de-l



Annonces twitter
-------------------

- https://twitter.com/LaAilleurs/status/1575560302216634368?s=20&t=laN-AbusXNtAAsjFDKCymw
- https://twitter.com/EnsembleGre38/status/1575373652551471104?s=20&t=laN-AbusXNtAAsjFDKCymw



Affiche à télécharger
-----------------------

:download:`Affiche à télécharger Femme, Vie, Liberté #mahsa_amini Portons la voix de l'Iran <femme_vie_liberte_portons_la_voix_de_l_iran.png>`


Compte rendu du rassemblement
====================================

.. toctree::
   :maxdepth: 3

   rassemblement/rassemblement


Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2022_09_24`
