.. index::
   pair: Slogan; Celle qui choisit c'est moi

.. _photos_rassemblement_2022_10_01:

=====================================================================================================================================================================================================================================================
Rassemblement : photos et chants
=====================================================================================================================================================================================================================================================

We are not Islamic Republic

We are IRAN
#WomanLifeFreedom #FemmeVieLiberte
📣 Portons la voix de l'Iran 📣
🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
#mahsa_amini #JinaMahsaAmini #Grenoble #NousSommesLeursVoix  #OpIran #مه#ساامینی
https://iran.frama.io/luttes/slogans/femme_vie_liberte/historique/historique.html


Une jeune Iranienne très dynamique pour porter la voix de Iraniennes

🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی (Persan) #ZanZendegiÂzâdi 🇮🇷
🇫🇷 Femme, Vie, Liberté #FemmeVieLiberte (Français, 🇫🇷)
🇮🇹 Donna, Vita, Libertà (Italien, 🇮🇹 )
🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )

📣 Portons la voix de l'Iran 📣

#mahsa_amini #Grenoble #NousSommesLeursVoix #OpIran #مه#ساامینی

https://iran.frama.io/luttes/slogans/femme_vie_liberte/historique/historique.html

.. place Félix Poulat Grenoble Centre

.. _chants_2022_10_01_grenoble:

Plusieurs chants ont résonné sur la place Félix Poulat
=========================================================


**Pour ma sœur, ta sœur, nos sœurs** de Shervin Hajipour
------------------------------------------------------------

La chanson :ref:`Pour ma sœur, ta sœur, nos sœurs <pour_ma_soeur>` du
célèbre chanteur iranien Shervin Hajipour qui a :ref:`été arrêté le 29 septembre 2022 <arrestation_shervin_hajipour>`.

Bella Ciao interprétée par Yashgin Kiyani
----------------------------------------------

La chant antifascsiste :ref:`Bella Ciao <bella_ciao>` interprétée par :ref:`Yashgin Kiyani <yashgin_kiyani>`


Photos + https://www.instagram.com/iranluttes/
=======================================================

https://www.instagram.com/iranluttes/
----------------------------------------

Voir aussi sur https://www.instagram.com/iranluttes/


📣 ♀️✊  Prise de parole par une jeune femme habillée en noir
-----------------------------------------------------------------------------------------------------

Prise de parole par une jeune femme iranienne, place Félix Poulat
à Grenoble le samedi 1er octobre 2022


.. figure:: images/prise_de_parole_femme_en_noir.png
   :align: center
   :width: 600

.. figure:: images/prise_de_parole_femme_en_noir_2.png
   :align: center
   :width: 600

📣 ♀️✊  Prise de parole par une jeune femme habillée en jaune
-----------------------------------------------------------------------------------------------------

Prise de parole par une jeune femme habillée en jaune, place Félix Poulat
à Grenoble le samedi 1er octobre 2022

.. figure:: images/prise_de_parole_femme_en_jaune.png
   :align: center
   :width: 600

.. figure:: images/prise_de_parole_femme_en_jaune_2.png
   :align: center
   :width: 600

.. figure:: images/prise_de_parole_femme_en_jaune_3.png
   :align: center
   :width: 600

📣 ♀️✊ Prise de parole sous le déluge ☔ 🌧️
-----------------------------------------------------------------------------------------------------

.. figure:: images/prise_de_parole_sous_deluge.png
   :align: center
   :width: 600

📣 Prise de parole d'un representant d'Amnesty International Grenoble
-----------------------------------------------------------------------------------------------------

- https://www.amnesty.fr/pres-de-chez-vous/grenoble-centre

.. figure:: images/intervention_representant_amnesty_internationale.png
   :align: center
   :width: 600


️♀️✊ **Non au dictateur, celle qui choisit c'est moi, #mahsa_amini**
-----------------------------------------------------------------------------------------------------

- :ref:`mahsa_jina_amini`
- :ref:`mort_au_dictateur`

.. figure:: images/celle_qui_choisit_cest_moi.png
   :align: center
   :width: 600


️♀️✊ **We are all Masah**
-----------------------------------------------------------------------------------------------------

.. figure:: images/we_are_all_masah.png
   :align: center
   :width: 600

🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
-----------------------------------------------------------------------------------------------------

- :ref:`femmes_vie_liberte`

.. figure:: images/femme_vie_liberte.png
   :align: center
   :width: 600

.. figure:: images/femme_vie_liberte_2.png
   :align: center
   :width: 600

.. _cut_it_grenoble_2022_10_01:

️♀️✊ **Cut it (Grenoble 2022-10-01)**
-----------------------------------------------------------------------------------------------------

.. figure:: images/cut_it_femme_vie_liberte.png
   :align: center
   :width: 600

🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
-----------------------------------------------------------------------------------------------------

- :ref:`femmes_vie_liberte`

.. figure:: images/jin_jiyan_azadi.png
   :align: center
   :width: 600


🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
-----------------------------------------------------------------------------------------------------

- :ref:`femmes_vie_liberte`

.. figure:: images/woman_life_freedom.png
   :align: center
   :width: 600

Soyez solidaire avec le peuple d'Iran
-----------------------------------------------------------------------------------------------------

- :ref:`femmes_vie_liberte`

.. figure:: images/soyez_solidaire_avec_le_peuple_d_iran.png
   :align: center
   :width: 600



Hommage à Mahsa Amini : son portrait + une fleur
-----------------------------------------------------------------------------------------------------

- :ref:`mahsa_jina_amini`

.. figure:: images/carton_fleur_mahsa.png
   :align: center
   :width: 400


Libération des prisonniers politiques
-----------------------------------------------------------------------------------------------------


.. figure:: images/liberation_prisonniers_politiques.png
   :align: center
   :width: 400


♀️✊📷 ☔ Prises de photos sous la pluie 🌧️ et les parapluies ☂️
-----------------------------------------------------------------------------------------------------

.. figure:: images/prise_de_photos_sous_les_parapluies.png
   :align: center
   :width: 600

.. figure:: images/prise_de_photos_sous_les_parapluies_2.png
   :align: center
   :width: 600

Parapluies place Félix Poulat, sous une pluie battante + libération des prisonniers politiques
-----------------------------------------------------------------------------------------------------

.. figure:: images/place_felix_poulat_parapluies.png
   :align: center
   :width: 600

.. figure:: images/place_felix_poulat_parapluies_2.png
   :align: center
   :width: 600



Jo Briant
-----------------------------------------------------------------------------------------------------

- https://fr.wikipedia.org/wiki/Jo_Briant

.. figure:: images/jo_briant.png
   :align: center
   :width: 600
