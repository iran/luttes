
.. _dessous_cartes_2022_10_03:

==================================================================================================================================
2022-10-03 Le dessous des cartes #Iran #Femmes Géographie d'une révolte
==================================================================================================================================

- https://www.arte.tv/fr/videos/110291-012-A/le-dessous-des-cartes-l-essentiel/


.. figure:: geographie_d_une_revolte.png
   :align: center

   https://www.arte.tv/fr/videos/110291-012-A/le-dessous-des-cartes-l-essentiel/


En Iran, le mouvement de contestation entre dans sa troisième semaine.

La République islamique a déjà connu des mouvements de révolte, notamment
en 2019, mais ce qui frappe cette fois, c’est la radicalité du slogan
("À bas la dictature") et l’origine des événements (la mort de Mahsa Amini
et la cause des femmes).

Quelle est la géographie de cette révolte ? Sortons nos cartes.


