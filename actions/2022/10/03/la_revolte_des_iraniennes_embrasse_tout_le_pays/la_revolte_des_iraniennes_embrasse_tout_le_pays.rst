
.. _blast_2022_10_03:

==================================================================================================================================
2022-10-03 Blast #Iran #Femmes #Révolte LA RÉVOLTE DES IRANIENNES EMBRASE TOUT LE PAYS
==================================================================================================================================

- https://twitter.com/azadeh_kian
- https://www.youtube.com/watch?v=ruD5lCHbfN0

Alors que l’Iran s’embrase sous les cris du slogan “Femme, vie, liberté”
depuis la mort de Mahsa Amini le 16 septembre dernier, la sociologue
et spécialiste des questions féministes  franco-iranienne Azadeh Kian,
revient pour Blast sur les racines de la colère des iraniennes, décrypte
le soulèvement national en cours et envisage les conséquences possibles
à court et moyen terme de cette révolte d’une ampleur inédite.

Journalistes : Soumaya Benaïssa, Hamza Chennaf
Montage : Camille Chalot, Alexandre Cassier
Images : Arthur Frainet
Son : Baptiste Veilhan
Graphisme : Adrien Colrat
Diffusion : Maxime Hector
Production : Thomas Bornot
Directeur du développement : Mathias Enthoven
Rédaction en chef : Soumaya Benaissa

Le site : https://www.blast-info.fr/
Facebook : https://www.facebook.com/blastofficiel
Twitter : https://twitter.com/blast_france
Instagram : https://www.instagram.com/blastofficiel/

