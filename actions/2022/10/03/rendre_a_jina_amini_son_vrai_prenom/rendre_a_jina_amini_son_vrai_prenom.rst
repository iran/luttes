
.. _vrai_prenom_jina_2022_10_03:

==================================================================================================================================
2022-10-03 Rendre à Mahsa Amini son vrai prénom **Jîna** et aux femmes kurdes la maternité du slogan **Femme, Vie, Liberté**
==================================================================================================================================

- https://novaramedia.com/2022/10/04/jina-mahsa-amini-was-kurdish-and-that-matters/
- https://kurdistan-au-feminin.fr/2022/10/03/rendre-a-mahsa-amini-son-vrai-prenom-et-aux-femmes-kurdes-la-maternite-du-slogan-femme-vie-liberte/


Gulîstan, une jeune femme kurde nous a contactées récemment pour transmettre
un message destiné à une page Facebook dont elle apprécie par ailleurs
la ligne politique.

Gulîstan voulait attirer l’attention de cette page en particulier et des
médias grand-public en général, sur le traitement de l’actualité concernant
la révolte populaire en Iran suite au meurtre de Jina Mahsa Amini par la
police des mœurs à Téhéran mi-septembre dernier.

Gulîstan demande à ce que les médias clarifient deux points importants
concernant la lutte des femmes en Iran et le vrai prénom de Mahsa:

1) Mahsa Amini s’appelle en réalité **Jina Amini**, mais comme l’état civil
iranien refuse d’enregistrer les prénoms kurdes, la famille a dû choisir
un prénom persan pour tout ce qui est administratif.

Mais presque personne dans l’entourage proche ne connaissait le prénom
**Mahsa** qui était destiné au régime iranien.
D’ailleurs, sur sa tombe, sa mère l’appelait **Jina** et jamais **Mahsa**…

2) Le deuxième point également très important à clarifier est le slogan
**Femme, Vie, Liberté** (en kurde: Jin, Jiyan, Azadî), un slogan fruit
de la lutte de libération du mouvement kurde, pour ne pas le nommer,
le PKK (organisation classée terroriste par l’Occident à la demande
de la Turquie qui colonise une très grande partie du Kurdistan).

Alors, il faut rendre à César ce qui est à César et à **Jina juste son
prénom** tandis qu’aux femmes kurdes la maternité du slogan **Femme, Vie, Liberté**,
fruit de décennies de lutte contre le colonialisme et le patriarcat au Kurdistan.


Maintenant, place au message de Gulîstan:
============================================


En tant que membre de la communauté kurde, je suis particulièrement
touchée par l’actualité en Iran et de manière générale au Proche ainsi
qu’au Moyen-Orient. Dès lors, dans les lectures d’articles, de posts que
j’ai effectué, quasi nulle part, il n’est fait mention du vrai prénom
de la victime du gouvernement iranien et sa politique de **moralité**
à savoir **Jîna Amini** plus connue sous le prénom **Mahsa**.

Comme vous êtes une page engagée, il me semblait essentiel de vous faire
part de cela.

**Mahsa est son prénom issu du racisme étatique** car les prénoms kurdes
sont interdits, l’identité kurde est réprimée en Iran.

Sur sa tombe, il est écrit **Jîna** pas Mahsa, sa mère ne la pleure pas comme
telle… Respecter sa mémoire et celles de toutes les victimes reviendrait
à la nommer réellement avec le prénom que ses parents lui ont donné à
la naissance, ce prénom qui, de par son origine, fait tant peur à l’Iran.

L’appeler Mahsa revient à jouer le jeu de l’Iran à l’égard des **minorités**
que je nommerais même minorisées.

En outre, il faut savoir que la révolte a commencé au :ref:`Rojhilat <rojhilat>`, la région
kurde d’Iran et s’est étendue à Téhéran et d’autres parties de l’Iran.

Il n’est jamais fait mention qu’elle était originaire de :ref:`Saqqez <saqqez>`, la Province
du Kurdistan et pourtant, cela est raison supplémentaire de son arrestation.
Cela ne vous concerne pas exclusivement évidemment, la plupart des médias
ne l’ont pas abordé.

Enfin, il me semble indispensable de resituer d’où provient le slogan
qui est désormais connu mondialement : :ref:`Femmes, Vie, Liberté <femme_vie_liberte>`,
en kurde :ref:`Jin, Jîyan, Azadî <jin_jiyan_azadi_ref>`.

Il n’est pas issu du mouvement de protestation pour les droits des femmes
en Iran mais plutôt du mouvement de libération des femmes kurdes et ce,
depuis de nombreuses années (plus de 20 ans).

Ce qu’il se passe en Iran concerne les Iranien·ne·s mais aussi tous les
peuples d’Iran (Kurdes, Baloutches, Arabes, Azéris) et **toutes les
femmes du monde** car elle met en exergue que, qu’importe où l’on est
dans le monde, le patriarcat fait des dégâts et il faut que ce système
soit stoppé/détruit.

Mon but ici en vous écrivant n’est pas de vous jeter la pierre mais ça
se veut être dans un esprit de bienveillance et de reconnaissance.

Je lutte pour que le peuple kurde cesse d’être invisibilisé aux yeux
du monde. Malheureusement, l’Histoire nous a montré que ce fut sans
cesse le cas.


