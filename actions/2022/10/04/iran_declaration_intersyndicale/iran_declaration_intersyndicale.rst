.. index::
   pair: Déclaration intersyndicale; 2022-10-04

.. _declaration_intersyndicale_2022_10_04:

======================================================================================================
2022-10-04 France Déclaration intersyndicale ✊ **Solidarité avec les manifestant.e.s d'Iran** ✊
======================================================================================================

.. figure:: intersyndicale.png
   :align: center

:download:`Déclaration intersyndicale au format PDF <iran_declaration_intersyndicale_2022_10_04.pdf>`

Solidarité avec les manifestant.e.s d'Iran

La colère gronde en Iran suite au meurtre, le 16 septembre 2022, de la
jeune Masha Jina AMINI par la Police des mœurs.

A l'annonce de son décès des mouvements de protestation ont eu lieu dans
la région kurde dont elle était originaire, ainsi qu'a Téhéran et plusieurs
universités du pays.

Dès le lendemain du meurtre, le syndicat VAHED des autobus de la région
de Téhéran a notamment déclaré:

    "Nous condamnons fermement ce crime et exigeons des poursuites, un procès
    public et la punition de tous les responsables de ce meurtre.
    La liberté d’expression et d'habillement, le droit à l’éducation, à l’emploi,
    au divorce, ainsi que le droit de participer à des activités sociales
    doivent faire pleinement partie des droits de tous/toutes les
    habitant.e.s du pays, ainsi que de tout groupe social.

    La discrimination structurelle, institutionnalisée et patriarcale à
    l’égard des filles et des femmes dans le pays doit cesser.

    Le mouvement syndical et les organisations indépendantes de salarié.e.s
    sont de fervents défenseurs de l’égalité entre les hommes et les femmes.
    Ils s’opposent à l'obligation de porter le hijab, ainsi qu'aux
    autres injustices et discriminations contre les femmes et les personnes opprimées."


Lundi 19 septembre 2022 dans la ville natale de Mahsa Jina AMINI, les
partis politiques kurdes (interdits par le régime) ont appelé à la grève
générale.

Depuis, divers mouvements ont eu lieu dans les régions kurdes demandant
notamment la fin des discriminations envers les Kurdes, et une possible
autonomie.
Le slogan :ref:`Jin, jiyan, azadî (femme, vie, liberté) <jin_jiyan_azadi>`,
repris du mouvement des femmes kurdes libres, est largement scandé par
toutes et tous dans les manifestations.

Partout dans le pays, des milliers de manifestant.e.s descendent chaque
jour dans la rue. Ils/elles dénoncent notamment la Police des mœurs,
placée sous l’autorité du ministre de l’Intérieur.

Celle-ci est notamment chargée de vérifier l'application du port
obligatoire du foulard pour les femmes.
Selon Amnesty International « entre mars 2013 et mars 2014, plus de 2,9
millions de femmes iraniennes ont reçu un avertissement de la police pour
non-respect du code vestimentaire islamique, et 18 081 autres femmes ont
été déférées aux autorités judiciaires pour être poursuivies et sanctionnées ».

Ces évènements font écho au 8 mars 1979, à l’occasion de la journée
internationale des femmes, où des centaines de milliers de femmes (et d'hommes)
ont manifesté contre l'instauration du port obligatoire du hidjab et les
régressions dans le Code civil, ainsi que pour l’égalité entre hommes et
femmes.

Des manifestant.e.s s'attaquent aux symboles de la République islamique
instaurée en 1979, en déboulonnant des statues ou déchirant des portraits
de dignitaires passés ou actuels du régime.
Sont scandés de plus en plus massivement des slogans comme : **A bas la République Islamique**,
**Mort au dictateur**, **Ni Chah, ni Guide Suprême**.

**Au 1er octobre 2022, la police aurait tué au moins 133 personnes**.

La mort :ref:`d’Hadis NAJAFI <hadis_najafi>` abattue par six balles suscite
notamment beaucoup d’émotion.

Des milliers d'arrestations ont lieu, dont à ce jour plus de 700 dans
une province kurde du nord du pays.

Nombre de personnes arrêtées sont torturé.e.s et condamnées à des peines
d’emprisonnement ou de flagellation à l’issue de procès iniques.

De nombreuses familles de détenu.e.s se rassemblent devant les prisons,
pour exiger leurs libération.

Face à la popularité du mouvement, le pouvoir a bloqué l’accès aux moyens
de communication par Internet.

Des manifestations de soutien se multiplient dans le monde.

Nous condamnons fermement la répression envers les manifestant.e.s.

Nous appelons le gouvernement iranien à libérer immédiatement et inconditionnellement
l'ensemble des manifestant.es.s détenu.e.s, ainsi que les défenseur·e·s
des droits humains, les syndicalistes, les militant.e.s étudiant.e s,
les journalistes, etc.

Nous soutenons notamment :

- le droit essentiel des femmes à disposer de leurs corps,
- l’abrogation de la loi rendant obligatoire le port du Hijab ainsi que
  toutes les lois phallocratiques en vigueur.

Paris, le 4 octobre 2022
