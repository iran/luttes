

.. _vrai_prenom_jina_2022_10_04:

=======================================================================================================
2022-10-04 Jîna ‘Mahsa’ Amini Was Kurdish And That Matters Say her Kurdish name. by Meral Çiçek
=======================================================================================================

- https://novaramedia.com/2022/10/04/jina-mahsa-amini-was-kurdish-and-that-matters/

In 1852 the 35-years old women’s rights activist Tahirih Ghoratolein was
executed by the Iranian regime in Tehran for two things: her Bábí faith
and unveiling herself. Her last words were: “You can kill me as soon as
you like, but you cannot stop the emancipation of women.”

Almost exactly 170 years later, in the same city, a 22-year-old woman
died after being arrested by the so-called guidance patrol, Islamic
religious police who adhere to strict interpretations of sharia law.

Her offence was not wearing the hijab in accordance with government
standards.

When the police detained her, the woman’s brother explained they weren’t
from Tehran and were unaware of the city’s rules (the family were
visiting from Saqqez, a Kurdish city in the west, close to the border
of Iraqi Kurdistan) to no avail: she was taken to a police station anyway.

There, her family allege, she was “insulted and tortured”, collapsing
before eventually being taken to hospital.

Upon arrival doctors discovered the woman had suffered “brain death”.

Two days later, she suffered a cardiac arrest and was unable to be resuscitated.


The woman’s name was Jîna, which means ‘life’ in Kurdish. Jîn (and its
equivalent Jiyan) is etymologically related to Jin, the Kurdish word for
woman.

But the world has come to know her better in death by her Iranian name: Mahsa Amini.

Shortly after Amini’s violent death on 16 September, protests broke out
and spread from the Kurdish parts of Iran to the whole country and the world.

Demonstrators chanted  the Kurdish slogan “jin, jiyan, azadî” – “woman, life, freedom”.

But in news reports, particularly Western ones, Jîna Amini’s Kurdish
identity has been erased – she is described as an Iranian woman and her
‘official’ Persian name ‘Mahsa’ – which for her family and friends existed
only on state-documents –is the one in headlines.

Calls to “say her name” echo in real life and across social media but
unwittingly obscure Jîna’s real name and, in doing so, her Kurdish identity.

Amini’s death has seen Kurdish slogans calling for women’s liberation
and revolution echo around the world. “Jin, jiyan, azadî” – and its
translations – has reverberated through crowds and demonstrations held
in solidarity with freedom-seeking women in Iran.

Even in Afghanistan women chanted the slogan, despite attacks on demonstrators by the Taliban.

This chant originated in the Kurdistan women’s liberation movement.
It embodies the movement’s goal: to liberate life through a women’s
revolution.

It was first chanted collectively by Kurdish women `on 8 March 2006 <https://mobile.twitter.com/elifxeyal/status/1576507905926664193>`_, at
gatherings marking International Women’s Day across Turkey.

After this came a period in which annual campaigns challenged patriarchal
mindsets and misogynist practices within Kurdish society.

This period of intense struggle against patriarchy culminated in the
Rojava revolution 10-years-ago, on 19 July 2012, which sent the slogan
“jin, jiyan, azadî” echoing around the world, beyond the borders of Kurdistan.

The Kurdish women’s movement does not aim to monopolise this slogan, in
contrast it aims to universalise it in the struggle for women’s democratic
confederalism worldwide.

Nevertheless, its roots and context should be acknowledged.

Otherwise, we run the risk of emptying our slogans of active struggle
and allowing them to lose their meaning.
As I write this piece, women of the German party CDU/CSU – under whose
government the Kurdish liberation movement has been criminalised the
most – are protesting Jîna’s killing in Berlin, holding posters with
the German translation of “jin, jiyan, azadî”.

Jîna Amini was a Kurdish woman. Kurdish women have fought so hard not
to be erased in life; do not let their stories be rewritten in death.

Meral Çiçek is a Kurdish political activist and journalist.

