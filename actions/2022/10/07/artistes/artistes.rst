.. index::
   ! Be the voice of iranian people

.. _artistes_iran_2022_10_07:

===================================================================================================================================================
2022-10-07 La communauté artistique iranienne, engagée aux côtés des manifestants, avec des productions puissantes et touchantes. #MahsaAmini
===================================================================================================================================================

- https://www.youtube.com/watch?v=7YILf6SUKS4

La communauté artistique iranienne, engagée aux côtés des manifestants,
avec des productions puissantes et touchantes. #MahsaAmini

.. figure:: images/video_artistique.png

   https://twitter.com/FaridVahiid/status/1578480720334708736?s=20&t=Iy5eeDCsch0nmUlYdfdsXQ


.. figure:: images/be_the_voice.png

   https://twitter.com/FaridVahiid/status/1578480720334708736?s=20&t=Iy5eeDCsch0nmUlYdfdsXQ
