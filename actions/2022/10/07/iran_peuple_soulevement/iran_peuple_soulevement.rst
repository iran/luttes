.. index::
   pair: Article; Un peuple qui se soulève
   pair: Contretemps; Un peuple qui se soulève
   ! Ali Karimi

.. _contretemps_iran_2022_10_07:

===================================================================================================================================================
2022-10-07 Un article de Contretemps **Iran : un peuple qui se soulève**
===================================================================================================================================================

- https://www.contretemps.eu/iran-peuple-soulevement/


Introduction
============

Alors que la répression déployée par l’État iranien a fait près de 200
morts en trois semaines, le soulèvement populaire qui défie le régime
ne faiblit pas.

HB et AB sont membres d’un collectif d’Iranien·nes en France, féministes
et queers anticapitalistes et internationalistes, qui a organisé plusieurs
rassemblements et actions en soutien au mouvement populaire en Iran.

Ils reviennent pour Contretemps sur le mouvement actuel et la vague de
contestations que connait l’Iran depuis 2009.

Par souci de précaution, ils préfèrent rester anonymes. Cet entretien a
été réalisé le 3 octobre 2022.


Pouvez-vous rappeler ce qui a déclenché le mouvement populaire actuel en Iran et comment celui-ci s’est développé dans tout le pays ?
=======================================================================================================================================

HB et AB – Le soulèvement actuel a été déclenché après la mort de
Jîna Mahsa Amini, une jeune femme de 22 ans, à Téhéran.

Jîna était originaire de :ref:`Saqqez <saqqez>`, une ville du Kurdistan d’Iran.

Cela a déclenché des rassemblements, dans un premier temps devant
l’hôpital où elle avait été emmenée. Mais le véritable point de départ
a été l’enterrement de Jina, où l’on a accusé tout de suite les dirigeants
du pays comme étant coupables de ce meurtre.

Les slogans ont immédiatement demandé la fin du régime, et le mot d’ordre
le plus repris était « Jin, Jiyan, Azadî », c’est-à-dire, « femme, vie, liberté ».

Ce slogan s’est propagé partout dans le pays.

Cet événement est important parce qu’on n’a pas fait comme pour un
enterrement normal ; des femmes kurdes ont pris la parole pour accuser
le régime et ont revendiqué l’égalité.
Ça a été vraiment l’événement déclencheur de cette insurrection.

Le gouvernement voulait organiser rapidement cet enterrement.
À trois heures du matin, la ville a été encerclée par les forces de
l’ordre pour empêcher la population des villes voisines de rejoindre
l’enterrement, mais les habitants de Saqqez sont sortis dans la rue et
ont permis que les autres personnes puissent entrer dans la ville.

Une grande cérémonie a été organisée, et ce qui était touchant, c’est
que les femmes ont pris la parole, en particulier les enseignantes.

Ça s’est inscrit dans une mobilisation depuis plusieurs années.

À Saqqez les travailleur.ses sont actif.ves. Il y a eu beaucoup d’arrestations
le premier mai dernier qui ont touché en particulier les plus connus des
activistes parmi les travailleur.ses, et en juin, ce sont une vingtaine
d’enseignant.es qui ont été arrêté.es. Il y avait donc déjà une certaine
mobilisation sur place, et c’est grâce à ces travailleur.ses mobilisé.es
que la cérémonie s’est transformée en événement.

Contretemps – Comment est-on passé d’un évènement local à une mobilisation d’ampleur nationale ?
====================================================================================================================================================

::

    Et qui se mobilise depuis l’enterrement de Jîna ?

HB et AB – Il y a eu d’abord des manifestations dans quelques endroits,
au Kurdistan, à Téhéran, dans d’autres villes, les universités.

Cela s’est propagé très vite, et aujourd’hui, les 31 provinces iraniennes
sont touchées par le mouvement. On parle de 80 villes qui auraient connu
des soulèvements, et il y a des villes qui tous les soirs connaissent
des manifestations, des scènes d’affrontement avec la police anti-émeutes.

C’est donc assez répandu au niveau géographique.

Si l’on considère Téhéran, la mobilisation a lieu aussi bien dans les
quartiers riches que dans les quartiers pauvres, dans des quartiers qui
n’avaient pas été concernés par les mobilisations antérieures.

C’est un mouvement transclasses, mais également un mouvement qui touche
différentes minorités ethniques.

Le mouvement a été déclenché au Kurdistan, et ce n’est pas anodin, parce
que le Kurdistan est très politisé, et c’est aussi une région où il y a
beaucoup de répression ; les premières cibles de la répression de l’État
sont très souvent des militants kurdes.

Mais le mouvement a produit une solidarité dans tout le pays et parmi
d’autres minorités ethniques comme par exemple les Azerbaidjanais d’Iran
qui soutiennent le peuple kurde en disant que l’Azerbaïdjan est le refuge
des Kurdes.

On constate la même chose dans le Baloutchistan d’Iran qui a été aussi
fortement réprimé.

Autrement dit, les Kurdes représentent les autres ethnies, dans leur
affrontement face à l’État.


Contretemps – On constate en effet que le poids des Kurdes est important dans le déclenchement du soulèvement et ses suites.
==============================================================================================================================

::

    De même, dans certains rassemblements de soutien en France, on observe
    une forte présence kurde. Comment expliquer que cela n’ait pas été un
    frein au développement de la mobilisation dans tout le pays ?


HB et AB – Le Kurdistan était prêt à réagir à un tel contexte et à
mobiliser les gens. Cela ne veut pas dire que si Jîna avait été originaire
d’une autre ethnie, on n’aurait pas eu la même chose, mais peut-être que
ça n’aurait pas pris une telle dimension.

En plus des soulèvements depuis 2009, on a connu de nombreuses grèves,
particulièrement à partir de 2016. Le peuple s’est donc préparé pour
descendre dans la rue. Mais la mobilisation n’a pas été vue comme une
mobilisation des Kurdes, les slogans kurdes ont été repris par d’autres
communautés, les Turcs d’Iran ont affiché leur solidarité avec les Kurdes…
tout cela a favorisé une unité entre les différentes communautés, alors
que le gouvernement essaye depuis des années de créer des tensions entre
Turcs et Kurdes.

Et donc ça a dépassé très vite la question du Kurdistan, du hijab, de la
mort d’une jeune femme kurde… Ça a commencé au Kurdistan, mais ça ne
finira pas au Kurdistan.

Par ailleurs, le Kurdistan n’est pas la seule région touchée par les
révoltes populaires récentes.
En juillet 2021, il y a eu une révolte dans la province du Khuzestan,
suite à une très importante pénurie d’eau, dans une région par ailleurs
riche en pétrole :ref:`[1] <note_1>`.
Durant cette révolte, plusieurs manifestants ont été abattus et des centaines
ont été arrêté.es. Il y a eu dix jours de révolte, et ça a provoqué des
soutiens dans d’autres régions, mais le soulèvement ne s’est jamais
vraiment propagé dans le reste du pays.

Là on est dans une situation très subversive, peut-être révolutionnaire,
car on est arrivés à un point de non-retour pour une bonne partie de la
population.

Depuis 2017, toutes les révoltes avaient quelque chose de radical comme
aujourd’hui, elles visaient directement le régime, et n’attendaient plus
rien de l’opposition entre le régime et les réformateurs.

Il y a quelques mois, en mai dernier, la construction d’un centre commercial
a entrainé la mort d’une quarantaine de personnes à Abadan, à nouveau
dans le Khuzestan. Le propriétaire du centre commercial est proche du
régime, des pasdarans :ref:`[2] <note_2>`, et tout de suite les protestations qui ont suivi
ont demandé la fin du régime.

En dehors des séquences insurrectionnelles de 2017 et de 2019, le pays
connaît une vague régulière de contestation depuis au moins 7 ans : les
ouvriers contre la précarisation du travail et les privatisations – notamment
la lutte des ouvriers de Haft Tapeh et des habitant.es de Shoush, les
ouvriers du complexe d’acier de Ahvaz… ou la grève des travailleurs
de plusieurs grandes raffineries de pétrole, les retraité.es, les infirmières,
les étudiant.es, les grandes mobilisations des enseignant.es de l’Éducation nationale,
les routiers, les chauffeurs de bus, etc.

À titre d’exemple, l’année 1400 du calendrier iranien (c’est-à-dire du
21 mars 2021 jusqu’au 21 mars 2022), a été l’une des années les plus
mouvementées de ces quatre dernières décennies quant aux contestations
ouvrières et salariales.
Le mouvement de 2022 n’est donc pas apparu dans un ciel serein pour le
pouvoir de Téhéran.


Contretemps – Par rapport aux soulèvements précédents que le pays a connus depuis 2009,
==========================================================================================

::

    constate-t-on des changements dans la composition des manifestations,
    dans les caractéristiques des personnes mobilisées ?

HB et AB – Si on compare avec les autres soulèvements massifs qui ont
eu lieu en 2009, 2017 et 2019 – il y en a eu d’autres mais de moindre
importance, ou plus anciens dans les années 1990 –, ce n’est pas les
mêmes dynamiques.

En 2009, il s’agissait plus d’un mouvement réformiste, composé des
classes moyennes, particulièrement au nord de Téhéran et dans les
grandes villes centrales du pays comme Ispahan, Tabriz, Shiraz où l’on
trouve les classes moyennes.

Les revendications étaient tournées vers des réformes et des changements
au sein du mouvement, même si la dynamique du mouvement l’avait fait évoluer.

Au départ, les manifestants revendiquaient majoritairement le respect du
résultat des élections, et contestaient la réélection frauduleuse de
Ahmadinejad, mais, au bout de 8 mois, vers la fin du mouvement, les
slogans se sont radicalisés et visaient plus directement Ali Khamenei,
le guide suprême.

En 2017, la crise économique, qui n’était pas seulement due aux sanctions
états-uniennes, a provoqué une explosion parmi les gens ordinaires,
particulièrement au sud de Téhéran, là où habitent les classes populaires,
et dans une centaine de petites villes dont on entendait le nom pour la
première fois, dans des régions très défavorisées et marginalisées.

Et en 2019, ça a été la même chose, des gens ordinaires.

Il faut savoir que du fait de la crise économique permanente, il y a
beaucoup de personnes pauvres, il y a des bidonvilles dans chaque grande
ville, **parfois avec 30 à 40 % des habitant.es qui vivent dans ces
bidonvilles**.

Les habitant.es de ces bidonvilles ont osé sortir dans la rue en 2017
et 2019, et maintenant, ce sont plusieurs classes qui sont mobilisées,
les classes moyennes et les classes subalternes.
On ne peut pas dire qu’elles agissent complètement ensemble, mais elles
sont toutes dans la rue. Aujourd’hui, les classes moyennes, même les
fractions les plus hautes, sont touchées par la crise, leur pouvoir
d’achat est affaibli.

Et par ailleurs, la question des droits des femmes traverse toutes les
classes.

La République islamique est basée sur la répression des femmes, sur la
propriété du corps des femmes. Si l’Islam politique insiste beaucoup sur
le hijab, alors qu’il n’existait pas avant, ou pas sous sa forme intégrale,
**c’est qu’il est le symbole de l’Islam politique**.
Pour le gouvernement c’est essentiel, il cherche à stabiliser son pouvoir
par l’appropriation du corps des femmes. Une grande partie des femmes
sont fatiguées de porter chaque jour ce hijab.

La répression des femmes s’est donc retrouvée cette année commune aux
différentes classes, ce qui explique que la mobilisation ne se limite
pas à un groupe social.

On voit le même phénomène concernant les différentes générations.
En Iran, il y a toujours eu une grande différence et des tensions entre
les générations. Les jeunes accusaient souvent les anciennes générations
parce qu’elles avaient fait la révolution en 1979, mais le décalage est
beaucoup moins fort aujourd’hui.
Si les plus ancien.nes ne sont pas présents dans la rue, iels soutiennent
les manifestations.


Contretemps – Quelles formes a pris la mobilisation ? Quels sont les slogans, les revendications, les formes d’organisation ?
================================================================================================================================

HB et AB – Il n’y a pas de revendication, contrairement à 2009 par exemple
lorsque le mouvement s’adressait plutôt au président qu’au guide suprême.

Aujourd’hui, on ne s’adresse pas au président, ce que les gens visent,
ce n’est pas le président, c’est le guide. Les mots d’ordre sont surtout
« À bas la dictature, À bas la République islamique », c’est tout.

Les manifestants ne parlent donc pas du tout avec le pouvoir, il n’y a
pas de communication.

Le guide a parlé pour la première fois ce matin (3 octobre 2022), pour remercier
les forces de police. Le président Raïssi est intervenu à plusieurs
reprises, avant et après l’intervention de Khamenei, mais il est complètement
aligné sur le guide. Sa ligne est d’expliquer que le soulèvement est
téléguidé de l’extérieur. Il remercie les forces de l’ordre et continue
à menacer les personnes qui manifestent leur colère, il les qualifie
de casseurs et de fouteurs de trouble. Cependant il précise parfois
qu’il faut distinguer entre « trouble » et « contestation » et qu’il
faut entendre les critiques. Mais les personnes mobilisées attendaient
surtout la réaction de Khamenei pour savoir la façon dont le régime
agirait dans les jours suivants.

À côté des slogans qui visent directement la dictature, on entend beaucoup
le slogan « Jin, Jiyan, Azadî » (femme, vie, liberté).

Celui-ci ne date pas des manifestations actuelles, il a une histoire,
il est utilisé depuis 2005 au Kurdistan, et encore plus depuis 2013,
après l’assassinat à Paris de trois militantes du PKK par les forces
turques et suite aux événements de Kobané et la libération de la ville
par les YPG, les unités de protection du peuple dans lesquels on trouve
de nombreuses femmes qui utilisaient ce slogan.

Celui-ci était donc déjà connu au Kurdistan mais aussi dans d’autres pays,
à Paris, Berlin, Kaboul…

Le jour de l’enterrement de Jîna, les gens ont utilisé ce slogan, et
il a été repris ailleurs, en particulier à l’université de Téhéran, à
côté d’autres qui remettent en cause les fondements de la République
Islamique et le pouvoir des religieux.


Tous ces slogans visent l’entièreté du régime. On a aussi entendu des
slogans contre les royalistes, qui disent « À bas la répression, qu’elle
vienne du guide ou du Shah », ou « À bas l’oppresseur, que ce soit le guide ou le Shah »,
ce qui est important parce que l’opposition de droite à la République
Islamique s’est rassemblée autour de la figure du fils du Shah d’Iran,
Reza Pahlavi:ref:`[3] <note_3>`, qui a acquis une présence médiatique importante ces
dernières années.

Il y a aussi des slogans qui défendent la solidarité entre différentes
ethnies, différentes populations du pays, comme « Du Kurdistan à Téhéran,
tout l’Iran est dans le sang » :ref:`[4] <note_4>`, ou d’autres slogans qui sont scandés
en langue turque ou en turc azéri :ref:`[5] <note_5>`.

Après la mort de Jîna, la revendication n’était pas de mettre de côté
le hijab ni même d’abolir la police des mœurs, mais d’abolir l’ordre
qui impose le hijab aux femmes.
Le hijab est un pilier de la répression que le régime mène à l’encontre
des femmes, et s’attaquer à son imposition par le régime constitue un
enjeu majeur pour les contestataires.

Ça ouvre un espace énorme pour lutter contre toutes les inégalités, toutes
les formes d’oppression dont la République Islamique est responsable.
On n’est pas dans la convergence des luttes comme on en parle en France,
on fait un aller direct du particulier au général.

Il y a tout un ordre symbolique qui s’effondre. C’était impressionnant
de voir beaucoup de sportifs de haut niveau, des gens très célèbres,
des acteurs et des actrices – dont certaines se dévoilent –, des cinéastes,
qui se sont désengagé.es vis-à-vis du régime et qui soutiennent le mouvement.
Ça dit quelque chose de la profondeur de ce qui est en train de passer,
de cette rupture qui traverse le pays, car il s’agit de personnes dont
le travail et le statut dépendent en partie du régime, ce qu’on n’avait
jamais vu à ce niveau-là auparavant.
C’est le cas par exemple du plus grand footballeur de l’histoire de l’Iran,
Ali Karimi :ref:`[6] <note_6>`. Ça donne une idée de la gravité de la situation
pour le régime.

Concernant les formes d’organisation collective, étant donné la répression
du régime, c’est habituellement impossible de s’organiser, de créer des
associations.
Les gens sont à la maison et forment des groupes à travers leurs réseaux
amicaux. Il peut s’agir de groupes culturels, mais aussi politiques.
Par exemple des groupes qui se réunissaient pour regarder des films et
discuter de cinéma, mais ils en profitent pour parler de politique.
De même, il existe beaucoup de groupes qui se réunissent pour parler
d’écologie ou organiser des activités pour l’environnement.

Tous ces petits groupes affinitaires créent des opportunités pour
s’organiser, formés par les réseaux personnels, qui permettent d’organiser
le mouvement, ce à quoi il faut ajouter d’autres groupes : les syndicats
des travailleur.ses, notamment les enseignant.es, les infirmier.es,
les chauffeurs de bus de Téhéran, ainsi que les retraité.es et les étudiant.es.

Contretemps – On a vu qu’à Paris, le soutien aux manifestant.es iranien.nes avaient donné lieu à des manifestations organisées par différents groupes
=========================================================================================================================================================

::

    L’opposition au régime de Téhéran ne semble pas unifiée.
    Quelles sont les composantes de cette opposition et sont-elles capables d’agir en commun ?

HB et AB – Il n’y a pas d’opposition au sein du régime, du moins pas
d’opposition officielle. Les deux tendances, à savoir les « principialistes »
et les « réformateurs » sont inscrits dans un espace politique homogène,
où il n’y a pas de contestation, et pas beaucoup de jeu politique possible.

Les premiers rassemblent les groupes politiques les plus proches du guide
suprême et des gardiens de la révolution, plus hostiles aux libertés politiques
et sociales et aux relations avec l’occident, au moins en apparence.

Rappelons que dans l’organigramme du pouvoir de la République Islamique
d’Iran, le guide suprême est l’instance politique qui possède le pouvoir
décisionnaire et exécutif le plus important, que ce soit sur le plan politique,
économique, militaire ou judiciaire.

Les gardiens de la révolution, la première force d’armée du régime, qui
est en même temps la première puissance économique du pays, sont dirigés
directement par le guide suprême.

Aujourd’hui, les trois pouvoirs (exécutif, judiciaire et législatif) sont
dirigés par les personnes très proches de Khamenei.
Beaucoup de réformistes ont soutenu Hassan Rohani lors des élections
présidentielles de 2013, mais ce dernier a été le chef du conseil de la
sécurité nationale pendant 16 ans et a fait partie pendant une vingtaine
d’années du camp conservateur.
De fait, la « séquence réformiste » s’est terminée en 2009, et cela a été
particulièrement visible en 2017 quand pour la première fois les étudiant.es
ont déclaré la rupture entre le peuple et les réformistes par ce slogan :
« réformistes, fondamentalistes, c’est la fin de votre histoire ! »

Au niveau économique, **la quasi-totalité des tendances à l’intérieur du
régime sont partisanes d’une économie capitaliste**, elles sont toutes
d’accord pour privatiser l’économie et pour mettre en place des réformes
néolibérales.
Elles divergent seulement sur la question de savoir qui sont les acteurs
et les partenaires de cette économie.

En face, en ce qui concerne les partis et les organisations politiques
structurées en opposition au régime et à l’intérieur de l’Iran, à
l’exception peut-être du Kurdistan d’Iran, on ne voit pas d’activité
importante, du moins de façon non clandestine, car toute sorte d’activité
politique partisane de ce genre est **interdite et réprimée**.

Au Kurdistan d’Iran, on peut parler de trois partis et de leurs différentes
branches : le Parti Démocrate du Kurdistan d’Iran (PDKI) qui s’est reformé
le mois dernier en un seul parti après la fusion des deux branches du
parti démocrate ; Komala (« Organisation révolutionnaire du peuple ouvrier du Kurdistan »),
d’origine maoïste dans les années 1970, scindé en trois branches, qui a
rompu avec le communisme et se revendique aujourd’hui du fédéralisme et
de la social-démocratie ; et le parti « Pour une vie libre au Kurdistan » (PJAK )
qui est la branche du PKK en Iran.

Ces partis ont beaucoup de membres à l’étranger, mais ils sont aussi
actifs à l’intérieur du pays dans la clandestinité.

Il y a aussi d’autres partis comme Al Ahwaziya (le mouvement de libération national de Ahwaz)
au Khuzestan, et le Front Uni du Baloutchistan d’Iran.


Concernant les groupes d’opposant.es à l’étranger, il existe différentes
tendances politiques au sein de la diaspora iranienne, mais aucun parti
ni aucune personnalité ne font autorité aujourd’hui en Iran.
Ces dernières années, une nouvelle opposition de droite est apparue en
autour de la figure de Reza Pahlavi, le fils du chah.

Mais on trouve aussi des activistes des droits humains, des groupes
féministes, des partis et des organisations représentant différentes
tendances de la gauche (de la tendance sociale-démocrate jusqu’à la gauche radicale).

Il y a aussi des groupes à tendance « nationaliste » qui se considèrent
comme des héritiers du mouvement de nationalisation du pétrole et son
leader Mossadegh.
Aucun de ces groupes ne représente de grandes organisations.

Et puis il y a les Moudjahidin du Peuple, une des organisations politiques
les plus importantes lors de la révolution de 1979, fondée au départ sur
une sorte d’Islam politique de gauche.
Aujourd’hui, cette organisation cherche ses principaux alliés parmi les
personnalités de la droite et de l’extrême droite européenne et états-unienne
et semble être isolée par rapport à d’autres groupes d’opposition.
Il faut bien avoir en tête que toutes les oppositions ont subi une forte
répression qui a déstructuré les pratiques partisanes en Iran.
Pendant la première décennie après la révolution, plus de 10 000 militant.es
ont été exécuté.es par le régime.


Contretemps – Comment décrire l’attitude du pouvoir ?
==========================================================

::

    On a pu voir une répression violente, mais il semble que tous les
    corps répressifs n’ont pas encore été utilisés ; comment opère la
    violence contre les manifestant.es ?

HB et AB – La répression est forte. Mais il semble qu’elle soit pour le
moment moins forte que celle qui a eu lieu lors de la révolte de 2019 où
en l’espace de moins d’une semaine, 1 500 personnes ont été tuées selon
l’agence Reuters.

Jusqu’aujourd’hui (3 octobre 2022), le nombre des victimes arrive à 133.

Le massacre le plus sanglant a eu lieu vendredi dernier à :ref:`Zahedan <zahedan>`,
dans  le Balouchistan iranien, où au moins 58 personnes ont été tuées.

Les gardiens de la révolution ont également bombardé des locaux et des
bases des partis kurdes iraniens en Irak et ont tué 13 personnes.

Pourtant, les pasdarans contrairement à 2019 ne sont intervenus dans la
répression que de façon ponctuelle. Les forces de l’ordre (Police et la
police anti-émeute) mènent la majeure partie de la répression, sous la
tutelle du ministère intérieur et les basidjis[7].
Une autre particularité quant aux réactions du régime vis-à-vis de la
situation concerne le long silence d’Ali Khamenei. Il n’est intervenu
sur les événements qu’après plus de deux semaines d’émeutes.

Le meurtre de Jîna Mahsa Amini a suscité deux types de réactions au sein
du régime. Le noyau dur du régime ne recule pas et reste sur ses positions.
Les plus modérés reviennent sur la question de la police des mœurs et
demandent sa suppression.
Mais cette dernière position n’a pas de poids suffisant et de toute façon
il semble trop tard pour faire marche en arrière. Par ailleurs, un groupe
d’enseignant.es et d’étudiant.es des écoles religieuses de Téhéran,
de Qom et de Mashhad ont écrit une lettre ouverte qui annonce que
Ali Khamenei n’a pas de légitimité (religieuse) pour gouverner.

Ce texte rejette également le principe du gouvernement des doctes sur
lequel le système politique de la république islamique est bâti.

Contretemps – Est-il possible d’envisager ou d’imaginer des perspectives à ce mouvement ?
===========================================================================================

::

    Comment un mouvement aussi puissant qui s’affronte aussi directement
    au régime mais qui subit une telle répression, peut-il se développer ?

HB et AB – Il semble qu’on soit arrivés à un point de non-retour.
En tout cas, c’est un ressenti partagé par beaucoup d’iranien.nes.

L’effondrement de l’ordre symbolique et la nécessité que chacun.e ressent
de choisir son camp nous poussent à y croire.
Cela ne veut pas nécessairement dire que le régime tombera dans les
deux mois qui viennent. Mais c’est la perspective que dans l’avenir de l’Iran,
l’Islam politique n’aura aucune place dans le gouvernement.

L’autre question, c’est-à-dire celle de savoir quand et comment le régime
sera renversé, dépend de plusieurs autres questions : **jusqu’où le régime
est-il prêt à aller dans la répression ?**

Le mouvement est-il capable d’engendrer ses propres formes d’organisations
pour affronter et épuiser la répression du régime et pour maintenir et
amplifier la mobilisation dans la durée sous différentes formes de luttes
(manifs, grèves…) ?

Le mouvement est-il capable de créer des formes d’organisation politique
capables de gérer la séquence de la chute du régime ?

Ce qu’on voit aujourd’hui c’est que le soulèvement se poursuit après plus
de deux semaines. Il y a une grande mobilisation des étudiant.es avec des
grèves et des rassemblements sur les campus.
Certains enseignant.es les ont rejoint.es. Les enseignant.es de l’éducation
nationale ont déjà organisé deux journées de grève.
Des lycéen.nes participent massivement au mouvement, même au sein de
leurs établissements, ce qui ne s’est jamais produit à cette échelle
depuis la révolution.
Des chauffeurs routiers préparent également des grèves.

Nous avons aussi vu plusieurs journées de grève des commerçants dans
différentes villes, suite à une grève générale dès les premiers jours
du soulèvement au Kurdistan.
Tout cela nous donne l’espoir de croire que ce mouvement a la capacité
de perdurer dans le temps, même si ce ne sera pas toujours possible
d’occuper la rue.


Propos recueillis par Vincent Gay.

Notes
======

.. _note_1:

[1] Le   Khuzestan est la province où se situe la majorité des installations
    pétrolières du pays. Cette région a été aussi une des lieux majeurs
    de confrontation militaire pendant la guerre Iran-Irak.

.. _note_2:

[2] Les pasdarans sont les gardiens de la Révolution islamique, une
    organisation paramilitaire dépendant directement du Guide de la révolution.

.. _note_3:

[3] Reza Pahlavi est né en 1960 à Téhéran. Il est le fils aîné du dernier
    chah d’Iran, se considère comme l’héritier du trône perse.
    Il a été formé aux États-Unis où il vit depuis la destitution de
    son père et demeure proche des milieux néoconservateurs états-uniens.

.. _note_4:

[4] Ou « Du sang dans tout l’Iran, du Kurdistan à Téhéran »,
    « Blood in all of Iran, from Kurdistan to Tehran »

.. _note_5:

[5] L’azéri est une langue parlée en Azerbaïdjan et en Iran.

.. _note_6:

[6] Grande figure du football iranien, Ali Karimi s’est fait remarquer
    dès 2009 lorsqu’avec ses coéquipiers il arbore un bracelet vert lors
    d’un match de qualification pour la coupe du monde, en soutien à
    l’opposition au régime après les élections.

.. _note_7:

[7] Les basidjis sont une force paramilitaire iranienne fondée par
    l’ayatollah Khomeini afin de fournir des volontaires populaires aux
    troupes d’élite dans la guerre Iran-Irak. Depuis, ils sont en charge
    de la sécurité intérieure et extérieure de l’Iran.
