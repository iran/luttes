.. index::
   pair: Piratage ; Télévision d’Etat

.. _piratage_iran_2022_10_08:

==========================================================================================================================================
2022-10-08 PIRATAGE Le JT de 21 h de la chaîne d’info continue de la télévision d’Etat piraté par la **Justice d’Ali**
==========================================================================================================================================


Twitter
=========

|Anonymous| PIRATAGE. Le JT de 21 h de la chaîne d’info continue de la télévision
d’Etat piraté par la **Justice d’Ali** qui place, à côté du portrait
de l’ayatollah Khamenei, ceux de 4 femmes tuées dans la répression

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- et :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|



.. figure:: piratage_chaine_television.png
   :align: center

   https://twitter.com/arminarefi/status/1578839914212626432?s=20&t=jTImyzx_WTL1hoBO0JJXYA




Dauphiné
===========

- https://www.ledauphine.com/defense-guerre-conflit/2022/10/09/le-journal-televise-pirate-en-direct

« Le sang de nos jeunes dégouline de tes doigts. » C'est le message apparu
à l'écran lors de la diffusion, samedi soir, du journal télévisé,
accompagnant une photo manipulée d'Ali Khamenei, montré entouré de
flammes, la tête dans le viseur. « Il est temps de ranger tes meubles (...)
et de te trouver un autre endroit pour y installer ta famille à l'extérieur
de l'Iran », pouvait-on lire sur un autre message accompagnant la photo.

Alors que le guide suprême de l'Iran, Ali Khamenei, s'exprimait sur la
télévision d'État, ont réussi à prendre le contrôle de l'image.
Dans cette courte vidéo, les visages de Mahsa Amini et de trois autres
victimes des protestations ont été affichées à l’écran, alors que le
slogan « Femmes, vie, liberté » était scandé.
