
.. index::
   pair: Rassemblement ; Samedi 8 octobre 2022
   pair: LDH ; Iran

.. _iran_grenoble_2022_10_08:

=====================================================================================================================================================================================================================================================
♀️✊ 3e rassemblement samedi 8 octobre 2022 à Grenoble place Félix Poulat à 14h30 **Femme, Vie, Liberté #mahsa_amini 📣 Rassemblement de solidarité avec la révolte du peuple iranien pour la liberté #Grenoble #NousSommesLeursVoix  #مهساامینی** 📣
=====================================================================================================================================================================================================================================================


Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 08 octobre 2022 cela fait donc 3 semaines 1 jour
=> 22e jour = on est dans la 4e semaine de la révolution, N°semaine:40 (2022)



- :ref:`mahsa_jina_amini`
- :ref:`slogans`


.. ♀️️✊ #Grenoble #jina_amini  #mahsa_amini #NikaShakarami #NagihanAkarsel #NousSommesLeursVoix #EndIranRegime #IranProtests
.. #LetUsTalk #WhiteWednesdays
.. - #TwitterKurds #zhinaAmini #JinaMahsaAmini #MasahAmini #MasahAmini #Masah_Amini  #OpIran #مهساامینی
.. ♀️✊
.. https://iran.frama.io/luttes/actions/2022/10/08/solidarite_avec_la_revolte_du_peuple_iranien_pour_la_liberte.html

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|

Hommage à Nagihan Akarsel une des autrices du slogan :ref:`JinJiyanAzadî <jin_jiyan_azadi>` assassinée le 4 octobre 2022
    :ref:`#NagihanAkarsel <nagihan_akarsel>` |NagihanAkarsel|  (assassinée au Kurdistan d'Irak)


Affiche annonçant le rassemblement
==================================

Ce rassemblement est organisé par **La voix de l'Iran**, comité de Grenoble

Avec le soutien de:

- `LDH Grenoble Métropole <- https://www.grenoble.fr/association/129060/69-ligue-des-droits-de-l-homme-et-du-citoyen.htm>`_
- LDH Iran
- Cercle Laïque
- Association isèroise des amis kurdes (AIAK, https://www.facebook.com/people/Aiak/100064705085192/)


Le QR code pointe sur https://padlet.com/bhrsdqi/MahsaAmini


.. figure:: images/affiche_manif_2022_10_08.png
   :align: center
   :width: 500


Quelques événements (parmi des milliers) du 29 septembre au 7 octobre 2022
=============================================================================

- :ref:`ehess_2022_09_29`
- :ref:`massacre_balouchistan_2022_09_30`
- :ref:`hackers_amini_murderers_2022_10_02`
- :ref:`vrai_prenom_jina_2022_10_03`
- :ref:`nagihan_akarsel_2022_10_04`
- :ref:`nika_shakarami_2022_10_04`
- :ref:`marvarid_ayaz_2022_10_04`
- :ref:`declaration_intersyndicale_2022_10_04`
- :ref:`liberation_shervin_hajipour`
- :ref:`soutien_angela_davis`


Annonces sur internet (réseaux sociaux et sites Web)
=======================================================

Ici-grenoble
---------------

- https://www.ici-grenoble.org/evenement/rassemblement-de-solidarite-avec-la-revolte-du-peuple-iranien-pour-la-liberte


.. figure:: ../../../../images/ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org

Merci à `Ici-Grenoble <https://www.ici-grenoble.org>`_ :) pour l'annonce
du rassemblement.

.. figure:: images/agenda_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/agenda


Twitter
-------------

EnsembleGre38
+++++++++++++++++

Rassemblement de solidarité avec la révolte du peuple iranien pour la
liberté, samedi 8 octobre 14h30 à Félix Poulat #Grenoble.
#Mahsa_Amini #FemmesVieLiberté #WomanLifeFreedom #JinJiyanAzadî #ZanZendegiÂzâdi


- https://twitter.com/EnsembleGre38/status/1578042519140474880?s=20&t=imRRJDs12GT9iI0WUANXYw

Compte rendu de la manifestation
===================================

Videos
--------

- https://youtu.be/oljMfH2ljjo

- photos
- annonce de la manifestation du 15 octobre 2022


