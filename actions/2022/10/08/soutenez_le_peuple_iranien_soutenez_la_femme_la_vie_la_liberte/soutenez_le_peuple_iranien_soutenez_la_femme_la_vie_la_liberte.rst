.. index::
   pair: Appel ; Pour soutenir la rébellion courageuse des femmes iraniennes, et du peuple

.. _soutien_femme_2022_10_08:

=====================================================================================================================================================
2022-10-08 Appel pour soutenir la rébellion courageuse des femmes iraniennes, et du peuple, demandant des libertés politiques et des droits égaux
=====================================================================================================================================================


- https://www.liberation.fr/idees-et-debats/tribunes/soutenez-le-peuple-iranien-soutenez-la-femme-la-vie-la-liberte-20221008_WGI5RMKRRRGITHC5KH3V4WCVUA/?outputType=amp

Article de Libération
============================

Des intellectuels dont Etienne Balibar, Ludivine Bantigny, Phillipe Descola
lancent un appel pour soutenir la rébellion courageuse des femmes iraniennes,
et du peuple, demandant des libertés politiques et des droits égaux.


Les événements faisant suite à l’assassinat de Mahsa Amini, 22 ans, par
la police des mœurs iranienne le 13 septembre 2022 justifient l’indignation
et le plus large soutien international.

Dès l’annonce de sa mort, des femmes se sont rassemblées à Téhéran.

Certaines se sont affichées publiquement sans foulard, d’autres se sont
filmées en train de se couper les cheveux ou de brûler leur voile.

Ces images sont devenues virales. Des manifestations ont lieu dans de
nombreuses villes du pays, comme Qzavin (centre), Kerman (sud) ou Sari
(nord), associant non seulement des femmes et des jeunes mais toutes
les classes d’âge.

:ref:`«Femme, vie, liberté», <jin_jiyan_azadi>` tel est le slogan repris
à travers le pays.

Décidées en urgence et sans la possibilité de faire une autopsie de
Mahsa Amini, ses funérailles ont été organisées dans sa ville natale
de Saqqez, dans la province du Kurdistan, un rassemblement que les forces
de sécurité iraniennes ont dispersé dans la violence.

Malgré la coupure partielle d’Internet et des réseaux sociaux, on a
appris que l’armée avait bombardé le Kurdistan iranien, mais aussi des
activistes kurdes basés en Irak sous prétexte qu’ils seraient à
l’origine de ce soulèvement…

Mobilisation à haut risque
------------------------------

Il faut insister sur la rébellion courageuse des femmes iraniennes, emmenant
avec elles des parties de plus en plus larges du peuple iranien, qui
aurait entraîné, en 20 jours, une répression féroce avec au moins 92 de
morts selon une ONG iranienne basée à Oslo.

Elle survient un mois seulement après la signature par le président
Ebrahim Raïssi d’un décret énonçant des peines plus sévères en cas
d’infraction au code vestimentaire, à la fois en public et en ligne.

Une mobilisation à haut risque pour chacune et chacun des protestataires
qui ne peut que forcer l’admiration si on se souvient que lors des
dernières grandes manifestations de 2019, les forces de l’ordre auraient
tué au moins 321 manifestants selon Amnesty International.

