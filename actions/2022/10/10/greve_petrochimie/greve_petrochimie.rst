.. index::
   pair: Grève ; Pétrochimie

.. _greve_petrochimie_2022_10_10:

====================================================================================
2022-10-10 **Grève dans la pétrochimie** (**Assalouyeh**, Abadan, etc)
====================================================================================

**Appel à la mobilisation et à la grève aujourd'hui à Assalouyeh**
======================================================================

- https://twitter.com/FaridVahiid/status/1579429441700495360?s=20&t=b9ArI5niDIubC2IG8B_8Xw

.. figure:: images/greves.png
   :align: center

   https://twitter.com/FaridVahiid/status/1579429441700495360?s=20&t=b9ArI5niDIubC2IG8B_8Xw


- :ref:`assalouyeh`

Après les universités, les lycées et d'autres secteurs, c'est maintenant
l’un des piliers de l’économie iranienne qui se mobilise.

Appel à la mobilisation et à la grève aujourd'hui à :ref:`Assalouyeh <assalouyeh>`, le cœur
de l'industrie pétrochimique en #Iran. #MahsaAmini


