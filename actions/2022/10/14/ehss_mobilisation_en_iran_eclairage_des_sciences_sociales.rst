.. index::
   pair: EHESS ; Vendredi 14 octobre 2022

.. _ehess_iran_2022_10_14:

================================================================================================================================================================
2022-10-14 **Visioconférence** Invitation presse  **Mobilisation en Iran : éclairages de sciences sociales** #DebatsEHESS #MahsaAmini #iranprotests2022 #shs
================================================================================================================================================================

- https://www.youtube.com/watch?v=QHyBj6nA0rk (Mobilisation en Iran : éclairages de sciences sociales)
- https://www.ehess.fr/fr/communiqu%C3%A9/invitation-presse-%C2%AB-mobilisation-en-iran-%C3%A9clairages-sciences-sociales-%C2%BB


Annonce
========

L’EHESS rassemble plusieurs spécialistes de l’Iran pour analyser sous le
prisme des sciences sociales la mobilisation iranienne qui se joue actuellement.

Rendez-vous vendredi 14 octobre de 12h à 14h pour une visioconférence
animée par Emmanuel Laurentin (France Culture).

Comment analyser l'extraordinaire mobilisation iranienne depuis l'Europe ?
De quelles informations dispose-t-on pour analyser les gestes de la révolte ?
A quelle juste distance se placer pour leur témoigner de notre solidarité
et de notre attention ?

Outre la dimension féministe, puissante, qui donne forme à la révolte
aujourd'hui, quelle place y occupe la question kurde ou celle d'une
jeunesse étudiante qui, aux côtés de tant d'autres, risque sa vie
aujourd'hui ?

Des chercheuses et des chercheurs, spécialistes de ces questions,
apporteront un éclairage venu des sciences sociales sur un mouvement
exigeant d'être ressaisis à différentes échelles, dans le temps et
l'espace de l'Iran.

Un éclairage à distance, donc, mais qui n'exclut ni l'engagement, ni la
proximité.

Cet événement sera modéré par Emmanuel Laurentin, producteur de
l’émission « Le Temps du débat » sur France Culture, accompagné de
chercheuses et chercheurs :

- `Christophe Prochasson <https://www.ehess.fr/fr/personne/christophe-prochasson>`_, président de l'EHESS (absent pour Covid)
- `Caroline Callard <https://www.ehess.fr/fr/personne/caroline-callard>`_, vice-présidente chargée de la recherche et du campus Condorcet
- Chowra Makaremi, anthropologue, spécialiste des frontières et de la
  violence de masse, dont l'Iran post-révolutionnaire (CNR - Iris)
- `Hamit Bozarslan <https://www.ehess.fr/fr/personne/hamit-bozarslan>`_, historien, spécialiste de la Turquie et de la question
  kurde (EHESS - Cetobac)
- `Somayeh Rostampour <https://www.cresppa.cnrs.fr/gtm/equipe/les-membres-du-gtm/rostampour-somayeh/>`_, doctorante en sociologie (université Paris 8)
- `Stéphane Dudoignon <https://www.ehess.fr/fr/personne/st%C3%A9phane-dudoignon>`_, historien des mondes musulmans asiatiques
  (notamment du Baloutchistan), des renouveaux sunnites en Iran et de
  leur politisation depuis les années 1930 (CNRS - GSRL, Cetobac)

::

    #Visioconférence modérée par @EmmanuelLauren2
    @francecultulture avec @CProchasson , @chowmak @laboIRIS @CNRS,
    Hamit Bozarslan (#EHESS @CETOBaC_lab), Somajeh Rostampour (@UnivParis8)
    & Stéphane Dudoignon (@EPHE_PSL @LaboGSRL).

Sur Youtube
=============

- https://www.youtube.com/watch?v=QHyBj6nA0rk (Mobilisation en Iran : éclairages de sciences sociales)


.. figure:: images/extrait_1.png
   :align: center



