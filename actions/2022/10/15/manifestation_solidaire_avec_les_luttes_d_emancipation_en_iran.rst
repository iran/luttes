
.. index::
   pair: Rassemblement ; Samedi 15 octobre 2022

.. _iran_grenoble_2022_10_15:

=====================================================================================================================================================================================================================================================
♀️✊ 4e rassemblement samedi 15 octobre 2022 à Grenoble place Félix Poulat à 14h30 **Manifestation solidaire avec les luttes d’émancipation en Iran** #Grenoble #NousSommesLeursVoix  #مهساامینی📣
=====================================================================================================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 15 octobre 2022 cela fait donc 4 semaines 1 jour
=> 29e jour = on est dans la 5e semaine de la révolution, N°semaine:41 (2022)


- :ref:`mahsa_jina_amini`
- :ref:`slogans`


🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
♀️️✊ #Grenoble #mahsa_amini #NikaShakarami #NagihanAkarsel #HadisNajafi #NikaShakarami  #SarinaEsmaeilzadeh  #NousSommesLeursVoix #EndIranRegime #IranProtests

.. #LetUsTalk #WhiteWednesdays
.. - #TwitterKurds #zhinaAmini #JinaMahsaAmini #MasahAmini #MasahAmini #Masah_Amini  #OpIran #مهساامینی
.. ♀️✊
.. #EndIranRegime #LetUsTalk #TwitterKurds #مهسا_امینی #MahsaAmini #opiran
.. #IranRevolution2022 #IranProrests2022 #JinJiyanAzadi #SayHerName #Rojhilat #Iran
.. #HadisNajafi #NikaShakarami  #SarinaEsmaeilzadeh #FemmesVieLiberté #WomanLifeFreedom #JinJiyanAzadî #ZanZendegiÂzâdi
.. It’s time to call this freedom movement, the #IranRevolution.

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|


Hommage à Nagihan Akarsel une des autrices du slogan :ref:`JinJiyanAzadî <jin_jiyan_azadi>` assassinée le 4 octobre 2022
    :ref:`#NagihanAkarsel <nagihan_akarsel>` |NagihanAkarsel|  (assassinée au Kurdistan d'Irak)


Texte annonçant le rassemblement
==================================

.. figure:: images/affiche_manif_2022_10_15.png
   :align: center
   :width: 500


:download:`Télécharger le tract d'appel à la manifestation du 15 octobre 2022 au format PDF <manifestation_solidaire_grenoble_2022_10_15.pdf>`


En solidarité avec les femmes en lutte pour l’égalité et contre la République islamique d’Iran

**Manifestation solidaire avec les luttes d’émancipation en Iran Samedi 15 octobre 2022 – 14h30 Place Félix Poulat**

Arrêtée le 13 septembre 2022 et décédée le 16 septembre 2022 en garde à vue,
**la République islamique d’Iran a assassiné Mahsa(Jina) Amini, jeune femme
kurde en visite à Téhéran**.
Pour la police des mœurs, elle n’était pas assez voilée.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre. Pas un seul jour sans qu’elles n’affirment leur droit
à exister, avec ou sans voile, comme elles le veulent.

Dans un contexte de crise sociale majeure et de dictature, la grande
majorité de la population se solidarise tous les jours avec les femmes
et la jeunesse.

Partout dans le pays, jour et nuit, la jeunesse (hommes et femmes) entre
en insurrection, et fait reculer des hommes armés jusqu’aux dents.
Des forces de l’ordre, nombreuses et variées, en civil ou en uniforme,
unies dans une répression sanguinaire.
La violence du régime à l’égard des manifestants a fait déjà plus de 200
morts et des milliers de prisonniers ou disparus.

Face à la République Islamique, les insurgé.es occupent la rue, défient
le patriarcat, crient leur haine, réclament justice.

Les manifestants scandent : « Femme, Vie, Liberté ! », « C’est notre dernier
avertissement, c’est le régime que nous visons », « Malgré la répression
sanguinaire, la lutte continue ». « A Bas la République Islamique d'Iran ! »...

**Nous soutenons les aspirations légitimes des peuples d’Iran, des femmes
et de la jeunesse à la justice sociale et à la liberté**.

Nous condamnons fermement la répression envers les manifestant.e.s et
exigeons la libération immédiate de l’ensemble des manifestants détenus,
ainsi que les défenseurs des droits humains, syndicalistes, militants,
étudiants, avocats et journalistes...

Nous soutenons notamment :

- **Le droit essentiel des femmes à disposer de leurs corps**,
- **L’abrogation de la loi rendant obligatoire le port du voile ainsi que
  toutes les lois phallocratiques en vigueur**


Cette manifestation est organisée par **La voix de l'Iran**, comité de Grenoble

Avec le soutien de:

- `LDH Grenoble Métropole <- https://www.grenoble.fr/association/129060/69-ligue-des-droits-de-l-homme-et-du-citoyen.htm>`_
- LDH Iran
- Cercle Laïque
- Association isèroise des amis kurdes (AIAK, https://www.facebook.com/people/Aiak/100064705085192/)
- Cisem
- Ensemble Isère
- `NPA38 <https://www.facebook.com/npa38/>`_
- `CGT Isère <https://ud38.reference-syndicale.fr/>`_
- `FSU 38 <https://fsu38.fsu.fr/>`_

Le QR code pointe sur https://padlet.com/bhrsdqi/MahsaAmini




Quelques événements du 7 au 15 octobre 2022
=============================================================================

- :ref:`evin_incendie_2022_10_15`
- :ref:`piratage_iran_2022_10_08`
- :ref:`soutien_femme_2022_10_08`
- :ref:`sanandaj_2022_10_09`
- :ref:`saqqez_2022_10_09`
- :ref:`greve_petrochimie_2022_10_10`
- :ref:`clip_hommage_jina_2022_10_10_ref`
- :ref:`ehess_iran_2022_10_14`


Annonces sur internet (réseaux sociaux et sites Web)
=======================================================

Ici-grenoble
---------------

- https://www.ici-grenoble.org/evenement/manifestation-solidaire-avec-les-luttes-demancipation-en-iran

.. figure:: ../../../../images/ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org

.. figure:: images/agenda_ici_grenoble.png
   :align: center

Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`

