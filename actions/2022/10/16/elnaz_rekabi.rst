.. index::
   ! Elnaz Rekabi

.. _elnaz_iran_2022_10_16:

===============================================================================================================
2022-10-16 **Elnaz Rekabi a participé aux championnats d’Asie d’escalade à Séoul sans #voile** #Elnaz_Rekabi
===============================================================================================================

.. figure:: images/elnaz_rekabi.png
   :align: center



.. figure:: images/dessin_elnaz_rekabi.png
   :align: center
   :width: 300

   https://twitter.com/Omid_M/status/1582395086800711680?s=20&t=HgdIqI2T65eaC3JPndfX2Q


.. figure:: images/dessin_elnaz_rekabi_2.png
   :align: center
   :width: 300

   https://twitter.com/mushfraj/status/1582395919022317569?s=20&t=HgdIqI2T65eaC3JPndfX2Q


Annonce twitter https://twitter.com/FaridVahiid
===================================================

Des images magnifiques ! |ElnazKerabi| Elnaz Rekabi a participé aux championnats d’Asie
d’escalade à Séoul sans #voile, défiant ainsi les lois de la République
islamique qui imposent un hijab complet aux sportives dans toutes les
compétitions. #Iran #MahsaAmini


.. figure:: images/elnaz_rekabi_jump.png
   :align: center

   https://twitter.com/FaridVahiid/status/1581710263429890050?s=20&t=CtvAgTGyjkLRi6BWStAawQ

