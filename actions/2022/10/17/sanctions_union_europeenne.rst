
.. _sanctions_iran_2022_10_17:

============================================================================================
2022-10-17 **Sanctions contre la police des moeurs et onze dirigeants iraniens impliqués**
============================================================================================

- https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=OJ:L:2022:269I:TOC
- https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=OJ:L:2022:269I:FULL&from=EN


Annonce
===========

- https://twitter.com/arminarefi/status/1581999280989929474?s=20&t=CB5KoRhdCgECBXo8itrhhQ

.. figure:: images/annonce_armin_arefi.png
   :align: center

   https://twitter.com/arminarefi/status/1581999280989929474?s=20&t=CB5KoRhdCgECBXo8itrhhQ

URGENT @afpfr Les ministres des Affaires étrangères de l'Union européenne ont adopté
lundi 17 octobre 2022 des sanctions contre la police des moeurs et onze
dirigeants iraniens impliqués dans la répression des manifestations



Détails
==========

- https://twitter.com/arminarefi/status/1582019231754858496?s=20&t=bFIkLHVK57Tsw-gOpB60KQ
- https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=OJ:L:2022:269I:FULL&from=EN

.. figure:: images/annonce_armin_arefi_detail.png
   :align: center

   https://twitter.com/arminarefi/status/1582019231754858496?s=20&t=bFIkLHVK57Tsw-gOpB60KQ


VOICI LE DÉTAIL des sanctions de l'Union européenne adoptées ce
lundi 17 octobre contre la police des moeurs et onze dirigeants iraniens
impliqués dans la répression des manifestations en #Iran déclenchées
par la mort de #MahsaAmini ⬇️. =>https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=OJ:L:2022:269I:TOC



Le chef de la police des mœurs iranienne : Mohammad Rostami Cheshmeh Gachi
-------------------------------------------------------------------------------

Mohammad Rostami Cheshmeh Gachi est le chef de la police des mœurs iranienne.
Il a dirigé la police de sécurité publique de Kermanshah du début de l’année 2014 au
début de l’année 2019 et a occupé des postes de haut niveau au sein de la police
iranienne du renseignement.

La police des mœurs fait partie des forces de l’ordre iraniennes, et constitue une
unité de police spéciale, chargée de faire appliquer le strict code vestimentaire prévu
pour les femmes, y compris le port obligatoire du voile.

La police des mœurs a fait un usage illégal de la force à l’encontre de
femmes pour non-respect des lois iraniennes sur le hijab, et elle s’est
livrée à des violences sexuelles et sexistes, à des arrestations et
détentions arbitraires, à des violences excessives et à la torture.

Le 13 septembre 2022, la police des mœurs a arrêté arbitrairement Mahsa Amini,
âgée de 22 ans, à Téhéran, pour avoir prétendument porté un hijab de manière
inappropriée. Celle-ci a ensuite été emmenée au siège de la police des mœurs
pour y recevoir un “cours d’éducation et d’orientation”.
Selon des informations et des témoins fiables, elle a été brutalement
battue et maltraitée en détention, ce qui a conduit à son hospitalisation
et, le 16 septembre 2022, à sa mort.

Le comportement abusif de la police des mœurs ne se limite pas à cet
incident et a été largement documenté.
En tant que chef de la police des mœurs iranienne, M. Rostami est responsable
des agissements de la police des mœurs.
**Par conséquent, il porte la responsabilité de graves violations des droits de l’homme en Iran.**
