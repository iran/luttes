
.. _article_farahani_2022_10_19:

=============================================================================
2022-10-19 **Vers un soulèvement généralisé en Iran ?** par Behrouz Farahani
=============================================================================

Sources:

- https://lanticapitaliste.org/actualite/international/vers-un-soulevement-generalise-en-iran
- https://www.europe-solidaire.org/spip.php?article64414
- https://www.pressegauche.org/Vers-un-soulevement-generalise-en-Iran-54640
- :ref:`behrouz_farahani`

Introduction
================

En Iran, le bilan de la répression était, au 13 octobre 2022, de plus de
200 mortEs (dont 23 enfants de 7 à 14 ans), des centaines de blesséEs et
des milliers d’arrestations.

Malgré cela, le mouvement s’est étendu aux lycées, et à certains secteurs
du salariat.

L’entrée en lutte de travailleurEs du secteur pétrolier constitue un
événement majeur.
Ce mouvement exprime son « soutien au soulèvement révolutionnaire des femmes,
des jeunes et des masses opprimées et exploitées ».
Si ces grèves politiques s’étendaient, une modification de l’équilibre
des forces pourrait se produire au profit du mouvement révolutionnaire et
du monde du travail.

Ces grèves se situent dans la suite logique de l’histoire récente du
mouvement ouvrier en Iran. Après des années de luttes revendicatives et
l’expérience de défaites et de victoires, une des parties les plus
conscientes des salariéEs de l’industrie est entrée en scène, scandant
notamment « Mort au dictateur ».
Des militantEs d’autres secteurs ont averti que « si l’appareil de sécurité
du régime n’arrêtait pas ses mesures répressives et ne mettait pas fin à
l’effusion de sang, [ils/elles] se mettron[t] également en grève ».

Vers une convergence ?
======================

Le mouvement « Femme, Vie, Liberté » a fourni le terrain politique pour
que le monde du travail entre à son tour en lutte avec des slogans politiques,
dont « Tous ensemble contre la dictature ».
Le tabou frappant le recours à la grève politique a été levé.

Après une décennie de luttes essentiellement revendicatives, la mobilisation
massive des salariéEs démultiplierait la force et l’énergie du soulèvement
en cours.
C’est en effet la première fois depuis l’écrasement des conseils ouvriers
il y a 35 ans qu’a lieu une grève politique dépassant la sphère des ­
revendications économiques.

Les mouvements de rue sont dans leur quatrième semaine sans discontinuer,
et maintenant une nouvelle force vient les épauler.
Mais c’est uniquement l’expansion de la grève et sa transformation en
une insurrection de masse populaire qui déterminera le destin du système
en place. L’entrée de salariéEs du pétrole sur la scène politique a
rendu cette perspective plus probable et plus claire.

Les nuits agitées des mollahs
================================

Le souvenir du mouvement anti­monarchique de 1979 et l’entrée en lutte des
salariéEs du secteur pétrolier perturbe le sommeil des dirigeants.
Les grèves de 1979 s’étaient en effet rapidement transformées en grève
générale et avaient ébranlé les fondements du régime monarchique.

Malgré la censure, des nouvelles fuitant des cercles du pouvoir montrent
qu’en leur sein règne la panique, ainsi que d’importants désaccords
entre les différents clans.
Malgré la rhétorique répressive des dirigeants, on peut voir sur leurs
visages et percevoir dans leurs paroles la peur provoquée par l’expansion
rapide et la prolongation sans précédent des mobilisations de rue, ainsi
que le début de convergence avec le monde du travail.

La nécessité de l’auto-organisation
========================================

La formation de conseils et/ou de comités de quartier rendrait possible
la convergence rapide et directe des participantEs aux mobilisations de
rue et aux grèves.

Un premier exemple vient de Sanandaj (chef-lieu du Kurdistan) avec la
formation de plusieurs comités de coordination de quartier.
Les jeunes leaders du mouvement apprennent vite.

L’expérience de l’échec final de la révolution de 1979 nous enseigne que
les travailleurEs doivent commencer dès aujourd’hui à établir leurs
propres organisations indépendantes, et en aucun cas accepter d’être
dirigéEs et/ou représentéEs par des personnes ne participant pas aux
grèves et au soulèvement.
C’est en particulier le cas de celles et ceux qui, en sous-main pour
l’instant, sont pousséEs par les grandes puissances, en premier lieu
les USA, et qui se proposent de « sauver la nation du chaos » et/ou qui
voient dans le retour à la monarchie un « symbole de la stabilité et de
l’unité du pays ».

À cela les acteurs majeurs du mouvement en Iran répondent::

    « Mort aux dictateurs, Ni Mollah Ni Chah ».

Les mobilisations dans le monde du travail
==============================================

Il est actuellement très difficile d’avoir des informations vérifiables
sur l’état des mouvements dans les entreprises.
Celles qui suivent sont considérées comme fiables par l’association SSTI
(Solidarité socialiste avec les travailleurs d’Iran).

– Le 10 octobre 2022, des centaines de travailleurs salariés des sites pétrochimiques
  de Bouchehr (site 1), de Hengam à Assalouyeh, et de Kangan se sont
  rassemblés devant l’entrée de leurs lieux de travail en soutien au soulèvement.

  Ceux d’Assalouyeh ont bloqué la route principale.
  Les forces de répression les ont attaqués et plusieurs manifestants
  ont été arrêtés, dont onze sont identifiés.

– Le 11 octobre 2022, les salariés de la raffinerie d’Abadan ont déclaré la
  grève et se sont rassemblés devant l’une des entrées.
  Ils exigeaient notamment la libération des travailleurs arrêtés et
  détenus la veille à Assalouyeh. Des nouvelles arrestations ont eu lieu. 

– Le 12 octobre 2022, les salariés d’une usine pétrochimique de Bouchehr
  ont bloqué une autoroute. Ils ont été dispersés, et 15 ont été interpellés.

