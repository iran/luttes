.. index::
   pair: Pétition; Leaders: Expel Iran's Diplomats / Demand that Political Prisoners Be Freed (2022-10-21)

.. _petition_iran_2022_10_21:

==============================================================================================
2022-10-21 **G7 Leaders: Expel Iran's Diplomats / Demand that Political Prisoners Be Freed**
==============================================================================================

- https://www.change.org/p/g7-leaders-expel-iran-s-diplomats-demand-that-political-prisoners-be-freed


To the Honourable Foreign Ministers of the United Kingdom, France, Germany,
Italy, Japan, Canada and the United States, the G7 Group of Nations: 

We, the founding members of Iranians for Justice and Human Rights, along
with other co-signatories, hereby express our deepest concern about
the developments in Iran over the past five weeks.
 
The brutal crackdown and violence by various forces of the Islamic regime
in Iran in reaction to protests sparked by the Islamic regime's killing
of Mahsa Amini has resulted in countless deaths, injuries, and incarcerations.

It is evident that the Islamic regime in Iran intends to continue and
escalate the inhumane violence against its own citizens.

On October 15, troubling reports and video evidence emerged of shootings
and a fire in Evin Prison, the notorious location in which many prisoners
are incarcerated.

We are concerned that the lives of many prisoners of conscience (including
but not limited to political prisoners, civic activists, artists,
representatives of ethnic and religious groups, and labour unionists)
and dual citizens held in that prison and other prisons across Iran are
in grave danger.


We hereby demand that you:
(1) forcefully and unequivocally demand the release of all prisoners of
conscience in Iran;


(2) immediately designate the ambassadors or other representatives of
the Islamic regime in your countries, serving either within embassies
or international institutions, personae non gratae and order their removal
from your countries in protest to the illegal and inhumane treatment
of protesters in Iran.

