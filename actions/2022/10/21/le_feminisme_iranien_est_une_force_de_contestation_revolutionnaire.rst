
.. _feminisme_iran_2022_10_21:

================================================================================================================================
2022-10-21 **Le féminisme iranien est une force de contestation révolutionnaire** avec Chowra Makaremi interviewée par Anne Roy
================================================================================================================================

- https://twitter.com/chowmak/status/1583455775128944640?s=20&t=wghR6xAfrZCZY8soA88QzQ
- https://us19.campaign-archive.com/?u=924d90f99ddbed07f95341c8b&id=15a4c44418


Introduction
=============

**Le féminisme iranien est une force de contestation révolutionnaire**
Entretien réalisé par Anne Roy, journaliste et membre du comité éditorial de La Déferlante


Chères toutes, chers tous,

Un mois après le début du soulèvement en Iran, les manifestations de
femmes contre le port du voile se sont transformées en un large mouvement
de contestation sociale qui ne semble pas faiblir.

Un cas d’école de convergence des luttes qu’analyse pour La Déferlante
l’anthropologue franco-iranienne Chowra Makaremi


Depuis la mi-septembre et l’assassinat de la jeune Mahsa Amini par la
police religieuse, la jeunesse iranienne manifeste un peu partout aux
cris de « Femme, vie, liberté ».

Comment faut-il, selon vous, comprendre ce slogan ? 
========================================================
 
Zhina Mahsa Amini appartenait à la minorité kurde qui est extrêmement
discriminée en Iran – son « vrai » prénom est d’ailleurs un prénom kurde,
Zhina, que l’État refuse d’enregistrer.

C’est important pour comprendre les circonstances de sa mort : Zhina Mahsa
n’était pas plus mal voilée que la majorité des filles à Téhéran, mais
celles qui sont originaires de la capitale savent où aller pour éviter
les contrôles, comment se comporter avec les agents, à qui donner de l’argent,
qui appeler en cas de problème...

Les protestations ont commencé le soir des funérailles de la jeune femme
dans la ville de Saqqez. C’est de là qu’est parti le slogan en langue
kurde « Femme, vie, liberté », une devise politique inventée au sein du
Parti des travailleurs kurdes (PKK) d’Abdullah Öcalan –  dans lequel,
certes, les femmes n’ont pas toujours été suffisamment représentées,
mais qui a théorisé que la libération du Kurdistan ne se ferait pas sans elles.

Je note au sujet du mot « vie » contenu dans ce slogan que beaucoup de
jeunes manifestantes et manifestants donnent littéralement leur vie pour
le changement de régime qu’ils réclament.

Quand Zhina Mahsa est morte, les premières images d’elle qui ont été
diffusées la montraient en robe rouge en train d’exécuter une danse
traditionnelle qui témoigne d’un culte de la joie qu’on retrouve sur
tous les comptes Instagram ou Tiktok des manifestantes tuées auparavant.

Quelle est la place des féministes dans le mouvement qui agite l’Iran depuis plus d’un mois ?
================================================================================================

La dimension principale de cette révolte est le refus du voile qui est
la matérialisation de ce que les féministes iraniennes appellent
« l’apartheid de genre » : un ensemble de discriminations économiques,
culturelles et juridiques, inscrites dans les lois sur le travail ou
l’héritage.

Mais ce mouvement veut aussi mettre fin à d’autres discriminations : par
exemple, celles contre les minorités comme les Baloutches, les arabophones,
les Baha’is, ou encore les réfugiés afghans de deuxième génération qui
n’ont jamais pu avoir la nationalité iranienne.

« OPPOSER LES MANIFESTANTES IRANIENNES QUI ENLÈVENT LE VOILE AUX FRANÇAISES
MUSULMANES QUI SOUHAITENT LE PORTER, C’EST PASSER À CÔTÉ DE CETTE
PUISSANCE RÉVOLUTIONNAIRE. »

Le mouvement féministe iranien existe depuis trente ans, et il est très
puissant – la Prix Nobel de la paix [en 2003], Shirin Ebadi, est une femme,
tout comme les détenues emblématiques du régime.

Ses militantes ont été entraînées à une lecture juridique du système de
domination, et leur doctrine constitue la colonne vertébrale de nombreuses
formes d’activisme.
Comme théorie et comme méthode, le féminisme intersectionnel iranien
permet aujourd’hui de comprendre comment, pour la première fois depuis
quarante ans, des segments de la population qui n’ont jamais été solidaires
se soulèvent en même temps.


Que demandent les hommes qui prennent part au soulèvement ?
==============================================================
 
Il ne s’agit pas uniquement de manifestations pour les droits des femmes :
les hommes originaires des quartiers populaires descendent aussi dans la
rue pour protester contre la vie chère ; ceux originaires du Kurdistan
manifestent pour ne pas être victimes de violence… Il faut aussi avoir
en tête l’appauvrissement rapide de l’Iran, où les classes moyennes sont
réduites à peau de chagrin en raison du Covid, des sanctions internationales
et de la corruption. Tous ces éléments sont à comprendre ensemble.

Finalement, le voile n’est devenu une demande de premier plan que lorsque,
ces dernières années, les féministes sont arrivées au bout des revendications
réformistes possibles.

C’est ainsi qu’est né l’activisme quotidien sur cette question qui constitue
un des piliers de l’ordre théocratique  – une façon de rappeler à tous·tes
les Iranien·nes que le pouvoir s’inscrit sur les corps.

En 2018, « les filles de la rue de la révolution », défendues par l’avocate
Nasrin Sotoudeh, se sont mises à manifester avec un voile blanc porté
non pas sur la tête mais au bout d’un bâton.
Elles ont écopé de quinze ans de prison et sont encore détenues aujourd’hui.

En France, dans les médias comme chez les commentateur·ices politiques, un
parallèle a souvent été établi entre les Iraniennes qui se dévoilent et
les Françaises musulmanes qui se voilent.

Pensez-vous que cette grille de lecture soit pertinente ?
============================================================

Ce que montre le soulèvement en Iran, c’est que le féminisme n’est pas
uniquement un outil intellectuel qui permet de revendiquer l’égalité à
l’intérieur d’un État de droit mais qu’il peut être une force de
contestation révolutionnaire.

Opposer les manifestantes iraniennes qui enlèvent leur voile aux Françaises
musulmanes qui souhaitent le porter, c’est passer à côté de cette puissance
révolutionnaire.

La haine du voile chez celles qui le brûlent lors des manifestations ne
renvoie à aucune altérité : elles ne détestent pas le voile de leur mère,
de leurs grands-mères et de leurs amies, mais le tissu dont on les emmaillote.

La question qui se pose à cet endroit est celle du contrôle politique du
corps des femmes par les gouvernements partout dans le monde.

Pour autant, je ne souscris pas au raccourci qui consiste à dire :
« En Iran on oblige les femmes à porter le voile et en France à l’enlever. »

En France on oblige les femmes à enlever le voile, et si elles ne le font
pas elles risquent d’être déscolarisées, licenciées ou humiliées devant
leurs enfants.

En Iran ou en Afghanistan, si elles retirent leur voile, elles risquent
d’être torturées et tuées.

C’est une différence constitutive, pas un continuum de violences.

Malgré tout cela, réduire ce qui se passe actuellement en Iran à une
révolte contre le voile, c’est jouer le jeu des réformistes iraniens qui
assimilent la situation insurrectionnelle actuelle à une revendication
vestimentaire.

Tous les slogans demandent un changement de régime, aucun ne dit non au
hijab.
Quand les filles brûlent leur voile dans la rue, c’est une façon de s’en
prendre à un pilier du régime : elles le brûlent en disant « à bas la dictature ».
Il faut les écouter.




