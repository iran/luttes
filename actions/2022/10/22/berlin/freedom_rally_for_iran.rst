.. index::
   pair: Berlin ;  Soldarität mit den Protestierenden im Iran


.. figure:: images/berlin_80_000.png
   :align: center


.. _freedom_iran_2022_10_22:

===========================================================================================================================
2022-10-22 **Freedom Rally for Iran, Soldarität mit den Protestierenden im Iran** #Mahsa_Amini #Berlin #Iran
===========================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022.
ce samedi 22 octobre 2022 cela fait donc 1 mois 6 jours
=> 36 jours = on est dans la 6e semaine de la révolution.


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

::

    #Mahsa_Amini #Feminista #Berlin #Iran #EndIranRegime #LetUsTalk #مهسا_امینی  #IranianProtests2022 #IranRevoIution2022 #IranProtests #SayHerName #JinaAmini
    #Kurdistan #Berlin #protest  #JinaAmini
    #MahsaAmini #TwitterKurds #IranRevolution #IranProtests #SayHerName #JinaAmini
    #PS752 #ps752justice
    #NikaShakarami  #SarinaEsmaeilzadeh


.. figure:: images/the_time_has_come.png
   :align: center

   https://twitter.com/esmaeilion/status/1580032039704985601?s=20&t=x5Ca_ofwcEhJYz-BAu0LwQ


.. figure:: images/berlin_police.png
   :align: center

   https://twitter.com/ehsan_bz/status/1580116190156197889?s=20&t=x5Ca_ofwcEhJYz-BAu0LwQ



.. figure:: images/grossen_stern.png
   :align: center

   https://www.openstreetmap.org/search?query=Gro%C3%9Fer%20Stern%2C%2010557%20%2C%20Berlin%2C%20Germany#map=16/52.5145/13.3468


https://www.lemonde.fr/international/article/2022/10/22/en-iran-le-ministere-de-l-interieur-affirme-que-la-mobilisation-faiblit-une-manifestation-de-soutien-rassemble-80-000-personnes-a-berlin_6146951_3210.html
=======================================================================================================================================================================================================================

- https://www.lemonde.fr/international/article/2022/10/22/en-iran-le-ministere-de-l-interieur-affirme-que-la-mobilisation-faiblit-une-manifestation-de-soutien-rassemble-80-000-personnes-a-berlin_6146951_3210.html


Quelque 80 000 personnes ont ainsi défilé samedi à Berlin, a affirmé un
porte-parole de la police. « Aujourd’hui, des milliers de personnes
affichent leur solidarité aux courageuses femmes et aux manifestants en Iran »,
a salué la ministre de la famille allemande, l’écologiste Lisa Paus, sur Twitter. «
Nous sommes à vos côtés », a-t-elle ajouté.

Parmi les participants à cette manifestation organisée par un collectif
de femmes, certains ont brandi des affiches avec le slogan « Women, Life, Freedom »
(« Femmes, Vie, Liberté »), d’autres des drapeaux kurdes.

Les manifestants ont marché au cœur de la ville dans le calme, a précisé
la police, qui les a comptés à bord d’un hélicoptère.

.. figure:: images/berlin_80_000_2.png
   :align: center

.. figure:: images/berlin_80_000_3.png
   :align: center



Discours
==========

- https://twitter.com/ranarahimpour/status/1583946316031627264?s=20&t=7X5W6gQX1zQ1eSDeny4u8w

Photos
======

- https://www.instagram.com/ninouchkka
- https://www.instagram.com/iranluttes/
- https://www.instagram.com/feminista.berlin/
- https://www.instagram.com/fridaysforfuture.berlin/
