
.. index::
   pair: Rassemblement ; Samedi 22 octobre 2022

.. _iran_grenoble_2022_10_22:

=====================================================================================================================================================================================================================================================
♀️✊ 5e rassemblement samedi 22 octobre 2022 à Grenoble place Félix Poulat à 14h30 **Manifestation solidaire avec les luttes d’émancipation en Iran** #Grenoble #NousSommesLeursVoix  #مهساامینی📣
=====================================================================================================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 22 octobre 2022 cela fait donc 1 mois 6 jours
=> 36e jour = on est dans la 6e semaine de la révolution, N°semaine:42 (2022)


Autres manifestations
========================

- :ref:`freedom_iran_2022_10_22`



- :ref:`mahsa_jina_amini`
- :ref:`slogans`


Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`

