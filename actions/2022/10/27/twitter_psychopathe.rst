
.. _twitter_2022_10_27:

=============================================================
2022-10-27 Twiter est désormais aux mains d'un psychopathe
=============================================================

Twiter est désormais aux mains d'un psychopathe, le deuxième actionnaire
étant l'Arabie Saoudite.

Il est temps de rejoindre le :ref:`fediverse <tuto_fediverse:tuto_fediverse>` et :ref:`mastodon <tuto_fediverse:mastodon>` en particulier.
