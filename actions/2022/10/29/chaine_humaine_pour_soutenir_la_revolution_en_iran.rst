
.. figure:: images/human_chain_the_time_has_come.png
   :align: center
   :width: 300

   https://rally.ps752justice.com/rally/oct29


.. _chaine_humaine_iran_2022_10_29:
.. _iran_grenoble_2022_10_29:

====================================================================================================================================================================
♀️✊ 6e rassemblement **Chaine humaine pour soutenir la révolution en Iran** le samedi 29 octobre 2022 à 15h place Félix Poulat, #Grenoble #NousSommesLeursVoix
====================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 29 octobre 2022 cela fait donc 1 mois 1 semaine 6 jours
=> 43e jour = on est dans la 7e semaine de la révolution, N°semaine:43 (2022)



- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- |ps752justice|

Annonce
=======

.. figure:: images/chaine_humaine.png
   :align: center

   Mains dans les mains, nous soutenons les luttes d'émancipation en Iran


.. figure:: images/femme_vie_liberte.png
   :align: center
   :width: 400

Appel international: créons une chaîne humaine en solidarité avec la lutte
du peuple iranien.

Rassemblement le samedi 29 octobre à 15h place Félix Poulat organisé par
le **Collectif Iran solidarité** avec le soutien de:

- **La voix de l’Iran**, comité de Grenoble.
- LDH Iran,
- `LDH Grenoble Métropole <- https://www.grenoble.fr/association/129060/69-ligue-des-droits-de-l-homme-et-du-citoyen.htm>`_
- Cercle Laique



Liste des villes faisant une chaine humaine
=============================================

- https://rally.ps752justice.com/rally/oct29
- https://rally.ps752justice.com/rally/oct29/Geneva
- https://rally.ps752justice.com/rally/oct29/Lyon
- https://rally.ps752justice.com/rally/oct29/Montpellier
- https://rally.ps752justice.com/rally/oct29/paris
- https://rally.ps752justice.com/rally/oct29/toulouse


Quelques événements du 22 au 29 octobre 2022
=============================================================================

- :ref:`freedom_iran_2022_10_22`


Annonces sur internet (réseaux sociaux et sites Web)
=======================================================

Ici-grenoble 🙏
------------------

.. figure:: images/ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/chaine-humaine-pour-soutenir-la-revolution-en-iran


"Arrêtée le 13 septembre 2022 et décédée le 16 septembre 2022 en garde à vue,
la République islamique d’Iran a assassiné :ref:`Mahsa (Jina) Amini <mahsa_jina_amini>`, jeune femme
kurde en visite à Téhéran. Pour la police des mœurs, elle n’était pas assez voilée.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre. Pas un seul jour sans qu’elles n’affirment leur droit
à exister, avec ou sans voile, comme elles le veulent.

Dans un contexte de crise sociale majeure et de dictature, la grande
majorité de la population se solidarise tous les jours avec les femmes
et la jeunesse.
Partout dans le pays, jour et nuit, la jeunesse (hommes et femmes) entre
en insurrection, et fait reculer des hommes armés jusqu’aux dents.

Des forces de l’ordre, nombreuses et variées, en civil ou en uniforme,
unies dans une répression sanguinaire.
La violence du régime à l’égard des manifestants a fait déjà plus de 200
morts et des milliers de prisonniers ou disparus.

Face à la République Islamique, les insurgé.es occupent la rue, défient
le patriarcat, crient leur haine, réclament justice.
Les manifestants scandent : **Femme, Vie, Liberté !**, **C’est notre dernier
avertissement, c’est le régime que nous visons**,
**Malgré la répression sanguinaire, la lutte continue**.
**A Bas la République Islamique d'Iran !**...

**Nous soutenons les aspirations légitimes des peuples d’Iran, des femmes
et de la jeunesse à la justice sociale et à la liberté**.

Nous condamnons fermement la répression envers les manifestant.e.s et
exigeons la libération immédiate de l’ensemble des manifestants détenus,
ainsi que les défenseurs des droits humains, syndicalistes, militants,
étudiants, avocats et journalistes...

Nous soutenons notamment :

- **Le droit essentiel des femmes à disposer de leurs corps**,
- **L’abrogation de la loi rendant obligatoire le port du voile ainsi que
  toutes les lois phallocratiques en vigueur**

Autres événements ce samedi 29 octobre 2022 à Grenoble
---------------------------------------------------------

.. figure:: images/autres_evenements.png
   :align: center

   https://www.ici-grenoble.org/agenda


Compte rendu
==============

.. toctree::
   :maxdepth: 3

   france3/france3


Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`
