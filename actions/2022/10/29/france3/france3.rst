

.. _france3_iran_2022_10_29:

==================================================================================================================================================
France3 régions **Grenoble : 150 personnes rassemblées pour soutenir le peuple iranien** par Olivia Boisson avec Damien Borrelly
==================================================================================================================================================

- https://france3-regions.francetvinfo.fr/auvergne-rhone-alpes/isere/grenoble/grenoble-150-personnes-rassemblees-pour-soutenir-le-peuple-iranien-2645304.html


Introduction
===============

Plus de 40 jours après le décès de Mahsa Amini en Iran, les manifestations
se poursuivent dans le pays mais aussi dans le reste du monde.
A Grenoble, en Isère, 150 personnes se sont rassemblées ce 29 octobre 2022
pour manifester leur soutien au peuple iranien.

L’émotion est vive, ce 29 octobre 2022, dans les rues de Grenoble.
Alors que des chants iraniens résonnent, des dizaines de pancartes
affichent des visages de celles et ceux qui se sont battus pour leurs
droits au prix de leur vie, suite au décès de Mahsa Amini.

Cette Iranienne de 22 ans est décédée le 13 septembre 2022, après avoir
été arrêtée par la police des mœurs à Téhéran, qui lui reprochait d'avoir
enfreint le code vestimentaire strict de la République islamique, imposant
notamment le port du voile pour les femmes. 

Aujourd'hui, sur place, le climat ne s'apaise pas.

150 personnes réunies à Grenoble
===================================

En France et dans le reste du monde, les manifestations s'enchaînent.

Plus d’une centaine de personnes s’est rassemblée à Grenoble, en Isère,
pour rendre hommage à toutes celles et ceux qui se battent pour leur
liberté. Parmi elles, Zahra, qui vit en France depuis 8 ans.
"J’ai vécu la même chose que Mahsa" confie-t-elle.

::
    J’ai été arrêtée trois fois en Iran, il y a plus de dix ans. Ce n’était
    pas aussi violent mais il y avait des insultes, des humiliations.
    Zahra.

"Cela peut détruire une femme" affirme Zahra pour qui "le cas de Mahsa
Amini est la goutte d’eau qui a fait changer la vie des Iraniens".

Une autre militante, étudiante à Grenoble depuis un an, est là aussi.
Sa famille et ses amis sont en Iran. "Ils se font battre, tuer, emprisonner.
Chaque jour, je vérifie les nouvelles" confie celle qui a quitté le pays
pour vivre librement.

::

    En France, on a la liberté de choisir ce que l’on veut porter, ce que
    l’on veut faire dans la vie. C’est un rêve en Iran.
    Anonyme.

Avant d’ajouter, avec détermination : "On n’arrêtera pas tant qu’on
n’obtiendra pas ce que l’on veut".
