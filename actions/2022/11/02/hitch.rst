
.. _hitch_2022_11_02:

==============================================================
2022-11-02 HITCH UNE HISTOIRE IRANIENNE_Bande annonce
==============================================================

- https://vimeo.com/487185097


.. figure:: images/hitch.png
   :align: center


.. figure:: images/une_histoire_iranienne.png
   :align: center


« Iran, 1988. Des milliers de prisonniers politiques, enfermés depuis les
lendemains de la révolution de 1979, sont massacrés.

Parmi elles et eux, Fatemeh Zarei, ma mère. » @chowmak

« Tandis que l’État iranien nie toujours ses crimes et s’efforce encore
aujourd’hui d’en effacer les traces, le film part en quête des lieux,
des objets et des gestes qui permettront de dénouer le silence. »

« Comment l’absence des corps emprisonne-t-elle nos mémoires, là où le
politique griffe au plus intime ? Là où seul l’intime reste en témoignage
d’une politique ? »

« Au cœur de cette histoire de violence et de déni, j'enquête en dressant
la cartographie de de ce qui reste, quand l'histoire a effacé les êtres
et s'attache à gommer les contours de la disparition. »

Les travaux de Chowra Makaremi portent sur l’anthropologie de l’État, les
formes juridiques et ordinaires de la violence et l’expérience qu’en
font les sujets, notamment en situation d’exil.

Son film “Hitch” sera projeté le mercredi 2 novembre 2022 à 20h à la
Péniche Anako lors d'une soirée en soutien à la résistance du peuple iranien.


.. figure:: images/hitch_explain.png
   :align: center

