
.. index::
   pair: Rassemblement ; Samedi 15 octobre 2022

.. _iran_grenoble_2022_11_05:

=====================================================================================================================================================================================================================================================
♀️✊ 7e rassemblement samedi 5 novembre 2022 à Grenoble place Félix Poulat à 14h30 **Manifestation solidaire avec les luttes d’émancipation en Iran** #Grenoble #NousSommesLeursVoix  #مهساامینی📣
=====================================================================================================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 05 novembre 2022 cela fait donc 1 mois 2 semaines 6 jours
=> 50e jour = on est dans la 8e semaine de la révolution, N°semaine:44 (2022)



- :ref:`mahsa_jina_amini`
- :ref:`slogans`


.. figure:: images/le_point_armin_arefi.png
   :align: center

   https://www.lepoint.fr/monde/iran-il-ne-reste-pas-beaucoup-de-temps-a-vivre-au-regime-04-11-2022-2496509_24.php#xtor=CS2-239


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

♀️️✊ #Grenoble  #NousSommesLeursVoix #EndIranRegime #IranProtests #IranRevolution2022 #IranProrests2022

.. #LetUsTalk #WhiteWednesdays
.. - #TwitterKurds #zhinaAmini #JinaMahsaAmini #MasahAmini #MasahAmini #Masah_Amini  #OpIran #مهساامینی
.. #EndIranRegime #LetUsTalk #TwitterKurds #مهسا_امینی #MahsaAmini #opiran
.. #IranRevolution2022 #IranProrests2022 #JinJiyanAzadi #SayHerName #Rojhilat #Iran
.. #HadisNajafi #NikaShakarami  #SarinaEsmaeilzadeh #FemmesVieLiberté #WomanLifeFreedom #JinJiyanAzadî #ZanZendegiÂzâdi
.. It’s time to call this freedom movement, the #IranRevolution.

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|


Texte annonçant le rassemblement
==================================

En solidarité avec les femmes en lutte pour l’égalité et contre la République islamique d’Iran

**Manifestation solidaire avec les luttes d’émancipation en Iran ce samedi 5 novembre 2022 – 14h30 Place Félix Poulat**

Arrêtée le 13 septembre 2022 et décédée le 16 septembre 2022 en garde à vue,
**la République islamique d’Iran a assassiné Mahsa(Jina) Amini, jeune femme
kurde en visite à Téhéran**.
Pour la police des mœurs, elle n’était pas assez voilée.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre. Pas un seul jour sans qu’elles n’affirment leur droit
à exister, avec ou sans voile, comme elles le veulent.

Dans un contexte de crise sociale majeure et de dictature, la grande
majorité de la population se solidarise tous les jours avec les femmes
et la jeunesse.

Partout dans le pays, jour et nuit, la jeunesse (hommes et femmes) entre
en insurrection, et fait reculer des hommes armés jusqu’aux dents.
Des forces de l’ordre, nombreuses et variées, en civil ou en uniforme,
unies dans une répression sanguinaire.
La violence du régime à l’égard des manifestants a fait déjà plus de 200
morts et des milliers de prisonniers ou disparus.

Face à la République Islamique, les insurgé.es occupent la rue, défient
le patriarcat, crient leur haine, réclament justice.

Les manifestants scandent : « Femme, Vie, Liberté ! », « C’est notre dernier
avertissement, c’est le régime que nous visons », « Malgré la répression
sanguinaire, la lutte continue ». « A Bas la République Islamique d'Iran ! »...

**Nous soutenons les aspirations légitimes des peuples d’Iran, des femmes
et de la jeunesse à la justice sociale et à la liberté**.

Nous condamnons fermement la répression envers les manifestant.e.s et
exigeons la libération immédiate de l’ensemble des manifestants détenus,
ainsi que les défenseurs des droits humains, syndicalistes, militants,
étudiants, avocats et journalistes...

Nous soutenons notamment :

- **Le droit essentiel des femmes à disposer de leurs corps**,
- **L’abrogation de la loi rendant obligatoire le port du voile ainsi que
  toutes les lois phallocratiques en vigueur**


**Cette manifestation est organisée par la communauté Iranienne**.


Quelques événements du 29 octobre au 4 novembre 2022
=============================================================================

- :ref:`hitch_2022_11_02`


Annonces sur internet (réseaux sociaux et sites Web)
=======================================================

Ici-grenoble 🙏
------------------

Autres événements ce samedi 5 novembre 2022 à Grenoble
---------------------------------------------------------

.. figure:: images/autres_evenements_a_grenoble.png
   :align: center

   https://www.ici-grenoble.org/agenda


Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`

Manifestation européenne à Berlin le samedi 22 octobre 2022 (80 000 manifestants)
=====================================================================================

- :ref:`freedom_iran_2022_10_22`


