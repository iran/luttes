.. index::
   pair: Luttes; Genève


.. _luttes_iran_geneve_2022_11_12:

=====================================================================
2022-11-12 Manifestation devant l'ONU à Genève, Place des Nations
=====================================================================

- http://www.solidarite-iran.ch/
- https://www.instagram.com/solidarite_iraniens/

.. figure:: images/attachee.png
   :align: center

.. figure:: images/mollah.png
   :align: center




Manifestation des iraniens
=============================

Des photos ici: https://www.instagram.com/solidarite_iraniens/


solidarite_iraniens
Free IRAN - Manifestation en solidarité
Samedi 12 Novembre 2022 à 14h
Place des Nations

We invite you to join the protest in support of the Iranian people.
We are fighting for one common goal, we all want the same thing.We want
freedom for the Iranian people regardless of our beliefs and backgrounds.

En solidarité avec le peuple Iranien, nous vous invitons à nous rejoindre
pour manifester le 12 novembre 2022 .

Nous nous battons pour le même but, la liberté du peuple iranien,
quelques soient nos origines et nos croyances.

THE TIME HAS COME  #جنبش_همبستگی_ایرانیان_سوئیس.
===========================================================================

#mahsaamini #iranprotests #iran #geneve #switzerland #jinaamini
#zanzendegiazadi #azadi #freedom #revolution #زن_زندگی_آزادی #women #femme
#vie #liberte #jinjiyanazadi


.. figure:: images/danse.jpeg
   :align: center


.. figure:: images/marjane.jpeg
   :align: center



Manifestation des Hazâras (Afghans) au même moment, par Benjamin Joyeux
===========================================================================

- https://librinfo74.fr/les-hazaras-demandent-la-protection-des-nations-unies/

Ce samedi 12 novembre aux environs de 13h, alors que le brouillard commence
à s’estomper un peu, plusieurs dizaines de personnes se rassemblent
autour de la célèbre Broken Chair, devant les locaux des Nations Unies,
Place des Nations à Genève.

Une majorité de jeunes femmes sont présentes et brandissent des pancartes
et des drapeaux. Leur objectif, commémorer le 40e jour du massacre perpétré
dans le centre éducatif de Kaaj, à l’ouest de Kaboul la capitale afghane,
le 30 septembre dernier. Un attentat suicide à la bombe qui avait
provoqué 53 morts et plus d’une centaine de blessés[1], en majorité des
jeunes femmes qui préparaient leur entrée à l’université, dernière
terrible attaque d’une longue série visant particulièrement les Hazâras. 

Une minorité historiquement persécutée
========================================

La communauté hazâra est en effet l’un des groupes ethniques afghans
ayant subi le plus de persécutions au cours de son histoire.

Vivant principalement au centre de l’Afghanistan, mais également présents
au Pakistan, en Iran, au Tadjikistan et dans une moindre mesure au
Turkménistan, et présents dans la région depuis au moins le 15ième siècle,
les Hazâras appartiennent à la branche chiite de l’Islam, dans un
environnement géographique majoritairement sunnite.

Mais certains sont également ismaéliens, sunnites, voir même chrétiens
ou laïcs. Car les Hazâras se caractérisent avant toute chose par des
valeurs libérales de tolérance, se focalisant sur la connaissance,
l’éducation et même l’égalité homme-femme.

Des valeurs totalement inacceptables pour les Talibans et les extrémistes
de l’Etat islamique.

Dès le début du 20ème siècle, Abdur Rahman Khan, alors émir d’Afghanistan,
condamnait par décret les Hazaras comme « infidèles » et exhortait le
reste de la population afghane à mener la guerre sainte contre eux.

Les deux tiers de la population hazâra sont alors décimés, des milliers
de femmes et d’enfants mis en esclavage et leur territoire historique,
le Hazarajat, occupé (c’est au cœur de ce même territoire qu’un siècle
plus tard, en 2001, les Talibans détruisent les célèbres bouddhas de Bamiyan).

La persécution des Hazâras se perpétue jusqu’à nos jours, notamment
lors de la première prise de Kaboul par les Talibans entre 1996 et 2001.

Aujourd’hui, les près de 4 millions d’Hazâras vivant en Afghanistan
(soit presque 9% de la population) craignent pour leur vie, en
particulier depuis le retour au pouvoir des Talibans en août 2021, et
n’hésitent plus à qualifier de « génocide » les persécutions qu’ils
subissent.

C’est pourquoi ils en appellent à la protection de la communauté internationale.

Un appel à la protection internationale
==========================================

Comme l’indique l’ONG Human Rights Watch[2], depuis le retour au pouvoir
des Talibans en Afghanistan le 15 août 2021, le groupe armé État islamique
de la province de Khorasan (ISKP), branche de l’Etat islamique en Afghanistan,
a revendiqué 13 attaques contre les Hazaras, tuant et blessant au moins 700 personnes.

C’est pourquoi en ce 12 novembre, des représentants de la diaspora Hazâra,
regroupés notamment sous la bannière du World Hazara Council[3],
manifeste en maints endroits de la planète, et en particulier devant
les Nations Unies à Genève. Ils demandent l’aide internationale pour
faire cesser immédiatement ce qu’ils n’hésitent pas à qualifier de
génocide, comme énoncé sur les pancartes qu’ils brandissent place des Nations.

Le Conseil mondial hazâra réclame notamment une mission d’enquête onusienne
sur les exactions perpétrées par les Talibans et une pression accrue sur
leur gouvernement.

Parmi les manifestants de ce 12 novembre, certains se montrent extrêmement
sévères vis-à-vis des Etats-Unis qui, après 20 ans d’occupation militaire
en Afghanistan, ont laissé les Talibans reprendre le pouvoir, abandonnant
au passage les minorités ethniques livrées à elles-mêmes.

Comme nous le rappelle Jasmin Jaghuri ( en vidéo ci-dessous), en charge
de la communication lors de la manifestation, les femmes et les filles
afghanes sont les premières victimes de cet état de fait, elles qui,
déjà exclues des écoles secondaires et de la plupart des emplois publics,
viennent de se voir également privées de gymnases et de bains publics
par un pouvoir Taliban barbare et rétrograde[4].

La balle est dans le camp des Nations Unies. Cette manifestation de samedi
rappelle également une fois de plus que les premières victimes du fanatisme
islamique à travers le monde sont bien souvent les musulmans, et en particulier les femmes.

Benjamin Joyeux



