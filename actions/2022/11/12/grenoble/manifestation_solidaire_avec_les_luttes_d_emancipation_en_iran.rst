
.. index::
   pair: Rassemblement ; Samedi 12 novembre 2022

.. _iran_grenoble_2022_11_12:

=====================================================================================================================================================================================================================================================
♀️✊ 8e rassemblement samedi 12 novembre 2022 à Grenoble place Félix Poulat à 14h30 **Manifestation solidaire avec les luttes d’émancipation en Iran** #Grenoble #NousSommesLeursVoix  #مهساامینی📣
=====================================================================================================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 12 novembre 2022 cela fait donc 1 mois 3 semaines 6 jours
=> 57e jour = on est dans la 9e semaine de la révolution, N°semaine:45 (2022)



- :ref:`mahsa_jina_amini`
- :ref:`slogans`


Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`

