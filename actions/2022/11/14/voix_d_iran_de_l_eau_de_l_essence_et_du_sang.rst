
.. _voix_iran_2022_11_14:

================================================================
2022-11-14 Voix d'Iran - De l'eau, de l'essence et du sang
================================================================

- https://blogs.mediapart.fr/sirinealkonost/blog/141122/voix-diran-de-leau-de-lessence-et-du-sang


Les trois prochains jours marquent le troisième anniversaire du « Aban sanglant »
de 1398.

Des appels à manifester ont été lancés dans tout le pays, y compris pour
un rassemblement de plus grande envergure à Téhéran.
Ce témoignage aborde les incertitudes, les inquiétudes et les espoirs des manifestants.

