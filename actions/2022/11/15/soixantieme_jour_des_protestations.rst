
.. _soixantieme_jour_2022_11_15:

========================================================================
2022-11-15 Nous sommes au soixantième jour des protestations en #Iran
========================================================================


Nous sommes au premier jour des trois journées d'actions d'une ampleur sans précédent depuis le début
======================================================================================================

- https://masthead.social/@NaderTeyf/109347906065241019

Nous sommes au soixantième jour des protestations en #Iran après
l'assassinat de #Mahsa_Amini par la police des mœurs;

nous sommes au premier jour des trois journées d'actions d'une ampleur
sans précédent depuis le début, à la mémoire des tués de la révolte
de novembre 2019 annoncées pour les 15, 16 et 17 novembre.

Ce mouvement ne s'arrêtera pas même si le régime des mollahs coupe
définitivement Internet.
Il poursuivra son chemin jusqu'au renversement du régime islamique.


Les grèves de petits commerces dans ces villes
========================================================

- https://masthead.social/@NaderTeyf/109347271335951925

Comme prévu pour les 15, 16 et 17 novembre, un très grand mouvement
d'amplification des protestations a commencé dès ce matin en #Iran de
sorte que couvrir toutes les informations sera difficile.

Des grèves dans les commerces, des manifestations de rue, des rassemblements
dans les universités etc. sont en cours.

Les grèves de petits commerces dans ces villes: #Boukan, #Saghez, #Marivan,
#Najafabad, #Chiraz, #Téhéran, #Sanandaj, #Kamyaran, #Arak, #Bandar_Abbas,
#Gorgan, #Bojnourd, #Kermanchah 🔽


Elles font le V de victoire face à une affiche montrant le monument de la place de la Liberté à #Téhéran
===========================================================================================================

- https://masthead.social/@NaderTeyf/109343310955501166

.. figure:: images/lyceennes_v.png
   :align: center

Ces lycéennes préparent des tracts écrits à la main invitant à amplifier
le mouvement en #Iran pour les 15, 16 et 17 novembre.
Elles font le V de victoire face à une affiche montrant le monument de
la place de la Liberté à #Téhéran.  #Mahsa_Amini
