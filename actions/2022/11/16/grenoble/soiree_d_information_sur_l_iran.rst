
.. _mourir_a_20_ans_iran_2022_11_16:

=====================================================================================================
2022-11-16 **Soirée d’information mercredi 16 novembre 2022 à 20h**, Maison du tourisme Grenoble
=====================================================================================================

- :ref:`mahsa_jina_amini`
- :ref:`slogans`


**60 jours** que le peuple iranien est en lutte contre la République islamique d'Iran.

- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)


♀️️✊ #Grenoble  #NousSommesLeursVoix #EndIranRegime #IranProtests #IranRevolution2022 #IranProrests2022

.. #LetUsTalk #WhiteWednesdays
.. #TwitterKurds #zhinaAmini #JinaMahsaAmini #MasahAmini #MasahAmini #Masah_Amini  #OpIran #مهساامینی
.. #EndIranRegime #LetUsTalk #TwitterKurds #مهسا_امینی #MahsaAmini #opiran
.. #IranRevolution2022 #IranProrests2022 #JinJiyanAzadi #SayHerName #Rojhilat #Iran
.. #HadisNajafi #NikaShakarami  #SarinaEsmaeilzadeh #FemmesVieLiberté #WomanLifeFreedom #JinJiyanAzadî #ZanZendegiÂzâdi
.. It’s time to call this freedom movement, the #IranRevolution.


- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|


.. figure:: images/photo_mahsa.png
   :align: center


Femme Vie Liberté Pourquoi mourir à 20 ans en IRAN ?
==========================================================

Cela fera **60 jours le 16 novembre 2022** que le peuple iranien est en lutte
contre la République islamique d'Iran.

L’étincelle qui a mis le feu aux poudres fut la mort violente et tragique
de :ref:`Mahsa Jina Amini <mahsa_jina_amini>` jeune fille de 22 ans arrêtée par la police des mœurs
à cause de la petite mèche de cheveux dépassant son voile imposé.

Oui cela fait presque 60 jours que le régime théocratique d’Iran se voit
impuissant malgré une répression féroce face à ce formidable mouvement
des femmes suite à cet assassinat.

Ce mouvement de contestation féministe contre les inégalités, contre le
code pénal, le code civil et la constitution qui considèrent la femme
iranienne comme un être inférieur, est devenu rapidement la révolte de la
jeunesse, des minorités ethniques et religieuses et une large part de la
population.

Depuis 60 jours le peuple iranien est dans les rues des grandes et des
petites villes à travers le pays pour refuser l’ordre établi, la dictature, et
l’essence même de la République Islamique ainsi que la légitimité du guide
suprême.

La manifestation légitime et pacifique du peuple pour mettre fin à la
situation catastrophique des millions d’iraniens vivant sous le seuil de
pauvreté, pour en finir avec la corruption, le détournement de la richesse
nationale, les ravages écologiques et environnementaux et tant d’autres
problèmes accumulés depuis plus de quatre décennies n’a eu de réponse
que la répression sanglante de la part du régime et ses forces de l’ordre.

Des milliers d’arrestations, des centaines de morts parmi lesquels une
trentaine d’enfants. Des jeunes filles et des jeunes gens de 16 à 20 ans
tombent tous les jours en criant mort à la dictature, mort à la République
Islamique, Femme, Vie, Liberté.

Aujourd’hui malgré la coupure de l’Internet et les moyens de communication,
malgré les massacres au Kurdistan, en Sistan et Baloutchistan la grève
et les manifestations s’étend à travers le pays affirmant leur solidarité
avec ces femmes et cette jeunesse héroïque qui préfèrent mourir pour la
liberté que de se soumettre à un régime dictatorial et ses lois d’un
autre temps.

Soirée d’information organisée par :

- LDH Grenoble Métropole,
- LDH Iran,
- Voix de l’Iran comité de Grenoble
- CISEM,
- Maison de l’International
- Solidarité socialiste avec les travailleurs en Iran

Mercredi 16 novembre 2022 à 20h, Maison du tourisme Grenoble


Affiche à télécharger
========================

:download:`Télécharger l'affiche au format PDF <affiche_soiree_d_information_mercredi_2022_11_16.pdf>`


IRAN mourir à 20 ans : FEMME, VIE , LIBERTÉ soirée d’information mercredi 16 novembre 2022 à 20h

Amphithéâtre de la maison du tourisme Grenoble.

Avec:

- LDH Grenoble Métropole,
- LDH Iran,
- Voix de l’Iran comité de Grenoble
- CISEM,
- Maison de l’International
- Solidarité socialiste avec les travailleurs en Iran




Annonce de la réunion sur ici-grenoble |ici_grenoble|
=============================================================

- https://www.ici-grenoble.org/evenement/reunion-publique-pour-soutenir-les-luttes-demancipation-en-iran

.. figure::  images/annonce_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/reunion-publique-pour-soutenir-les-luttes-demancipation-en-iran


Autres événements à grenoble
-------------------------------

.. figure:: images/autres_evenements_grenoble_2022_11_16.png
   :align: center

   https://www.ici-grenoble.org/agenda


Annonce sur Grenoble agenda
=============================================

- https://www.grenoble.fr/agenda/89341/38-iran-mourir-a-20-ans-femme-vie-liberte.htm


.. figure::  images/annonce_grenoble.png
   :align: center

   https://www.grenoble.fr/agenda/89341/38-iran-mourir-a-20-ans-femme-vie-liberte.htm

La Ville de Grenoble, en partenariat avec la Ligue des Droits de l’Homme (LDH)
Grenoble Métropole, la LDH Iran et Voix de l’Iran CISEM, organisent une
soirée d’information sur la révolte iranienne.

Cette conférence permettra de mieux saisir la situation tourmentée en Iran,
à partir de ces trois mots : Femme, Vie et Liberté.

La lutte du peuple contre la République islamique en Iran sera développée
lors des interventions.

Emmanuel Carroz déclare : « Grenoble, Ville Compagnon de la Libération,
par son histoire, sa culture et ses particularités, a été et restera un
bastion de la Résistance contre les totalitarismes.

Nous apportons tout notre soutien aux Iraniennes et Iraniens qui se
soulèvent avec courage contre la dictature islamique.

Les injonctions sur les corps des femmes sont insupportables, injustes,
insensées. Mahsa Amini en est le terrible et extrême symbole.

A l'heure où le régime tue et menace de tuer les prisonnières et prisonniers
arrêtés lors des manifestations, où la presse est muselée, où internet
est coupé pour empêcher l'organisation de la révolte, celle-ci continue
et il semblerait que rien n'arrête le peuple et la jeunesse iranienne de
se soulever contre l'injustice.

Du combat contre les règles imposées aux femmes, c'est désormais un
combat pour la vie, pour la liberté et pour la démocratie qui est mené.»


En présence de
----------------

- `Emmanuel Carroz <https://www.grenoble.fr/1823-emmanuel-carroz.htm>`_
   Adjoint à la Mémoire, aux Migrations, aux Coopérations internationales et à l'Europe

   .. figure:: images/emmanuel_carroz.png
      :align: center
      :width: 500

      https://www.grenoble.fr/1823-emmanuel-carroz.htm


- :ref:`behrouz_farahani`

   .. figure:: images/behrouz_farahani.png
      :align: center
      :width: 400

- S. Zari, représentante de Voix de l'Iran


Quelques événements du 13 novembre au 16 novembre 2022
=============================================================================

- :ref:`luttes_iran_geneve_2022_11_12`
- :ref:`soixantieme_jour_2022_11_15`
- :ref:`luttes_iran_2022_11_15_16_17`
- :ref:`mourir_a_20_ans_iran_2022_11_16`


Prochaine manifestation à Grenoble
=====================================

- :ref:`iran_grenoble_2022_11_19`


Quelques photos
==================

- https://www.instagram.com/iranluttes/

.. figure:: images/amphi_2022_11_16.jpeg
   :align: center


Vidéos
=======

- en cours




