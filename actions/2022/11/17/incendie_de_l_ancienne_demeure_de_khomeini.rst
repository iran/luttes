
.. _maison_khomeini_2022_11_17:

========================================================================
2022-11-17 **L’ancienne demeure de l’ayatollah Khomeiny est incendiée**
========================================================================


- https://nitter.manasiwibi.com/arminarefi/status/1593496908156342272#m


LOURD DE SENS. L’ancienne demeure de l’ayatollah Khomeiny, fondateur
de la République islamique, devenue aujourd’hui un musée, incendiée
ce jeudi 17 novembre 2022 par des manifestants à Khomein, ville d’origine
du religieux située dans le centre de l’#Iran. #MahsaAmini


Iran protesters set fire to Ayatollah Khomeini's ancestral home
==================================================================

- https://www.france24.com/en/middle-east/20221118-iran-protesters-set-fire-to-ayatollah-khomeini-s-ancestral-home


Protesters in Iran have set on fire the ancestral home of the Islamic
republic's founder Ayatollah Ruhollah Khomeini two months into the
anti-regime protest movement, images showed on Friday.

The house in the city of Khomein in the western Markazi province was
shown ablaze late Thursday with crowds of jubilant protesters marching
past, according to images posted on social media, verified by AFP.

Khomeini is said to have been born at the house in the town of Khomein --
from where his surname derives -- at the turn of the century.

He became a cleric deeply critical of the US-backed shah Mohammed Reza Pahlavi,
moved into exile but then returned in triumph from France in 1979 to
lead the Islamic revolution.

>> Mahsa Amini’s death is the ‘straw that broke the camel’s back’ for
Iran's defiant youth

Khomeini died in 1989 but remains the subject of adulation by the
clerical leadership under successor Ayatollah Ali Khamenei.

The house was later turned into a museum commemorating Khomeini. It was
not immediately clear what damaged it sustained.

The protests sparked by the death of Mahsa Amini, who had been arrested
by the morality police, pose the biggest challenge from the street to
Iran's leaders since the 1979 revolution.

>> Ayatollah Khamenei's health issues prompt fresh speculation on succession

They were fuelled by anger over the obligatory headscarf for women imposed
by Khomeini but have turned into a movement calling for an end to the
Islamic republic itself.

Images of Khomeini have on occasion been torched or defaced by protesters,
in taboo-breaking acts against a figure whose death is still marked each June with a holiday for mourning.



