.. index::
   ! Nasrin Sotoudeh


.. figure:: images/nasrin_sotoudeh.png
   :align: center


.. _nasrin_sotoudeh_2022_11_18:

=====================================================================================================
Vendredi 18 novembre 2022 **L’avocate iranienne Nasrin Sotoudeh lauréate du prix Robert-Badinter**
=====================================================================================================

- https://fr.wikipedia.org/wiki/Nasrin_Sotoudeh
- https://www.ecpm.org/lavocate-iranienne-nasrin-sotoudeh-laureate-du-prix-robert-badinter/
- https://www.iranhr.net/en/

Texte de l'article de la PNM (Presse Nouvelle Magazine)
===========================================================

.. figure:: images/article_pnm.png
   :align: center

   Article de la PNM (Presse Nouvelle Magazine)

**Cette justice d'élimination, cette justice d'angoisse et de mort, décidée
avec sa marge de hasard, nous la refusons**. Jamais ces paroles de Robert Badinter,
ancien garde des sceaux, n'auront mieux prouvé leur justesse que parmi
les 1500 délégués venus de 128 pays à Berlin du 15 au 18 novembre 2022
à l'occasion du 8e Congrès mondial contre la peine de mort.

Au vu de la situation que la population iranienne endure du fait de la
répression sanglante (au moins 416 morts dont 51 enfants) menée par le
régime de la République islamique depuis la mort de Mahsa Amini
le 16 septembre 2022, **les congressistes ont attribué pour a première fois
le Prix Robert-Badinter à Nasrin Sotoudeh, avocate iranienne et militante
des droits de l'homme**.

Christine Taubira, l'ancienne garde des Sceaux, a remis ce prix à **Mahmood
Amiry-Moghaddam**, directeur de l'ONG `Iran Human Rights <https://www.iranhr.net/en/>`_, l'avocate iranienne
n'ayant pu faire le déplacement.

**L'Iran détient le record du pays qui a le taux d'exécutions par habitant
le plus élevé**: durant les dix premiers mois de 2022, 448 personnes ont
été exécutées.
En 2021, 333 exécutions ont eu lieu.

Plus de 5000 personnes personnes sont actuellement détenues dans les
couloirs de la mort. Et plus de 15000 manifestants ont été arrêtés dont
6 ont été condamnés à mort.

L'Iran figure parmi les 5 pays qui exécutent le plus dans le monde
avec la Chine, l'Egypte, l'Arabie Saoudite et la Syrie.

Pour Raphaël Chenuil-Hazan, directeur général d'Ensemble contre la peine
de mort (ECPM), ONG organisatrice du congrès, le but (du congrès) est de
créer le moment politique pour porter la question de l'abolition de la
peine de mort auprès des acteurs politiques de pays qui ont toujours la
peine de mort.

Il s'est félicité des signaux venant du contient africain annonçant que,
d'ici quelques années, l'Afrique sra un continent quasiment ou entièrement
abolitionniste.

Si les congerssistes ont fêté à Berlin le 20e aniiversaire de la Coalition
mondiale contre la peine de mort, il reste à poursuivre le combat abolitionniste
contre cette **"pratique cruelle"**. Car malgré les énormes avancées
obtebues en 2022, 52 Etats restent à ce jour rétentionnistes et 30000
personnes se trouvent dans les coulors de la mort.

Alors que 108 Etats sont désormais abolitionnistes pour tous les crimes,
8 autres le sont pour les seuls crimes de droit commun et 29 pays
observent un moratoire sur les exécutions.



Article de l'ECPM (Ensemble contre la peine de mort)
======================================================

- https://www.ecpm.org/lavocate-iranienne-nasrin-sotoudeh-laureate-du-prix-robert-badinter/

Introduction
----------------

L’avocate iranienne, Nasrin Sotoudeh, militante de longue date des droits
humains dans son pays, est la toute première lauréate du Prix Robert-Badinter,
Grand prix du public, attribué aujourd’hui à Berlin, en conclusion du
8e Congrès mondial contre la peine de mort qui réunissait depuis le 15 novembre 2022
la communauté abolitionniste mondiale.

Dans une lettre transmise cette nuit l’avocate iranienne déclare :

Je demande au monde entier et à ce Congrès d’être les yeux et les oreilles
des Iraniens en ces jours difficiles. Des jours où des jeunes gens qui
ont utilisé leur droit légal de participer aux manifestations **Femme, Vie, Liberté**
sont injustement condamnés à mort.

En changeant le comportement du gouvernement à l’égard des manifestants
et en empêchant le système judiciaire d’exécuter les manifestants, on
contribuera à abolir la peine de mort dans l’Iran de demain.


Ce prix du public est la reconnaissance de la communauté abolitionniste
et plus largement de toute la communauté internationale pour le travail
et le courage exceptionnel de cette femme engagée pour la défense des
droits humains et l’abolition de la peine de mort en Iran », a déclaré
Raphaël Chenuil-Hazan, le directeur général d’Ensemble contre la peine
de mort (ECPM), organisatrice du Congrès.

**Plus que jamais nous devons être solidaire du peuple iranien et c’est
ce que ce Prix Robert-Badinter vient souligner**, a-t-il conclu.


Mobilisation exceptionnelle
---------------------------------

C’est sur cette note que s’achève le 8e Congrès mondial contre la peine
de mort avec une participation record, plus de 1500 congressistes, venus
de 128 pays, et une mobilisation politique sans précédent.

Trois pays africains, le Libéria, la Zambie et le Malawi ont profité de
l’occasion pour annoncer leurs intentions d’abolir la peine de mort dans
leur pays respectif au cours de la prochaine année.
« Il s’agit là d’engagements majeurs », se réjouit Aminata Niakaté, la
présidente d’ECPM, et qui confirme bien que l’Afrique pourrait bientôt
devenir le prochain continent abolitionniste.

« Cela prouve aussi la nécessité de rendez-vous comme ceux-là pour
atteindre notre objectif : l’abolition universelle de la peine de capitale »,
ajoute-t-elle.


.. _lettre_nasrin_sotoudeh_2022_11_18:

**La lettre de Nasrin Sotoudeh**
==================================

Honorables organisateurs et participants du Congrès mondial contre la
peine de mort

Avec tous mes respects,

J'ai été informée ces derniers jours que je faisais partie des quatre
candidats au Grand Prix du Public Robert-Badinter.

Quelle belle opportunité de faire entendre nos voix au monde entier et
surtout à votre prestigieux congrès à l'heure où les condamnations à mort
jettent une ombre sur notre jeunesse contestataire en Iran.

Je ne sais pas encore avec qui j'ai l'honneur d'être nominée mais sans
aucun doute, ils méritent tous plus ce prix que moi. Je vais d'ailleurs
leur demander, ainsi qu'à tous les participants au congrès, de m'aider.

**Moi, Nasrin Sotoudeh, avocate et prisonnière politique en Iran, je demande
au monde entier et à ce congrès d'être les yeux et les oreilles des
Iraniens en ces jours difficiles.**
Des jours où des jeunes gens qui ont utilisé leur droit légal de participer
aux manifestations "Femme, Vie, Liberté" sont injustement condamnés à mort.

En changeant le comportement du gouvernement à l'égard des manifestants
et en empêchant le système judiciaire d'exécuter les manifestants, on
contribuera à abolir la peine de mort dans l'Iran de demain.

Ces jours-ci, l'opinion publique iranienne est plus proche que jamais de
l'abolition de la peine de mort et s'aligne sur les objectifs de la
communauté mondiale des droits de l'homme, qui visent à instaurer une
vie fondée sur la paix, la justice et le droit.

Par conséquent, en tant qu'institution civile fiable, nous vous demandons
de nous aider à instaurer la paix, la justice et l'abolition de la peine de
mort ; que votre soutien et votre alliance pour faire entendre la voix
des Iraniens en faveur de la justice soit le début de la fin de la peine
de mort.

Avec mes meilleures salutations

Nasrin Sotoudeh
Iran, Téhéran
Novembre 2022


