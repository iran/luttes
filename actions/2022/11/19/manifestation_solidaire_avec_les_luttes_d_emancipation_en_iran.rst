
.. index::
   pair: Rassemblement ; Samedi 19 novembre 2022


.. figure:: images/clip_place_aux_herbes.jpeg
   :align: center


.. _iran_grenoble_2022_11_19:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ 9e rassemblement samedi 19 novembre 2022 à Grenoble **place aux Herbes** à 14h30 **Le temps est venu ... rejoignez le mouvement mondial de la révolution** #NovembreSanglant #BloodyNovember ⁦ #ps752justice #Grenoble 📣
==============================================================================================================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 19 novembre 2022 cela fait donc 2 mois 3 jours
=> 64e jour = on est dans la 10e semaine de la révolution, N°semaine:46 (2022)



- :ref:`mahsa_jina_amini`
- :ref:`slogans`
- https://www.ps752justice.com/
- https://www.instagram.com/ps752justice/
- https://linktr.ee/ps752justice
- https://fr.wikipedia.org/wiki/Manifestations_de_2019-2020_en_Iran
- https://blogs.mediapart.fr/sirinealkonost/blog/141122/voix-diran-de-leau-de-lessence-et-du-sang


⚖️ En commémoration du #NovembreSanglant 2019 en Iran
========================================================

Réclamer la justice ⚖️, fait partie du processus de la révolution en cours en Iran.

Nous n’oublions jamais les atrocités commisent par la république islamique en Iran.

**Il y a trois ans, des milliers d’Iraniens ont été tués dans la plus
grande indifference. Il nous appartient de faire la lumière sur ces atrocités**.

**L’Association des familles des victimes du vol PS752 et un certain nombre
de familles de victimes de #NovembreSanglant, organisent un rassemblement
mondial le samedi 19 novembre 2022, en commémoration des victimes du
massacre de novembre 2019 et en soutien à la révolution en Iran**.


Affiches
===========

Le massacre silencieux de novembre 2019
En commémoration des atrocités de la république islamique
Le temps est venu 19 novembre 2022
Rejoignez le mouvement mondial de la révolution


.. figure:: images/affiche_place_aux_herbes.jpeg
   :align: center
   :width: 500



.. figure:: images/affiche_place_aux_herbes_anglais.jpeg
   :align: center
   :width: 500


   https://bibliogram.priv.pw/p/ClFlqlsuK0z/?igshid=YmMyMTA2M2Y=
   On the third anniversary of  #BloodyNovember and in support of Iranian Revolution Grenoble 🇫🇷


.. figure:: images/some_places_around_the_world.jpeg
   :align: center

   https://www.instagram.com/ps752justice/


#NovembreSanglant #BloodyNovember ⁦ #ps752justice #Grenoble #KianPirfalak
#MahsaAmini #IranProtests #iran #grenoble #france #JinaAmini #ZanZendegiAzadi
#azadi #freedom #revolution #زن_زندگی_آزادی #women #femme #vie #liberte
#jinjiyanazadi  #revolution

- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|
- :ref:`#KianPirfalak <kian_pirfalak>` |KianPirfalak|

Texte annonçant le rassemblement
==================================

En solidarité avec les femmes en lutte pour l’égalité et contre la République islamique d’Iran

**Manifestation solidaire avec les luttes d’émancipation en Iran ce samedi 19 novembre 2022 – 14h30 Place aux herbes**

Arrêtée le 13 septembre 2022 et décédée le 16 septembre 2022 en garde à vue,
**la République islamique d’Iran a assassiné Mahsa(Jina) Amini, jeune femme
kurde en visite à Téhéran**.
Pour la police des mœurs, elle n’était pas assez voilée.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre. Pas un seul jour sans qu’elles n’affirment leur droit
à exister, avec ou sans voile, comme elles le veulent.

Dans un contexte de crise sociale majeure et de dictature, la grande
majorité de la population se solidarise tous les jours avec les femmes
et la jeunesse.

Partout dans le pays, jour et nuit, la jeunesse (hommes et femmes) entre
en insurrection, et fait reculer des hommes armés jusqu’aux dents.
Des forces de l’ordre, nombreuses et variées, en civil ou en uniforme,
unies dans une répression sanguinaire.
La violence du régime à l’égard des manifestants a fait déjà plus de 200
morts et des milliers de prisonniers ou disparus.

Face à la République Islamique, les insurgé.es occupent la rue, défient
le patriarcat, crient leur haine, réclament justice.

Les manifestants scandent : « Femme, Vie, Liberté ! », « C’est notre dernier
avertissement, c’est le régime que nous visons », « Malgré la répression
sanguinaire, la lutte continue ». « A Bas la République Islamique d'Iran ! »...

**Nous soutenons les aspirations légitimes des peuples d’Iran, des femmes
et de la jeunesse à la justice sociale et à la liberté**.

Nous condamnons fermement la répression envers les manifestant.e.s et
exigeons la libération immédiate de l’ensemble des manifestants détenus,
ainsi que les défenseurs des droits humains, syndicalistes, militants,
étudiants, avocats et journalistes...

Nous soutenons notamment :

- **Le droit essentiel des femmes à disposer de leurs corps**,
- **L’abrogation de la loi rendant obligatoire le port du voile ainsi que
  toutes les lois phallocratiques en vigueur**


**Cette manifestation est organisée par la communauté Iranienne**.



Quelques événements du 13 novembre au 19 novembre 2022
=============================================================================

- :ref:`luttes_iran_geneve_2022_11_12`
- :ref:`soixantieme_jour_2022_11_15`
- :ref:`luttes_iran_2022_11_15_16_17`
- :ref:`mourir_a_20_ans_iran_2022_11_16`

Annonces sur internet (réseaux sociaux et sites Web)
=======================================================

Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/evenement/manifestation-solidaire-avec-les-luttes-demancipation-en-iran-2


.. figure:: images/ici_grenoble_evt_2022_11_19_annonce.png
   :align: center

   https://www.ici-grenoble.org/evenement/manifestation-solidaire-avec-les-luttes-demancipation-en-iran-2


.. figure:: images/ici_grenoble_evt_2022_11_19_la_une.png
   :align: center


Autres événements ce samedi 19 novembre 2022 à Grenoble
---------------------------------------------------------

.. figure:: images/ici_grenoble_evt_2022_11_19.png
   :align: center


Autres rassemblements en soutien à la révolution iranienne samedi 19 novembre 2022
=================================================================================================


Annonce Genève en français #NovembreSanglant 🇫🇷
---------------------------------------------------

- https://ig.tokhmi.xyz/p/Ck_IWAbI3f9/?utm_source=ig_web_copy_link


#NovembreSanglant #BloodyNovember ⁦ #ps752justice #Grenoble
#mahsaamini #iranprotests #iran #geneve #switzerland #jinaamini #zanzendegiazadi
#azadi #freedom #revolution #زن_زندگی_آزادی #women #femme #vie #liberte
#jinjiyanazadi  #revolution


Réclamer la justice , fait partie du processus de la révolution en cours
en Iran.

Nous n’oublions jamais les atrocités commisent par la république islamique
en Iran

. Il y a trois ans, des milliers d'Iraniens ont été tués dans la plus
grande indifference. Il nous appartient de faire la lumière sur ces atrocités .

L'Association des familles des victimes du vol PS752 et un certain nombre
de familles de victimes de #NovembreSanglant, organisent un rassemblement
mondial le samedi 19 novembre 2022, en commémoration des victimes du massacre
de novembre 2019 et en soutien à la révolution en Iran.

A " GENEVE", nous nous joignons également à ce mouvement.


Annonce Genève en anglais  #BloodyNovember  🇬🇧
---------------------------------------------------

The pursuit of justice is a part of ongoing revolution in Iran.
We shall remember the atrocities of Islamic Republic. Three years ago,
thousands of Iranians were killed in silence. It is upon us to shed light
on the darkness.

The Association of Families of Flight PS752 Victims and a number of
families of ⁦ #BloodyNovember⁩ victims, are organizing a worldwide rally
on Saturday Nov. 19, to commemorate the victims of November 2019 Massacre
and in support of the revolution in Iran.

In GENEVA we will also join this movement.


.. figure:: images/affiche_geneve.jpeg
   :align: center

   http://www.solidarite-iran.ch/


.. figure:: images/some_places_around_the_world_geneve.jpeg
   :align: center

   https://www.instagram.com/ps752justice/


Photos sur https://www.instagram.com/iranluttes/
====================================================

- https://www.instagram.com/iranluttes/

- 📣 Find Armita Abbasi
- ⚖️ Free #AfrinAzarfar
- ⚖️ Free #AliYounesi
- ⚖️ Free #ArtefehMahalian
- ⚖️ Free #BabakPaknia
- ⚖️ :ref:Free #NiloufarHamedi <niloofar_hamedi>`
- 😥 Say his name #AbolfaziAdinezadeh 16 years old
- 😥 Say his name #AhmadShokrolahi 24 years old
- 😥 Say his name #AliMozaffari 17 years old
- 😥 Say his name #ArianMoridi 18 years old
- 😥 Say his name #BehnamLayeghpour 25 years old
- 😥 Say his name #FarjadDarvishi 23 years old
- 😥 Say his name :ref:`#KianPirfalak <kian_pirfalak>` 10 years old 😥 👦
- 😥 Say his name #MehrshadShahidi 19 years old
- 😥 Say his name #Navidfkari 27 years old
- 😥 Say his name #PouyaBakhtiari was shot on 16 november 2019 #NovembreSanglant #BloodyNovember
- 😥 Say his name #ShahooKhezri ?? years old
- 😥 Say his name #SinaMalayeri 34 years old
- 😥 Say his name #YahyaRahimi ?? years old
- ⚖️ Stop execution of :ref:`#SamanSeydi <saman_seydi>` a popular singer.
  His stage name is Saman Yasin. His crime was singing a political song.


♀️✊ ⚖️ 8e rassemblement samedi 19 novembre 2022 à Grenoble **place aux Herbes** à 14h30
⚖️ En commémoration du #NovembreSanglant 2019 en Iran
#NovembreSanglant #BloodyNovember ⁦ #ps752justice #Grenoble #KianPirfalak
#MahsaAmini #IranProtests #iran #grenoble #france #JinaAmini #ZanZendegiAzadi
#azadi #freedom #revolution #زن_زندگی_آزادی #women #femme #vie #liberte
#jinjiyanazadi  #revolution


Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


