
.. index::
   ! Khodanoor Lajei

.. _textes_semaine_9_2022_11_18:

====================================================================================================
Samedi 19 novembre 2022 textes de la 9e semaine du soulèvement depuis la mort de Mahsa Jina Amini
====================================================================================================

9e semaine du soulèvement, 64e jour (2022-11-19)


Aujourd'hui, nous commémorons les massacres d'Aban en Iran.

Il y a trois ans, en novembre 2019, le gouvernement iranien a triplé le prix
de l'essence en secret vers minuit. Ce qui a donné lieu à une révolte
importante  contre la vie chère.

Le gouvernement a coupé internet, a mis des tireurs d'élite sur les
toits, et a tiré sur les manifestants avec des helicoptères.
En quelques jours, environ 1500 personnes ont été tués.

De lourdes peines de prison et des exécutions ont suivi. 

Mais ce mouvement réprimé dans le sang, a été repris depuis le 19 septembre 2022,
le jour de la mort de Mahsa Amini, après avoir été arrêtée et tuée  par
la police des mœurs.
Cette révolte  sans précédent avec la participation  des femmes en
première ligne est suivie par des  hommes et tout le peuple iranien
avec des grèves.
Le peuple iranien scande sa détermination pour en finir avec le gouvernement
islamique d'Iran malgré les massacres et es arrestations massives et
des condamnations à mort.

Depuis le 14 Novembre 2022, l'anniversaire du mouvement de 2019 le pays
est en feu.

Le peuple d'Iran a besoin de votre solidarité  Soyez sa voix.


Qui est Khodanoor Lajei ?
===========================

Il appartenait à la minorite sunnite de Baloutchistan. Il a été arrêté
avant la manifestation. La police de la République Islamique de l'Iran
le menotte à  un mât pour l'humilier.
Et à sa demande de boire, on lui met un verre d'eau par terre en lui
répondant "tu peux le regarder".

Il a été tué à l'âge de 27 ans à Zahedan par une balle lors des
protestations de 2022.

.. figure:: images/khodanoor_lajei.png
   :align: center


Résumé
======================

Il y a une révolution en cours en Iran

Il y a 2 mois de ça,  la République islamique d’Iran a arrêté puis
assassiné Mahsa-Jina Amini, jeune femme kurde de 22 ans.

Pour la police des mœurs, elle n’était pas assez voilée.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

La population se solidarise tous les jours avec les femmes.

Jour et nuit, la jeunesse en insurrection, fait reculer les forces de
l'ordre armées jusqu'aux dents.

Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir de ce régime sanguinaire.

La République islamique ne fait plus peur à ses jeunes que malgré les
massacres  et  les empoisonnements et des condamnations  continuent à leurs combats.

Pour que le monde ne soit pas au courant de ce qui se passe en Iran, le
régime perturbe internet au plussible.

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.


Répression
===================

L'insurrection en cours en  Iran depuis la mort de Mahsa Amini pour la non
conformité de son hijab islamique, est réprimée dans le sang.

Plus de 342 personnes sont mortes dont 42 enfants en dessous  de 18 ans.

15000 personnes sont arrêtés et le régime commence les condamnations à la mort.

Niloufar Hamedi et Ekaheh Mohamadi  deux journalistes qui ont rapporté
ce qui s'est passe avec Mahsa Amini sont en prison.

Le rapeur, Saman Yacin, pour ses chansons  et Parham Parvari, Champion
de nage, pour avoir participé à la manifestation dans les rues sont
condamnés a la peine maximum qui est la peine de mort.

Toumaj salehi, le rapeur renommé pour ses chansons anti-regime a été
arrêtée et lourdement torturé. Hossein Ronaghi activiste du droit de
l’homme est a nouveau en prison et sous torture.

Ils lui ont cassé les deux jambes en prison et il ne reçoit pas les
soins nécessaires.

Il a entamé une grève de la faim et ses jours sont en danger.

Le 15 octobre 2022
===================

Le régime a mis le feu à l'atroce prison des prisonniers politiques
espérant en profiter pour massacrer les prisonniers.
La réaction de la population  a été tellement vive qu'ils n'ont pas pu
atteindre leurs objectifs. Meme si il y a eu 8 morts officiellement annoncés.

A zahedan au sud-ouest du pays  la machine de la répression de la République
islamique a tué plus de 90 morts en un seul jour lors d'une manifestation pacifique.

Cette semaine, un enfant de 10 ans qui rentrait chez lui avec son père a
été tué par des balles qui ont traversee sa voiture, quand la police
tirait sur les manifestants.

Les horreurs de ce régime meurtrier n'ont pas de limite.

Ils kidnappent même les corps sans vies des assassinés et ne les rendent
pas aux familles.
Puis ils mettent la pression  sur les familles pour qu'elles disent que
leur enfant est mort dans d'autres circonstances en échange du corps de
leur enfants.

Le peuple iranien veut finir avec ce régime. Soyons leur voix.


