.. index::
   pair: Film ; Hitch
   ! Hitch

.. figure:: images/kaminsky.png
   :align: center


.. _hitch_2022_11_22:

================================================================================================================
Mardi 22 novembre 2022 [AHWAHNEE] Projection du film **Hitch : une histoire iranienne** de Chowra Makaremi
================================================================================================================

- :ref:`chowra_makaremi`
- https://cric-grenoble.info/infos-locales/article/ahwahnee-projection-du-film-hitch-une-histoire-iranienne-de-chowra-makaremi-et-2672


.. figure:: images/kaminsky.png
   :align: center

   https://fr.wikipedia.org/wiki/Maison_Kaminski, squatt AHWAHNEE

Introduction
==================

On rebondit une fois de plus sur l’actualité iranienne avec ce film de
la réalisatrice Chowra Makaremi, anthropologue au CNRS.

Entre autres choses, cette chercheuse étudie les violences post-révolution
en Iran avec notamment ce documentaire sortit en 2019.

Synopsis 
==========

« Iran, 1988. Des milliers de prisonniers politiques, enfermés depuis
les lendemains de la révolution de 1979, sont massacrés.

Ces derniers des révolutionnaires opposés à Khomeini sont liquidés dans
le plus grand secret. Parmi elles et eux, Fatemeh Zarei, ma mère.
Tandis que l’État iranien nie toujours ses crimes et s’efforce encore
aujourd’hui d’en effacer les traces, le film part en quête des lieux,
des objets et des gestes qui permettront de dénouer le silence, là où
seul l’intime reste en témoignage d’une politique. »

Un repas végane sera préparé et partagé avant le film !

Ouverture des portes 19h30. Ouvert à tous.tes à prix libre.

+ Permanence du free shop : dépose, récupère, essaye tout ce qui te plais

Ahwahnee.

Ⓐhwahnee est un squat de vie et d’activité situé au 106 rue des Alliés.


.. _hitch_chowra:

Article de Nicolas Cury
=============================

- https://blogs.mediapart.fr/les-ecrans-documentaires/blog/081119/hitch-une-histoire-iranienne-entretien-avec-chowra-makaremi


On sait que dix ans auparavant vous étiez allée en Iran, une caméra à
la main, est-ce que l'idée du documentaire était déjà existante ?
Pourquoi être passée par ce médium-là ? 

Pas dans cette forme-là. J'ai travaillé sur le documentaire à partir
de 2015. A l'époque je voulais faire un film, mais c'était plutôt un
film de famille.
Pourquoi est-ce que je suis passée par l'image ? Parce que je voulais
capturer la maison. Pour moi c'était important d'en avoir des images. 

Pourquoi un film ? Est-ce qu'il ouvre un rapport particulier à la mémoire ?
------------------------------------------------------------------------------

On est une génération qui a grandi en exil. Nous on peut retourner en
Iran parce qu'on n’a rien fait qui nous identifie comme des ennemis,
mais une fois qu'on retourne là-bas on se retrouve dans une société
où le passé familial qu'on connaît n'existe pas dans l'espace social.

Donc comment est-ce qu'on retourne dans cet espace qui n'est pas tel
qu'on le connaît ? Je pense que prendre la caméra, c'est une façon de
dire : “il y a quelque chose qui s'est passé”.
C'est vraiment la question du déni : comment attaquer la surface lisse ?
La deuxième raison, c'est peut-être que le film est ce qui arrive le
mieux à capturer le fonctionnement de la mémoire.

Parce que ce sont des images, parce qu'il n'y a pas forcément de logique
narrative, parce que ce sont des sensations.

Est-ce que vous avez dû vous confronter à beaucoup de résistance une
fois sur le territoire ? 

Ça ne se joue pas au niveau du rapport à l'Etat où on va de bureau
en bureau. Ce sont des démarches hyper dangereuses. Et comme les gens
sont terrorisés par ce qui s'est passé, il n'y a pas de recherche.
Donc à l'époque, personne ne m'a mis de bâtons dans les roues parce
que je me suis bien gardée de faire des démarches publiques.

D'ailleurs, il n'y a aucune investigation publique faite par des familles
de personnes ayant été exécutées dans les années 80, simplement parce
que c'est trop dangereux. 

Vous avez eu peur ? 
----------------------

Oui. En fait je n'avais pas peur quand je le faisais et une fois que
je me suis rendue  compte que j'avais réussi à donner une forme à tout
ça, là j'ai eu peur.
Mais je pense que ce n'était pas la peur de faire quelque chose, c'est
toute la mémoire de la peur de tout ce qui a été vécu qui est remontée
à la surface : une espèce de prise de conscience qu'il y a un certain
enjeu, que ce n'est pas sans risque que de rendre ces expériences de
violences transmissibles, de les amener dans l'espace public.

Dans le documentaire, les supports d'enquête se multiplient, comme des
preuves tangibles. Le papier, la cassette audio, le cahier, la photographie.

Est-ce que le film ne finirait pas un peu par jouer ce rôle qu'est
devenu celui de l'armoire-musée de votre père: un grand contenant
de tout ce qu’invoque l'image de votre mère à l'épreuve du temps ?  

C'est très compliqué. Je me rappelle que quand j'ai présenté mon projet,
une amie m'a dit: "C'est très bien mais il faut que tu fasses attention
à ce que ton film ne devienne pas un autre objet qu'on va aller
mettre dans un musée dont on referme la porte."

C'est le risque, entretenir une mémoire qui fonctionne en vase clos.

Quand j'ai écrit le film, j’ai fait tout mon possible pour justement
éviter ça, et faire en sorte que le film soit au bout du compte un
peu ouvert sur le monde, qu’il soit projeté, qu'il ne se referme pas. 

Propos recueillis par Nicolas Cury

