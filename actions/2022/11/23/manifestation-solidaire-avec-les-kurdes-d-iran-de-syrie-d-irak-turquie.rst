
.. _soutien_kurdistan_2022_11_23:

=====================================================================================================================================
🌄 Mercredi 23 novembre 2022 Rassemblement appelé par L'AIAK en solidarité avec les **kurdes d'Iran de Syrie d'Irak et de Turquie**
=====================================================================================================================================

::

    🌄 Mercredi 23 novembre 2022 Rassemblement appelé par L'AIAK en solidarité avec les kurdes d'Iran de Syrie d'Irak et de Turquie


Rassemblement appelé par L'AIAK (Association iséroise des amis des kurdes)
mercredi 23 novembre 2022 à 18h à Félix Poulat en solidarité avec les
Kurdes d'Iran, de Syrie, d'Irak et de Turquie.

Cela fait suite à la répression féroce par la République islamique d'Iran,
aux  bombardements des camps des oppositions en Irak et des bombardements
turcs sur les régions kurdes en Syrie.


.. figure:: images/aiak.png
   :align: center

   Rassemblement dans le froid sous une pluie battante
