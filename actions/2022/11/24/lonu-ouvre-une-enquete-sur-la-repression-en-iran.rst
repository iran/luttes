.. index::
   pair: ONU ; enquête sur la répression en Iran


.. _onu_iran_2022_11_24:

================================================================================
Jeudi 24 novembre 2022 **L'ONU ouvre une enquête sur la répression en Iran**
================================================================================


Article du Courrier Picard
=============================

- https://www.courrier-picard.fr/id363684/article/2022-11-24/lonu-ouvre-une-enquete-sur-la-repression-en-iran

Le Conseil des droits de l'homme de l'ONU a décidé jeudi, malgré
**l'opposition de Téhéran et de Pékin**, d'ouvrir une enquête internationale
sur la répression des manifestations en Iran après la mort de Mahsa Amini,
pour rassembler des preuves des violations et éventuellement poursuivre
les responsables.

Et c'est en plein débat devant le Conseil, que l'agence de presse
iranienne Fars a annoncé l'arrestation du célèbre footballeur
Voria Ghafouri, accusé d'avoir "insulté et sali la réputation de
l'équipe nationale (Team Melli) et de s'être livré à de la propagande"
contre l'Etat.

Réunis d'urgence à l'initiative de l'Allemagne et de l'Islande, les 47
Etats membres de l'instance onusienne la plus élevée en matière de
droits humains ont décidé de nommer une équipe d'enquêteurs de haut
niveau pour faire la lumière sur toutes les violations de ces droits
liées à la répression des manifestations.

Le texte a été voté à une majorité légèrement plus large qu'attendue,
malgré une manoeuvre dilatoire de dernière minute de Pékin : 25 pays
ont voté oui, six non (Arménie, Chine, Cuba, Erythrée, Pakistan et
Venezuela) et 16 se sont abstenus.

"Les responsables iraniens ne pourront pas perpétrer cette violente
répression dans l'anonymat; la communauté internationale les regarde",
a réagi l'ambassadrice américaine Michèle Taylor.

"Le courage et la détermination des manifestants nous obligent", a
tweeté la représentation française auprès de l'ONU, saluant également
la création de ce mécanisme d'enquête.

L'ONG Amnesty International s'est félicitée d'"une résolution historique"
qui marque "une étape importante vers la fin de l'impunité !", tandis
que Lucy McKernan, de Human Rights Watch, qualifiait la décision du
Conseil d'"étape bienvenue".

"Changement inévitable"
=========================

Pendant les débats, le Haut-Commissaire de l'ONU aux droits de l'homme,
Volker Türk, dont la demande de visite en Iran est restée jusqu'à présent
lettre morte, a appelé Téhéran à "cesser" son "usage inutile et
disproportionné de la force".

"La situation actuelle est intenable", a prévenu M. Türk, qui réclame
"un moratoire sur la peine de mort" et demande que le gouvernement
"s'engage dans un processus de réformes car le changement est inévitable".

Pendant la session qui a duré toute la journée, de nombreux diplomates
occidentaux -et parmi eux la ministre allemande des Affaires étrangères
et son homologue islandaise- ont dénoncé la répression des manifestations
qui, en plus de deux mois, a fait au moins 416 morts, dont 51 enfants,
selon l'ONG Iran Human Rights (IHR), dont le siège est en Norvège.

Cette vague de contestation -née de revendications sur les droits des
femmes après la mort de Mahsa Amini, arrêtée pour avoir mal porté le
voile islamique, qui se sont mues en contestation du pouvoir- est sans
précédent depuis la Révolution islamique de 1979.

Selon le Rapporteur spécial de l'ONU sur l'Iran, plus de 15.000 personnes
ont été arrêtées.

La justice iranienne a déjà prononcé six condamnations à mort en lien
avec les manifestations et a annoncé cette semaine l'arrestation en
deux mois de "40 étrangers" accusés d'implication dans les "émeutes" en Iran.


Justice pour le peuple
========================

S'exprimant devant les journalistes à Genève avant le vote, la cheffe
de la diplomatie allemande, Annalena Baerbock, avait encouragé les pays
qui en général votent avec l'Iran à avoir le courage de s'abstenir.

"Nous ne savons pas si (cette résolution) peut sauver des vies demain.
Mais ce que nous savons avec certitude, c'est que cela signifiera la
justice, justice pour le peuple", avait-elle déclaré.

Cette mission d'enquête internationale indépendante -qui n'a guère de
chance de pouvoir se rendre en Iran en raison de l'opposition de Téhéran-
devra collecter les preuves des violations et les conserver de manière
à ce qu'elles puissent servir à d'éventuelles poursuites.

Téhéran voit dans la plupart de ces manifestations des "émeutes" et accuse
notamment des forces étrangères d'être derrière ce mouvement pour chercher
à déstabiliser la République islamique.

"Le niveau d'extrême violence" des "émeutiers contre les citoyens et
les forces de l'ordre est au-delà de toute description", a encore assuré
l'ambassadrice iranienne en Finlande, Foroozandeh Vadiati, qui a
participé à la session à Genève.

D'autres comme le Pakistan et le Venezuela ont dénoncé ce qu'ils voient
comme une politisation croissante du Conseil des droits de l'homme,
devenu à leurs yeux un outil aux mains des démocraties occidentales
pour imposer leurs vues et s'ingérer dans les affaires intérieures des pays.



