
.. _femme_vie_liberte_2022_11_25:

=====================================================================================================================================================
♀️ Vendredi 25 novembre 2022 Femme Vie Liberte **Journée internationale pour l'élimination de la violence à l'égard des femmes**
=====================================================================================================================================================


- https://www.ici-grenoble.org/article/la-peur-doit-changer-de-camp
- https://fr.wikipedia.org/wiki/Journ%C3%A9e_internationale_pour_l%27%C3%A9limination_de_la_violence_%C3%A0_l%27%C3%A9gard_des_femmes
- https://www.un.org/fr/observances/ending-violence-against-women


.. figure:: images/femme_vie_liberte.png
   :align: center
   :width: 400


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)



🟠 Thème 2022 : TOUS UNiS ! L'activisme pour l'élimination de la violence à l'égard des femmes et des filles 
==========================================================================================================================

- https://www.un.org/fr/observances/ending-violence-against-women

Il y a cinq ans, le mouvement #MeToo, fondé par l’activiste Tarana Burke
en 2006, a pris de l’ampleur et déclenché une mobilisation mondiale
créant un sentiment d’urgence en matière de prévention et d’élimination
de la violence à l’égard des femmes et des filles.

Depuis lors, une prise de conscience et un élan sans précédent ont vu le
jour grâce au travail acharné des activistes de base, des défenseuses
et défenseurs des droits humains des femmes et des défenseuses et défenseurs
des droits des survivantes dans le monde entier pour prévenir et éliminer
la violence à l’égard des femmes et des filles.

Parallèlement, il y a eu une recrudescence des mouvements anti-droits,
y compris des groupes antiféministes, ce qui a entraîné un rétrécissement
de l’espace de la société civile, une réaction hostile envers les
organisations de défense des droits des femmes et une augmentation
des attaques contre les défenseuses des droits humains, les défenseurs
des droits des femmes et les activistes des droits humains.

Soutenir et financer des organisations de défense des droits des femmes
et des mouvements féministes solides et autonomes est crucial pour
mettre fin à la violence à l’égard des femmes et des filles.

C’est pourquoi le thème de la campagne Tous UNiS pour 2022 appellera à
un soutien renforcé envers l’activisme pour prévenir la violence à
l’égard des femmes et des filles : « Tous UNiS ! L’activisme pour
l’élimination de la violence à l’égard des femmes et des filles ! »


.. figure:: images/onu_observatoire.png
   :align: center

   🟠 Lors de la Journée internationale pour l'élimination de la violence
   à l'égard des femmes, la couleur orange 🟠 est utilisée pour représenter
   un avenir meilleur, exempt de violence à l'égard des femmes et des filles.


Comme lors les années précédentes, la Journée internationale pour
l'élimination de la violence à l'égard des femmes donne le coup d'envoi
de 16 jours d'activisme qui se termineront le 10 décembre, jour de la
commémoration de la Journée internationale des droits de l'homme.

Plusieurs événements publics sont en cours de coordination et des bâtiments
et points de repère emblématiques seront « orangés » à travers le monde 
pour représenter un avenir meilleur, exempt de violence à l'égard des
femmes et des filles.


Participez au 16 Jours d’activisme
=======================================

- https://media.un.org/en/webtv

Les 16 Jours d’activisme de lutte contre la violence basée sur le genre
à l’égard des femmes et des filles s’inscrivent dans le cadre d’une
campagne internationale qui a lieu tous les ans.

Elle débute le 25 novembre, à l’occasion de la Journée internationale
pour l’élimination de la violence à l’égard des femmes, et se termine
le 10 décembre, à l’occasion de la Journée des droits de l'homme. 

Cette campagne, menée par le Secrétaire général des Nations Unies et
ONU Femmes depuis 2008, vise à prévenir et à éliminer la violence
à l'égard des femmes et des filles dans le monde, en appelant à une
action mondiale pour accroître la sensibilisation et le soutien à
cette question; et créer des opportunités de discussion sur les
défis et les solutions.

À cette occasion, l'ONU organise un événement le 23 novembre (10h00-11h30 heure de New York.)

Vous pouvez suivre l'événement en ligne sur la chaîne Youtube d'ONU Femmes
ou sur la `WebTV de l'ONU <https://media.un.org/en/webtv>`_



Article d'Ici-grenoble
=========================

- https://www.ici-grenoble.org/article/la-peur-doit-changer-de-camp


.. figure:: images/15_feminicides_noel.png
   :align: center

   https://www.ici-grenoble.org/article/la-peur-doit-changer-de-camp


Un moment fort pour se donner de la force, crier notre colère, faire
entendre la voix des victimes : ce vendredi 25 novembre à 18h place Notre-Dame,
l'Assemblée Généraliste féministe de Grenoble organise une grande Marche
aux flambeaux contre les violences sexistes et sexuelles.

Le cortège de tête sera en mixité choisie sans hommes cis.

Soyons nombreuses et nombreux !


