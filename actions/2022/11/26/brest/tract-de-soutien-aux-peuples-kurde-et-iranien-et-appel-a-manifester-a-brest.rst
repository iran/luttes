.. index::
   pair: Tract; Brest


.. _soutien_brest_iran_2022_11_26:

==============================================================================================================
Samedi 26 novembre 2022 **Tract de soutien au peuples kurde et iranien et appel à manifester  à Brest**
==============================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022.
ce samedi 26 novembre 2022 cela fait donc 2 mois 1 semaine 3 jours
=> 71e jour = on est dans la 11e semaine de la révolution.


Partie 1 Soutien aux peuples kurde et iranien !
====================================================

Source: https://eldritch.cafe/@ucl_finistere/109444977019100786


Le président turc Erdogan frappe le Rojava, territoire Kurde au nord de
la Syrie.

Le 19 novembre 2022, la Turquie a lancé une série de bombardements meurtriers
principalement contre des infrastructures civiles vitales pour la population
du Rojava, région à majorité kurde du nord de la Syrie sous contrôle des
Forces Démocratique Syrienne (FDS), il y a encore peu temps en première
ligne dans la lutte contre l’état islamiste.

Des milliers de combattantes et de combattants des FDS sont tombés en
martyrs dans cette lutte, ne l’oublions pas.

Dans sa propagande télévisuelle, la Turquie s’est vantée de la précision
des frappes menées par son aviation qui utilise une technologie de haute
précision élaborée dans une **grande entreprise brestoise Thalès**.

La Turquie justifie ses attaques en représailles à l’attentat qu’elle
attribue au PKK / PYD (FDS) qui a eu lieu le dimanche 13 novembre 2022
sur l’avenue Istlikal, ce que ces organisations ont démenties.

En réalité, cet attentat s’inscrit dans une longue tradition quelque soit
la couleur politique des dirigeants au pouvoir en Turquie ; disparitions,
crimes « à auteurs inconnus », coups tordus, attentats aveugles, comme
ce dernier en date, sont les signes d’un régime aux abois, plombé par
des résultats économiques catastrophiques.

C’est signé.
Erdogan a besoin de réveiller la flamme nationaliste pour gagner les
élections du 18 juin 2023.
A ce jour il n’obtient que 36,3 % d’opinion favorable alors que lors des
dernières scrutins c’était plutôt 50%.

Punir Kobané, envahir le Rojava et tout le nord de la Syrie, pourrait lui
permettre de rebondir. Il peut compter, pour cela, sur son fidèle
Hakan Fidan, qui dirige ses services secrets depuis 2010, pour fomenter
un de ses coups dont il a le secret et pour lesquels il dispose d’hommes
de main dont les loups gris du MHP font partie.

Les services secrets d’Erdoğan sont très présents en Europe, ils montrent,
s’il en était besoin, qu’ils sont aussi très actifs en Turquie même, au
cœur de toutes les opérations souterraines, comme l’attentat d’Istanbul,
qui permettent au régime néo-fasciste d’Erdoğan de perdurer.

Nous exigeons donc, l’arrêt des bombardements, le retrait des troupes
turques de Syrie et le retour de l’état de droit et de la démocratie en
Turquie, la libération de tous les prisonniers politiques et la réouverture
des pourparlers de paix.


Le peuple iranien lève la voix : Femme,  vie, Liberté !
===========================================================

Source: https://eldritch.cafe/@ucl_finistere/109444979225080316

Depuis le meurtre de Mahsa Jina Amini par la police des mœurs il y a
maintenant 9 semaines (16 septembre 2022), une crise révolutionnaire
politique, religieuse, économique et sociale fait rage en Iran.

Cette contestation du pouvoir est la conséquence d’une religion imposée,
d’une inégalité entre les sexes, de la corruption qui règne à tous les
niveaux de l’État, et d'une économie déclinante, due à une élite qui
s’enrichit sur le dos des pauvres (plus de 60 % de la population vit
dans la pauvreté en Iran).

**Cette révolution, ce sont les femmes qui l’ont commencée.**

Souffrant le plus des discriminations, des inégalités, du manque de droits
fondamentaux, de la pauvreté (certaines n’ont même pas de quoi nourrir
leurs enfants), elles se battent aujourd’hui avec colère et passion
pour renverser ce gouvernement qui leur interdit de vivre.

Très vite, les hommes les ont soutenues, notamment les ouvriers.
En effet, plusieurs usines font grève dans le pays pour contester ce
régime dictatorial qui tue, torture, viole, sans foi ni loi.

Beaucoup de commerces sont fermés, par peur des violences policières,
mais aussi en signe de contestation. C’est le cas du Grand Bazar de Téhéran,
qui est le cœur économique du pays, et d’autres grands bazars d’Iran.

Pendant la grève générale des 16, 17 et 18 novembre 2022, aucun marchand
n’est venu vendre ses articles afin de porter un coup économique au
gouvernement. Cette grève continue aujourd’hui encore.

Cette révolution politique et sociale montre au monde entier la vraie
nature dictatoriale du gouvernement iranien. En plus des répressions
sanguinaires qui sévissent dans les centres de détention, comme dans les
rues, les entreprises et universités nous apportent aussi la preuve de
l’intolérance et des répressions du gouvernement.

En effet, des femmes se sont présentées sans voile dans les banques, et
autres administrations. Les salariés qui se sont occupés d’elles ont
immédiatement été renvoyés.
Il en est de même pour les professeurs d’université qui ont fait cours
à des jeunes femmes qui ne portaient pas le voile.

Jusqu’à aujourd’hui, le gouvernement ne lâche absolument rien et durcit
sa position.

Autre exemple des répressions sanguinaires, les manifestants arrêtés sont
menacés d’exécution de masse. 227 sur 290 parlementaires iraniens ont
voté pour l’exécution de 14 802 manifestants arrêtés.

Il est impossible de savoir le nombre exact de personnes menacées d’exécution;
jusqu’à présent, le gouvernement a prouvé qu’il était capable de tout.

Cette demande a été envoyée à la cour de Justice iranienne pour être
étudiée pendant que des simulacres de procès ont lieu. Il s’agit de
procès extrêmement durs, avec une fausse justice, dans lesquels les
accusés n’ont même pas le droit de parler pour se défendre.
Dans la plupart des cas, ils sont forcés, par la torture, à passer aux aveux.

Mais les bras du gouvernement iranien ne se limitent pas aux frontières
de l’Iran. En effet, l’Iran aurait envoyé des soldats en Russie pour
participer à l’invasion de l’Ukraine.

Il aurait également envoyé des Gardiens de la Révolution Islamique (police
politique iranienne) en Syrie en soutien à Bachar El  Assad.
Il y a environ deux semaines, le gouvernement a également transféré 150
militaires, stationnés en Irak, à Mashhad, une ville au nordest de l’Iran.

On assiste à une militarisation de la répression avec notamment
l’intervention des Gardiens de la Révolution Islamique, l’utilisation
d’armes de guerre, de gaz...
Leur seul but est d’écraser la révolution. La situation est aggravée par
l’importation illégale d’armes et de munitions européennes en Iran,
venant d’usines franco-italiennes par exemple.

En outre, les événements, les injustices et les  répressions en Iran sont
durs à prouver à cause de  l’absence de journalistes.
Les journalistes iraniens se font arrêter, torturer, violer et parfois
exécuter; et le gouvernement iranien n’autorise pas de journalistes
étrangers sur son sol (même si certains arrivent à s’aventurer parfois
aux frontières).
Ainsi, les seules informations disponibles proviennent des réseaux
sociaux et des  témoignages des familles et amis restés au pays.


Partie 3 FACE AUX RÉPRESSIONS, SOLIDARITÉ ENTRE LES PEUPLES !
=================================================================

La répression sanglante que subissent les peuples iranien et kurde
aujourd’hui est révoltante.

L’instrumentalisation de l’attenta d’Istanbul pour justifier des représailles
militaires au Kurdistan syrien et les tentatives d’écrasement ultra violent
de l’appareil d’état iranien ont quelque part les mêmes motivations : faire
taire la contestation politique en assassinant tous ceux qui portent le
projet d’une société plus juste, libérée des oppressions contre les minorités
ethniques, les femmes, les pauvres et les travailleurs.

Le pouvoir Turc veut détruire le projet politique émancipateur expérimenté
au Rojava (Kurdistan du Nord de la Syrie), et le régime des Mollahs en Iran
veut écraser le mouvement qui depuis 9 semaines conteste son autorité
dans la rue et par les grèves.

Nous dénonçons la position du gouvernement français, qui ne se gène pas
pour discuter avec des dictateurs sanglants pour plus tard montrer un
soutien hypocrite aux militantes iraniennes dans une mise en scène
millimétrée devant les caméras.

Nous soulignons également la complicité passive dont se rend coupable
l’État français vis à vis des dernières attaques de Erdogan contre
le Rojava, ne voulant pas froisser le président Turc du fait du rôle
d’interlocuteur central qu'il s'est donné dans le conflit RussoUkrainien.

Les oppressions n’ont pas de frontières, alors mobilisons nous en soutien
à nos frères et soeurs en Iran et au Rojava !


Liste des signataires
=======================================

- Collectif des Iraniennes et Iraniens de Brest en soutien au peuple iranien,
- Communauté Kurde de Brest,
- AKB,
- NPA Brest,
- Collectif des Brestoises pour les droits des femmes,
- Union Locale CGT Brest,
- UCL Finistère,
- Douar Ha Frankiz,
- CNT interpro Brest,
- Solidaires 29,
- PCF 29,
- UDB Bro Brest,
- Brest Insoumise



