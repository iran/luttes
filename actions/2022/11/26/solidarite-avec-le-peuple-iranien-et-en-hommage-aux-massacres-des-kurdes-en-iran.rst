
.. index::
   pair: Rassemblement ; Samedi 26 novembre 2022


.. _iran_grenoble_2022_11_26:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ 🌄  10e rassemblement samedi 26 novembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarite avec le peuple iranien et aux kurdes durement réprimé·e·s en Iran, Syrie, Irak et Turquie** #Grenoble #MahsaAmini #MahsaJinaAmini #Kurdistan 📣
==============================================================================================================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 26 novembre 2022 cela fait donc 2 mois 1 semaine 3 jours
=> 71e jour = on est dans la 11e semaine de la révolution, N°semaine:47 (2022)


- :ref:`mahsa_jina_amini`
- :ref:`slogans`


Affiches
===========

Solidarite avec le peuple iranien et et aux kurdes durement réprimés en
Iran, Syrie, Irak et Turquie.


.. figure:: images/annonce_rassemblement_2022_11_26.png
   :align: center
   :width: 500

Samedi 26 novembre 2022, 10e semaine du soulèvement, 71e jour

#Grenoble #Solidarité #Kurdes #Kurdistan #Rojava #Syrie #Rojava #Turquie
#MahsaAmini #MahsaJinaAmini #IranProtests #iran #grenoble #france #JinaAmini #ZanZendegiAzadi
#azadi #freedom #revolution #زن_زندگی_آزادی #women #femme #vie #liberte
#jinjiyanazadi  #revolution


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|
- :ref:`#KianPirfalak <kian_pirfalak>` |KianPirfalak|

Texte annonçant le rassemblement
==================================

Il y a une révolution en cours en Iran

Il y a 2 mois de ça,  la République islamique d’Iran a arrêté puis
assassiné Mahsa-Jina Amini,  jeune femme kurde de 22 ans.

Pour la police des mœurs, elle n’était pas assez voilée.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

La population se solidarise tous les jours avec les femmes.

Jour et nuit, la jeunesse en insurrection, fait reculer les forces de
l'ordre armées jusqu'aux dents.

Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

La République islamique ne fait plus peur à ces jeunes qui malgré les massacres
et les empoisonnements et des condamnations  continuent leurs combats.

Pour que le monde ne soit pas au courant de ce qui se passe en Iran, le
régime perturbe internet le plus possible.

Aujourd'hui (vendredi 25 novembre 2022), on parle de 412 morts dont 51
enfants (en dessous de 18 ans).
18000 personnes ont été emprisonnés.
Des condamnations a morts ont commencé d'être prononcé. Tout en sachant
que ces nombres sont bien en dessous de la réalité.

Depuis la semaine dernière, le kurdistan d'Iran est sous les attaques
militaires et les mitraillettes.
On compte 42 morts et 2500 blessés en une semaine. La police volé les
corps des morts pour mettre la pression sur les familles pour dire que
leur proches sont décédés dans d'autres circonstances s'ils veulent avoir
le corps.
La police arrête les gens avec des ambulances et même tirent sur les
manifestants à partir des ambulances.

Nous demandons des gouvernements des pays occidentaux:

- d'arrêter tout contrat et de négociation avec le gouvernement islamique
  d'Iran,
- De mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort
- d'expulser les ambassadeurs et les représentants du gouvernent islamique
- de mettre sur la liste noir les acteurs des massacres et de confisquer
  leurs bien a l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyez leurs voix. les iraniens ont besoin d'un soutien réel et d'actions
réelles pour en finir avec ce régime sanguinaire; le blabla ne suffit pas.



**Cette manifestation est organisée par la communauté Iranienne**.



Quelques événements du 20 novembre au 27 novembre 2022
=============================================================================

- :ref:`debat_rojava_2022_11_27`
- :ref:`femme_vie_liberte_2022_11_25`
- :ref:`rojava_luttes:abkgrenoble_rojava_2022_11_25`
- :ref:`onu_iran_2022_11_24`
- :ref:`soutien_kurdistan_2022_11_23`
- :ref:`hitch_2022_11_22`


Annonces sur internet (réseaux sociaux et sites Web)
=======================================================

Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/evenement/rassemblement-de-solidarite-avec-le-peuple-iranien-et-hommage-aux-kurdes-durement-reprime-e-s-en-iran-syrie-irak-et-turquie

.. figure:: images/appel_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/rassemblement-de-solidarite-avec-le-peuple-iranien-et-hommage-aux-kurdes-durement-reprime-e-s-en-iran-syrie-irak-et-turquie

🌄 Dimanche 27 novembre 2022 à 14h30 **débat avec Agit Polat responsable du Conseil démocratique du Kurdistan**
---------------------------------------------------------------------------------------------------------------------

- :ref:`debat_rojava_2022_11_27`

.. figure:: images/autres_evenements.png
   :align: center


Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


