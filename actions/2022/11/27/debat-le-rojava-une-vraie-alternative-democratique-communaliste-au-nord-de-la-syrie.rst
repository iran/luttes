.. index::
   pair: Rojava ; Kurdistan

.. _debat_rojava_2022_11_27:

================================================================================================================================================================
🌄 Dimanche 27 novembre 2022 à 14h30 **débat avec Berivan Firat  porte-parole des relations extérieures du Conseil Démocratique kurde en France (CDK-F)**
================================================================================================================================================================

- :ref:`rojava_luttes:debat_rojava_2022_11_27`
- :ref:`rojava_luttes:abkgrenoble_rojava_2022_11_25`

.. figure:: images/berivan_firat.png
   :align: center

   :ref:`kurdistan_luttes:berivan_firat`


Débat avec :ref:`Berivan Firat <kurdistan_luttes:berivan_firat>`  |BerivanFirat| porte-parole des relations extérieures du Conseil Démocratique kurde en France (CDK-F)
(:ref:`Agit Polat <kurdistan_luttes:agit_polat>` était initialement annoncé) 🌄

Les Kurdes du Rojava mettent-ils réellement en application le confédéralisme
démocratique avec pour piliers la démocratie directe, l’émancipation
des femmes, l’écologie, et **l’inclusion de toutes les composantes ethniques
et religieuses de la société ?**

Organisé par :ref:`l’association iséroise des amis des Kurdes (AIAK)  <kurdistan_luttes:aiak>`.


Maison de Quartier Fernand Texier
161 avenue Ambroise Croizat
Saint-Martin-d'Hères
