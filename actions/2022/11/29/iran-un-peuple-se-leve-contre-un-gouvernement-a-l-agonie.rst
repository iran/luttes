.. index::
   pair: blast; Iran : un peuple se lève contre un gouvernement à l'agonie

.. _blast_un_peuple_se_leve_2022_11_29:

===============================================================================================
Mardi 29 novembre 2022 Blast **Iran : un peuple se lève contre un gouvernement à l'agonie**
===============================================================================================


- https://video.blast-info.fr/videos/watch/528037c1-9235-415d-bdf1-dbd5275285bc
- https://www.blast-info.fr/emissions/2022/iran-un-peuple-se-leve-contre-un-gouvernement-a-lagonie-UoA3wZI1QV298dvVJ1KFvA


.. figure:: images/golshiteh_farahani.png
   :align: center

   https://video.blast-info.fr/w/bbSWztuvnDD441nU2B2GTo?start=8m29s

.. figure:: images/golshiteh_farahani_les_choses_vont_sarranger.png
   :align: center

   https://video.blast-info.fr/w/bbSWztuvnDD441nU2B2GTo?start=8m58s

Interrogée par le quotidien **20 minutes** pour savoir si la situation
en Iran allait s’arranger, la comédienne iranienne Golshiteh Farahani a
répondu qu’elle pensait que **oui**.

Cela ne signifiait pas qu’elle espérait que les choses se calment.

Bien au contraire. Pendant 39 ans (son âge), elle avouait avoir toujours
répondu **non**. Mais là, c’était oui, un **grand oui**.

J’ai l’impression que le gouvernement islamique est devenu une fourmi
ridicule et blessée qui a perdu toute crédibilité, un monstre qui saigne.
Et même si c’est une question de temps, même s’il saigne longtemps, il va
finalement mourir parce que cette génération de la jeunesse a arraché sa
colonne vertébrale.

Il ne peut plus marcher ajoutait-elle avant de conclure: **Il va ramper
un moment puis s’écrouler. Les Iraniens ne le laisseront pas se relever**.

**Il était temps que Blast se saisisse de la révolution iranienne**.


.. figure:: images/marie_ladier_fouladi.png
   :align: center

Dans ce zoom arrière sentant le soufre, la sueur et le sang, Denis Robert
reçoit Marie Ladier-Fouladi, directrice de recherche au CNRS et démographe
spécialiste de l’Iran et l’avocat franco-iranien Sahand Saber.

.. figure:: images/sahand_saber.png
   :align: center


Ensemble ils vont chercher à y voir un peu plus clair dans ce que vit
le plus grand pays du Moyen Orient depuis la mort, le 16 septembre dernier
pour une mèche de cheveux mal cachée, de Masha Amini assassinée par la
police de l’Ayatollah Khamenei.

Depuis six semaines, pas un jour où une manifestation n’est pas réprimée,
une femme, un homme, un enfant ou un adolescent tués par les milices des
mollahs qui tiennent encore le pays. D’un fil, si on en croit les témoignages
d’opposants.

Difficile de chiffrer les morts : 416 morts répertoriés et plus de 15000
arrestations sans compter les disparitions.

Tout peut arriver là-bas, y compris le pire, une répression encore plus
sanglante. Le régime peut-il encore tenir ?

Les femmes sont-elles les seules à tenir le flambeau de la révolte ?

Comment les pays étrangers peuvent il aider le peuple iranien ?

Vers quoi peut basculer cet Etat religieux ?

Une opposition est-elle en train de s’organiser ?

Pourquoi les familles des hauts dignitaires de Téhéran quittent-elles le pays ?
sont quelques-unes des questions auxquelles vont répondre nos invités.


.. figure:: images/carte_iran.png
   :align: center

   https://video.blast-info.fr/w/bbSWztuvnDD441nU2B2GTo?start=32m40s


.. figure:: images/carte_iran_blast_densite.png
   :align: center


.. figure:: images/carte_iran_blast_langues.png
   :align: center


.. figure:: images/vol_ps752.png
   :align: center



.. figure:: images/hamed_esmaeilion.png
   :align: center

   Hamed Esmaeilion, https://video.blast-info.fr/w/bbSWztuvnDD441nU2B2GTo?start=52m41s


.. figure:: images/ali_karimi.png
   :align: center

   Ali Karimi, https://video.blast-info.fr/w/bbSWztuvnDD441nU2B2GTo?start=53m9s


.. figure:: images/kian_pirfalak.png
   :align: center

   :ref:`#KianPirfalak <kian_pirfalak>` |KianPirfalak|, https://video.blast-info.fr/w/bbSWztuvnDD441nU2B2GTo?start=57m1s
