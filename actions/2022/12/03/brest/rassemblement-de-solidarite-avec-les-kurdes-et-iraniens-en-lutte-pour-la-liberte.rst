.. index::
   pair: Brest ; 2022-12-03

.. _brest_kurdes_2022_12_03:

==============================================================================================================
Samedi 3 décembre 2022 **Rassemblement de solidarité avec les Kurdes et Iraniens en lutte pour la liberté**
==============================================================================================================

- https://kurdistan-au-feminin.fr/2022/12/04/bretagne-iran-kurdistan-rojava-kurdes/


.. figure:: images/on_ma_tuee.png
   :align: center

Rassemblement de solidarité avec les Kurdes et Iraniens en lutte pour la liberté
==================================================================================

BREST – Le samedi 3 décembre 2022, de nombreuses organisations et partis
politiques, ainsi que des collectifs d’Iraniens et Kurdes ont manifesté
ensemble à Brest, en Bretagne, en solidarité avec les Kurdes attaqués
par la Turquie et l’Iran et avec les peuples d’Iran en lutte pour la liberté.

Lors du rassemblement d’hier, les organisateurs de l’événement ont appelé
à la libération du chef du kurde Abdullah Ocalan et de tous les prisonniers
politiques détenus en Turquie.

Ils ont également exigé le retrait du PKK de la *liste des organisations terroristes*
de l’UE, tout en appelant à l’arrêt des attaques de l’armée turque contre
le Rojava et l’utilisation d’armes chimiques turques.

Les militants se sont rassemblés place La Liberté à Brest à 14h30 et ont
défilé dans les rues les plus fréquentées de la ville.

La foule a scandé des slogans en kurde, breton et français,
*Vive la guérilla résistante*, *Le Kurdistan sera le cimetière du fascisme*,
*Dictateur Erdogan*, *femme, vie, liberté*, *Vive le président Apo *
et *Les martyrs sont immortels*.

Les manifestants portaient également des banderoles sur lesquelles on
pouvait lire *Protégez la révolution du Rojava*, *femmes, vie, liberté*,
*Liberté pour Ocalan*, *Brisons le silence pour le Kurdistan* et des
banderoles des photos de 17 combattants du PKK tués dans des attaques
chimiques de l’armée turque au Kurdistan irakien.

Les amis du soldat français Olivier le Claînche (Kendal Breîzh), originaire
de Bretagne et tombé martyr le 10 février 2018 lors des bombardements
de l’État turc à Afrin, en Syrie, se sont joints à la marche.

Par ailleurs, les militants kurdes ont distribué des tracts concernant
les attaques, dont celles aux armes chimiques, turques visant le Kurdistan
irakien et le Rojava et le silence des pays occidentaux face à ces
crimes de guerre turcs.

La solidarité les peuples et femmes d’Iran
================================================

.. figure:: images/discours.png
   :align: center

Lors du rassemblement, des intervenant ont également dénoncé le régime
sanguinaire iranien qui tue, emprisonne et torture sa propre population
qui manifeste depuis près de 3 mois suite au meurtre barbare de Jina Mahsa
Amini par la police des moeurs à Téhéran.

L’action de solidarité s’est terminée par des chants kurdes et iraniens
et des danses kurdes.


Les organisateurs du rassemblement
====================================

Les organisateurs du rassemblement sont:

- Collectif des Iranien-nes de Brest,
- Communauté Kurde de Brest,
- Amitiés Kurdes de Bretagne,
- Brest Insoumise,
- CNT Interpro-Brest,
- Douar ha Frankiz, NPA BREST,
- Union Communiste Libertaire Finistère,
- UDB Bro-Brest,
- UDB Jeunes
- UDB Yaouank,
- Union Locale CGT BREST,
- Solidaires 29,
- PCF Pays de Brest,
- Collectif des brestoises pour les droits des femmes,
- Union Pirate


