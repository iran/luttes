
.. index::
   pair: Rassemblement ; Samedi 3 décembre 2022
   pair: Ici-Grenoble ; Samedi 3 décembre 2022
   pair: Mobilizon ; Samedi 3 décembre 2022

.. figure:: images/annonce_rassemblement_2022_12_03.png
   :align: center
   :width: 400

.. _iran_grenoble_2022_12_03:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ 🌄  11e rassemblement samedi 3 décembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarite avec le peuple iranien et aux kurdes durement réprimé·e·s en Iran** #Grenoble #MahsaAmini #MahsaJinaAmini 📣
==============================================================================================================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 03 décembre 2022 cela fait donc 2 mois 2 semaines 3 jours
=> 78e jour = on est dans la 12e semaine de la révolution, N°semaine:48 (2022)



- :ref:`mahsa_jina_amini`
- :ref:`slogans`

Solidarite avec le peuple iranien et et aux kurdes durement réprimés en
Iran, Syrie, Irak et Turquie.

Samedi 3 décembre 2022, 12e semaine de la révolution, 78e jour
#Grenoble #Iranrevolution
#JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran #grenoble #france #JinaAmini #ZanZendegiAzadi
#azadi #freedom #revolution #زن_زندگی_آزادی #women #FemmeVieLiberte
#JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran #JinaAmini #ZanZendegiAzadi
#azadi #freedom #revolution #زن_زندگی_آزادی  #FemmeVieLiberte
#Solidarité #Kurdes #Kurdistan #Rojava #Syrie #Rojava #Turquie
#jinjiyanazadi  #revolution


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|
- :ref:`#KianPirfalak <kian_pirfalak>` |KianPirfalak|

Texte annonçant le rassemblement du samedi 3 décembre 2022
==============================================================

Il y a une révolution en cours en Iran.

Le mardi 13 septembre 2022 Jina Amini était arrêtée par la police des mœurs
à Téhéran car elle n’était pas **"assez voilée"**.

Le vendredi 16 septembre 2022, la République islamique d’Iran a assassiné
Mahsa-Jina Amini, jeune femme kurde de 22 ans; **ce samedi 3 décembre 2022
cela fait donc 2 mois 2 semaines et 3 jours que Jina Masah Amini est morte**.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

La population se solidarise tous les jours avec les femmes.

Jour et nuit, la jeunesse en insurrection, fait reculer les forces de
l'ordre armées jusqu'aux dents.

Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

La République islamique ne fait plus peur à ces jeunes qui malgré les massacres
et les empoisonnements et des condamnations  continuent leurs combats.

Pour que le monde ne soit pas au courant de ce qui se passe en Iran, le
régime perturbe internet le plus possible.

Vendredi 1er décembre 2022, on parle de 462 morts dont 64 enfants (en dessous de 18 ans).

Nous demandons des gouvernements des pays occidentaux:

- d'arrêter tout contrat et de négociation avec le gouvernement islamique
  d'Iran,
- De mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort
- d'expulser les ambassadeurs et les représentants du gouvernent islamique
- de mettre sur la liste noir les acteurs des massacres et de confisquer
  leurs bien a l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyez leurs voix. les iraniens ont besoin d'un soutien réel et d'actions
réelles pour en finir avec ce régime sanguinaire; le blabla ne suffit pas.


**Cette manifestation est organisée par la communauté Iranienne**.



Quelques événements du 27 novembre au 2 décembre 2022
=============================================================================

- :ref:`ali_rakhshani_mohammad_rakhshani_2022_12_02`
- :ref:`blast_un_peuple_se_leve_2022_11_29`


  .. figure:: images/ici_grenoble_golshiftef_farahani.png
     :align: center

     :ref:`blast_un_peuple_se_leve_2022_11_29`, https://video.blast-info.fr/w/bbSWztuvnDD441nU2B2GTo?start=8m58s


- :ref:`debat_rojava_2022_11_27`


Annonces sur internet (Ici-grenoble, mobilizon)
=======================================================

Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/evenement/manifestation-de-solidarite-avec-le-peuple-iranien-et-les-kurdes-durement-reprime-e-s-en-iran


.. figure:: images/ici_grenoble_kilian.png
   :align: center

   :ref:`#KianPirfalak <kian_pirfalak>` |KianPirfalak|, https://video.blast-info.fr/w/bbSWztuvnDD441nU2B2GTo?start=57m1s


En une de https://www.ici-grenoble.org
+++++++++++++++++++++++++++++++++++++++++++++

.. figure:: images/en_une_ici_grenoble.png
   :align: center

   En une de https://www.ici-grenoble.org


📣 Annonce sur https://mobilizon.chapril.org/@iranluttes38 📣
-----------------------------------------------------------------

- :ref:`flux web atom <tuto_fediverse:fed_rss_2022_11_18>` : https://mobilizon.chapril.org/@iranluttes38/feed/atom
- https://mobilizon.chapril.org/events/48e3d413-d628-4ca8-a333-419aee6e8b46


.. figure:: images/mobilizon_chapril_resume.png
   :align: center


Requête de recherche sur https://mobilizon.chapril.org
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


- https://mobilizon.chapril.org/search?search=grenoble&contentType=EVENTS&eventPage=1&target=INTERNAL&when=today



Autres événements ce samedi 3 décembre 2022 à Grenoble
---------------------------------------------------------

.. figure:: images/ici_grenoble_2022_12_03.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/le-3-decembre-1983-la-premiere-marche-pour-legalite-et-contre-le-racisme-arrive-a-paris
- https://www.ici-grenoble.org/evenement/cinema-annie-colere-sur-les-luttes-du-mlac-en-1974-pour-le-droit-a-livg


Quelques photos
=================

.. figure:: images/aiak.jpeg
   :align: center
   :width: 300

   AIAK, Association iseroise des amis des kurdes :ref:`kurdistan_luttes:aiak`


.. figure:: images/scene_zoya.jpeg
   :align: center
   :width: 300

   Slogans


.. figure:: images/3_femmes_marjane.jpeg
   :align: center
   :width: 300

   Slogans


.. figure:: images/mains_ensanglantes.jpeg
   :align: center
   :width: 300

   Mains ensanglantées

.. figure:: images/le_cercle.jpeg
   :align: center
   :width: 300

   Le cercle


.. figure:: images/la_photographe.jpeg
   :align: center
   :width: 300

   Le cercle

Manifestations précédentes à Grenoble
=======================================


- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


