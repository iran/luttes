
.. _actions_iran_2022_12_05:

===========================================================================================
Un bilan partiel des manifestations et grèves en #Iran pour la journée du 5 décembre 2022
===========================================================================================


Un bilan partiel des manifestations et grèves en #Iran pour la journée du 5 décembre 2022
==============================================================================================

L'appel des 5, 6 et 7 décembre

Un bilan partiel des manifestations et grèves en #Iran pour la journée du 5 décembre 2022:

🔴 Manifestations:

- ✔️ Dans 13 quartiers de #Téhéran et plusieurs stations du métro;
- ✔️ Dans 19 villes de tous les coins du pays;
- ✔️ Dans 13 universités également partout dans le pays.

🔴 Grèves sans précédent depuis 43 ans:

- ✔️ Dans 24 endroits commerciaux différents de Téhéran;
- ✔️ Dans 60 villes aux quatre coins cardinaux du pays.

⬇️ La caserne des bassidjis incendiée à #Arak

- https://cdn.masto.host/mastheadsocial/media_attachments/files/109/463/816/814/501/501/original/ad30faa17a6b0bc6.mp4

Toute la cité Tchitgar de #Téhéran
===================================

- https://masthead.social/@NaderTeyf/109462808278578741

Toute la cité Tchitgar de #Téhéran a encore tremblé ce soir avec les
slogans de ses habitants visant directement le régime en #Iran et ses
bras de répression:" Mort à Khamenei", "Mort aux bassidjis (miliciens)
et "Mort aux Pasdaran (gardiens).

Les manifestants ont justement brûlé une caserne de bassidjis à #Arak
de telle sorte que les pompiers n'ont pas réussi à étreindre le feu.

Les bassidjis sont les forces armées qui tuent les protestataires.

dans plusieurs autres quartiers de #Téhéran
=============================================

- https://masthead.social/@NaderTeyf/109462470975098220

Des manifestations ont eu lieu ce soir dans plusieurs autres quartiers
de #Téhéran.

Les jeunes révolutionnaires de #Machhad, #Chiraz, #Marivan et #Amol ont
eu la même tactique: faire des barrages dans les rues.

Des manifestations se sont tenues à #Arak. Ici à #Karaj les manifestants
demandent aux autres passants de les rejoindre et scandent: "Mort au dictateur"
et "Pauvreté, corruption et la vie chère/Nous irons jusqu'au renversement (du régime)".

