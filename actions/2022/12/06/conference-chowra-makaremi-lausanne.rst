.. index::
   pair: Chowra Makaremi; Mardi 6 décembre 2022

.. _chowra_makaremi_2022_12_06:

========================================================================================================================
Mardi 6 décembre 2022, conférence de Chowra Makaremi @chowmak & Babak Kia sur le soulèvement iranien à Lausanne 🔥
========================================================================================================================

- https://solidarites.ch/vaud/2022/11/29/iran-les-femmes-au-coeur-du-mouvement-populaire/
- https://nitter.poast.org/solidariteSvaud/status/1598425979726209038#m

.. figure:: images/conference_chowmak.png
   :align: center


Conférence Iran: les femmes au coeur du mouvement populaire
===============================================================


Mardi 6 décembre 2022, conférence de Chowra Makaremi @chowmak & Babak Kia
sur le soulèvement iranien 🔥

Avec le cercle de débats Rosa Luxembourg + la @GF_Vaud + @pagesdegauche
+ les femmes pour la démocratie + les Foulards violets

Présentation
==============

- https://solidarites.ch/vaud/2022/11/29/iran-les-femmes-au-coeur-du-mouvement-populaire/


.. figure:: images/image_conference_chowmak.png
   :align: center

   https://solidarites.ch/vaud/2022/11/29/iran-les-femmes-au-coeur-du-mouvement-populaire/


En Iran, depuis maintenant plus de deux mois, des manifestations se succèdent
dans tous les coins du pays. La coupe est pleine : la paupérisation
croissante de la population, l’oppression des minorités nationales, la
corruption des élites ne sont plus supportables. 

La mort de Jina Mahsa Amini, jeune femme kurde arrêtée car elle ne portait
pas « correctement » son hijab, a mis le feu au pays.

Dans toutes les régions de l’Iran, autour du slogan Femme – Vie – Liberté,
la jeunesse, et en particulier les jeunes femmes, se regroupent et crient
« mort au dictateur » dans des manifestations brutalement réprimées par
le gouvernement.

SolidaritéS, le cercle de débats Rosa Luxembourg, la Grève Féministe Vaud,
Pages de Gauche, les FemmeS pour la démocratie et les Foulards Violets 
vous invitent à venir en apprendre plus sur ce mouvement historique et
à lui témoigner votre solidarité.

Annonce sur https://mobilizon.chapril.org/@iranluttes38
==========================================================

- :ref:`flux web atom <tuto_fediverse:fed_rss_2022_11_18>` https://mobilizon.chapril.org/@iranluttes38/feed/atom
- https://mobilizon.chapril.org/events/4b247adb-9ad0-4fdf-a50b-31b40d8c25e5
