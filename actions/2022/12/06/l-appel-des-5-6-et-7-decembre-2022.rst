
.. _appel_iran_2022_12_06:

=======================================================================================================
Mardi 6 décembre 2022 Le deuxième jour des grèves continue dans les universités + pétrochime et ciment
=======================================================================================================


Deux grèves ouvrières importantes de ce 6 décembre 2022
===========================================================

- https://masthead.social/@NaderTeyf/109466960157554936

Les ouvriers ont fait plus de 4000 grèves l'année dernière en #Iran alors
que faire grève est interdit.

Deux grèves ouvrières importantes de ce 6 décembre:

- ☑️ Grève des ouvriers de la pétrochimie de #Sanandaj depuis hier,
  surtout pour augmentation de salaire et suppression des sociétés sous-traitantes;
- ☑️ Grève des ouvriers du Ciment Sepahan de #Ispahan.
  Les grévistes ont expulsé trois directeurs de cette entreprise gérée
  par les Pasdaran.


Un bilan partiel des manifestations et grèves en #Iran pour la journée du 5 décembre 2022
==============================================================================================

- https://masthead.social/@NaderTeyf/109466372005807963

L'appel des 5, 6 et 7 décembre

Le deuxième jour des grèves continue dans les universités. Il y eu au
moins 13 universités en mouvement pour le premier jour en #Iran.

Les étudiant.es font des rassemblements et manifestations souvent à
l'intérieur des établissements universitaires, mais beaucoup sont
interdits d'entrer.

Les étudiant.es de l'université d'Allameh Tabataba'i à #Téhéran essayaient
ce matin d'entrer.

Les manifestations sont reprises dans plusieurs quartiers de #Téhéran et certaines autres villes de province
================================================================================================================

- https://masthead.social/@NaderTeyf/109468047028134242

L'appel des 5, 6 et 7 décembre
Les manifestations sont reprises dans plusieurs quartiers de #Téhéran
et certaines autres villes de province en #Iran comme #Karaj, #Kermanchah
et #Sanandaj.

Ce clip concerne le quartier Pounak où les manifestants promettent ce
soir au pouvoir son renversement.

#Mahsa_Amini

- https://cdn.masto.host/mastheadsocial/media_attachments/files/109/468/036/407/643/310/original/54c3e2d792ecf52f.mp4

Beaucoup d'écoles et de lycées étaient en grève aujourd'hui en #Iran
========================================================================

- https://masthead.social/@NaderTeyf/109467725403718099

L'appel des 5, 6 et 7 décembre
Beaucoup d'écoles et de lycées étaient en grève aujourd'hui en #Iran.
De petits groupes se sont formés pour manifester dans les rues comme ici
à #Téhéran où les jeunes scandent haut et fort contre le numéro 1 du régime,
l'ayatollah Khamenei, en le qualifiant d'assassin.

#Mahsa_Amini

- https://cdn.masto.host/mastheadsocial/media_attachments/files/109/467/721/027/800/082/original/cb150871f27ca065.mp4

Au septième jour du décès de #Mehran#Samaak,  #Mehran_Samaak
================================================================

- https://masthead.social/@NaderTeyf/109467238802044774

Au septième jour du décès de #Mehran_Samaak, des habitants de #Bandar_e_Anzali
ont voulu lui rendre un nouvel hommage aujourd'hui.

Les forces de répression ont lancé des grenades lacrymogènes et tiré.
Les manifestant.es ont scandé contre le régime dans toutes les rues menant
au cimetière.

Les forces du régime ont tué Mehran le soir de l'élimination de l'équipe
de football des mollahs au Qatar.
Les gens de plus de 20 villes sont descendus dans les rues pour fêter
cet échec.

Mahsa_Amini

- https://cdn.masto.host/mastheadsocial/media_attachments/files/109/467/213/067/961/232/original/354e1120a762f687.mp4

CETTE ANNÉE est celle du sang, Seyed-Ali sera renversé !
=============================================================

- https://nitter.manasiwibi.com/arminarefi/status/1600119921450487809#m

**CETTE ANNÉE est celle du sang, Seyed-Ali sera renversé !**, scandent
ce mardi 6 décembre des étudiants de l’université de Technologie Khajeh
Nassir Toussi de Téhéran, qui ciblent l’ayatollah Khamenei à l’occasion
du deuxième jour de mobilisation nationale en #Iran. #MahsaAmini


mort au dictateur
====================

- https://nitter.manasiwibi.com/FaridVahiid/status/1600153391254020097#m

Quel courage ! Après le discours du maire de Téhéran, les étudiants de
l’université de Sharif lui crient « mort au dictateur » alors qu’il est
entouré par des forces de Basij. #Iran #MahsaAmini


