
.. _appel_iran_2022_12_07:

====================================================================================================================
Mercredi 7 décembre 2022 **La journée de l'étudiant**, le troisième jour des grèves continue dans les universités
====================================================================================================================


Le 7 décembre 1953 le régime du Chah a tué 3 étudiants à #Téhéran
===================================================================

- https://masthead.social/@NaderTeyf/109472144333715129

L'appel des 5, 6 et 7 décembre

Le 7 décembre 1953 le régime du Chah a tué 3 étudiants à #Téhéran.

Le régime des mollahs a tout fait pour arrêter le mouvement étudiant
depuis 11 semaines. Les étudiant.es manifestent aujourd'hui dans de
nombreuses universités en #Iran.

Les forces de répression ont attaqué certaines protestations.

Ici à l'université d'Allameh Tabataba'i, les étudiant.es scandent entre
autres:"Étudiant est éveillé/Il déteste le totalitarisme" et
"Étudiants, ouvriers/Unité, unité".

- https://cdn.masto.host/mastheadsocial/media_attachments/files/109/472/126/765/182/727/original/28ddbff74bc1ab24.mp4

des rassemblements sans précédent depuis 3 mois
===================================================

- https://masthead.social/@NaderTeyf/109473123852114895

L'appel des 5, 6 et 7 décembre

Selon les dernières informations, des rassemblements sans précédent
depuis 3 mois se forment sur les places Emam Hossein, Enghelab et Azadi
de #Téhéran.

Elles sont sur une ligne droite et il y a 10 kilomètres de distance
entre les deux extrémités.

Internet est fortement perturbé et aucune image ou vidéo n'a pu être
encore diffusée.

D'autres informations confirment que des protestations sont en cours à
#Kermanchah, #Rasht, #Qazvin, #Machhad et #Chiraz.

#Mahsa_Amini


Certains protestataires ont pu aller sur la place Azadi (Liberté) de #Téhéran
===============================================================================

- https://masthead.social/@NaderTeyf/109473776867934767
- https://cdn.masto.host/mastheadsocial/media_attachments/files/109/473/759/952/843/721/original/8171309e91ab34f0.mp4

L'appel des 5, 6 et 7 décembre
Certains protestataires ont pu aller sur la place Azadi (Liberté) de #Téhéran
pour manifester contre le régime des mollahs en #Iran.

Les forces de répression ont jeté des grenades de gaz lacrymogènes et
tiré.

L'on voit le monument Azadi en haut à gauche sur le clip. Il y a d'autres
manifestations dans d'autres quartiers de la capitale.

Des manifestations ont eu lieu dans d'autres villes comme #Ardakan,
#Nadjafabad, #Boukan et #Saghez, la ville de #Mahsa_Amini



La ville de #Qorveh est presque entièrement en grève aujourd'hui
====================================================================

- https://masthead.social/@NaderTeyf/109472533546613410

La ville de #Qorveh est presque entièrement en grève aujourd'hui.

Des habitants ont décidé de marcher dans les rues en SILENCE, mais les
forces de répression sont quand bien arrivées quelques minutes plus tard.

Elles ont tiré et blessé au moins 4 personnes.

#Mahsa_Amini

- https://cdn.masto.host/mastheadsocial/media_attachments/files/109/472/523/792/082/417/original/c2fd8634dbd46b3f.mp4
