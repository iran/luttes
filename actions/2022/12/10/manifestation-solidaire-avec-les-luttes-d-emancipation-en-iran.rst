
.. index::
   pair: Rassemblement ; Samedi 10 décembre 2022


.. figure:: images/annonce_2022_12_10.jpg
   :align: center
   :width: 400

.. _iran_grenoble_2022_12_10:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ 🌄  12e rassemblement samedi 10 décembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarité avec le peuple iranien, libération des prisonniers politiques et annulation des condamnations à mort** 📣
==============================================================================================================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022.
ce samedi 10 décembre 2022 cela fait donc 2 mois 3 semaines 3 jours
=> 85e jour = on est dans la 13e semaine de la révolution.


- :ref:`mahsa_jina_amini`
- :ref:`slogans`

Samedi 10 décembre 2022, 13e semaine de la révolution, 85e jour
#Grenoble #Iranrevolution
#JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran #grenoble #france #JinaAmini #ZanZendegiAzadi
#azadi #freedom #revolution #زن_زندگی_آزادی #women #FemmeVieLiberte
#JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran #JinaAmini #ZanZendegiAzadi
#azadi #freedom #revolution #زن_زندگی_آزادی  #FemmeVieLiberte
#Solidarité #Kurdes #Kurdistan #Rojava #Syrie #Rojava #Turquie
#jinjiyanazadi  #revolution


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|
- :ref:`#KianPirfalak <kian_pirfalak>` |KianPirfalak|

Texte annonçant le rassemblement du samedi 10 décembre 2022
==============================================================

Il y a une révolution en cours en Iran.

Le mardi 13 septembre 2022 Jina Amini était arrêtée par la police des mœurs
à Téhéran car elle n’était pas **"assez voilée"**.

Le vendredi 16 septembre 2022, la République islamique d’Iran a assassiné
Mahsa-Jina Amini, jeune femme kurde de 22 ans; **ce samedi 10 décembre 2022
cela fait donc 2 mois 2 semaines et 3 jours que Jina Masah Amini est morte**.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

La population se solidarise tous les jours avec les femmes.

Jour et nuit, la jeunesse en insurrection, fait reculer les forces de
l'ordre armées jusqu'aux dents.

Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

La République islamique ne fait plus peur à ces jeunes qui malgré les massacres
et les empoisonnements et des condamnations  continuent leurs combats.

Vendredi 8 décembre 2022, on parle de 462 morts dont 64 enfants (en dessous de 18 ans).

Nous demandons des gouvernements des pays occidentaux:

- d'arrêter tout contrat et de négociation avec le gouvernement islamique
  d'Iran,
- De mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort
- d'expulser les ambassadeurs et les représentants du gouvernent islamique
- de mettre sur la liste noir les acteurs des massacres et de confisquer
  leurs bien a l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyez leurs voix. les iraniens ont besoin d'un soutien réel et d'actions
réelles pour en finir avec ce régime sanguinaire; le blabla ne suffit pas.

A ceux qui pensent qu’en supprimant la police des mœurs en Iran le
dimanche 4 décembre 2022, les Iraniennes et les Iraniens deviendront libres…
44 ans d’expérience, on connaît les barbares et leurs barbaries.

**Nous irons jusqu’à la liberté vraie, humaine, cette fois-ci jusqu’au bout.**

**Cette manifestation est organisée par la communauté Iranienne**.



Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`petition_contre_condamnations_2022_11_11`


Quelques événements du 4 décembre au 9 décembre 2022
=============================================================================


- :ref:`assassinat_mohsen_shekari_2022_10_08`
- :ref:`appel_iran_2022_12_07`
- :ref:`appel_iran_2022_12_06`
- :ref:`actions_iran_2022_12_05`
- :ref:`moeurs_2022_12_04`

Annonces sur internet (Ici-grenoble)
=======================================================

Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/evenement/manifestation-de-solidarite-avec-le-peuple-iranien-pour-la-liberation-des-prisonniers-politiques-et-lannulation-des-condamnations-a-mort



Autres événements ce samedi 10 décembre 2022 à Grenoble
---------------------------------------------------------

.. figure:: images/autres_evenements_ici_grenoble.png
   :align: center


Quelques photos
=================

.. figure:: images/corde_de_pendu.png
   :align: center


.. figure:: images/militante_froid.png
   :align: center



Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


