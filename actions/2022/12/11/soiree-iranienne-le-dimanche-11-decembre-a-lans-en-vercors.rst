.. index::
   pair: Cinéma; Le Clap
   pair: Isère; Lans-en-Vercors

.. _isere_iran_2022_12_11:

==============================================================================================================
Dimanche 11 décembre 2022 **Soirée iranienne à Lans-en-Vercors organisée par le cinéma Le Clap**
==============================================================================================================

- https://mobilizon.chapril.org/events/44229a4f-dd61-45e0-ae10-0f9b52674a30

.. figure:: images/soiree_iranienne.png
   :align: center


.. figure:: images/zara_et_zoreh.png
   :align: center

   A droite, Madame Zoreh Baharmast, présidente de la ligue des droits de l'Homme en Iran et à gauche Zara jeune iranienne



Cinéma Le Clap
=====================

Centre culturel Le Cairn
180 rue des écoles
38250 Lans-en-Vercors
Le cinéma se situe sur la rue principale de Lans-en-Vercors : l’avenue Leopold Fabre

Aurélie Tanné / Programmatrice, Médiatrice

::

    info.leclapvercors @ mail.com / 07 66 18 39 59

Cinéma Le Clap (Le Cairn)

- https://leclapvercors.fr/

180 rue des écoles 38250 Lans-en-Vercors

Au moment où le peuple iranien manifeste contre le régime qui l’oppresse,
Le Clap vous propose une soirée autour de deux films iraniens qui sortent
en France cet automne:

- :ref:`juste une nuit <juste_une_nuit>`
- :ref:`aucun ours <aucun_ours>`
