
.. _executions_2022_12_12:

==================================================================================================================================
2022-12-12 Le régime des ayatollahs a déjà exécuté par pendaison deux protestataires #Mohsen_Shekari et #Majidreza_Rahnavard
==================================================================================================================================


- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|


- https://masthead.social/@NaderTeyf/109507031220180266

Le régime des ayatollahs a déjà exécuté par pendaison deux protestataires,
:ref:`#Mohsen_Shekari <mohsen_shekari_2022_10_08>` et :ref:`#Majidreza_Rahnavard <majid_reza_rahnavard_2022_12_12>`

Les étudiant.es multiplient leurs rassemblements contre la peine de mort
partout en #Iran, comme à la faculté de médecin à #Tabriz ce matin.
