
.. _expulsion_com_femmes_2022_12_14:

===============================================================================================
Mercredi 14 décembre 2022 **Expulsion de l'Iran de la Commission de la femme de l'ONU (CSW)**
===============================================================================================


- https://www.rfi.fr/fr/en-bref/20221214-les-nations-unies-expulsent-l-iran-d-une-commission-sur-les-femmes?ref=tw_i


Le Conseil économique et social de l'ONU (Ecosoc) a voté ce mercredi 14
décembre 2022 l'expulsion de l'Iran de la **Commission de la femme de
l'ONU (CSW)** en raison de la répression des manifestations par la
République islamique depuis septembre.

L'Ecosoc, qui chapeaute la CSW, s'est prononcé à 29 voix pour (16 abstentions
et huit contre) pour sortir Téhéran de cette commission pour le reste
de son mandat jusqu'en 2026, suite à l'adoption d'une résolution présentée
par les États-Unis, qui n'ont pas de relations diplomatiques avec l'Iran
depuis 1980 et qui était portée depuis deux mois par une **pétition internationale**.



- https://nitter.manasiwibi.com/arminarefi/status/1603075974589026304#m

LA République islamique d’#Iran exclue ce mercredi 14 décembre de la
Comission de l’ONU sur le statut des femmes par 29 voix pour, 8 voix
contre et 16 abstentions.

Décision votée par les 54 membres du Conseil économique et social de
l’ONU sur proposition des États-Unis.

.. figure:: images/les_votes.png
   :align: center
