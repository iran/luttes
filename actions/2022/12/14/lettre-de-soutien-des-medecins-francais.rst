.. index::
   pair: Soutien; Médecins français

.. _soutien_medecins_2022_12_14:

===========================================================================================================
Mercredi 14 décembre 2022 **Lettre de soutien des médecins français aux droits de l'Homme en Iran**
===========================================================================================================

- :ref:`soutien_place_felix_poulat_2022_12_17`



#StopExecutionsInIran #Solidarité  #Iran #JinaAmini #MahsaAmini #MahsaJinaAmini
#Medecins #france

Lettre de soutien des médecins français
=========================================

Les droits de l’homme, l’enseignement et la pratique médicale sont
en danger en Iran.

Depuis près de trois mois et suite au décès d’une jeune femme de
22 ans, Mahsa (Jina) Amini, le monde universitaire iranien, à l’instar
de la société tout entière, est le lieu de manifestations quotidiennes
réprimées dans une violence inouïe.

Le peuple iranien tente d’exprimer son rejet de la dictature en place et
des conditions politiques, culturelles et économiques qu’elle impose
par la force.

**Or, les cris pacifiques de protestation ont été, à chaque fois, réprimés
dans le sang**.

La situation actuelle se singularise par le rôle pivot des femmes, des
étudiants et des lycéens avec des arrestations touchant une majorité
de personnes de moins de 25 ans et au moins 45 personnes en
dessous de 18 ans.

Les universités sont quotidiennement les lieux de répression de manifestations.
Les étudiants sont arrêtés au sein même de ces lieux sacrés de savoir
et de formation.

Les ambulances sont utilisées pour transporter les personnels de la
répression. Pire encore, les blessés sont traqués jusque dans les
services d’urgences où ils sont privés de soins et arrêtés.

Les médecins sont menacés et forcés d’établir des certificats en
camouflant l’origine des blessures et des décès.

De nombreuses arrestations ont eu lieu et des condamnations à mort
ou des longues peines de prison viennent d’être prononcées.
D’après plusieurs sources concordantes, plusieurs médecins refusant d’être
complices seraient arrêtés. Les secours et les soins les plus
élémentaires aux blessés seraient volontairement empêchés ou
retardés.

**Ainsi, les lieux de soins et les universités ne sont plus les enceintes
inviolables qu’elles devraient être**.

À la suite d’un procès expéditif par un tribunal et à l’encontre du
respect des droits élémentaires de l’homme, une condamnation à
mort a été récemment prononcée contre un collègue médecin
radiologue, le Dr Hamid Ghareh Hasanlou arrêté au cours d’une
manifestation. Sa femme, technicienne de laboratoire, a été
condamnée à 25 ans de prison.

Treize autres condamnations à mort ont été prononcées contre de jeunes
manifestants. L’un d’eux Mohsen Shekari, âgé de 23 ans, a été exécuté
le 9 décembre.

En tant que médecins, universitaires, alertés par nos collègues
français d'origine iranienne, nous ne pouvons rester silencieux face à
ces exactions que nous condamnons et dénonçons avec la plus
grande fermeté.

Au-delà du soutien que nous témoignons au peuple iranien, nous protestons
contre les pressions exercées sur les médecins et enseignants dans
l’exercice de leur profession et contre les arrestations massives et
arbitraires des étudiants.

**Nous demandons la suspension immédiate de toutes les condamnations à mort.**


- Pr Didier Samuel Président de la conférence des doyens des facultés de médecine
- Dr Thierry Godeau Président de la conférence des présidents de CME des centres hospitaliers
- Pr Rémi Salomon Président de la conférence des présidents de CME des centres hospitalo-universitaires

