.. index::
   ! Javad Rehman

.. _javad_rehman_2022_12_16:

=====================================================================================================================
Vendredi 16  Iran : **Le régime se venge et punit les manifestants, je reste absolument horrifié, choqué et indigné**
=====================================================================================================================

- https://www.ohchr.org/fr/special-procedures/sr-iran

Introduction
==============

Depuis qu'il est en poste, Javaid Rehman, rapporteur spécial de l’ONU sur
les droits de l’homme en Iran, n'a pas été autorisé à aller dans le pays.

Un pays où, chaque jour, ont lieu des "exécutions sommaires, arbitraires
et illégales", témoigne-t-il.

Javaid Rehman est rapporteur spécial de l’ONU sur les droits de l’homme
en Iran.

Ce professeur de droit, qui enseigne à la Brunel University de Londres,
**dénonce la violence de la répression et s’alarme de la dérive du régime**
depuis l’arrivée au pouvoir à Téhéran du président ultraconservateur Ebrahim Raïssi.

Il y a trois mois jour pour jour (16 septembre 2022), la mort d'une jeune Kurde de 22 ans,
Mahsa Amini, faisait basculer le pays dans une nouvelle période de troubles.

Comme lors des manifestations de 2009 et 2019, le régime des mollahs a
choisi la manière forte pour mater la protestation. Les morts se comptent
par centaines, plus de 450 personnes, les arrestations par milliers.


Téhéran a également recours aux exécutions pour tenter de dissuader les
jeunes Iraniens de descendre dans la rue. Et tout semble indiquer que la
répression va se poursuivre tant que le mouvement de contestation durera.

Comment avez-vous réagi aux exécutions de deux manifestants les 8 et 12 décembre 2022 ?
===========================================================================================

Javaid Rehman : J’ai été et je reste absolument horrifié, choqué et
indigné par la manière dont le régime iranien a traité ces manifestants.

**C'est une vengeance, ce sont des punitions**.

Et ce sont des symboles de la brutalité de l'État. Il s'agit d'exécutions
sommaires, arbitraires et illégales. Ces deux jeunes hommes ont été torturés
pour qu'ils avouent. Ils n'ont pas été autorisés à consulter des avocats
susceptibles de défendre leur cause.
Ces procès sont des simulacres de procès. Et cela constitue une violation
totale du droit à un procès équitable.

Je reçois constamment des informations et des messages de la part des
familles, des proches des victimes et de la société civile indiquant
qu'il existe un risque d'exécution imminente d'un certain nombre de
manifestants. Je crains donc que de nouvelles exécutions n'aient lieu.
