
.. index::
   pair: Rassemblement ; Samedi 17 décembre 2022


.. figure:: images/annonce_rassemblement.png
   :align: center
   :width: 300

.. _iran_grenoble_2022_12_17:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ 13e rassemblement samedi 17 décembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarité avec le peuple iranien, libération des prisonniers politiques et annulation des condamnations à mort** 📣
==============================================================================================================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 17 décembre 2022 cela fait donc 3 mois et 1 jour que Jina a été assassiné
=> 92e jour = on est dans la 14e semaine de la révolution, N°semaine:50 (2022)


- :ref:`mahsa_jina_amini`
- :ref:`slogans`

Samedi 17 décembre 2022, 14e semaine de la révolution, 92e jour
#Grenoble #Iranrevolution #StopExecutionsInIran
#JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran #grenoble #france #JinaAmini #ZanZendegiAzadi #AidaRostami
#azadi #freedom #revolution #زن_زندگی_آزادی #women #FemmeVieLiberte
#JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran #JinaAmini #ZanZendegiAzadi
#azadi #freedom #revolution #زن_زندگی_آزادی  #FemmeVieLiberte
#Solidarité #Kurdes #Kurdistan #Rojava #Syrie #Rojava #Turquie
#jinjiyanazadi  #revolution


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|
- :ref:`#KianPirfalak <kian_pirfalak>` |KianPirfalak|
- :ref:`#AidaRostami <aida_rostami>` |AidaRostami|



Texte annonçant le rassemblement du samedi 17 décembre 2022
==============================================================

Il y a une révolution en cours en Iran.

Le mardi 13 septembre 2022 Jina Mahsa Amini était arrêtée par la police des mœurs
à Téhéran car elle n’était pas **"assez voilée"**.
Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

Nous demandons des gouvernements des pays occidentaux:

- d'arrêter tout contrat et de négociation avec le gouvernement islamique
  d'Iran,
- De mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort
- d'expulser les ambassadeurs et les représentants du gouvernent islamique
- de mettre sur la liste noire les acteurs des massacres et de confisquer
  leurs bien a l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyez leurs voix. les iraniens ont besoin d'un soutien réel et d'actions
réelles pour en finir avec ce régime sanguinaire; le blabla ne suffit pas.


**Nous irons jusqu’à la liberté vraie, humaine, cette fois-ci jusqu’au bout.**

**Cette manifestation est organisée par la communauté Iranienne**.



Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`petition_contre_condamnations_2022_11_11`

.. figure:: images/mollah_corde.png
   :align: center


Quelques événements du 11 décembre au 16 décembre 2022
=============================================================================

- :ref:`expulsion_com_femmes_2022_12_14`
- :ref:`soutien_medecins_2022_12_14`
- :ref:`executions_2022_12_12`
- :ref:`isere_iran_2022_12_11`


Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/evenement/rassemblement-de-solidarite-avec-le-peuple-iranien-pour-liberation-des-prisonniers-politiques-et-lannulation-des-condamnations-a-mort


.. figure:: images/ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/rassemblement-de-solidarite-avec-le-peuple-iranien-pour-liberation-des-prisonniers-politiques-et-lannulation-des-condamnations-a-mort



Mastodon
---------

- https://kolektiva.social/@iranluttes/109508253813811518


Mobilizon
------------

- https://mobilizon.chapril.org/events/6dd49e79-4cf1-477a-8969-ed7ee63459cf


Autres événements ce samedi 17 décembre 2022 à Grenoble
---------------------------------------------------------


.. figure:: images/autres_evenements.png
   :align: center

   https://www.ici-grenoble.org/agenda


Quelques photos
=================

.. figure:: images/chant_fin_de_manif.png
   :align: center


.. figure:: images/pancartes_dans_la_neige.png
   :align: center


.. _soutien_place_felix_poulat_2022_12_17:

Lettre de soutien des médecins français sur la place Félix Poulat
-------------------------------------------------------------------

La vidéo sur peertube: https://www.orion-hub.fr/w/sqxUNqd7wN72QT5XNE9JLA

.. figure:: images/discours_medecin.png
   :align: center

   :ref:`soutien_medecins_2022_12_14`


Manifestations précédentes à Grenoble
=======================================


- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


