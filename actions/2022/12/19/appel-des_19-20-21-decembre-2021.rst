
.. _appel_2022_12_19:

======================================================================
Lundi 19 décembre 2022 **L'appel des 19 20 et 21 décembre 2022**
======================================================================


Liberté, liberté, liberté
============================

- https://masthead.social/@NaderTeyf/109541736693286231

Une vidéo montre l'attaque des forces de répression dans une station du
métro de #Téhéran contre les gens qui scandaient:"Nous ne voulons pas
de régime islamique."

Celle-ci concerne la ville de :ref:`#Racht <racht>` ce soir et le slogan simple et
continu de **Liberté, liberté, liberté.**


La grève et le rassemblement des travailleurs du terminal pétrolier de l'île de #Qeshm pour #Femme_Vie_Liberté
================================================================================================================

- https://masthead.social/@NaderTeyf/109539942617218513

Le Conseil organisateur des protestations des ouvriers non-contractuels
avait appelé à la grève pour les 3 journées. L'ouvrier qui envoie cette
courte vidéo dit:"La grève et le rassemblement des travailleurs du terminal
pétrolier de l'île de #Qeshm pour #Femme_Vie_Liberté le 19 décembre."

#Mahsa_Amini


Des villes kurdes sont en grève
================================

- https://masthead.social/@NaderTeyf/109539886134267328

Des villes kurdes comme #Ravansar, #Kamyaran, #Sanandaj, #Saghez (`le clip <https://cdn.masto.host/mastheadsocial/media_attachments/files/109/539/879/938/578/154/original/b4b0900d66fdfcfd.mp4>`_)
et #IvanGharb sont en grève.

La grève se fait alors que le régime des mollahs en #Iran a mis des
scellés à beaucoup de magasins pendant les grèves précédentes.

#Mahsa_Amini
