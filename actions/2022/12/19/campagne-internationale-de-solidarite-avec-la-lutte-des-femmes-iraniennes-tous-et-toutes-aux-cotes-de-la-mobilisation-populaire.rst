.. index::
   ! CNT-SO

.. _cnt_so_2022_12_19:

==================================================================================================================================================================
Lundi 19 décembre 2022 **Campagne internationale de solidarité avec la lutte des femmes iraniennes ! tous et toutes aux côtés de la mobilisation populaire !**
==================================================================================================================================================================

- https://cnt-so.org/iran-solidarite-avec-la-lutte-des-femmes-iraniennes-tous-et-toutes-aux-cotes-de-la-mobilisation-populaire/
- https://cnt-so.org/wp-content/uploads/2022/12/Solidarite-Iran-Dec-22.pdf

:download:`Télécharger le document au format PDF <docs/solidarite_iran_2022_12_19.pdf>`


#CampagneInternationale #Solidarite #Solidariry
#Iranrevolution #StopExecutionsInIran #JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran



.. figure:: images/solidarite_iran.png
   :align: center

   https://cnt-so.org/wp-content/uploads/2022/12/cnt_so_soutien_iranien_nes.png

Introduction
==============

Débuté il y a plus de trois mois, le mouvement de révolte populaire en
Iran contre la dictature cléricale et son cortège de misère, continue
malgré une répression féroce du régime qui a fait des centaines de victimes
et provoqué l’incarcération de milliers d’autres dont certain-nes sont
condamné-es à mort et risquent l’exécution rapidement.

La CNT-SO salue la lutte exemplaire des femmes qui refusent la main-mise
de la dictature cléricale sur leurs corps et leurs vies et sont à l’avant-garde
du mouvement populaire qui déferle sur ce pays.

La CNT-SO appelle à la solidarité internationaliste avec ce mouvement
révolutionnaire en particulier face à la répression.

Les faits : Le 13 septembre 2022, Jina Mahsa Amini, originaire de Saghez,
dans le Kurdistan iranien, effectue en famille une visite de Téhéran.
Elle est alors arrêtée par la Police des Mœurs du régime, structure
chargée de la traque des déviants du prêt-à-porter politiquement correct.
Elle était suspecte car une mèche de ses cheveux dépassait de son voile.

Jina Mahsa Amini décède trois jours après son arrestation, le 16 septembre 2022,
des suites des coups et tortures subis durant son incarcération

La mort de Mahsa met le feu aux poudres
==========================================

Un mouvement de révolte, parti de Saghez, ville d’où était originaire Mahsa,
s’est immédiatement étendu, rapidement et durablement, à l’ensemble du pays.

Aux cris de «Femme, Vie, Liberté» les femmes sont alors descendues dans
la rue, brûlant leur hijab symbole d’une oppression, patriarcale et
cléricale, imposée par la République Islamiste.

La mobilisation s’est renforcée par l’entrée dans la lutte de la jeunesse étudiante,
des travailleur-euses, des chômeurs et chômeuses, couches sociales solidaires
de l’héroïque combat des femmes qui prônent conjointement le départ du
dictateur suprême et le renversement de son régime.

La contestation est également sociale, tant la situation matérielle de la
population est difficile et la dictature cléricale la garante d’un ordre
social inégalitaire.
Des mouvements de grèves ont eu lieu dans de nombreux secteurs. Dans la
rue des représentants des institutions religieuses sont moqués, disqualifiés.
Des commerçants baissent le rideau malgré les forces policières qui
tagguent leurs devantures, ceux qui ne suivent pas la grève sont boycottés
par la population.

Soutien contre la répression policière, judiciaire et étatique
=================================================================

Contre les femmes qui manifestent, contre les jeunes et les travailleur-euses,
l’État réprime : lacrymogènes et surtout tirs à balles réelles.

Les opposants évoquent 18000 arrestations, plus de 500 morts. Pour isoler
la résistance l’accès à internet est bloqué.
Si les exécutions ont lieu généralement dans les prisons, rappelons-nous
qu’en juillet 2022, un ouvrier a été pendu en place publique, ceci pour
intimider le peuple le dissuader de s’engager.

Le 12 décembre 2022, Majïdreza Rahnavard, 23 ans, pendu en place publique,
comme motif : « guerre contre Dieu et corruption sur terre ». 15 hommes
condamnés à mort attendent leur exécution.
Le régime aux abois tente d’intimider le peuple.

La Confédération CNT-SO apporte son soutien aux manifestants-es iranien-nes
==============================================================================

Notre Confédération exprime sa totale solidarité à la révolte des femmes
iraniennes, aux grèves qui s’organisent.
Nous faisons nôtre les revendications de la lutte du peuple iranien : le
refus de l’oppression des femmes par le pouvoir théocratique, la fin
de la dictature, la liberté d’expression, d’organisation, de grève, de manifestation.

EN IRAN COMME AILLEURS : NON À L’OPPRESSION, NON À L’EXPLOITATION !
VIVE LA LUTTE DES FEMMES ET DE LA CLASSE OUVRIÈRE !
SOLIDARITÉ INTERNATIONALE !


Campagne internationale de solidarité
=========================================

Ecrire:

En Iran, les autorités ont déjà exécuté deux manifestants.
26 personnes risquent de subir le même sort dans les jours à venir.
Ces exécutions doivent cesser et toute condamnation à mort doit être
annulée #StopExecution StopExecutionsInIran !

Responsable du pouvoir judiciaire,

Gholamhossein Mohseni Ejei c/o Ambassade d’Iran auprès de l’Union européenne,
Avenue Franklin Roosevelt No. 15, 1050 Bruxelles. Belgique.
Copie à : Ambassade de la Rép. Islamique d’Iran, 4 avenue d’Iéna -75116 Paris
