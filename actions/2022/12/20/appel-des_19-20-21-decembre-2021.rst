
.. _appel_2022_12_20:

======================================================================
Mardi 20 décembre 2022 **L'appel des 19 20 et 21 décembre 2022**
======================================================================



LE RAPPEUR Saman Yassin aurait fait une tentative de suicide
===============================================================

- https://nitter.manasiwibi.com/arminarefi/status/1605665825809219584#m

LE RAPPEUR Saman Yassin, condamné à mort en #Iran pour avoir soutenu les
manifestants, aurait fait une tentative de suicide mardi 20 décembre en
raison de ses piètres conditions de détention dans la prison Rajaeishahr
de Karaj, ville à l’ouest de Téhéran, selon @KurdistanHRN.


Meurtres de Hossein Saïdi, Modjahed Kourkour et Mahmoud Ahmadi
=================================================================

- https://masthead.social/@NaderTeyf/109548134009960971

Des combattants de la liberté de la ville d'#Izeh qui savaient être
poursuivis par la police politique des mollahs en #Iran se sont
réfugiés dans un village près de cette ville.

La police a finalement trouvé leur cachette. Elle a attaqué la maison
par des armes semi-lourdes de guerre et a tué 3 d'entre eux:
Hossein Saïdi, Modjahed Kourkour et Mahmoud Ahmadi.

Deux autres ont été arrêtés.
#Mahsa_Amini

Cette année est l'année du sang/Seyed Ali (Khamenei) est renversé
=====================================================================

- https://masthead.social/@NaderTeyf/109547980685807024

Deux manifestations sont signalées à #Machhad et #Ispahan.

Ici dans le quartier populaire Salsabil de #Téhéran le jeune précise la
date du jour et les autres scandent:

- "Mort au dictateur"
- et "Cette année est l'année du sang/Seyed Ali (Khamenei) est renversé."

#Mahsa_Amini


Le temps de la Révolution est arrivé/La voie est la grève;
=============================================================

- https://masthead.social/@NaderTeyf/109547609194579341

Les habitants de la #Cité_Ekbatan à #Téhéran sont encore en action ce
soir avec les slogans suivants:

- Le temps de la Révolution est arrivé/La voie est la grève;
- Protestations et grèves nous mènent à la Révolution;
- Pauvreté, corruption et vie chère/Nous irons jusqu'au renversement.

D'autres manifestations sont signalées dans les quartiers Sadeghieh,
Tehranpars et Park Way de Téhéran ainsi qu'à Gohardasht de #Karaj.


Grève totale à l'université de Racht
========================================

- https://masthead.social/@NaderTeyf/109545630481082935

Les Conseils syndicaux des universités d'#Iran ont publié ce `clip <https://cdn.masto.host/mastheadsocial/media_attachments/files/109/545/627/082/852/367/original/81545260b9dcdf73.mp4>`_
de l'université libre de #Racht où les étudiant.es sont en grève totale
aujourd'hui, mais un mollah est venu pour faire cours dans une salle vide!

#Mahsa_Amini


Des villes kurdes sont en grève
======================================

- https://masthead.social/@NaderTeyf/109545490270897430

Au deuxième jour, des villes kurdes de #Sanandaj, #Saghez (`le clip <https://cdn.masto.host/mastheadsocial/media_attachments/files/109/545/487/405/992/477/original/b4da869cb7a589ba.mp4>`_) et
#Kamyaran sont en grève. Une administration de contrôle du régime menace
les commerçants de fermer définitivement leurs magasins s'ils continuent
le mouvement.

#Mahsa_Amini
