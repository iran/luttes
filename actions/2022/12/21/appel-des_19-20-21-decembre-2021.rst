
.. _appel_2022_12_21:

======================================================================
Mercredi 21 décembre 2022 **L'appel des 19 20 et 21 décembre 2022**
======================================================================


La ville kurde de #Kamyaran est en grève
============================================

- https://masthead.social/@NaderTeyf/109551776809842792

Au troisième jour de l'appel à la grève et à la manifestation, la ville
kurde de #Kamyaran est en grève. Un rapport de netblocks.org montre que
le régime a pratiquement coupé Internet au Kurdistan et les informations
passent difficilement.

Les lycéennes de #Kermanchah et #Ahvaz (le clip) ont manifesté ce matin.

On entend dans cette vidéo:

- "Mort au dictateur",
- "Liberté, liberté, liberté"
- et "Mauvais et voyeur, c'est toi/Femme libre, c'est moi."

#Mahsa_Amini
