.. index::
   pair: Solidarité; Suisse

.. _solidarite_suisse_2022_12_21:

=======================================================================================================================
Mercredi 21 décembre 2022 **Contre la répression en Iran : Résolution de l’Assemblée des délégué-e-s CGAS (Genève)**
=======================================================================================================================

- http://www.cgas.ch/SPIP/spip.php?article3925

.. figure:: images/cgas_geneve.jpg
   :align: center

   http://www.cgas.ch/SPIP/?lang=fr

::

    Communauté genevoise d’action syndicale
    Organisation faitière regroupant l’ensemble des syndicats de la République et canton de Genève
    Rue des Terreaux-du-Temple 6 - 1201 Genève
    iban CH69 0900 0000 8541 2318 9
    info@cgas.ch


Contre la répression en Iran : Résolution de l’Assemblée des délégué-e-s CGAS
===============================================================================================

Depuis quatre mois, une large part des iranien.ne.s occupent les rue dans
l’ensemble du pays. Initié par des centaines de milliers de femmes, ce
mouvement entraîne aujourd’hui la majorité de la population de l’ Iran.

Il vise à se débarrasser d’un régime, celui des mollahs, autoritaire,
liberticide, assassin.

Durant l’année qui se termine, plus de 500 personnes ont été exécutées
en Iran.
Encore ces jours, des manifestantes et manifestants sont pendu.e.s en
public, sans procès, coupables d’avoir, par leurs déclarations, par leur
participation aux manifestations, osé défier le régime.

Après les femmes, après les étudiant.e.s, ce sont aujourd’hui les salariées
et les salariés qui se mettent en grève. Elles, ils, méritent toute la
solidarité du mouvement syndical à l’échelle mondiale.

En Suisse, le récent congrès de l’Union syndicale suisse a acclamé une
position que la CGAS fait sienne et qui :

- Affirme la solidarité totale du mouvement syndical avec les mouvements
  populaires en Iran et avec leurs revendications démocratiques,
  économiques et sociales ;
- Condamne le climat de terreur instauré par la répression -exercée
  notamment à coups d’exécutions sommaires- par les autorités iraniennes ;
- Exige du gouvernement fédéral :

    - l’expulsion du personnel diplomatique de la république islamique d’Iran et
    - la saisie des avoirs directs et indirects des responsables iraniens
      déposés en Suisse ;

- S’engage à fournir dans la mesure de ses possibilités de l’aide matérielle
  aux grévistes en Iran ;
- Appelle ses membres à participer aux moments de mobilisation sur la
  Place des Nations qu’organise régulièrement la communauté iranienne à Genève.

