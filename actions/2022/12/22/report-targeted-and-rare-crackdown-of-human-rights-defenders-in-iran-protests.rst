.. index::
   pair: Iran Human Rights; 2022-12-22

.. _iran_hr_2022_12_22:

================================================================================================================
2022-12-22 **Report: Targeted and Rare Crackdown of Human Rights Defenders in Iran Protests**  #IranProtest2022
================================================================================================================

- https://iranhr.net/en/articles/5664/
- https://iranhr.net/media/files/HRD_2022_Eng_2bMr713.pdf

Introduction
==============

Iran Human Rights (IHRNGO); December 22, 2022: Human rights defenders have
always been under repression by the Islamic Republic.

However, their targeted and systematic repression has dramatically increased
in recent years. In its 2019/2020 Human Rights Defenders report, Iran Human Rights
documented 54 human rights defenders which doubled in 2021 with more
than 100 human rights defenders.

This upward trend continued in 2022, reaching a peak with the current
report documenting 218 human rights defenders who have been arrested and
harassed in less than three months.
While the Islamic Republic is considered one the biggest repressive states
in the world, this report shows that the targeted crackdown on human rights
defenders, was unprecedented following the outbreak of nationwide protests,
even for the Islamic Republic.

Iran Human Rights Director, Mahmood Amiry-Moghaddam said: “Three months
have passed since the beginning of the nationwide protests in Iran.
Changing the political system is the core protest demand but the demands
of those who take to the streets at the risk to their lives, is even
greater than a change of system.

The people of Iran have stood up to claim all the rights millions have
been deprived of for decades. All protesters who took to the streets
are themselves human rights defenders according to the UN definition.

Human rights defenders play an important role in the **‘Woman, Life, Liberty’ revolution.”**


This report shows that the Islamic Republic’s response to any human rights
activism is violent crackdowns, and that the situation of human rights
defenders has significantly deteriorated compared to last year.

Many activists were unlawfully arrested “preemptively” at their homes
and locations other than protests.
Activists like Golrokh Irayi-Ebrahimi, Milad Fadayi, Saba Sherdoost and
Majid Tavakoli.

A significant number of defenders also suffer from illnesses. Arash Sadeghi
was arrested despite suffering from a rare form of cancer.
Rapper Toomaj Salehi is facing charges that carry the death penalty for
his protest songs.

There are also teachers who were arrested or faced reprisals for refusing
to hand over names of protesting/striking students and pupils, with one
losing her life due to a heart attack from the pressure.

Lawyers willing to represent protesters and other human rights defenders
have been particularly targeted at an unprecedented rate with at least
46 subjected arrests or legal action. 

Iran Human Rights calls on civil society worldwide to support their
colleagues who are fighting for their fundamental rights under such
difficult circumstances in Iran.

Artists, Bar Associations, workers’ unions, journalists, women’s activists
and others can help save their lives by being their voices.
We call on well-known figures to take coordinated action by each following
up on the situation of one of the people listed in this report, and to
be their voice in interviews and public appearances.

Many of the human rights defenders are behind bars in dangerous conditions;
international pressure and raising the political cost of the repression,
is the only way to reduce the pressure on them.

