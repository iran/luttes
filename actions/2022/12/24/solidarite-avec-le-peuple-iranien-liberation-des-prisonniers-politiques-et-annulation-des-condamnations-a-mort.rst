
.. index::
   pair: Rassemblement ; Samedi 24 décembre 2022

.. figure:: images/affiche_rassemblement.png
   :align: center
   :width: 300


.. _iran_grenoble_2022_12_24:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ 14e rassemblement samedi 24 décembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarité avec le peuple iranien, libération des prisonniers politiques et annulation des condamnations à mort** 📣
==============================================================================================================================================================================================================================================================

Jina Amini est morte le vendredi 16 septembre 2022 N°semaine:37 (2022)
ce samedi 24 décembre 2022 cela fait donc 3 mois 1 semaine et 1 jour que Jina a été assassiné
=> 99e jour = on est dans la 14e semaine de la révolution, N°semaine:50 (2022)


- :ref:`mahsa_jina_amini`
- :ref:`slogans`

Samedi 17 décembre 2022, 14e semaine de la révolution, 99e jour
#Grenoble #Iranrevolution #StopExecutionsInIran
#JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran #grenoble
#france #JinaAmini #ZanZendegiAzadi #AidaRostami
#azadi #freedom #revolution #زن_زندگی_آزادی #women #FemmeVieLiberte
#JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran #JinaAmini #ZanZendegiAzadi
#azadi #freedom #revolution #زن_زندگی_آزادی  #FemmeVieLiberte
#Solidarité #Kurdes #Kurdistan #Rojava #Syrie #Rojava #Turquie
#jinjiyanazadi  #revolution


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|
- :ref:`#KianPirfalak <kian_pirfalak>` |KianPirfalak|
- :ref:`#AidaRostami <aida_rostami>` |AidaRostami|


.. figure:: images/les_morts_avec_mahsa.png
   :align: center

   Les morts avec Jina Mahsa Amini


Texte annonçant le rassemblement du samedi 24 décembre 2022
==============================================================

Le mardi 13 septembre 2022 Jina Mahsa Amini était arrêtée par la police des mœurs
à Téhéran car elle n’était pas **"assez voilée"**.
Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

Nous demandons des gouvernements des pays occidentaux:

- d'arrêter tout contrat et de négociation avec le gouvernement islamique
  d'Iran,
- De mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort
- d'expulser les ambassadeurs et les représentants du gouvernent islamique
- de mettre sur la liste noire les acteurs des massacres et de confisquer
  leurs bien a l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyez leurs voix. les iraniens ont besoin d'un soutien réel et d'actions
réelles pour en finir avec ce régime sanguinaire; le blabla ne suffit pas.


**Nous irons jusqu’à la liberté vraie, humaine, cette fois-ci jusqu’au bout.**

**Cette manifestation est organisée par la communauté Iranienne**.



Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`petition_contre_condamnations_2022_11_11`


Quelques événements du 18 décembre au 23 décembre 2022
=============================================================================

- :ref:`iran_hr_2022_12_22`
- :ref:`appel_2022_12_21`
- :ref:`appel_2022_12_20`
- :ref:`appel_2022_12_19`
- :ref:`cnt_so_2022_12_19`

Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/
- https://www.ici-grenoble.org/evenement/rassemblement-de-solidarite-avec-le-peuple-iranien-pour-la-liberation-des-prisonniers-politiques-et-lannulation-des-condamnations-a-mort


.. figure:: images/annonce_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/rassemblement-de-solidarite-avec-le-peuple-iranien-pour-la-liberation-des-prisonniers-politiques-et-lannulation-des-condamnations-a-mort


Mastodon iranluttes
---------------------

.. figure:: images/annonce_mastodon.png
   :align: center

   https://kolektiva.social/@iranluttes/109552577493992511



Mobilizon
------------

- https://mobilizon.chapril.org/events/a21473ce-ec3c-4d3e-a991-82d64d06b2a4


.. figure:: images/annonce_mobilizon.png
   :align: center

   https://mobilizon.chapril.org/events/a21473ce-ec3c-4d3e-a991-82d64d06b2a4


Autres événements ce samedi 24 décembre 2022 à Grenoble
---------------------------------------------------------


.. figure:: images/evenements_ici_grenoble.png
   :align: center


Quelques photos
=================


.. figure:: images/stop_pendaison.jpeg
   :align: center
   :width: 400

.. figure:: images/paix_au_rojava.jpeg
   :align: center
   :width: 400


.. figure:: images/halte_a_la_sale_guerre.jpeg
   :align: center
   :width: 400

.. figure:: images/cnt_so_avec_les_iraniennes.jpeg
   :align: center
   :width: 400

Vidéos peertube
==================

Performance
-------------

- https://www.orion-hub.fr/w/22YWXocEmUFMM8izh4pKb9

.. figure:: images/fusion.jpeg
   :align: center
   :width: 400


.. figure:: images/fusion_emotion.jpeg
   :align: center
   :width: 400



Déclaration au nom de la ligue des Droits de l'Homme
------------------------------------------------------

- https://www.orion-hub.fr/w/eyf2NBH6AVYn6VbBmUottS


Quelques déclarations
--------------------------

- https://www.orion-hub.fr/w/5Pmtdb84zrQ3iFqVDtJwMf
- https://invidious.tiekoetter.com/watch?v=9TWy6rd4h2A


Manifestations précédentes à Grenoble
=======================================


- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


