.. index::
   ! Sara Khadem
   ! Atousa Pourkashyan

.. _sara_khadem_et_atousa_pourkashyan_2022_12_27:

============================================================================================================================
2022-12-27 **Sara Khadem et Atousa Pourkashyan se sont présentées hier au championnat du monde au Kazakhstan sans voile**
============================================================================================================================


#womanlifefreedom #sarakhadem #atousapourkashiyan #mahsaamini #iranrevolution


- https://piaille.fr/@FabM/109587177792465139


.. figure:: images/sara_khadem_et_atousa_pourkashyan.png
   :align: center


Sara Khadem et Atousa Pourkashyan, maîtres d’échecs iraniennes, se sont
présentées hier au championnat du monde au Kazakhstan sans voile.

Les femmes iraniennes sont obligées de le porter dans des contextes
internationaux en sport.
On le sait, les représailles peuvent être terribles.

Mais le regard de Sara Khadem raconte le courage plus que la peur, celui
que donne la certitude de faire ce que l’on doit.

