.. index::
   ! Mohammad Moradi

.. _mohammed_moradi_2022_12_27:
.. _mohammed_moradi:

===================================================
2022-12-27 **Suicide de Mohammad Moradi à Lyon**
===================================================

- https://www.francetvinfo.fr/monde/iran/manifestations/lyon-le-geste-desespere-d-un-refugie-iranien-contre-la-repression-du-regime_5568246.html
- https://www.francetvinfo.fr/monde/iran/manifestations/un-iranien-se-suicide-a-lyon-pour-alerter-sur-la-situation-dans-son-pays_5567037.html

.. figure:: images/mohammad_moradi.png
   :align: center


- https://kurdistan-au-feminin.fr/2022/12/28/lyon-suicide-kurde-iran/

LYON – Mohammad Moradi, un jeune Kurde d’Iran étudiant à Lyon, s’est suicidé
le 26 décembre en se jetant dans le Rhône en guise de protestation contre
la répression sanglante menée par les mollahs iraniens depuis le début
des protestations anti-régime. « La police attaque les gens [en Iran],
on a perdu beaucoup de fils […]

L’article LYON. Suicide: acte désespéré d’un Kurde d’Iran contre le
régime sanguinaire iranien est apparu en premier sur Kurdistan au féminin.

- https://kurdistan-au-feminin.fr/2022/12/28/lyon-suicide-acte-desespere-dun-kurde-diran-contre-le-regime-sanguinaire-iranien-iranrevolution-iranprotests-jinaamini-mahsaamini/
- https://twitter.com/KurdistanAu/status/1607877758020980737#m

LYON. Suicide: acte désespéré d'un Kurde d'Iran contre le régime sanguinaire
iranien #IranRevolution #IranProtests #JinaAmini #mahsaamini
kurdistan-au-feminin.fr/2022…

LYON - Mohammad Moradi, un jeune Kurde d'Iran étudiant à Lyon, s'est
suicidé le 26 décembre en se jetant dans le .

- https://piaille.fr/@FabM/109587177792465139

Un Iranien se suicide à Lyon pour alerter sur la situation dans son pays
==========================================================================

- https://www.francetvinfo.fr/monde/iran/manifestations/un-iranien-se-suicide-a-lyon-pour-alerter-sur-la-situation-dans-son-pays_5567037.html


Avant de se jeter dans le Rhône, cet homme de 38 ans avait publié une vidéo
sur les réseaux sociaux. Il y présentait son geste comme une preuve de
la "fatigue" de ses compatriotes qui manifestent contre le pouvoir depuis
plus de trois mois.

Quand vous regarderez cette vidéo, je serai mort." Un Iranien s'est suicidé,
lundi 26 décembre, en se jetant dans le Rhône, à Lyon, afin d'attirer
l'attention sur la situation dans son pays secoué par des manifestations
contre le pouvoir.
Il a été retrouvé noyé, lundi en fin de journée, et n'a pas pu être
réanimé par les pompiers, selon la police, confirmant une information du
journal local Le Progrès.

L'homme avait publié une vidéo, sur plusieurs réseaux sociaux, avant de
commettre son geste. "J'ai décidé de me suicider dans le fleuve Rhône,
c'est un challenge pour montrer que nous, peuple iranien, nous sommes
très fatigués de cette situation [en Iran]", y annonçait-il, d'une voix
calme.
"La police attaque les gens, on a perdu beaucoup de fils et de filles,
on doit faire quelque chose." Il appelait à soutenir le peuple iranien
dans sa lutte contre "des policiers et un gouvernement extrêmement violents".

"Son cœur battait pour l'Iran"
--------------------------------------

Selon plusieurs membres de la communauté iranienne, Mohammad Moradi était
étudiant en licence d'histoire et travaillait dans un restaurant.

Agé de 38 ans, il vivait à Lyon avec sa femme depuis trois ans.

"Son cœur battait pour l'Iran, il ne supportait plus ce régime", se désole
Timothée Amini, porte-parole de 3 000 membres de la communauté iranienne de Lyon.

Un rassemblement a eu lieu mardi sur les lieux du drame, pont Gallieni.
Devant de nombreux journalistes, une quarantaine de personnes ont déposé
bougies, bouquets de roses et photos du défunt sur les rambardes, avant
de prononcer discours et chants.

"On a droit tous les matins à l'Ukraine, mais l'Iran on n'en entend parler
que très peu. C'est difficile à vivre pour nous, Iraniens de la diaspora",
a déclaré Timothée Amini, accusant les médias occidentaux de ne pas
"propager" la voix des Iraniens et "de la révolution en Iran".


Lyon : le geste désespéré d'un réfugié iranien contre la répression du régime
=================================================================================

- https://www.francetvinfo.fr/monde/iran/manifestations/lyon-le-geste-desespere-d-un-refugie-iranien-contre-la-repression-du-regime_5568246.html

Un Iranien qui vivait à Lyon a mis fin à ses jours, lundi 26 décembre,
pour alerter le monde sur la situation de son pays. Il s'est expliqué
dans un message vidéo avant de se jeter dans le Rhône.

Son geste radical a ému au-delà de la communauté iranienne.

Avant de se suicider, lundi 26 décembre à Lyon (Rhône), Mohammad Moradi
a posté une vidéo. "Quand vous regardez cette vidéo, je ne serai plus
vivant. (…)
J'ai décidé de faire ça pour montrer à tout le monde que nous, Iraniens,
avons besoin d'aide", dit-il. Peu après, il se jetait dans les eaux
glaciales du Rhône.

Son corps a été retrouvé vers 19 heures. Les pompiers n'ont pas réussi
à le réanimer. 

Attirer l'attention du monde sur les manifestations en Iran 

Sa vidéo posthume résonne comme un testament. Il souhaitait attirer
l'attention du monde sur les manifestations dans son pays, et la sanglante
répression du régime.
"Nous, les Iraniennes, on a un sentiment de culpabilité sur nous, on vit
mal d'être en sécurité ici, [alors] que notre peuple, à l'intérieur de

l'Iran, ils sont devant les balles", confie Hadis, membre du collectif
"Femme, Vie, Liberté" à Lyon. Mohammad Moradi, 38 ans, vivait avec sa
femme à Lyon depuis trois ans. Il était étudiant en histoire.


A Lyon, le suicide de Mohammad Moradi bouleverse et mobilise la communauté iranienne
=========================================================================================

- https://www.lemonde.fr/societe/article/2023/01/01/a-lyon-le-suicide-de-mohammad-moradi-bouleverse-et-mobilise-la-communaute-iranienne_6156252_3224.html


Le geste désespéré de Mohammad Moradi, exilé qui s’est jeté dans le Rhône
le 26 décembre, fait écho à la solitude et la culpabilité ressenties par
les Iraniens en exil en France.

.. figure:: images/pont_gallieni.png
   :align: center


Un rassemblement est prévu en sa mémoire à Lyon le 8 janvier 2023.

Des fleurs et des bougies sont déposées sur le pont Gallieni , à Lyon,
le 30 décembre 2022, là où Mohammad Moradi s’est suicidé en se jettant
dans le Rhône, le 26 décembre 2022.


La vidéo, depuis lundi 26 décembre, passe de téléphone en téléphone.
On y voit le visage de Mohammad Moradi, filmé en légère contreplongée.
Assis sur la berge du Rhône, l’homme de 38 ans fixe l’objectif.
« J’ai choisi cette voie sans aucun stress, ce n’est pas triste »,
affirme-t-il, avant d’annoncer sa volonté de mettre fin à ses jours.

« Je décide de me suicider dans le fleuve Rhône », dit-il en montrant
par-dessus son épaule le cours d’eau qu’on devine entre les arbres.

Il s’enregistre en langue française, puis en persan.

En revendiquant son acte, Mohammad Moradi souhaite « attirer l’attention
des autorités et des peuples des pays occidentaux sur le problème de
l’Iran (…), je préfère mourir que d’être témoin de la vie misérable
du peuple iranien ».

