

.. _discours_2022_12_31:

==============================================================================================================================================================================================================================================================
Discours du 31 décembre 2022
==============================================================================================================================================================================================================================================================

Mohammad Moradi : **nous sommes très très fatigués de cette situation**

Cette semaine nous avons appris une triste nouvelle, Mohammad Moradi,
un étudiant iranien de 38 ans, engagé dans le mouvement révolutionnaire
de « Femme, Vie, Liberté » en Iran, s’est noyé dans le Rhône, lundi 26 décembre 2022.

Selon une vidéo mise en ligne avant son suicide, il revendique son acte
comme un acte protestataire ouvertement mis en scène au service d’une
cause, la cause de « Femme, Vie, Liberté » et pour attirer l'attention
du monde entier sur ce qui se passe en Iran.

Quand il a pensé que sa voix n'était pas suffisamment forte pour arriver
au monde entier, il a donné sa vie pour être la voix d'Iran.
C'est la République islamique qui est responsable de sa mort.
Nous présentons les sincères condoléances à sa famille, et ses amis.

Nous serons sa colère, sa voix . Nous continuerons son combat. Il sera
notre force. Il sera toujours avec nous dans cette guerre sans merci
avec l'obscurantisme. La route est longue mais la victoire certaine

Dis son nom Mohamad Moradi

Je me souviens de mon arrivée en France après avoir quitté l’Iran.
L’Isere était devenue ma passion, ma confidente. Je la regardais, je lui
parlais. Les néons rouges sur l’eau me faisaient penser au sang, et les
ombres dans l’eau aux morts qui descendraient doucement dans l’eau.
Une vision d’horreur d'un esprit torturé. C’était il y a 33 ans après
les massacres de 1989, et les assassinats des opposants à l'étranger.

A la fin de la guerre avec l'Irak, le gouvernement avait peur d'un
soulèvement populaire et avait besoin de faire une démonstration de force.
On parle de près de 5000 exécutions dans les prisons. Les prisonniers
qui n’acceptaient pas de revenir sur leurs convictions et faire des
aveux publics se faisaient exécutés et enterrés dans des fausses communes.
Nous les avons enterrés dans nos cœurs, les avons pleurés.

Ce n'était pas la première fois. Déjà en 82, pour instaurer la dictature,
plus de 2200 prisonniers ont été exécutés et en 4 ans 12500.

Pendant 10 ans, la République islamique a tué 80 intellectuels en Iran
et à l'étranger dans des circonstances criminelles. 

Puis en 2009 une révolte avec des centaines de morts, puis en 2016, puis
en 2018, 1500 morts en 3 trois jours, puis 2021, puis aujourd'hui.

Pendant que le monde pensait que la république islamique était notre choix
et notre tradition, nous nous sommes rebellés, nous nous sommes fait
arrêtés, torturés et exécutés et ca depuis depuis 43 ans.
Les mollahs ont confisqué la revolution de 1979.

Même si le monde ne l'a pas su ou n'a pas voulu le savoir, le peuple
iranien et les femme iraniennes, ont résisté et lutté depuis les débuts
de ce régime sanguinaire. 

Le mouvement révolutionnaire de ces trois derniers mois, n'est pas né
aujourd'hui. Il est le résultat de 43 ans de combat du peuple iranien
qui arrive à son point de non retour.

Aujourd'hui, avec 500 morts dont 70 enfants, plus de 18000 emprisonnés
et 2 exécutés et malgré la répression, les tortures et les viols
**ce mouvement ne s'essouffle pas**

Mais cette fois le monde entier a entend notre voix. La flamme de ce
mouvement qui a commencé depuis plus de trois, ne s'éteint pas. 

Cette semaine, un autre drame nous a frappé. Un de nos compatriotes à Lyon
a donné sa vie pour attirer l'attention du monde entier sur ce qui se passe
en Iran.
Il a fait de sa vie son arme de combat contre ce régime barbare.
Bien qu'attristé par cette nouvelle terrible, aujourd'hui nous tenons à
saluer sa mémoire. Nous tenons a prêter serment sur sa vie que nous
sècherons nos larmes et mettrons nos habits de combat pour être sa voix
et continuer son combat.
Pour que le monde entier connaisse le courage des iraniens et sache qu'is
sont dans ce combat sans merci pour en finir avec ce régime.   

- Pour demander au monde entier d'expulser les representants et les ambassadeurs iraniens,
- de mettre la pression sur le régime islamique
- pour annuler les exécutions et libérer les prisonniers politiques.
- D'arrêter toute relation diplomatique et tout contrat avec ce regime sanguinaire.

Cher Mohammad, nous vengerons ta mort.
