
.. index::
   pair: Rassemblement ; Samedi 31 décembre 2022


.. _iran_grenoble_2022_12_31:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ 15e rassemblement samedi 31 décembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarité avec le peuple iranien annulation des condamnations à mort, hommage à Mohammad Moradi** 📣
==============================================================================================================================================================================================================================================================


- :ref:`mahsa_jina_amini`
- :ref:`slogans`

#Grenoble #Iranrevolution #StopExecutionsInIran #MohammadMoradi
#JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran #grenoble
#womanlifefreedom #sarakhadem #atousapourkashiyan #mahsaamini #iranrevolution
#france #JinaAmini #ZanZendegiAzadi #AidaRostami
#azadi #freedom #revolution #زن_زندگی_آزادی #women #FemmeVieLiberte
#JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran #JinaAmini #ZanZendegiAzadi
#azadi #freedom #revolution #زن_زندگی_آزادی  #FemmeVieLiberte
#Solidarité #Kurdes #Kurdistan #Rojava #Syrie #Rojava #Turquie
#jinjiyanazadi  #revolution


.. figure:: images/annonce_rassemblement.png
   :align: center

.. figure:: images/mohammad_moradi_2.png
   :align: center
   :width: 300



- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|
- :ref:`#KianPirfalak <kian_pirfalak>` |KianPirfalak|
- :ref:`#AidaRostami <aida_rostami>` |AidaRostami|



Texte annonçant le rassemblement du samedi 31 décembre 2022
==============================================================

Le mardi 13 septembre 2022 Jina Mahsa Amini était arrêtée par la police des mœurs
à Téhéran car elle n’était pas **"assez voilée"**.
Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

Nous demandons des gouvernements des pays occidentaux:

- d'arrêter tout contrat et de négociation avec le gouvernement islamique
  d'Iran,
- De mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort
- d'expulser les ambassadeurs et les représentants du gouvernent islamique
- de mettre sur la liste noire les acteurs des massacres et de confisquer
  leurs bien a l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyez leurs voix. les iraniens ont besoin d'un soutien réel et d'actions
réelles pour en finir avec ce régime sanguinaire; le blabla ne suffit pas.


**Nous irons jusqu’à la liberté vraie, humaine, cette fois-ci jusqu’au bout.**

**Cette manifestation est organisée par la communauté Iranienne**.



Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`petition_contre_condamnations_2022_11_11`


Quelques événements du 25 décembre au 30 décembre 2022
=============================================================================

- :ref:`miryoussef_younesi_2022_12_28`
- :ref:`mohammed_moradi_2022_12_27`
- :ref:`sara_khadem_et_atousa_pourkashyan_2022_12_27`

Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/
- https://www.ici-grenoble.org/evenement/rassemblement-de-solidarite-avec-le-peuple-iranien-pour-la-liberation-des-prisonniers-politiques-et-lannulation-des-condamnations-a-mort

.. figure:: images/annonce_ici_grenoble.png
   :align: center

Mastodon iranluttes
---------------------

- https://kolektiva.social/@iranluttes/109601313123048273


.. figure:: images/mastodon_iranluttes.png
   :align: center


Mobilizon
------------

- https://mobilizon.chapril.org/events/2fc3a902-d0db-4950-af59-aa5eb607c3ea
- https://mobilizon.chapril.org/search?search=grenoble


.. figure:: images/mobilizon.png
   :align: center


Autres événements ce samedi 31 décembre 2022 à Grenoble
---------------------------------------------------------

.. figure:: images/autres_evenements.png
   :align: center

Quelques photos
=================


.. figure:: images/zoya.jpeg
   :align: center


.. figure:: images/stop_pendaison.jpeg
   :align: center

.. figure:: images/mohammad_moradi.jpeg
   :align: center



Vidéos peertube
==================

- https://www.orion-hub.fr/w/nGd2431pYjMAxQsz8MXPED (Discours en hommage à Mohammad Moradi)
- https://www.orion-hub.fr/w/qGQ1kxCvKmsPqoFPYoFD2Q (La chanson Barâyé de Shervin Hajipour (en Français : Pour …)
- https://www.orion-hub.fr/w/8TvPNcUtGF7GABZG1eZ5Ko (chanson 1 avec Z. et Pirooz)
- https://www.orion-hub.fr/w/8SFDQSPojNiZW7bvSoPpeH (chanson 2 avec Z. et Pirooz)

Hommage à nos camarades Kurdes assassinés à Paris le 23 décembre 2022
===============================================================================

- :ref:`kurdistan_luttes:ref_assassinat_kurdes_2022_12_23`
- :ref:`kurdistan_luttes:cnt_si_2022_12_29`

|AbdurrahmanKizil| :ref:`Abdurrahman Kizil <kurdistan_luttes:abdurrahman_kizil>` |EmineKara| :ref:`Evîn Goyî (Emine Kara) <emine_kara>`  |MirPerwer| :ref:`Mîr Perwer <mir_perwer>`

.. figure:: images/les_3_tuees.png
   :align: center

|EmineKara| :ref:`Emine Kara (Evîn Goyî ) <emine_kara>` était très impliquée dans la solidarité au soulèvement en Iran.


Manifestations précédentes à Grenoble
=======================================


- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


