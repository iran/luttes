
.. _greve_faim_2023_01_02:

====================================================================================
2023-01-02 **Greve de la faim de 15 femmes dans la prison de Katchoï (1er jour)**
====================================================================================


.. figure:: images/les_15_femmes.jpg
   :align: center


Introduction
=============


Depuis hier elles ne mangent rien et elles ne boivent rien.

Une grève de la faim incompatible avec l’état de santé de la plupart de
ces 15 femmes détenues à la prison de Katchoï. C’est pour réprimer le
mouvement #FemmeVieLiberté qu’elles sont emprisonnées.

Say their names👇🏼

#FatemehJamalpour #NiloufarKerdouni #FatemehNazarinejad #AnsiehMousavi
#JasmineHajmirza #NiloufarShakeri #ArmitaAbbasi #ElhamModaressi #FatemehHarbi
#SomayeMasoumi #HamidehZeraie #MarziehMirghasem #MaedehSohrabi #FatemehMosallah
#ShahrzadDerakhshan



#MaedehSohrabi
=================

- https://twitter.com/LettresTeheran/status/1612050370603409408#m

#MaedehSohrabi 23 ans fait parti des 15 détenues en grève de la faim à
la prison de Katchoï. Maedeh a été arrêtée le 18 octobre 2022, elle est en
détention provisoire à la prison de Katchoï depuis cette date


.. figure:: images/maedeh_sohrabi.png
   :align: center
