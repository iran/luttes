.. index::
   pair: Prix; Simone de Beauvoir 2022
   ! Simone-de-Beauvoir

.. _simone_de_beauvoir_2023_01_04:

======================================================================================================================================
2023-01-04 Le Prix Simone-de-Beauvoir pour la liberté des femmes 2023 est décerné aux femmes iraniennes, à la mémoire de Mahsa Amini
======================================================================================================================================

- https://www.prixsimonedebeauvoir.com/
- https://www.prixsimonedebeauvoir.com/IMG/pdf/201222-prixsdb-cp-23-sh-alw_sdeb_pb.pdf

:download:`Communiqué au format PDF <prixsdb-2023_01_09.pdf>`

.. figure:: images/page_sdb.png
   :align: center

   https://www.prixsimonedebeauvoir.com/

Introduction
===============

Le Prix Simone-de-Beauvoir pour la liberté des femmes 2023 est décerné
aux femmes iraniennes, à la mémoire de Mahsa Amini.

Il sera remis le lundi 9 janvier 2023, à 11h00, à la Maison de l’Amérique
latine, 217, boulevard Saint-Germain, Paris VII.

Le prix Simone de Beauvoir pour la liberté des femmes a été fondé par
Julia Kristeva en 2008, à l’issue du colloque international organisé à
Paris pour le centenaire de Simone de Beauvoir (9 janvier 1908- 14 avril 1986).

Julia Kristeva, la fondatrice, a présidé le prix de 2008 à 2011.
Josyane Savigneau lui succède, puis Sihem Habchi en 2017.
Depuis octobre 2019, Sylvie Le Bon de Beauvoir est présidente, et Pierre
Bras est Délégué général du jury.

Le prix, doté, est remis chaque année le 9 janvier, jour de la naissance
de Simone de Beauvoir.

Ce prix est décerné à une personne ou une association, à une œuvre ou une
action qui, partout dans le monde - dans tous les domaines, droit, travail,
éducation, recherche, littérature, vie quotidienne, militantisme... - défend
et fait progresser la liberté des femmes, jamais définitivement acquise.

Communiqué de presse
======================

- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)


« Femme, Vie, Liberté ! » Cet appel résonne dans les rues en Iran et à
travers le monde depuis la mort de Mahsa Amini, le 16 septembre 2022 à
Téhéran.

L’étudiante de 22 ans, originaire du Kurdistan, avait été arrêtée trois
jours plus tôt par la « police des mœurs » parce que quelques boucles
de cheveux s’échappaient de son voile.
Accusée par les policiers de « port de vêtements inappropriés », elle est
emprisonnée, rouée de coups, sombre dans le coma et meurt à l’hôpital.

La nouvelle de sa mort déclenche un mouvement insurrectionnel sans
équivalent. Des femmes enlèvent leur voile en public, se coupent les
cheveux, manifestent dans les rues.
Parti du refus de porter le voile, le mouvement de révolte des femmes
contre la dictature islamique est rejoint par les jeunes Iraniens à travers
tout le pays. Il ne recule pas malgré la terreur, les tortures, les viols,
les condamnations à mort et les pendaisons.

Quand à son arrivée au pouvoir en 1979 l’ayatollah Khomeiny avait rendu
le voile obligatoire, les Iraniennes étaient descendues par milliers dans
les rues.
Simone de Beauvoir avait soutenu leur révolte. En 2009, le prix Simone-de-Beauvoir
pour la liberté des femmes a été attribué, sur proposition de l’écrivaine
Chahla Chafiq, alors membre du jury, au collectif «One MillionSignatures »
qui avait lancé une pétition pour faire supprimer les lois discriminatoires
envers les femmes en Iran.
Il s’agissait d’une campagne audacieuse et originale pour « faire avancer l’égalité ».

La poétesse iranienne Simin Behbahani avait reçu le prix, à Paris, au nom
de ce Collectif

Chahla Chafiq, qui participe à cette remise du prix 2023, est l’autrice
du Rendez-vous iranien de Simone de Beauvoir (2019). Dans cet essai,
elle montre l’influence actuelle de la pensée de Simone de Beauvoir dans
la lutte des Iraniennes contre l’oppression culturelle et religieuse de
l’Etat islamique.

Le cri de révolte **Femme, Vie, Liberté !** est repris par des dizaines
de milliers de manifestantes et manifestants. La révolte des femmes
devient le combat de toute une génération pour la liberté : c’est
cette extension révolutionnaire que le jury soutient en attribuant son
**prix 2023 aux Iraniennes, à la mémoire de Mahsa Amini**.

Après la cérémonie, un DEBAT aura lieu à 18h **Femme, Vie, Liberté : Iran, une révolution existentielle ?**

Auditorium du Monde
67-69, avenue Pierre Mendès-France – 75013 Paris
Avec Virginie Larousse, journaliste au Monde, Chahla Chafiq, écrivaine
et sociologue, Fawzia Zouari, présidente du parlement des écrivaines
francophones, Farzaneh Milani, écrivaine et universitaire d’origine
iranienne et Sedef Ecer, romancière et dramaturge franco-turque.


