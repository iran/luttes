.. index::
   ! IFRI

.. _ifri_2023_01_05:

==================================================================================
Jeudi 5 janvier Fermeture de l'**Institut français de recherche en Iran (IFRI)**
==================================================================================


- https://nitter.manasiwibi.com/arminarefi/status/1610981741757628416#m

L'#Iran a décidé de fermer jeudi 5 janvier l'Institut français
de recherche en Iran(IFRI, spécialisé dans l'archéologie et les sciences
humaines) en réponse aux caricatures contre l'ayatollah Khamenei publiées
mercredi par @Charlie_Hebdo
