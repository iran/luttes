.. index::
   pair: CRIC ; Samedi 7 janvier 2023

.. _compte_rendus_grenoble_2023_01_07:

===============
Compte rendus
===============

.. toctree::
   :maxdepth: 3

   cric/cric
   travailleur_alpin/travailleur_alpin
   place-grenet/place-grenet
