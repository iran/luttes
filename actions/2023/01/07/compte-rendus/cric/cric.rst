
.. _compte_rendu_cric_2023_01_07:

============================
Compte rendu du CRIC
============================

- https://cric-grenoble.info/infos-locales/article/solidarite-avec-les-peuples-iranien-et-kurde-2746


.. figure:: ../../images/devant_eglise.png
   :align: center

Une centaine de personnes est rassemblée devant l'Église Saint-Louis, sur
la place Félix Poulat, mais ce n'est ni pour une messe, ni pour un mariage.

Dans la foule, on voit se côtoyer un drapeaux vert, blanc et rouge, un
autre rouge et noir, et une multitude de pancartes.
L'une d'elle représente une femme aux longs cheveux noirs flottant au vent.
Elle vient de les libérer en ôtant son voile, qu'elle tient dans les mains.

Il est vert, prend la forme d'une carte de l'Iran, et dessus sont écrits
les mots **Femme, vie, liberté**.

Sur les marches qui mènent à la grande porte de l'édifice, disposées autour
d'un panneau **Stop pendaison**, des pancartes montrent des photos de
visages, surmontées d'un titre : **Free Atrin Azarfar**. **Say her name, Asra Panahi**.

Des dizaines d'affiches, en hommage aux victimes de la répression en Iran.
Parmi elles, d'autres pancartes réclament justice pour les victimes du
vol PS752, abattu accidentellement par le régime iranien il y a trois ans.

Devant toutes ces images, face aux personnes rassemblées, une potence s'élève,
symbole macabre des exécutions qui se succèdent depuis le début des
manifestations en septembre 2022.
Des fleurs rouges ont été placées dans la boucle de la corde, et sur le
poteau, quatre portraits sont accrochés : les visages de deux iraniens
exécutés aujourd'hui (#MehdiKarami |MehdiKarami|  #MohammadHosseini |MohammadHosseini|, NDLR) ;
deux autres, arrêtés, qui attendent la sentence.

Enfin, au pied des marches, un slogan est écrit sur un carton : **#Sanandaj, coeur battant de l'Iran**.

.. figure:: ../../images/sanandaj.png
   :align: center


:ref:`Sanandaj <sanandaj>`, capitale du Kurdistan iranien (de la province du :ref:`Kurdistan <kurdistan>`, NDLR),
le simple nom de cette ville explique pourquoi ce rassemblement n'est
pas seulement celui des iranien·nes, mais aussi celui des kurdes.

C'est d'ailleurs de cela dont il est question au moment où j'arrive.
La :ref:`personne au micro (Maryvonne Mathéoud, présidente d’AIAK, NDLR) <discours_maryvonne_matheoud>`
rappelle que :ref:`Jina Mahsa Amini <jina_amini>`, la jeune femme assassinée
par la Police des Mœurs en septembre, et dont la mort a déclenché le
soulèvement toujours en cours, était Kurde.

Elle rappelle que le peuple Kurde subit depuis des décennies la répression
implacable du régime des Mollah Iraniens, mais aussi celle du régime
Turc d'Erdoĝan, qui mène une politique de la terreur dans son pays, et a
récemment mené de nouvelles attaques contre les Kurdes du Rojava, en Syrie.

Elle rend hommage aux :ref:`trois militant·es Kurdes assassiné·es <kurdistan_luttes:assassinats_2022_12_23>` à Paris en
décembre 2022, et aux :ref:`trois autres tués en 2013 <kurdistan_luttes:assassinats_2013_01_09>`, également à Paris, dénonce
la responsabilité de la Turquie dans ces massacres, et l'inaction du
gouvernement Français, à qui elle demande de reconnaître la dernière
tuerie comme étant un acte terroriste.

Les prises de parole s'enchaînent, entrecoupées de chants et de slogans :
**Femme, Vie, Liberté !**, **Démocratie pour l'Iran**, **Laïcité pour l'Iran**,
**Libérez les prisonniers politiques**, **Expulsez l'ambassadeur de l'Iran**,
**Mort aux dictateurs !**.

On dénonce à nouveau la dictature islamiste patriarcale et les exécutions
des opposant.es, on condamne à nouveau la complaisance des pays occidentaux,
on parle du :ref:`prix Simone de Beauvoir pour la liberté des femmes <simone_de_beauvoir_2023_01_04>`, décerné
aux iraniennes, on exprime son soutien à la communauté kurde, on défend
le projet **démocratique, laïc et féministe** du :ref:`Rojava <rojava_luttes:rojava_luttes>` ...

Les prises de parole se terminent avec une minute de silence, en hommage
aux deux iraniens exécutés aujourd'hui.

.. figure:: ../../images/stop_pendaison.png
   :align: center


Puis, pour conclure le rassemblement, un groupe de militant·es réalise
une performance chorégraphiée sur un chant de résistance perse, terminant
sur les trois mots répétés en boucle depuis septembre : :ref:`Zan, Zendegi, Azadi, Femme, vie liberté <jin_jiyan_azadi>`.

.. figure:: ../../images/performance.png
   :align: center


Puis la foule se disperse, la potence est rangée, les pancartes enlevées,
ne reste plus qu'un petit groupe d'Iranien.nes qui se réconfortent, se
prennent dans les bras.

Le tram B passe, des gens entrent dans l'église. Un livreur à vélo
s'engouffre dans la rue de Sault.

On reviendra samedi prochain.



