
===========================================================================================================================================
**Grenoble : une centaine de personnes rassemblées en solidarité avec les peuples iranien et kurde** par  ©Manuel Pavard – Place Gre’net
===========================================================================================================================================

- https://www.placegrenet.fr/2023/01/09/grenoble-une-centaine-de-personnes-rassemblees-en-solidarite-avec-les-peuples-iranien-et-kurde/589138
- https://www.placegrenet.fr/2013/06/14/iranoblois-entre-espoir-et-defaitisme/524319
- :ref:`comptes_rendus_2023_01_07`

Introduction
==============

REPORTAGE – Une centaine de personnes se sont réunies ce samedi 7 janvier
2023, rue Félix-Poulat, à Grenoble, en solidarité avec les peuples
iranien et kurde, à l’appel de la `communauté iranienne de Grenoble <https://www.placegrenet.fr/2013/06/14/iranoblois-entre-espoir-et-defaitisme/524319>`_ et
de l’Association iséroise des amis des Kurdes (Aiak).

Un rassemblement destiné à soutenir la révolte en cours en Iran et à rendre
hommage aux militants kurdes assassinés à Paris, en 2013 et le 23 décembre 2022.

Deux contextes différents mais une même répression féroce et, selon
elles, **une même passivité du gouvernement français face à Téhéran
et Ankara**.

C’est pour soutenir à la fois les peuples iranien et kurde qu’une centaine
de personnes se sont rassemblées ce samedi 7 janvier 2023, rue Félix-Poulat,
à l’appel de la communauté iranienne de Grenoble et de l’Association iséroise
des amis des Kurdes (Aiak).

Parmi les manifestants présents, des militants du Mouvement de la paix
et du NPA, mais aussi les adjoints grenoblois Emmanuel Carroz et Alan Confesson.

.. figure:: images/place_felix_poulat.png
   :align: center

Une centaine de personnes se sont rassemblées ce samedi 7 janvier 2023,
rue Félix-Poulat, à Grenoble, en soutien aux peuples iranien et kurde,
dans cette période de contestation en Iran et suite à l’assassinat de
militants kurdes à Paris.

Les mots d’ordre ?
====================

Les mots d’ordre ?

D’un côté, la solidarité avec le mouvement de contestation
en Iran et l’appel à la justice pour les victimes du vol PS 752, trois
ans après le crash, le 8 janvier 2020, du Boeing 737 abattu par des missiles
iraniens.

De l’autre, l’hommage aux militants kurdes assassinés à Paris, à dix ans
d’intervalle, en janvier 2013 et le 23 décembre 2022.
Des crimes derrière lesquels l’Aiak voit la main de la Turquie.

Entre 14 et 30 manifestants iraniens condamnés à mort Au pied des marches
de l’église Saint-Louis, les organisateurs ont donc évoqué tour à tour
ces différents évènements.

Entre chaque discours, la foule a scandé les trois mots « Femmes, vie, liberté »,
devenus le slogan de ralliement de la révolte en Iran.
Depuis la mort, le 16 septembre 2022, de l’étudiante Jina
Mahsa Amini – Kurde iranienne -, interpellée par la police des mœurs
car pas « assez voilée », des milliers de femmes et jeunes Iraniens
défilent quotidiennement dans les principales villes du pays.

Sur les marches de l’église, des affiches dénonçaient les condamnations
à mort par pendaison et les arrestations de manifestants, tandis que le
drapeau iranien arborait le slogan « Femmes, vie, liberté » de la
révolte en Iran.


.. figure:: images/zoreh.png
   :align: center


Les contestataires réclament la fin du voile obligatoire et, plus largement,
le renversement du régime islamique. Mais la répression est brutale.
Entre 14 000 et 19 000 personnes ont ainsi été arrêtées – puis souvent
torturées – suite aux manifestations.

Pire, « au moins 30 manifestants ont été condamnés à mort », d’après les
organisateurs du rassemblement grenoblois, d’autres sources évoquant 14
condamnés à la peine capitale.
Parmi eux, quatre ont été exécutés, dont deux ce samedi, par pendaison.


.. figure:: images/drapeau_ali.png
   :align: center

   Les manifestants grenoblois ont affiché les visages de jeunes Iraniens
   emprisonnés ou exécutés arbitrairement.


La diaspora iranienne suit cela avec anxiété, à la fois solidaire des
insurgés et inquiète pour ses proches restés au pays. C’est le cas de
Z., Iranienne et Grenobloise d’adoption, exilée en France depuis 33
ans.

::

    Internet est coupé, donc la relation est très difficile, mais
    on est toujours en contact,

raconte-t-elle.

    Un jeune de 22 ans de ma famille, lauréat des olympiades de mathématiques,
    a été arrêté il y a trois ans et, il y a quelques jours, c’est son père
    qui a été arrêté. 

    On se sent impuissant, éloigné, et aussi un peu coupable d’avoir une
    situation confortable alors qu’eux se font emprisonner ou tuer

Pour Z., ces évènements sont **très difficiles à vivre à distance. 
On se sent impuissant, éloigné, et aussi un peu coupable d’avoir
une situation confortable alors qu’eux se font emprisonner ou tuer**,
explique-t-elle.

Depuis le début de la révolte, elle participe à tous les rassemblements
de solidarité organisés à Grenoble.
Une mobilisation **importante**, selon elle, **pour que ça ne soit pas
oublié. Car, dès qu’on arrête de parler de quelque chose, c’est comme si ça avait
disparu.**

.. figure:: images/women_of_iran.png
   :align: center

   Les femmes sont au cœur du mouvement de contestation en Iran, réclamant la
   fin du voile obligatoire.


Mais l’autre objectif, c’est d’appeler les gouvernements des pays occidentaux – notamment français – à réagir.
**La simple condamnation de l’Iran et la simple solidarité avec le peuple
iranien ne suffisent pas**, assène Z.. **On leur demande de renvoyer
les ambassadeurs d’Iran, de déclarer les Pasdaran comme organisation
terroriste, d’arrêter tous les contrats et de couper toute relation
avec l’Iran**.

**Il faut que le secret-défense soit levé**
==============================================

Les manifestants rassemblés à Grenoble entendaient ainsi interpeller à double
titre les autorités françaises :

- sur l’Iran donc,
- mais également sur les deux crimes commis contre des militants kurdes.

Pour le premier, c’était en effet il y a dix ans, jour pour jour : dans
la nuit du 9 au 10 janvier 2013, trois membres du Parti des travailleurs
du Kurdistan (PKK) étaient exécutés de plusieurs balles dans la tête,
dans les locaux du Centre d’information du Kurdistan, à Paris.

/home/pvergain/iran/actions/2023/01/07/compte-rendus/place-grenet/images/en_face_de_leglise.png

.. figure:: images/en_face_de_leglise.png
   :align: center

   En soutien aux peuples iranien et kurde, les manifestants ont réclamé
   la justice pour les insurgés iraniens comme pour les militants kurdes assassinés
   à Paris.

Si le principal suspect, alors incarcéré, est décédé des suites
d’un cancer au cerveau en 2016, cinq semaines avant son procès, la communauté
kurde est convaincue de l’implication des services secrets turcs, également
fortement soupçonnés par la justice française.
Problème : le juge d’instruction n’a pas accès à certains documents des
services secrets français.

.. figure:: images/zoreh_et_maryvonne.png
   :align: center

   Maryvonne Mathéoud (veste colorée et rayée), co-présidente d’Aiak,
   voit la main des services secrets turcs derrière les triples assassinats
   de militants kurdes commis à dix ans d’intervalle, en janvier 2013
   et décembre 2022.

**Il y a à nouveau eu une enquête mais il faut que le secret-défense soit
levé pour que les avocats puissent avoir accès au dossier**, déplore
Maryvonne Mathéoud, co-présidente d’Aiak.

À première vue, l’affaire semble en revanche différente pour les trois
militants kurdes assassinés le 23 décembre 2022 à Paris.
William M., l’auteur présumé de l’attentat, a en effet déclaré avoir agi
pour un motif raciste. Mais les Kurdes, eux, y voient toujours la main
de la Turquie.

::

    On ne peut pas écarter le terrorisme d’État. Cet individu
    est certes raciste et xénophobe mais je pense qu’il était commandité
    par la Turquie.

**On ne peut pas écarter le terrorisme d’État**, estime Maryvonne Mathéoud.
**Cet individu est certes raciste et xénophobe mais je pense qu’il était
commandité**, ajoute-t-elle, évoquant des éléments **troublants**
dans le déroulé des faits.

**Il faut ouvrir une enquête pour déterminer où est la responsabilité et
s’il y a quelqu’un derrière**, affirme la militante.
Pour cette dernière, **le projet démocratique, laïc et féministe des Kurdes
au Rojava dérange beaucoup Erdogan**.

Le président turc et la France ont en outre **signé, en 2011, un accord
de lutte contre le terrorisme. Ce qui fait qu’en France, des Kurdes sont
arrêtés et renvoyés en Turquie où ils sont souvent torturés, alors
qu’ils sont réfugiés politiques ici**, dénonce Maryvonne Mathéoud.


.. figure:: images/sheherazade.png
   :align: center

   Une jeune militante d’origine iranienne a demandé justice pour les
   victimes du vol PS 752, ce Boeing ukrainien abattu par erreur par deux
   missiles iraniens près de Téhéran, le 8 janvier 2020.

Après la lecture d’un manifeste des proches des victimes du vol PS 752 
par une militante iranienne exilée, les manifestants
grenoblois ont diffusé des chants iraniens et kurdes, et repris en chœur,
à plusieurs reprises, le slogan **Femmes, vie, liberté**.

Avant de se donner rendez-vous un prochain samedi. Car tous le promettent : tant
que la répression se poursuivra en Iran et tant que l’enquête n’avancera
pas pour les militants kurdes, ils resteront mobilisés.
