
.. index::
   pair: Rassemblement ; Samedi 7 janvier 2023


.. _iran_grenoble_2023_01_07:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ 16e rassemblement samedi 7 janvier 2023 à Grenoble **place Félix Poulat** à 14h30 **Solidarité avec le peuple iranien; Femme Vie Liberté; En mémoire des victimes du vol PS752**
==============================================================================================================================================================================================================================================================

Solidarité avec le peuple iranien annulation des condamnations à mort 📣
113e jour depuis la mort de Jina Mahsa Amini (16 septembre 2022)


- :ref:`mahsa_jina_amini`
- :ref:`slogans`


#JinaAmini #MahsaAmini #Grenoble #Iranrevolution #StopExecutionsInIran #PS752

.. figure:: images/annonce_rassemblement.jpeg
   :align: center


.. figure:: images/bonne_annee_2023.png
   :align: center
   :width: 300


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|


Texte annonçant le rassemblement du samedi 7 janvier 2023
==============================================================

Le mardi 13 septembre 2022 Jina Mahsa Amini était arrêtée par la police des mœurs
à Téhéran car elle n’était pas **"assez voilée"**.
Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

Nous demandons des gouvernements des pays occidentaux:

- d'arrêter tout contrat et de négociation avec le gouvernement islamique
  d'Iran,
- De mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort
- d'expulser les ambassadeurs et les représentants du gouvernent islamique
- de mettre sur la liste noire les acteurs des massacres et de confisquer
  leurs bien a l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyez leurs voix. les iraniens ont besoin d'un soutien réel et d'actions
réelles pour en finir avec ce régime sanguinaire; le blabla ne suffit pas.


**Nous irons jusqu’à la liberté vraie, humaine, cette fois-ci jusqu’au bout.**

**Cette manifestation est organisée par la communauté Iranienne**.


Hommage aux camarades Kurdes assassinées le 9 janvier 2013 et le 23 décembre 2022
======================================================================================

- :ref:`cnt_si_2022_12_29`

- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷

.. figure:: images/les_3_tuees_2022_avec_les_noms.png
   :align: center

   :ref:`kurdistan_luttes:assassinats_2022_12_23`


.. figure:: images/les_3_tuees_2013_avec_les_noms.png
   :align: center

   :ref:`kurdistan_luttes:assassinats_2013_01_09`


Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters

Les martyrs exécutés
----------------------

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|

4 manifestants sont déjà pendus depuis le début du soulèvement.

Des dizaines sont condamnés à mort. Le dictateur Ali Khamenei a dit dans
un récent discours à propos des manifestants : **Il faut les marquer au fer rouge**.
Ses sbires s’exécutent.

.. figure:: images/pendus_par_ali_khamenei.png
   :align: center
   :width: 300


.. _ps752_2023_01_07:

PS752
========

- https://linktr.ee/ps752justice
- https://www.ps752justice.com/
- https://www.instagram.com/ps752justice/

- https://masthead.social/@NaderTeyf/109644236958947732


.. figure:: images/ps752.jpg
   :align: center
   :width: 300

De nombreux appels aux manifestations ont été distribués ces derniers jours
partout en #Iran pour le 8 janvier.

**Les Pasdaran ont abattu par deux missiles, le 8 janvier 2020, le vol #PS752,
allant de Téhéran à Kiev et ont tué les 176 passagers**.

Ce soir une action de rue s'est tenue à #Sanandaj et les gens ont scandé
contre le régime depuis chez eux dans plusieurs quartiers de #Téhéran comme
sur le boulevard Ferdos, l'avenue Amirabad ou encore à la cité Ekbatan.

#Mahsa_Amini
#Femme_Vie_Liberté


.. _hommage_hosseini_karami_2023_01_07:

Hommage à #SeyedMohammadHosseini #MohammadMehdiKarami sauvagement exécutés par le gouvernement voyou d'Iran
===============================================================================================================

#MehdiKarami |MehdiKarami|


- :ref:`executions_iran_2023_01_07`

.. figure:: ../../../../repression/executions/2023/01/07/mohammad-mehdi-karami/images/mohammad_mehdi_karami.png
   :align: center
   :width: 200

   :ref:`mehdi_karami`


#MohammadHosseini |MohammadHosseini|

.. figure:: ../../../../repression/executions/2023/01/07/seyed-mohammad_hosseini/images/seyed_mohammad_hosseini_2.png
   :align: center
   :width: 200

   :ref:`mohammad_hosseini`


Discours
============

- :ref:`solidarite_suisse_2022_12_21`
- :ref:`simone_de_beauvoir_2023_01_04`

Quelques photos
=================

.. figure:: images/maryvonne_matheoud.jpeg
   :align: center

.. figure:: images/elus.jpeg
   :align: center

.. figure:: images/vue_foule.jpeg
   :align: center

.. figure:: images/jo_briant.jpeg
   :align: center


.. figure:: images/performance.png
   :align: center

.. figure:: images/stop_pendaison.png
   :align: center


.. figure:: images/sanandaj.png
   :align: center


.. figure:: images/devant_eglise.png
   :align: center


Vidéos peertube
==================


Discours de Z. sur le prix Simone de Beauvoir 2022
---------------------------------------------------------

- https://www.orion-hub.fr/w/83KeQdokNddb2zdWjsaLBD

Discours de S.
---------------------------------------------------------

- https://www.orion-hub.fr/w/vXRxrtu3FRXWtQYaq8VxAq


.. _discours_maryvonne_matheoud:

Discours de Maryvonne Mathéoud, présidente d’AIAK
----------------------------------------------------

- :ref:`aiak:hommage_aux_6_martyrs_kurdes_2023_01_07`



.. _comptes_rendus_2023_01_07:

Compte-rendus (CRIC, Travailleur Alpin, Place-Grenet) du samedi 7 janvier 2023
================================================================================

.. toctree::
   :maxdepth: 3

   compte-rendus/compte-rendus

Quelques événements du 1er janvier au 7 janvier 2023
=============================================================================

- :ref:`greve_faim_2023_01_07`
- :ref:`mohammad_mehdi_karami_2023_01_07`
- :ref:`mohammad_hosseini_2023_01_07`
- :ref:`ifri_2023_01_05`
- :ref:`simone_de_beauvoir_2023_01_04`
- :ref:`greve_faim_2023_01_02`

Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/
- https://www.ici-grenoble.org/evenement/rassemblement-de-soutien-au-peuple-iranien-en-memoire-des-victimes-du-vol-ps752-et-au-peuple-kurde-suite-aux-attentats-de-paris

.. figure:: images/annonce_ici_grenoble.jpeg
   :align: center


Mastodon iranluttes
---------------------

- https://kolektiva.social/@iranluttes/109638332572207655



Mobilizon
------------

- https://mobilizon.chapril.org/events/2fc3a902-d0db-4950-af59-aa5eb607c3ea
- https://mobilizon.chapril.org/search?search=grenoble


.. figure:: images/annonce_mobilizon.jpeg
   :align: center


Autres événements ce samedi 7 janvier 2023 à Grenoble
---------------------------------------------------------

.. figure:: images/evenements_grenoble_2023_01_07.png
   :align: center

   https://www.ici-grenoble.org/agenda



Manifestations précédentes à Grenoble
=======================================


- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


