
.. _greve_faim_2023_01_07:

===================================================================================
2023-01-07 **Greve de la faim de 15 femmes dans la prison de Katchoï (6e jour)**
===================================================================================


.. figure:: ../02/images/les_15_femmes.jpg
   :align: center


Elles sont arrêtées, torturées, accusées et condamnées car « une femme
qui n’a pas peur des hommes, leur fait peur ».

6ème jour de la grève de la faim pour les 15 filles de la prison de Katchoï.

Leur revendication : La libération sans condition de toutes. #FemmeVieLiberté


Say their names👇🏼

#FatemehJamalpour #NiloufarKerdouni #FatemehNazarinejad #AnsiehMousavi
#JasmineHajmirza #NiloufarShakeri #ArmitaAbbasi #ElhamModaressi #FatemehHarbi
#SomayeMasoumi #HamidehZeraie #MarziehMirghasem #MaedehSohrabi #FatemehMosallah
#ShahrzadDerakhshan
