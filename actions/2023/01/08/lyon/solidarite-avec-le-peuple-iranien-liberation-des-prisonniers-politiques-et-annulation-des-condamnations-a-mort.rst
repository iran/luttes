
.. index::
   pair: Rassemblement ; Dimanche 8 janvier 2023
   pair: PS752 ; https://www.ps752justice.com/


.. _iran_lyon_2023_01_08:

==============================================================================================================================================================================================================================================================
Manifestation à Lyon dimanche 8 janvier 2023 à Lyon de 14h à 17h
==============================================================================================================================================================================================================================================================

- :ref:`femmevieliberte_wordpress`
- https://www.ps752justice.com/
- https://www.instagram.com/ps752justice/

.. figure:: images/justice_prevail.png
   :align: center


.. figure:: images/ps752.png
   :align: center

   https://www.ps752justice.com/

- :ref:`mahsa_jina_amini`
- :ref:`slogans`


**Manifestation nationale en soutien au peuple iranien.**

En hommage aux victimes du vol Ps752 et à la mémoire de Mohammad Moradi.

Lyon, France
De la place des Terreaux à la place Bellecour
Dimanche 8 janvier 2023 à la place Bellecour de 14 à 17h.


#Lyon #Iranrevolution #StopExecutionsInIran #MohammadMoradi
#JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran #grenoble
#womanlifefreedom #sarakhadem #atousapourkashiyan #mahsaamini #iranrevolution
#france #JinaAmini #ZanZendegiAzadi #AidaRostami
#azadi #freedom #revolution #زن_زندگی_آزادی #women #FemmeVieLiberte
#JinaAmini #MahsaAmini #MahsaJinaAmini #IranProtests #iran #JinaAmini #ZanZendegiAzadi
#azadi #freedom #revolution #زن_زندگی_آزادی  #FemmeVieLiberte
#Solidarité #Kurdes #Kurdistan #Rojava #Syrie #Rojava #Turquie
#jinjiyanazadi  #revolution

Manifestation nationale, en soutien au peuple iranien.

En hommage aux victimes du vol Ps752 et à la mémoire de Mohammad Moradi.

Lyon, France
De la place des Terreaux à la place Bellecour
Dimanche 8 janvier 2023 à la place Bellecour de 14 à 17h.
