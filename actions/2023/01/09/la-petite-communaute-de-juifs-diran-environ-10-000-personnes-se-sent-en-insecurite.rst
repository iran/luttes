.. index::
   pair: Juives/Juifs ; Iran

.. _juives_juifs_iran_2023_01_09:

=======================================================================================================
2023-01-09 La petite communauté de juifs d’Iran (environ 10 000 personnes) se sent en insécurité
=======================================================================================================

- https://twitter.com/LettresTeheran/status/1612455734335111168#m

Le citoyen juif d’Iran, El Nathan Massih Isrealian, arrêté fin octobre
après le soulèvement populaire, a été libéré aujourd’hui sous caution
selon @hra_news.

La petite communauté de juifs d’Iran (environ 10 000 personnes) se sent
en insécurité suite à la répression du soulèvement populaire, étant donné
le passif très hostile du régime envers elle.

Dès le début du soulèvement le 22 septembre, l’association des juifs
d’Iran a demandé aux pratiquants de ne pas venir aux synagogues après
17h jusqu’à nouvel ordre.

.. figure:: images/synagogue_iran.png
   :align: center
