.. index::
   pair: Pape; 2023-09

.. _pape_francois_2023_01_09:

=============================================================================================================
2023-01-09 **Le pape François condamne l’Iran pour son recours à la peine de mort contre des manifestants**
=============================================================================================================

- https://csdhi.org/actualites/repression/39283-le-pape-francois-condamne-liran-pour-son-recours-a-la-peine-de-mort-contre-des-manifestants/
- :ref:`pope_francis_2023_01_09`

.. figure:: images/pape_francois.png
   :align: center

   https://fr.wikipedia.org/wiki/Fran%C3%A7ois_(pape)



CSDHI – Le pape François a condamné pour la première fois, lundi 9 janvier 2023,
**la peine de mort** et l’exécution de manifestants par l’Iran dans son
traditionnel discours du Nouvel An aux diplomates.

Il a déclaré que la **guerre en Ukraine** était « un crime contre dieu et **l’Humanité.»**

Le souverain pontife a fait ces remarques dans un discours aux diplomates
accrédités auprès du Vatican, son aperçu au début de la nouvelle année
qui est désormais connu de manière informelle comme son discours sur
**«l’état du monde»**.

Son discours de huit pages en italien, lu devant les représentants de la
plupart des 183 pays accrédités auprès du Vatican, a passé en revue toutes
les zones de conflit du monde, y compris celles d’Afrique, du Moyen-Orient
et d’Asie.

Toutefois, la principale nouveauté du discours prononcé dans la salle
des Bénédictions du Vatican a été **la rupture du silence sur les troubles
qui secouent l’Iran depuis la mort, en septembre dernier, d’une Iranienne
kurde de 22 ans, Mahsa Amini, en garde à vue**.

**«Le droit à la vie est également menacé là où la peine de mort continue
d’être imposée, comme c’est le cas ces jours-ci en Iran, à la suite des
récentes manifestations réclamant un plus grand respect de la dignité
des femmes»**, a-t-il ajouté.

:ref:`Quatre manifestants ont été exécutés <executions>` dans le cadre de la vague de protestations
populaires dans la République islamique.

**«La peine de mort ne peut être employée pour une prétendue justice d’État,
car elle ne constitue pas un moyen de dissuasion et ne rend pas justice
aux victimes, mais ne fait qu’alimenter la soif de vengeance»**, a déclaré François.

**Il a ensuite réitéré son appel à mettre fin à la peine de mort dans le
monde entier**, affirmant qu’elle est « toujours inadmissible car elle
porte atteinte à l’inviolabilité et à la dignité de la personne».

Le pape François a déclaré que de nombreux pays ne respectaient que du bout des
lèvres les engagements qu’ils avaient pris en matière de droits humains
et il a **appelé au respect des femmes, affirmant qu’elles étaient encore
largement considérées comme des citoyens de second rang, soumises à la
violence et aux abus.**

**« Il est inacceptable qu’une partie d’un peuple soit exclue de l’éducation,
comme cela arrive aux femmes afghanes »**, a-t-il déclaré.

François a évoqué « la guerre en Ukraine, avec son cortège de morts et
de destructions, avec ses attaques contre les infrastructures civiles
qui font que des vies sont perdues non seulement par des tirs et des
actes de violence, mais aussi par la faim et le froid glacial ».

Il a ensuite immédiatement cité une constitution du Vatican, affirmant
que «tout acte de guerre visant à la destruction aveugle de villes
entières ou de vastes zones avec leurs habitants est un crime contre
dieu et **l’Humanité** qui **mérite une condamnation ferme et sans équivoque.»**

Faisant référence à la crise des missiles de Cuba en 1962, il a déclaré :
« Malheureusement, aujourd’hui aussi, la menace nucléaire est élevée,
et le monde ressent à nouveau la peur et l’angoisse. »

Le pape a réitéré son appel à une interdiction totale des armes nucléaires,
**affirmant que même leur possession pour des raisons de dissuasion est «immorale.»**

Source : VOA

