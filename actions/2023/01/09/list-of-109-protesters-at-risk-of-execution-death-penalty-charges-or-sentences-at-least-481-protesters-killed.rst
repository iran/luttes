
.. _list_protesters_2023_01_09:

=====================================================================================================================================
2023-01-09 **List of 109 Protesters at Risk of Execution, Death Penalty Charges or Sentences; At Least 481 Protesters Killed**
=====================================================================================================================================

- https://iranhr.net/en/articles/5687/
- :ref:`morts_kurdes_2023_01_11`

Iran Human Rights (IHRNGO); January 9, 2023:


.. figure:: ../../../../images/logo_ihrngo.png
   :align: center


**At least 481 people including 64 children and 35 women**, have been killed
by security forces in the current nationwide protests.

The death toll increase relates to recently verified cases from the first
two months of the protests.

Furthermore, **at least 109 protesters are currently at risk of execution,
death penalty charges or sentences**.

This is a minimum as most families are under pressure to stay quiet,
**the real number is believed to be much higher**.  

Publishing this report, Iran Human Rights draws Iranian civil society
and the international community’s attention to the **intensification of
repression through arbitrary arrests, physical torture, sexual assault
and rape in detention and the mass issuance of sentences**.

There have been enough cases reported throughout the country to conclude
that it is not merely isolated incidents but a **systematic policy by the
government**. 

It also expresses its serious concern about the widespread arrests taking
place in Sistan and Baluchistan province which according to official
reports, has reached 100 people.
Many of those arrested were branded as “illegal aliens” and as many
Baluch citizens are deprived of having birth certificates, they are
subject to double the government oppression.

Director Mahmood Amiry Moghaddam said: “Many lesser known protesters,
especially in Sistan and Baluchistan province are under torture and at
risk of death penalty sentences.
**Saving their lives through mass campaigns and international pressure,
must be one the main priorities**.

New punitive measures should be taken against repressive organisations
like the :term`IRGC` and the Office of Khamenei in response to the
recent executions, along with increasing protests and campaigns inside
and outside the country, which can be effective steps to save their lives.”

According to information obtained by Iran Human Rights, at least 481
people including 64 children have been killed by security forces in the
nationwide protests so far.

**Of the 64 children, nine were girls**.

They were all under 18 years of age, but have not all been verified
through document evidence. Iran Human Rights is working to obtain confirmation
of their ages.

The aforementioned numbers only relate to protests on the streets.

The protesters executed and those that have died under suspicious circumstances
(include alleged suicides) shortly after release, are not included in
these statistics.

Death Toll by Province
===============================

Protesters have been killed in 25 provinces, with the most reported in
Sistan and Baluchistan, Western Azerbaijan, Kurdistan, Tehran and Mazandaran
respectively.

Deaths have been recorded in 25 provinces:

- Sistan and Baluchistan:  131 people;
- Tehran: 56 people;
- West Azerbaijan: 54 people;
- Kurdistan province: 53 people;
- Mazandaran: 39 people;
- Gilan: 25 people;
- Kermanshah: 24 people;
- Alborz: 24 people;
- Isfahan: 14 people;
- Khuzestan: 12 people;
- Fars: 12 people;
- Khorasan-Razavi: 7 people;
- East Azerbaijan: 4 people;
- Zanjan: 3 people;
- Lorestan: 3 people;
- Markazi: 3 people;
- Qazvin: 3 people;
- Hamedan: 2 people;
- Kohgiluyeh and Boyer Ahmad: 2 people;
- Ardabil: 2 people;
- Ilam: 2 people;
- Bushehr: 2 people;
- Hormozgan: 2 person;
- Semnan: 1 person;
- Golestan: 1 person.

Crucial to note: Iran Human Rights started researching and recording
protester deaths from the outset of protests.

In the course of research and as new information is obtained, the data
will be adjusted accordingly.
As such, it is reflected in the change in province numbers.

The most number of deaths were recorded on 21, 22 and 30 September
(**Baluchistan’s “Bloody Friday”**).

**November 4 was the bloodiest day in November with 21 recorded deaths**.

Numbers are a “minimum”

The death toll is an absolute minimum.

Reports of protester killings in the last few days are still being
investigated.

Iran Human Rights has received a high volume of reports of deaths which
it continues to investigate with security considerations and internet
disruptions. The actual number of people killed therefore, is certainly higher.

The Islamic Republic is intentionally creating confusion in protester
cases by sharing contradicting statements, particularly in death penalty
cases.

This is further perpetuated by the fact that security defendants do not
have access to their own lawyers per the Note to Article 48 of the CCP
and their family contact is restricted.

Many lawyers have reported being prevented from accessing their clients
and their cases at all stages of the legal proceedings.


PROTESTERS AT RISK OF EXECUTION, DEATH PENALTY CHARGES OR SENTENCES
=======================================================================

This list includes both officially reported cases and those reported by
family members and citizen journalists.

Iran Human Rights has been unable to confirm the details of all individual
cases and therefore requests that anyone with information about protesters
at risk of death penalty charges or sentences contact us on mail@iranhr.net
or 0061478494849 on Signal, WhatsApp or Telegram.

Since the December 27 list, nine people released on bail, one who is no
longer in custody and two who were executed, have been removed.
21 other protesters who are at risk of death penalty charges, sentences
or execution have been added to the current list. 

 
Protesters at risk according to province:

- Khuzestan: 24 people;
- Sistan and Baluchistan: 22 people;
- Tehran: 20 people;
- Mazandaran: 10 people;
- Isfahan: 8 people;
- West Azerbaijan: 7 people;
- Gilan: 5 people;
- Alborz: 3 people;
- Fars: 3 people;
- East Azerbaijan: 2 people;
- Chahar Mahal and Bakhtiari: 2 people;
- Kermanshah: 1 people;
- Kurdistan province: 1 people;
- Razavi Khorasan: 1 person.

It is important to note that all defendants in the following cases have
been deprived of the right to access their own lawyer, due process and
fair trials.

In cases where they have had managed to make contact or details of
their cases reported by cellmates and human rights defenders, **all have
been subjected to physical and mental torture to force false self-incriminating
confessions**.

In many cases, their forced confessions were aired prior to the commencement
of any legal proceedings, violating their right to be presumed innocent
until proven guilty.

The forced confessions are used as a method of proving guilt at show
trials.
There is ambiguity in many cases due to the Islamic Republic’s lack of
transparency.

Those no longer at risk have been removed from the list with updates
added at the end of the report.

Mohmmad Mehdi Karami #MehdiKarami |MehdiKarami|  and Mohammad Hosseini #MohammadHosseini |MohammadHosseini|
were executed on January 7. 



 
