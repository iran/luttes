.. index::
   pair: Pape ; Déclaration


.. _pope_francis_2023_01_09:

=======================================================================================================================
2023-01-09 Pope Francis condemned Iran for using death penalty against protesters demanding greater respect for women
=======================================================================================================================

.. figure:: images/pope_francis.png
   :align: center

Pope Francis condemned Iran for using death penalty against protesters
demanding greater respect for women.

"Death penalty can't be employed for a purported state justice, since
it doesn't constitute a deterrent nor render justice to victims, but
only fuels thirst for vengeance."


- https://www.usnews.com/news/world/articles/2023-01-09/pope-francis-condemns-iran-for-using-death-penalty-against-demonstrators

VATICAN CITY (Reuters) -Pope Francis on Monday condemned Iran for using
the death penalty against demonstrators demanding greater respect for women.

The pope's remarks, made in his yearly speech to diplomats accredited to
the Vatican, were his strongest since the start of nationwide protests
in Iran following the death last September of 22-year-old Kurdish Iranian
woman Mahsa Amini in police custody.

"The right to life is also threatened in those places where the death
penalty continues to be imposed, as is the case in these days in Iran,
following the recent demonstrations demanding greater respect for the
dignity of women," Francis said.

"The death penalty cannot be employed for a purported state justice,
since it does not constitute a deterrent nor render justice to victims,
but only fuels the thirst for vengeance," he said.

He then repeated an appeal for an end to the death penalty worldwide,
saying it is " always inadmissible since it attacks the inviolability
and the dignity of the person".

.. figure:: images/extrait_youtube.png
   :align: center

   https://youtu.be/Lpc8EvnFMSs

- https://www.reuters.com/world/europe/pope-says-wars-like-that-ukraine-are-crime-against-god-humanity-2023-01-09/

VATICAN CITY, Jan 9 (Reuters) - Pope Francis condemned Iran's execution
of protesters for the first time on Monday in his traditional New Year's
address to diplomats, and said the war in Ukraine was "a crime against
God and humanity".

The pontiff made his remarks in a speech to diplomats accredited to the
Vatican, his overview at the start of the new year which has come to
be known informally as his "state of the world" address.

His eight-page speech in Italian, read to representatives of most of the
183 countries accredited to the Vatican, ran the gamut of all the world's
conflict areas, including those in Africa, the Middle East and Asia.

the main novelty of the speech in the Vatican's Hall of Benedictions
was his breaking of silence on the nationwide unrest in Iran since the
death last September of 22-year-old Kurdish Iranian woman Mahsa Amini
in police custody.

"The right to life is also threatened in those places where the death
penalty continues to be imposed, as is the case in these days in Iran,
following the recent demonstrations demanding greater respect for the
dignity of women," he said.

Four protesters have been executed in connection with the wave of popular
protests in the Islamic Republic.

"The death penalty cannot be employed for a purported state justice,
since it does not constitute a deterrent nor render justice to victims,
but only fuels the thirst for vengeance," Francis said.

He then repeated an appeal for an end to capital punishment worldwide,
saying it is "always inadmissible since it attacks the inviolability
and the dignity of the person".

Francis said many countries were paying lip service to commitments they
had made to respect human rights and he called for respect for women,
saying they were still widely being deemed second-glass citizens,
subjected to violence and abuse.

"It is unacceptable that part of a people should be excluded from education,
as is happening to Afghan women," he said.

Francis spoke of the "war in Ukraine, with its wake of death and destruction,
with its attacks on civil infrastructures that cause lives to be lost not
only from gunfire and acts of violence, but also from hunger and
freezing cold".

He then immediately quoted from a Vatican constitution, saying "every
act of war directed to the indiscriminate destruction of whole cities
or vast areas with their inhabitants is a crime against God and humanity
which merits firm and unequivocal condemnation".

Referring to the Cuban Missile Crisis in 1962, he said: "Sadly, today too,
the nuclear threat is raised, and the world once more feels fear and anguish."
