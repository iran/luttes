.. index::
   pair: Soutiens ; Avocats
   pair: Justice; Iranjustice1401
   ! Iranjustice1401
   ! Avocats

.. _soutiens_avocats_2023_01_10:

=============================================================================================================================================================
2023-01-10 **Les avocats du monde entier, et aujourd’hui ceux du barreau de ROUEN, se tiennent aux côtés de leurs consœurs et de leurs confrères iraniens**
=============================================================================================================================================================

- https://nitter.manasiwibi.com/Iranjustice1401

#IRGCterorrists #FemmeVieLiberte #Mahsa_Amini #StopExecutionsInIran #IranRevolution


.. figure:: images/compte_twitter.png
   :align: center

   https://nitter.manasiwibi.com/Iranjustice1401

Annonce
===========

- https://nitter.manasiwibi.com/Iranjustice1401/status/1612823147731980289#m

Nous, avocats membres du collectif Iran Justice, appelons les Confrères
partout dans le monde à soutenir nos Confrères iraniens par une courte
vidéo individuelle, collective, ou une photo, pour nous tenir à leur cotés.

La profession d’avocat internationalement solidaire.

.. figure:: images/annonce_2023_01_10.png
   :align: center


- https://nitter.manasiwibi.com/Iranjustice1401/status/1613085683777949698#m

Nous, avocats, appelons les Confrères partout dans le monde à soutenir nos
Confrères iraniens par une courte vidéo individuelle, collective, ou une
photo, pour nous tenir à leur cotés.

@arminarefi @BFMTV


- https://nitter.manasiwibi.com/Iranjustice1401/status/1613176275731992576#m

En Iran, nos Confrères sont empêchés d’exercer. Menacés, arrêtés, emprisonnés,
kidnappés, ils sont réduits au silence. La profession d'avocat internationalement solidaire.
#همبستگی_با_وکلای_ایرانی
#solidaritéaveclesavocatsiraniens


- https://nitter.manasiwibi.com/BarreauRouen/status/1613951157457621001#m

Aujourd’hui le Barreau de Rouen s’est réuni, sous l’égide du Bâtonnier
Patrick Mouchet, dans la cour d’honneur du Palais de Justice pour exprimer
notre solidarité avec nos confrères avocats iraniens et avec tout le
peuple d’Iran épris de justice et de liberté.


- https://nitter.manasiwibi.com/BarreauRouen/status/1613951192060629011#m


Les avocats du monde entier, et aujourd’hui ceux du barreau de ROUEN,
se tiennent aux côtés de leurs consœurs et de leurs confrères iraniens
et s'inclinent devant leur courage

.. figure:: images/avocats_rouen.png
   :align: center


Merci pour votre solidarité.
#IranRevoIution2023 @IRstopexecution @Azadi4iranParis @OrdreavocatsN @CNBarreaux

nitter.manasiwibi.com/Iranjustice1401/status/1613085683777949698#m

- https://nitter.manasiwibi.com/Iranjustice1401/status/1614154458866270208#m

Nous serons leur voix le 15 sur le parvis du Trocadéro.
#FemmeVieLiberte #Mahsa_Amini #StopExecutionsInIran
#IRGCterorrists
#همبستگی_با_وکلای_ایرانی
#solidaritéaveclesavocatsiraniens

.. figure:: ../15/images/annonce_2023_01_15.png
   :align: center


Je participerai Dimanche 15 janvier à #Paris au rassemblement de soutien
au peuple iranien. Aidons-le à réussir sa révolution, à gagner sa liberté !
#IranRevolution #StopExecutionInIran #IRGCterrorists #FemmeVieLiberte
@azijangravi @mamadporii

