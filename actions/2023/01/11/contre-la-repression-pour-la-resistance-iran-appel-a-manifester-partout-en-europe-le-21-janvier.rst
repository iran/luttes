

.. _appel_2023_01_11:

==============================================================================================================================================================================================================================================================
**Contre la répression, pour la résistance – Iran – Appel à manifester partout en Europe le 21 janvier 2023**
==============================================================================================================================================================================================================================================================


- https://serhildan.org/contre-la-repression-pour-la-resistance-iran-appel-a-manifester-partout-en-europe-le-21-janvier/


Partout en Europe le 21 janvier 2023, nous descendrons dans la rue pour
condamner les exécutions, la répression, la tyrannie et dire notre solidarité
avec la résistance des prisonnier.es et leurs familles, avec les condamné.es à mort
et le mouvement des mères qui demandent justice.

