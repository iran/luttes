
.. _morts_kurdes_2023_01_11:

================================================================================================
2023-01-11 **Details of 121 Kurdish people killed during Mahsa (Jina) Amini protests in Iran**
================================================================================================

- https://kurdistanhumanrights.org/en/details-of-121-kurdish-people-killed-during-mahsa-jina-amini-protests-in-iran/
- :ref:`list_protesters_2023_01_09`

.. figure:: images/121_morts.png
   :align: center


Details of 121 Kurdish people killed during Mahsa (Jina) Amini protests in Iran

According to Iran Human Rights, a total of 481 people have been killed
so far during the Mahsa (Jina) Amini protests across Iran, including in
Iran’s Kurdish region.

KHRN data of confirmed deaths of Kurdish people shows that 121 Kurds
have been killed by Iranian security forces during the protests.

**KHRN has verified this information via multiple sources**.

At least two sources have confirmed the deaths and the sources were
witnesses to the incidents, immediate family members or close family
relatives of the killed persons.

Most of those killed were protesters. Others were civilians who had
nothing to do with the protests. Victims include 11 children, and **eight
people who died under torture**.

50 people were killed in :ref:`West Azerbaijan Province <azerbaidjan_occidental>`
(18 in :ref:`Bukan <boukan>`, 14 in Mahabad, eight in Piranshahr, four in Orumiyeh, three
in Oshnavieh, three in Sardasht).

37 people were killed in :ref:`Kurdistan Province <kurdistan_province>` (18 in :ref:`Sanandaj <sanandaj>`, five in :ref:`Saqqez <saqqez>`,
four in Divandareh, three in Kamyaran, three in Dehgolan, two in Baneh,
two in Marivan).

23 people were killed in :ref:`Kermanshah Province <province_kermanchah>`
(nine in :ref:`Kermanshah <ville_kermanchah>`, eight in :ref:`Javanrud <ville_javanroud>`, three in Eslamabad-e-Gharb, one
in Qasr-e Shirin, one in Salas-e Babajani, one in Gilan-e-gharb).

Four people were killed in Tehran

Three people were killed in the city of :ref:`Karaj <karaj>` in Alborz Province.
One person was killed in the city of Ilam in Ilam Province.

One person was killed in Quchan of Razavi Khorasan Province. One person
was killed in the city of Qazvin of Qazvin Province.

One person was killed in Arak of Markazi Province.

As the protests grew inside Iran, the Islamic Revolutionary Guard Corps
(IRGC) launched three cross-border drone and missile attacks on the
headquarters of Iranian Kurdish armed opposition groups based in neighbouring
Iraqi Kurdistan Region.

The cross-border attacks on 28 September, 14 November and 21November hit
the bases of Democratic Party of Iranian Kurdistan (KDPI), Kurdistan Freedom Party (PAK)
and two Komala factions.

A total of 20 members of those parties and a baby died as a result of
attacks.

This report does not include details about the casualties in those
cross-border attacks.

