
.. _femmes_2023_01_12:

===========================================================================================================
2023-01-12 **La révolte féministe qui se déroule en Iran et ses conséquences pour les droits des femmes**
===========================================================================================================

- https://nitter.poast.org/Asalbagherigr/status/1613954286400016396#m
- https://nitter.poast.org/igg_geo/status/1613941709997576194#m
- https://open.spotify.com/episode/5Ymll2QpImnloIl7ttEfyf?si=52a6caa48a8d4d46&nd=1

Depuis le mois de septembre 2022, les différentes manifestations en Iran
ont été au cœur de l’actualité, sensibilisant la communauté internationale
aux importantes violations des droits des femmes ayant lieu au sein du
pays.

Bien que l’élection d’Ebrahim Raïssi à la présidence de l’État, ainsi
que la mise en place de ses mesures liberticides, aient été décrites
comme le point de départ de la révolte féministe en Iran, cet évènement
n’a fait qu’embraser la relation déjà très tendue entre la population
iranienne et le gouvernement iranien à la politique très conservatrice.

Afin d'explorer les spécificités de ces soulèvements sans précédents et
leurs conséquences structurelles, nous avons la chance d’avoir avec nous
aujourd’hui, Asal Bagheri, docteure en Sémiologie, spécialiste du
cinéma iranien et enseignante-chercheuse à l'université Cergy Paris.


