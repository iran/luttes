.. index::
   pair: IRGC ; Terrorist


.. _irgc_terrorist_2023_01_13:

=====================================================================================================
2023-01-13 **international consensus over designating Iran’s IRGC as a terrorist organization**
=====================================================================================================

- https://nitter.manasiwibi.com/IranIntl_En/status/1613985352754073624#m
- https://www.iranintl.com/en/202301137484

ICYMI: As international consensus over designating Iran’s :term:`IRGC` as a
terrorist organization is growing, over 100 members of the European
Parliament call for proscribing the Guards in its entirety.
