
.. index::
   pair: Rassemblement ; Samedi 14 janvier 2023


.. _iran_grenoble_2023_01_14:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ 17e rassemblement samedi 14 janvier 2023 à Grenoble **place Félix Poulat** à 14h30 **Arrêt des exécutions, annulation des condamnations à mort libération des prisonniers politiques, expulsion de l'ambassadeur d'iran** 📣
==============================================================================================================================================================================================================================================================

Arrêt des exécutions, annulation des condamnations à mort liberation des prisonniers politiques, expulsion de l'ambassadeur d'iran
120e jour depuis la mort de #JinaMahsaAmini (2022-09-16)

- :ref:`mahsa_jina_amini`
- :ref:`slogans`

.. figure:: images/annonce_rassemblement.png
   :align: center

- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|


Texte annonçant le rassemblement du samedi 14 janvier 2023
==============================================================

Le mardi 13 septembre 2022 Jina Mahsa Amini était arrêtée par la police des mœurs
à Téhéran car elle n’était pas **"assez voilée"**.
Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

Nous demandons des gouvernements des pays occidentaux:

- d'arrêter tout contrat et de négociation avec le gouvernement islamique
  d'Iran,
- De mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort
- d'expulser les ambassadeurs et les représentants du gouvernent islamique
- de mettre sur la liste noire les acteurs des massacres et de confisquer
  leurs bien a l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyez leurs voix. les iraniens ont besoin d'un soutien réel et d'actions
réelles pour en finir avec ce régime sanguinaire; le blabla ne suffit pas.


**Nous irons jusqu’à la liberté vraie, humaine, cette fois-ci jusqu’au bout.**

**Cette manifestation est organisée par la communauté Iranienne**.


Les tags de la semaine
========================

#IRGCterrorists Iranjustice1401 #SolidaritéAvecLesAvocatsIraniens

Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`soutiens_avocats_2023_01_10`

Les victimes de la barbarie des mollahs
--------------------------------------------

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|


Quelques événements du 8 janvier 2023 au 14 janvier 2023
=============================================================================

- :ref:`arabie_saoudite_2023_01_14`
- :ref:`irgc_terrorist_2023_01_13`
- :ref:`morts_kurdes_2023_01_11`
- :ref:`soutiens_avocats_2023_01_10`
- :ref:`pope_francis_2023_01_09`
- :ref:`pape_francois_2023_01_09`
- :ref:`juives_juifs_iran_2023_01_09`
- :ref:`list_protesters_2023_01_09`


Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/
- https://www.ici-grenoble.org/evenement/rassemblement-pour-larret-des-executions-en-iran-lannulation-des-condamnations-a-mort-la-liberation-des-prisonniers-politiques


.. figure:: images/annonce_ici_grenoble.png
   :align: center



Mastodon iranluttes
---------------------

- https://kolektiva.social/@iranluttes/109683323195263440



Mobilizon
------------

- https://mobilizon.chapril.org/events/f245d161-1b23-49a5-a616-523d4ff31054
- https://mobilizon.chapril.org/search?search=grenoble


Quelques photos
=================

.. figure:: images/stop_pendaison_2.jpeg
   :align: center


.. figure:: images/zoya_peinture.jpeg
   :align: center


.. figure:: images/marvarid.jpeg
   :align: center



Vidéos peertube
==================

- https://sepiasearch.org/search?search=grenoble+iran

Discours de Z.
--------------------

- https://www.orion-hub.fr/w/5xEYhhbxbgnwmQwLJE79WN


Discours de S.
--------------------------

- https://www.orion-hub.fr/w/3cc3PRNFTRC9XABCRh3T81


Poème lu par Z.
----------------------

- https://www.orion-hub.fr/w/9AvWsUZPigwfWEqqEesHFD

::

    Nazli les tulipes ont éclot
    Et le printemps sourit
    Derrière la fenêtre de la maison
    Le vieux jasmin fleurit


    N'hésite pas
    Ne te confronte pas
    A cette mort dans l'effroi
    Mieux vaut vivre que disparaître
    Surtout au printemps.

     Nazli parle Nazli
    Nazli ne parlait pas
    Nazli était une étoile
    Qui un moment,  ici
    A brillé puis est partie
    Elle a serré les dents de rage
    Fatiguée endolori, elle est partie

    Nazli parle Nazli
    Nazli ne parlait pas
    Nazli était une pensée
    Qui a fleuri pour annoncer
    Que l'hiver sera fini
    De l'obscurité elle sortie
    Dans le sang s'est effondré et partie


Annonce de la fin de la manifestation par S.
-------------------------------------------------------

- https://www.orion-hub.fr/w/73mb8f9GrH2DxvQVjUg99d


Autres événements ce samedi 14 janvier 2023 à Grenoble
---------------------------------------------------------

.. figure:: images/evenements_grenoble_2023_01_14.png
   :align: center

   https://www.ici-grenoble.org/agenda



Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


