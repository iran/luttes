
.. index::
   ! Cris de rage
   ! Rage


.. _cris_de_rage_2023_01_14:

===========================================================
2023-01-14 **Cris de rage contre le régime iranien**
===========================================================


- https://www.ledevoir.com/societe/777916/cris-de-rage-contre-le-regime-iranien

.. figure:: images/cris_de_rage.png
   :align: center


Les hurlements, rauques et déchirants, pulvérisent le calme de ce samedi
après-midi d’hiver.

Au pied des bureaux de Radio-Canada, à Montréal, une dizaine de manifestants
hurlent leur colère contre le régime iranien, dans une tentative de mobiliser
l’attention médiatique sur le sujet.

Pendant cinq longues minutes, les personnes présentes, d’origine iranienne
et majoritairement des femmes, lancent des cris de rage.

La scène est angoissante. L’une d’entre elles se prostre, agenouillée
sur un carton ; d’autres fondent en larme. Le titre de la performance ?
« La colère est intraduisible. »

« Nous sommes fâchés et nous crions. Il n’y a pas besoin de traduction
pour ça », articule Soorena Noorai. La voix saisie par l’émotion, les
larmes s’agglutinant au coin des yeux, le Canado-Iranien raconte comment
il s’est senti pendant les cinq minutes qu’a duré la performance, à
laquelle il a participé.

« C’était comme si j’entendais les prisonniers en Iran. »

Depuis le meurtre de Mahsa Amini au mois de septembre dernier, la colère
des Iraniens et Iraniennes est palpable. La jeune femme a été battue à
mort puisqu’elle ne portait pas son hidjab au goût de la police des moeurs
locale.
Depuis, des milliers de ses concitoyens se sont révoltés, parfois au péril de leur vie.

L’organisme Iran Human Rights estime le nombre de personnes tuées dans
la répression des manifestants à 458, tandis que l’Organisation des
Nations Unies parle de 14 000 arrestations.

« [Le changement] va passer uniquement par le renversement du régime »,
martèle Mina Favar, Montréalaise née et ayant grandi en Iran.
« On ne veut plus l’islam politique, continue-t-elle. Ce qui se passe,
c’est une révolution séculaire et féministe. » Comme quelques dizaines
d’autres personnes, la jeune femme a assisté à la courte performance,
à quelques mètres de l’entrée de la nouvelle Maison de Radio-Canada,
au coin des rues René-Lévesque et Papineau.

« Nous protestons contre la passivité des médias et des instances nationales
et internationales qui ne couvrent pas comme il faut la situation en Iran »,
explique l’un des organisateurs quant au choix du lieu pour tenir la
performance. Celui-ci a préféré préserver l’anonymat « pour des raisons de sécurité ».

Après quelques minutes hautes en émotion passées à chanter des slogans,
comme « Femme, Vie, Liberté », les manifestants se dispersent lentement
dans le froid hivernal.

« La colère n’a pas besoin de mots, poursuit l’organisateur.
On peut l’exprimer par le cri. »
