.. index::
   pair: Arabie Saoudite; Iran

.. _arabie_saoudite_2023_01_14:

=================================================================================================
2023-01-14 **L'Iran et l'Arabie saoudite s'apprêtent à rouvrir leurs ambassades respectives**
=================================================================================================


https://www.lapresse.ca/international/moyen-orient/2023-01-13/le-chef-de-la-diplomatie-iranienne-espere-une-normalisation-avec-l-arabie-saoudite.php
==========================================================================================================================================================

- https://www.lapresse.ca/international/moyen-orient/2023-01-13/le-chef-de-la-diplomatie-iranienne-espere-une-normalisation-avec-l-arabie-saoudite.php


(Beyrouth) Le ministre iranien des Affaires étrangères a exprimé l’espoir
d’une normalisation prochaine des relations de son pays avec l’Arabie saoudite,
lors d’une visite à Beyrouth au cours de laquelle il a rencontré le chef
du puissant Hezbollah pro-iranien.


Lors d’une conférence de presse, Hussein Amir-Abdollahian a également
salué le rapprochement en cours entre la Turquie et la Syrie, où il doit
est attendu samedi selon le quotidien progouvernemental syrien Al-Watan.

Un dialogue entre l’Iran chiite et l’Arabie saoudite sunnite avait été
amorcé à Bagdad, mais la dernière réunion remonte à avril 2022.

Le ministre iranien a brièvement rencontré son homologue saoudien en
marge d’une réunion régionale en Jordanie en décembre dernier.

« Nous accueillons favorablement un retour à des relations normales avec
l’Arabie saoudite, qui aboutirait à une réouverture des bureaux de
représentation ou des ambassades à Téhéran et Riyad, dans le cadre du
dialogue entre les deux pays qui doit se poursuivre », a déclaré M. Abdollahian.

Ses déclarations interviennent alors que l’Iran, secoué par une vague
de manifestations, accuse ses « ennemis » menés par les États-Unis,
d’attiser les protestations.

En novembre, Téhéran avait appelé Riyad à changer son comportement « inamical »
et menacé les voisins de l’Iran, dont l’Arabie saoudite, de représailles
contre toute tentative de déstabilisation du pays.

Les deux poids lourds de la région ont rompu leurs liens depuis 2016 et
soutiennent des parties rivales dans plusieurs conflits dans la région,
notamment au Yémen.
L’Iran a une influence prépondérante en Irak et au Liban et soutient
militairement et politiquement le régime en Syrie.

« Menaces » israéliennes
-----------------------------

Concernant ce pays, M. Abdollahian s’est déclaré « heureux du dialogue
en cours entre la Syrie et la Turquie » qui devrait « avoir des répercussions
positives dans l’intérêt des deux pays ».

Après une rupture de plus d’une décennie en raison du soulèvement en Syrie,
où la Turquie soutient les rebelles combattant le régime, les ministres
de la Défense des deux pays se sont rencontrés en décembre dernier et
ceux des Affaires étrangères doivent se retrouver prochainement à Moscou.

Le ministre iranien a par ailleurs rencontré le chef du Hezbollah Hassan
Nasrallah, qui apparait rarement en public, avec lequel il a évoqué la
formation d’un nouveau gouvernement en Israël, le plus à droite dans
l’histoire de ce pays dont le Hezbollah est l’ennemi juré.

Les deux hommes ont évoqué « les menaces découlant de la formation du
gouvernement des extrémistes et des corrompus » en Israël, selon un
communiqué du Hezbollah.

Le ministre s’est également entretenu avec des responsables libanais dont
le premier ministre Najib Mikati.

Il a renouvelé la proposition de l’Iran de fournir du fioul au Liban, ou
bien de « réhabiliter les centrales électriques » vétustes « ou d’en
construire de nouvelles », malgré les sanctions imposées à Téhéran.

Mais le ministre libanais des Affaires étrangères Abdallah Bouhabib a
évoqué au cours de la conférence de presse des « pressions extérieures
et d’obstacles » qui pourraient entraver l’offre iranienne, dans une
allusion aux États-Unis.


https://www.aa.com.tr/fr/monde/liran-et-larabie-saoudite-sappr%C3%AAtent-%C3%A0-rouvrir-leurs-ambassades-respectives-/2475083
================================================================================================================================

- https://www.aa.com.tr/fr/monde/liran-et-larabie-saoudite-sappr%C3%AAtent-%C3%A0-rouvrir-leurs-ambassades-respectives-/2475083

AA/ Téhéran

Un législateur iranien a révélé samedi, que Téhéran et Riyad s'apprêtaient
à rouvrir leurs ambassades respectives.

Jalil Rahimi Jahan-Abadi, membre de la commission de la sécurité nationale
et de la politique étrangère du Parlement iranien, a déclaré dans un tweet,
que les relations entre l'Iran et l'Arabie saoudite sont relancées et
que les ambassades des deux pays seront bientôt rouvertes.

"Cet évènement aura des impacts importants sur l'apaisement des tensions
régionales d'une part et sur la promotion de la cohésion du monde arabo-musulman
d'autre part", a-t-il ajouté.

L'Iran avait annoncé lundi, la tenue d'un nouveau cycle de négociations
avec l'Arabie saoudite, en Irak, dans le but de régler les différends
et rétablir les relations bilatérales.

Le porte-parole du ministère iranien des Affaires étrangères, Saeed Khatibzadeh,
a fait savoir lors d'un point de presse hebdomadaire, qu'un nouveau cycle
de pourparlers entre l'Iran et l'Arabie saoudite, organisé par l'Irak,
était à l'ordre du jour.

"Nous essayons de poursuivre nos négociations et de préserver la stabilité
de nos liens malgré les dossiers litigieux entre les deux pays", a-t-il
affirmé, selon l'Agence de presse officielle iranienne (IRNA).

Le ministre saoudien des Affaires étrangères, Faisal bin Farhan, avait
révélé en octobre dernier, que son pays avait tenu un quatrième cycle
de négociations exploratoires et directes avec l'Iran, en date du 21 septembre 2021.

L'Irak a été le premier à révéler la tenue de discussions directes entre
Téhéran et Riyad, le président irakien, Barham Salih, ayant annoncé en
mai dernier, que son pays avait accueilli plus d'un cycle de dialogue
entre l'Arabie saoudite et l'Iran.

Les relations entre l'Iran et l'Arabie saoudite sont rompues depuis le
3 janvier 2016. Cette rupture n'est pas la première, les deux pays avaient
rompu leurs relations diplomatiques de 1987 à 1991.
L'Arabie saoudite considère l'Iran comme un pays rival dans la région.

Traduit de l'arabe par Malèk Jomni





