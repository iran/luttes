
.. _paris_2023_01_15:

====================================================================================
Dimanche 15 janvier 2023 RASSEMBLEMENT solidarité pour l'iran 🌹 Paris Trocadero
====================================================================================

- :ref:`soutiens_avocats_2023_01_10`

.. figure:: images/annonce_2023_01_15.png
   :align: center
