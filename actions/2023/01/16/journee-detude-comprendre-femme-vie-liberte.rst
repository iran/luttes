.. index::
   pair: Etude; Journée d’étude Comprendre "Femme, Vie, Liberté

.. _etude_2023_01_16:

=================================================================================================================================================
2023-01-16 **Journée d’étude Comprendre "Femme, Vie, Liberté"**
=================================================================================================================================================


Description
============

Journée d’étude Comprendre « Femme, Vie, Liberté ». 4 mois après. Paris,
undi 16 janvier 2023.

Coordination : Azadeh Thiriez-Arjangi (CRAL/EHESS).

Un événement du Centre de recherches sur les arts et le langage (CRAL-EHESS/CNRS)
et du Fonds Ricœur.

PRÉSENTATION
===============

4 mois après la mort de Mahsa Amini et le début de la révolution en cours
en Iran, une expertise académique semble plus que jamais nécessaire.
La journée d’étude : « Comprendre ‘femme, vie, liberté’ 4 mois après »
a pour objectif de proposer un regard interdisciplinaire sur le sujet.

Le but est d’établir un lien d’échanges non seulement entre les différentes
disciplines scientifiques mais aussi entre le monde académique et le
milieu politique.

Les contributions des différents chercheurs vont embrasser différents
domaines comme la philosophie politique et morale, l’économie-politique,
la sociologie, l’histoire, la culture, l’art et le cinéma iranien.

Cette journée d’étude sera ainsi le haut lieu du croisement et du «
 conflit » des différentes interprétations de la tragédie iranienne.

Les dialogues qui seront menés apporteront le déchiffrage de cette
révolution, ses racines, ses actualités, sa brutalité et sa violence.

Ils seront un incontestable outil de réflexion, afin de commenter au
mieux cette situation. Laissons les problématiques posées par les
chercheurs nous aider à resituer le contexte politique de la révolution
« Femme, Vie, Liberté » et ce malgré la distance spatiale consentie.

INTERVENANTS ET INTERVENANTES UNIVERSITAIRES
====================================================

- Olivier Abel : Professeur de philosophie et d'éthique à la faculté de
  théologie protestante de Montpellier, membre du CRAL
- Asal Bagheri : Enseignante-chercheuse à l’université Cergy Paris Université),
  sémiologue et spécialiste du cinéma iranien
- Hamit Bozarslan : Directeur d’étude à l’EHESS, historien et politologue
  spécialiste du Moyen-Orient, de la Turquie et de la question kurde
- Farhad Khosrowkhavar : Sociologue et directeur d'études émérite à l'EHESS
- Marie Ladier-Fouladi : Directrice de recherche au CNRS et au Centre
  d’études turques, ottomanes, balkaniques et centrasiatiques (CETOBaC)
  à l’EHESS
- Chowra Makaremi : Chercheuse en anthropologie à l’Institut de recherche
  interdisciplinaire sur les enjeux sociaux. Sciences sociales, politique,
  santé au CNRS
- Azadeh Thiriez-Arjangi : Enseignante de philosophie politique et morale
  à l’I.E.P. de Rennes et chercheuse associée au CRAL (EHESS)
- Vahid Yücesoy : Doctorant à l’Université de Montréal

PROGRAMME
==============

Les mots de bienvenus – Introduction (9h-9h30)
------------------------------------------------

Azadeh Thiriez-Arjangi : « La république islamique : une schizophrénie
politico-religieuse »

Panel I : Art et culture (9h40-11h10)
--------------------------------------

- https://soundcloud.com/user-897145586/republique-islamique?in=user-897145586/sets/femme-vie-liberte&utm_source=clipboard&utm_medium=text&utm_campaign=social_sharing

Asal Bagheri : « L’évolution de la représentation de la femme dans le
cinéma indépendant postrévolutionnaire : prémices du mouvement « Femme, Vie, Liberté » ? »

Panel II : Velayet-e-Faghih : un système d'abus du pouvoir (11h20-13h)
--------------------------------------------------------------------------

- Olivier Abel : « Les libertés comme résistance aux abus de pouvoir et
  comme orientation constitutive du politique »
- Hamit Bozarslan : « L'automne du patriarche : usure du velayet-e faqih en Iran ? »

Panel III : Automne 2022 et économie politique (14h-15h30)
---------------------------------------------------------------

- Chowra Makaremi : « Subjectivation révolutionnaire et expérience insurrectionnelle de l'automne 2022 en Iran »
- Vahid Yücesoy : « Pourquoi les iraniens tentent de renverser le régime ? Un regard tiré de l’économie politique »

Panel IV : Jeunesse et sécularisation par le bas (15h45-17h30)
-------------------------------------------------------------------

- Farhad Khosrowkhavar : « la sécularisation par le bas des classes moyennes iraniennes »
- Marie Ladier-Fouladi : « La génération « Femme, Vie, Liberté » »

Avec la participation de Maud Petit (Députée du Val de Marne), Francois
Bechieau (Maire-Adjoint du 19ème arrondissement de Paris), Geneviève Garrigos (Conseillère de Paris).


