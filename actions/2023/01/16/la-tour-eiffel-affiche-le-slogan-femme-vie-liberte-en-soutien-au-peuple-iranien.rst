.. index::
   pair: Tour Eiffel ; Lundi 16 janvier 2023

.. _tour_eiffel_2023_01_16:

=================================================================================================================================================
2023-01-16 **La tour Eiffel affiche le slogan Femme Vie Liberté en soutien au peuple iranien pour le 4e mois de la mort de #JinaMasahAmini**
=================================================================================================================================================


:ref:`Jina Mahsa Amini <jina_amini>` est morte le vendredi 16 septembre 2022.

.. figure:: doc/tour_eiffel.png
   :align: center


.. figure:: images/femme_vie_liberte.png
   :align: center


.. figure:: images/stop_executions.png
   :align: center

- http://bird.trom.tf/AnouchToranian/status/1615102183762169856#m

Ce soir, la Tour #Eiffel s’illumine du slogan : #FemmeVieLiberté

A mes sœurs, à mes frères d’#Iran, nous vous adressons ce message :
votre lutte pour la liberté est notre lutte à tous.

#Paris est et restera à vos côtés.

#StopExecutionsInIran
#JinJiyanAzadi
