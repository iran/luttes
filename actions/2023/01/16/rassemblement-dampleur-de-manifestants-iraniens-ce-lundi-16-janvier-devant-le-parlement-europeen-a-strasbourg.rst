.. index::
   pair: Strasbourg ; Lundi 16 janvier 2023

.. _strasbourg_2023_01_16:

=================================================================================================================================================
2023-01-16 **RASSEMBLEMENT D’AMPLEUR de manifestants iraniens ce lundi 16 janvier devant le parlement européen à Strasbourg**
=================================================================================================================================================

- https://nitter.manasiwibi.com/arminarefi/status/1615033416323371009#m

RASSEMBLEMENT D’AMPLEUR de manifestants iraniens ce lundi 16 janvier 2023
devant le parlement européen à Strasbourg dans le but de pousset le
Conseil européen à placer les Gardiens de la révolution iraniens sur la
liste des organisations terroristes de l’UE


- https://www.mediapart.fr/journal/fil-dactualites/160123/strasbourg-12000-personnes-en-soutien-aux-manifestations-en-iran


.. figure:: images/dessin_strasbourg.jpg
   :align: center


.. figure:: images/dessin_strasbourg_shah_d_iran.jpg
   :align: center

