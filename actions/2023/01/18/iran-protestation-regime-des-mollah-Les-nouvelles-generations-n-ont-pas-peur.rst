
.. _point_du_2023_01_18:

============================================================================================================================================
2023-01-18 Iran "Les nouvelles générations font des choses que la mienne n’aurait pas imaginées, elles n’ont pas peur" par Rachel Knaebel
============================================================================================================================================

- https://basta.media/Iran-protestation-regime-des-mollah-Les-nouvelles-generations-n-ont-pas-peur


Quatre mois après le début d’un nouveau mouvement de protestation en Iran,
la répression est terrible.

Hawzhin Baghali, sociologue, vit en France depuis douze ans où elle est
active au sein d’un collectif d’opposant·e·s créé en septembre. Entretien.

L’Iran connaît depuis cet automne un nouveau vaste mouvement de protestation
contre le régime autoritaire et ultra-répressif en place depuis 1979.
Les mobilisations ont débuté à la suite de la mort en détention, le 16 septembre 2022,
de Jina Mahsa Amini, une jeune femme kurde arrêtée à cause d’un voile « mal »
porté selon les forces de l’ordre iraniennes.

Des mobilisations ont eu lieu dans une grande partie du pays. La répression
des autorités est encore une fois terrible. Des centaines de personnes
ont été tuées. Depuis décembre, les condamnations à morts se multiplient,
plusieurs manifestants ont été exécutés.

Hawzhin Baghali, sociologue, avait participé au mouvement de protestation
de 2009. En France depuis douze ans, elle est aujourd’hui active au sein
d’un collectif créé après l’assassinat de Jina Mahsa Amini nommé Roja
et composé d’Iranien·ne·s et de Kurdes opposé·e·s au régime. Entretien.

Basta! : Parvenez-vous à avoir des nouvelles de la situation en Iran ?

Hawzhin Baghali : C’est moins compliqué aujourd’hui qu’en 2019 [où il y
avait déjà eu un vaste mouvement de manifestations en Iran, violemment réprimé].

En 2019, internet avait été coupé complètement pendant plusieurs jours,
on n’avait plus aucune relation avec l’Iran. Aujourd’hui, on reste en
contact avec les gens là-bas.

On regarde les infos tout le temps, heure par heure, sur des chaînes
Telegram essentiellement, en kurde et en perse. Nous avons des contacts
avec nos familles, nos amis. Quand je lis une nouvelle sur une ville,
je demande à quelqu’un qui vient de cette ville si c’est bien vrai.

Jina Mahsa Amini, dont la mort a déclenché les protestations était kurde.
La région kurde d’Iran s’est particulièrement mobilisée depuis l’automne.
Est-ce nouveau ?

Le Kurdistan iranien a toujours été un front de résistance contre le régime.
Il était déjà un front de résistance contre la monarchie, et au début de
la révolution de 1979. Ensuite, le Kurdistan est devenu la dernière barricade
des forces de gauche contre le gouvernement.

Il y a toujours eu des échanges d’idées entre le Kurdistan d’Iran et le
Kurdistan d’Irak.

Depuis 2014-2015, ce qui se passe au Rojava [région kurde dans le Nord-Est
de la Syrie, lieu d’une expérience émancipatrice depuis 2014 joue aussi
un grand rôle au Kurdistan d’Iran.

Quand les femmes iraniennes crient « Femmes, vie, liberté », c’est un
slogan qui vient du mouvement des femmes du Kurdistan turc, et qui est
devenu massivement connu parmi les kurdes d’Iran via le Rojava.
Quand on se retrouve à la marge, qu’on est opprimé, cela pousse à la
résistance. C’est ce qui joue, je pense, dans le fait que les Kurdes
n’ont pas lâché depuis 1979.


Les mobilisations se poursuivent-elles aujourd’hui ? Où sont-elles plus importantes ?

Comparé au début du mouvement, il y a moins de manifestations. C’est normal
après quatre mois. Lors du mouvement vert de 2009 [Vaste mouvement de
protestation après l’élection présidentielle de juin 2009 remportée par
Mahmoud Ahmadinejad], Téhéran était le centre des mobilisations.

J’étais à Téhéran à cette époque, j’étais étudiante et j’étais dans la
rue. C’était alors plutôt les classes moyennes qui étaient mobilisées.

À partir de 2017- 2019, il y a encore eu des soulèvements dans les quartiers
de la classe moyenne de Téhéran, mais ce n’était plus le centre des mobilisations.

Aujourd’hui, Téhéran n’est pas du tout le centre, même s’il y a aussi
des manifestations dans les quartiers de la classe moyenne du nord de
la ville comme dans les quartiers défavorisés du sud. Je dirais que le
centre du mouvement, c’est le Baloutchistan et le Kurdistan.

Au Kurdistan, la population était déjà organisée. Des mouvements quasi
clandestins sur la question des femmes, de l’écologie, pour la langue
kurde, existent depuis des années dans cette région. Il existe aussi
encore des partis politiques kurdes en exil.

Le Baloutchistan [dans le sud-est du pays, zone frontalière avec le Pakistan,
ndlr] est une région encore plus défavorisée que le Kurdistan.

Il y a au Baloutchistan des personnalités comme par exemple un imam de
la ville Zahedan, Molavi Abdolhamid, qui est devenu un leader de la
protestation. Chaque vendredi, il parle contre le régime et la population
descend dans la rue. Mais à côté, il y a aussi par exemple un groupe
de femmes baloutches qui s’est organisé, alors qu’il y a beaucoup de
répression contre les femmes dans la région, par le régime et par la
société et le système patriarcal. Cette fois, les femmes baloutches sont
aussi descendues dans la rue et parlent de leurs droits. Il y a encore
des manifestations chaque vendredi au Baloutchistan, et on voit y émerger
de plus en plus de slogans démocratiques qui parlent d’égalité des sexes
et d’égalité des peuples en Iran.

Comment s’organise l’opposition, dans la diaspora et en Iran ?

Après la révolution de 1979, Rouhollah Khomeini [l’ayatollah Khomeini,
au pouvoir en Iran de 1979 à sa mort en 1989] a pris le pouvoir et a
commencé à réprimer tous les autres groupes politiques, qui sont devenus
opposants au régime. Les forces de gauche ont alors quitté le pays.

Plus tard, les partis kurdes ont fait de même. Entre 1979 et 1982, il y
a eu une guerre entre le pouvoir central et le Kurdistan iranien.
Finalement, les forces kurdes sont parties en exil au Kurdistan d’Irak.
Elles sont devenues Peshmergas. Les Moudjahidin [réunis dans l’Organisation
des moudjahidines du peuple iranien] sont aussi partis.

Eux voulaient un marxisme musulman. Toutes ces forces qui ont quitté
l’Iran après 1979 sont devenues des opposants. Il y a eu aussi une vague
de migration des partisans de l’ancien régime monarchique, mais eux
sont partis individuellement.


Ceux qui sont restés en Iran étaient les libéraux nationalises islamistes,
comme le chef du gouvernement provisoire après la révolution.
Ce groupe était présent autour de Khomeini au début, puis Khomeini a
supprimé les libéraux nationaux aussi. Certains des membres de ce groupe
qui étaient restés avec le régime sont devenus l’un des groupes des réformistes.

À l’intérieur du pays, il n’y avait pendant longtemps plus que les réformistes
qui pouvaient encore former une sorte d’opposition. Mais à partir de 2010-2011,
après le mouvement vert, il y a eu aussi une vague d’émigration de réformistes.
À l’étranger, une partie des réformistes sont devenus monarchistes.
Ils ont des lobbys et beaucoup de fonds et œuvrent pour embellir l’image
de l’ancien régime monarchique.

Les Moudjahidin de leur côté restent très organisés, mais n’ont pas beaucoup
de possibilité de jouer un rôle dans le mouvement actuel car sont des
musulmans pratiquants. Les femmes Moudjahidin sont voilées.
La mobilisation actuelle qui s’oppose au port forcé du de voile ne peut
plus faire confiance à un groupe pratiquant.

Des discussions entre ces différents mouvements ont-elles lieu malgré
leurs divergences ?

D’un côté, il y a le groupe de ceux qui parlent de monarchie constitutionnelle
et de ceux qui se présentent comme républicains et parlent de démocratie
parlementaire.
De l’autre, il y a le groupe, actif à l’intérieur de l’Iran, des étudiants,
des femmes, certains partis politiques kurdes, qui parlent de supprimer
la répression contre les femmes, les populations LGBT et les minorités,
s’opposent à la centralisation du pouvoir, veulent trouver une solution
démocratique aux questions écologiques et aller vers une démocratie
directe appuyée sur les conseils de quartiers pour donner du pouvoir
au peuple. Ces deux discours existent en parallèle.

Le premier groupe a sa base en exil, le deuxième dans les universités et
au Kurdistan. Même si cela ne veut pas dire que tous les kurdes sont sur
cette ligne ou que tous les opposants en exil sont monarchistes ou républicains.
Existe-t-il un dialogue entre ces deux fronts ?
Jusqu’à maintenant, je dirais non.

Les républicains, monarchistes, libéraux, essaient de nouer des alliances,
mais cela passe en fait principalement par des célébrités : par Reza Pahlavi,
le fis de l’ancien shah, Masih Alinejad, qui se présente comme féministe
libérale, des actrices, un footballeur… Ce sont plutôt des stars sur les
réseaux sociaux.

« On demande de la solidarité de la part des sociétés, qu'elles dénoncent
la politique hypocrite de leurs États face aux pays du Moyen Orient »

Sinon, dans chaque ville à l’étranger où vivent des Iraniens, on essaie
de s’organiser. Il y a des collectifs à Paris, dans cinq ou six villes
en Allemagne, aux Pays-Bas, aux États-Unis, au Canada.
On a des groupes, on organise des actions, mais malheureusement, nous
n’avons pas beaucoup de pouvoir.

Qu’attendez-vous de ces pays, et de la France, en matière de soutien ?

On ne veut rien des États. Je ne peux pas parler au nom de tout le monde,
mais dans notre collectif, on demande que les États n’interviennent
surtout pas. Parce qu’on sait que là où il y a eu des interventions,
en Syrie, Irak, en Afghanistan, en est sorti encore plus de malheur.

Mais on demande de la solidarité de la part des sociétés, qu’elles dénoncent
la politique hypocrite de leurs États face aux pays du Moyen-Orient et
leurs entreprises qui vendent des équipements de répression à nos États.

L’enjeu de la sécularisation est-il vraiment central dans cette mobilisation
ou est-ce une perception déformée qu’on a en Europe, particulièrement en France ?


Les contextes français et iranien sont complètement différents sur cet
aspect. La société iranienne est peut être la plus séculière au sein du
monde musulman, si on considère l’ensemble de la population iranienne.

Une grande partie de la société iranienne ne pratique pas.

Ceci dit, la question de la sécularisation de l’État et des institutions
étatiques est un enjeu central de la mobilisations actuelle. La séparation
de la religion et de l’État est je pense un des deux seuls points sur
lequel tous les opposants actuels au régime sont d’accord.

Cela ne veut pas dire qu’ils sont contre les musulman·e·s ou les femmes
voilées, mais contre le voile forcé et pour la liberté de religion et
de pensée.

L’autre point d’accord est la changement de régime, même si les forces
de gauche parlent au-delà plutôt d’une révolution sociale et politique.




