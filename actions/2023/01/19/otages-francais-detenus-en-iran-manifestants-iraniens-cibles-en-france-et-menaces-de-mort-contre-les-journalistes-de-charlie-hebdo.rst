.. index::
   pair: Otages; 2023-01-19

.. _otages_2023_01_19:

===================================================================================================================================================
2023-01-19 OTAGES FRANÇAIS détenus en #Iran, MANIFESTANTS IRANIENS ciblés en #France et MENACES DE MORT contre les journalistes de @Charlie_Hebdo
===================================================================================================================================================

- :ref:`bernard_phelan` |BernardPhelan|
- :ref:`benjamin_briere` |BenjaminBriere|


OTAGES FRANÇAIS détenus en #Iran, MANIFESTANTS IRANIENS ciblés en #France
et MENACES DE MORT contre les journalistes de @Charlie_Hebdo_, la France
est devenue la nouvelle cible privilégiée de la République islamique.

Comment en est-on arrivé là ?
Cette semaine dans @LePoint

.. figure:: images/le_point.png
   :align: center
