.. index::
   pair: Otages; Français

.. _otages_2023_01_20:

==========================================================================================================
2023-01-20 **Appel au rassemblement de soutien aux otages francais en iran le 28 janvier 2023**
==========================================================================================================

- https://www.ldh-france.org/otages-francais-detenus-en-iran/

.. include:: ../../../../repression/otages/liste_2023_02_09.txt


Otages français détenus en Iran

La LDH appelle au rassemblement de soutien le 28 janvier 2023, à 14h00, à Paris

Actuellement, sept Français sont détenus en Iran pour des raisons fallacieuses.

Détenus de façon arbitraire, pour certains accusés d’espionnage par la
République islamique d’Iran, ils sont privés des droits les plus élémentaires,
à commencer par le droit à une instruction judiciaire et un procès dignes
de ces noms.

Nous, familles et proches des otages français détenus en Iran, souhaitons
tout d’abord alerter sur le sort injuste et les conditions de détention
inhumaines qui leur sont infligés.

Privés de contacts avec leurs proches depuis des mois et placés à
l’isolement pour certains, la santé physique et psychologique des otages
français se dégrade.

Nous demandons instamment leur libération et leur rapatriement, puisqu’ils
sont innocents et accusés à tort.

C’est pourquoi, nous vous invitons à vous joindre à notre rassemblement
symbolique et pacifique le samedi 28 janvier, à 14h00, sur le parvis des
Droits de l’Homme à Paris, en soutien aux otages français détenus en Iran.

Familles et comités de soutien de Fariba Adelkhah, Benjamin Brière et
Cécile Kohler

