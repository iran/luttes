.. index::
   pair: Mediapart; 2023-01-20

.. _mediapart_2023_01_20:

==================================================================================================================================================
2023-01-20 **Iran - Soutien au soulèvement « Femme, Vie, Liberté » - Non aux exécutions capitales ! (revendications, article de Mediapart)**
==================================================================================================================================================

- https://blogs.mediapart.fr/les-invites-de-mediapart/blog/200123/iran-soutien-au-soulevement-femme-vie-liberte-non-aux-executions-capitales


Introduction
===============

Depuis le 16 septembre 2022, un soulèvement populaire inédit fait trembler
la République Islamique d’Iran.

La répression a fait plus de 500 mort·es dont 69 mineur·es.

Pour éviter « des solutions imposées de l’extérieur »,  de nombreuses
personnalités politiques, du monde social et intellectuel proposent
« une véritable campagne de solidarité internationale avec toutes celles
et ceux qui se mobilisent en Iran pour en finir avec la République Islamique ».

Depuis le meurtre de Jina-Mahsa Amini le 16 septembre 2022 par la police
des mœurs, un soulèvement populaire inédit par son ampleur, sa profondeur
et sa durée fait trembler la République Islamique d’Iran.

En moins de 48h, le mot d'ordre « Femme, Vie, Liberté » s'est propagé
dans tout le pays, puis dans le monde entier.

La lutte pour la chute de la République Islamique est engagée

Rapidement d’autres slogans ont fleuri : « Mort au dictateur »,
« Mort à l'oppresseur, que ce soit le Chah ou le Guide suprême »,
« Pain, Travail, Liberté », « Pauvreté, corruption, vie chère, nous irons jusqu’au renversement ».

Ce mouvement de contestation radical rassemble des femmes, des jeunes,
les minorités nationales, des travailleuses et travailleurs avec ou
sans emploi, dans un rejet total de ce régime théocratique, misogyne et
totalement corrompu.

Le soulèvement s'ancre dans la durée et touche plus de 160 villes petites
et grandes.
Avec plus de 50% de la population sous le seuil de pauvreté et l’absence
de droits démocratiques et sociaux élémentaires, c’est l’ensemble du
système que les peuples d’Iran veulent renverser.

Les appels à la grève se multiplient, notamment parmi les enseignant·es
des universités, les salarié·es de la pétrochimie, des aciéries d’Ispahan,
des transports en commun de Téhéran et de sa banlieue, des chauffeurs routiers…

Les grévistes subissent licenciements, arrestations et tortures.


Une répression féroce et sans limite
=========================================

À ce jour, la répression a fait plus de 500 mort·es dont 69 mineur·es,
des milliers de blessé·es, plus de 19 000 prisonnier·es et disparu·es,
des enlèvements.

Au Kurdistan iranien et au Sistan-Baloutchistan, les Gardiens de la
révolution mènent une guerre sanglante contre la population révoltée.
Les villes kurdes subissent un état de siège qui ne dit pas son nom.

La violence de ce régime criminel n'a pas de limite. De nombreux témoignages
attestent de la brutalité inouïe des conditions de détention visant à
briser la détermination des détenu·es.
Des prisonnier·es sont torturé·es, violé·es, battu·es à mort.

Afin d’instaurer un climat de terreur et éteindre la contestation, le
pouvoir judiciaire prononce des condamnations de plus en plus lourdes à
l'encontre des manifestant·es.
Malgré cela, la mobilisation ne faiblit pas. Avec courage et détermination,
des étudiant·es, des jeunes, des femmes, des travailleurs et travailleuses,
des artistes et des journalistes continuent de défier le régime, et
celui-ci a décidé de franchir un cran supplémentaire.


La multiplication des condamnations à mort
=================================================

Pour le simple fait d’avoir manifesté, au moins 65 personnes (dont 11 femmes
et cinq enfants) ont été inculpées « d’inimitié avec Dieu », de « corruption sur Terre »,
d’insurrection ou de meurtre.
Le pouvoir judiciaire enchaîne des parodies de procès, sans aucun droit
de la défense et multiplie les condamnations à mort.

Après les exécutions de Mohsen Shekari et de Majidreza Rahnavard les 8
et12 décembre dernier, le pouvoir iranien a procédé, le 7 janvier, à la
pendaison de Seyed Mohammad Hosseini et de Mohammad Mehdi Karami.
Leurs crimes : avoir osé exprimer leur révolte face à la mort de Jina-Mahsa Amini
à Téhéran ou de Hadis Nadjafi à Karaj.

Le pire est à craindre pour celles et ceux qui attendent dans les couloirs
de la mort des sordides prisons d’Iran et plus largement pour l’ensemble
des détenu·es.


Les peuples d’Iran doivent être maîtres de leur destin
============================================================

Dans ce contexte et face au spectre d'une révolution politique et sociale
en Iran, les dirigeants des grandes puissances œuvrent, plus ou moins
discrètement, à la constitution d'un Conseil de transition rassemblant
tous les courants de l’opposition de la droite iranienne, dont les
monarchistes.

**Ces courants, libéraux sur le plan économique et autoritaires sur le plan
politique, sont à l’opposé des dynamiques des mobilisations et des
aspirations sociales et démocratiques qui s’expriment en Iran.**

Du coup d’État de 1953 organisé par la CIA et les services secrets britanniques
contre le gouvernement Mossadegh et sa politique de nationalisation du pétrole,
en passant par la conférence de Guadeloupe en 1979 où les chefs d'Etat
de France, d’Allemagne, de Grande Bretagne et des États-Unis ont accéléré
le départ en exil du Chah et l’avènement de Khomeiny, **les grandes puissances
ont toujours agi, sans surprise, en faveur de leurs propres intérêts
contre ceux des peuples d’Iran**.

A l’opposé des solutions imposées de l’extérieur, nous défendons une véritable
campagne de solidarité internationale avec toutes celles et ceux qui se
mobilisent en Iran pour en finir avec la République Islamique.


.. _revendications_mediapart:

Être à la hauteur de la détermination et du courage du peuple iranien
=========================================================================

L'issue du soulèvement en cours sera déterminante pour les peuples de la
région et du monde.
**Il est donc de notre responsabilité, à la mesure de nos moyens, d’aider
le soulèvement « Femme, Vie, Liberté » à réaliser ses aspirations émancipatrices.**

En effet, **la machine à répression qu’est la République Islamique ne sera
pas brisée sans une puissante campagne internationale et sans une mobilisation
forte des opinions mondiales**.

.. include:: ../../../../revendications/revendications_2023_03_07.txt


Signataires de cette tribune, nous réaffirmons notre soutien plein et
entier à toutes celles et ceux qui luttent en Iran pour l’égalité, la
justice sociale, la démocratie et contre toute forme de pouvoir
autocratique et autoritaire.

Nous sommes à leur côté par tous les moyens qui sont les nôtres, et nous
nous engageons à multiplier les initiatives de solidarité avec les peuples
d'Iran. Jusqu’à la victoire de cet élan révolutionnaire irrépressible !






