
.. index::
   pair: Rassemblement ; Samedi 21 janvier 2023
   ! Hymne de l'égalité


.. _iran_grenoble_2023_01_21:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ 18e rassemblement samedi 21 janvier 2023 à Grenoble **place Félix Poulat** à 14h30 **Arrêt des exécutions, annulation des condamnations à mort libération des prisonniers politiques, expulsion de l'ambassadeur d'iran** 📣
==============================================================================================================================================================================================================================================================

Arrêt des exécutions, annulation des condamnations à mort liberation des prisonniers politiques, expulsion de l'ambassadeur d'iran
127e jour depuis la mort de #JinaMahsaAmini (2022-09-16)

- :ref:`mahsa_jina_amini`
- :ref:`slogans`

.. figure:: images/annonce_rassemblement.png
   :align: center

- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|


Texte annonçant le rassemblement du samedi 21 janvier 2023
==============================================================

Le mardi 13 septembre 2022 Jina Mahsa Amini était arrêtée par la police des mœurs
à Téhéran car elle n’était pas **"assez voilée"**.
Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

Nos Revendications
-------------------

- :ref:`revendications_mediapart`

.. include:: ../../../../revendications/revendications_2023_03_07.txt


Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyez leurs voix. les iraniens ont besoin d'un soutien réel et d'actions
réelles pour en finir avec ce régime sanguinaire; le blabla ne suffit pas.


**Nous irons jusqu’à la liberté vraie, humaine, cette fois-ci jusqu’au bout.**

**Cette manifestation est organisée par la communauté Iranienne**.


Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`soutiens_avocats_2023_01_10`

Les victimes de la barbarie des mollahs
--------------------------------------------

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|


Quelques événements du 15 janvier 2023 au 21 janvier 2023
=============================================================================


- :ref:`mediapart_2023_01_20`
- :ref:`otages_2023_01_20`
- :ref:`otages_2023_01_19`
- :ref:`strasbourg_2023_01_16`
- :ref:`tour_eiffel_2023_01_16`
- :ref:`paris_2023_01_15`


Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/
- https://www.ici-grenoble.org/evenement/rassemblement-pour-larret-des-executions-en-iran-lannulation-des-condamnations-a-mort-la-liberation-des-prisonniers-politiques


.. figure:: images/ici_grenoble_en_une.png
   :align: center



Mastodon iranluttes
---------------------

- https://kolektiva.social/@iranluttes/109683323195263440



Mobilizon
------------

- https://mobilizon.chapril.org/events/cee4a1c2-357e-41f8-8721-4ad03a62ef2c
  Arrêt des exécutions, annulation des condamnations à mort libération des prisonniers politiques, expulsion de l'ambassadeur d'iran

- https://mobilizon.chapril.org/search?search=grenoble


Quelques photos
=================


Vidéos peertube
==================

- https://sepiasearch.org/search?search=grenoble+iran


.. _discours_zoya_2023_01_21:

Discours de Z. sur les revendications
---------------------------------------------

- https://www.orion-hub.fr/w/scVDK9PfGa1CuyFqR6fuAq

Voir :ref:`revendications_2023`

.. _discours_sheherazade_2023_01_21:

Discours de S. sur la répression dans la province de Sistan-Balouchistan
------------------------------------------------------------------------------------

- https://www.orion-hub.fr/w/3ug82eWFSJEGUw6FSDhBpb


- https://www.orion-hub.fr/w/ssXmQSWdtm6GqVqeaouXYk

Z. Sur les sepa pasdaran reconnus comme une organisation terroriste #IRGCTerrorists
-----------------------------------------------------------------------------------

- https://www.orion-hub.fr/w/dZmUt7Ts22ovbZWZU9aTGc


Z discours de fin
--------------------

- https://www.orion-hub.fr/w/3KFDzVQHnBaBMjZvhiC3cv


Chanson **Hymne de l'égalité**
----------------------------------

- https://www.orion-hub.fr/w/ssXmQSWdtm6GqVqeaouXYk


Chant 2 (à modifier)
-----------------------

- https://www.orion-hub.fr/w/ufMN7LS7SgZSmDRCM5tNPB

Chant 3 (à modifier)
-------------------------

- https://www.orion-hub.fr/w/nFr8LNcfcKVVVNWFsp3SgR


Chant 4 (à modifier)
----------------------

- https://www.orion-hub.fr/w/v92fTt7DBQDZEKri6e3x9j

Autres événements ce samedi 21 janvier 2023 à Grenoble
---------------------------------------------------------

.. figure:: images/evenements_grenoble_2023_01_21.png
   :align: center

   https://www.ici-grenoble.org/agenda


.. figure:: images/ici_grenoble_en_une.png
   :align: center

   https://www.ici-grenoble.org/
   https://www.ici-grenoble.org/article/avec-les-iranien-ne-s-en-lutte


Manifestations précédentes à Grenoble
=======================================


- :ref:`iran_grenoble_2023_01_14`
- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


