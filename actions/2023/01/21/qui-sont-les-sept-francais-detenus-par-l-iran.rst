

.. _francais_iran_2023_01_21:

==============================================================================================================================================================================================================================================================
Qui sont les sept francais detenus par l'iran ?
==============================================================================================================================================================================================================================================================

- https://www.liberation.fr/international/moyen-orient/qui-sont-les-sept-francais-detenus-par-liran-20230121_OS44GGOG5FE3NN3RSFB5A6TMDQ/


Au moins sept ressortissants français sont actuellement emprisonnés en Iran,
considérés comme des «otages d’Etat» par le Quai d’Orsay.

L’état de santé de l’un d’entre eux, Bernard Phelan |BernardPhelan|, est très préoccupant.


Alors que les relations entre l’Iran et les Européens se détériorent,
des dizaines d’Occidentaux sont détenus dans les geôles iraniennes,
parfois depuis plusieurs années et dans des conditions très difficiles.

Pour les défenseurs des droits humains, Téhéran utilise cette stratégie
de prise d’otages pour arracher des concessions à l’Occident.

Un «chantage» dénoncé à plusieurs reprises par la France, qui reconnaît
la présence d’au moins sept de ses ressortissants emprisonnés en Iran.

L’identité de deux d’entre eux reste inconnue.

