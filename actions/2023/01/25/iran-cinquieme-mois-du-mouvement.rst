.. index::
   pair: Behrooz Farahany; Article 2023-01-25

.. _iran_cinquieme_mois_2023_01_25:

============================================================================================
2023-01-25 **Iran cinquième mois du Mouvement, Femme Vie Liberté** par Behrooz Farahany
============================================================================================

INTERNATIONAL Iran Cinquième mois du Mouvement Femme Vie Liberté
Behrooz Farahany

LE PLUS GRAND ET LE PLUS DIVERSIFIÉ , GÉOGRAPHIQUEMENT ET SOCIALEMENT ,
des mouvements de protestation contre le régime islamique qu’a
connus l’Iran au cours de toute son histoire entre dans son cinquième
mois.

La répression est à la hauteur du mouvement, de plus en plus brutale :
18 000 arrestations, plus de 500 morts, des exécutions (à ce jour
4 jeunes exécutés, 21 condamnations à la peine capitale d’ores et déjà
prononcées).

Le mouvement montre des signes d’essoufflement. Les protestations
sont moins nombreuses et sont plutôt nocturnes avec des slogans hostiles
au régime scandés par des habitants des grandes résidences.
Les universités, un des deux piliers du mouvement avec le Kurdistan en ébullition,
sont pratiquement occupées par les forces de sécurité, les étudiants
meneurs des protestations sont arrêtés, éloignés des universités ou interdits
d’entrée.

Les grèves revendicatives des travailleurs continuent, mais n’apparaissent pas
politisées, même si des communiqués de solidarité viennent des activistes et
de syndicats embryonnaires. Il faut noter la présence des services de sécurité
dans l’enceinte des usines et des grandes unités du travail, surtout dans le
secteur pétrochimique, celle-ci s’est renforcée, ainsi qu’une première vague
d’arrestations : il y a trois mois 350 arrestations en une journée ont touché
les militants de ce secteur à la suite de la première grève ouvertement politique.

D’un autre côté, il semble que pour le moment, du fait de la militarisation de
la répression au Kurdistan, le centre de gravité de la protestation
s’est déplacé vers la province de Sistan-Baloutchistan, où de nouveaux slogans
liant les revendications sociales à la question des libertés se sont multipliés
dans les grandes manifestations de vendredi.
Le plus connu étant **Ni Mollah Ni Chah, Liberté Égalité !**

Retour sur les mouvements de protestation du passé
Pour mieux comprendre la situation et analyser les futurs chemins
possibles, un rappel des mouvements de protestation précédents est
nécessaire pour appréhender les similitudes et les différences entre ceux-ci
et le mouvement en cours.
Pendant la décennie sanglante des années 1980, avec plus de 30 000 morts,
des dizaines de milliers des prisonniers et autant d’exilés, des militants
vaincus en plusieurs vagues, et 8 années de guerre contre l’Irak (ce « don
du ciel » selon Khomeiny, en ce qu’il a sauvé le nouveau régime), la
République islamique s’est installée par le sang et les larmes. Il a fallu
attendre dix ans pour connaître un premier mouvement de protestation
d’envergure, ce fut en 1998 une première révolte des étudiants, de courte
durée et réprimée brutalement.
❘
L’application des politiques néo-libérales après la fin de la guerre
d’Irak a provoqué un puissant mouvement revendicatif parmi les
travailleurs, avec des records du nombre des manifestations et actions de
protestation. Mais ce n’est que des années plus tard qu’une protestation
purement politique s’est concrétisée. Le mouvement dit Vert (du nom de la
couleur choisie pour les soutiens de M. Moussavi) fut un soulèvement
contre la fraude électorale qui a suivi l’annonce de la victoire de Mahmoud
Ahmadinejad en 2009. À ce moment les conditions sociales étaient bien
différentes de celles d’aujourd’hui. La confrontation avait été
essentiellement provoquée par la rivalité entre des deux principales factions
du régime, dans le cadre opposant les soi-disant « réformateurs » et les
durs dits « principalistes ».

À la surprise des dirigeants du mouvement, Moussavi et Karoubi, la
protestation s’est déplacée des urnes vers la rue, par ceux-là mêmes qui
étaient influencés par le discours dominant du « changement pacifique
par le haut » et la croyance que les processus de réforme seraient
fructueux. Ceux-là, qui formaient la large base sociale des réformateurs,
sont descendus dans la rue. Ils avaient constaté, de par les expériences
antérieures, que militer pour gagner dans les urnes étant insuffisant et
qu’ils devaient défendre leurs revendications dans la rue. Des centaines
des milliers de manifestants, essentiellement des couches moyennes,
investirent les rues de Téhéran, et à un moindre degré de quelques grandes
villes d’Iran.

Le Mouvement vert a duré quelques mois de manière discontinue, il a
réuni jusqu’à 2 millions de manifestants dans les rues de Téhéran. Après
les manifestations violentes de l’Achoura, le 26 décembre 2009,
désapprouvées par les leaders, un vent de panique a gagné les deux
camps adverses. La réaction du régime a été de confirmer « le verdict
des urnes » et d’engager une répression violente, provoquant des dizaines
de victimes (on a parlé de plus de 150 morts au total, dont 37 au cours
de cette seule journée) et l’emprisonnement et la torture de milliers de
personnes.
Face à la répression féroce et au manque de décisions claires des
dirigeants du mouvement, qui n’osèrent pas défier le Guide Suprême, la
protestation a perdu de son intensité et s’est progressivement éteinte. Les
dirigeants réformateurs, qui ont complètement échoué du fait de leurs
hésitations et de leur manque de perspective stratégique, ont été un facteur
déterminant de cet échec. Leur peur d’une convergence des puissantes
luttes ouvrières revendicatives alors en cours et du mouvement Vert les a
paralysés. Ils ne voulaient absolument pas d’une telle convergence, car
ils n’étaient pas opposés aux politiques néolibérales, et n’ont réagi aux
arrestations des leaders du mouvement ouvrier que par un silence total !
Il faut souligner que le mouvement Vert était principalement limité aux
métropoles du centre du pays (c’est-à-dire Téhéran et plusieurs métropoles
à majorité persane comme Ispahan et Chiraz) et n’a pas été en mesure
d’attirer à lui la classe ouvrière et les couches démunies de la société. La
domination des « réformateurs » sur ce mouvement était incontestable et
incontestée, quant à la plupart des slogans ils avaient une résonnance
religieuse « Ô Hussein Mir Hussein ! » (en référence à l’imam martyr des
chiites et au prénom de M. Moussavi).

Il est indéniable que ce leadership a privé les mouvements de protes-
tation des références politiques de classe qui auraient été nécessaires à
la poursuite et à l’approfondissement du mouvement.

Après l’échec de ce mouvement, qu’on peut qualifier de tentative de
« participation au pouvoir » d’une partie de la nouvelle bourgeoisie
exclue du cercle dirigeant, en alliance avec des couches plus ou moins
aisées de la petite bourgeoisie urbaine, s’est ouverte une période de
calme sur le plan des protestations qui a duré presque neuf longues
années. Beaucoup ont pensé que seule l’ampleur de la répression
expliquait la victoire du régime sur ce mouvement comme sur les
précédents et le calme qui s’en est suivi.

Mais pour ce mouvement et pour ceux de la décennie suivante, outre
le facteur de la répression et les problèmes du leadership, il faut prendre
en compte les facteurs économiques, alors favorables au régime. Et aussi
la pression idéologique du discours dominant des réformistes tels Khatami
et Moussavi, c’est-à-dire une faction au sein du pouvoir qui a également
joué un rôle décisif dans la poursuite des mêmes rapports entre les classes
moyennes et la totalité du pouvoir malgré la défaite qu’elle avait subie.
La combinaison de ces facteurs a permis au régime islamique dans sa
totalité, qui englobe les deux factions, de disposer d’une assisse sociale
non négligeable, surtout au sein des couches moyennes urbaines. C’est
pour cette raison que dans la série d’élections de cette période (le premier
tour des conseils de ville et de village, le sixième mandat du Conseil
islamique et le second tour de l’élection présidentielle de Mohammad
Khatami), on a enregistré une victoire significative des réformateurs. Dans
le même temps, l’autre faction conservatrice maintenait son emprise
idéologique, plus ou moins forte, sur sa base traditionnelle dans la société,
et l’islam politique, prôné par le Guide, restait un facteur de légitimation
efficace aux yeux de sa base populaire et déshéritée.

De plus, et cela a joué un rôle décisif à la suite de la défaite du
mouvement Vert, le régime islamique disposait à ce moment des
possibilités économiques lui permettant de « se légitimer » auprès d’une
bonne partie de la population. Le mouvement Vert a eu lieu à un moment
où les revenus en devises provenant des exportations de pétrole iranien
avaient commencé à atteindre des records historiques. Cela a permis au
régime d’être à partir de 2008 en mesure d’offrir des opportunités
économiques séduisantes, à la fois aux classes défavorisées et aux classes
moyennes et aisées de la société.

La politique de subventions sous forme d’aides en liquide aux classes
inférieures de la société est apparue très attrayante. Le montant de la
subvention allouée à une famille de quatre personnes au cours de la
première année de sa mise en œuvre représentait près des deux tiers du
salaire minimum de l’époque. Par conséquent, de nombreux groupes et
personnes qui avaient un revenu égal ou inférieur au salaire minimum
Des étudiantes et étudiants iraniens tiennent un sit-in de protestation à l’université de Téhéran,
le 23 octobre 2022.

ont, du jour au lendemain, disposé d’un revenu presque deux fois plus
élevé qu’auparavant. Bien que la classe moyenne ait été politiquement et
socialement vaincue, elle s’est vu offrir de nombreuses opportunités
financières sur le plan économique. Comme l’ont souligné des économistes
comme l’économiste hétérodoxe Parviz Sédaghate, les signes positifs
étaient bien là : l’indice boursier a augmenté de 50 % en 2008 et de 85 %
en 2009. Cela alors qu’un taux de change stable vis-à-vis du dollar était
maintenu à environ 1 000 tomans pendant toutes ces années. Les classes
moyennes et supérieures de la société disposaient de ressources
financières excédentaires, de possibilités de voyager à l’étranger et
pouvaient profiter d’opportunités rentables dans le logement et avec les
spéculations rentables sur l’or et les devises.

Pour leur part, les leaders vaincus du mouvement Vert, bien que considérant
l’élection précédente comme ayant été frauduleuse, invitaient le peuple à
participer à l’élection suivante, cela par peur d’une radicalisation. D’où
le nom de « soupape » attribué à certaines figures connues de cette
faction. À l’époque, et dans la situation économique décrite ci-dessus, ce
discours était encore dominant et M. Rouhani a été élu avec le soutien de
la même base et porté par la même alliance.

Donc réduire la cause de l’échec du mouvement et le « calme » d’après
à la seule répression sanglante du gouvernement est une vision réductrice
qui, face à des mécanismes de répression de plus en plus sophistiqués,
peut conduire à une forme de défaitisme faute d’issue aux problèmes
existants.

Les manifestations de décembre 2017 et novembre 2019
La première explosion sociale d’envergure depuis l’échec du
mouvement Vert s’est produite en décembre 2017. Elle a commencé dans
la ville sainte de Meched, après un discours violent du représentant du
Guide à l’égard du cabinet de M. Rouhani. Mais, à la surprise générale,
les manifestants sont vite passés des slogans hostiles à Rouhani aux
slogans contre le régime et contre le Guide en personne ! Non seulement
ils ne soutenaient pas le gouvernement mais exprimaient violement leur
détestation de tout le système. Le mouvement s’est propagé comme un
feu de paille et a embrasé plus de 100 villes grandes et petites. Le
mouvement a duré deux semaines et une fois de plus a subi une répression
très violente. Signe distinctif : la mise hors jeu des leaders réformateurs !
Absence de tout slogan religieux, de tout mot d’ordre en faveur du camp
réformateur ! Le premier mouvement de protestation politique de la
République islamique d’Iran depuis plus de 20 ans était né. La croyance
en la possibilité du changement par le haut était révolue. Le discours des
amis de MM. Khatamai et Rouhani a cessé d’être le discours dominant.
Un slogan, formulé par des étudiants de l’université de Téhéran et
scandé partout, résume à lui seul ce changement : « Réformateurs, Princi-
palistes, Votre tour est terminé ! ».

Un vrai tournant politique s’est alors produit en Iran
Du fait que le régime n’a proposé aucune réponse aux demandes
exprimées pendant ces manifestations, et que la situation politico-
économique a bien changé par rapport aux deux décennies passées, à
peine deux ans après le soulèvement du décembre 2017 une nouvelle
explosion s’est produite en novembre 2019. Cela au lendemain de
l’annonce du gouvernement Rouhani concernant la suppression des
subventions sur l’essence et le fioul. Ce mouvement, plus vaste, avec un
nombre plus important des manifestants embrasant alors plus de 160
villes, y compris toutes les grandes métropoles comme Téhéran, Chiraz,
Tabriz, Zâhedân, Rasht, Sanandaj..., a secoué les fondements du régime.
Une différence importante entres ces soulèvements et le mouvement
Vert est que les couches sociales porteuses de ces manifestations étaient
majoritairement des couches pauvres et déshéritées des périphéries des
grands villes, de jeunes ouvriers et des salariés précaires, des chômeurs,
des vendeurs à la sauvette, etc. On était loin de la domination des couches
moyennes aisées constatée pendant le mouvement de 2009.

Cela s’est produit dans un contexte de blocus économique structurel
presque total. La somme des développements économiques depuis le
début de cette décennie a provoqué la crise de stagflation des années
ultérieures, qui s’est manifestée par le taux d’inflation à deux chiffres au
cours de presque toutes ces années, un taux de croissance économique
insuffisant et souvent inférieur aux niveaux prévus pour la plupart de ces
années. D’après les calculs des économistes indépendants, des années
2010 jusqu’à aujourd’hui le taux de croissance économique moyen a
été nul, le montant des nouveaux investissements dans l’économie
iranienne est inférieur au capital déprécié, et le taux d’inflation a atteint
des niveaux sans précédent. Par conséquent, les facteurs qui ont créé des
booms spéculatifs temporaires dans certains secteurs de l’économie dans
Les protestations, ont éclaté le 16 septembre 2022
après le décès à l’hôpital de la jeune Iranienne de 22 ans, Mahsa Amini.
les années 2008 et 2012 ont disparu, en particulier à partir de la seconde
moitié de la décennie 2010. Comme le souligne Parviz Sédaghat, le
boom de la Bourse en 2018 était une pyramide de Ponzi que le
gouvernement de l’époque dirigé par M. Rouhani a mise en œuvre, et
n’était en rien le signe d’une amélioration de la situation économique.
Pendant ces 20 dernières années, l’utilisation massive d’Internet en
Iran et la présence très importante des Iraniens sur les réseaux sociaux
ont rapidement rendu inefficace l’outil de propagande du gouvernement
et ainsi aidé à délégitimer l’idéologie de l’islam politique dans de larges
groupes de la société, en particulier la jeunesse. Quand on observe l’âge
moyen des manifestants en Iran, c’est-à-dire les 18-35 ans, on constate
qu’ils n’ont connu que le système d’éducation islamique et pourtant n’ont
que des idées et des idéaux laïques et non religieux, et dans la jeunesse
augmente fortement le nombre des non-croyants voire des athées. On
voit que le ciment idéologique du régime est complétement fissuré et que
l’emprise idéologique de l’islam politique n’est plus évidente, même parmi
les couches défavorisées les plus traditionnalistes.

À cela il faut ajouter la crise diplomatique constante dans les rapports
entre l’Iran et les pays occidentaux durant ces deux décennies : renvoi du
cas de l’Iran au Conseil de sécurité et imposition de sanctions, puis la
conclusion du JCPOA et la réduction temporaire des sanctions, ensuite
avec Trump le retrait des États-Unis du JCPOA et le retour des sanctions
qui ont intensifié et assombri la vie économique et sociale des Iraniens.
La crise géopolitique a été permanente et a ajouté à l’instabilité
économique, avec surtout l’effondrement de la monnaie nationale par
rapport au dollar et à l’euro.

Donc, contrairement à l’époque du mouvement Vert, le régime était
beaucoup plus fragilisé économiquement, il avait perdu une grande partie
de sa base sociale ainsi que la bataille idéologique.
Quelle organisation et quel leadership ?

En République islamique d’Iran, la possibilité de former des organisa-
tions indépendantes du gouvernement a toujours été très limitée et
formellement interdite par le Code du travail pour les salariés et les organi-
sations syndicales indépendantes. Le Syndicat Vahed, des transports de
Téhéran et de sa banlieue, et le Syndicat de la Sucrerie de Haft Tapeh ont
été sévèrement réprimés, et toutes sortes d’organisations civiles, y compris
des organisations caritatives, ont vécu dans des conditions très difficiles.
Quant aux partis politiques indépendants des deux factions du
gouvernement ils n’ont pratiquement pas pu exister.

Mais avec l’internet, les réseaux sociaux permettaient à des personnes
en affinités de réfléchir ensemble et créaient la possibilité d’appeler à
des actions communes. La société iranienne étant parmi les plus connectées
du monde, au même rang que La France par exemple, ces outils ont été
utilisés à merveille, comblant partiellement le manque d’organisations réelles.
Ils offrent une méthode efficace de mise en relation et de mobilisation,
mais force est de constater que cela ne répond pas au vide du leadership.
Les manifestations de 2017, de novembre 2019 et celles de 2022
ont été rendues possibles principalement en s’appuyant sur la communi-
cation via les réseaux sociaux. Les militants des mouvements sociaux
revendicatifs ont utilisé les mêmes outils, tout en s’appuyant, quand c’était
possible, sur des formes classiques, comme l’Association des Enseignants
d’Iran, créée sous la présidence de M. Khatami, qui a exploité, fort
intelligemment, une zone grise « juridique ». Cette association est à l’origine
de nombreuses manifestations nationales efficaces pendant des années et
peut être citée comme un exemple réussi d’organisation « à l’ancienne ».
Ses membres fondateurs sont actuellement tous sous les verrous.

Grâce à ces facteurs, nous avons assisté à l’une des périodes les plus
turbulentes de toute l’histoire moderne de l’Iran en termes de revendications
de divers groupes de salariés, notamment les ouvriers, les enseignants et
les retraités. Pas un seul jour sans une manifestation ou une protestation !
On a dénombré 4 122 actions de protestation, sous différentes formes et à
des échelles différentes, entre le 1 er mai 2020 et le 1 er mai 2021, organisées
par les salariés et retraités iraniens. Un record absolu depuis l’avènement
du régime islamique en Iran. Même la pandémie de Covid-19 n’a pas mis
fin à ces protestations, tout en diminuant leur ampleur, cela contrairement à
l’Irak ou au Liban où les protestations ont été stoppées brusquement.
Où va le soulèvement Femme, Vie, Liberté ?

Il n’est pas besoin d’entrer ici dans le détail du déroulement du
soulèvement en cours. En revanche il faut analyser les forces et faiblesses
de ce mouvement historique. Il existe une large convergence des analyses
parmi des observateurs qui viennent d’horizons politiques différents sur
les caractéristiques de ce mouvement, mais pas nécessairement quant à
l’avenir de cette révolte inédite et historique, qui s’inscrit dans la continuité
des soulèvements de 2017 et 2019.

Résumons :

- Les leaders de ce mouvement utilisent les mêmes méthodes que leurs
  prédécesseurs, tout en améliorant leurs façons d’organiser les actions. Il n’y a pas
  une rupture de méthode. Les expériences des soulèvements de 2017 et 2019
  sont bien là et sont utilisées avec brio par les jeunes engagés dans cette lutte.
- Le caractère féminin de ce soulèvement, qui a placé le mouvement
  des femmes et ses revendications au sommet de ses nombreuses revendi-
  cations, distingue ce mouvement de tous les autres. La présence active et
  parfois décisive des femmes dans ce soulèvement est indéniable. Leur
  courage face aux forces de la répression a ébloui le monde entier et a
  provoqué un élan de solidarité aux quatre coins du monde, comme en
  témoignent les campagnes de solidarité des artistes et de certaines
  célébrités en faveur de ce mouvement. Dans le même temps la présence
  des femmes sans voile dans les espaces publics des villes d’Iran – chose
  impensable il y a un an ! –, a ajouté une forme de désobéissance civile
  aussi vaste que sans précédent au mouvement général de protestation.

- Outre les femmes, la présence des étudiants à la pointe de la contestation
  rappelle leur rôle dans les soulèvements de 2017 et 2019, mais
  par son ampleur l’engagement massif des étudiants dans ce mouvement
  dépasse de loin leur activisme dans des mouvements antérieurs. La
  participation des étudiants, du fait de la dispersion géographique des
  universités, a élargi la portée des protestations, et en même temps a
  donné un esprit progressiste au mouvement, grâce à son histoire moderne
  et héroïque, car durant les 90 dernières années les étudiants ont toujours
  porté l’étendard des revendications progressistes et radicales du peuple
  iranien.
- En regardant de près la composition des manifestants, on constate
  qu’à l’exception de la nouvelle bourgeoisie capitaliste-religieuse qui s’est
  constituée autour du pouvoir du Guide, toutes les classes sociales sont
  présentes dans ce mouvement, avec une prédominance des couches
  moyennes inférieures, ainsi que des jeunes ouvriers précaires, chômeurs,
  petits vendeurs, etc. Des mouvements de grève ont eu lieu dans les
  boutiques et les centres commerciaux, surtout en Kurdistan, dans la
  Province de Guilan au nord, à Téhéran et quelques autres grandes villes.



Il y a eu des grèves de solidarité y compris dans le Bazar de Téhéran,
base historique du régime islamique. Cela également est nouveau et
représente un signe distinctif du mouvement Femme Vie Liberté.

➤ Il faut ajouter que cette fois-ci même des groupes sociaux comme
les médecins et avocats, qui n’étaient pas engagés dans les protestations
de 2017 et 2019, sont présents, à leur façon, dans ce mouvement. C’est
sans précédent et montre que le caractère « tous ensemble » de ce
soulèvement est bien plus large que par le passé.

➤ L’étendue géographique de ce soulèvement est beaucoup plus
inclusive. Les habitants de toutes les provinces, du nord au sud et de
l’est à l’ouest, y participent. Les grandes et petites villes, centres ou
périphéries sont en mouvement. Les groupes nationaux comme les
Kurdes, les Azéris, les Lors, les Baloutches... sont non seulement
présents mais sont solidaires, et jouent un rôle de leader dans le cas
des Kurdes qui ont été à la pointe du mouvement, déjouant les accusa-
tions de « séparatisme » lancées par leurs adversaires. Les Kurdes
avec les étudiants ont été jusqu’ici les deux principaux piliers du
mouvement.

➤ En dernier lieu il faut souligner la solidarité sans précédent de la
diaspora iranienne avec le soulèvement en cours. Jamais les Iraniens
exilés ou vivant dans d’autres pays n’avaient manifesté un tel élan de
solidarité avec ce qui se passe en Iran. Même au sommet du mouvement
Vert de 1998, il n’y avait eu une telle solidarité. Le rassemblement de
Berlin, en décembre dernier, où plus de cent mille Iraniens sont venu de
toute l’Europe est sans précédent dans toute l’histoire de la diaspora
iranienne, y compris par delà les 43 années de l’existence du régime
islamique.

Reste jusqu’à présent un grand absent dans ce mouvement : la grève
politique des travailleurs en tant que classe et non comme citoyens engagés
dans des protestations de rue. L’Iran est un pays où les rapports de
production capitalistes dominent à cent pour cent, le salariat urbain, dans
son ensemble diversifié, constitue la majorité de la population. Rien de
radical ou de profond ne se fera en Iran sans la participation de la classe
ouvrière. Ce fait n’a pas été négligé par l’opposition de droite royaliste !
D’où les appels du pied en direction des travailleurs de ceux-là mêmes
qui, quand ils étaient au pouvoir, n’avaient pas ratifié les conventions de
l’OIT sur le droit de grève et le droit à la formation des organisations
indépendantes des salariés.

Si l’on considère que le succès de manifestations urbaines et étudiantes
de ce type nécessite qu’elles soient accompagnées de grèves du monde
du travail, comme le montre une analyse de la révolution contre la
monarchie en 1978-1979, le soulèvement actuel n’a pas encore réussi à
pallier cette insuffisance. Ce fait constitue encore une grande faiblesse
du mouvement Femme Vie Liberté.

Bien sûr, pour la première fois, principalement lors des premières
semaines, nous avons également assisté à une grève de solidarité des
travailleurs contractuels de la pétrochimie, ceux-là qui avaient réussi à
organiser la grande grève revendicative de l’été 2020 avec une partici-
pation historique de plus de 100 000 grévistes dans 13 provinces.
Mais, comme souligné ci-dessus, après l’arrestation de 350 militants,
cette grève n’a pas eu de suite. Et, quel que soit son impact symbolique
très fort, un tel mouvement ne saurait suffire à lui seul. Sa réussite suppose
qu’il puisse être complété par des grèves dans les complexes industriels.
On sait que, dans les conditions de la crise générale et le chômage de
masse, les mobilisations sont très difficiles lorsque le marché du travail
est celui des contrats temporaires et précaires. Reste que la poursuite et
le succès du soulèvement ne seront pas possibles sans l’engagement de
la classe ouvrière, et surtout des prolétaires du secteur pétrochimique. Il
n’y a pas d’alternative à cette voie.

Il faut ajouter à ces difficultés de la mobilisation des travailleurs, le fait
qu’en ce qui concerne le secteur gouvernemental et les institutions
publiques, outre les facteurs liés à l’instabilité de l’emploi, il existe en Iran
un système de sélection de l’emploi qui vise à privilégier l’embauche des
partisans du régime dans les secteurs sensibles de l’économie nationale.
Si on ajoute à cela la présence des forces de sécurité, « Harasate » en
persan, au sein même des grandes entreprises, on peut comprendre
qu’organiser une grève, et de surcroît une grève politique, n’est pas une
mince affaire. Il faut du temps et l’approfondissement de la crise politico-
sociale pour que cette jonction entre les protestations dans la rue et les
grèves industrielles s’opère. Sans tomber dans le piège d’une analyse
rigide, non critique et surtout non-actualisée, on rappellera que la classe
ouvrière était entrée dans le mouvement de masse de la révolution anti-
monarchique au bout de 14 mois, bien après les autres classes sociales.

Jusqu’ici les leaders du mouvement et les militants ouvriers qui ont une
longue expérience d’organisation de grèves revendicatives n’ont pas
réussi à accélérer cette entrée en mouvement. L’arrestation de presque
toutes les figures connues du mouvement ouvrier et du mouvement des
enseignants, juste avant le déclenchement du mouvement Femme Vie
Liberté, n’est pas sans conséquences dans ce domaine.
Mais une nouvelle forme d’organisation sous forme de « Comités de
Quartier » est apparue au cours des deux derniers mois, à l’initiative de
jeunes militants. Cela peut être une réponse partielle mais efficace au
besoin du développement du mouvement. Car l’intensification de la crise
de ces dernières années a créé une « migration interne » des salariés à
faible revenu et des couches paupérisées, du centre vers la périphérie
des villes. Des ouvriers, des jeunes chômeurs, des enseignants avec des
bas salaires et des étudiants vivent dans les mêmes quartiers, et cette
forme d’organisation facilite une coordination en vue de convergence
des luttes. D’autant que les grandes usines sont souvent à proximité de
ces quartiers, et donc l’échange d’information est presque permanant.
Est-ce la chaînon manquant ? L’avenir le dira.

L’organisation des réseaux d’une manière horizontale, qui a fortement
réduit la vulnérabilité du mouvement à la répression, rend difficile dans
le même temps la coordination et l’élaboration d’une stratégie commune
pour les militants. Ce mouvement est géré sur le mode « Nous sommes
tous Spartakus ! », lequel crée aussi la possibilité des leaders
autoproclamés à l’intérieur et/ou l’extérieur du pays.
Sur ce point existe un véritable danger de « fabrication de leaders »
à l’étranger, voire en Iran. Les grandes chaînes de télévision par satellite,
dont l’audience en Iran se chiffre en millions, jouent un rôle efficace dans
l’information en couvrant en permanence l’actualité des manifestations,
mais elles s’efforcent ouvertement de « constituer des leaders » pour ce
mouvement, surtout en essayant de « réhabiliter » l’ancien régime
monarchique « blanchi » par les méfaits du régime islamique qui a
largement dépassé les atrocités commises par l’appareil répressif
monarchique. Le fils de l’ancien Chah d’Iran, un politicien inexpérimenté
mais ô combien fidèle aux Américains et surtout aux Israéliens, est leur
candidat à ce poste ! Chaque jour on nous parle de ses initiatives en vue
« d’unifier le mouvement et le représenter » auprès des gouvernements
occidentaux. Une véritable OPA est lancée par les monarchistes, avec la
complicité et la participation active de ces super-médias, financés
essentiellement par l’Arabie Saoudite, les Cheiks du Golfe Persique et
Israël. Préparation d’un « Retour vers le Futur » autour de la dynastie
Pahlavi !

Le rôle de certaines célébrités iraniennes dans cette offensive est néfaste
et il ne doit pas être pris à la légère. De toute évidence les puissances
occidentales, tout en observant la situation ne sont pas encore arrivées à
la conclusion que le régime islamique n’a plus d’avenir. La réception de
certaines de ces « célébrités » et figures de la droite iranienne par
Emmanuel Macron, ainsi que l’activisme de Justin Trudeau dans son
soutien à monsieur Hamed Esmailioun (président de l’association des
victimes de l’avion ukrainien abattu par le système anti-missiles des
gardiens de la révolution, organisateur du grand rassemblement de Berlin)
ont fait beaucoup de bruit. Mais ce ne sont que des signaux faibles
destinés à cultiver « la sympathie » de la diaspora iranienne. Ils ne doivent
pas être compris comme un « alignement » stratégique sur l’opposition et
l’abandon de la République islamique par les occidentaux.
En revanche existe un autre danger qui a récemment montré le bout
de son nez. Depuis un mois la coalition de droite-extrême droite au pouvoir
en Israël évoque quotidiennement une « préparation » d’attaque sur l’Iran,
et les extrémistes va-t-en guerre dans le cercle du pouvoir iranien, qui
sont de plus en plus isolés sur la scène internationale, attendent ce nouveau
« cadeau du ciel » afin d’écraser toute contestation et les mouvements
sociaux en Iran. Une guerre est le pire qui pourrait arriver au mouvement
Femme Vie Liberté !

Quel avenir ?

Au fur et à mesure de l’avancement du mouvement, les aspirations à
la liberté, les demandes de démocratie, la négation complète du système
de gouvernance, et surtout, récemment à Zahedan chef-lieu de Sistan-
Baloutchistan, au Kurdistan, et dans les universités, des demandes d’égalité
et de justice sociale se font entendre de plus en plus fortement. C’est là
que les visions d’avenir divergent. L’opposition de droite en exil est
totalement acquise à la doxa néolibérale prônée par le Fonds monétaire
international et l’Union européenne. Force est de constater que pendant
le soulèvement de 2019, à la suite de l’augmentation brutale des prix
de l’essence, les économistes en exil, proches des cercles de l’opposition
de droite, ont approuvé cette politique de « la vérité des prix » ! Ils jugent
« prématurées » les demandes de justice sociale et insistent sur la priorité
absolue qui doit être accordée au « développement économique en liaison
cordiale avec les instances internationales », afin de « remédier » aux
désastres économiques provoqués par le régime islamique.

Cette tendance va forcément provoquer une rupture avec les vastes
couches sociales défavorisées de la population iranienne. Le « Tous
ensemble » ayant ses limites, le mouvement actuel n’échappera pas à
cette contradiction. Le rôle des militants des mouvements sociaux qui sont
engagés dans ce mouvement est de constituer un « Bloc social » voire un
« Bloc socialiste » en rassemblant les très nombreux éléments de la gauche
iranienne. Plus le mouvement s’avance, plus il s’approfondit, plus la
fracture entre la droite et l’aspiration à la justice sociale sera visible. Il
faut se préparer à cette situation inévitable. En ce domaine l’importance
du mouvement ouvrier et de ses grèves est primordiale. La question de
l’organisation n’est pas encore tranchée et cela va forcément nuire à la
suite du mouvement. Une jonction entre le mouvement Femme Vie Liberté
et les grèves revendicatives en cours est la clé de la réussite. Sans elle
non seulement les demandes de justice sociale n’avanceront pas, mais
les protestations dans la rue vont s’affaiblir. On ne renverse pas un régime
capitalo-religieux comme la République islamique, avec ses multiples
appareils répressifs, par les seules manifestations de rue.
La classe dirigeante est divisée. On entend de plus en plus les appels
de Khamenei aux « élites » silencieuses à s’engager davantage dans
« la protection du pouvoir islamique », cela sans succès. L’utilisation de
la répression à outrance n’est pas un signe de puissance, au contraire
cela montre que le régime n’a pas d’autre choix et qu’il a perdu la bataille
de l’opinion. La rupture entre la très grande majorité du peuple et le
pouvoir est totale et irréversible. Le régime n’a plus les moyens
économiques nécessaires pour répondre, même partiellement, aux
demandes sociales, et une partie de la classe dominante ne veut pas
sombrer avec la faction extrémiste unie autour du Guide. C’est devenu
une évidence, et les appels répétés de Khamenei à l’unité sont un aveu
de faiblesse et le signe d’un manque réel d’unité de l’élite du régime. Ce
manque d’unité « en haut » peut aboutir assez rapidement à une fracture
visible du pouvoir, et cela donnerait une impulsion inouïe au mouvement
en cours.

La crise en Iran est multi-dimensionnelle, mais c’est avant tout une crise
économico-sociale. Le nombre des gens vivant en dessous du seuil officiel
de pauvreté augmente, exponentiellement, chaque mois ; et l’écart entre
le revenu réel des ménages et le seuil de pauvreté ne cesse d’augmenter.
Par conséquent, l’émergence de nouvelles vagues de protestations et de
grèves urbaines pour exiger des moyens de subsistance est inévitable.
À cela s’ajoute une crise écologique, surtout une crise de l’eau
disponible, qui a déjà provoqué des émeutes dans un passé récent. Et
aussi une crise idéologique et morale due à la corruption systémique et
au pillage des ressources du pays par une poignée des puissants. Une
liste non exhaustive !

La révolte contre le hijab islamique a déclenché cette révolte et a placé
les courageuses filles et femmes iranienne en première ligne de la contes-
tation. Et accompagnant l’avancement du mouvement d’autres revendi-
cations ont fait surface. Les conditions nationales et internationales dans
lesquelles le régime se trouve font qu’il n’a absolument pas les moyens
de répondre à ces demandes, même partiellement, donc le mouvement
de révolte en cours, même affaibli, voire défait momentanément, est
appelé à se redresser.

On entre dans le cinquième mois de la révolte. La baisse récente
d’intensité des protestations ne doit pas tromper. Nous sommes de plus
en plus au seuil d’une révolution. Les conditions politico-sociales font que
44 ❘ CONTRETEMPS JANVIER 2023I NTERNATIONAL
les racines du mécontentement généralisé sont toujours là et l’incapacité
du régime à donner des réponses l’alimente. Reste que beaucoup de
faiblesses sont apparues, des questions restent sans réponse qui nécessitent
une réflexion approfondie, surtout sur la question de l’organisation des
luttes en cours.
La lutte continue ! 
Le 15 janvier 2023

Post scriptum (18 janvier 2023)

Des signes indiquent que la situation est en train de changer. Le 17 janvier, à
l’appel de Conseil de coordination et de la solidarité de travailleurs de l’industrie
du pétrole, les travailleurs sous contrat à durée indéterminée, qui sont les « cols
blancs » de ce secteur et qui reçoivent un salaire moyen bien supérieur aux autres
secteurs industriels, ont cessé le travailler dans plusieurs sites. Plusieurs raffineries
et d’autres unités du travail ont été sérieusement touchées, malgré un contrôle
sécuritaire renforcé. Sous une banderole demandant le respect strict d’une loi
concernant l’augmentation de leurs revenus, ils se sont rassemblés devant les
bâtiments et ont scandé des slogans hostiles aux « responsables » de leur situation,
et dénonçant « la situation générale où les fruits de notre travail sont accaparés
par les autres »... Plusieurs « Comités du Quartier des Jeunes Révolutionnaires »,
notamment au Kurdistan, ont salué cette grève et ont appelé les autres salariés à
suivre leur exemple, tout en soulignant « le rôle déterminant et le leadership de
classe nécessaire des travailleurs ». Pendant cette grève, 6 grévistes ont été arrêtés
à Assalouyéh, une place forte des actions de protestation au sud de l’Iran. Face
aux menaces d’extension de la grève, ils ont été libérés dans la soirée sous caution.
