.. index::
   pair: Eric Piolle; Grenoble

.. _maire_grenoble_2023_01_25:

====================================================
2023-01-25 **Rencontre avec le maire de Grenoble**
====================================================

- https://www.grenoble.fr/uploads/Externe/09/1601_209_CP-Grenoble-reaffirme-son-soutien-a-la-revolte-iranienne.pdf

"J'ai rencontré hier les associations iraniennes locales pour réaffirmer
la solidarité et le soutien de Grenoble au mouvement Femme Vie Liberté
du peuple iranien. Nous nous tenons à leurs côtés dans ce combat pour
la liberté !"

Source: https://singapore.unofficialbird.com/EricPiolle/status/1618531693442510849#m


.. figure:: images/mairie_grenoble.jpeg
   :align: center


https://www.grenoble.fr/uploads/Externe/09/1601_209_CP-Grenoble-reaffirme-son-soutien-a-la-revolte-iranienne.pdf
=====================================================================================================================

- https://www.grenoble.fr/uploads/Externe/09/1601_209_CP-Grenoble-reaffirme-son-soutien-a-la-revolte-iranienne.pdf


::

    SERVICE PRESSE DE LA VILLE DE GRENOBLE / presse@grenoble.fr / 04 76 76 39 21 - 04 76 76 11 42


Ce mercredi 25 janvier 2023 Eric Piolle, Maire de Grenoble, a rencontré des associations iraniennes pour
exprimer sa solidarité, échanger avec elles et les soutenir dans leur combat pour la liberté.
Grenoble se tient aux côtés des Iraniens, des Iraniennes et des collectifs associatifs en lutte.

Le nom de Mahsa Jina Amini est désormais dans toutes les mémoires : arrêtée le 13 septembre 2022 à
Téhéran par la police des mœurs pour « port de vêtements inappropriés », la jeune femme Kurde
de 22 ans est décédée suite à la torture qu’elle a subie. Ce drame a déclenché des manifestations
historiques en Iran, et une répression féroce de la part des autorités. A Grenoble, la communauté
iranienne s’est fortement mobilisée, en créant de nouveaux collectifs, en partenariat avec la Ligue
des Droits de l’Homme, Amnesty ou la CISEM. Depuis le début de la révolte, Grenoble se tient
aux côtés du peuple iranien et soutient leur combat pour la liberté.

Ce mercredi 25 janvier 2023, Eric Piolle, Maire de Grenoble, a rencontré des associations iraniennes pour
exprimer sa solidarité, échanger avec elles et les soutenir dans leur combat pour la liberté. Grenoble
se tient aux côtés des Iraniens, des Iraniennes et des collectifs associatifs en lutte.

Le 16 novembre 2022, Grenoble, en partenariat avec la Ligue des Droits de l’Homme (LDH), Grenoble
Alpes Métropole, la LDH Iran et Voix de l’Iran CISEM, a organisé une soirée d’information sur la révolte ira-
nienne en cours (« Mourir à 20 ans, Femme, Vie, Liberté »). Behrooz Farahani, représentant de Solidarité
Socialiste avec les travailleurs en Iran, et Madame S. Zari, représentante de « Voix de l’Iran » sont interve-
nues lors de la conférence. 130 participant-es y ont assisté

La rencontre de ce jour a été l’opportunité de réfléchir ensemble à d’autres formes de collaboration pour
davantage visibiliser la lutte pour la liberté, en particulier celle des femmes.


D'autres événements d'information et de soutien sont à venir :
---------------------------------------------------------------------

- Mardi 21 février 2023  : Organisation d’une conférence avec Jean Marcou, Professeur des Universités et
  Directeur des Relations Internationales à Sciences Po Grenoble, pour un point géopolitique sur la sous-région.
- Mardi 7 mars 2023 : Lancement d’une exposition sur l’Iran à la Maison de l’international, conçue par
  Roya Vergain, membre d’un des collectifs locaux. En présence de la formation musicale Sazet Avaz.
  Grenoble soutient historiquement les associations iraniennes de Grenoble. Elle héberge dans des locaux
  municipaux l’association Rencontres des cultures franco-iraniennes. La Ville est par ailleurs engagée pour
  la liberté et le respect des droits des hommes et des femmes à l’international. Chaque 10 octobre, pour
  protester contre la peine de mort dans le monde, l’Hôtel de Ville accueille des expositions et des conférences à ce sujet.
