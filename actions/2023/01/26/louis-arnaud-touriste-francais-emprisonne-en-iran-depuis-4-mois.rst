
.. _louis_arnaud_2023_01_26:

====================================================================================
2023-01-26 **Louis Arnaud, touriste français emprisonné en Iran depuis 4 mois**
====================================================================================

- https://www.lepoint.fr/monde/louis-arnaud-touriste-francais-emprisonne-en-iran-depuis-4-mois-26-01-2023-2506335_24.php#xtor=CS2-239

par `Armin Arefi <https://www.lepoint.fr/journalistes-du-point/armin-arefi>`_


Introduction
==============

Les parents du jeune homme de 35 ans, arrêté le 28 septembre 2022 à Téhéran,
ont dévoilé son identité. Sept Français sont incarcérés en Iran.


Il s'appelle Louis Arnaud et est détenu dans la prison Evin de Téhéran.

La famille de ce touriste français de 35 ans, qui se trouvait en Iran
depuis le 2 septembre 2022 dans le cadre d'un tour du monde entamé un
mois et demi plus tôt à partir de la France, a décidé de briser le
silence dans un communiqué transmis ce 26 janvier à l'Agence France-Presse.

On y apprend que le jeune homme, consultant en services financier à Paris,
a été interpellé dans la capitale iranienne le 28 septembre 2022, soit
douze jours après la mort de Mahsa Amini qui a provoqué un soulèvement
populaire sans précédent en Iran.


« Connaissant avec certitude ses propres engagements et ayant eu quelque
écho de témoignages de ses compagnons de route, nous sommes sûrs que
notre fils Louis n'a pris part à aucune manifestation ni exprimé d'idées
hostiles à l'Iran, à son gouvernement ou à l'islam », assurent ses parents
Jean-Michel et Sylvie Arnaud dans le communiqué.

« Notre fils n'est ni comploteur, ni espion, ni malfaiteur. Il est un
simple citoyen du monde, qui souhaite le parcourir pour mieux le connaître
et le comprendre. »

Le gouvernement français investi
=====================================

Incarcéré dans des « conditions de détention très rudes », Louis Arnaud
a pu recevoir une fois la visite de l'ambassadeur de France à Téhéran,
Nicolas Roche, le 11 décembre 2022. Il a également pu s'entretenir à
deux brèves reprises au téléphone avec ses parents au mois d'octobre.

« Nous sommes conscients de l'investissement du gouvernement français et
de la ministre de l'Europe et des Affaires étrangères pour l'amélioration
des conditions de détention et la libération des otages français »,
écrivent ces derniers dans le communiqué.

« Mais nous voulons alerter et mobiliser plus largement le public sur la
situation de nos compatriotes et de notre fils, car nous voyons bien
que l'idée de rester discret et silencieux ne fonctionne pas. »

Louis Arnaud est donc le sixième détenu français dont le nom a été rendu
public après le cas de la chercheuse Fariba Adelkhah, emprisonnée depuis
juin 2019, du touriste Benjamin Brière, arrêté en mai 2021 alors qu'il
manipulait un drone de voyage dans un parc naturel, des touristes et
syndicalistes Cécile Kohler et Jacques Paris, interpellés en mai 2022
après avoir rencontré des collègues iraniens impliqués dans des manifestations,
et de Bernard Phelan, consultant en tourisme franco-irlandais de 64 ans,
arrêté le 3 octobre 2022. Un septième Français, dont l'identité n'est
pas connue, est emprisonné dans le pays.

« Diplomatie des otages »
==============================

Leur cas entre dans le cadre de la « diplomatie des otages », une pratique
de longue date de la République islamique, qui vise à arrêter arbitrairement
des ressortissants étrangers sur son territoire pour s'en servir de monnaie
d'échange sur des sujets de contentieux avec leur pays d'origine.

« Ces arrestations visent à faire pression sur la France pour obtenir de
notre part des concessions sur tel ou tel dossier », explique un diplomate.
C'est ainsi que l'universitaire français Roland Marchal, emprisonné en
juin 2019 à Téhéran, n'a été libéré que neuf mois plus tard en échange
de Jalal Rohollahnejad, un ingénieur iranien qui était détenu en France
sur ordre de la justice américaine.

Les prises d'otages de citoyens européens, et plus particulièrement de
ressortissants français, se sont accélérées depuis l'interpellation en
Allemagne le 1er juillet 2018 d'Assadollah Assadi, un diplomate espion
iranien impliqué dans une tentative d'attentat avortée contre un
rassemblement d'opposants iraniens à Villepinte, en banlieue parisienne,
le 30 juin 2018.

Officiellement troisième conseiller de l'ambassade d'Iran à Vienne, cet
Iranien de 50 ans agissait en réalité sous couverture pour le ministère
iranien du Renseignement. Le tribunal d'Anvers l'a condamné en juin 2021
à vingt ans de prison pour terrorisme. Depuis, le régime des mollahs
fait tout pour récupérer son agent secret.

Valeurs universalistes
============================

La récente vague d'arrestations de Français en Iran répond aussi à la
volonté du régime iranien, en proie à une révolte intérieure sans précédent,
de renforcer auprès de ses partisans l'idée d'un complot ourdi par
l'Occident. « Le complot américano-britannique ne suffit plus », ironise
un autre diplomate.

« La France demeure suffisamment importante à leurs yeux pour justifier
la théorie du complot sans prendre de risques inconsidérés.

Le pays représente aussi une certaine identité universaliste… » Des valeurs
qui séduisent une grande partie de la jeunesse iranienne, au grand dam
des ayatollahs.

