.. index::
   pair: Maire; Grenoble

.. _panahi_2023_01_27:

===============================================================================================================================
2023-01-27 **«Sommes-nous heureux ? Nous sommes anéantis…» Lettre de Tahereh Saeedi à son mari emprisonné, Jafar Panahi**
===============================================================================================================================

Le cinéaste iranien est détenu depuis cet été à Téhéran.

Ses proches oscillent entre moments d’optimisme et retours cruels à la
réalité. Après un énième espoir de délivrance déçu, son épouse a décidé
de sortir du silence.


Depuis une semaine, on lisait ici et là qu’il allait sortir de prison
sous caution, comme le signe d’un relâchement du régime des mollahs.

Le cinéaste Jafar Panahi est en taule depuis deux cents jours et la
lumière au bout du tunnel semble d’ores et déjà un leurre auquel sa
propre épouse ne peut plus croire, ayant été trop baladée de mensonges
en atermoiements, de textes implacables en négociations de couloirs,
guidés par la peur et le cynisme de ceux qui, sous le masque de la loi
et de dieu, décident selon leur bon plaisir ou les nécessités de leur
maintien au pouvoir.

C’est la raison pour laquelle Tahereh Saeedi a décidé de sortir de son
silence pour poster sur son compte Instagram un message désespéré qui
raconte son combat mais dit aussi à quel point, en Iran, ce qui pourrait
être dans tout autre contexte ou pays le gage d’un privilège, d’un espoir
d’immunité, joue ici en défaveur du cinéaste.
Car ce que le régime déteste par-dessus tout dans son entreprise totalitaire
de muselage généralisé, ce sont les voix qui portent et qui peuvent fédérer
autour d’elles la colère et l’action.


Les soulèvements en Iran sous la bannière révolutionnaire et féministe
de «Femme, vie, liberté», depuis la mort de Mahsa Amini en septembre,
couvaient déjà au printemps dans les multiples actions (grèves, manifestations…)
ayant suivi notamment l’écroulement mortel de la tour Metropol à Abadan
dans le sud-ouest du pays.

C’est dans ce contexte que furent jetés en prison les cinéastes Mohammad
Rasoulof et Mostafa Al-Ahmad qui réclamaient juste dans une pétition que
les forces de l’ordre n’utilisent pas leur arme contre les citoyens défilant
dans les rues.
A leur suite, c’est Jafar Panahi qui s’est rendu à la cour de justice de
Téhéran pour plaider leur cause et réclamer leur libération.
Bien mal lui en a pris une fois encore : son courage indéfectible lui a
valu d’être jeté dans les geôles de la prison d’Evin à Téhéran sans autre
forme de procès, le pouvoir se réclamant d’une peine ancienne, lui qui
fut arrêté puis condamné en 2010 à six ans de prison et vingt ans
d’interdiction de réaliser ou d’écrire des films, de voyager ou même de
s’exprimer dans les médias pour «propagande contre le régime», après avoir
soutenu le mouvement de protestation de 2009 contre la réélection de
l’ultraconservateur Mahmoud Ahmadinejad à la présidence de la République islamique.

Détenu pendant deux mois en 2010 alors qu’il était invité à être membre
du jury au Festival de Cannes, il fera une grève de la faim et sera
finalement libéré sous caution le 25 mai et vivait depuis lors sous un
régime de liberté conditionnelle pouvant être révoqué à tout instant.

C’est cette sanction qui a servi de justification purement formelle à sa
mise à l’ombre. Encore plus spectaculaire que celle de ses deux infortunés
collègues car Panahi, 62 ans, est sans conteste le cinéaste iranien le
plus connu et honoré au monde avec ses prix récoltés dans les plus grands
festivals internationaux (le Cercle a reçu le lion d’or à Venise en 2000,
puis Taxi Téhéran, l’ours d’or à Berlin en 2015…).

Il fut aussi honoré en 2012 par le Parlement européen du prix Sakharov
pour la liberté de l’esprit. Il s’agit clairement d’un dissident de
l’intérieur qui s’est toujours refusé à quitter son pays, ne fût-ce que
clandestinement, comme il le met sublimement en scène dans son dernier
long métrage, Aucun Ours, sorti en France en novembre, dans lequel il
interprète le rôle d’un cinéaste sous surveillance qui se voit offrir un
passage de frontière nocturne sans (trop) de danger.
Quand il franchit la ligne imaginaire, il sursaute, comme horrifié, et
fait demi-tour, quitte à affronter dans la suite du récit d’autres
horreurs, une adversité pleine d’arbitraire et d’angoisse.


Sommes-nous heureux ? Nous sommes anéantis…
=================================================

«Nous sommes heureux. Ou, devrais-je dire, nous étions heureux…

Il y a un an, le verdict de six ans de prison de Jafar datait d’une décennie
et nous pensions qu’il ne retournerait plus en prison.
Selon la loi même par laquelle il avait été jugé, une sentence non exécutée
dans les dix ans devient obsolète.

Mais… le 11 juillet dernier, jour de son anniversaire, j’ai été informée
qu’il avait été incarcéré au nom d’un verdict rendu, donc, il y a onze ans.

Nous avons oublié pourquoi nous étions heureux…

Jusqu’au jour où M. Amir Salar Davoodi, avocat spécialiste des droits
de l’homme, et le compagnon de cellule de Jafar à la prison d’Evin,
lui a rappelé toutes les spécificités de cette loi.

Sommes-nous donc à nouveau heureux ?

Saleh Nikbakht et Youssef Molayi, les avocats de Jafar, ont plaidé
inlassablement sa cause, en utilisant des arguments irréfutables, et
ont convaincu la Cour suprême de son innocence et plaidé pour
l’annulation de ce verdict dépassé.

Jafar a donc été acquitté par la Cour suprême et son cas a été transféré
à la cour d’appel révolutionnaire numéro 21. Sommes-nous ainsi beaucoup
plus heureux ? Selon la loi, il doit être immédiatement libéré sous caution.

Et pourtant ? Le 3 décembre, nous avons été informés qu’une intervention
de l’administration de la sécurité avait été suivie de l’annulation du
jugement permettant la libération de Jafar.

Nous sommes anéantis…
-------------------------

La semaine dernière, nous avons été informés qu’il serait libéré dans une
semaine. Nous étions heureux à nouveau. Une semaine a passé, et Jafar
n’est toujours pas avec nous. Cela fait exactement 200 jours maintenant.

Nous sommes désespérés…

La libération de Jafar est en totale conformité avec leurs propres lois,
mais ils sont au-dessus des lois, sans aucun respect pour la loi.

Si en dépit de tout cela, vous nous demandez comment nous allons, et si
nous sommes heureux, nous vous répondrons que nous sommes heureux, et
que nous allons bien. Mais ne nous croyez pas sur parole.»


