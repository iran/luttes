
.. index::
   pair: Rassemblement ; Samedi 28 janvier 2023


.. _iran_grenoble_2023_01_28:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ 19e rassemblement samedi 28 janvier 2023 à Grenoble **place Félix Poulat** à 14h30 **Arrêt des exécutions, annulation des condamnations à mort libération des prisonniers politiques, expulsion de l'ambassadeur d'iran** 📣
==============================================================================================================================================================================================================================================================

Arrêt des exécutions, annulation des condamnations à mort liberation des prisonniers politiques, expulsion de l'ambassadeur d'iran
134e jour depuis la mort de #JinaMahsaAmini (2022-09-16)

- :ref:`mahsa_jina_amini`
- :ref:`slogans`

.. figure:: images/annonce_rassemblement.png
   :align: center

- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|


Texte annonçant le rassemblement du samedi 28 janvier 2023
==============================================================

Le mardi 13 septembre 2022 Jina Mahsa Amini était arrêtée par la police des mœurs
à Téhéran car elle n’était pas **"assez voilée"**.
Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

Nos Revendications
-------------------

- :ref:`revendications_mediapart`

.. include:: ../../../../revendications/revendications_2023_03_07.txt


Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.


**Cette manifestation est organisée par la communauté Iranienne**.

..  _texte_despoir_2023_01_28:

Texte d'espoir
================

La rupture entre du peuple et le pouvoir est totale et irréversible.

Le régime n’a plus les moyens économiques nécessaires pour répondre,
même partiellement, aux demandes sociales, et une partie de la classe
dominante n’est plus solidaire avec le Guide et ses positions extrêmes.

Le régime et son appareil de répression sont affaiblis.
Une fracture à l’intérieur de l’appareil de l’état est plus que visible
Par conséquent, l’émergence de nouvelles vagues de protestations et de
grèves est inévitable.

À cela s’ajoute une crise écologique, surtout une crise de l’eau
disponible, qui a déjà provoqué des émeutes dans un passé récent.
Et aussi une crise idéologique et morale due à la corruption systémique et
au pillage des ressources du pays par une poignée des puissants.
**Une liste non exhaustive !**

Le mouvement de révolte en cours, même affaibli, voire défait momentanément,
est appelé à se redresser.

On entre dans le cinquième mois de la révolte. La baisse récente d’intensité
des protestations ne doit pas tromper. Les protestations prennent d’autres
formes plus sur la base sociale et économique et social.
Comme la grève des titulaires du secteur pétrolier le 17 janvier 2023.

Le feu sous les cendres est en train d’œuvrer . Nous sommes plus que
jamais au seuil d’une révolution.


Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`soutiens_avocats_2023_01_10`

Les victimes de la barbarie des mollahs
--------------------------------------------

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|

Les otages français
=======================

.. include:: ../../../../repression/otages/liste_2023_02_09.txt


Quelques événements du 22 janvier 2023 au 28 janvier 2023
=============================================================================

- :ref:`panahi_2023_01_27`
- :ref:`maire_grenoble_2023_01_25`



Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/
- https://www.ici-grenoble.org/evenement/rassemblement-pour-larret-des-executions-en-iran-lannulation-des-condamnations-a-mort-la-liberation-des-prisonniers-politiques

.. figure:: images/annonce_ici_grenoble.png
   :align: center


Quelques photos
=================

.. figure:: images/zoya_zoreh.jpeg
    :align: center

.. figure:: images/zoya_marjane.jpeg
    :align: center

.. figure:: images/ladan_marjane.jpeg
    :align: center


Vidéos peertube
==================

- https://sepiasearch.org/search?search=grenoble+iran

Discours de Z. **Nous sommes plus que jamais au seuil d’une révolution**
-----------------------------------------------------------------------------------------------------------

- :ref:texte_despoir_2023_01_28`
- :ref:iran_cinquieme_mois_2023_01_25`
- https://www.orion-hub.fr/w/bpWV1e56vNDWCp4L7cESXg

Le feu sous les cendres est en train d’œuvrer . Nous sommes plus que
jamais au seuil d’une révolution.


Baraye
------------

- :ref:`baraye`
- https://www.orion-hub.fr/w/qsyTaTVWSzbGtdzJ2Bhdc4


Autres événements ce samedi 28 janvier 2023 à Grenoble
============================================================

.. figure:: images/un_samedi_4_manifs.png
   :align: center

   https://www.ici-grenoble.org/article/un-samedi-quatre-manifs


.. figure:: images/ici_grenoble_2023_01_28.png
   :align: center

   https://www.ici-grenoble.org/agenda


Manifestations précédentes à Grenoble
=======================================


- :ref:`iran_grenoble_2023_01_21`
- :ref:`iran_grenoble_2023_01_14`
- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


