.. index::
   pair: Journaliste ; Nazila Maroofian

.. _nazilla_maroofian_2023_01_28:

===============================================================================================================================
2023-01-30 **Iran: Une journaliste kurde condamnée à deux ans de prison pour avoir publié une interview du père de Jîna Amini**
===============================================================================================================================

- https://rojinfo.com/iran-une-journaliste-kurde-condamnee-a-deux-ans-de-prison-pour-avoir-publie-une-interview-du-pere-de-jina-amini/
- https://rojinfo.com/iran-une-journaliste-arretee-pour-avoir-interviewe-le-pere-de-jina-amini/


.. figure:: images/nazilla_maroofian.png
   :align: center

La journaliste kurde Nazila Maroofian a été détenue pendant près de trois
mois à Téhéran suite à la publication d'une interview du père de Jîna
Mahsa Amini.

**Elle vient d'être condamnée par la justice iranienne à deux ans de prison
avec sursis**.

La journaliste Nazila Maroofian a été condamnée à deux ans de prison en
Iran pour « propagande anti-étatique ». Elle avait publié en octobre dernier
une interview du père de Jîna Mahsa Amini, qui rejetait la version officielle
sur les circonstances du décès de sa fille.

Une journaliste kurde d’Iran a été condamnée à deux ans de prison avec un
sursis de cinq ans.
Selon plusieurs groupes de défense des droits humains, Nazila Maroofian
a été reconnue coupable de « propagande anti-étatique » et de
« diffusion de fausses informations ».

Le verdict a été prononcé  par un tribunal de Téhéran en l’absence de la
jeune femme de 23 ans qui a été condamnée par ailleurs à une amende
équivalente à 310 euros et une interdiction de quitter le pays pendant cinq ans.


Maroofian, qui vit à Téhéran, est originaire de la ville de Seqiz, dans
le Kurdistan oriental, ville natale de Jîna Mahsa Amini, dont la mort a
déclenché une révolte connue sous le mot d’ordre « Jin, Jiyan, Azadî ».

La jeune femme de 22 ans est décédée trois jours après une garde à vue
par la police des moeurs de Téhéran pour « port incorrect du hijab »
le 13 septembre 2022.
Selon des témoins, elle y a été violemment battue, notamment à la tête,
suite à quoi elle a perdu connaissance. Transportée dans un hôpital,
Amini a été déclarée morte officiellement le 16 septembre.
Le régime a fait circuler la version selon laquelle elle souffrait d’un
problème cardiaque qui aurait causé sa mort. La clinique de Téhéran, où
Amini était dans le coma, a déclaré après sa mort qu’elle était en état
de mort cérébrale au moment de son admission.

Le 19 octobre, Maroofian a publié une interview du père de la victime,
**Amjad Amini**, sur le site Web Mostaghel.
Amjad Amini y rejetait la version officielle selon laquelle sa fille
souffrait d’une maladie. Le titre de l’article était sans ambiguïté :
« Le père de Mahsa Amini : ‘Vous mentez !’ ».

Quelques jours plus tard, Maroofian a été arrêtée à Téhéran et emmenée
dans le tristement célèbre centre de torture qu’est la prison Evin.

Bien que souffrant de problèmes cardiaques, la journaliste n’a été
libérée qu’à la mi-janvier, après paiement d’une caution.

