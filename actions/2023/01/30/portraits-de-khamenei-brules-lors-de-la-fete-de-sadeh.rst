
.. _khamenei_brule_2023_01_30:

===========================================================================================
2023-01-30 **Des portraits de l’ayatollah Khamenei, guide suprême iranien, sont brûlés**
===========================================================================================

- https://nitter.poast.org/IranHrm/status/1620315043186475008#m


DÉTOURNEMENT. Des portraits de l’ayatollah Khamenei, guide suprême iranien,
sont brûlés par des contestataires réunis lundi 30 janvier sur le Mont Tochal
qui surplombe Téhéran, à l’occasion de la fête pré-islamique de Sadeh
qui célèbre l’apparition du feu. #Iran #MahsaAmini


- https://masthead.social/@NaderTeyf/109779285042681646


Se servant d'une fête païenne qui s'appelle Sadeh, les jeunes sont
redescendu.es dans les rues de certaines villes en #Iran en scandant:
"Mort au dictateur" et "Pauvreté, corruption et la vie chère/Nous irons
jusqu'au renversement du régime."

Les villes de #Sanandaj, #Kerman et #Machhad (le clip) sont des endroits
où des protestations ont eu lieu ce soir.

Les femmes, nombreuses à Kerman, entonnaient #Femme_Vie_Liberté autour
des feux de paille propres à cette fête.

