.. index::
   pair: 2023-01-31 ; Des nouvelles du front

.. _nouvelles_iran_2023_01_31:

====================================================================================
2023-01-31 Des nouvelles du front **Nouvelles d’Iran du 18 au 29 janvier 2023**
====================================================================================


- http://dndf.org/?p=20449



Introduction
====================

Dans cette atmosphère de recul d’un soulèvement populaire qui a commencé
il y a 4 mois, il semblerait que le Kurdistan et le Baloutchistan ont
décidé de jouer les prolongations et de garder intact le feu de ce
soulèvement malgré une répression inédite et un froid exceptionnel.

Dans les autres coins de l’Iran, malgré les slogans nocturnes dans les
quartiers de grandes villes, les graffitis sur les murs, les mouvements
de protestation au sein des écoles, des lycées et des universités et
des grèves et rassemblements ouvriers éparpillés dans le pays, les
manifestations et les rassemblements populaires s’amenuisent et le
régime, profitant du froid et de la crise de l’énergie qui, physiquement
crée une diversion dans les possibilités d’action et les objectifs de la
population, essaie d’accélérer et de fortifier sa répression pour calmer
le soulèvement tout en poursuivant ses efforts de normalisation notamment
en continuant ses cérémonies annuelles comme le Festival artistique de Fajr.


Kurdistan, Baloutchistan, l’œil et lumière de l’Iran !
==========================================================

Néanmoins à Zāhedān et à Rask les baloutchs continuent leurs manifestations
hebdomadaires, les vendredis, pour démontrer leur détermination et pour
essayer d’éveiller les autres peuples et villes du territoire ; et ce,
malgré un froid avoisinant les moins 10.

Le régime qui constate que cette région, à côté du Kurdistan, est devenue
un bastion de la révolte, a depuis deux semaines intensifié sa répression
en essayant d’arrêter toutes les personnes qui, d’une façon ou d’une autre,
étaient identifiées en tant que meneur des rassemblements et des manifestations.

Le vendredi 20 janvier 2023, encore une fois, on a vu une manifestation
à Zāhedān, après la prière de midi, aux alentours de la mosquée Makki,
qui était menacé de destruction, Molavi Abdol-Hamid, imam de vendredi
de la ville, de confession sunnite, a encore une fois exigé la poursuite
des personnes qui avaient tiré sur la foule le 30 septembre 2022 et tué
plus de 90 baloutchs ; aussi il a demandé la modification de la constitution
iranienne.

Il a notamment déclaré que “notre constitution n’a pas bougé depuis 44 ans;
il serait temps que cette constitution change. Comment peut-on fixer des
lois et des règles pour les jeunes quand on n’écoute plus la jeunesse
du pays ? Il faut mettre au goût du jour cette constitution pour que ça
puisse correspondre à notre époque”.

Ce sont très exactement des propos que la République islamique n’a aucune
envie d’entendre. Les forces de l’ordre, à plusieurs reprises, ont bloqué
le parcours des manifestants en essayant de fouiller les gens et de créer
des coupures dans leur rang.

La République islamique a augmenté la pression sur les responsables
religieux sunnites dans toute la province, également à la ville de Rask,
l’imam de vendredi de cette ville est interdit d’y tenir des rassemblements.

Abdol-Ghaffar Naghshbandi, qui était l’imam de vendredi provisoire de la
ville, a été convoqué par la police et depuis on a plus de nouvelles de
sa situation.

La semaine avant c’est arrestation, il avait déjà prédit que « je ne sais
pas ce qui va m’arriver cette semaine, mais ce qui est sûr, c’est que
l’État doit arrêter de réprimer les baloutchs.

On avait dit qu’à un moment, la rumeur de la destruction de la mosquée
principale de Zāhedān qui s’appelle la mosquée “Makki” était répandue;
ce qui avait suffi pour que des dizaines de personnes se portent
volontaires pour la défendre; ils y ont monté la garde jour et nuit;
entre-temps, la semaine dernière, 17 personnes parmi ces “gardes” ont
été arrêtés par le régime.

Au nord du pays, à Galikesh, dans la province de Golestân, les baloutchs
qui y résident, à côté des turkmens qui sont eux aussi de confession sunnite,
ont vu leur imam, Mohamad-Hossein Molavi Gorgui l’ex l’imam de vendredi
de Azadshahr mis sous pression; il a été convoqué et destitué auprès du
tribunal spécial du clergé.

Les Baloutchs et les turkmens de cette région ont donc manifesté pour
demander sa libération.

Le vendredi 27 janvier, à Zāhedān, sous la neige, des milliers de personnes
se sont rassemblées et ont continué leur manifestation avec les slogans
maintenant habituels de “Mort à Khamenei !” “De Zāhedān à Īzeh, je donne ma vie pour l’Iran !”…

Il faut voir dans la répression de ces deux dernières semaines, la main
du nouveau gouverneur qui a été nommé et qui veut faire une démonstration
de force en éteignant ces dernières flambées du soulèvement.

A Galikesh, la mobilisation de la semaine dernière a eu du succès et a
pu libérer Gorgui de son assignation à domicile ; lui qui n’avait pas
le droit de faire de discours, s’est présenté ce vendredi 27 dans le
rassemblement ; c’est un recul du régime dans cette région ; le fait
même de le laisser discourir en diapason avec le peuple baloutch et
Molavi Abdul Hamid à Zāhedān constitue une victoire.
Il était accompagné par un certain nombre de ses fidèles vêtus d’un
linceul blanc pour démontrer leur détermination à défendre leur guide.


Les habitants de Taftân ont de nouveau arrêté les activités de la mine d’or dans cette zone
============================================================================================

La ville de Taftân est située au pied du volcan du même nom qui culmine
à plus de 4000m ; La ville qui est dans la province de Sistan et Baloutchistan
est à 100km de Zāhedān.

On a su que le 13 janvier 2023 et pour la deuxième fois, les habitants
de Taftân se sont à nouveau rassemblés sur la route de la mine d’or
d’Anjirak de Taftan qui fait partie des mines les plus riches de l’Iran,
pour protester contre l’extraction de l’or dans cette région et ont
empêché les mineurs de poursuivre leur travail.

Des militants baloutchs ont rapporté que la mine d’or de Taftân est
exploitée par la société Aria South”, alors qu’aucun permis n’a été
délivré pour cela par les organisations environnementales.

Ce n’est pas la première fois que les habitants de Taftân protestent
contre les activités prédatrices de cette mine, et en octobre et novembre 2022,
ils avaient empêché la poursuite de l’exploitation minière à plusieurs
reprises en se rassemblant devant la mine, en récupérant des machines
et en bloquant les chemins qui y mènent.

Mais aujourd’hui, en même temps que les vendredis de résistance à Zāhedān,
les habitants de Taftân ont maintenu un autre bastion de résistance afin
de ne pas laisser la république Islamique piller leurs terres ancestrales.

Les cérémonies de La quarantaine à Īzeh
==========================================

Comme nous l’avons déjà indiqué, malgré le fait que la République islamique
n’a pas rendu les corps de deux personnes tuées à l’occasion du soulèvement, à
Īzeh (Khouzestân) la population dans le petit village de Persilla, a
néanmoins organisé cette cérémonie sans le corps de Hossein Saïdi le vendredi 27 janvier.

Ils avaient déjà organisé la cérémonie de quarantaine de Mahmoud Ahmadi
martyr du peuple Qashqai de la même façon, sans corps et en enterrant
ses vêtements traditionnels.

Rien n’entame la détermination du peuple Bakhtyari pour défendre le
soulèvement contre la République islamique.

Rassemblement à Torbat-e Djam
================================

Le mardi 17 janvier, dans la journée, à Torbat-e-Djam, une ville du
nord-est du pays (100.000hab.), dans la province du Khorassan-e–Razavi
qui est située à 165 km au sud de Mashhad, la population s’est rassemblée
au centre-ville et s’est plainte contre la crise du gaz et le fait qu’il
n’arrive plus à chauffer leur logement.

Des femmes qui racontaient leur galère à chauffer des enfants en bas
âge ou des jeunes adultes qui se plaignaient de ne pouvoir chauffer
leur mère qui doit s’asseoir sous 5 couvertures, mais qui tombe néanmoins
malade. Un certain nombre d’entre eux ont pensé acheter des appareils de
chauffage électrique mais le prix de ces appareils a augmenté en flèche
et il est devenu très difficile de s’en procurer.

Une fois que les magasins ont été vidés de ce type de produit, les gens
se sont amassés devant la Croix-Rouge qui a refusé de leur en donner ;
après des essais de négociation avec les responsables, les gens y sont
entrés en force et ont pris tous les appareils électriques pour essayer
de chauffer chez eux. On était bien à Torbat-e-Djam, mais on avait
l’impression d’assister à des pillages à Los Angeles où les gens sortaient
des magasins avec des cartons de produits électriques.

Déjà la veille, ils s’étaient rassemblés devant le siège de la province
pour interpeller le gouverneur sur la question du chauffage.

La nuit tombée, les gens étaient de plus en plus nombreux et en colère.

Le porte-parole du gouverneur s’est pointé pour les calmer mais ça n’a
fait que faire monter la tension pour finir par des slogans de
« A mort gouverneur, démission gouverneur ! »

« Concitoyen, crie ton droit ! »

Le rassemblement s’est transformé en une manifestation nocturne et les
slogans se sont mutés en « A mort Khamenei ! »

Dans d’autres villes de Khorassan aussi, des scènes de révolte populaire
ont été constatés. Partout ça commence avec des gens qui sortent pour
trouver un moyen pour chauffer leur logement, et devant l’incapacité
des responsables à répondre à leur problème, ils manifestent contre le régime.


Grève des ouvriers à la Pétrochimie de Mahshahr
====================================================

Les ouvriers de la pétrochimie de Bandar (port) Mahshahr (Khuzestân)
sont entrés dans leur quatrième jours de grève pour obtenir les arriérés
de salaire et l’amélioration de leur condition de travail.

Ils se sont rassemblés devant l’usine pour protester contre la direction
qui reste sourde à leurs revendications.

La protestation des travailleurs de cette boite pétrochimique au cours
des derniers mois visait également le fait que l’entreprise avait transféré
300 personnes des employés, sous la supervision de Bandar Mahshahr
Non-Industrial Operations Company.
Ce transfert s’est traduit par la suppression de certains avantages tels
que les primes à la production et, par conséquent, la réduction des
revenus des travailleurs.

Un des ouvriers contractuels spécialisés, avec quinze ans d’expérience,
a témoigné lors d’une courte vidéo que « leur salaire est de 13 millions
de Tomans tandis que les organismes étatiques ont déclaré le seuil
de pauvreté à 30 millions.
Aussi, qu’il y a des Mollâs pour leur raconter des “balivernes” qui sont
embauchés sur des contrats avantageux en CDI. »

Grève des ouvriers et employés du Cuivre à Shahr-e-Babak
===========================================================

Les ouvriers et les employés du Holding “Mésse féléz ranguine” (cuivre-fer coloré)
qui fait partie d’un ensemble de compagnies minières de la ville de Babak
à proximité de la ville de Kerman (Sud-ouest du pays) se sont mis en grève.

Les historiens pensent que la ville de Babak a été construite par Ardeshir Babakan,
le plus célèbre des rois sassanides, il y a près de 1800 ans.

Maymand, l’un des 4 villages les plus anciens d’Iran, se trouve à 36 km
de Shahr-e-Babak. Sarcheshmeh et Miedook, les plus grandes mines de
cuivre d’Iran, sont situées autour de cette ville.

Rangin Copper Metal Company a été créée en juin 1999, et a commencé ses
travaux par la mise en place d’une usine de traitement de concentré de
cuivre près de la mine de cuivre de Miedook dans la ville de Babak.
En 2004, avec la formation de la Daryaban Gohran Company, qui est la
principale exploitation du groupe, plusieurs autres entreprises manufacturières
ont été incluses dans cette société.
L’une des fonctions de cette société est l’extraction et l’exploitation
de la mine de cuivre et la production de concentré de cuivre.

Nous savons que les ouvriers du cuivre de KhatounAbad depuis des années
sont en lutte pour régulariser leurs conditions de travail précaires.

Déjà en 2003, à  KhatounAbad à proximité de Shahr-é-Babak, province de
Kerman, l’employeur de la mine de cuivre a licencié 200 ouvriers du
bâtiment du complexe de cuivre en construction, à qui il avait promis
de les embaucher comme mineurs après l’achèvement des travaux de construction.

Mais une fois les travaux terminés, il ne respecte pas sa promesse.
Les travailleurs licenciés se mettent en grève et font un sit-in avec
leurs familles pour protester contre cette décision.
Mais cette grève et ce sit-in ne donne pas de résultat. Pour attirer
l’attention, les ouvriers bloquent la route entre KhatounAbad et Babak.
Mais au lieu de répondre aux revendications des travailleurs, les forces
du régime attaquent les travailleurs avec des tirs à partir d’un hélicoptère.

Un des ouvriers est mort sur le champ. Les travailleurs se dirigent alors
vers le siège du gouverneur de la ville de Babak. Les autres citadins
les rejoignent. Les forces répressives envoyées par le gouvernement
(réformiste à l’époque!) attaquent les travailleurs.
Dans cette attaque brutale, au moins 4 travailleurs nommés et un étudiant
ont été tués et des centaines d’autres blessés.

Plus récemment, en 2016 aussi, plus de 200 personnes étaient rassemblées
durant plusieurs jours devant le siège du gouverneur. Leur revendication
était toujours l’obtention des contrats fixes en CDI pour sortir de la précarité.

Grève de l’aqueduc de MeshguinShahr
========================================

Samedi 28 janvier, une grève illimitée des travailleurs de l’aqueduc de
Meshgin Shahr a commencé pour protester contre le non-paiement de 9 mois
de salaires et les promesses non respectées des autorités.

Ils ont écrit : « Nous, 28 des ouvriers de l’aqueduc de Meshgin Shahr
avec des emplois spécialisés de plombier, coloriste et dessinateur de
contours, qui travaillons dans les villages de la ville, n’avons pas
reçu de salaire depuis plus de 9 mois.

Il n’y a pas si longtemps, le PDG d’Ardabil Province Waterworks nous avait
promis que les arriérés seraient payés en moins d’une semaine, mais cette
promesse, une fois encore n’a pas été tenue. »

L’un des travailleurs de Meshgin Shahr Waterworks a également déclaré à
un journaliste d’un média local : « Les travailleurs utilisent des
véhicules personnels pour effectuer des missions dans les villages aux
alentours, de sorte qu’en raison du non-paiement des salaires, ils n’ont
même pas l’argent nécessaire pour remplir leur réservoirs d’essence et
partir en mission;

C’est pourquoi, nous nous sommes unis et nous avons décidé d’arrêter de
travailler à partir d’aujourd’hui, samedi 28 janvier, dans un mouvement
de protestation et jusqu’à ce que nous recevions nos droits légaux.
Cette fois-ci nous n’écoutons plus les belles promesses. »

Grève des ouvriers de production d’huile alimentaire Jahan
==============================================================

Le samedi 28 janvier, les travailleurs de l’huile végétale de “Jahan”
ont annoncé leurs protestations contre le chômage avec des revendications
en suspens et des promesses non tenues des fonctionnaires de l’Etat.

L’un des ouvriers de l’huile végétale de Jahan a déclaré à un journaliste :
« Avant le licenciement, nous travaillions avec 80 autres ouvriers dans
cette unité de production d’huile alimentaire, mais finalement, au
début du second semestre de cette année, 60 d’entre nous ont été licenciés
par l’employeur de Jahan Oil et depuis, nous cherchons partout pour
trouver un emploi. »

Il a déclaré que « l’ancien employeur a soi-disant “ajusté” le surplus
de main-d’œuvre après l’arrêt de l’exploitation de l’usine, en licenciant
tous les ouvriers qui étaient sur des contrats précaire d’un mois;
13 de nos collègues ont pris la responsabilité de protéger et de garder l’usine. »

« Parmi les travailleurs licenciés, il y a des personnes qui ont des
anciennetés de 15 ans.

Bien que nous recevions actuellement une assurance-chômage, mais dans
les conditions économiques actuelles, il est devenu très difficile de
faire face aux dépenses de subsistance, nous nous inquiétons pour notre
futur, car il n’y a plus de travail…

Après cinq mois de chômage, nous n’avons pas été en mesure de trouver
un nouvel emploi pour subvenir à nos besoins et à ceux de nos familles,
et les autorités provinciales et municipales ne nous ont pas aidés dans
nos recherches.

Chacun des chômeurs a déposé une plainte distincte auprès du Département
du travail de Zanjan pour recevoir des demandes d’assurance-chômage et
des arriérés, et cela fait maintenant trois mois que leurs règlements
ont été décidés. »

Ce travailleur de l’huile végétale de Jahan a parlé des arriérés qui
sont en suspens: « après la fin de la relation de travail, ils restent
des arriérés comprennent quatre à cinq mois de salaires impayés ainsi
que des heures supplémentaires et du travail de nuit, des trajets
domicile-travail, des indemnités de déjeuner et d’habillement.

Le règlement de ces arriérés passe par le département du travail qui
ralenti leur application. »

Le budget 1402
====================

Raïssi, Président de la République a fait un discours au Parlement en
promettant monts et merveilles. Que le dollar, l’or, l’inflation … va
baisser dans les semaines et mois à venir.

L’inflation d’après le centre des statistiques en Iran a dépassé les 46%
ce qui est le plus grand chiffre depuis 27 ans.

Le dollar est à 45000 Tomans.

C’est dans cette situation que le parlement a décidé du budget qui a une
augmentation de 40% par rapport à l’année dernière.

Les organismes étatiques vont recevoir un budget plus important comme
le ministère du renseignement, l’armée des Pasdarans, la radio télé publique,
l’armée, le centre des services de Howsé, l’organisation des publicités islamique etc.

Par contre, d’autres lignes budgétaires n’ont pas bougé comme la lutte
contre la pollution atmosphérique.

A côté des augmentations de budget pour les organismes militaires,
sécuritaires, de répression et de propagande, on voit que la ligne
budgétaire concernant la protection des personnes souffrant d’un handicap
a purement et simplement disparu. Un rassemblement de protestation de
nombre d’handicapés a eu lieu devant le parlement ce 22 janvier.

Le budget pour la santé, l’enseignement, la protection de l’environnement
n’ont pratiquement pas bougé; le budget pour les maladies qui ont des
traitements difficiles a drastiquement baissé.

**Pour la République islamique seulement la répression et la propagande
justifient une augmentation de budget**.

Il faut bien que les gens paient pour être réprimés et éduqués.

Une nouvelle vague de privatisation
=======================================

Le gouvernement Raïssi a fait passer en catimini une loi sous l’appellation
de “Rendre productif les biens étatiques” pour mettre en vente l’équivalent
de 108 mille milliards de tomans des biens et des propriétés lui appartenant
pour essayer de combler le déficit budgétaire prévu  pour l’année 1402
(du 21 mars 2023 au 20 mars 2024), en fait, des dépenses non encore approvisionnées.

Sur proposition des chefs de pouvoir exécutifs, judiciaire et législatives
et avec l’accord du Guide, une commission de 7 personnes a été désignée
pour déterminer et conduire ces ventes qui ne sont que des privatisations
aux profit des proches et des copains.
Cette commission va travailler sous la houlette de M.Mokhber, adjoint de
M.Raïssi,  mais surtout le patron du fameux “Quartier général de l’exécution
du commandement de l’imam “.

L’État iranien a une longue expérience pour faire passer les biens publics
au secteur privé.  Il soi-disant “vend” ces biens à des tarifs ridicules
à ses proches en crédit.

Au moment de l’achat, l’acheteur ne paie en liquide qu’une petite partie
du prix et le reste doit être payé par mensualité sur plusieurs années.
Même cette avance est en général empruntée à une banque sans caution
sérieuse et par la recommandation d’une autorité militaire ou religieuse.
En plus ce “reste ” est oublié et personne ne vient les enquiquiner pour
le remboursement du crédit.
Une fois que l’acheteur a mis la main sur le bien vendu, il commence à
le démanteler et le revendre par lots.

S’il s’agit d’une entreprise de production, il entame la “restructuration”
en virant le plus grand nombre d’ouvriers et d’employés, en démantelant
les appareils productifs et en les vendant à prix cassé… souvent le
terrain et les bâtiments de l’entreprise sont vendus une fois qu’ils ont
obtenu un changement de destination ou d’affectation de la mairie du coin
pour transformer par exemple une usine agroalimentaire en terrain
bâtissable pour y construire des logements ou des habitations.

L’exemple parfait de ce genre de privatisation était l’usine agroalimentaire
de Haft-Tappeh dont on a déjà raconté les péripéties.

Souvent les personnes qui sont à l’origine de ces forfaitures ne peuvent
être inquiétées car ils exigent leur liberté d’entreprendre pour payer
le salaire des ouvriers.

En réalité, l’État veut faire rentrer un peu d’argent liquide dans ces
caisses mais surtout se débarrasser des biens ou des entreprises déficitaires
qui lui créent plus de problèmes que ce qui lui rapportent.

Cette fois-ci, disposant de l’expérience passée des privatisations et
tous les problèmes que celles-ci leur ont créés, en terme de poursuites
judiciaires, le gouvernement a décidé de trouver dès le départ une solution
en bloquant toute possibilité de recours ultérieur contre les décideurs
et même les exécutants ; autrement dit toutes les personnes qui ont
d’une façon ou d’une autre une implication dans ses privatisations ne
seraient jamais inquiétées par la justice et leurs propriétés ne seraient
pas mises en cause.

Et pour renforcer ce bouclier juridique, il a été décidé que toute
intervenant contre l’application de cette procédure peut être condamné
à 5 ans de prison.

Autrement dit si un responsable honnête quelconque des organes de protection
de l’État, un comptable ou un observateur des procédures légales de l’Etat …
trouve à redire sur un aspect d’une vente, trop manifestement contre le
bien public, il aurait lui-même des problèmes avec la justice !

Les otages occidentaux en Iran
====================================

Nous avons appris ce samedi 28 à travers une manifestation qui a eu lieu
à la place Trocadéro à Paris demandant la libération de 7 otages français
détenus en Iran, qu’il y a au total une quarantaine de ressortissants
étrangers notamment des franco-iraniens qui sont détenus depuis parfois
des années en Iran et qui risquent des peines qui peuvent être très
lourdes ; c’est la politique d’échange de prisonniers que la République
islamique a toujours poursuivie pour pouvoir continuer ses actions
terroristes sur le sol occidental ; ses agents peuvent toujours compter
sur un régime qui ne lésine pas sur les moyens pour les libérer et peut
aller jusqu’à prendre en otage de simples touristes ou de membres des
ONG pour pouvoir les échanger plus tard avec ses envoyés

Le festival des arts visuels Fajr : essai de normalisation
===============================================================

Le gouvernement islamique, de longue date, a essayé d’imiter tout ce que
faisait le régime du Shah, en y introduisant ses règles et ses coutumes islamiques.

Une des domaines dans lesquels le régime du Shah était actif, surtout à
travers le personnage de Farah Diba, “impératrice de l’Iran”, qui se
considérait comme grande amatrice d’art et connaisseuse en la matière,
était le festival de l’Art de Shiraz où étaient invité dans un faste
extraordinaire, accompagné de frais considérables, les meilleures artistes
internationaux du moment ; des réalisateurs de théâtre et du cinéma, des
compositeurs de premier plan, des chefs d’orchestre, des danseurs et
danseuses étoiles, des artistes peintres …

Il fallait proclamer face au monde moderne que nous sommes dans le coup.

Maurice Béjart faisait son ballet de Golestân, Stockhausen son concert
de musique électroacoustique et de la spatialisation du son et Peter Brook
y représentait sa dernière trouvaille théâtrale.

L’Iran, politiquement une dictature qui ne tolérait même pas la publication
des journaux indépendants, d’un seul coup s’ouvrait, pour une dizaine de
jours, à l’avant-garde artistique du monde culturel !

Il se considérait à l’apogée de la civilisation moderne et cherchait un
rayonnement international.

Ce petit jeu a duré une dizaine d’années avant que l’insurrection de 79
y mette un point final.


Il y avait quelque chose d’ironique et en même temps dramatique de voir
la population du centre du pays, du côté de Shiraz et les villes aux
alentours, dont la population était encore très traditionnelle et même
on pourrait dire campagnarde, d’affronter la musique moderne (qui était
à l’époque, même moderne et audacieuse pour l’Europe) ou des filles
nues attachées par les pieds et pendouillant dans les allées du Bazar
Vakil par Shuji Terayama.
C’était la confrontation de deux mondes qui laissait les gens perplexes
et un peu en colère. Dans les mêmes allées du Bazar, la nuit les sans-logis,
venus en ville pour se dégoter un boulot, dormaient en pleine rue quand
les artistes étrangers étaient logés dans les palaces Safavides.

Il est vrai qu’un certain nombre d’intellectuels, notamment les étudiants
de l’université de Shiraz ou d’Ispahan en profitait pour se mettre à la
page de ce qui se passait en Occident.
Ces festivals et notamment le festival international de films de Téhéran é
tait l’occasion de voir tout ce qui était normalement censuré en Iran
et donc vous pouvez y voir des films comme “la couleur de la grenade”
de Paradjanov pendant une séance, tandis que même en France, il a fallu
attendre 1977 pour qu’il soit projeté une seule séance au Cosmos à Montparnasse.

Après une première période de méfiance, ou le cinéma iranien n’a quasiment
pas existé, la RI a accepté d’utiliser ce vecteur de propagande à condition
que les artistes propageant les “valeurs islamiques”.
Donc, la République islamique a essayé de renouer avec cette tradition
en y mettant tout son cœur doctrinal.

**Le festival de Fajr était l’exemple parfait de cet effort**.

Pendant une dizaine de jours, d’abord le théâtre puis le cinéma y sont
à l’honneur. Le ministre de la culture ne manque pas l’occasion pour se
féliciter à chaque fois de la qualité supérieure des œuvres jouées ou
projetées pendant ce festival.
Cette année, à l’occasion du 41ème année du festival, il a fait un speech
à la gloire de la création artistique Islamique et a rendu hommage avec
raison aux efforts de l’armée des Pasdarans car quasi toute la production
audiovisuelle en Iran est sous la coupe des gardiens de la Révolution et
de leur organisme de financement de production artistique qui s’appelle
l’Organisation Owj.(Haut/Apogée)


Cette année une grande partie des artistes qui participent normalement
à ce genre d’événements sont ou arrêtés ou menacés ; donc beaucoup
d’entre eux s’abstiennent à y participer.

Le secrétaire général du festival a critiqué les artistes qui n’ont pas
voulu participer à leur cérémonie on les appelant des “dictateurs culturels
et monétisés”.

Depuis l’année dernière et surtout le commencement du soulèvement, beaucoup
de ces artistes, réalisateurs, actrices, acteurs ou techniciens ont publiquement
annoncé leur démission et ne participent plus à ce genre de propagande
honteuse; ce qui n’empêche pas, bien sûr, une autre partie qu’on appelle
les « artistes gouvernementaux » de continuer à y produire leurs œuvres.

Mais cette année, du moins la partie théâtre qui a déjà commencé, n’a
pas attiré les foules et ils ont dû remplir les salles en amenant les
familles des sympathisants du régime, de sorte qu’on distinguait parmi
les spectateurs des enfants et même des bébés.

Le boycott était tellement fort que même les médias pro-régimes ont
avoué que cette année « le théâtre n’avait pas trouvé ses interlocuteurs ».

Sur la partie cinéma, qui a commencé cette semaine, la présence des films
produits par les organismes étatiques est encore plus prononcée que d’habitude.

En fait, c’est la liste des différents organes artistico-culturels du
pouvoir en place :

« La Fondation cinématographique Fârâbî » y présente 9 films à elle seule.
C’est l’institution nationale du Cinéma iranien sous la houlette du ministère
de la culture et de la propagation islamique, à travers laquelle la
politique culturelle et artistique de l’Etat est mise en musique.

« L’école artistique de la révolution islamique » a produit 3 films ;
cette école est directement dirigée par « l’Organisation islamique de
développement/de publicité » qui se concentre principalement sur les
activités artistiques.

« Foyer d’éducation artistique de la jeunesse » ou « Institut pour le
développement intellectuel des enfants et des jeunes adultes » qui est
responsable pour la présentation de 2 films.


Cet institut a une part importante dans la qualité du cinéma iranien et
a pu faire grandir des cinéastes importants comme Kiarostami en son sein.

Cette institution qui existe depuis 1975 sous la houlette de Farah Diba,
avec notamment ses bibliothèques implantées un peu partout en Iran, a
fait beaucoup dans le développement culturel de générations de jeunes.

Mais, justement comme elle a été créée par l’ancien régime, elle a toujours
été l’objet d’attaques incessantes des responsables du régime.
Dernièrement, le gouvernement de Raïssi a essayé, sous prétexte de la
non-rentabilité de fermer l’institut et de vendre ses locaux au privé
c’est à dire aux famille aux proches

« L’organisation artistique Owj » qui est la propriété du Sepah a présenté 2 films

**En 2018, le directeur exécutif d’Owj, Ebrahim Hatamikia, a publiquement
admis que l’organisation était gérée et financée par le CGRI**.

Même l’armée régulière a produit un film qui fait partie de la compétition.

Donc on peut constater que sur une vingtaine de films présentés à la
compétition, une grande majorité sont des productions gouvernementales
et étatiques et donc il faut les considérer plutôt comme de la propagande
officielle.

D’un autre côté, comme les réalisateurs confirmés n’ont pas voulu participer
et présenter des films pour le festival, nous voyons que 16 des réalisateurs
sont des inconnus et qui sont à leur premier film.  A quelque chose malheur est bon.

Malgré toutes les préventions de la République islamique pour choisir
les personnalités qui vont s’exprimer sur la scène de cette cérémonie
il n’a pas pu empêcher Mohamad-Hossein Farahbakhsh, scénariste et producteur
de cinéma, qui est un des lauréats, de déposer son prix par terre et de
dire qu’« il n y a pas à se glorifier quand nos meilleurs personnes ont
été tués ou se trouvent en prison ».

Au commencement du soulèvement au mois de septembre, une centaine de
cinéastes iraniens avaient rédigé un appel au boycott en faisant référence
à la chanson de Shajarian, qui pendant la répression qui avait suivi
l’élection contesté d’AhmadiNejad a interpellé les forces de l’ordre par
une chanson qui est restée dans les annales : « dépose ton fusil par terre ».

Sous ce titre, les cinéastes se sont réclamés pour le soulèvement et
contre la répression qui est abattue sur les protestataires ; c’était
leur deuxième déclaration dans ce sens.

Un des critiques du cinéma, au nom de Farasati aussi, a boycotté la télé
et les festivals officiels. C’est pourtant une personnalité qui pendant
des années avait travaillé avec le régime et avait critiqué d’un air
très hautain le niveau du cinéma iranien par rapport au cinéma classique
occidental.

Le cinéma indépendant iranien a toujours essayé de suivre les traces de
ce qui était le cinéma du réel, le cinéma progressiste et aujourd’hui
aussi un grand nombre de ces cinéastes se trouve exilé à l’étranger.

Le plus marrant c’est que le ministre de la culture lui-même dans son
discours insiste sur le fait qu’il ne faut pas politiser ce genre
d’événement artistique alors que la quasi-totalité de la production
cinématographique présentée dans ce festival est produit par les organes
de répression et les organes culturels de la République islamique et
n’ont pas d’autres buts que de propager leur ligne politique et leur
valeur soi-disant islamique.

Si cette année, il se plaignent de la politisation du festival, c’est
parce qu’une grande partie des artistes ont refusé de participer à
cette mascarade de propagande.

Suite aux activités des cinéastes indépendants iraniens qui sont à l’étranger,
le festival de Berlin a déclaré qu’il ne recevra plus les films produits
par le gouvernement iranien.

Le rapport de l’État iranien avec ses cinéastes et à travers eux avec
le monde culturel occidental est assez complexe, avec des hauts et des
bas, suivant la politique déterminée de la République islamique avec
le monde extérieur.

Quand les artistes iraniens, installés à l’étranger gagnent des prix,
la République islamique censure les informations à leur propos pour les
dénigrer ou les traite comme des personnes ayant vendu leur patrie.

Mais en même temps, il suffit qu’un de ces artistes, lors de son speech
de remerciement, ne mentionne pas le sort de la population, ne dise rien
qui pourrait être interprété “contre le régime” ou qu’il dédicace son
prix au peuple iranien pour que le gouvernement en tire gloire et
l’affiche sur tous ses moyens de communication comme réussite de la
République islamique.


Les critiques internes et le rôle de Mojtaba, fils du Guide
================================================================

On a déjà évoqué ici la question de la perte des soutiens internes du
régime.
Ce mouvement continue depuis quelques semaines et on entend des fuites
à propos des critiques envers la façon dont l’État réagit par rapport
au soulèvement actuel.

Ces critiques viennent même de la part des personnalités de l’armée des
pasdarans, des Bassiji, ou autres responsables étatiques.
On les a même entendus parfois de la bouche des imams de vendredi qui
sont normalement censés formuler leur discours d’après les orientations
qu’ils reçoivent d’un bulletin interne qui est publié chaque semaine
par la maison du guide.

C’est en réaction à ce genre de discours critique envers l’État et le
gouvernement que Khamenei le 10 janvier avait prononcé son discours
qu’on a déjà relaté la semaine dernière à propos des tavabines.

Par exemple, on peut citer le discours qui a été prononcé le 30 décembre
dernier par le remplaçant du commandant des forces armées, un certain
Sepehri, remplaçant de M.Baghéri, chef de l’état-major des forces armées,
mais aussi le discours de Sardar Hamid Abazari, conseiller général auprès du Sepah.

Dans tous ces discours, on entend la critique orientée non pas directement
vers Khamenei mais plutôt vers les gens qui dans leur jargon sont appelés
“les Khavas”, ce qui veut dire les particuliers.

Ils entendent par-là, les hauts responsables de l’État, ceux qui ont
une influence dans la gestion des affaires.

Quand ils sont en train de critiquer Khavas, ils entendent essentiellement
les personnes qui, de leur point de vue, sont en train de mener réellement
la répression et de l’organiser de manière quotidienne; ces personnes ne
sont autres que le cercle de Commandants qui entoure le fils de Khamenei,
Mojtaba qui est né en 1969, qui a fait ses études secondaires dans le
lycée Alavi, qui était au temps du Shah, l’institution spécifique pour
les enfants de l’élite du clergé et des nationalistes religieux.

Il a fait ses études supérieures au sein de du Howsé et a fini par y enseigner.


Mojtaba était toujours auprès de son père et en quelque sorte a joué le
rôle du secrétaire particulier. Il est marié à la fille de Monsieur
Haddad-Adele que l’on a eu déjà le plaisir de présenter dans ses colonnes.

Apparemment, le couple a eu des problèmes de fertilité qui ont été traités
à Londres durant un voyage de toute la famille pour accompagner la mère.
La rumeur publique dit que ce traitement a coûté plus de 2 millions de
livres sterling. Il a amassé une fortune importante, bien sûr, à travers
des prête-noms au Canada et en Grande-Bretagne.

Pourtant Mojtaba n’a pas de travail, ni de fonction officielle à part
l’enseignement dans le Howsé. Il doit en particulier enseigner ce qu’on
appelle “les leçons externes” (درس خارج) qui sont celles qu’on est censées
recevoir au-delà l’enseignement classique de théologie pour pouvoir
prétendre au degré de Mojtahed.
Au bout d’une dizaine d’années d’enseignement, il pourrait éventuellement
et avec l’accord de ses pairs être nommé Ayatollah.

Ce que son père Khamenei, au moment de sa désignation comme Guide n’était pas.

D’ailleurs, une vingtaine d’années après sa désignation par le “majless-é-khobregan”
(“l’assemblée des élites”, c’est une institution qui a le devoir de vérifier
la correspondance des lois avec la constitution, le Parlement des élites
religieux, constitué par une cinquantaine de religieux.
Ça peut être considéré comme l’équivalent du Conseil Constitutionnel en France).
L’enregistrement vidéo de cette séance particulière et des discussions qui
y ont eu lieu a été rendu public, il y a une dizaine d’années.

On y voit L’Ayatollah Rafsanjani, qui assure la présidence de cette
assemblée, au moment où celui-ci est en train de décider si, après Khomeyni,
la succession doit être assurée par un conseil ou par une personne.

A ce moment particulier de la séance, Rafsanjani propose Khamenei en
tant que guide provisoire, le temps que les khobregans désignent en
temps voulu et sans précipitation un Guide définitif.
Pour convaincre les autres membres de l’assemblée, il évoque un souvenir
de discussion avec l’imam Khomeyni qui aurait désigné lui-même Khamenei
comme quelqu’un de tout à fait apte à assurer sa succession.


Il faut savoir qu’avant cette date, la personne choisie par Khomeyni et
tout indiquée pour cette charge était l’Ayatollah Montazeri qui après
son intervention au sujet de la fatwa de Khomeyni pour liquider les
révolutionnaires et les communistes en 81, a été, comme on l’a déjà
expliqué, écarté du cercle du pouvoir et assigné à domicile.

D’ailleurs quelques années plus tard, il est mort de manière douteuse
et son fils a prétendu qu’il a été assassiné par le régime.

Donc Rafsanjani prétend que l’imam Khomeyni a lui-même désigné monsieur
Khamenei comme un successeur potentiel.

Quand son nom est proposé comme candidat éventuel, Khamenei qui n’a pas
encore le degré de l’Ayatollah se lève en refusant cette proposition et
en précisant (c’est une phrase que tous les Iraniens ont maintes fois
entendue) “qu’il faut pleurer pour la situation d’un pays où on me désignerait
comme guide” et pour donner une explication à son refus il précise que
“beaucoup de ces personnes, présentes ici même, ne vont pas accepter
mes décisions”.
C’est à ce moment-là qu’une partie des ayatollahs présents réagisse en
disant qu’ils acceptent volontiers monsieur Khamenei « à condition que
ça soit provisoire. »

Comme beaucoup d’autres situations, c’est un provisoire qui a, pour
l’instant, duré plus de 30 ans.
Khamenei aujourd’hui à plus de 80 ans et son camp depuis quelques années,
suivant son choix de désigner son fils, prépare activement sa succession
en plaçant des personnes de confiance en charge des responsabilités,
c’est à dire ceux qui ont déjà fait allégeance à Mojtaba, notamment une
grande partie des Pasdarans et toute la structure de répression mais
aussi financière du pays.

C’est ainsi que les critiques qui sont faites aujourd’hui sur la manière
de fonctionner le régime sont indirectement dirigées vers Mojtaba.

D’ailleurs, la rumeur dit que ça fait des mois que c’est lui qui prend
les décisions les plus importantes et que son père ne fait que diffuser
ses décisions auprès du public.

Quand la discussion au sujet de la succession du guide a pris de l’importance,
notamment il y a quelques mois, quand Khamenei était malade et sous
traitement, des personnalités se sont exprimées pour dire qu’on a fait
une révolution pour ne pas avoir un système monarchique, basé sur
l’héritage, que si on a chassé un souverain monarque, ce n’est pas pour
le remplacer par un autre, même s’il est le fils du guide actuel.

D’autres ont répondu à ces critiques en disant que Mojtaba ne serait pas
choisi en tant que fils de Khamenei, mais par ses qualités intrinsèques;
c’est vrai qu’on ne veut pas un système basé sur l’héritage, mais en
même temps, il n’y a aucune raison de se priver d’une personne qualifiée
juste parce qu’il est le fils de quelqu’un.

C’est ironique de savoir que c’est exactement dans les mêmes termes que
le petit Shah justifie sa candidature implicite pour le pouvoir futur de l’Iran.

Mojtaba, depuis des années, développe ses réseaux à l’intérieur du Sépah
et les autres instances du pouvoir pour préparer le terrain avec l’appui
de son père bien entendu; il a à sa disposition un vrai réseau de fidèles
mais la crise actuelle que le régime traverse lui fait subir les critiques
de l’intérieur de la part des gens qui ne sont pas contents de la façon
que les affaires du pays sont gérées.

La tuerie des enfants, plus de 70, les condamnations par pendaison, les
arrestations sommaires, la répression des peuples Kurdes, baloutch … la
situation des prisonniers récents qui aujourd’hui dépassent largement
les 20000, les tortures et les aveux publics, l’attitude du régime à
l’égard de ses célébrités artistiques ou sportifs s’ajoutent aux critiques
sur la gestion des protestations.

Beaucoup de ces personnes peuvent même considérer que des membres de
leur famille soient impliquées dans les protestations.

L’appareil judiciaire aussi se trouve sur le feu des critiques pour ces
tribunaux expéditifs et notamment les tribunaux « on line » qu’ils ont
instaurés, qui après quelques minutes condamnent les gens à des peines
de prison ou même à des exécutions.


Rassemblement devant le parlement européen
==============================================

La manifestation des Iraniens devant le Parlement européen, qui a eu
lieu au début de la semaine dernière a rassemblé selon la police 12000 personnes,
ce qui veut dire qu’il pouvait être entre 20 et 30 000 personnes.

À la fin de ce rassemblement la présidente du Parlement européen, Roberta
Massola, a prononcé des paroles convenues sur le soutien de l’Europe,
que les gens sont du bon côté de l’histoire et que l’histoire leur
donnera raison, … que 500 million d’Européens sont derrière vous et
soutiennent le mouvement pour la liberté.

Dans la manifestation de Berlin, au mois d’octobre, devant tous les caméras
il y avait des drapeaux tricolores avec le signe de la royauté; même si
on pouvait voir ici et là des banderoles qui disaient “on ne veut pas
d’oppresseurs, que ça soit le Shah, que ça soit le Guide”.

Visiblement ce slogan était porté par une minorité.

Ce qui confirme de plus en plus que le mouvement d’opposition dans la
diaspora tend plutôt vers le nouveau Shah, malgré tous les efforts des
gens et des organisations de gauche qui essaient d’imposer un soi-disant
bloc internationaliste.

Cette tendance idéologique n’est pas très étonnante vu le niveau de vie
en générale de ces iraniens, dont une grande partie s’est sauvée de l’Iran
suite à l’insurrection de 79.
Beaucoup d’entre eux aussi sont tout à fait intégrés dans les classes
moyennes des sociétés occidentales; depuis la manifestation de Berlin,
on a l’impression qu’il y a une coupure entre les slogans scandés par
les gens de l’intérieur et ceux de l’extérieur.

La population modeste qui se trouve dans la rue en Iran, n’a jamais ou
très rarement prononcé des slogans pro-royauté et une vraie coupure
semble se dessiner entre les slogans choisis par la diaspora et la
réalité des revendications politiques en Iran; on ne va pas jusqu’à
dire qu’ils représentent aujourd’hui une contre révolution mais on n’en
est pas loin.

Donc, suite aux actions et manifestations de la diaspora iranienne dans
les capitales et les villes occidentales, surtout le rassemblement de
Strasbourg, le Parlement européen a décidé de considérer l’armée des
corps des gardiens de la Révolution de l’Iran comme terroriste.

Cette décision qui à court terme pourrait, en cas d’acceptation par les
instances supérieures de l’Europe, avoir une certaine importance dans
l’activité économique du Sepah a fait réagir le gouvernement iranien.

Le porte-parole de l’État iranien a de son côté menacé les Européens.

Pour l’instant, il s’agit d’une résolution du parlement qui porte sur la
réponse de l’union européenne par rapport à la répression et les exécutions
en Iran; le parlement donne ainsi des directives aux organes exécutifs
de l’Europe et formule des demandes aux autorités iraniennes.
La grande majorité de ces demandes concerne les questions de droit de
l’homme, leur respect en Iran … et va jusqu’à demander aux iraniens de
donner une date pour l’abolition de la peine de mort.

Par rapport aux organes exécutifs de l’Europe, le parlement demande
qu’on soutienne plus activement la société civile iranienne, l’opposition,
qu’on utilise les budgets qui existent pour aider les immigrés iraniens,

Accorder plus facilement des visas aux iraniens qui sont en danger et
poursuivis… et enfin Il a demandé à l’union européenne de mettre le nom
du Sépah sur la liste des organisations terroristes.

Une partie des États membres ont insisté pour l’arrêt total des négociations
sur le traité nucléaire. Cette demande a été rejetée et il semble donc
que la majorité des pays désirent encore continuer les négociations sur
le nucléaire iranien.

Toute la partie de la résolution en direction de l’Iran, est du blabla
démocratique qui ne serait même pas lu avec attention par ses responsables ;
la partie qui concerne les instances internes de l’union aussi, compte
tenu des limitations de l’union concernant les questions de politique
étrangère et de sécurité intérieure de chaque pays, n’aurait aucun
caractère contraignant pour les États membres.
Cette position de souveraineté sur ces deux champs, est surtout défendue
par la commission et le conseil de l’Europe (à 27).

Donc, primo, l’Europe ne peut pas faire passer une loi contraignante sur
la politique étrangère, et secundo au moment d’étudier une proposition,
la commission et le conseil doivent la considérer du point de vue de
chaque État et de ses intérêts particuliers.

En plus, il paraît que Josep Borrell, Haut représentant de l’Union
européenne pour les affaires étrangères, n’est pas sur la même ligne
politique que ce qui est exprimé par la résolution, c’est apparemment
la raison pour laquelle il n’a même pas participé au débat sur cette résolution.

Si réellement, le Conseil de l’Europe et la commission européenne désiraient
mettre les gardiens de la révolution sur la liste des terroristes, ils
n’avaient aucunement besoin d’une résolution du Parlement, ils pouvaient
le faire directement.

Au fond, on peut dire qu’il s’agit d’un élément supplémentaire de pression
politique sur l’État iranien, sans conséquences immédiates sur son orientation.

La diaspora iranienne et surtout cette opposition bourgeoise qui petit à
petit est en train de se constituer à l’étranger, a besoin de dire au
millions d’Iraniens qui participent à ces actions, qui se déplacent de
pays en pays que leur activité sert réellement à quelque chose.

Les positions de Borrell mais aussi de Macron sur la question iranienne
restent ambiguës. Macron, il y a une quinzaine de jours, a rencontré
Abdollahi à Oman et a discuté chaleureusement avec lui. Il a relancé
l’idée de poursuivre les négociations sur le nucléaire iranien qui,
depuis le soulèvement, est au point mort. D’ailleurs cette rencontre “
inopinée” a permis aux iraniens de se gargariser sur le plan intérieur
encore une fois sur leur force et la stabilité du régime.

L’Iran en réponse aux décisions du parlement européen les a, de son côté,
menacé en disant qu’il pourrait « considérer leur armée comme terroriste ».

Ils ont également parlé de sortir du NPT (Treaty on the Non-Proliferation
of Nuclear Weapons).


Les efforts de la famille royale
====================================

En parlant de l’alternative bourgeoise, nous sommes obligés de signaler
les efforts de la famille royale iranienne; dans ce creux du mouvement,
accentué par le froid glacial du pays, la famille royale s’est mise en
avant et essaie de combler le vide médiatique existant.

Les médias iraniens de langue persane à l’étranger, essaient de tout
leur pouvoir de gonfler cette alternative.

Le nouveau Shah est assis dans une interview type très moderne de CNN
et discute avec une journaliste en persan (18 janvier 2023, la chaîne Manoto);
quand la journaliste lui dit : “mais Monsieur le roi, pourquoi vous ne
faites rien pour prendre le pouvoir?” il répond: “c’est parce que les
gens ne m’ont pas donné leur tutorat pour que je puisse m’exprimer en
tant que leur représentant; j’ai besoin que tout le monde me le demande”
et ainsi Twitter devient un instrument de démocratie bourgeoise!

C’est à la suite de cette intervention que tous les défenseurs du régime
royal ou du Shah en personne, se sont réclamés followers de Monsieur Pahlavi!
Aussi à plusieurs reprises quand la journaliste lui dit “mais des millions
de personnes vous réclament en Iran”, il hoche la tête de haut en bas
en signe d’approbation, de manière très réfléchie, comme si c’était
l’évidence même; tandis que personne n’a encore vu le nom du Shah, sauf
peut-être dans des provinces arriérées où les gens pensent que le Shah
est toujours Nasser-el-din shah Qajar! Ou plutôt Ahmad Shah Qajar!
Je me souviens que ma grand-mère pensait toujours qu’il était au pouvoir!

D’ailleurs cette histoire de tutorat a donné l’occasion d’une autre
humiliation pour le Roi autoproclamé. Sur les 4 millions de followers
qu’il a sur les réseaux sociaux, moins de 400 milles l’ont nommé tuteur !
et tout de suite un autre hashtag a vu le jour qui dit : « je ne donne pas de tutorat ».

Sa mère aussi, Mme Farah diba n’a pas manqué de réagir à une déclaration
du porte-parole de l’État iranien qui prétendait que le Shah est parti
avec des coffres de bijoux appartenant à la royauté iranienne depuis
des lustres et des dizaines de coffre de diamant, des pierres précieuses
ainsi que des valises de devises étrangères et des bobards de ce type.

Elle s’est montrée outragée par ses propos et c’est vrai que ce qu’il
racontait était tellement exagéré que ça pouvait faire douter de sa
véracité. Néanmoins elle se décrit comme une femme dont le mari vient
de lui dire “Lilianne,fais les valises, on rentre à Paris!”

Apparemment, le Shah est entré dans le Palais et a crié à sa femme “Farah,
fait les valises on va se sauver en Égypte!” et elle a eu à peine le temps
de mettre ses chaussures pour cavaler. Elle a certainement oublié que
son fils a déjà répondu à cette question en disant qu’ils ont pris 67
millions de dollars, sans compter les biens immobiliers, les actions,
les bons de trésor,… et l’argent qu’il avait déjà à l’étranger.

Abbas Milani, un historien qui ne peut en aucun cas être classé à gauche,
a fourni un témoignage direct dans lequel il précise que deux grands
avions et un avion plus petit étaient remplis des possessions de la
famille royale quand elle a quitté le pays;
Aussi, nous avons su par la suite que (par exemple dans l’affaire Bettencourt)
Liliane Bettencourt avait acheté une île de la famille royale iranienne,
qu’elle avait refilé à son photographe, gigolo de service.
Ce genre de biens devrait pulluler dans la fortune de la famille royale
quand ils ont quitté le pays.

Mais au-delà de l’anecdote, cette séquence pose la question de savoir
les manigances que la RI est en train de monter pour faire disparaître
les joyaux de la couronne; est-ce qu’ils veulent les dérober et mettre
leur forfaiture sur le dos de l’ancien régime?

Ce ne serait pas étonnant de la part des gens qui considèrent le pays
comme leur prise de guerre.

La République islamique ne se voit pas comme une force d’oppression, mais
plutôt comme une armée victorieuse qui a le droit de prendre ce qu’il
a gagné à travers ses guerres. ِ

D’ailleurs, la sourate Al-Anfal (arabe : سورة الأنفال, Le butin) qui est
la 8e sourate du Coran a précisé les choses :

« Les croyants qui auront abandonné leurs familles, pour défendre, de
leurs biens et de leurs personnes la cause de Dieu, partageront le butin
avec ceux qui ont donné du secours et un asile au prophète….

Nourrissez-vous des biens licites enlevés aux ennemis, et craignez
le Seigneur. Il est clément et miséricordieux. »

Mais attention, il ne faut pas oublier la part du clergé :

« Souvenez-vous que vous devez la cinquième part du butin à Dieu, au
prophète, à ses parents, aux orphelins, aux pauvres et aux voyageurs,
si vous croyez en Dieu, et à ce que nous révélâmes à notre serviteur,
dans ce jour mémorable où les deux armées se rencontrèrent. La puissance
du Seigneur est infinie. »

(Traduction de Claude-Étienne Savary).


Projet Goghnous (phénix) 28 janvier 23
===========================================

On avait déjà mentionné de projet Phénix comme un courant constitué sous
la houlette de raisin par la vie et qui formait une sorte de « Shadow Government »

Monsieur Allahyar Kangarloo, physicien, résidant aux États-Unis et ancien
directeur de ce projet qui fait partie des proches de Reza Pahlavi a
fait part au média des discussions afin de créer des rapprochements
entre les royalistes iraniens et ayatollah Sistani qui est un des Mojtahed
shiites résidant en Irak qui pendant toutes ces années a su garder ses
distances par rapport à la République islamique et qui, de temps en
temps, peut éventuellement les critiquer; c’est quelqu’un de très
influent parmi les chiites irakiens et le rapprochement de ces deux
personnages peut constituer une base sérieuse pour la création d’une
sorte d’alliance du sabre et du goupillon, sous forme d’une monarchie
constitutionnelle.


Les infos diverses, glanées par ci, par là
============================================


Le samedi 21 janvier 2023  Iran et Turquie
--------------------------------------------

Le samedi 21 janvier 2023 lors de la rencontre bilatérale du ministre
iranien des affaires étrangères M.Abdollahi avec les responsables de la
Turquie dans son discours est revenu sur “nous regrettons la mort
accidentelle d’une jeune femme iranienne mais la vérité est que les
intervenants étrangers, les médias occidentaux et les réseaux sociaux
ont attaqué de manière organisée l’Iran avec des but politiques et
sécuritaires particuliers contre la République islamique d’Iran”,

L’exécution de M.Akbari, un des responsables binational (irano-britannique)
de la RI a fait réagir les anglais qui se sont rapprochés des Américains
pour harmoniser leur position  envers l’Iran. Blinken et le ministre
anglais des affaires étrangères ont décidé d’étendre les sanctions contre
l’Iran.

Le vendredi 21 janvier 2023 Ardebil en Azarbayjan
---------------------------------------------------------

Le vendredi 21 janvier à Ardebil en Azarbayjan des cocktails Molotov ont
été lancés sur le bâtiment du palais de justice..


.. _attaque_de_drones_2023_01_29:

Le 29 janvier 2023 attaque de drones à Ispahan
-------------------------------------------------

Le 29 janvier, les médias ont indiqué qu’une usine de production d’armement
a été attaquée par drones à Ispahan.

La population a entendu 3 déflagrations au centre de la ville, il semblerait
qu’il s’agisse plutôt d’entrepots d’armement, ou de laboratoires pour
les industries militaires au centre de la ville; il est improbable que
le régime ait installé des usines de production d’armement au centre
ville quand nous savons que plusieurs installations de ce type existent
dans le sud et aux alentours d’Ispahan.

Ce genre d’attaque est généralement l’œuvre des organismes sécuritaires
d’Israël sur les cibles en relation avec le nucléaire.

On saurait plus tard ce qui était réellement visé.
La République islamique a bien entendu déclaré que cette attaque a été
neutralisée à temps sans faire de dégâts.

Le 29 janvier 2023 tremblement de terre à Khoy
--------------------------------------------------

Le même jour, un tremblement de terre est survenu à Khoy.


Le 25 janvier 2023 des paroles de dirigeants de la RI
--------------------------------------------------------

Le 25 janvier, Yahya rahim-safavi, commandant du Sepah et conseiller de
Khamenei a déclaré lors d’un discours: “Dieu a jeté tout le tableau
Mendeleïev dans le sol de l’Iran, on a non seulement du pétrole et du gaz
mais du chrome, chromite, Fer, cuivre, uranium… on a tous les éléments.
Dieu nous a donné le meilleur des territoires et le meilleur des nations”…
Et bien sûr le meilleur des Guides!

Monsieur Ahmad Vahidi ministre de l’Intérieur a nommé le soulèvement qui
a commencé maintenant depuis 4 mois “des troubles” et il a dit que
c’est le résultat d’une mauvaise mode de vie que les gens ont adopté.

Il a prétendu que les gens qui ont participé dans ces événements étaient
ceux chez qui, la notion de famille avait perdu son importance.

Il a donc insisté pour renforcer le mode de vie islamique dans les familles.











