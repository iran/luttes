
.. index::
   pair: Jafar Panahi ; 2023-02-01


.. _jafar_panahi_2023_02_01:

==============================================================================================================================================================================================================================================================
Mercredi 1er février 2023 **Jafar Panahi entame une grève de la faim sèche**
==============================================================================================================================================================================================================================================================

- https://nitter.manasiwibi.com/arminarefi/status/1621063734155255809#m


EN PRISON depuis le 11 juillet 2022, l’illustre cinéaste iranien Jafar Panahi
a annoncé dans une lettre divulguée par sa famille qu’il entamait à compter
du 1er fevrier 2023 une grève de la faim sèche pour protester contre
les « traitements illégaux et inhumains » du pouvoir judiciaire


Jailed Iranian Director Jafar Panahi Hunger Strikes Against “Hostage-Taking”
================================================================================

- https://iranhumanrights.org/2023/02/jailed-iranian-director-jafar-panahi-hunger-strikes-against-hostage-taking/

In an open letter, renowned Iranian director Jafar Panahi has announced
the beginning of his hunger strike in Tehran’s Evin Prison until he is
freed from his illegal imprisonment.

“I firmly declare that in protest against the unlawful and inhumane behavior
of the judicial and security apparatus and this particular hostage-taking,
I have started a hunger strike since the morning of the 1st of February 2023,
and I will refuse to eat and drink any food or medicine until the time of
my release,” wrote Panahi in a letter published on his wife’s Instagram page.
“I will remain in this state until perhaps my lifeless body is freed from prison.”

At the time of this writing—while Panahi is starving himself in an Iranian
prison—the government is promoting its Fajr International Film Festival,
which has been boycotted by several Iranian directors and actors in protest
against the government’s ongoing persecution of activists and dissidents,
including in the film industry, as well as the state’s ongoing trampling
of basic human rights such as freedom of speech and expression.


Following is a translation by the Center for Human Rights in Iran (CHRI) of Panahi’s letter
=============================================================================================

On July 20 of this year, in protest against the arrest of two of our
beloved colleagues, Mr. Mohammad Rasulof and Mostafa Al-Ahmad, I, together
with a group of filmmakers, gathered in front of Evin prison, and it was
decided that a number of us and the lawyers of the detained colleagues
entered the Evin courthouse and peacefully we were talking with the
relevant authorities and the relevant investigator when an agent came
and took me to the judge of Branch 1 of Evin’s sentence enforcement.

The young judge said without introduction, “We were looking for you in
the skies, we found you here. You are under arrest!”

In this way, I was arrested and transferred to Evin prison for the
execution of a sentence that had been issued for 11 years.

According to the law, for which I was arrested in 2009, after more than
10 years of non-implementation of the sentence it is subject to the passage
of time and becomes unenforceable.
Therefore, this arrest was more like banditry and hostage-taking than
the enforcement of a judicial sentence.

Even though my arrest was illegal, the respected lawyers succeeded in
violating the ruling issued in 2010 by resuming the proceedings in the
Supreme Court, which is the highest authority for judicial cases, on
the 15 of October 2022 of this year, so that they can go to the same
branch for retrial.

…In this way, according to the law, with the acceptance of the request
for retrial and violation of the verdict, the case was referred to the
branch and I should have been released immediately by issuing bail; while
we have seen that it takes less than 30 days from the time of the arrest
to the hanging of the innocent youth of our country, it took more than
a 100 days to transfer my case to the branch with the intervention of
security forces.

According to the clarity of the law in cases of violation of the sentence
in the Supreme Co the judge of the same branch was obliged to release me
by issuing a bail order as soon as the case was referred to that branch,
however, by issuing a heavy bail order, in practice after months of
detention… I was still kept in prison with repeated excuses every day by
the security agencies.

What is certain is that the behavior of the bullying and extrajudicial
security institutions and the unquestioning surrender of the judicial
authorities once again show the implementation of selective and tasteful laws.

It is only an excuse for repression. Even though I knew that the judicial
system and the security institutions have no will to implement the law
(which they insist on), but out of respect for my lawyers and friends,
I went through all the legal ways to get my rights.

Today, like many people trapped in Iran, I have no choice but to protest
against these inhumane behaviors with my dearest possession, that is, my life.

Therefore, I firmly declare that in protest against the extrajudicial
and inhumane behavior of the judicial and security apparatus and this
particular hostage-taking, I have started a hunger strike since the
morning of the 1st of February 2023, and I will refuse to eat and drink
any food and medicine until the time of my release.

I will remain in this state until perhaps my lifeless body is freed
from prison.

With love for Iran and the people of my land,
Jafar Panahi

Dissident. Emprisonné, le cinéaste iranien Jafar Panahi commence une grève de la faim
========================================================================================

- https://www.courrierinternational.com/article/dissident-emprisonne-le-cineaste-iranien-jafar-panahi-commence-une-greve-de-la-faim
