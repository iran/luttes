.. index::
   pair: Grève de la faim; Farhad Meysami
   ! Farhad Meysami


.. _farhad_meysami_2023_02_02:

========================================================================================
2023-02-02 **Civil Rights Defender Farhad Meysami at Risk of Death in Iranian Prison**
========================================================================================


- https://iranhumanrights.org/2023/02/civil-rights-defender-farhad-meysami-at-risk-of-death-in-iranian-prison/

.. figure:: images/farhad_meysami.png
   :align: center


February 2, 2023 – The UN and international human rights organizations
should urgently demand that the Iranian judiciary release political
prisoner Farhad Meysami, a civil and women’s rights defender whose life
is in danger, said the Center for Human Rights in Iran (CHRI).

Meysami has reportedly lost 53kgs (117lbs) from a hunger strike he began
on October 7, 2022, in Rajaee-Shahr Prison in Karaj.

Alarming images of the physician shared by BBC Persian, which first
reported his condition, showed his emaciated body.

“Meysami was one of the first male activists to go to prison in Iran for
demanding an end to the compulsory hijab for women,” said CHRI Deputy
Director Jasmin Ramsey.

“He has sacrificed everything to demand basic rights for the Iranian people,”
added Ramsey. “
The international community should stand with him by amplifying his calls
and by demanding his freedom to prevent the death of another political
prisoner in Iran.”

CHRI urges international human rights organizations, as well as medical
associations (because Meysami is a medical doctor) to band together in
demanding freedom for the ailing human rights advocate.

During the first 75 days of his hunger strike, Meysami drank only liquids,
but since December 19 has been on a “dry” hunger strike, according to a
report by Voice of America.

“At a time when the rulers have denied the people their rights to a healthy
body, livelihood and dignity, leaving nothing but daily pain and suffering,
I have begun my own small contribution, hoping to turn these poisonous
times into an antidote with a collective effort,” wrote Meysami in a
letter published by BBC Persian.

“I will still stand by my three demands: stopping the executions of
protesters, releasing political-civil prisoners, and stopping forced-hijab
harassments, and I will continue my impossible mission in the hope that
it may become possible later on with a collective effort,” he added.

Meysami has been behind bars since July 2018, serving a five-year prison
term for peacefully advocating an end to Iran’s compulsory-hijab law
along with fellow prominent activists including human rights lawyer
Nasrin Sotoudeh.

Fellow political prisoner Jafar Panahi, a renowned filmmaker, has also
started a hunger strike to demand freedom from his unlawful imprisonment.


2023-02-10 #FarhadMeissami en grève de la faim depuis 4 mois vient d’être libéré
=====================================================================================

- :ref:`farhad_meysami_2023_02_10_local`
- https://twitter.com/LettresTeheran/status/1624070764893708288#m

Ce médecin et traducteur a purgé la quasi totalité de sa peine injuste
de 4 ans de prison. Les photos récentes de lui, le montrant extrêmement
amaigri après sa grève de la faim avaient énormément choqué.

