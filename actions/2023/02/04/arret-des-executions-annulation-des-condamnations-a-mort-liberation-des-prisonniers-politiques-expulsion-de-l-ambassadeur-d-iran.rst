
.. index::
   pair: Rassemblement ; Samedi 4 février 2023


.. _iran_grenoble_2023_02_04:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ **Femme, Vie, Liberté** 20e rassemblement samedi 4 février 2023 à Grenoble **place Félix Poulat** à 14h30 **Arrêt des exécutions, annulation des condamnations à mort libération des prisonniers politiques, expulsion de l'ambassadeur d'iran** 📣
==============================================================================================================================================================================================================================================================

Arrêt des exécutions, annulation des condamnations à mort liberation des prisonniers politiques, expulsion de l'ambassadeur d'iran
141e jour depuis la mort de #JinaMahsaAmini (2022-09-16)

- :ref:`mahsa_jina_amini`
- :ref:`slogans`


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|


Texte annonçant le rassemblement du samedi 4 février 2023
==============================================================

Comme chaque samedi depuis la mort de Jina Mahsa Amini, la communauté
iranienne de Grenoble organise un rassemblement place Félix Poulat
pour être la voix des iraniennes et iraniens assassinés, emprisonnés
ou torturés depuis le début des manifestations.

La rupture entre du peuple et le pouvoir est totale et irréversible.

Le régime n’a plus les moyens économiques nécessaires pour répondre,
même partiellement, aux demandes sociales, et une partie de la classe
dominante n’est plus solidaire avec le Guide et ses positions extrêmes.

Le régime et son appareil de répression sont affaiblis.
Une fracture à l’intérieur de l’appareil de l’état est plus que visible
Par conséquent, l’émergence de nouvelles vagues de protestations et de
grèves est inévitable.

À cela s’ajoute une crise écologique, surtout une crise de l’eau
disponible, qui a déjà provoqué des émeutes dans un passé récent.
Et aussi une crise idéologique et morale due à la corruption systémique et
au pillage des ressources du pays par une poignée des puissants.
**Une liste non exhaustive !**

Le mouvement de révolte en cours, même affaibli, voire défait momentanément,
est appelé à se redresser.

On entre dans le cinquième mois de la révolte. La baisse récente d’intensité
des protestations ne doit pas tromper. Les protestations prennent d’autres
formes plus sur la base sociale et économique et social.
Comme la grève des titulaires du secteur pétrolier le 17 janvier 2023.

Le feu sous la cendre est en train d’œuvrer . Nous sommes plus que
jamais au seuil d’une révolution.

Nos Revendications
-------------------

- :ref:`revendications_mediapart`

.. include:: ../../../../revendications/revendications_2023_03_07.txt


La rupture entre du peuple et le pouvoir est totale et irréversible.

Le régime n’a plus les moyens économiques nécessaires pour répondre,
même partiellement, aux demandes sociales, et une partie de la classe
dominante n’est plus solidaire avec le Guide et ses positions extrêmes.

Le régime et son appareil de répression sont affaiblis.
Une fracture à l’intérieur de l’appareil de l’état est plus que visible
Par conséquent, l’émergence de nouvelles vagues de protestations et de
grèves est inévitable.

À cela s’ajoute une crise écologique, surtout une crise de l’eau
disponible, qui a déjà provoqué des émeutes dans un passé récent.
Et aussi une crise idéologique et morale due à la corruption systémique et
au pillage des ressources du pays par une poignée des puissants.
**Une liste non exhaustive !**

Le mouvement de révolte en cours, même affaibli, voire défait momentanément,
est appelé à se redresser.

On entre dans le cinquième mois de la révolte. La baisse récente d’intensité
des protestations ne doit pas tromper. Les protestations prennent d’autres
formes plus sur la base sociale et économique et social.
Comme la grève des titulaires du secteur pétrolier le 17 janvier 2023.

Le feu sous la cendre est en train d’œuvrer . Nous sommes plus que
jamais au seuil d’une révolution.


Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`soutiens_avocats_2023_01_10`

Les victimes de la barbarie des mollahs
--------------------------------------------

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|

Les otages français
=======================

.. include:: ../../../../repression/otages/liste_2023_02_09.txt


Quelques événements du 28 janvier 2023 au 4 février 2023
=============================================================================

- :ref:`semaine_05_iran_2023`


Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/

.. figure:: images/annonce_ici_grenoble.png
   :align: center


Mobilizon
=============

- https://mobilizon.chapril.org/events/c249f4a0-f059-4df2-9736-a30c0abb0568

Quelques photos
=================


Vidéos peertube
==================

- https://sepiasearch.org/search?search=grenoble+iran


Autres événements ce samedi 4 février 2023 à Grenoble
==========================================================

.. figure:: images/ici_grenoble_2023_02_04.png
   :align: center

   https://www.ici-grenoble.org/agenda


Manifestations précédentes à Grenoble
=======================================


- :ref:`iran_grenoble_2023_01_28`
- :ref:`iran_grenoble_2023_01_21`
- :ref:`iran_grenoble_2023_01_14`
- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


