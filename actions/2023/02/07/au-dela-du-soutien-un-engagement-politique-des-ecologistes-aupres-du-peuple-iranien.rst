.. index::
   EELV; Un engagement politique des écologistes auprès du peuple iranien

.. _eelv_2023_02_07:

=======================================================================================================
2023-02-07 Au-delà du soutien, un engagement politique des écologistes auprès du peuple iranien
=======================================================================================================

Fév 7, 2023 | Le Conseil Fédéral

Exposé des motifs
======================

La défense des droits humains et la lutte contre toutes les formes de
discrimination et contre la peine de mort sont des valeurs fondamentales
de notre parti.

Nous, écologistes, devons en tant que parti féministe rester vigilant·e·s
et contribuer à ce que le droit à l’autodétermination des femmes soit
une priorité absolue.

L’actualité mondiale donne un terrible échos à l’alerte de Simone de Beauvoir :
“il suffira d’une crise politique, économique ou religieuse pour que les
droits des femmes soient remis en question. Ces droits ne sont jamais acquis”.

Comme le droit à l’avortement, mis à mal dans le monde entier jusque dans
des pays occidentaux tels que les Etats-Unis, par des juridictions ou
des gouvernements conservateurs et misogynes, mettant ainsi à mal les
droits des femmes : le droit à l’éducation dont sont souvent privés les
filles et les femmes, à l’instar des Afghanes qui viennent de se voir
interdire le droit d’aller à l’université ; la liberté des femmes de se
vêtir comme elles l’entendent, de porter ou de ne pas porter le voile.

De trop nombreux pays, tel que l’Iran, imposent dans leur corpus législatif
le port du voile obligatoire.

En Iran, il est imposé par la force depuis 1979, contrôlé quotidiennement
par la police des mœurs instaurée en 1981. Il illustre un ensemble de
politiques conservatrices reléguant toutes les femmes iraniennes au rang
de citoyennes de seconde zone.

La société iranienne est d’une part bâillonnée par un appareil juridique
liberticide appuyé par le recours systématique à la violence d’Etat et
d’autre part grevée par l’embargo, la pauvreté et l’inflation, rendant
toute mobilisation particulièrement dangereuse et difficile, donnant
naissance à de nombreux mouvements sociaux, comme le mouvement étudiant
de 1998, le mouvement vert de 2009 ou encore le mouvement de 2019 qui ont
tenté de remettre en question la situation économique catastrophique,
l’absence de liberté et la discrimination des femmes.

Cette situation est aussi un héritage des stratégies de déstabilisation
et de contrôle des puissances occidentales, au nom d’un extractivisme
pétrolier attentatoire à l’environnement.

Depuis l’assassinat de Jina Mahsa Amini le 16 septembre 2022, par la police
des mœurs iraniennes, pour un voile mal porté, le peuple iranien, au
rang duquel les femmes se trouvent en première ligne, se soulève unanimement
contre le régime théocratique en place, l’inégal partage des richesses,
la corruption, et pour la démocratie, autour de la devise « Femme, vie, liberté”.

Nous saluons le courage de ces femmes qui se mobilisent depuis plusieurs
mois dans ce que l’on pourrait définir comme la première révolution
féministe de l’Histoire, ces femmes qui retirent leur voile en tenant
tête aux forces militaires et paramilitaires.

Nous saluons le courage de la jeunesse iranienne que le régime iranien
terrorise, assassine afin de maintenir son contrôle sur la population
et conserver le pouvoir.

Cette révolution féministe commencée par les femmes touche maintenant
toute la population iranienne.

Une répression sanglante et arbitraire a causé la mort de plusieurs
centaines de personnes, dont des enfants, ainsi que des centaines de
blessé·e·s.
Et de nombreux.ses Iraniennes et Iraniens ont été placé·e·s arbitrairement
en détention où ils et elles subissent des tortures, des viols et des humiliations.

Un nombre insoutenable d’Iranien·nes, notamment des jeunes, ont été enfermé·e·s
dans des centres de rééducation en raison de leur participation à des
manifestations ou de leur refus de participer à des rassemblements pro‑régime.

Un nombre insoutenable de manifestant·e·s risquent, pour des raisons
purement politiques, d’être condamné·e·s à mort au terme de procédures
expéditives, d’aveux extorqués sous la torture, souvent sans avocat,
sans procès équitable.

Cette violente répression cible particulièrement les minorités kurdes
et baloutchs mais également la population LGBT+.

Une majorité des parlementaires iraniens (227 sur 260) ont en outre
appelé les autorités judiciaires à sanctionner de manière exemplaire
les manifestant·e·s, jusqu’à la peine de mort y compris pour les mineur·e·s.

S’il est difficile d’obtenir des informations complètes sur ce qui se
passe en Iran, notamment sur l’ampleur et la violence de la répression,
nous savons déjà que des condamnations à mort sont prononcées par dizaines.
Par exemple, celle prononcée en novembre 2022 contre le jeune Saman Yasin,
musicien d’origine kurde de 27 ans condamné à mort pour avoir soutenu en
musique sur les réseaux sociaux les manifestations ou celle de l’entraîneuse
de volley-ball Fahimeh Karimi, mère de trois enfants, condamnée à mort
pour sa participation aux manifestations de Pakdasht, province de Téhéran.

De premières exécutions ont déjà eu lieu malgré la mobilisation sans
faille de la communauté iranienne qui manifeste sa colère et son soutien
semaine après semaine.

Nous devons être à l’écoute et échanger avec les associations et l’Inter
collectif France Iran qui rassemble notamment “Femmes Azadi”, “Iran Justice”,
“Azadi 4 Iran”, “Neda d’Iran”, “Action 4 Iran”, “Queers and feminists for Iran libération”.

Nous leur devons la mobilisation des parlementaires, au rang desquels on
compte de nombreux écologistes, et de nombreuses autres initiatives.

La mobilisation de la diplomatie française et de la communauté internationale
est quant à elle à la traîne.
A tel point qu’un homme, Mohammad Moradi, étudiant iranien à Lyon, a
sacrifié sa vie pour que la communauté internationale réagisse. N’oublions pas son nom.

Il aura fallu 115 jours après le début de la révolte en Iran, pour que
le ministère français de l’Europe et des Affaires étrangères sorte de
l’indifférence et annonce enfin qu’il allait convoquer, début janvier 2023,
le chargé d’affaires iranien à Paris « pour lui signifier » sa « plus
ferme condamnation » des exécutions et de la répression.

Mais cela est très loin d’être suffisant, nous devons aller plus loin
pour que la République islamique d’Iran respecte les traités internationaux
dont elle est signataire, tel que le Pacte international relatif aux
droits civils et politiques du 23 mars 1976 qui prohibe la peine de mort.

Le 19 janvier 2022, le Parlement européen a adopté une résolution invitant
l’Union européenne et ses États membres à inscrire le corps des Gardiens
de la révolution sur la liste des organisations terroristes de l’Union européenne.

Si de nouvelles sanctions ont été adoptées par les ministres des Affaires
étrangères de l’Union européenne, à l’encontre de plusieurs responsables
des Gardiens de la Révolution islamique pour violations des droits humains,
l’Union européenne s’est cependant refusée à placer les Gardiens de la
Révolution islamique, bras armé de la répression, sur la liste des
organisations terroristes.

Motion
===============

Réuni à Paris les 4 et 5 février 2022, le Conseil Fédéral d’Europe Ecologie Les Verts :

- Apporte son soutien total et entier à la société civile iranienne et
  en particulier aux femmes ;

- Demande la libération immédiate de toutes les personnes détenues ou
  condamnées à mort à raison de participation aux manifestations ;

- Demande un accueil inconditionnel des Iraniennes et des Iraniens à
  l’État français et demande que le dispositif exceptionnel de la protection
  temporaire créée par la directive 2001 / 55 / CE soit proposé par la
  commission européenne au Conseil de l’Union européenne ;

- Demande aux Gouvernement et à l’Union européenne de favoriser la délivrance
  de visas à toute personne persécutée ;

- Appelle la communauté internationale à faire une priorité des violations
  des droits humains et des obligations au titre des traités dans tous
  les pourparlers et négociations avec l’Iran,

- Demande à l’Union européenne, qui a adopté des mesures restrictives
  liées à des violations des droits humains, parmi lesquelles un gel des
  avoirs et une interdiction de visa pour les personnes et entités
  responsables de graves violations des droits humains en Iran, ainsi
  qu’une interdiction d’exporter à destination de l’Iran des équipements
  susceptibles d’être utilisés à des fins de répression interne ou des
  équipements de surveillance des télécommunications, que ces mesures,
  prorogées jusqu’au 13 avril 2023, soient régulièrement mises à jour ;

- Approuve les récentes initiatives de la France, des États-Unis, du
  Royaume-Uni et de l’Allemagne tendant à dénoncer le manque de coopération
  de l’Iran dans la mise en œuvre de l’accord de Vienne sur le nucléaire
  iranien adopté le 18 octobre 2015 mais appelle à une cessation définitive
  de l’application de cet accord ;

- Invite le Gouvernement et l’Union européenne à consolider et à étendre
  la limitation de l’accès aux marchés primaire et secondaire des capitaux
  de l’Union pour les banques iraniennes, y compris celles implantées
  sur le territoire de l’Union européenne ;

- Demande au Gouvernement Français et à l’Union européenne d’ajouter des
  responsables iraniens dont les Gardiens de la Révolution, qui condamnent
  à mort des Iraniens et iraniennes, à la liste établie par l’Union des
  personnes faisant l’objet de mesures restrictives pour de graves
  violations des droits de l’homme et de saisir les biens et les avoirs
  qu’ils possèdent sur le territoire de l’Union européenne ;

- Demande au Gouvernement et à l’Union européenne d’ajouter l’instance
  iranienne des Gardiens de la Révolution à la liste des personnes, groupes
  et entités impliqués dans des actes de terrorisme et faisant l’objet
  de mesures restrictives ;

- Demande que la France rappelle son Ambassadeur en Iran et garde une
  représentation diplomatique officielle (chargé d’affaires…) susceptible
  de servir de refuge.
