.. index::
   pair: Erfan Mortezaei;  2023-02-08


.. _erfan_mortezaei_2023_02_08:

===============================================================================================
2023-02-08 Erfan Mortezaei : **"Ma cousine Jina Mahsa Amini ne croyait pas au voile"**
===============================================================================================

- https://www.lepoint.fr/monde/iran-ma-cousine-jina-mahsa-amini-ne-croyait-pas-au-voile-08-02-2023-2507959_24.php


.. figure:: images/erfan_mortezaei.png
   :align: center

Introduction
=============

Pourchassé par la République islamique et désormais réfugié en Europe,
Erfan Mortezaei, cousin de l’icône de la révolte des Iraniens, se confie au « Point ».


Il était l'Iranien le plus recherché d'Irak.

Réfugié depuis 2017 au Kurdistan irakien dans un camp d'entraînement à
Souleimaniye, Erfan Mortezaei a reçu ces derniers mois de nombreuses
menaces d'enlèvement et d'assassinat en provenance des services de sécurité
de la République islamique d'Iran.

Au-delà de son appartenance au parti kurde iranien Komala, un groupe
d'opposition de gauche installé en Irak ayant pratiquement abandonné
la lutte armée, ce jeune homme de 34 ans est surtout le cousin de
Mahsa Amini, cette Iranienne de 22 ans dont la mort le 16 septembre dernier
aux mains de la police des mœurs à Téhéran a provoqué un soulèvement
populaire sans précédent contre le régime iranien. Grâce à l'intervention
du Consulat de France à Erbil, il a pu obtenir un laissez-passer pour
Paris et se trouve désormais en sécurité dans un autre pays européen.

Il a accepté de se confier au Point.


Le Point : Où vous trouviez-vous quand votre cousine Mahsa Amini est décédée ?
=================================================================================

Erfan Mortezaei
----------------

Le jour où Jina Mahsa Amini a été tuée par le régime dictatorial d'Iran,
je me trouvais dans la ville de Souleimaniye, dans un camp du parti
iranien d'opposition Komala, où je mène mes activités politiques.

J'ai alors reçu un appel de ma famille qui m'a appris que lors d'un voyage
à Téhéran en compagnie de sa famille, Jina avait été arrêtée par la
police des mœurs et avait été violemment battue par celle-ci, ce qui
l'a plongée dans un coma de trois jours.

Armin Arefi Pourquoi appelez-vous votre cousine Jina et non Mahsa ?
======================================================================

Pourquoi appelez-vous votre cousine Jina et non Mahsa ?

Il existe en République islamique une loi non écrite empêchant de choisir
des prénoms kurdes lors de l'enregistrement des naissances d'enfants.

Il existe au Kurdistan dans les bureaux d'état civil un dictionnaire de
prénoms dans lequel figurent ceux qui sont autorisés par les autorités.
Ainsi, Mahsa est le nom qui a été retenu sur la carte d'identité de ma
cousine. Mais Jina est le prénom que sa famille lui a réellement donné
et c'est ainsi qu'on l'appelait à la maison. Voilà pourquoi elle a deux prénoms.

Des responsables iraniens affirment que votre cousine est décédée des
suites d'une maladie. Comment êtes-vous certain qu'elle a été frappée ?

Lorsqu'elle a été arrêtée, Jina était accompagnée de son frère.
Au moment de l'interpellation, ce dernier a précisé aux agents de police
qu'ils n'étaient pas de Téhéran et qu'ils se trouvaient en voyage dans
la capitale et leur a ainsi demandé de les laisser partir.
Il s'est ensuivi une altercation verbale. Le frère de Mahsa a été violemment
poussé vers l'arrière et Mahsa a été conduite de force dans la camionnette
de la police des mœurs, où se trouvaient déjà plusieurs femmes arrêtées.

Nous disposons de témoignages selon lesquels Jina a été frappée.

Un de ces témoins se trouve encore en Iran et a confirmé à la chaîne de
télévision Iran International que ma cousine a été violemment battue à
l'intérieur de la camionnette, et a notamment reçu un coup à la tête.
Elle a de nouveau été frappée au siège de la police des mœurs, situé
dans le quartier Vozara de Téhéran.

C'est là-bas qu'elle a fini par perdre connaissance, avant d'être conduite
à l'hôpital Kasra de la capitale. Mais le régime de la République islamique
n'est jamais venu admettre cette vérité auprès du peuple, car celui-ci
ne l'aurait jamais acceptée.
À la mort de ma cousine, le régime a été pris d'une grande peur et a tué
un nombre très important de gens, et nous avons connu cinq mois de
manifestations à travers tout l'Iran.

La télévision d'État a néanmoins diffusé une vidéo dans laquelle on voit
Mahsa Amini s'effondrer toute seule sans avoir reçu le moindre coup…

La République islamique a diffusé des enregistrements provenant des
caméras de sécurité du centre de détention. Laissez-moi vous poser une
question : comment se fait-il que les images diffusées ne comportent
aucune date ni heure, comme cela est toujours le cas dans ce type de
vidéos ?
Autre question, comment se fait-il qu'il fasse jour, d'après ces images,
lorsque Mahsa pénètre dans le bâtiment lorsqu'on observe les fenêtres,
et que la nuit tombe à peine deux minutes plus tard ?

Comment expliquez-vous que les personnes présentes à l'intérieur autour
de ma cousine changent tout à coup ?

Tout cela montre que ce film a été coupé et monté de façon à correspondre
au scénario échafaudé par les autorités. À plusieurs reprises, la famille
de Mahsa leur a demandé que l'on diffuse l'intégralité des enregistrements
vidéo, mais elles n'ont jamais répondu.

Comment se fait-il que la famille de Mahsa Amini, après avoir pris la parole
pour contester l'authenticité de cette vidéo, s'est depuis terrée dans le silence ?


La pression exercée par les agents du ministère iranien des Renseignements
et des renseignements des Gardiens de la révolution [l'armée idéologique
de la République islamique, NDLR] contre les parents de Mahsa Amini dans
leur ville de Saghez [ouest de l'Iran, dans la région du Kurdistan iranien]
a été d'une intensité que vous ne pouvez imaginer.
La cruauté dont ils ont fait preuve a plongé sa famille dans un profond
désarroi. Rendez-vous compte, ils ne les ont même pas laissés participer
aux cérémonies de commémoration du 40e jour après la mort de Mahsa !

À plusieurs reprises, ils ont diffusé des communiqués prétendument écrits
par la famille alors que ceux-ci n'étaient que de vulgaires faux !

Le but était que les parents de Mahsa se taisent et n'accordent aucune
interview à la presse iranienne ni étrangère. La pression des autorités
était telle qu'elles ont plusieurs fois menacé d'arrêter le propre frère
de Mahsa ainsi que son père.
Ils ont tenté d'emmener ses parents devant les caméras de la télévision
d'État afin qu'ils « confessent » que leur fille était en réalité malade
et que c'est pour cette raison qu'elle est décédée.
Mais sa famille a tenu bon et n'a jamais accepté de se livrer à un tel exercice.


Quel genre de personne était votre cousine ?

Jina était le deuxième enfant de la famille Amini. Avant qu'elle ne vienne
au monde, ses parents avaient déjà eu un garçon qui s'appelait Armin.
Mais celui-ci est mort très jeune des suites d'une maladie.

Jina est née deux ans plus tard. Elle était la lumière d'espoir de
ses parents et l'étincelle qui a redonné de la joie à sa famille.

À l'école, Jina frappait par son calme et son sourire en toutes circonstances.
Je ne me souviens pas personnellement que quiconque se soit jamais plaint
d'elle. Elle aimait profondément voyager, aller à la rencontre des gens,
en dépit des traditions conservatrices qui se dressaient parfois contre elle.


Ma cousine a montré qu’une femme pouvait diriger sa propre vie.

Qu'entendez-vous par là ?

Il existe au sein de la société kurde une série de traditions qui ne sont
pas forcément compatibles avec les valeurs que défend aujourd'hui la
jeunesse d'Iran. Il est par exemple mal vu qu'une fille soit
indépendante sur le plan économique.
Pour certains milieux conservateurs, les femmes doivent rester dans
l'ombre des hommes et s'occuper du foyer. Au contraire, Mahsa avait ouvert
un commerce de vente de manteaux dans un centre commercial de Saqqez,
et avait en ce sens brisé un tabou.
Avec ce travail, ma cousine a montré qu'une femme pouvait diriger sa
propre vie. Après son meurtre, les filles du Kurdistan et celles de tout
l'Iran sont arrivées à cette conclusion qu'elles pouvaient accéder à
la liberté, l'égalité et la justice.
Fort heureusement, la révolution à laquelle nous assistons aujourd'hui
a repoussé ces traditions.


Quelle était selon vous la position de Mahsa sur le port obligatoire du voile en Iran ?

Si vous vous penchez sur la question du voile en République islamique,
vous observerez qu'il s'agit surtout d'une loi qui oblige les femmes à
porter le hidjab sans leur demander leur avis. Le voile est une loi
patriarcale du régime dictatorial de la République islamique d'Iran
contre les femmes. Or celles-ci, au cours des 44 dernières années, n'ont
jamais montré qu'elles avaient choisi de le porter de leur propre gré.
Concernant ma cousine, si vous observez les différentes photos d'elle
qui ont été publiées, on voit que Jina respectait le voile de la même
manière que le reste des filles en Iran. C'est-à-dire qu'en famille,
le voile n'avait aucun sens. Si vous observez les films qui ont été
diffusés où on la voit participer à des fêtes de famille ou à des mariages,
Jina ne portait ni voile ni manteau [islamique]. Parce qu'elle n'y croyait
tout simplement pas. Mais quand elle quittait le domicile, elle était
obligée de le porter selon la loi. Voilà pourquoi Jina essayait de
revêtir le voile de manière à ne pas montrer qu'elle y croyait.
Si vous observez la manière dont elle portait le foulard, ses cheveux
dépassaient, ce qui était une manière pour elle de protester.


Le peuple iranien est-il descendu selon vous dans la rue pour Mahsa Amini
ou sa mort a-t-elle plutôt servi d'étincelle alors que la colère contre
le régime était déjà très forte ?

Avant la mort de Jina, le régime avait déjà tué un grand nombre de femmes,
comme la photographe irano-canadienne Zahra Kazemi en 2003 ou la manifestante
Neda Agha-Soltan en 2009. Or, à l'époque, la société iranienne n'avait
pas réagi de la même manière qu'avec ma cousine. La différence tient à
mon avis au moment où Jina a été assassinée.
Comme vous le savez, la République islamique est soumise aujourd'hui à
une énorme pression économique, qui impacte directement la société
iranienne.
Ainsi, celle-ci était ces derniers temps dans l'attente de la moindre
occasion pour déclencher une révolution. Et la mort de Mahsa a constitué
cette étincelle. Elle a été l'excuse pour que la colère que le peuple
avait accumulée au cours des dernières années explose et que les Iraniens
descendent dans la rue.
Mais cela n'aurait pas pu arriver sans l'obstination de notre famille
pour que la vérité sur le meurtre de Mahsa par la République islamique
éclate au grand jour, ainsi que le soutien du peuple kurde.
Ainsi, lorsque sa dépouille est revenue dans sa ville natale de Saqqez
pour y être enterrée le 17 septembre dernier, au moins 100 000 personnes
étaient présentes. C'est cette unité du peuple kurde qui a fait peur au
régime et poussé celui-ci à réprimer violemment les premières manifestations
qui ont éclaté dans cette ville.
Et c'est cette colère populaire qui s'est exprimée dès les premiers jours
dans les villes kurdes qui a fait que la contestation s'est ensuite
rapidement étendue aux quatre coins de l'Iran.

Que ressentez-vous aujourd'hui personnellement à l'évocation de son nom ? De la tristesse ou de la fierté ?

C'est un sentiment très étrange. Le meurtre de ma cousine m'a terriblement
attristé et j'ai alors ressenti une profonde colère vis-à-vis des dirigeants
de la République islamique, contre lesquels je voulais me venger.
Mais j'ai été ensuite heureux que ce soit le nom de Jina qui ait permis
au peuple iranien de s'unir à travers l'ensemble du pays comme nous
l'attendions.
Au cours des dernières années en effet, l'opposition iranienne frappait
par ses divisions, mais la mort de ma cousine a d'une certaine manière
permis de créer cette unité.
Cela a provoqué en moi, ainsi que chez ma famille, un grand sentiment
de joie, notamment quand nous nous sommes rendu compte que la mort de
Mahsa avait servi d'étincelle pour que le peuple descende dans la rue
et réclame la fin des discriminations de genre, la justice, l'égalité
et la liberté.
La vérité est que Jina était désormais décédée et qu'il fallait que nous
combattions pour nos droits.

Le nombre de manifestations en Iran a diminué en ce début d'année 2023.
Ne pensez-vous pas que le mouvement de contestation né après la mort
de votre cousine touche à sa fin ?

Non, je pense le contraire. En 1978, cela a pris dix-sept mois au peuple
iranien pour amener la révolution qui a renversé le régime du Shah.
Et à cette époque, il n'y avait pas de manifestations tous les jours.

Aujourd'hui, le peuple continue à protester mais de manière différente
et très réfléchie. Il ne faut donc en aucun cas interpréter la diminution
du nombre de manifestations de rue comme la fin de la révolution.

Chaque nuit, à Téhéran et dans les autres villes du pays, des slogans
contre les autorités sont lancés depuis les toits des maisons.
Chaque jour, des ouvriers, des enseignants, des médecins, des étudiants
et même des écoliers font grève.
Tout cela illustre la persistance d'une vaste colère populaire à travers
l'Iran. Une chose est sûre : la répression terrible menée par les autorités
iraniennes à l'intérieur du pays, ainsi que les ingérences dont se
rendent coupables les Gardiens de la révolution à l'étranger, nous
montrent que la République islamique n'a pas d'avenir.

La révolution se poursuit. La seule inconnue demeure la date de la chute du régime.





