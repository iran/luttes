.. index::
   pair: Grenoble ; Veillé de soutien
   pair: Grenoble ; Amnesty Internationale


.. _veillee_2023_02_10:

======================================================================================================================================
2023-02-10 Grenoble **Veillée de soutien aux victimes de la répression en Iran** organisée par Amnesty International Grenoble
======================================================================================================================================

- https://www.ici-grenoble.org/evenement/veillee-pour-larret-des-executions-en-iran-lannulation-des-condamnations-a-mort-la-liberation-des-prisonniers-politiques
- https://www.amnesty.fr/pres-de-chez-vous/grenoble-centre
- https://www.amnesty.fr/pays/iran

.. figure:: ../11/images/veillee.png
   :align: center


L'appel
=========

:download:`Télécharger l'appel à la veillée <doc/CP_Iran_10_février_2023VF1.pdf>`


Rassemblement aux bougies Place Félix Poulat Vendredi 10 février 2023 de 18H à 19H30

Pour la liberté d'expression en Iran avec Amnesty International,
la Ligue pour la Défense des Droits de l'Homme en Iran et le collectif
des Iranien.ne.s de Grenoble

Le 11 février 1979, le régime des mollahs prenait le pouvoir et imposait une dictature religieuse et
misogyne : dès fin février 1979, les compétitions sportives féminines sont annulées, en mars 1979 les
femmes sont exclues de la magistrature, le divorce est accordé uniquement aux hommes et le port
obligatoire du voile est institué pour les femmes au travail. Suivront dans la même année les 1ères
exécutions pour "vice", les lapidations, les coups de fouet, les quotas d'étudiantes dans les universités,
l'interdiction de voyager sans la permission du mari, etc...

En vertu de l’article 638 du Code pénal islamique iranien, tout acte jugé « offensant » pour la décence
publique est passible d’une peine d’emprisonnement allant de 10 jours à deux mois, ou de 74 coups de
fouet. La loi s’applique aux filles dès l’âge de neuf ans, l’âge minimum de la responsabilité pénale
pour les filles en Iran (15 ans pour les garçons). Dans la pratique, les autorités imposent le port
obligatoire du voile aux filles dès l’âge de sept ans, lorsqu’elles commencent l’école primaire

2ème pays en nombre d'exécutions capitales après la Chine, l'Iran poursuit chaque année des
milliers de personnes qui n'avaient fait qu'exercer pacifiquement leur droit à la liberté d'expression. La
torture et autres formes de mauvais traitements sont généralisées et systématiques.

La répression meurtrière du soulèvement populaire en Iran, qu’a suscitée l'arrestation de Jina Mahsa
Amini parce qu'elle n'avait pas mis correctement son voile, puis sa mort en détention le 16 septembre
2022, s’inscrit dans le sillage du cycle d’attaques violentes menées par les autorités contre la
population qui exprime ses revendications légitimes depuis fin 2017.

Au 7 février 2023, l'ONG Iran Human Rights dénombre 527 morts dont 71 enfants, et 20 000
arrestations au cours des manifestations depuis le 16 septembre 2022.

Amnesty International a recueilli des informations sur les crimes de droit international et autres graves
violations des droits humains perpétrés par les autorités iraniennes en marge des manifestations,
notamment les homicides, les arrestations et détentions arbitraires massives, les disparitions forcées, la
torture et les mauvais traitements et les condamnations à de lourdes peines de prison ou à la peine de
mort à l’issue de procès manifestement iniques pour "inimitié envers Dieu" ou "corruption sur
terre". Les autorités iraniennes font la sourde oreille face aux multiples appels de la communauté
internationale en faveur de l’ouverture d’enquêtes pénales sur ces crimes. Elles s’efforcent même de
détruire les preuves de leurs crimes, tout en persécutant les victimes et leurs familles qui réclament
vérité, justice et réparation.

A la veille de l'anniversaire de 44 ans de prise de pouvoir par les mollahs, nous vous
appelons à venir nous rejoindre Place Félix Poulat pour soutenir le peuple iranien dans
sa lutte pour l’égalité, le droit et la démocratie. Femme, Vie, liberté.


Contact Presse :

Véronique KLAJNBERG, secrétaire du groupe Amnesty International de Grenoble,
tel 07 82 84 96 95




Les photos
=============

- https://public.joomeo.com/albums/63e90b683725e (Veillée IRAN 10 février 2023)

.. figure:: images/femme_vie_liberte.png
   :align: center
