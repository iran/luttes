.. index::
   pair: Revendications; 2023-02-15
   pair: Sara Selami; 2023-03-30

.. _revendications_iran_2023_02_15:

==================================================================================================================
2023-02-15 **Déclaration des revendications minima d'organisations indépendantes syndicales et civiles d’Iran**
==================================================================================================================

- https://laboursolidarity.org/fr/n/2544/declaration-des-revendications-minima-d039organisations-independantes-syndicales-et-civiles-diran


Annonce sur Mastodon
=====================

- https://masthead.social/@NaderTeyf/109886186003520193

NaderTeyf@masthead.social - Le 15 février, vingt organisations syndicales
et civiles d'#Iran qui ont mené beaucoup de luttes ces dernières années
et de ce fait certains de leurs militants sont actuellement en prison ont
ont publié une déclaration des revendications minima au titre de #Femme_Vie_Liberté.
La traduction française de celle-ci est disponible sur le lien suivant:
https://laboursolidarity.org/fr/n/2544/declaration-des-revendications-minima-d039organisations-independantes-syndicales-et-civiles-diran


Dessin de Baharh Akrami
==========================

- :ref:`akrami_iran:dessins_bahareh_akrami`

.. figure:: images/akrami.png
   :align: center


.. _sara_selami_2023_03_30:

**2023-03-30 Discours de Sara Selami au 53ème congrès de la CGT le jeudi 30 mars 2023 pour porter un message fraternel du syndicat des travailleurs de la régie de transports de Téhéran et sa banlieue (Vahed) et évoquer la situation politique de son pays**
====================================================================================================================================================================================================================================================================

- :ref:`sarah_selami_cgt_2023_03_30`
- https://www.youtube.com/watch?v=5cutmIP6uHM
- https://youtu.be/5cutmIP6uHM?t=657 (citation de la charte)

.. figure:: images/sara_selami.png
   :align: center

   https://www.youtube.com/watch?v=5cutmIP6uHM

**Sara Selami**, militante ouvrière et politique iranienne, était invitée
au 53ème congrès de la CGT pour porter un message fraternel du syndicat
des travailleurs de la régie de transports de Téhéran et sa banlieue
(Vahed) et évoquer la situation politique de son pays.


Discours du samedi 25 février 2023 à Grenoble
==================================================

- :ref:`video_2023_02_25`

Introduction
===============

Femme, Vie, Liberté

Peuple juste et épris de liberté d’Iran,

Au quarante-quatrième anniversaire de la révolution de 1979, les fondements
économique, politique et social du pays sont dans un tourbillon de crises
et de décomposition de telle sorte qu’aucune perspective n’est envisageable
dans le cadre de la superstructure politique actuelle.

C’est pourquoi depuis cinq mois le peuple opprimé d’Iran – femmes, jeunes
épris de liberté et d’égalité – ont transformé les rues des villes de tout
le pays en une arène historique et décisive du combat pour mettre fin à
la situation inhumaine actuelle, et cela en mettant en danger leur vie
à cause de la répression sanglante de l'État.

**Les femmes, étudiants, enseignants, ouvriers, ceux qui demandent justice
(familles et proches des prisonniers politiques ou des morts dans les manifestations),
artistes, queers, écrivains et tout le peuple opprimé du pays, du Kurdistan
au Sistan et Baloutchistan, ont levé le drapeau des protestations fondamentales
contre la misogynie, l’exclusion sexiste, l'interminable insécurité économique,
l’esclavage de la force du travail, la pauvreté, l’oppression de classe
et l’oppression nationaliste et religieuse.**

Tels sont les maux de notre société que le despotisme religieux ou non-religieux
nous imposent depuis plus d’un siècle.
Les femmes et hommes qui luttent en Iran ont un soutien international sans précédent.

Les protestations profondes actuelles viennent des grands mouvements
sociaux modernes et du soulèvement d'une génération qui n'a plus peur et
veut mettre fin à un siècle d’arriération et relever le défi de construire
une société moderne de bien-être et de liberté.

Après deux grandes révolutions dans l’histoire contemporaine d’Iran, les
grands mouvements sociaux pionniers – mouvement ouvrier, mouvement des
enseignants et retraités, mouvement égalitaire des femmes, étudiantes et
jeunes, mouvement contre la peine de mort… – veulent changer la structure
politique, économique et sociale du pays en intervenant en masse et
depuis en bas.

C’est pourquoi ce mouvement veut mettre fin, une fois pour toute, à l'existence
d’un pouvoir d’en haut et commencer une révolution sociale, moderne et
humaine pour l’émancipation du peuple de toute forme d’oppression, d’exclusion,
d’exploitation et de dictature.

Nous, les organisations syndicales et civiles signataires de la présente
déclaration, **nous nous concentrons sur l’unité et la construction de liens
entre mouvements sociaux et revendicatifs, et sur la lutte contre la
situation inhumaine et destructrice actuelle**.

Nous considérons que l’aboutissement des revendications de base sont les
premières exigences des protestations de fond du peuple d’Iran.
Elles préparent les fondements de l’établissement d’une société nouvelle,
moderne et humaine.

.. _revendications_iran_2023_02_15_texte:

Revendications revendications minima d'organisations indépendantes syndicales et civiles d’Iran
====================================================================================================

:download:`Au format PDF <images/revendications.pdf>`

Nous demandons à tous les êtres humains justes qui ont un cœur battant
pour la liberté, l’égalité et l’émancipation de lever l’étendard de ces
revendications de l’usine à l’université, des écoles aux quartiers, et
partout dans le monde.

1. Libération immédiate et sans condition de tous les prisonniers politiques,
   interdiction de criminalisation des activités politiques, syndicales
   et civiles, jugement public des commanditaires et agents des répressions
   des protestations populaires ;

2. Liberté sans condition d’opinion, d’expression, de pensée, de presse,
   d’organisation, de groupes locaux et nationaux syndicaux et populaires,
   de rassemblement, de grève, de manifestation, de réseaux sociaux et
   de médias audiovisuels ;

3. Abolition immédiate de la peine de mort, de la loi du talion et interdiction
   de toute sorte de torture physique et psychologique ;

4. Etablissement immédiat de l’égalité des droits entre les femmes et les
   hommes dans tous les domaines politique, économique, social, culturel,
   familial.

   Abolition immédiate de toutes les lois et formes d’exclusion contre
   les appartenances sexuelles et reconnaissance de la communauté LGBTQ+.

   Décriminalisation de toutes les tendances et appartenances sexuelles
   et respect sans condition de tous les droits des femmes pour contrôler
   leur corps et leur destinée, interdiction du contrôle patriarcal ;

5. Non-intervention de la religion dans les lois politiques, économiques,
   sociales et culturelles : la religion est une affaire personnelle ;

6. Renforcement de la sécurité des lieux de travail et de l’emploi.
   Hausse immédiate des salaires des ouvriers, enseignants, fonctionnaires
   et de tous les travailleurs actifs et retraités, par la présence,
   l’intervention et l'accord des représentants élus d'organisations
   indépendantes et nationales ;

7. Suppression des lois basées sur l’exclusion, l’oppression nationale,
   religieuse, et création des institutions adéquates pour soutenir et
   distribuer justement et égalitairement les moyens publics pour le
   progrès culturel et artistiques dans toutes les régions du pays, et
   mise en place des moyens nécessaires et identiques pour tous, pour
   l’apprentissage et l’éducation de toutes les langues existantes dans le pays ;

8. Suppression des organes de répression, limitation des prérogatives
   de l'État et intervention directe et permanente de tous dans l’administration
   des affaires du pays par les conseils de quartier et nationaux.

   Révocabilité de tout responsable gouvernemental ou autre à tout moment,
   ce qui doit être un droit de base de tous les électeurs ;

9. Confiscation des fortunes de toutes les personnes morales ou physiques,
   des organes étatiques, semi-étatiques et privés qui pillent directement
   ou par la rente gouvernementale les biens et richesses sociales du peuple
   d’Iran.

   Le montant des confiscations doit être utilisé immédiatement pour la
   modernisation et refondation de l’éducation nationale, des caisses de
   retraites, de l’écologie et des besoins des populations des régions
   d’Iran **qui ont beaucoup souffert sous les deux régimes islamique et monarchique** ;

10. Fin des destructions écologistes, application des politiques fondamentales
    pour la reconstruction des structures écologiques détruites depuis
    un siècle et restitution à la propriété publique de toutes les parties
    de la nature qui ont été privatisées, entre autres les pâturages,
    les plages, les forêts et les montagnes ;

11. Interdiction du travail des enfants et garantie pour leur vie quotidienne
    et pour leur éducation, indépendamment de la situation économique et
    sociale de leurs familles. Création d'assurances chômage et d'une
    sécurité sociale fortes pour toutes les personnes en capacité de
    travailler ou non. Gratuité de l’éducation et du système de santé
    pour toutes et tous ;

12. Normalisation des relations extérieures au plus haut niveau avec
    tous les pays du monde, basée sur des relations justes et le respect
    réciproque. Interdiction des armes atomiques et efforts pour la paix mondiale.

Nous pensons que les revendications de base ci-dessus sont réalisables
immédiatement, étant donné les ressources réelles et potentielles du pays,
une population consciencieuse et capables, des jeunes et adolescents qui
ont la conviction de pouvoir avoir une vie décente, gaie et libre.

Ces revendications sont les axes généraux des signataires.

Il est évident que la poursuite de la lutte et de la solidarité nous
permettront d’apporter plus de précisions.

Signataires
==============

- Conseil de coordination des associations syndicales des enseignants d’Iran
- Union libre des ouvriers d’Iran
- Union des associations étudiantes unitaires
- Association des défenseurs des droits de l’homme
- Syndicat des ouvriers de la canne à sucre Haft – Tapeh
- Conseil d’organisation des protestations des ouvriers pétroliers non – contractuels
- Maison des enseignants d’Iran
- Éveil féminin
- Voix des femmes d’Iran
- Voix indépendante des ouvriers métallurgistes du groupe national de l’aciérie d’Ahvaz
- Association de défenseurs des droits ouvriers
- Association syndicale des ouvriers électriciens et métallurgiste de Kermanchah
- Comité de coordination pour l'aide à la constitution des syndicats ouvriers
- Union des retraités
- Conseil des retraités d’Iran
- Association des étudiants progressistes
- Conseil des élèves libres-penseurs d’Iran
- Syndicat des ouvriers peintres en bâtiment de la province de l’Alborz
- Comité de soutien à la fondation de syndicats ouvriers d’Iran
- Conseil des retraités de la Sécurité sociale

