.. index::
   pair: Pact; 2023-02-23
   pair: Pact; Justice

.. _justice_seeking_pact_2023_02_23:

====================================================================================
2023-02-23 ⚖️ **Iranian Activists Announce “Justice-Seeking” Pact in New Statement**
====================================================================================

- https://iranhumanrights.org/2023/02/iranian-activists-announce-justice-seeking-pact-in-new-statement/


A coalition of Iranian human and civil rights defenders, including prominent
activists Shirin Ebadi, Nasrin Sotoudeh, Narges Mohammadi, and Atena,
published a “Justice-Seeking Pact” on February 23, 2023.

Following is the Center for Human Rights in Iran’s (CHRI) translation
of the Persian text.


Introduction
=============

After more than four decades, there are hardly any people in our country
who have not been struck by terror, imprisonment, torture, execution, and
other forms of violence committed by the [government of the] Islamic Republic
of Iran.

The harms from state repression, as well as arbitrary violence perpetrated
by non-state agents, are countless.

Individual or collective killings and mass intimidation have been one
side of the policy of eliminating “others” in our society.

The flip side is depriving large sections of society of their fundamental
rights in every arbitrary, violent, and cruel manner.
[Among the] harmful effects of removing “others” is the fragmentation
of society and the spread of animosity among various social groups.

The hope of freedom was extinguished by retribution and vengeance in a
cold February avalanche [revolution of 1979].
Today, based on bitter experiences of history, we have found that prison,
torture and killing are deviations from democracy, freedom and justice;

that the foundation of injustice rides on hiding reality and depriving
people of the right to know.
The people who remember are less likely to be victims of ignorance and
oppression, or agents of oppression against each other, and do not accept
the rule of violence and discrimination.

Hiding the truth for any excuse, even under the pretext of “national reconciliation”
or “establishing peace and democracy,” amounts to erasing memories and
burning history, which results in repeating the past.

Without complete knowledge of the tragedy, seeking justice would not be
possible…

Today, in our country, the progressive and egalitarian “Women, Life, Freedom”
movement has emerged, whose foundations are to seek and realize truth
and justice.
We must use the knowledge and findings from our history to advance this
movement.
We, who are the survivors of more than four decades of violence, killings
and ruin, whose loved ones have been executed, murdered and tortured by
the state; we, who have tried to advance justice to the best of our ability,
while insisting on truth and justice, must courageously and explicitly
demand unquestioning compliance with human rights and dignity for everyone;
an end to torture and executions, even for the perpetrators and brokers of massacres.

We will take this opportunity to build the foundations for solidarity to
advance the “Women, Life, Freedom” movement.

**From this day on, until the end of hope, we, the signatories of this pact,
with adherence to all international conventions and agreements regarding
the observance of human rights, declare our commitment to**

- Abolition of the death penalty in Iran.
- The right to seek justice and the truth in any circumstance.
  This requires cooperation and coordination with domestic and international
  institutions to clarify all the facts about gross violations of human
  rights in Iran, especially political crimes, some of which are clear
  examples of crimes against humanity.
- Ending impunity for perpetrators of political crimes.
- Complete reorganization and independence of the judicial system.
- Fighting amnesia, by taking in all varieties of narratives as well as
  accurate information for the historical record, without exception.

February 2023

Signatories
=================

Baqer Ebrahimzadeh, Hamed Ismailiyoun, Hamid Ashtari, Mehdi Aslani, Ziba Azami,
Shahnaz Akmali, Asghar Izadi, Monireh Baradaran, Shohreh Badiei, Ladan Broumand,
Sholeh Pakravan, Mahsa Piriaie, Zohreh Tonekaboni, Siroos Javidi, Aghdas Javidi,
Fattaneh Jowkar, Golrokh Jahangiri, Sediqeh Hajmohsen, Ali Hojjat,
Amjad Hossein-Panahi, Akram Khatam, Nasim Khaksar, Atena Daemi, Saeid Dehghan,
Anahita Rahmani, Taghi Rahmani, Ribin Rahmani, Mihan Roosta, Zari Riahi,
Simin Sami Kermani, Nasrin Sotoudeh, Shokoofeh Sakhi, Sayeh Saeedi Sirjani,
Abolfattah Soltani, Farzaneh Soltani, Mona Silavi, Atash Shahkarmi,
Shahla Shafiq, Shirahmad Shirani Naroie, Sima Sahebi, Abbas Sadeghi,
Keyvan Samimi, Esmat Talebi, Shirin Ebadi, Maryam Ajam, Reza Allamehzadeh,
Malaekeh Alam Houli, Reza Alijani, Homayoun Alizadeh, Massoud Fathi,
Parastou Forouhar, Parvin Fahimi, Mahin Fahimi, Mersedeh Ghaedi, Atieh Ghoreishi,
Mehrangiz Kar, Kazem Kardavani, Kaveh Kermanshahi, Raouf Kaabi, Barbad Golshiri,
Effat Mahbaz, Mahnaz Matin, Shahrazad Mojab, Mahboubeh Mojtahed, Mersedeh Mohseni,
Narges Mohammadi, Parviz Mokhtari, Shahnaz Moratab, Iraj Masdaghi, Mahmoud Memaranjad,
Reza Moini, Rezvan Moghaddam, Shora Makkaremi, Nasser Mahajer, Shahin Nassiri,
Akram Neghabi, Hamid Nozari, Mohammad Reza Nikfar, Vahid Vahdathagh,
Esmat Vatanparast, Sadieh Vakili, Shokoofeh Yadollahi.

