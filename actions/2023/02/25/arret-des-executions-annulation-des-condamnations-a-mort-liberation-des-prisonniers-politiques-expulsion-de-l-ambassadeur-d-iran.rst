
.. index::
   pair: Rassemblement ; Samedi 25 février 2023


.. _iran_grenoble_2023_02_25:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ **Femme, Vie, Liberté** 23e rassemblement samedi 25 février 2023 à Grenoble **place Félix Poulat** à 14h30 **Arrêt des exécutions, annulation des condamnations à mort libération des prisonniers politiques, expulsion de l'ambassadeur d'iran** 📣
==============================================================================================================================================================================================================================================================

Arrêt des exécutions, annulation des condamnations à mort liberation des prisonniers politiques, expulsion de l'ambassadeur d'iran
162e jour depuis la mort de #JinaMahsaAmini (2022-09-16)

- :ref:`mahsa_jina_amini`
- :ref:`slogans`


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|

.. figure:: images/annonce.png
   :align: center




Texte annonçant le rassemblement du samedi 25 février 2023
==============================================================

Pour le 23e samedi depuis l'assassinat de Jina Mahsa Amini, nous nous
rassemblerons sur la place Félix Poulat de Grenoble pour être la voix du
peuple iranien dans son combat contre le régime meurtrier du gouvernement
islamique d'Iran.

Il y a une révolution en cours en Iran.

Le mardi 13 septembre 2022 Jina Mahsa Amini était arrêtée par la police
des mœurs à Téhéran car elle n’était pas "assez voilée".

Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.
Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

Nous demandons des gouvernements des pays occidentaux :

- d'arrêter tout contrat et de négociation avec le gouvernement islamique d'Iran,
- de mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort,
- d'expulser les ambassadeurs et les représentants du gouvernent islamique
- de mettre sur la liste noire les acteurs des massacres et de confisquer
  leurs bien a l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste. Soyez leurs voix. les iraniens ont
besoin d'un soutien réel et d'actions réelles pour en finir avec ce régime
sanguinaire; le blabla ne suffit pas.

Nous irons jusqu’à la liberté vraie, humaine, cette fois-ci jusqu’au bout.

Le 15 février 2023, vingt organisations syndicales et civiles d’#Iran
qui ont mené beaucoup de luttes ces dernières années et de ce fait
certains de leurs militants sont actuellement en prison, ont publié
une déclaration des revendications minima au titre de #Femme_Vie_Liberté.
La traduction française de celle-ci est disponible sur le lien suivant:
https://laboursolidarity.org/fr/n/2544/declaration-des-revendications-minima-d039organisations-independantes-syndicales-et-civiles-diran

Toute notre solidarité aux populations de Syrie et de Turquie très durement
touchées par le terrible tremblement de terre qui a déjà fait plus de 46000 victimes.
(https://cdkf.fr/appel-a-soutenir-les-victimes-du-seisme-au-kurdistan/))

- http://bird.trom.tf/Le_CDKF/status/1626316481607737346#m

Pour venir en aide aux victimes du séisme dévastateur qui a fait des dizaines
de milliers de morts et laissé des millions de personnes sans abri, nous vous
invitons à une journée de soutien le 25/02 à @montreuil
@Francelibertes @UnionSolidaires @MrapOfficiel @mvtpaix @FranceKurdista1

Revendications
===============================================================

- :ref:`revendications_mediapart`

.. include:: ../../../../revendications/revendications_2023_03_07.txt


Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`soutiens_avocats_2023_01_10`

Les victimes de la barbarie des mollahs
--------------------------------------------

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|

Les otages français
=======================

.. include:: ../../../../repression/otages/liste_2023_02_10.txt


Quelques événements du 20 février 2023 au 26 février 2023
=============================================================================

- :ref:`semaine_08_iran_2023`


Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/
- https://cdkf.fr/appel-a-soutenir-les-victimes-du-seisme-au-kurdistan/

.. figure:: images/annonce_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/rassemblement-de-solidarite-avec-le-peuple-iranien-pour-larret-des-executions-la-liberation-des-prisonniers-politiques


Mobilizon
------------

- https://mobilizon.chapril.org/@iranluttes38
- https://mobilizon.chapril.org/events/4e6198cc-1c7e-4829-8293-e090345f71dd



Quelques photos
=================


.. figure:: images/zoya.png
   :align: center


.. figure:: images/zoreh.png
   :align: center


.. figure:: images/ukraine.png
   :align: center



.. _video_2023_02_25:

Vidéos peertube Déclaration des revendications minima d'organisations indépendantes syndicales et civiles d’Iran
===================================================================================================================

- https://sepiasearch.org/search?search=grenoble+iran
- https://iran.frama.io/luttes/actions/2023/02/15/declaration-des-revendications-minima-des-organisations-independantes-syndicales-et-civiles-d-iran.html

.. figure:: images/peertube_orion.png
   :align: center

   https://www.orion-hub.fr/w/nCWN7BLBUwJt6qWYaBbcE7


Autres événements ce samedi 25 février 2023 à Grenoble
==========================================================


.. figure:: images/ici_grenoble_2023_02_25.png
   :align: center

   https://www.ici-grenoble.org/agenda


Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2023_02_18`
- :ref:`iran_grenoble_2023_02_11`
- :ref:`iran_grenoble_2023_02_04`
- :ref:`iran_grenoble_2023_01_28`
- :ref:`iran_grenoble_2023_01_21`
- :ref:`iran_grenoble_2023_01_14`
- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


