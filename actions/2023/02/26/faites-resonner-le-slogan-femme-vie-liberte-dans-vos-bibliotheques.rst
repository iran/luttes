

.. _femmevieliberte_wix_2026_02_26:

==============================================================================================================================================================================================================================================================
2023-02-26 **Faites résonner le slogan Femme, vie, liberté dans vos bibliothèques**
==============================================================================================================================================================================================================================================================

- :ref:`femmevieliberte_wix`

Bonjour,

nous sommes des bibliothécaires mobilisées pour la liberté, l'égalité et
la solidarité et touchées par la révolution féministe et intersectionnelle
en cours en Iran, réprimée avec une violence inouïe.

Nous avons créé le site Femme Vie Liberté (https://femmevieliberte.wixsite.com/my-site)
pour mobiliser le monde de la culture et faire un appel à contributions
artistiques afin de ne pas laisser le silence et l'indifférence s'installer.


Nous avons sollicité des artistes à s’emparer du slogan Femme, vie, liberté
afin qu’il soit vu et partagé par le plus grand nombre de personnes.

Ilya Green, Mélanie Body, Henri Meunier, Mandragore, Benjamin Flao,
Baudoin, Laetitia Rouxel, Anne Defreville, Jean-Louis Tripp et bien
d’autres, nous ont envoyé leurs contributions.


Elles sont en téléchargement libre sur notre site et sur notre page
facebook https://www.facebook.com/Femmevieliberte


Nous vous incitons à les imprimer, les exposer, les partager au plus
grand nombre, à faire des tables de présentation (voir quelques ressources
bibliographiques sur notre site) et à nous envoyer des photos afin qu’on les publie.


Soyons leur voix !

Merci
