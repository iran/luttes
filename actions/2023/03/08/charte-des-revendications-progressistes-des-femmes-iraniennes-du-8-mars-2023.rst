.. index::
   pair: Revendications ; 8 mars 2023
   ! 8 mars 2023

.. _charte_2023_03_08:

========================================================================================
**♀️ Charte des revendications progressistes des femmes iraniennes du 8 mars 2023**
========================================================================================

A vérifier
=============

- https://www.rferl.org/a/iranian-activist-women-bill-rights/32310681.html

Introduction
==============

- https://twitter.com/jonathanpiron1/status/1633380152347435008#m
- https://telegra.ph/%D9%85%D9%86%D8%B4%D9%88%D8%B1-%D9%85%D8%B7%D8%A7%D9%84%D8%A8%D8%A7%D8%AA-%D9%BE%DB%8C%D8%B4%D8%B1%D9%88-%D8%B2%D9%86%D8%A7%D9%86-%D8%A7%DB%8C%D8%B1%D8%A7%D9%86-03-07


À l'occasion du 8 mars 2023, plusieurs militantes iraniennes publient
une "Charte des revendications progressistes des femmes iraniennes", avec
plusieurs "revendications progressistes,modernes et laïques" et se liant
à d'autres groupes contestataires.


Charte des revendications progressistes des femmes iraniennes ♀️
==================================================================

- Liberté de se couvrir et abolition du hijab obligatoire
- Abolition complète de la ségrégation sexuelle à tous les niveaux de la société
- Séparation de la religion du gouvernement, du système judiciaire et de l'éducation
- Liberté de pensée, de croyance et d'expression
- Abolition de toute tutelle sur les femmes, reconnaissance des femmes
  en tant qu'êtres humains avec une identité indépendante et libres de
  prendre des décisions dans leur vie personnelle et sociale sans l'implication
  d'aucune sorte de personne physique ou morale
- Égalité inconditionnelle des hommes et des femmes dans l'usage de tous
  les lieux publics, y compris les stades sportifs
- Assurer la sécurité sexuelle, physique et mentale des femmes à toutes
  les heures de la journée et dans tous les lieux de la société
- Interdiction du test de virginité
- Avoir le droit de voyager et de quitter le pays
- Égalité inconditionnelle des hommes et des femmes sur le plan juridique
  et des activités sociales dans les domaines éducatif, sportif, politique,
  économique et judiciaire
- L'égalité dans toutes les lois du travail et l'assurance sociale ainsi
  que l'égalité de rémunération pour un travail similaire avec des
  considérations juridiques pendant la grossesse et l'accouchement
- Une assurance chômage, maladie et retraite suffisante
- Abolition générale de la loi du concubinage
- Égalité complète dans les lois sur la famille, y compris l'égalité dans
  la tutelle et la gestion de toutes les questions liées aux travaux ménagers,
  aux enfants, aux biens et autres questions qui sont conjointement liées au couple
- Accès des femmes aux contraceptifs gratuits et accès gratuit aux tests
  de dépistage de grossesse
- Le droit à un avortement sécurisé
- Avoir des droits égaux pour le divorce et la garde des enfants en cas de séparation
- Criminalisation et sanction de tout type de violence et d'abus physique,
  mental et verbal, de tout type d'humiliation et de restriction à l'égard
  des femmes dans tous les domaines et pour toute raison religieuse,
  traditionnelle, patriarcale et honorable
- Interdiction du hijab pour les enfants
- Une éducation gratuite et égale pour tous les enfants vivant dans le
  pays sans exception
- Interdiction du mariage des enfants et du mariage des enfants de moins
  de 18 ans, interdiction de toute vente et transaction d'enfants et de femmes
- Reconnaître un troisième sexe et garantir les droits et la sécurité de
  la citoyenneté aux personnes trans, et abolir toute obligation légale de changer de sexe

Femme, vie, liberté

Suivi des noms des signataires
