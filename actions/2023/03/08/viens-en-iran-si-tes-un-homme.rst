.. index::
   ! Viens en Iran si t'es un homme
   pair: Niloufar Hamedi ; Viens en Iran si t'es un homme
   pair: Elaheh Mohammadi ; Viens en Iran si t'es un homme
   ! Shafiei Kadkani

.. _viens_en_iran_2023_03_08:

========================================================================================
**Viens en Iran si t'es un homme** 📣
========================================================================================

- https://blogs.mediapart.fr/sirinealkonost
- https://blogs.mediapart.fr/sirinealkonost/blog/080323/viens-en-iran-si-tes-un-homme

Introduction
===============

En ce 8 mars 2023 porteur de nombreux combats, je reprends quelques mots au
poète **Shafiei Kadkani**, pour tenter de porter jusqu'ici la flamme du combat
des femmes d'Iran.

Vous pouvez les soutenir en allant manifester (à 14:00 place de la République
pour les parisiens), ou simplement en amplifiant leur voix.


Shafiei Kadkani
===================

- https://en.wikipedia.org/wiki/List_of_Persian-language_poets_and_authors
- https://fr.wikipedia.org/wiki/Mohammad_Reza_Shafiei-Kadkani

Mohammad Reza Shafiei-Kadkani (persan : محمدرضا شفیعی کدکنی), né le 12 octobre
1939 à Nichapur au Khorasan-e-razavi, est un des poètes et sémiologue
iraniens du XXe siècle.

Il est notamment connu pour son œuvre Musicalité de la poésie.

Il a édité les travaux des poètes classiques persans les plus importants,
notamment Bidel Dehlavi.

Texte 📣
============

Viens en Iran, si t'es un homme,

Viens et sois Nika,
------------------------

qui avait à peine dix-sept ans lorsqu'elle est descendue dans la rue et
a grimpé sur des capots de voitures et sur des poubelles, mettant le feu
à son foulard et l'agitant vers la police tout en scandant "femme vie liberté"

Nika a été pourchassée, battue, enlevée et rendue à sa famille dans une
housse mortuaire.

Viens et sois Maedeh, Setareh, Sarina, ou une de ces adolescentes qui font
exactement la même chose, à chaque manifestation à travers le pays depuis
près de six mois maintenant,

Alors qu'elles savent ce qui est arrivé à Nika.

Viens en Iran si t'es un homme

Viens et sois Asra
------------------------

Qui est allée à l'école un jour et a reçu pour consigne de chanter une
chanson de propagande à la gloire du dictateur, avec ses camarades de
classe, devant des représentants du régime. Les élèves et même des
enseignants, ont été poursuivis et frappés à coups de matraque pour
avoir refusé d'obtempérer. Certaines ont dû être hospitalisés


Asra a été frappée trop fort: elle a été rendue à sa famille dans une
housse mortuaire.

Viens, et sois une de ces écolières qui vont encore à l'école tous les
matins, et refusent de se couvrir les cheveux, et refusent également de
se laisser imposer la propagande du régime, et risquent l'empoisonnement
au gaz et les abus sexuels.

Alors qu'elles savent ce qui est arrivé à Asra


Viens en Iran si t'es un homme,

.. _viens_et_sois_nasrin:

Viens et sois Nasrin
------------------------

- https://fr.wikipedia.org/wiki/Nasrin_Sotoudeh

Qui se bat sans relâche pour les droits humains, les droits des femmes
et les droits des prisonniers politiques, au point d'en devenir une
elle-même et d'endurer des années d'isolement.

Sa santé est fragile et de temps en temps elle est libérée sous caution
ou assignée à résidence, mais à chaque fois, au détriment de sa propre
sécurité et de celle de sa famille, elle reprend la parole, toujours,
exigeant la justice, haut et fort,

Nasrin retournera en prison, encore et encore, et continuera à parler quand même.


Viens et sois Atena, Fatemeh et tous les autres avocats et défenseurs
des droits humains qui continuent d'être envoyés en prison. Encore et encore.

Alors qu'ils savent bien comment ça finit.

Viens en Iran si t'es un homme,

Viens et sois Niloufar et Elaheh
----------------------------------

Viens et sois Niloufar et Elaheh qui ont écrit dans la presse sur la mort
de Mahsa Amini, après qu'elle ait été transportée à l'hôpital avec tous
les symptômes d'une hémorragie cérébrale, suite à son arrestation brutale
pour port inapproprié de son foulard, et ont été arrêtées et emprisonnées
pour avoir fait leur travail de journalistes.

C'était il y a des mois et elles risquent maintenant toutes les deux d'être
exécutées pour espionnage.

Viens et sois Nazila, Elnaz et tous les autres journalistes qui essaient
de faire leur travail et de raconter au monde ce qui se passe en Iran,

Alors qu'ils sont au courant de ce qui est arrivé à Niloufar et Elaheh

Viens en Iran si t'es un homme,

Viens et sois Aida
-----------------------

Viens et sois Aida, qui se rendait chaque jour au domicile des manifestants
blessés après sa garde à l'hôpital, pour soigner leurs blessures sans
les exposer au risque d'être arrêtés s'ils se rendaient aux urgences.

Aida a disparu lors d'une de ces rondes et a été rendue à sa famille dans
une housse mortuaire

Viens et sois un de ces médecins, infirmières ou même vétérinaires qui
continuent de faire le sale boulot, réparant les peaux déchirées et les
os brisés, nettoyant les yeux explosés et les plaies ouvertes, au cœur
de la nuit, dans les arrière-cours et les salons à travers l'Iran...

Alors même qu'ils savent ce qui est arrivé à Aida.

Viens en Iran si t'es un homme,

Viens et sois Bahar
----------------------

Viens et sois Bahar, qui a posté en ligne  ses peintures et distribué des
flyers en faveur de la révolution, et a été poursuivie jusqu'à son
appartement par des agents en civil avant d'être jetée par sa propre fenêtre.

Viens et sois Astiaj
---------------------------

Viens et sois Astiaj, qui a été condamnée à 10 ans d'emprisonnement, aux
côtés de son petit ami, pour le crime d'avoir dansé ensemble devant la
tour Azadi.


Viens et sois l'un des artistes anonymes qui risquent leur vie en diffusant
leur art au grand jour, même s'ils savent ce qui est arrivé à Bahar et Astiaj


Viens en Iran si t'es un homme


.. _viens_et_sois_zeynab:

Viens et sois Zeynab
-----------------------------

Zeynab était le stéréotype de l'Insta-Mamma, avec une touche de fierté
tribale en plus (oui, l'Iran est une nation de nombreuses tribus).

Zeynab publiait fièrement photo après photo de ses deux garçons et de
son mari Maysam en tenues tribales traditionnelles complètes, et des
vidéos de son premier-né, l'ingénieur en herbe Kian, âgé de 9 ans,
expliquant ses inventions et ses expériences.

Au cours des manifestations qui ont accompagné dans tout le pays la
cérémonie de deuil marquant le 40e jour après la mort de Mahsa Amini,
la voiture de Zeynab a été prise pour cible par les forces du régime.

Son mari Maysam a reçu une balle dans la colonne vertébrale et, à ce jour,
n'a pas récupéré sa mobilité.

Kian a reçu une balle dans la poitrine et en est mort. Mais sa mère s'est
assurée qu'il ne voie jamais l'intérieur d'une housse mortuaire.

Après 40 jours de manifestations et des dizaines d'histoires d'horreur,
Zeynab savait que les forces du régime ont pour habitude de retenir les
corps de leurs victimes en otage, pour faire pression sur les familles,
procédant parfois à des enterrements secrets dans des cimetières aléatoires,
à leur insu.

Ainsi, alors que son mari blessé (et parfaitement ignorant du sort de leur fils)
était emmené à l'hôpital, Zeynab, qui devait encore s'occuper de son fils
cadet de 4 ans, a fait du porte-à-porte, demandant à un voisin après
l'autre des blocs de glace, pour empêcher le corps de son enfant de se
décomposer en attendant l'enterrement.

(vous pouvez faire une pause de quelques secondes ici, le temps de comprendre
pleinement le sens de la phrase précédente...)

Lors des funérailles de Kian, Zeynab a chanté une comptine sur sa tombe
et a changé les paroles en insultes contre le régime tueur d'enfants.

La police est intervenue et lui a pris le micro.

Zeynab a juste chanté plus fort.

Des mois plus tard, après avoir traversé la douleur atroce de perdre son
enfant,  d'avoir dû annoncer la nouvelle à son père, puis de l'avoir
ramené de l'hôpital avec une colonne vertébrale brisée et un cœur plus
brisé encore, et malgré les menaces constantes du régime, Zeynab,
réclamait encore justice sur Instagram.

Après avoir été licenciée de son poste d'enseignante et convoquée au
tribunal (tout comme son mari toujours alité), Zeynab publiait toujours
des photos, des vidéos et des poèmes, et réclamait justice.

A ce jour, elle ne s'est toujours pas tue.

Si tu es un homme, viens et sois Zeynab.

Viens et sois toutes les mères iraniennes qui refusent de se taire, après
avoir tant perdu, et alors même qu'elles savent qu'elles ont encore
tant à perdre.

Si tu es un homme,

Viens en Iran et sois une femme !




