.. index::
   pair: Akadem ; Conférence Les femmes en Iran Les femmes iraniennes aux avant-postes d'une révolution laïque et démocratique
   pair: Medem ; Conférence Les femmes en Iran Les femmes iraniennes aux avant-postes d'une révolution laïque et démocratique

.. _conference_iran_2023_03_09:

============================================================================================================================
2023-03-04 **Conférence Les femmes en Iran Les femmes iraniennes aux avant-postes d'une révolution laïque et démocratique**
============================================================================================================================

- https://www.centre-medem.org/EVT/les-femmes-en-iran-avec-mahnaz-shirali-annie-sugier-arlette-zilberg/
- https://akademimg.akadem.org/Medias/Programmes/20230225233619_FEMIRA.pdf
- https://www.centre-medem.org/agenda-mars-avril-2023/

Paris le 9 mars 2023, 20:00
Conférence
Les femmes en Iran
Les femmes iraniennes aux avant-postes d'une révolution laïque et démocratique


Annonce
==========

.. figure:: images/annonce_akadem.png
   :align: center

Le 16 septembre 2022, la mort à Téhéran de Masha Amini, 22 ans, jeune femme
kurde tuée par la police des mœurs pour un voile mal ajusté, a provoqué un
soulèvement des femmes puis de la jeunesse iranienne.


Arlette Zilberg, militante féministe impliquée dans le soutien en France
à la révolution en Iran, recevra Mahnaz Shirali, sociologue/politiste,
spécialiste de l’Iran, pour nous éclairer sur la révolte de la jeunesse
iranienne contre le voile et le régime des mollahs, et Annie Sugier,
présidente de la Ligue du Droit International des Femmes qui a fait émerger
la notion **d’apartheid sexuel d’État**.


.. figure:: images/intervenantes.png
   :align: center


Informations pratiques
============================

::

    Lieu: Centre Medem, 1er étage
    Adresse: 52 rue René Boulanger 75010 PARIS France
    Métro République
    Tarifs: Tarif adhérent: 5€ /Tarif non-adhérent: 8€


    Centre Medem Arbeter Ring
    52 rue René Boulanger
    75010 Paris. 01 42 02 17 08

    Contact : centre.medem@gmail.com



.. figure:: images/centre_medem_mars_avril.png
   :align: center
