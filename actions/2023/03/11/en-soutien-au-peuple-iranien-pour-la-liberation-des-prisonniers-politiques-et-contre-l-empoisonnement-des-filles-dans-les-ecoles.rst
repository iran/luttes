
.. index::
   pair: Rassemblement ; Samedi 11 mars 2023


.. _iran_grenoble_2023_03_11:

===================================================================================================================================================================================================================================================================================================================
♀️✊ ⚖️ **Femme, Vie, Liberté** 25e rassemblement samedi 11 mars 2023 à Grenoble **place Félix Poulat** à 14h30 **Arrêt des exécutions, annulation des condamnations à mort libération des prisonniers politiques, expulsion de l'ambassadeur d'iran, arrêt de l'empoisonnement des filles dans les écoles** 📣
===================================================================================================================================================================================================================================================================================================================

Arrêt des exécutions, annulation des condamnations à mort liberation des prisonniers politiques, expulsion de l'ambassadeur d'iran
176e jour depuis la mort de #JinaMahsaAmini (2022-09-16)

- :ref:`mahsa_jina_amini`
- :ref:`slogans`

#Grenoble #IranRevolution #SaveOurIranianSchoolGirls #MahsaAmini #MahsaJinaAmini #SaveOurIranianSchoolGirls


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|

.. figure:: images/annonce.png
   :align: center

   https://www.instagram.com/p/CpidO2luWWh/?igshid=YmMyMTA2M2Y%3D


.. figure:: images/filles_avec_masques_a_gaz.png
   :align: center


.. figure: images/le_regime_des_mollahs_tue.png
   :align: center

.. figure: images/filles_empoisonnees.png
   :align: center


Texte annonçant le rassemblement du samedi 11 mars 2023
==============================================================

Pour le 25e samedi depuis l'assassinat de Jina Mahsa Amini, nous nous
rassemblerons sur la place Félix Poulat de Grenoble pour être la voix du
peuple iranien dans son combat contre le régime meurtrier du gouvernement
islamique d'Iran.

Il y a une révolution en cours en Iran.

Le mardi 13 septembre 2022, Jina Mahsa Amini était arrêtée par la police
des mœurs à Téhéran, car elle n’était pas "assez voilée".

Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.
Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

Nous demandons des gouvernements des pays occidentaux :

- d'arrêter tout contrat et de négociation avec le gouvernement islamique d'Iran,
- de mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort,
- d'expulser les ambassadeurs et les représentants du gouvernement islamique
- de mettre sur la liste noire les acteurs des massacres et de confisquer
  leurs bien à l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyez leurs voix.

Les Iraniens ont besoin d'un soutien réel et d'actions réelles pour en finir avec ce régime
sanguinaire ; le blabla ne suffit pas.

Nous irons jusqu’à la liberté vraie, humaine, cette fois-ci jusqu’au bout,
jusqu'au dernier souffle.




Revendications
===============================================================

- :ref:`revendications_mediapart`

.. include:: ../../../../revendications/revendications_2023_03_08.txt


Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`soutiens_avocats_2023_01_10`

Les victimes de la barbarie des mollahs
--------------------------------------------

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|

Les otages français
=======================

.. include:: ../../../../repression/otages/liste_2023_02_10.txt


Quelques événements du 6 mars 2023 au 12 mars 2023
=============================================================================

- :ref:`semaine_10_iran_2023`


Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/

.. figure:: images/annonce_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/rassemblement-de-soutien-au-peuple-iranien-pour-la-liberation-des-prisonniers-politiques-et-contre-lempoisonnement-des-filles-dans-les-ecoles



Mobilizon
------------

- https://mobilizon.chapril.org/@iranluttes38


Videos
========

- https://www.orion-hub.fr/w/s2VDhNAeTX9xymQ8jk7EC9 (Discours de Zoya)
- https://iran.frama.io/luttes/revendications/revendications.html#charte-des-revendications-progressistes-des-femmes-iraniennes-du-8-mars-2023

🇮🇷 ♀️ Charte des revendications progressistes des femmes iraniennes du 8 mars 2023
-------------------------------------------------------------------------------------


- Liberté de se couvrir et abolition du hijab obligatoire
- Abolition complète de la ségrégation sexuelle à tous les niveaux de la société
- Séparation de la religion du gouvernement, du système judiciaire et de l'éducation
- Liberté de pensée, de croyance et d'expression
- Abolition de toute tutelle sur les femmes, reconnaissance des femmes
  en tant qu'êtres humains avec une identité indépendante et libres de
  prendre des décisions dans leur vie personnelle et sociale sans l'implication
  d'aucune sorte de personne physique ou morale
- Égalité inconditionnelle des hommes et des femmes dans l'usage de tous
  les lieux publics, y compris les stades sportifs
- Assurer la sécurité sexuelle, physique et mentale des femmes à toutes
  les heures de la journée et dans tous les lieux de la société
- Interdiction du test de virginité
- Avoir le droit de voyager et de quitter le pays
- Égalité inconditionnelle des hommes et des femmes sur le plan juridique
  et des activités sociales dans les domaines éducatif, sportif, politique,
  économique et judiciaire
- L'égalité dans toutes les lois du travail et l'assurance sociale ainsi
  que l'égalité de rémunération pour un travail similaire avec des
  considérations juridiques pendant la grossesse et l'accouchement
- Une assurance chômage, maladie et retraite suffisante
- Abolition générale de la loi du concubinage
- Égalité complète dans les lois sur la famille, y compris l'égalité dans
  la tutelle et la gestion de toutes les questions liées aux travaux ménagers,
  aux enfants, aux biens et autres questions qui sont conjointement liées au couple
- Accès des femmes aux contraceptifs gratuits et accès gratuit aux tests
  de dépistage de grossesse
- Le droit à un avortement sécurisé
- Avoir des droits égaux pour le divorce et la garde des enfants en cas de séparation
- Criminalisation et sanction de tout type de violence et d'abus physique,
  mental et verbal, de tout type d'humiliation et de restriction à l'égard
  des femmes dans tous les domaines et pour toute raison religieuse,
  traditionnelle, patriarcale et honorable
- Interdiction du hijab pour les enfants
- Une éducation gratuite et égale pour tous les enfants vivant dans le
  pays sans exception
- Interdiction du mariage des enfants et du mariage des enfants de moins
  de 18 ans, interdiction de toute vente et transaction d'enfants et de femmes
- Reconnaître un troisième sexe et garantir les droits et la sécurité de
  la citoyenneté aux personnes trans, et abolir toute obligation légale de changer de sexe

Femme, vie, liberté

- :ref:`charte_2023_03_08`

Photos
=================

Voir :ref:`media_2023:images_iran_grenoble_2023_03_11`

.. figure:: images/kave.png
   :align: center


.. _video_2023_03_11:

Autres événements ce samedi 11 mars 2023 à Grenoble
==========================================================


.. figure:: images/ici_grenoble_2023_03_11.png
   :align: center

   https://www.ici-grenoble.org/agenda


Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2023_03_04`
- :ref:`iran_grenoble_2023_02_25`
- :ref:`iran_grenoble_2023_02_18`
- :ref:`iran_grenoble_2023_02_11`
- :ref:`iran_grenoble_2023_02_04`
- :ref:`iran_grenoble_2023_01_28`
- :ref:`iran_grenoble_2023_01_21`
- :ref:`iran_grenoble_2023_01_14`
- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


