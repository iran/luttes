.. index::
   pair: Conférence; La place des femmes dans les mouvements revolutionnaires Algerie Tunisie Iran


.. _conference_2023_03_17:

===============================================================================================================================
Vendredi 17 mars 2023 **La place des femmes dans les mouvements revolutionnaires Algerie Tunisie Iran** par Wassyla Tamzali
===============================================================================================================================

- :ref:`iran_exposition:conference_2023_03_17`
