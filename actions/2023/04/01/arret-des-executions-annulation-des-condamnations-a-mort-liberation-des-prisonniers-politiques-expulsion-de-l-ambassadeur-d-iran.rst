
.. index::
   pair: Rassemblement ; Samedi 1er avril 2023
   pair: Soutien ; Myriam Laïdouni-Denis (2023-04-01)


.. _iran_grenoble_2023_04_01:

================================================================================================================================================================================================================================================================
♀️✊ ⚖️ 📣 **Femme, Vie, Liberté** 27e rassemblement samedi 1er avril 2023 à Grenoble **place Félix Poulat** à 14h30 **Arrêt des exécutions, annulation des condamnations à mort libération des prisonniers politiques, expulsion de l'ambassadeur d'iran** 📣
================================================================================================================================================================================================================================================================

Arrêt des exécutions, annulation des condamnations à mort liberation des prisonniers politiques, expulsion de l'ambassadeur d'iran
197e jour depuis la mort de #JinaMahsaAmini (2022-09-16)

#ReyhanehJabbari

- :ref:`mahsa_jina_amini`
- :ref:`slogans`


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|

.. figure:: images/annonce.png
   :align: center


Autres photos
===============

.. figure:: images/sept_hivers_a_teheran.png
   :align: center
   :width: 500

   :ref:`sept_hivers_a_teheran`


.. figure:: images/mollah_ecrase.png
   :align: center
   :width: 500

.. figure:: images/jeunes_femmes_sans_voile.png
   :align: center
   :width: 500

.. figure:: images/femme_non_voilee.png
   :align: center
   :width: 500

   https://www.instagram.com/ahmad.halabisaz/



Texte annonçant le rassemblement du samedi 1er avril 2023, Soyons leurs voix 📣
=====================================================================================

Ce samedi 1er avril 2023, nous nous rassemblerons sur la place Félix Poulat
de Grenoble pour être la voix 📣 du peuple iranien dans son combat contre
le régime meurtrier du gouvernement islamique d'Iran.

Le mardi 13 septembre 2022, Jina Mahsa Amini était arrêtée par la police
des mœurs à Téhéran, car elle n’était pas "assez voilée".

Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

Nous demandons des gouvernements des pays occidentaux :

- d'arrêter tout contrat et de négociation avec le gouvernement islamique d'Iran,
- de mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort,
- d'expulser les ambassadeurs et les représentants du gouvernement islamique
- de mettre sur la liste noire les acteurs des massacres et de confisquer
  leurs bien à l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyons leurs voix 📣

Toute notre solidarité |solidarite| avec les luttes sociales en France !


Revendications pour le peuple Iranien et motions de France
===============================================================

- :ref:`revendications_mediapart`

.. include:: ../../../../revendications/revendications_2023_03_07.txt


Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`soutiens_avocats_2023_01_10`

Les victimes de la barbarie des mollahs
--------------------------------------------

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|

Les otages français
=======================

.. include:: ../../../../repression/otages/liste_2023_02_10.txt


Quelques événements du lundi 27 mars 2023 au dimanche 2 avril 2023
=============================================================================

- :ref:`semaine_13_iran_2023`


Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/

.. figure:: images/annonce_ici_grenoble.png
   :align: center


Quelques images du rassemblement du 1er avril 2023
=======================================================

- :ref:`media_2023:images_2023_04_01`


.. _videos_2023_04_01:

Videos
=======

Introduction zoya (samedi 1er avril 2023)
----------------------------------------------

- https://www.orion-hub.fr/w/mkjP5V2E7Ubw7dexFLy7mV


Soutien de militants EELV
-------------------------------

- https://www.orion-hub.fr/w/31txvUTNGfdnuGfaX5cYoi


Merci à Myriam Laïdouni-Denis, élue EELV de la région Rhône-Alpes

.. 06 75 67 95 63

Femme Vie Liberté
---------------------

- https://www.orion-hub.fr/w/jK7zxZG5wAn4R6FtVdXvB9


Discours de Sarah Salemi (retransmis)
-------------------------------------------

- https://www.orion-hub.fr/w/hyHjf3FL8KFXUW6ZZvgcoz (partie 1)

- https://www.orion-hub.fr/w/eZz6tGSKtS3Ldn5MxZ8JS6 (partie 2)



Sept hivers à Téhéran
=========================

- https://www.orion-hub.fr/w/m9XMLtM3KjwkrcUpJ2nGuJ
- :ref:`sept_hivers_a_teheran`



Autres événements ce samedi 1er avril 2023 à Grenoble
==========================================================

- :ref:`grenoble_retraites:actions_2023_04_01`

.. figure: images/ici_grenoble_2023_03_04.png
   :align: center

   https://www.ici-grenoble.org/agenda

- https://www.ici-grenoble.org/evenement/portes-ouvertes-de-l-ouvre-aux-1000-jardins
- https://www.ici-grenoble.org/evenement/atelier-clown-e-s-en-mixite-choisie-1
- https://www.ici-grenoble.org/evenement/depart-collectif-a-velo-pour-la-manif-de-leau-pas-des-puces
- https://www.ici-grenoble.org/evenement/distribution-gratuite-de-drones-be-gone-campus
- https://www.ici-grenoble.org/evenement/manifestation-de-leau-pas-des-puces-contre-le-gaspillage-de-leau-par-les-industriels-high-tech
- https://www.ici-grenoble.org/evenement/atelier-comment-fabriquer-une-tartapulte-en-non-mixite
- https://www.ici-grenoble.org/evenement/conference-l-utilisation-des-cacatovs-a-travers-les-ages
- https://www.ici-grenoble.org/evenement/fin-de-la-treve-hivernale-rassemblement-contre-les-expulsions-et-les-coupures-denergie
- https://www.ici-grenoble.org/evenement/cine-debat-isere-2053-sur-la-plus-grande-cooperative-danticipation-des-crises-climatiques-pres-de-grenoble
- https://www.ici-grenoble.org/structure/la-cooperative-isere-2053
- https://www.ici-grenoble.org/evenement/cine-repas-concert-hommage-aux-femmes-battantes
- https://www.ici-grenoble.org/evenement/grande-soiree-sosies-deric-piolle-concerts-jeux-pierrade-vegan
- https://www.ici-grenoble.org/evenement/rassemblement-pour-larret-des-executions-en-iran-et-la-liberation-des-prisonniers-politiques


Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2023_03_25`
- :ref:`iran_grenoble_2023_03_11`
- :ref:`iran_grenoble_2023_03_04`
- :ref:`iran_grenoble_2023_02_25`
- :ref:`iran_grenoble_2023_02_18`
- :ref:`iran_grenoble_2023_02_11`
- :ref:`iran_grenoble_2023_02_04`
- :ref:`iran_grenoble_2023_01_28`
- :ref:`iran_grenoble_2023_01_21`
- :ref:`iran_grenoble_2023_01_14`
- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


