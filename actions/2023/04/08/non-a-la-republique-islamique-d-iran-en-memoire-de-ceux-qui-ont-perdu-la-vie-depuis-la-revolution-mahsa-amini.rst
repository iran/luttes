
.. index::
   pair: Rassemblement ; Samedi 1er avril 2023
   pair: Soutien ; Myriam Laïdouni-Denis (2023-04-01)


.. _iran_grenoble_2023_04_08:

==============================================================================================================================================================================================================================================================
♀️✊ ⚖️ 📣 **Femme, Vie, Liberté** 28e rassemblement samedi 8 avril 2023 à Grenoble **place Félix Poulat** à 14h30 **Non à la république islamique d'Iran, en mémoire de ceux qui ont perdu la vie depuis la révolution Mahasa Jina Amini** 📣
==============================================================================================================================================================================================================================================================

Arrêt des exécutions, annulation des condamnations à mort liberation des prisonniers politiques, expulsion de l'ambassadeur d'iran
204e jour depuis la mort de #JinaMahsaAmini (2022-09-16)

#Grenoble #IranRevolution  #MahsaAmini #MahsaJinaAmini
#ReyhanehJabbari

- :ref:`mahsa_jina_amini`
- :ref:`slogans`


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|

.. figure:: images/annonce.png
   :align: center


Autres photos
===============

.. figure:: ../13/images/femme_vie_liberte_2023_04_13.png
   :align: center
   :width: 500

   https://www.imarabe.org/fr/evenement-exceptionnel/femme-vie-liberte

.. figure:: ../13/images/soiree_des_100_voix.png
   :align: center
   :width: 500

   https://www.avocatparis.org/agenda-des-formations/soiree-des-cent-voix-pour-liran


Texte annonçant le rassemblement du samedi 1er avril 2023, Soyons leurs voix 📣
=====================================================================================

Ce samedi 8 avril 2023, nous nous rassemblerons sur la place Félix Poulat
de Grenoble pour être la voix 📣 du peuple iranien dans son combat contre
le régime meurtrier du gouvernement islamique d'Iran.

Le mardi 13 septembre 2022, Jina Mahsa Amini était arrêtée par la police
des mœurs à Téhéran, car elle n’était pas "assez voilée".

Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.

Nous demandons des gouvernements des pays occidentaux :

- d'arrêter tout contrat et de négociation avec le gouvernement islamique d'Iran,
- de mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort,
- d'expulser les ambassadeurs et les représentants du gouvernement islamique
- de mettre sur la liste noire les acteurs des massacres et de confisquer
  leurs bien à l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyons leurs voix 📣

Toute notre solidarité |solidarite| avec les luttes sociales en France !


Revendications pour le peuple Iranien et motions de France
===============================================================

- :ref:`revendications_mediapart`

.. include:: ../../../../revendications/revendications_2023_03_07.txt


Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`soutiens_avocats_2023_01_10`

Les victimes de la barbarie des mollahs
--------------------------------------------

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|

Les otages français
=======================

.. include:: ../../../../repression/otages/liste_2023_02_10.txt


Quelques événements du lundi 3 avril 2023 au dimanche 9 avril 2023
=============================================================================

- :ref:`semaine_14_iran_2023`


Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/

.. figure: images/annonce_ici_grenoble.png
   :align: center



Sept hivers à Téhéran
=========================

- https://www.orion-hub.fr/w/m9XMLtM3KjwkrcUpJ2nGuJ
- :ref:`sept_hivers_a_teheran`


Autres événements ce samedi 8 avril 2023 à Grenoble
==========================================================

.. figure: images/ici_grenoble_2023_04_08.png
   :align: center


- https://www.ici-grenoble.org/evenement/concerts-le-developpement-du-rap
- https://www.ici-grenoble.org/evenement/manifestation-de-soutien-au-peuple-ukrainien
- https://www.ici-grenoble.org/evenement/discussion-le-sens-politique-du-travail
- https://www.ici-grenoble.org/evenement/rencontre-avec-les-collectifs-ancrage-et-la-fonciere-antidote-comment-perenniser-des-lieux-autogeres
- https://www.ici-grenoble.org/evenement/repas-vegan-et-concert-de-soutien-a-kontrosol-centre-social-autogere-lgbtqi-en-grece
- https://www.ici-grenoble.org/evenement/soiree-rap-hip-hop-ferme-de-loutas
- https://www.ici-grenoble.org/evenement/replay-choc-manifs-la-guerre-est-declaree-sur-les-violences-policieres-a-sainte-soline
- https://www.francetvinfo.fr/replay-magazine/france-2/complement-d-enquete/complement-d-enquete-du-jeudi-6-avril-2023-manifs-la-guerre-est-declaree_5745668.html



.. _videos_2023_04_08:

📣 Soyons leurs voix videos du samedi 8 avril 2023  #Grenoble, #SoyonsLeursVoix #FemmeVieLiberte #MahsaAmini 📣
=====================================================================================================================

Introduction zoya (samedi 8 avril 2023)
----------------------------------------------

- https://www.orion-hub.fr/w/fPgMotMtBwF6VTBmeUtntW

.. video_viens_en_iran_si_tes_un_homme:

Viens en Iran si t'es un homme
------------------------------------

- :ref:`viens_en_iran_2023_03_08`

- https://www.orion-hub.fr/w/roAJb1wti4ZLMivLUh7bD1 (Pirouz Viens en Iran si tes un homme (partie 1 2023-04-08))
- https://www.orion-hub.fr/w/kJuBA7C1tGBhJSmhz3n445 (Pirouz Viens en Iran si tes un homme (partie 2 2023-04-08))
- https://www.orion-hub.fr/w/c95abNc2rZntL92uX2y6CX (Pirouz Viens en Iran si tes un homme (partie 3 2023-04-08))

Témoignage
----------------

- :ref:`greve_faim_2023_01_02`

- https://www.orion-hub.fr/w/mCJwz1z4kZLszDyHruMNY4 (Zoya temoignage (2023-04-08, partie 1))
- https://www.orion-hub.fr/w/rhbaqKNHGqgnkKW9LiVZpN (Zoya temoignage (2023-04-08, partie 2))

Quelques images du rassemblement du 8 avril 2023
=======================================================

- :ref:`media_2023:images_2023_04_08`



Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2023_04_01`
- :ref:`iran_grenoble_2023_03_25`
- :ref:`iran_grenoble_2023_03_11`
- :ref:`iran_grenoble_2023_03_04`
- :ref:`iran_grenoble_2023_02_25`
- :ref:`iran_grenoble_2023_02_18`
- :ref:`iran_grenoble_2023_02_11`
- :ref:`iran_grenoble_2023_02_04`
- :ref:`iran_grenoble_2023_01_28`
- :ref:`iran_grenoble_2023_01_21`
- :ref:`iran_grenoble_2023_01_14`
- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


