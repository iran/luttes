.. index::
   pair: Marjan Yazdani ; Exposition
   pair: Marjan Yazdani ; 2023-04-11


.. _marjan_2023_04_11:

=======================================================================================================================================================================================
Exposition **La nature en moi** de Marjan Yazdani du mardi 11 avril au dimanche 30 avril  2023 à la MJC Abbaye, 1 place de la commune de 1871 Grenoble
=======================================================================================================================================================================================

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
   src="https://www.openstreetmap.org/export/embed.html?bbox=5.743459761142732%2C45.17828770766482%2C5.74700027704239%2C45.179838058360794&amp;layer=mapnik"
   style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.17906/5.74523">Afficher une carte plus grande</a></small>


.. figure:: images/exposition_marjane.png
   :align: center


.. figure:: images/exposition_marjane_greve_de_la_faim.png
   :align: center


Description
===========

La nature en moi** de Marjan Yazdani du mardi 11 avril au dimanche 30 avril
2023 à la MJC Abbaye, 1 place de la commune de 1871 Grenoble

Vernissage
=============

Le mardi 11 avril 2023 de 18h à 20h.

