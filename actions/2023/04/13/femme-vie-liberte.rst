.. index::
   pair: Femme Vie Liberté ; 2023-04-13

.. _femme_vie_liberte_2023_04_13:

======================================================================================
2023-04-13 **Femme Vie Liberté** à l'IMA en soutien à la lutte du peuple iranien
======================================================================================

- https://www.imarabe.org/fr/evenement-exceptionnel/femme-vie-liberte


.. figure:: images/femme_vie_liberte_2023_04_13.png
   :align: center

Description
============

Depuis le 8 mars 2023, journée internationale des droits des femmes,
les 3 mots « Femme, Vie, Liberté », symboles de la lutte du peuple iranien,
sont projetés plusieurs fois par semaine sur les moucharabiehs de la
façade Sud de l’Institut du monde arabe.

Le 13 avril 2023, afin de poursuivre son soutien, l’IMA réunit deux tables
rondes exceptionnelles : « Comprendre la révolution de 1979 »
et « Femme, vie liberté : où va le mouvement de contestation en Iran »?

En seconde partie de soirée : un programme de concerts, projections et
lectures réunira notamment Barbara Pravi, Anousha Nazari, Julie Gayet,
Darya Dadvar, Ariana Vafadari, Sepideh Farsi, Yasaman Hasani, Claire Laffut,
Haylen, Syrus Shahidi...


En septembre 2022, l’exécution en détention de Mahsa Amini, arrêtée par
la police des mœurs pour ne pas avoir porté son hijab selon les règles
vestimentaires en vigueur, marque en Iran le début d’un soulèvement populaire
sans précédent.

Partout dans le pays, des manifestations éclatent pour dénoncer le pouvoir
liberticide de la République islamique qui depuis plus de quarante ans
punit d’emprisonnement toute femme sortant en public sans être voilée.

Plusieurs mois après le début du mouvement, la mobilisation contre le
régime des mollahs ne faiblit pas.

Femmes et hommes défient avec un courage inouï le pouvoir en place.

Des milliers d’arrestations et des centaines de condamnations à mort
témoignent de l’ampleur de la répression.

Programmation élaborée en association avec l’association Femme Azadi,
avec le soutien de United Artists


