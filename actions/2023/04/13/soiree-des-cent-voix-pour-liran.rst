.. index::
   Soirée des 100 voix

.. _cent_voix_2023_04_13:

=====================================================================================================================================
2023-04-13 **Soirée des Cent Voix pour l'Iran** de 18h30 à 21:00 à l'auditorium de la Maison du Barreau, 2 rue de Harlay 75001 Paris
=====================================================================================================================================

- https://www.avocatparis.org/agenda-des-formations/soiree-des-cent-voix-pour-liran

.. figure:: images/soiree_des_100_voix.png
   :align: center

   https://www.avocatparis.org/agenda-des-formations/soiree-des-cent-voix-pour-liran


.. figure:: images/soiree_des_100_voix_prevision.png
   :align: center

   https://www.avocatparis.org/agenda-des-formations/soiree-des-cent-voix-pour-liran


Description
============

Une rencontre qui donne à entendre :

- Les cent voix, celles des avocates et avocats du barreau de Paris
- Les sans voix, celles et ceux en Iran dont nous devons relayer la voix

Collaboration entre le Barreau de Paris, Ensemble contre la peine de mort,
Reporters sans frontières et le Collectif Azadi4Iran.


Programme prévisionnel
================================

Soirée animée par Bertrand Périer, avocat au Conseil d'Etat et à la Cour de cassation .

18h30 – Allocution d’ouverture de Mme la bâtonnière, Julie Couturier.
1re séquence : Etat des lieux de la situation en Iran

Modération : Hirbod Dehghani-Azar, avocat au barreau de Paris et membre des collectifs Iran Justice et Azadi4Iran Paris

Javaid Rehman, Rapporteur spécial sur la situation des droits de l’homme en République islamique d’Iran : panorama de la situation en Iran

Mahmood Amiry-Moghaddam, directeur et porte-parole de l’association Iran Human Rights : la peine de mort en Iran en 2022, les condamnations à mort et les exécutions des manifestants

Richard Sedillot, porte-parole de l’association Ensemble contre la peine de mort : spécificités de l’utilisation de la peine de mort en Iran

Témoignage de Madame Samira Mokarami, ancienne condamnée à mort en Iran à l’âge de 15 ans

Professeur Nozar Aghakhani, neurochirurgien - Hôpital Bicêtre : attaques biologiques et répression lors des manifestations

La force de la liberté d’expression :

- Henri Leclerc, avocat honoraire, Président d'honneur de la Ligue des droits de l'Homme (LDH)
- Jonathan Dagher, responsable du bureau RSF Moyen-Orient
- Sophia Aram, journaliste radio et télévision

Pause et diffusion de vidéo des artistes engagés

20h - 2e séquence : Plaidoiries et célébration de la culture iranienne
===============================================================================

Introduction de Bertrand Périer et ouverture des salles d’enregistrement pour les Cent voix, sous la direction  d'Anne-Sophie Laguens

Danse traditionnelle persane avec une danseuse et deux musiciens en live, Sahar Dehghan

Plaidoiries 1 : Les femmes iraniennes - Safya Akorri, avocate au barreau de Paris (diffusion des dessins de Baboo)

Chant « Bella Ciao » en farsi - chanteuse : Leïla Saber, avocate au barreau de Paris

Plaidoiries 2 : La peine de mort - Marie Dosé, avocate au barreau de Paris (diffusion des dessins de Baboo)

Danse - Interprétation contemporaine et engagée du «Derviche tourneur» avec deux musiciens en live, Sahar Dehghan

Plaidoiries 3 : Les avocats en danger - Maryam Meylan, avocate au barreau de Genève (diffusion des dessins de Baboo)

Orchestre du Palais

Mot de conclusion par Vincent Nioré, vice-bâtonnier du barreau de Paris
3e Séquence : Partage d’un buffet iranien et poursuite des enregistrements des Voix

