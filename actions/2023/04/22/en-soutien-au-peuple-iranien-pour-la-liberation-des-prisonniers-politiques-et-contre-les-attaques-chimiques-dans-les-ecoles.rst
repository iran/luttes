
.. index::
   pair: Rassemblement ; Samedi 22 avril 2023


.. _iran_grenoble_2023_04_22:

=====================================================================================================================================================================================================================================================================================================================
♀️✊ ⚖️ 📣 **Femme, Vie, Liberté** 29e rassemblement samedi 22 avril 2023 à Grenoble **place Félix Poulat** à 14h30 **Arrêt des exécutions, annulation des condamnations à mort libération des prisonniers politiques, expulsion de l'ambassadeur d'iran, contre les attaques chimiques dans les écoles** 📣
=====================================================================================================================================================================================================================================================================================================================

Arrêt des exécutions, annulation des condamnations à mort liberation des prisonniers politiques, expulsion de l'ambassadeur d'iran
218e jour depuis la mort de #JinaMahsaAmini (2022-09-16)

- :ref:`mahsa_jina_amini`
- :ref:`slogans`

#Grenoble #IranRevolution #KaroPashabadi #ChemicalAttacksOnSchools #SaveOurIranianSchoolGirls #MahsaAmini #MahsaJinaAmini


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|

.. figure:: images/annonce.png
   :align: center

   https://www.openstreetmap.org/#map=19/45.18979/5.72682


.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.725057125091554%2C45.18901829435873%2C5.728592276573182%2C45.19056457227583&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/>
   <small><a href="https://www.openstreetmap.org/#map=19/45.18979/5.72682">Afficher une carte plus grande</a></small>


.. figure:: images/lundi_10_avril_2023_karo_pashbadi.png
   :align: center

   :ref:`akrami_iran:KaroPashbadi_2023_04_10`  #KaroPashabadi



Texte annonçant le rassemblement du samedi 22 avril 2023
==============================================================

Pour la 29e fois depuis l'assassinat de Jina Mahsa Amini, nous nous
rassemblerons sur la place Félix Poulat de Grenoble pour être la voix du
peuple iranien dans son combat contre le régime meurtrier du gouvernement
islamique d'Iran.

Le mardi 13 septembre 2022, Jina Mahsa Amini était arrêtée par la police
des mœurs à Téhéran, car elle n’était pas "assez voilée".

Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.
Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

Nous demandons aux gouvernements des pays occidentaux :

- d'arrêter tout contrat et de négociation avec le gouvernement islamique d'Iran,
- de mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort,
- d'expulser les ambassadeurs et les représentants du gouvernement islamique
- de mettre sur la liste noire les acteurs des massacres et de confisquer
  leurs bien à l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Soyons leurs voix 📣


Revendications
===============================================================

- :ref:`revendications_mediapart`

.. include:: ../../../../revendications/revendications_2023_03_08.txt


Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`soutiens_avocats_2023_01_10`

Les victimes de la barbarie des mollahs
--------------------------------------------

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|

Les otages français
=======================

.. include:: ../../../../repression/otages/liste_2023_02_10.txt


Quelques événements du 17 avril 2023 au 24 avril 2023
=============================================================================

- :ref:`semaine_15_iran_2023`


Annonces sur internet
=======================================================


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/
- https://www.ici-grenoble.org/agenda

.. figure:: images/annonce_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/rassemblement-pour-larret-des-executions-en-iran-et-contre-les-attaques-chimiques-dans-les-ecoles




Autres événements ce samedi 22 avril 2023 à Grenoble
==========================================================


.. figure:: images/ici_grenoble_2023_04_22.png
   :align: center

   https://www.ici-grenoble.org/agenda


- https://www.ici-grenoble.org/evenement/atelier-patriarchie-initiation-au-bootyshake-expose-sur-lhypersexualisation-des-fesses-des-femmes-noires
- https://www.ici-grenoble.org/evenement/formation-comprendre-et-minimiser-la-surveillance-numerique-tout-niveau
- https://www.ici-grenoble.org/evenement/rassemblement-pour-larret-des-executions-en-iran-et-contre-les-attaques-chimiques-dans-les-ecoles
- https://www.ici-grenoble.org/evenement/manifestation-de-soutien-au-peuple-ukrainien
- https://www.ici-grenoble.org/evenement/grande-velorution-de-grenoble-venez-deguise-e-s
- https://www.ici-grenoble.org/evenement/rencontres-la-gratuite-des-transports-en-communs-a-grenoble-pourquoi-et-comment
- https://www.ici-grenoble.org/evenement/90-8-fm-nuit-du-mix-sets-dj-en-non-mixite-choisie-sans-mecs-cis
- https://www.ici-grenoble.org/evenement/repas-vegan-et-concert-punk-a-la-baf
- https://www.ici-grenoble.org/evenement/coup-de-coeur-les-coulisses-de-la-revue-la-hulotte



Sept hivers à Téhéran au cinéma Le Méliès
--------------------------------------------

- :ref:`sept_hivers_a_teheran`
- https://www.cinemalemelies.org/
- https://www.openstreetmap.org/#map=19/45.18281/5.72427
- https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran

::

    Le cinéma Le Méliès
    28, allée Henri Frenay
    38 000 Grenoble

    https://www.cinemalemelies.org/



.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.722503662109376%2C45.182036837015886%2C5.726038813591004%2C45.183583304584374&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/>
   <small><a href="https://www.openstreetmap.org/#map=19/45.18281/5.72427">Afficher une carte plus grande</a></small>


Quelques photos
===================


.. figure:: images/photo_1.png
   :align: center

.. figure:: images/photo_2.png
   :align: center




Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2023_04_08`
- :ref:`iran_grenoble_2023_04_01`
- :ref:`iran_grenoble_2023_03_25`
- :ref:`iran_grenoble_2023_03_11`
- :ref:`iran_grenoble_2023_03_04`
- :ref:`iran_grenoble_2023_02_25`
- :ref:`iran_grenoble_2023_02_18`
- :ref:`iran_grenoble_2023_02_11`
- :ref:`iran_grenoble_2023_02_04`
- :ref:`iran_grenoble_2023_01_28`
- :ref:`iran_grenoble_2023_01_21`
- :ref:`iran_grenoble_2023_01_14`
- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


