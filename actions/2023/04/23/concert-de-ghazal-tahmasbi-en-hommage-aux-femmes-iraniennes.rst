.. index::
   pair: concert; Ghazal Tahmasbi

.. _concert_2023_04_23:

==============================================================================================================
2023-04-23  **Concert de Ghazal Tahmasbi - En hommage aux femmes iraniennes** au Musée Dauphinois à 18h
==============================================================================================================


- https://my.weezevent.com/concert-de-ghazal-tahmasbi-en-hommage-aux-femmes-iraniennes


:Chanteuse: Ghazal Tahmasbi
:Piano: Shayan Karimi
:Violon: Lucie Cointet

Chappelle Sainte-Marie d'en Haut, Musée Dauphinois,
30, rue Maurice Gignoux, Grenoble
Prix des places 8 et 16 euros,


.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.724858641624452%2C45.19438471939678%2C5.72839915752411%2C45.19593463174191&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/>
   <small>
    <a href="https://www.openstreetmap.org/#map=19/45.19516/5.72663">Afficher une carte plus grande</a>
   </small>



.. figure:: images/concert_ghazal_tahmasbi.png
   :align: center
