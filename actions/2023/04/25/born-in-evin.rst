
.. _born_in_evin_2023_04_25:

====================================================================================================================================
2023-04-25 Projection du film **Born in Evin** salle "Au clair du quartier" 1 bis rue des Champs-Élysées, GRENOBLE à 19h30
====================================================================================================================================

- https://lefilrouge.org/actualite/soiree-cine-debat-retrouvons-nous/
- https://www.on-tenk.com/fr/documentaires/politique/born-in-evin
- https://www.ici-grenoble.org/evenement/cine-debat-et-buffet-iranien-born-in-evin-une-enquete-sur-la-pire-prison-politique-diran
- :ref:`born_in_evin`


.. figure:: images/affiche_fil_rouge.png
   :align: center
   :width: 700


.. figure:: images/maryam_zaree.png
   :align: center
   :width: 400


   Maryam Zaree

Description
===============

- https://lefilrouge.org/actualite/soiree-cine-debat-retrouvons-nous/
- https://www.openstreetmap.org/#map=17/45.17851/5.71093

L'association le Fil Rouge via son groupe "ciné-débat" vous invite à
découvrir: "BORN IN EVIN"


Ouverture des portes 19H30 - Début du film 20H
Salle « Au clair du quartier »
1 bis rue des Champs-Élysées, GRENOBLE


Entrée prix libre (pas de CB), jauge limitée, réservation conseillée par e-mail : seyer.patrick38@orange.fr


.. figure:: images/lieu_diffusion.png
   :align: center

   Salle « Au clair du quartier » 1 bis rue des Champs-Élysées, GRENOBLE, https://www.openstreetmap.org/#map=17/45.17851/5.71093


.. figure:: images/batiment.png
   :align: center
   :width: 400

   Salle « Au clair du quartier » 1 bis rue des Champs-Élysées, GRENOBLE, https://www.openstreetmap.org/#map=17/45.17851/5.71093



"BORN IN EVIN" est un film documentaire de Maryam Zaree.

95 min / VOSTFR / 2019.
La cinéaste et actrice enquête sur les circonstances de sa naissance, en
Iran, dans l’une des prisons politiques les plus tristement célèbres au monde.

Après le film, nous prendrons du temps pour échanger et partager autour
d’un **buffet iranien (prix libre et conscient à partir de 5 euros)**


Résumé de tenk
=================

- https://www.on-tenk.com/fr/documentaires/politique/born-in-evin

La cinéaste et actrice Maryam Zaree enquête sur les circonstances de sa
naissance, en Iran, dans l’une des prisons politiques les plus tristement
célèbres au monde.


Description
--------------

Maryam Zaree est née en 1983 à Téhéran, Iran.

Fuyant les persécutions politiques, sa mère l'emmène en Allemagne
lorsqu'elle a deux ans.

Elle a grandi à Francfort-sur-le-Main et a étudié la comédie dans la
célèbre école de cinéma Konrad-Wolf à Potsdamm-Babelsberg.
Elle a tenu les rôles principaux dans une douzaine de longs métrages, a
travaillé pour le théâtre et la télévision et a été récompensée pour
ses performances.

En 2018, elle a reçu un "Grimme Preis" pour sa performance dans la série
télévisée "4 Blocks".

Sa première pièce de théâtre en tant qu'autrice, "Kluge Gefühle" a reçu
le prix du Stückemarkt de Heidelberg et a été représentée sur les scènes
de plusieurs théâtres. "Born in Evin" est son premier long métrage.


L'avis de Tënk
-----------------

Bien que ce film traite d’un sujet profondément politique, et en même
temps très personnel, Maryam Zaree parvient à employer une narration
caractérisée par l’humour ainsi que par un grand sérieux dans le traitement
des conséquences du traumatisme que nombre de protagonistes ont subi.

La réalisatrice suit sa propre histoire pour mettre fin au silence, qui
l’a accompagnée toute sa vie, à propos du lieu et des circonstances de
sa naissance.

Elle rencontre d’autres survivants, parle à des experts et recherche
d’autres enfants nés dans la même prison qu’elle.
Elle tente de trouver des réponses à ses questions personnelles et politiques.
Et sur son chemin, elle réalise plus d’une fois l’importance considérable
qu’il y a à briser le silence et à mettre des mots sur les événements passés.

Luc-Carolin Ziemann
Programmatrice, autrice et formatrice cinéma


Contexte des luttes à Grenoble le 25 avril 2023
==================================================

- :ref:`grenoble_retraites:actions_2023_04_25`
