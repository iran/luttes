
.. index::
   pair: Rassemblement ; Samedi 13 mai 2023


.. _iran_grenoble_2023_05_13:

=====================================================================================================================================================================================================================================================================================================================
♀️✊ ⚖️ 📣 **Femme, Vie, Liberté** 31e rassemblement samedi 13 mai 2023 à Grenoble **place Félix Poulat** à 14h30 **Contre les exécutions en Iran, pour la libération des prisonniers politiques en Iran, pour la libération de Toomaj Salehi** 📣
=====================================================================================================================================================================================================================================================================================================================

Arrêt des exécutions, annulation des condamnations à mort liberation des prisonniers politiques, expulsion de l'ambassadeur d'iran
239e jour depuis la mort de #JinaMahsaAmini (2022-09-16)

- :ref:`mahsa_jina_amini`
- :ref:`slogans`

#Grenoble #IranRevolution #ToomajSalehi #MahsaAmini #MahsaJinaAmini #FreeToomaj


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|

.. figure:: images/annonce.png
   :align: center

   https://www.openstreetmap.org/#map=19/45.18979/5.72682


.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.725057125091554%2C45.18901829435873%2C5.728592276573182%2C45.19056457227583&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/>
   <small><a href="https://www.openstreetmap.org/#map=19/45.18979/5.72682">Afficher une carte plus grande pour situer la place Félix Poulat (Parvis de l'Abbé Pierre) </a></small>



Texte annonçant le rassemblement du samedi 13 mai 2023
==============================================================

Pour la 31e fois depuis l'assassinat de Jina Mahsa Amini, nous nous
rassemblerons sur la place Félix Poulat de Grenoble pour être la voix du
peuple iranien dans son combat contre le régime meurtrier du gouvernement
islamique d'Iran.

Le mardi 13 septembre 2022, Jina Mahsa Amini était arrêtée par la police
des mœurs à Téhéran, car elle n’était pas "assez voilée".

Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.
Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

Nous demandons aux gouvernements des pays occidentaux :

- d'arrêter tout contrat et de négociation avec le gouvernement islamique d'Iran,
- de mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort,
- d'expulser les ambassadeurs et les représentants du gouvernement islamique
- de mettre sur la liste noire les acteurs des massacres et de confisquer
  leurs bien à l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Aujourd'hui, samedi 13 mai 2023, nous demandons la libération de Toomaj Salehi ! 📣


Revendications
===============================================================

- :ref:`revendications_mediapart`

.. include:: ../../../../revendications/revendications_2023_03_08.txt


Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`soutiens_avocats_2023_01_10`

Les victimes de la barbarie des mollahs
--------------------------------------------

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|
- :ref:`yousef_sadrollah_2023_05_08`


Les otages français
=======================

.. include:: ../../../../repression/otages/liste_2023_05_12.txt


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/
- https://www.ici-grenoble.org/agenda

.. figure:: images/annonce_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/rassemblement-contre-les-executions-en-iran-pour-la-liberation-de-toomaj-salehi


Quelques événements du 8 mai 2023 au 14 mai 2023
=============================================================================

- :ref:`semaine_19_iran_2023`


Autres événements ce samedi 13 mai 2023 à Grenoble
==========================================================


.. figure:: images/ici_grenoble_2023_05_13.png
   :align: center

   https://www.ici-grenoble.org/agenda



- 14h00 https://www.ici-grenoble.org/evenement/atelier-patriarchie-anger-management-face-aux-masculinites-toxiques-comment-creer-une-rage-room-feministe
- 14h00 https://www.ici-grenoble.org/evenement/discussion-sur-la-contraception-testiculaire-et-fabrication-de-sous-vetements-contraceptifs
- |important| 14h30 🇮🇷 |JinaAmini| https://www.ici-grenoble.org/evenement/rassemblement-contre-les-executions-en-iran-pour-la-liberation-de-toomaj-salehi
- |important| 14h30 |afrique| https://www.ici-grenoble.org/evenement/rencontre-avec-said-bouamama-pour-un-panafricanisme-revolutionnaire
- 15h00 https://www.ici-grenoble.org/evenement/rassemblement-pour-la-visibilite-de-lencephalomyelite-myalgique-lyon
- |important| 15h00 🇺🇦 |mriya| https://www.ici-grenoble.org/evenement/manifestation-de-soutien-au-peuple-ukrainien

  - https://www.insideoutproject.net/fr/explore/group-action/on-vit-la-guerre-dukraine-parmi-vous

- 17h20 🎥 https://www.ici-grenoble.org/evenement/cinema-letabli-sur-lentrisme-revolutionnaire-dans-les-usines
- 17h20 🎥 https://www.ici-grenoble.org/evenement/cinema-sur-ladamant-sur-une-alternative-aux-hopitaux-psychiatriques
- 19h00 https://www.ici-grenoble.org/evenement/conference-le-secret-defense-ca-concerne-tout-le-monde
- |important| 19h00 https://www.ici-grenoble.org/evenement/resto-malap-repas-congolais-vegan-cuisine-par-des-personnes-sans-papiers



Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2023_05_06`
- :ref:`iran_grenoble_2023_04_22`
- :ref:`iran_grenoble_2023_04_08`
- :ref:`iran_grenoble_2023_04_01`
- :ref:`iran_grenoble_2023_03_25`
- :ref:`iran_grenoble_2023_03_11`
- :ref:`iran_grenoble_2023_03_04`
- :ref:`iran_grenoble_2023_02_25`
- :ref:`iran_grenoble_2023_02_18`
- :ref:`iran_grenoble_2023_02_11`
- :ref:`iran_grenoble_2023_02_04`
- :ref:`iran_grenoble_2023_01_28`
- :ref:`iran_grenoble_2023_01_21`
- :ref:`iran_grenoble_2023_01_14`
- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`


