.. index::
   pair: Librairie ; Utopiran
   pair: Cahiers ; Les cahiers d'avant la chute

.. _utopiran_2023_05_14:

================================================================================================================
2023-05-14 **Rencontre à Seyssins de représentants de la librairie utopiran ; les cahiers d'avant la chute**
================================================================================================================

- https://www.utopiran.com/product-page/les-cahiers-d-avant-la-chute-hors-s%C3%A9rie-mana-neyestani


.. figure:: images/rencontre_utopiran.png
   :align: center

Introduction
==============

- :ref:`grenoble_retraites:jamshid_golmakani_2023_05_14`

C'est à l'occasion :ref:`de la projection du film documentaire Cauchemar dans les Alpes
(32min) de Jamshid Golmakani (Iran), place Victor Schoelcher, SEYSSINS <grenoble_retraites:jamshid_golmakani_2023_05_14>`
que nous avons rencontré des réprésentants de la librairie utopiran et
les cahiers d'avant la chute.


.. _les_cahiers_davant_la_chute:

**Les cahiers d'avant la chute**
===================================


.. figure:: images/les_cahiers_davant_la_chute.png
   :align: center

   https://www.utopiran.com/product-page/les-cahiers-d-avant-la-chute-hors-s%C3%A9rie-mana-neyestani

Les cahiers d’avant la chute sont là pour imprimer l’Histoire et l’inscrire
noir sur blanc.

Ils sont là pour raconter comment des femmes et des hommes se sont levés
un jour pour dire non à la tyrannie, à l’oppression, à l’humiliation et
à l'injustice.

Comme dans les films, mais là pour de bon, la rage au poing, bombant le
torse face à l’oppresseur, sans savoir comment tout se terminera.

Mais ce qui importe, n’est-ce pas le chemin parcouru ?

Lors des derniers grands soulèvements du peuple iranien, en 1999, 2009,
2018, 2019 et 2021, la censure dans le pays ne permit à aucune revue papier
de suivre les événements et de les relater.

Il n’en reste aujourd’hui que des traces électroniques sur le web, ainsi
que des articles publiés dans les journaux et revues étrangères.

Les principaux moyens de communication du soulèvement de 2022 que sont
les réseaux sociaux (twitter et instagram) et quelques chaînes de
télévision de la diaspora ne laissant pas de place à l’analyse de fond
(qui parfois permet de nuancer le discours ambiant et officiel du mouvement),
le support papier nous a paru comme une nécessité évidente dans ce
chemin vers la liberté du peuple iranien.

#hors_série

Le premier Hors Série de la revue « les cahiers avant la chute » a été
publié et est disponible dans la librairie Perse en poche et sera envoyé
à tous les abonnés.

Ce numéro spécial a été publié après les deux premiers numéros des
« cahiers avant la chute » et comprend les dessins et caricatures de
#mananeyestani depuis le premier jour des manifestations en Iran en
trois langues : persan, français et anglais.

La révolution iranienne 2022 vue par Mana Neyestani dans le premier
Hors-Série des cahiers d’avant la chute.

Femme, Vie, Liberté.
