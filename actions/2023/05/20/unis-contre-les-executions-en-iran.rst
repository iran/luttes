
.. index::
   pair: Rassemblement ; Samedi 20 mai 2023


.. _iran_grenoble_2023_05_20:

=====================================================================================================================================================================================================================================================================================================================
♀️✊ ⚖️ 📣 **Femme, Vie, Liberté** 32e rassemblement samedi 20 mai 2023 à Grenoble **place Félix Poulat** à 14h30 **Unis contre les exécutions en Iran** 📣
=====================================================================================================================================================================================================================================================================================================================

Arrêt des exécutions, annulation des condamnations à mort liberation des prisonniers politiques, expulsion de l'ambassadeur d'iran
246e jour depuis la mort de #JinaMahsaAmini (2022-09-16)

- :ref:`mahsa_jina_amini`
- :ref:`slogans`

#Grenoble #IranRevolution #StopExecutionsInIran #ContreLaPeineDeMort #MahsaAmini #LesCahiersAvantLaChute #MajidKazemi #SaeedYaghoubi #SalehMirhashemi #TortureMentale #NonAuFascisme #CecileKohler #JacquesParis #YoussefMehrdad #SadrollahFazeliZarei


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|

.. figure:: images/annonce.png
   :align: center
   :width: 800

   https://www.openstreetmap.org/#map=19/45.18979/5.72682


.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.725057125091554%2C45.18901829435873%2C5.728592276573182%2C45.19056457227583&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/>
   <small><a href="https://www.openstreetmap.org/#map=19/45.18979/5.72682">Afficher une carte plus grande pour situer la place Félix Poulat (Parvis de l'Abbé Pierre) </a></small>



Texte annonçant le rassemblement du samedi 20 mai 2023
==============================================================

Pour la 32e fois depuis l'assassinat de Jina Mahsa Amini, nous nous
rassemblerons sur la place Félix Poulat de Grenoble pour être la voix du
peuple iranien dans son combat contre le régime meurtrier du gouvernement
islamique d'Iran.

Le mardi 13 septembre 2022, Jina Mahsa Amini était arrêtée par la police
des mœurs à Téhéran, car elle n’était pas "assez voilée".

Le vendredi 16 septembre 2022, Jina rendait son dernier souffle.

Depuis, pas un seul jour sans que des milliers de femmes ne défient les
forces de l’ordre pour s'opposer au voile obligatoire.
Les insurgé.es occupent la rue et crient leur haine, réclament justice
et scandent de ne pas vouloir ce régime sanguinaire.

Nous demandons aux gouvernements des pays occidentaux :

- d'arrêter tout contrat et de négociation avec le gouvernement islamique d'Iran,
- de mettre la pression sur ce régime meurtrier pour la libération des
  prisonniers politiques, et l'annulation des condamnations à mort,
- d'expulser les ambassadeurs et les représentants du gouvernement islamique
- de mettre sur la liste noire les acteurs des massacres et de confisquer
  leurs bien à l'étranger

Nous faisons tout pour faire entendre la voix du peuple iranien jusqu'au
renversement de ce régime terroriste.

Depuis plusieurs semaines ce régime sanguinaire multiplie les exécutions
à mort pour faire régner la terreur.

Aujourd'hui, samedi 20 mai 2023, la diaspora mondiale iranienne est uni contre les exécutions en Iran!


Post Scriptum
-----------------

Depuis 2 semaines nous manifestons avec nos camarades Ukrainiens
de l'association `Mriya Ukraine <https://mriya-ukraine.org/>`_ |mriya|.
Lors de la dernière manifestation le samedi 13 mai 2023 nous avons
eu une désagréable rencontre avec **un fasciste qui a tenu des propos
insultants sur les Ukrainiens**.

**Soyons sur nos gardes ! Non au fascisme ! Non à la peste brune ! No pasaran !**


Autres appels à rassemblement en France
-----------------------------------------

- :ref:`yousef_sadrollah_2023_05_17`


Revendications
===============================================================

- :ref:`revendications_mediapart`

.. include:: ../../../../revendications/revendications_2023_03_08.txt


Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`soutiens_avocats_2023_01_10`

Les victimes de la barbarie des mollahs
--------------------------------------------

.. include:: ../../../../repression/executions/liste_2023_05_19.txt
.. include:: ../../../../repression/executions/liste_2023_05_08_pre.txt

**Une semaine d'actualité sur l'iran (Semaine 20 Du lundi 15 mai au dimanche 21 mai 2023 par Bahareh Akrami)**
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- :ref:`akrami_iran:bahareh_akrami_2023_20`


Les otages français
=======================

.. include:: ../../../../repression/otages/liste_2023_05_12.txt

Cette semaine :ref:`communiqué de l’union syndicale des enseignants iraniens en soutien à Cécile Kohler et Jacques Paris <soutien_cecile_jacques_2023_05_17>`

Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/
- https://www.ici-grenoble.org/agenda

.. figure:: images/annonce_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/rassemblement-contre-le-regime-meurtrier-du-gouvernement-islamique-diran



Quelques événements des 2 dernières semaines
==============================================

- :ref:`semaine_20_iran_2023`
- :ref:`semaine_19_iran_2023`


Notre découverte de la revue :ref:`Les cahiers d'avant la chute <les_cahiers_davant_la_chute>` le
dimanche 14 mai 2023 à Seyssins.

**Les cahiers d'avant la chute**
------------------------------------


.. figure:: ../14/images/les_cahiers_davant_la_chute.png
   :align: center

   https://www.utopiran.com/product-page/les-cahiers-d-avant-la-chute-hors-s%C3%A9rie-mana-neyestani

**Les cahiers d’avant la chute sont là pour imprimer l’Histoire et l’inscrire
noir sur blanc**.

**Ils sont là pour raconter comment des femmes et des hommes se sont levés
un jour pour dire non à la tyrannie, à l’oppression, à l’humiliation et
à l'injustice**.

Comme dans les films, mais là pour de bon, la rage au poing, bombant le
torse face à l’oppresseur, sans savoir comment tout se terminera.

**Mais ce qui importe, n’est-ce pas le chemin parcouru ?**

Lors des derniers grands soulèvements du peuple iranien, en 1999, 2009,
2018, 2019 et 2021, la censure dans le pays ne permit à aucune revue papier
de suivre les événements et de les relater.

Il n’en reste aujourd’hui que des traces électroniques sur le web, ainsi
que des articles publiés dans les journaux et revues étrangères.

Les principaux moyens de communication du soulèvement de 2022 que sont
les réseaux sociaux (twitter et instagram) et quelques chaînes de
télévision de la diaspora ne laissant pas de place à l’analyse de fond
(qui parfois permet de nuancer le discours ambiant et officiel du mouvement),
**le support papier nous a paru comme une nécessité évidente dans ce
chemin vers la liberté du peuple iranien**.

#hors_série

Le premier Hors Série de la revue **«les cahiers avant la chute»** a été
publié et est disponible dans la librairie Perse en poche et sera envoyé
à tous les abonnés.

Ce numéro spécial a été publié après les deux premiers numéros des
« cahiers avant la chute » et comprend les dessins et caricatures de
#mananeyestani depuis le premier jour des manifestations en Iran en
trois langues : persan, français et anglais.

La révolution iranienne 2022 vue par Mana Neyestani dans le premier
Hors-Série des cahiers d’avant la chute.

Femme, Vie, Liberté.


Autres événements ce samedi 20 mai 2023 à Grenoble
==========================================================

- :ref:`grenoble_retraites:actions_2023_05_20`

.. figure:: images/ici_grenoble_2023_05_20.png
   :align: center

   https://www.ici-grenoble.org/agenda , :ref:`grenoble_retraites:actions_2023_05_20`



Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2023_05_13`
- :ref:`iran_grenoble_2023_05_06`
- :ref:`iran_grenoble_2023_04_22`
- :ref:`iran_grenoble_2023_04_08`
- :ref:`iran_grenoble_2023_04_01`
- :ref:`iran_grenoble_2023_03_25`
- :ref:`iran_grenoble_2023_03_11`
- :ref:`iran_grenoble_2023_03_04`
- :ref:`iran_grenoble_2023_02_25`
- :ref:`iran_grenoble_2023_02_18`
- :ref:`iran_grenoble_2023_02_11`
- :ref:`iran_grenoble_2023_02_04`
- :ref:`iran_grenoble_2023_01_28`
- :ref:`iran_grenoble_2023_01_21`
- :ref:`iran_grenoble_2023_01_14`
- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`
