.. index::
   pair: Festival mondial de Kian ; 2023-06-10

.. _kian_2023_06_10:

=============================================================================================
2023-06-10 de 16h à 17h30 **Festival mondial de Kian** place Félix Poulat, 38000 Grenoble
=============================================================================================

- :ref:`kian_pirfalak`

.. figure:: images/affiche_festival_kian.png
   :align: center



