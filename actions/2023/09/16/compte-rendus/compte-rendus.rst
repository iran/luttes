
.. index::
   pair: Compte rendus ; Samedi 16 septembre 2023
   pair: Video ; Annonce de la création du parc Jina Mahsa Amini par la mairie de Grenoble
   pair: Place Grenette ; Samedi 16 septembre 2023

.. _compte_rendus_2023_09_16:

=================
Compte rendus
=================

Annonce de la création du parc Jina Mahsa Amini par la mairie de Grenoble
============================================================================

- :ref:`media_2023:photos_2023_09_16`

Annonce de la création du parc Jina Mahsa Amini par Emmanuel Carroz adjoint
'Mémoire, Migrations et Coopérations internationales, Europe' de la
ville de Grenoble lors de la manifestation du 16 septembre 2023
commémorant l'a mort de Jina Mahsa Amini le 16 septembre 2022.

Le parc sera situé à proximité du lycée Louise Michel de Grenoble


- https://www.youtube.com/watch?v=61rVaWKIxYc

.. youtube:: 61rVaWKIxYc


Discours en hommage aux victimes du mouvement Femme ! Vie ! Liberté ! en Iran
=================================================================================

- :ref:`media_2023:photos_2023_09_16`

Discours en hommage aux victimes du mouvement Femme ! Vie ! Liberté ! en Iran


- https://www.youtube.com/watch?v=Y4fLoe61glE

.. youtube:: Y4fLoe61glE



.. _place_grenette_2023_09_16:

Compte rendu de place Grenette (payant) Grenoble : **une centaine de manifestants en soutien de la lutte du peuple iranien, un an après la mort de Mahsa Amini** par Manuel Pavard
======================================================================================================================================================================================

- https://www.placegrenet.fr/2023/09/16/grenoble-une-centaine-de-manifestants-en-soutien-de-la-lutte-du-peuple-iranien-un-an-apres-la-mort-de-mahsa-amini/613217
- :ref:`media_2023:photos_2023_09_16`

:Tags: Amnesty International,femmes iraniennes, iran, Iraniens
       Ligue des droits de l'homme, Mahsa Amini, manifestation
       rassemblement


REPORTAGE - Plus d'une centaine de personnes se sont rassemblées ce samedi
16 septembre 2023, rue Félix-Poulat, avant de défiler dans le centre-ville
de Grenoble, en soutien à la lutte du peuple iranien.

Des manifestants mobilisés en hommage aux victimes du soulèvement
"Femmes, vie, liberté", un an après la mort de Mahsa Amini, étudiante
de 22 ans tuée en garde à vue pour un voile "mal porté".

Alors que la répression meurtrière se poursuit en Iran, tous affichaient
leur solidarité avec la résistance au régime des mollahs.


Le 16 septembre 2022, Mahsa Amini, étudiante de 22 ans d'origine kurde,
était ainsi arrêtée à Téhéran par la police des mœurs, puis tuée en garde
à vue, "pour le dépassement de quelques mèches de cheveux de son voile
obligatoire", rappellent les organisateurs grenoblois.


Près de 500 morts mais une révolte qui ne s'est "jamais arrêtée"

Ce "meurtre étatique" avait mis le feu aux poudres et déclenché une immense
vague de contestation, notamment chez les femmes et les jeunes Iraniens.

Des manifestations réprimées dans le sang par le régime des mollahs, avec
un bilan de près de 500 morts et plus de 15 000 arrestations de manifestants
en un an, selon l'ONG Iran Human Rights (IHR).




