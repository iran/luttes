
.. index::
   pair: Rassemblement ; Samedi 16 septembre 2023
   pair: Jina Mahsa Amini ; Samedi 16 septembre 2023


.. _iran_grenoble_2023_09_16:

=====================================================================================================================================================================================================================================================================================================================
♀️✊ ⚖️ 📣 **En hommage aux victimes du mouvement Femme ! Vie ! Liberté !** rassemblement samedi 16 septembre 2023 à Grenoble place Félix Poulat à 14h30 📣
=====================================================================================================================================================================================================================================================================================================================


- :ref:`mahsa_jina_amini`
- :ref:`slogans`

#Grenoble #IranRevolution #Hommage #Victime #MahsaJinaAmini #FemmeVieLiberte


- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|


.. figure:: images/affiche_2023_09_16.jpeg
   :align: center

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.725057125091554%2C45.18901829435873%2C5.728592276573182%2C45.19056457227583&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/>
   <small><a href="https://www.openstreetmap.org/#map=19/45.18979/5.72682">Afficher une carte plus grande pour situer la place Félix Poulat (Parvis de l'Abbé Pierre) </a></small>



Texte annonçant le rassemblement du samedi 13 mai 2023
==============================================================

En hommage aux victimes du soulèvement Femme,Vie, Liberté et
en soutien au peuple iranien dans sa lutte contre la Répression et
l’obscurantisme de la République Islamique.

Le 16 septembre 2022 la jeune Mahsa Jina Amini d’origine kurde en visite
à Téhéran a perdu la vie suite à son arrestation violente par la police
des mœurs (pour le dépassement de quelques mèches de cheveux de son voile
obligatoire ).

Ce meurtre étatique a allumé le feu de la colère des femmes dès la cérémonie
de sépulture de Mahsa au cri de Femme Vie Liberté pour s’étendre comme
une traînée de poudre à plus de cent villes à travers le pays.

Le soulèvement des femmes ébranle le régime
===============================================

Ce régime misogyne depuis son arrivée au pouvoir en 1979, n’a cessé
d’inférioriser et de discriminer les femmes par ses lois misogynes
issues de la charia.

Ce régime théocratique, depuis plus de 40 ans, essaie par la répression
brutale d’imposer sa conception de «la vertu et du rejet du vice».

Ce régime-là a tremblé en voyant les foulards brûlés et en entendant des
manifestants pacifistes exiger la liberté, l’égalité, la démocratie, la
justice sociale, et la laïcité.

La réponse sanglante ne s’est pas faite attendre
===================================================


En plus de quatre mois de manifestations régulières, plus de 600 personnes
ont été tuées dont 75 mineurs, il y a eu 20 000 arrestations, 4 exécutions
publiques et deux dans les prisons à l’aube au moment de l’appel à la
première prière en décembre 2022 et janvier 2023.

La torture, la violence sexuelle, le viol des filles et des garçons dans
les lieux de détention et les prisons permettent l’obtention d’aveux forcés.
Aveux qui seront bien sûr utilisés pour condamner encore d’autres personnes
à la mort ou à de longues peines de prison.
C’est dans les régions du Kurdistan et du Baloutchistan que les victimes
sont les plus nombreuses, au moins 85 exécutions dont 6 femmes.

Malgré tout la contestation continue
=======================================

Malgré l’empoisonnement de collégiennes et collégiens pour terroriser
leurs familles et plus largement toute la société, malgré cette nouvelle
vague de répression et d’intimidations des proches des victimes et des
prisonniers, comme les arrestations sans motif, l’empêchement de
recueillement familial autour des tombes, la destruction des pierres tombales.

Face à la poly crise économique, sociale, politique, environnementale,
face à la corruption, l’inflation, le chômage, l’effondrement de la monnaie
nationale et du pouvoir d’achat, les contestations multiformes des enseignant.es,
des ouvrier.es, des retraité.es s’accentuent.

Et la détermination et le courage des femmes qui refusent de porter le
voile obligatoire s’affichent toujours admirablement dans les rues.

C’est bien un mouvement profond, féministe et anti-autoritaire qui s’attaque
aux fondements même du pouvoir!

Mais hélas la répression aussi !
====================================

La police des mœurs et de nouveau les gardiens de hijab sont extrêmement
présents.

Un projet de loi inique annonce d’autres représailles, de lourdes condamnations
pécuniaires, plus d’emprisonnement, des refus d’accueil dans les services
de santé et dans les administrations, l’interdiction de passeport et de
sortie du pays pour les femmes refusant le voile obligatoire.

**Le voile a une signification politique.**

Il est le moyen de contrôle du corps des femmes, il impose la restriction
drastique de leur liberté. Et au-delà de cette contrainte misogyne et
obscurantiste, le voile obligatoire est pour le régime théocratique une
manière de mettre au pas l’ensemble de la société.

**Ce mouvement révolutionnaire du peuple iranien qui veut en finir avec la
république islamique a besoin d’une solidarité massive ã l’intérieur et
au-delà de ses frontières sur son long chemin de libération**.

Soyons nombreux.es à manifester notre soutien le samedi 16 septembre 2023 !

À l’appel de : LDH Grenoble Métropole, LDH Iran, CISEM, ACAT
Grenoble ,Collectif grenoblois contre la peine de mort.
Soutiens de : CLAG ( Cercle laïque ) UFAL Cercle BernardLazare, APARDAP ATTAC,
CSRA, LIFPL, Mouvement de la Paix, CGT38, CGT sans papiers, PCOF, Go citoyenneté
NPA38, Ensemble Isère, Libre Pensée Isère, fédé PS


`Tract au format PDF <https://iran.frama.io/media-2023/_downloads/15d56429a369476868ddc852088e522a/rassemblement_grenoble_en_hommage_aux_victimes_du_mouvement_femme_vie_liberte_grenoble_2023_09_16.pdf>`_


Compte rendus de la manifestation
===================================

.. toctree::
   :maxdepth: 3

   compte-rendus/compte-rendus


Pétition contre les peines de mort
======================================

- https://www.change.org/p/stop-execution-of-iranian-protesters
- :ref:`soutiens_avocats_2023_01_10`

Les victimes de la barbarie des mollahs
--------------------------------------------

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|
- :ref:`yousef_sadrollah_2023_05_08`


Articles récents sur l'état de la révolution iranienne
========================================================================

- :ref:`luttes_2023:temoignages_2023_09_08`
- :ref:`luttes_2023:mediapart_revolution_2023_09_15`
- :ref:`luttes_2023:mediapart_2023_09_15_resistance`


Livres récents
=================

- :ref:`livre_femme_vie_makaremi_2023`
- :ref:`livre_femme_vie_satrapi_2023`


Les otages français
=======================

.. include:: ../../../../repression/otages/liste_2023_05_12.txt


Ici-grenoble 🙏 |ici_grenoble|
-------------------------------------

- https://www.ici-grenoble.org/
- https://www.ici-grenoble.org/agenda

.. figure:: images/annonce_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/rassemblement-en-hommage-aux-victimes-du-mouvement-femme-vie-liberte-en-iran



Manifestations précédentes à Grenoble
=======================================

- :ref:`iran_grenoble_2023_05_20`
- :ref:`iran_grenoble_2023_05_13`
- :ref:`iran_grenoble_2023_05_06`
- :ref:`iran_grenoble_2023_04_22`
- :ref:`iran_grenoble_2023_04_08`
- :ref:`iran_grenoble_2023_04_01`
- :ref:`iran_grenoble_2023_03_25`
- :ref:`iran_grenoble_2023_03_11`
- :ref:`iran_grenoble_2023_03_04`
- :ref:`iran_grenoble_2023_02_25`
- :ref:`iran_grenoble_2023_02_18`
- :ref:`iran_grenoble_2023_02_11`
- :ref:`iran_grenoble_2023_02_04`
- :ref:`iran_grenoble_2023_01_28`
- :ref:`iran_grenoble_2023_01_21`
- :ref:`iran_grenoble_2023_01_14`
- :ref:`iran_grenoble_2023_01_07`
- :ref:`iran_grenoble_2022_12_31`
- :ref:`iran_grenoble_2022_12_24`
- :ref:`iran_grenoble_2022_12_17`
- :ref:`iran_grenoble_2022_12_10`
- :ref:`iran_grenoble_2022_12_03`
- :ref:`iran_grenoble_2022_11_26`
- :ref:`iran_grenoble_2022_11_19`
- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`iran_grenoble_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`

