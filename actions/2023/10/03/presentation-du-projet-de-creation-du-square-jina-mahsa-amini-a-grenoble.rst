.. index::
   pair: Square ; Jina Mahsa Amini
   pair: Grenoble ; Square Jîna Mahsa Amini

.. _square_jina_amini_2023_10_03:

====================================================================================================================
Mardi 3 octobre 2023 : **Présentation du projet de création du square Jîna Mahsa Amini à Grenoble** de 17 à 19h
====================================================================================================================


#Grenoble #projet #MahsaAmini #MahsaJinaAmini #Square #IranRevolution #IranRevolution2023

Affiche
========


**Stand de présentation du projet de création du square Jîna Mahsa Amini
à côté du lycée Louise-Michèle de Grenoble**.

En présence de :

- Gilles Namur (@Gilles_Namur@mastodon.gougere.fr), adjoint
  Espaces publics -Nature en ville - Biodiversité et Fraîcheur
- et de Thierry Chastagner, Maire adjoint du secteur 3

Aménagement d'un parvis végétalisé pour le lycée des métiers Louise Michel.
Création d'espaces verts et platation d'arbres.

.. figure:: images/square_jina_mahsa_amini.png
   :align: center

   Stand de présentation du projet de création du square Jîna Mahsa Amini


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.7060080766677865%2C45.16763641362521%2C5.713089108467103%2C45.17083976387771&amp;layer=mapnik"
    style="border: 1px solid black">
   </iframe>
   <br/><small>
   <a href="https://www.openstreetmap.org/#map=18/45.16924/5.70955">Afficher une carte plus grande</a></small>


Gilles Namur
----------------

- https://mastodon.gougere.fr/@Gilles_Namur
- https://www.grenoble.fr/1805-gilles-namur.htm


.. figure:: images/gilles_namur_mastodon.png
   :align: center

Thierry Chastagner
-------------------

- https://www.grenoble.fr/1835-thierry-chastagner.htm


Annonce de la création du square Jîna Mahsa Amini le samedi 16 septembre 2023
===================================================================================

- :ref:`video_annonce_square_jina_amini`

.. youtube:: 61rVaWKIxYc

Inauguration du jardin Villemin Mahsa Jîna Amini à Paris le samedi 16 septembre 2023
=======================================================================================

- :ref:`luttes_2023:jardin_mahsa_jina_amini_2023_09_16`


Exposition de peinture **Femme Vie Liberté et la Liberté en moi** de Marjan Yazdani du 6 au 26 octobre 2023
===============================================================================================================

- :ref:`marjan_2023:femme_vie_liberte_2023_10`
