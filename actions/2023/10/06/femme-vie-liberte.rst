.. index::
   pair: Dessins ; Marjan Yazdani

.. _ref_femme_vie_liberte_2023_10:

===============================================================================================================
Exposition de peinture **Femme Vie Liberté et la Liberté en moi** de Marjan Yazdani du 6 au 26 octobre 2023
===============================================================================================================

- :ref:`marjan_2023:femme_vie_liberte_2023_10`
