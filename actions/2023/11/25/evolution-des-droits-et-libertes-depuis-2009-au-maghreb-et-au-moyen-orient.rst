.. index::
   pair: Conférence ; Évolution des droits et libertés depuis 2009 au Maghreb et au Moyen Orient
   pair: Maison du Tourisme ; Samedi 25 novembre 2023

.. _conf_debat_2023_11_25:

===========================================================================================================================================================
Conférence-débat Évolution des droits et libertés depuis 2009 au Maghreb et au Moyen Orient le samedi 25 novembre 2023 16h00-18h30, Maison du Tourisme
===========================================================================================================================================================

Évolution des droits et libertés depuis 2009 au Maghreb et au Moyen Orient avec:

- Akram Belkaïd, rédacteur en chef du Monde Diplomatique
- Pinar Selek (en visio), sociologue, militante antimilitariste et éco-féministe turque

Des interventions sur la situation en Algérie, Egypte, Iran, Syrie

- faire le point sur l’évolution des droits et libertés, en particulier sur les plans la
  liberté de la presse, des prisonniers politiques, des droits des organisations
  démocratiques à pouvoir intervenir dans le débat public.

- marquer notre solidarité avec tous les peuples qui luttent pour plus de liberté et
  de démocratie, pour la reconnaissance de leurs droits politiques et culturels.


Organisée par:

- collectif 17 octobre 1961 Isère (Algérie au Coeur, Amal, ANPNPA, ASALI, CSRA,
- Libre Pensée, Mouvement de la Paix, Ras L’Front)
- LDH, LDDHI, AIAK, Nil 38
