.. index::
   ! Behrouz Farahani

.. _behrouz_farahani:

=====================================================================================================
**Behrouz Farahani** (représentant de Solidarité Socialiste avec les Travailleurs en Iran)
=====================================================================================================

.. figure:: images/behrouz_farahani.png
   :align: center


Articles sur http://www.iran-echo.com/articles.html
======================================================

- http://www.iran-echo.com/articles.html
- https://lanticapitaliste.org/actualite/international/iran-repression-brutale-face-la-montee-et-la-convergence-des-luttes
- http://www.iran-echo.com/echo_pdf/07062020.pdf
- https://nouveaupartianticapitaliste.org/actualite/international/iran-rohani-le-modere-sacharne-contre-les-syndicalistes
- http://www.iran-echo.com/echo_pdf/syrie_fara.pdf
- http://www.iran-echo.com/echo_pdf/un_president_iranien_a_paris.pdf
- https://www.europe-solidaire.org/spip.php?article23779


Articles sur https://lanticapitaliste.org
=======================================================================

- https://lanticapitaliste.org/auteurs/behrooz-farahany
- https://lanticapitaliste.org/actualite/international/iran-lindispensable-convergence-des-luttes
- https://lanticapitaliste.org/actualite/international/vers-un-soulevement-generalise-en-iran
- https://lanticapitaliste.org/actualite/international/iran-repression-brutale-face-la-montee-et-la-convergence-des-luttes
- https://lanticapitaliste.org/actualite/international/iran-epidemie-de-covid-19-tsunami-des-privatisations
