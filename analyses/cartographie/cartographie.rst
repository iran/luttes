.. index::
   pair: Analyses ;  Cartographie

.. _iran_cartographie:

=====================================================================================================
Cartographie
=====================================================================================================

- https://cartonumerique.blogspot.com/2022/11/manifestations-en-Iran.html

 La carte, objet éminemment politique : les formes du soulèvement en Iran


L'Iran est en proie à des manifestations depuis la mort en détention le
16 septembre 2022 de Mahsa (Zhina) Amini, une Iranienne d'origine kurde
de 22 ans qui avait été arrêtée trois jours plus tôt pour avoir enfreint
le code vestimentaire islamique imposé aux femmes.

En quelques semaines, les manifestations se sont propagées à l'ensemble
du pays comme le montrent les données disponibles qui permettent d'établir
une cartographie du soulèvement face au pouvoir militaro-religieux de Téhéran.

Le régime intensifie sa répression des manifestations d'une manière qui
pourrait alimenter un soulèvement durable et de plus en plus violent
contre l'establishment politique.

Ce ne sont pas les premières manifestations auxquelles Raisi est confronté
en tant que président, la hausse du coût de la vie ainsi que la répression
à l'égard des minorités ethniques ayant déjà provoqué des soulèvements
depuis deux ans.

Mais ces manifestations sont devenues beaucoup plus importantes et
concernent désormais les 31 provinces du pays. Le gouvernement iranien
a introduit des restrictions sur Internet et sur les télécommunications,
ce qui rend difficile d'évaluer l'ampleur du mouvement.

Des villes comme la capitale Téhéran, des groupes ethniques minoritaires
tels que les Kurdes, des étudiants et des lycéens sont impliqués dans
un soulèvement qui concerne désormais la défense des droits de l'homme
dans un pays frappé tout à la fois par le manque de libertés publiques,
l'absence d'égalité des sexes et les crises économiques récurrentes.

