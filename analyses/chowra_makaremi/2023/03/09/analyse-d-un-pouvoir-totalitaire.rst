.. index::
   pair: Chowra Makaremi; Analyse d'un pouvoir totalitaire
   ! #hezbollah
   ! #moebus


.. _chowmak_2023_03_09:

===================================================================================================================================
**Analyse d'un pouvoir totalitaire** par Chowra Makaremi  #hezbollah #moebus #arendt
===================================================================================================================================

- https://nitter.poast.org/chowmak/status/1633774254989754368#m

Regardez cette archive fabuleuse de la résistance des iraniennes le #8mars 1979 en Iran. 👇
Les premières minutes nous donnent une clé essentielle pour comprendre
le gazage des élèves & étudiantes iraniennes aujourd'hui (1000+ hospitalisées) 🧵 1

Un journaliste parle des "hommes zélés" qui frappent et poignardent les
manifestantes. Ce ne sont pas des fanatiques quelconques qui agissent
de leur propre chef.

**C'est le "hezbollah" : des milices en civil contrôlées par les mosquées,
et liées au sepah & au bassidj**.

Ces milices ont pris le contrôle de l'espace public par la terreur, point
crucial pour la conquête institutionnelle du pouvoir par Khomeini après
la révolution.
Mais elles étaient présentées comme:

1) n'ayant aucun rapport et lien avec l'Etat

2) étant non-organisées/spontanées = l'incarnation de cette souveraineté
   populaire qui a fait la révolution de 79.
   Cette fiction est un nerf vital du pouvoir iranien.

**Le hezbollah, dans son invisibilité substancielle**, organise la terreur
et en même temps assoit le récit d'une *République* islamique qui

1) agit légalement/proprement (fonctionne normalement),
2) applique les politiques islamistes que veulent "les gens".

**On est à l'articulation de la propagande et de la terreur** (pour reprendre
les notions de Arendt).

C'est le hezbollah qui en attaquant les femmes à l'acide, au couteau,
les a soumis à la loi sur le voile obligatoire, d'abord désobéie.

**Ces milices sont feminicidaires par nature.**

C'est la branche extra-légale et secrète d'un pouvoir qui s'est institué
et a gouverné en articulant **3 dimensions : légal/para-légal/extra-légal**.

Ces dimensions s'articulent comme des rubans de moebus: **le dehors passe dedans**.

**Extra-légal**: des inconnus gazent des lycéennes à travers le pays.
On ne sait rien d'eux sauf qu'ils jouissent sacrément de ressources,
d'impunité et d'organisation.

**Para-légal**: les réseaux de surveillance du bassidj lourdement présents
dans l'institution scolaire contrôlent & empêchent toute organisation/action
de protestation et autodéfense.
Des forces de l'ordre en civil répriment les parents en colère.

**Légal**: Khamenei demande une enquête pour faire la lumière sur ces crimes...
qui (ah bah ça tombe bien) organisent une politique de terreur et de punition
en réponse au mouvement Femme Vie Liberté.

Mais comment se fait-ce qu'il n'y ait aucune enquête sur ces milices
depuis 1979 ?

1. Parce que les faits sont difficiles à établir.
   Le dispositif est construit sur la dimension secrète de ces réseaux
   "c'est pas nous, c'est les gens (qu'on représente légitimement)" 🤷‍♂️😇🥸😎

2. Parce que mon analyse s'appuie sur les **théories du totalitarisme** :
   elles offrent les outils pour comprendre ces mécanismes de pouvoir.

   **Or il était académiquement interdit de penser que la République islamique
   fut totalitaire** (imprécision ! exagération !  biais militant !)

La pensée éclairée prévalant depuis les 1990s à consisté à explorer les
ambiguïtés de l'Iran ("loin des clichés") en exerçant certes son indépendance
critique, mais à **l'intérieur des fictions du pouvoir iranien**.

Hors de question de voir le "twist" du ruban de moebus.

3. Pourquoi les chercheur.e.s, auteur.e.s, artistes, journalistes ont
   non seulement reconduit de bonne foi mais aussi parfois été les gardiens
   farouches de ce statut-quo ?

This is another story... ;)

.. note:: Ce n'est pas le savoir critique qui attaque le pouvoir en en exposant
   les fictions et mécanismes (comme naïvement je le crus longtemps).

   C'est la déligitimation radicale de ce pouvoir dans la rue en Iran qui a
   opéré un renversement, rendant audible ce genre d'analyse.

