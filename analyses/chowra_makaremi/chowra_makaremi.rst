.. index::
   ! Chowra Makaremi

.. _chowra_makaremi:

==================================================================
**Chowra Makaremi**
==================================================================

- http://iris.ehess.fr/index.php?1240
- https://offsite.hypotheses.org/
- https://www.gallimard.fr/Catalogue/GALLIMARD/Temoins/Le-cahier-d-Aziz
- https://nitter.poast.org/chowmak/rss

.. figure:: images/chowmak.png
   :align: center

   https://twitter.com/chowmak

.. figure:: images/sur_media_part.png
   :align: center

   https://www.youtube.com/watch?v=KKVqOaOL_YA (Iran : « Femmes, vie, liberté, c'est un projet politique »)


.. figure:: images/le_cahier_daziz.png
   :align: center

   https://www.gallimard.fr/Catalogue/GALLIMARD/Temoins/Le-cahier-d-Aziz


Actions
========

- :ref:`analyse_chowmak_2023_03_09_local`
- :ref:`feminisme_iran_2022_10_21`
- :ref:`ehess_iran_2022_10_14`


.. toctree::
   :maxdepth: 3

   2023/2023
