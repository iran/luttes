.. index::
   pair: Sarah Selami; Jeudi 30 mars 2023
   pair: Sarah Selami; FSM

.. _sarah_selami_cgt_2023_03_30:

======================================================================================================================================================================================
Jeudi 30 mars 2023 **L'intervention de Sara Salemi, représentante de la Solidarité Socialiste avec les Travailleurs en Iran au 53ème congrès de la CGT (mars 2023) de Sara Selami**
======================================================================================================================================================================================

- https://www.youtube.com/watch?v=5cutmIP6uHM
- https://entreleslignesentrelesmots.wordpress.com/2023/03/13/la-fsm-soutient-la-repression-du-peuple-iranien/

.. figure:: ../../../images/sarah_selami_2023_03_31.png
   :align: center


.. figure:: ../../../images/sarah_2023_03_30_bis.png
   :align: center


..  youtube:: 5cutmIP6uHM

Introduction
===============

Sara Selami, militante ouvrière et politique iranienne, était invitée au
53ème congrès de la CGT pour porter un message fraternel du syndicat des
travailleurs de la régie de transports de Téhéran et sa banlieue (Vahed)
et évoquer la situation politique de son pays.


Discours
==========

- http://www.iran-echo.com/index.html
- http://www.iran-echo.com/echo_pdf/discours_sara_fr.pdf


Tout d’abord je remercie la CGT pour cette invitation grâce à laquelle
je peux m'adresser à vous.

Je représente l’association « Solidarité Socialiste avec les Travailleurs
en Iran » qui collabore avec le collectif intersyndical français de soutien
aux travailleuses/eurs en Iran, dont la CGT est membre.

Naturellement, celui qui aurait dû s’adresser à vous dans cette tribune,
est Reza Shahabi, membre de la direction du syndicat Vahed de la régie
des transports de Téhéran et sa banlieue, qui était invité au congrès
précédent.

Mais il est emprisonné depuis mai 2022 dans les geôles de la République Islamique.

Voici maintenant le message que le syndicat Vahed a adressé au 53ème congrès
de la CGT.

« Nous saluons les participant.es du 53eme Congrès de la "CGT", qui se
tient du 27 au 31 mars 2023 à Clermont-Ferrand.

Nos chaleureuses salutations à toutes et tous les travailleuses/eurs de
France qui ont toujours été l'un des pionniers de la lutte contre l'ordre
capitaliste mondial, et qui sont engagés ces jours-ci dans une lutte décisive
contre les lois régressives du gouvernement français.

Nous apportons notre soutien aux luttes que vous menez pour vos droits.

La brutalité policière à l’encontre des travailleurs protestataires montrent
que lorsque les profits des capitalistes et les lois anti-ouvrières sont
en jeu, il n'y a pas de différence substantielle entre les Etats capitalistes
du monde.

Elles montrent que leur réponse à nos protestations passe par la répression.

Trois de nos militants bien connus, Reza Shahabi, Daoud Razavi et Hassan
Saïdi, s’étaient déjà rendus en France à l’invitation des syndicats français
dont la CGT, pour y participer à des réunions syndicales publiques.

Reza Shahabi a été invité au 52ème congrès de la CGT à Dijon.
Ils sont tous les trois de nouveau incarcérés depuis plusieurs mois.

Comme vous le savez, depuis septembre de l’année dernière, à la suite de
l’assassinat de Jina (Mahsa) Amini notre pays connaît des manifestations
à l’échelle nationale, connu mondialement comme le soulèvement « Femme, Vie, Liberté ».

Les organisations syndicales indépendantes en Iran, dont le Syndicat des
travailleurs de la Regie du Transport de Téhéran et sa Banlieue (Vahed),
ont soutenu et continueront de soutenir les luttes des femmes, des jeunes
et des peuples opprimés dans notre pays.
Nous avons fermement condamné la répression, l’assassinat de manifestants,
les lourdes peines d’emprisonnement et en particulier la peine de mort
contre les manifestants.

Nous exigeons la libération inconditionnelle de toutes et tous les détenus
et prisonniers politiques.

**Nous n’attendons rien des Etats et des puissances capitalistes qui ne
cherchent que leurs propres intérêts**.

Nous ne comptons que sur la force de la classe ouvrière en Iran et le
soutien de mouvements ouvriers dans le monde, comme le votre, chers
camarades en France !

Victoire à la classe ouvrière en France, en Iran et dans le monde !
Vive la solidarité internationale des travailleurs !
Téhéran le 27 mars 2023

Syndicat des Travailleurs de la Régie du Transport de Téhéran et sa Banlieue
(Vahed) »

Description de la situation en Iran
---------------------------------------

Maintenant Je vous présente le plus brièvement possible la situation actuelle en Iran.
Après six mois d’affrontements virulents, le soulèvement « Femme, Vie, Liberté » s’essouffle
face à une répression féroce qui relève de crime contre l’humanité avec 469 morts dont 60
enfants et adolescents, plus de 19000 arrestations, des disparitions forcées, des actes de torture et
de viol, des dizaines de condamnations à mort, 4 exécutions, … et enfin la vague d’attaques au
gaz qui a frappé les écoles pour filles dans tout le pays. Mais malgré l’essoufflement des
manifestations insurrectionnelles, la colère ne cesse de s’amplifier face à cette répression
sanglante et à l’aggravation des conditions sociales de plus en plus insupportables. La lutte
continue autrement et les aspirations des femmes et des hommes en révolte restent inconciliables
avec le régime capitaliste dictatorial de la République Islamique.

Ce soulèvement est la suite et le point culminant des luttes engagées depuis longtemps. Je cite les
plus importants et les plus récents, les mouvements de masse de l’Hiver 2017/2018 et ensuite
celui de l’automne 2019, dont le mot d’ordre était « pain, travail, liberté ». Ils reflétaient le ras le
bol d’un grand nombre de travailleurs surexploités et de chômeurs négligés sous un ordre
ultralibéral déchainé dirigé par une dictature théologique. Ils ont été réprimés dans le sang.
Ensuite les grandes agitations sociales de travailleurs et de retraités en 2021/2022, avec plus de
4000 actions de protestations pour des revendications de subsistance, suivies de plusieurs vagues
de répression et d’arrestation des militants syndicaux.

Le soulèvement "Femme, Vie, Liberté" a ému le monde entier a engendré un élan de solidarité
internationale sans précédent. Plusieurs organisations syndicales, qu’elles soient mondiales ou
nationales, ont apporté leur soutien au peuple soulevé et ont condamné sa répression brutale par
le régime Islamique : la CGT, Solidaires, FSU, CFDT et l’UNSA en France, les différents
syndicats en Allemagne, en Angleterre, au Bangladesh, au Canada, au Danemark, en Espagne,
aux États-Unis, en Indonésie, en Norvège, en Palestine, au Pays bas, en Suède, en Suisse, en
Slovénie, ... et la Confédération Syndicale Internationale.

Presque tous sauf la Fédération Syndicale Mondiale. Alors que presque tous les conflits du
monde passent sous la plume de FSM, son silence sur les événements qui ont ébranlé l’Iran
pendant plus de six mois est assourdissant. C’est le même silence qu’elle a observé à propos du
mouvement de l’automne 2019 et la sanglante répression qui s’en est suivie. Mais quand on
connaît les liens étroits que cette organisation entreprend depuis plusieurs années avec les
représentants du régime islamique d’Iran on comprend les raisons de ce silence complice.
En effet, un des vice-présidents de la FSM depuis mai 2022 n’est autre que le dirigent d’un
groupe nommé « Maison des Travailleurs » en Iran. L’activité principale de « Maison des
travailleurs », en compagnie des « Conseils Islamiques » dans les entreprises, consiste à contrôler
et réprimer le mouvement ouvrier, à empêcher la constitution des syndicats indépendants, et à
parapher l’accord sur le salaire minimum de misère chaque année lors des dites « négociations
tripartites ». Un groupe qui n’est même pas un syndicat jaune, mais un parti idéologico-politique
lié au régime islamique, siège en tant que représentant des travailleurs iraniens au sein de la FSM.
Ses hommes se rendent chaque année à la conférence internationale de l’OIT, alors que les
représentants des syndicats et organisations ouvrières indépendants sont sous les verrous.
Le vice-président iranien de FSM, Alireza Mahjoub, président de « Maison des travailleurs », est
non seulement dirigeant et coordinateur de répression des travailleurs, mais a été personnellement
impliqué dans le tabassage des militants ouvriers.
Une telle situation au sein d'une organisation mondiale des travailleurs est inadmissible.

La Charte des revendications minimales
--------------------------------------------------

- :ref:`revendications_2023`
- :ref:`revendications_iran_2023_02_15`

Je vais finir avec le résumé d’une « Charte des revendications minimales »
que vingt organisations indépendantes syndicales et civiles en Iran ont
élaboré et publié récemment.

Elle regroupe en 12 articles des revendications sociales et politique progressistes qui ne peuvent être
réalisé qu’après la disparition de la république Islamique, mais elles constituent d’ors et déjà un
socle pour le développement d’un pôle progressiste et socialisant de gauche à l’intérieur du pays,
en face des formations de droite au sein de l’opposition.

Plusieurs organisations syndicales dont la CGT, associations et individus
ont apporté leur soutien à cette Charte.

Le texte de ce soutien est à votre disposition au stand de l'Espace International
du congrès, si vous souhaitez le signer.


Vive la lutte !
Vive la CGT !
Vive la solidarité internationale des travailleuse.eur


Liens
=======

- :ref:`raar_2024:fsm_2024_05_23`
