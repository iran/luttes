
.. _nvo_sarah_selami_2023_03_31:

=====================================================================================================
2023-03-31 **La militante iranienne Sara Selami raconte la résistance dans son pays**
=====================================================================================================

- https://nvo.fr/la-militante-iranienne-sara-selani-raconte-la-resistance-dans-son-pays/

Ovationnée par le 53e Congrès de la CGT, la militante iranienne Sara Selami
raconte la répression qui s'abat sur les défenseurs de la liberté en Iran,
le mouvement « Femme, vie, Liberté », les crimes du régime Iranien envers
la population et les syndicalistes emprisonnés.

Un contexte qui appelle à la solidarité internationale.
Interview par Régis Frutier.


.. figure:: images/sarah_selami_2023_03_31.png
   :align: center

