
.. _sarah_selami_2023_06:

=====================================================================================================
2023-06
=====================================================================================================


.. _sarah_selami_2023_06_26:

2023-05-26 **L’intervention d’une camarade iranienne sur le paysage social et syndical en Iran**
==========================================================================================================================================

- https://www.communisteslibertairescgt.org/FERC-Un-vent-syndicaliste-revolutionnaire-souffle-sur-le-congres.html
- :ref:`sarah_salemi_2023_06_26_local`
