
.. _somaye_iran_2023_04_02:

====================================================================================
2023-04-02 **Somayeh Rostampour : « En Iran, le mur de la peur est tombé »**
====================================================================================

- https://www.mediapart.fr/journal/international/020423/somayeh-rostampour-en-iran-le-mur-de-la-peur-est-tombe


:download:`Article à télécharger <pdfs/Somayeh_Rostampour_En_Iran_le_mur_de_la_peur_est_tombe_Mediapart_2023_04_02.pdf>`


Autrices
=========

- Somayeh Rostampour et Rachida El Azzouzi

Somayeh Rostampour : « En Iran, le mur de la peur est tombé »
================================================================

« Au Kurdistan iranien, il y a toujours eu une dynamique féministe »,
souligne Somayeh Rostampour, chercheuse à Paris 8 et spécialiste du
féminisme kurde.

Pour elle, le pays est désormais « uni pour faire chuter le régime » de Téhéran.

Rachida El Azzouzi


Chercheuse à l’université Paris 8, Somayeh Rostampour a grandi au Kurdistan
iranien, à la frontière avec l’Irak, avant de rejoindre Téhéran pour ses
études puis la France où elle vient de soutenir une thèse sur les femmes
combattantes kurdes au Rojava (Kurdistan syrien) et au Bakur (Kurdistan turc)
(Genre, savoir local et militantisme révolutionnaire.

Mobilisations politiques et armées des femmes kurdes du PKK après 1978).

Elle s’est notamment penchée sur le féminisme local, appelé Jineology, et
son apport aux études féministes particulièrement dans les pays en guerre
et en proie à des conflits ethniques. Entretien.

Mediapart : Six mois après la mort de Jina Mahsa Amini, la contestation se réinvente aujourd’hui en Iran ... Quel bilan dressez-vous jusqu’ici ?
===================================================================================================================================================

Six mois après la mort de Jina Mahsa Amini, la contestation se réinvente
aujourd’hui en Iran sous d’autres formes, collectives et individuelles,
face à l’extrême répression. Quel bilan dressez-vous jusqu’ici ?

Somayeh Rostampour : La base du régime a été profondément attaquée.
Le soulèvement a fait voler en éclats les catégories sociales, ethniques, etc.

Étudiants, travailleurs, Baloutches, Kurdes, diaspora, groupes à l’intérieur
du pays…, tout le monde est uni pour faire chuter le régime.

Le mur de la peur est tombé vis-à-vis de l’État mais aussi de la famille.

En six mois, l’impact est déjà visible dans le quotidien, à l’image de la
manière dont les femmes réagissent face à l’État et au sein des familles
pour reprendre le contrôle sur leur corps, s’habiller, s’exprimer, dire
non, participer aux manifestations, descendre dans la rue.

La fête de Norouz, qui célèbre le premier jour de printemps, est très
intéressante cette année.

Au Kurdistan, par exemple, elle a été très militante, très politique avec
des slogans comme « jin jiyan azadi » (« femme, vie, liberté ») même pour
ceux qui ne sont pas politisés, alors que les années précédentes,
c’était plus une fête culturelle.

Le soulèvement a-t-il permis de dépasser les clivages ethniques ?
======================================================================

On n’a pas réglé cette question mais on avance. Le peuple iranien a compris
l’importance de l’union.

Pour la première fois, on assiste à une visibilisation des Baloutches,
on rend hommage aux courageux Kurdes pour avoir lancé le soulèvement.


L’Iran et l’Irak ont signé dimanche 19 mars 2023 un accord pour « protéger la frontière », où sont basés des groupes d’opposition kurdes iraniens  dans le collimateur de Téhéran. Que représente cet accord ?
==================================================================================================================================================================================================================

L’Iran et l’Irak ont signé dimanche 19 mars 2023 un accord pour « protéger
la frontière », où sont basés des groupes d’opposition kurdes iraniens
dans le collimateur de Téhéran. Que représente cet accord ?

Cet accord, s’il est appliqué, peut marquer un tournant.

Le Bashur (Kurdistan d’Irak) a toujours représenté un refuge pour les
opposants politiques iraniens, kurdes et non-kurdes (lire ici notre reportage).

Téhéran a toujours fait pression sur l’Irak pour que cela cesse.

Avec ce nouvel accord, le régime iranien obtient l’engagement de l’État
irakien de ne pas accueillir sur son sol des partis d’opposition iraniens
dans la région autonome du Kurdistan.

Concrètement, cela constitue une vraie menace pour l’opposition iranienne.

Comme ce fut le cas dans le passé pour l’Organisation des moudjahidine
du peuple d’Iran (OMPI), qui prônait le renversement du régime des mollahs
et était protégé en Irak par Saddam Hussein, ennemi juré de Téhéran à l’époque.


Le Kurdistan d’Irak apparaît aussi comme une terre d’émancipation ?
=======================================================================

Oui, c’est vrai qu’il y a plus de liberté, comparé à la dictature iranienne
mais cette liberté reste limitée pour les femmes.

Pourtant, demeure en Occident l’idée reçue que les femmes kurdes seraient
plus libres que leurs voisines arabes, perses, ou turques…

On aime homogénéiser les communautés qu’on connaît mal.

L’Occident aime marquer une dualité entre les femmes qui s’engagent dans
la lutte armée et celles qui sont opprimées dans leur quotidien.
Mais ce n’est pas blanc ou noir.

La femme kurde n’existe pas pour moi.

De qui parle-t-on ? De la femme issue des classes populaires ou de celle
qui est proche des organisations politiques ?

D’une femme émancipée économiquement qui travaille ou d’une femme opprimée
qui subit des violences ?


À Soulemaynieh, ville considérée comme la plus ouverte du Kurdistan irakien,
une femme a été attaquée par un groupe d’hommes qui estimaient que sa tenue
n’était pas conforme.
La société kurde est encore très patriarcale.

Au Kurdistan iranien, il y a toujours eu une dynamique féministe.

D’ailleurs, c’est de là que part la révolte après la mort de Jina Mahsa Amini.

En septembre 2022, peu avant son assassinat, des manifestations ont secoué
la ville de Marivan pour dénoncer la mort de Shelir Rasouli, une mère de
famille qui s’est défenestrée pour échapper à un viol.

Les actions sur les questions de genre sont réelles dans cette région
mais peu audibles car la région est très peu connue et très peu médiatisée.

Les femmes sont présentes dans les grèves, les rassemblements, on y entonne
des slogans contre le patriarcat aux côtés des slogans contre le régime
et le capitalisme.


L’apport de la jîneologî
==============================

Vous avez justement réalisé une thèse sur les femmes kurdes combattantes
dans le Rojava et le Bakur où vous retracez une histoire du PKK (Parti
des travailleurs du Kurdistan) au prisme du genre.
Vous revenez notamment sur l’apport de la jîneologî, la science kurde de
la libération des femmes, aux études féministes, notamment dans les pays
en guerre et en proie à des conflits ethniques. Quel est-il ?

Ma thèse problématise la participation politico-armée des femmes dans une
société patriarcale et leur militantisme pour la reconnaissance de la
kurdicité et pour la cause des femmes.
Outre leur participation politique et armée à cette lutte, les femmes
construisent un féminisme local nommé Jîneolojî dont les causes et les
apports, ainsi que les limites dans le contexte des pays du Sud, ont
été discutés.

En fait, les principales questions de cette thèse sont liées à un projet
politique expérimenté à Bakur appelé « confédéralisme démocratique »,
dans lequel la question du genre occupe une place importante.

L’idéologie égalitaire de ce projet et la volonté de le réaliser au maximum
ont renforcé la présence des femmes en politique.
D’une part, une telle participation active a provoqué la rupture et
l’affaiblissement des structures rigides de genre dominantes, et d’autre
part, elle a conduit à la féminisation de l’ensemble de la société et
de ses processus collectifs.

La poursuite de ce projet après 2013 au Rojava a provoqué un changement
dans l’imaginaire collectif kurde sur les femmes et les rôles qu’elles
peuvent jouer.
Par conséquent, on est témoin d’un impact majeur sur la modification des
rapports sociaux de genre, de sorte qu’il a été capable de dépasser ses
frontières et devenir une source d’inspiration tant pour la diaspora que
pour les militantes féministes kurdes d’Iran et d’Irak.

Produire un  savoir indigène féminin
=========================================

Votre thèse aborde l’ensemble de ces tentatives positives en les mettant
en relation avec les efforts déployés au sein du PKK pour produire un
savoir indigène féminin, mais elle aborde également les contradictions
et les limites. Quelles sont-elles ?

Par exemple, pourquoi les rapports sociaux de genre ont beaucoup moins
évolué que les rapports de pouvoir politique dans le champ de la représentation,
pourquoi certaines questions comme le contrôle du corps et de la sexualité
ou l’avortement ont été très peu abordées, pourquoi il y a des ambiguïtés
autour de la question queer.

De la même manière, dans le domaine de la production de savoir, cette thèse
affirme que la Jineolojî, dans le cadre du capitalisme mondialisé, se
heurte à d’importantes limitations théoriques et ontologiques malgré ses
acquis, notamment en transformant les savoirs féminins d’une sphère
académique élitiste vers la vie quotidienne des femmes de la classe populaire
avec leur propre participation.

En effet, c’est un type de féminisme kurde dans lequel la « femme kurde »
ne se situe pas et reste un concept homogène sans tenir compte des
différences qui existent entre elles-mêmes.

Par exemple toutes les femmes kurdes sont considérées comme des patriotes
et des défenseurs de la kurdicité.

Cela tient aussi au fait que la Jineolojî, qui se définit par opposition
au féminisme occidental dominant, censé être nécessairement colonialiste,
capitaliste et orientaliste, peut mobiliser les femmes kurdes comme une
unité, afin de leur rendre subjectivité et confiance en soi.

Ce faisant, la Jineolojî glisse parfois vers un localisme idéalisé,
irréaliste et culturalisé qui n’est pas capable de voir les différences
et les contradictions qui traversent la société kurde et qui divisent
également les femmes kurdes.

Cependant, la contribution critique du projet politique des femmes du
PKK aux savoirs et pratiques des féminismes au niveau national, régional
et international est indéniable et comporte de nombreuses nouveautés.

par Rachida El Azzouzi



