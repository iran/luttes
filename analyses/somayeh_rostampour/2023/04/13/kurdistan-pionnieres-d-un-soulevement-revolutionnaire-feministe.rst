
.. _somayeh_rostampour_2023_04_13:
.. _kurdistan_2023_04_13:

======================================================================================
2023-04-13 **Kurdistan : Pionnières d’un soulèvement révolutionnaire féministe**
======================================================================================

- https://www.unioncommunistelibertaire.org/?Kurdistan-Pionnieres-d-un-soulevement-revolutionnaire-feministe


Introduction
============

Le 16 septembre 2022, jour de l’assassinat de Jina (Mahsa) Amini, marquait
le début d’une période révolutionnaire en Iran.

Alors que la contestation se poursuit, Somayeh Rostampour, doctorante à
l’université Paris-8 revient pour Alternative Libertaire sur les origines
du mouvement et son orientation profondément féministe et intersectionnelle.

Jina (Mahsa) Amini, jeune femme de 22 ans d’origine kurde, tuée par la
police des moeurs pour « tenue indécente », cristallisait simultanément
plusieurs oppressions dans son identité.

De ce fait, la récente révolte se distinguait des précédentes par une
intersection de la classe, de l’ethnicité et du genre.

Ses funérailles se sont transformées en une manifestation publique
protestataire avec le slogan « Femme, vie, liberté », inspiré par la
lutte des femmes kurdes au Rojava.
Il était scandé pour la première fois, ce jour-là, par les habitants en
colère de Saqqez, sa ville natale au Kurdistan, qui sont venus courageusement
en ce matin historique pour contrecarrer le projet du gouvernement
d’enterrer secrètement Jina.

Ce slogan est l’héritage du mouvement des femmes kurdes de Turquie, une
région connue des Kurdes sous le nom de Bakur, fortement influencée par
la philosophie politique proposée par le fondateur et leader charismatique
du Parti des travailleurs du Kurdistan (PKK), Abdullah Öcalan.

Depuis 2013, ce slogan est réutilisé au Rojava puis dans d’autres régions
du Kurdistan et même dans de nombreuses villes d’Amérique latine, d’Europe
et des États-Unis.

Les femmes pro-PKK (à la fois guérillas et militantes politiques civiles)
ont été les sujets qui ont progressivement fait de « Jin, Jiyan, Azadi »
le slogan le plus central de ce mouvement en amenant une vision
intersectionnelle : à la fois contre le gouvernement mais aussi contre
le patriarcat capitaliste local et même de leur organisation.

Cela facilite également le voyage transfrontière du slogan.


Un soulèvement intersectionnel
=================================

En contraste direct avec la structure dominante masculine meurtrière et
répressive de la République islamique, qui a refusé toute forme de liberté
à divers groupes, en particulier aux femmes et aux queers, aux militant·es
ethniques ou environnementaux, ou à d’autres groupes marginalisés comme
les travailleurs, « Jin, Jiyan, Azadi » agit comme une alternative
unificatrice qui englobe les oppressions plurielles.

La société iranienne a mis du temps à accepter que l’oppression sexiste
et ethnique ne soit pas seulement le problème des concerné·es, mais une
nécessité absolue pour une démocratie basée sur la justice sociale dans
tout le pays.

Se débarrasser de l’oppression de classe est profondément dépendant de
la résolution simultanée d’autres formes d’oppression qui ont rendu
certaines personnes, dont les Kurdes, « minoritaires » voire « inférieurs ».

Le soulèvement révolutionnaire de Jina a pu visibiliser ces fractions et
ainsi en faire un sujet principal : les périphéries marginalisées deviennent
le centre du soulèvement.

Il ne faut pas oublier que pendant des années, les forces de gauche en
Iran ont non seulement ignoré la « question kurde » et plus largement
les revendications des minorités nationales pour le droit à l’autodétermination,
mais elles ont également nié l’importance du féminisme ou la nécessité
d’en faire une priorité.

Dans le même temps, les nationalistes kurdes ont également essayé d’alimenter
le mythe selon lequel le patriarcat n’existe pas au Kurdistan, et que
s’il y a de la violence, elle est principalement enracinée dans l’oppression
de l’État central colonialiste, qui nie l’existence des Kurdes.

C’est bien l’aspect intersectionnel du soulèvement récent qui change
le discours masculiniste dominant en faveur d’une narration féministe.

Des réalités transfrontalières
====================================

Sur ce point également, il existe des similitudes entre la situation de
l’Iran et celle de la Turquie, provoquant aussi des révoltes dans les
deux cas : la vie quotidienne et les espaces privés sous le contrôle du
dirigeant oppresseur patriarcal sont en crise dans la forme d’un état
d’urgence permanent et les femmes sont les pionnières du changement parce
qu’elles sont les premières victimes.

Il est donc possible de retracer les luttes des femmes kurdes de manière
transfrontalière.

C’est notamment l’autoritarisme de l’État qui a créé une condition
sociopolitique similaire pour les Kurdes dans deux pays.

Avec l’établissement du Parti de la justice et du développement (AKP) et
la tentative d’islamiser les domaines liés au genre, cette similitude
s’est accrue de jour en jour.


Une évolution politique de longue date
===========================================

**Dès le début du soulèvement révolutionnaire en Iran, les femmes kurdes
ont joué un rôle très important**.

Elles en ont payé le prix : au moins cinq femmes kurdes ont été tuées par
les forces d’oppressions du régime, des centaines blessées et arrêtées.

Ces résistances et la magnifique performance des femmes kurdes le jour
des funérailles de Jina au Kurdistan (point de départ du soulèvement) en
agitant leurs foulards et en transformant le symbole de l’oppression
étatique en drapeau de la lutte féministe sont de la même manière le
résultat d’une tradition organisationnelle à Rojhelat (Kurdistan situé en Iran),
transmise de génération en génération en dépit de la répression brutale.

Les graines de la lutte pour l’émancipation qui ont été plantées en 1979,
à l’occasion d’un processus qui est resté inachevé, ont germé aujourd’hui
quatre décennies plus tard au Kurdistan et ont bénéficié à l’ensemble de l’Iran.

Conséquences de la marginalisation politico-économique, en Iran comme en
Turquie, les actes d’oppression nationale du gouvernement central sur le
peuple kurde ont entraîné des réactions collectives sous la forme de
divers mouvements avec une hégémonie des nationalistes kurdes et des
socialistes.

Dans les deux cas, le militantisme de ces forces organisées, qui ont
notamment émergé dans le vide du pouvoir et l’ouverture politique causés
par la chute du régime Pahlavi en 1979, ont ouvert la voie à la présence
active des femmes kurdes en politique.

Certains mouvements, comme le parti maoïste de Komala (1979-1991), bien
que de manière minime mais pionnière, ont également fourni une plate-forme
pour que les femmes de Rojhelat puissent entrer dans le champs politique
ou armé (même avant le PKK).

Et, à l’occasion de la manifestation du 8 mars 1979, plusieurs milliers
des femmes à Sanandaj, Marivan ou Kermanshah ont protesté contre le hijab
obligatoire avec les slogans comme « Pas de foulard, pas d’humiliation, mort à la dictature »,
« Nous n’avons pas fait de révolution pour revenir en arrière ».

Le noyau responsable de la préparation réussie de cette manifestation a
créé peu après, au début de l’année 1980, le Conseil des femmes de Sanandaj
dont les membres étaient principalement issues de diverses tendances de
la gauche.
Ces activités ont jeté les bases de la tradition progressiste et radicale
du 8 mars au Rojhelat, qui se déroule encore aujourd’hui de manière
continue et sous différentes formes.

Historiquement, les hommes ont été les principaux acteurs des mouvements
nationalistes kurdes selon une vision patriarcale d’après laquelle la
patrie était considérée comme une femme que les hommes devaient protéger
en tant que leur propriété.

Mais, avec la croissance des mouvements radicaux et socialistes au Kurdistan,
et, en particulier, au cours des deux ou trois dernières décennies, ce
discours a été progressivement marginalisé et remplacé par des idées
égalitaires et progressistes.
Cette nouvelle position politique se construit au coeur de plusieurs champs
d’oppression et d’exploitation intersectionnels auxquels les femmes font
face : avec le patriarcat des hommes kurdes et non-kurdes, le fondamentalisme
et l’oppression structurelle imposée par le régime en place, les chauvins
iraniens et le féminisme centriste (souvent nationaliste), ainsi que
l’homophobie et le racisme.

Grâce à ces efforts, **les femmes sont devenues l’un des principaux piliers
de la lutte et de la résistance du mouvement révolutionnaire du Kurdistan
comme en témoigne le soulèvement de Jina**.

Somayeh Rostampour

