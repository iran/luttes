.. index::
   pair: Shervin ; Hajipour
   pair: Shervin Hajipour ; Pour ma sœur, ta sœur, nos sœurs
   pair: Shervin Hajipour ; Barâyé
   ! Shervin Hajipour
   ! Pour
   ! Barâyé



.. figure:: images/shervin_hajipour_wiki.png
   :align: center

   https://en.wikipedia.org/wiki/Shervin_Hajipour

.. _pour_ma_soeur:
.. _shervin_hajipour:
.. _baraye:

========================================================================
**La chanson Barâyé de Shervin Hajipour** (en Français : **Pour** ...)
========================================================================

- https://youtu.be/FAeNCddonTc
- https://www.instagram.com/tv/CjISL5-oad7/?utm_source=ig_web_copy_link


.. _baraye_2023_02_05:

2023-02-05 Grammy de la meilleure chanson pour le changement social
========================================================================

- https://twitter.com/LettresTeheran/status/1622448963767996416#m

La chanson « Baraye » de @HajipourShervin devenue l’hymne du soulèvement
en Iran a gagné le Grammy de la meilleure chanson pour le changement social.

- https://twitter.com/LettresTeheran/status/1622458997151346689#m

Silencieux sur les réseaux sociaux depuis son arrestation et sa libération
Shervin Hajipour a réagi à cette victoire sur Instagram en publiant
simplement :« Nous avons gagné »


En français + translittération du persan
===========================================

- https://www.orion-hub.fr/w/k4UhmVNffGEHjM67UAQdmE


برای توی کوچه رقصیدن

::

    Barâyé touyé koutché raghsidan
    Pour danser dans la rue

برای ترسیدن به وقت  بوسیدن

::

    Barâýé tarsidan bé vaghté boussidan
    Pour la peur lors de s’embrasser

برای خواهرم خواهرت خو اهرامون

::

    Barâýé khaharam khaharet khaharamoun
    Pour ma sœur, ta sœur, nos sœurs

برای تغییر مغز ها که پوسیدن

::

    Barâýé taaghiiré maghzha keh poussidan
    Pour changer une mentalité pourrie

برای شرمندگی برای بی پولی

::

    Barâýé chamandégui Barâýé bi pouli
    Pour la honte du manque d’argent

برای حسرت یک زندگی معمولی

::

    Barâýé hsraté yek zéndéguié maaouli
    Pour le regret de ne pas avoir une vie normale

برای کودک زباله گرد و آرزوهاش

::

    Barâýé koudaké zobalégard o ârézougach
    Pour l’enfant qui cherche dans les ordures et pour ses rêves

برای این اقتصاد دستوری

::

    Barâýé în éghtéssadé destouri
    Pour cette économie imposée.

برای این هوای آلوده

::

    Barâýé in havayé âloudé
    Pour cet air pollué

برای ولی عصر و درختای فرسوده

::

    Barâýé valuasr o dérakhtâyé farssoudeh
    Pour l’avenue Vali-Asr et ses arbres sénescents

برای پیروز و احتمال انقراضش

::

    Barâýé pirouz oehtémalé énghérazech
    Pour le guépard en voie probable d’extinction

برای سگهای بی گناه ممنوعه

::

    Barâýé sag hayé bi gonahé mamnouéé
    Pour les chiens innocents interdits


برای گریه های بی وقفه

::

    Barâýé gueryé hayé bi vaghfeh
    Pour les pleurs ininterrompus

برای تکرار تصویر این لحظه

::

    Barâýé tekrâré tasviré in lahzeh
    Pour l’image répétée de cet instant

برای چهره ای که می خنده

::

    Barâýé tchehreh ii keh mikhandeh
    Pour un visage qui sourit


برای دانش آموزان برای آینده

::

    Barâýé danesamouzan barâýé âyandeh
    Pour les élèves, pour l’avenir

برای این بهشت اجباری

::

    Barâýé in behechté éjbari
    Pour ce paradis obligatoire

برای نخبه های زندان ی

::

    Barâýé nokhbeh hayé zéndàni
    Pour les étudiants brillants emprisonnés

برای کودکان افغانی

::

    Barâýé koudakâné afghani
    Pour les enfants afghans

برای این همه برای غیر  تکراری

::

    Barâýé in hameh Barâ haýé gheiré tekrâri
    Pour tous ces « pour » à l’infini

برای این همه شعارهای تو خالی

::

    Barâýé in hameh choâârhayé tou khâli
    Pour tous ces slogans vides

برای آوار خانه های پوشالی

::

    Barâýé âvâré khounehàyé pouchâli
    Pour les ruines des maisons mal construites

برای احساس آرامش

::

    Barâýé éhsassé ârâmech
    Pour le sentiment de sérénité

برای خورشید پس از شبای طولانی

::

    Barâýé khorchid pass az chabayé toulâni
    Pour la lumière qui revient après ces longues nuits

برای قرص های اعصاب و بی  خوابی

::

    Barâýé hors hayé aassâb o bi khâbi
    Pour les tranquillisants et les somnifères


برای مرد میهن آبادی

::

    Barâyé mard mihan âzâdi
    Pour l’homme, la patrie, la prospérité


برای دختری که آرزو داشت پسر بود

::

    Barâýé dokhtari ké ârézou dacht pessar boud
    Pour la fille qui voulait être un garçon

برای زن زندگی آزادی

::

    Barâyé zan zéndégui âzâdi
    Pour la femme, la vie, la liberté


برای آزادی

::

    Barâýé âzâdi
    Pour la liberté

برای آزادی

::

    Barâýé âzâdi
    Pour la liberté

برای آزادی

::

    Barâýé âzâdi
    Pour la liberté


Dessin de Bahraeh Akrami
===============================

- :ref:`akrami_iran:baraye_2023_02_06`

.. figure:: images/traduction_francais_akrami.png
   :align: center


Voici la chanson « Pour… » de Shervin traduit en 🇬🇧🇫🇷🇩🇪
===========================================================

Avec des dizaines de millions de vues en très peu de temps, ses paroles
qui évoquent les maux de la société iranienne sont devenues rapidement
l’hymne des protestations en #Iran.


.. figure:: images/traduction_3_langues.png

   https://twitter.com/FaridVahiid/status/1578674534399741952?s=20&t=CCtpOSsfgp2-N1AijyluOA
