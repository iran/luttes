.. index::
   ! pair: Yashgin Kiyani ; Bella Ciao
   ! Yashgin Kiyani
   ! Bella Ciao

.. _bella_ciao:

====================================
**Bella Ciao (Ma Belle Au Revoir)**
====================================

- https://fr.wikipedia.org/wiki/Bella_ciao_(chant)


Définition wikipedia en français
==================================

Bella ciao est un chant de révolte italien qui célèbre l'engagement dans
le combat mené par les partisans, résistants pendant la Seconde Guerre
mondiale opposés aux troupes allemandes alliées de la République sociale
italienne fasciste, dans le cadre de la guerre civile italienne.

Les paroles ont été écrites fin 1944 sur la musique d'une chanson populaire
que chantaient au début du XXe siècle les mondine, ces saisonnières qui
désherbaient les rizières de la plaine du Pô et repiquaient le riz,
pour dénoncer leurs conditions de travail.

**Ce chant est devenu un hymne à la résistance dans le monde entier**.


Définition 2
================

Pour rappel, à l’origine, Bella Ciao est un chant ouvrier entonné par les
“mondines” ces Italiennes qui travaillaient dans les rizières à la fin
du XIXe siècle et dénonçaient leurs conditions de travail misérables.

**Lors de la Seconde guerre mondiale, Bella Ciao est ensuite devenu un
chant antifasciste, un hymne à la Résistance**.

Popularisé auprès des jeunes générations par la série Casa De Papel,
Bella Ciao reprend ces jours-ci une couleur politique et militante
grâce à cette vidéo poignante.


.. _yashgin_kiyani:

🇮🇷 **Beautiful cover of Bella Ciao** in Persian by an Iranian woman **Yashgin Kiyani**
===========================================================================================

- https://www.youtube.com/watch?v=k0UAXdsEl5k
- https://www.youtube.com/watch?v=UEicFJMd9xI&list=OLAK5uy_kCBR-lD6IE1eepdAS0_W2cXDHdadfPIkw


.. figure:: images/bella_ciao_yashgin.png
   :align: center

   https://www.youtube.com/watch?v=k0UAXdsEl5k


.. figure:: images/yashgin_kiyani.png
   :align: center

   https://www.youtube.com/watch?v=UEicFJMd9xI&list=OLAK5uy_kCBR-lD6IE1eepdAS0_W2cXDHdadfPIkw


::

    Az Galooye   tu (From your throat)
    Taa sedaaye maa  (To our voices)
    Bella Ciao, Bella Ciao, Bella Ciao, Ciao Ciao

    Meepareem az khaab ye shabe mahtaab,
    (We wake up from a moonlight night)
    Yeki meege 'Ay Aadamhaa'
    (Someone calls out 'Hey All Humanity'

    Yaa hame ba ham, yaa hame tanhay
    (Either all of us together or all of us individually)
    Maa ke bidaarim taa Fardaa
    (As for us we are awake till tomorrow)

    Khakeh in gandum, too kheeyabooneh
    (The wheat dust are in the streets)
    Khoosheye khashme manoto teshneye barooneh
    (The cluster of wrath of me and you thirsts for Rain)

    Haghe maa kam nist
    (We deserve more than this)
    Zaanooye gham nist
    (we don't kneel in sadness)

    Ghalbe maa ke dour az ham nist
    (Our hearts are not distanced from each other)
    Donyaaye taare, In ye aaghaaze
    (This is a new beginning in the dark world)

    Panjere taa royaa baazeh
    (The window is open to dreams)


::

        بلا چاو  فارسی

        خاک این گندم
        تو خیابونه
        خوشه خشم من و تو تشنه  بارونه
        حق ما کم نیست
        زانوی غم نیست
        قلب  ما که دور از هم نیست
        دنیای تازه
        این یه آوازه
        پنجره تا رویا بازه
        یا همه با هم
        یا همه تنها
        او بلا چاو بلا چاو بلا چاو چاو چاو
        میپرسم از خواب
        یک شب مهتاب
        ما که بیداریم تا فردا
        آخرش زنجیر
        ظلم آدم گیر



En italien
============


::

    “Bella Ciao
    (Ma Belle Au Revoir)

    Una matina, mi sono svegliato
    Un matin, je me suis reveillé
    O bella ciao (x3)
    Ô ma belle au revoir (x3)
    Ciao, ciao
    Au revoir, au revoir

    Una matina, mi sono svegliato
    Un matin, je me suis réveillé
    E ho trovato l’invasore.
    Et l’envahisseur était là.
    O ! Partigiano portami via
    Ô ! partisan emporte-moi
    O bella ciao (x3)
    Ô ma belle au revoir (x3)
    Ciao, ciao
    Au revoir, au revoir

    O ! Partigiano portami via
    Ô ! Partisan emporte-moi
    Che mi sento di morir’.
    Je me sens prêt à mourir.
    E se muoio, da partigiano
    Et si je meurs en tant que partisan
    O bella ciao (x3)
    Ô ma belle au revoir (x3)
    Ciao, ciao
    Au revoir, au revoir

    E sei muoio, da partigiano
    Et si je meurs en tant que partisan
    Tu mi devi seppellir’.
    Tu devras m’enterrer.
    Mi lassu in montagna
    m’enterrer là-haut sur la montagne
    O bella ciao (x3)
    Ô ma belle au revoir (x3)
    Ciao, ciao
    Au revoir, au revoir

    Mi seppellire lassu in montagna
    m’enterrer là-haut sur la montagne
    Sotto l’ombra di un bel’ fior’.
    A l’ombre d’une belle fleur.
    E le genti, che passeranno
    Et les gens, ils passeront
    O bella ciao (x3)
    Ô ma belle au revoir (x3)
    Ciao, ciao
    Au revoir, au revoir

    E le genti, che passeranno
    Et les gens, ils passeront
    E diranno ‘Oh che bel fior’.
    Et diront ‘Oh quelle belle fleur’.
    E questo è il fiore Del partigiano
    Cette fleur est la fleur du partisan
    O bella ciao (x3)
    Ô ma belle au revoir (x3)
    Ciao, ciao
    Au revoir, au revoir

    E questo è il fiore Del partigiano
    Cette fleur est la fleur du partisan
    Morto per la libertà.
    Mort pour la liberté.”


.. _baboo_2023_04_29:

Interprétation bella ciao en italien avril 2023
====================================================

- https://www.instagram.com/reels/audio/608131254577072/
- https://www.instagram.com/reel/CrnyarWA22z/?igshid=YmMyMTA2M2Y%3D
- https://nitter.nicfab.eu/Baboobabounette/status/1652311851437285381#m

.. figure:: images/dessin_baboonette.png
   :align: center

