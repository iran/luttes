.. index::
   ! Chansons

.. _chansons_iran:

=====================
🎶 Chants 🎶
=====================

- https://open.spotify.com/playlist/2IqitkyYX5u3AQLb2FCsbZ
- https://radiooooo.com/?share=644bdb4bfa42b2786e0f568d (:ref:`chants_2023_05_07`)

.. toctree::
   :maxdepth: 3

   baraye/baraye
   bella_ciao/bella_ciao
   femme_vie_liberte/femme_vie_liberte
   hommage_jina/hommage_jina
   la-potence/la-potence
   l-estaca/l-estaca   
   sar-oomad-zemestoon/sar-oomad-zemestoon   
   2023/2023


