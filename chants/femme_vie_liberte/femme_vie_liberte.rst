.. index::
   pair: Femme, Vie Liberté  ; clip
   pair: Chanteur ; HunergehaWelat


.. _femme_vie_liberte_clip:

==============================================================================
1 clip féministe avec le refrain "Femme, Vie Liberté & "mort au dictateur"
==============================================================================

- https://www.youtube.com/watch?v=gCmpiJKqu4I  ( "Femme, Vie Liberté & "Mort au dictateur")
- :ref:`merg_ber_dictator`
- :ref:`jin_jiyan_azadi`


.. figure:: images/annonce_kurdistan.png
   :align: center

   https://twitter.com/KurdistanAu/status/1575945023161499658?s=20&t=SOLQjz3RPJY6QSR6ll2EWw


.. figure:: images/extrait_clip_2.png
   :align: center

   https://youtu.be/gCmpiJKqu4I?t=54




ROJAVA. En solidarité avec la révolte populaire en Iran, ls musiciens kurdes de @HunergehaWelat (https://twitter.com/HunergehaWelat)
 sortent 1 clip féministe avec le refrain "femme, vie liberté & "mort au dictateur"

#JinaAmini
#MahsaAmini #IranProtests #TwitterKurds #EndIranRegime
#مهسا_امینی #زن_زندگی_آزادی


