.. index::
   pair: Hommage ; Jina Mahsa Amini

.. _clip_hommage_jina:

==========================================================================================================================
**Le groupe féminin kurde, Hedar Rûbar** a sorti 1 clip en hommage à Jina Amini & à la révolte féminine en Iran
==========================================================================================================================


- https://www.youtube.com/watch?v=rt_imL4y820


.. figure:: images/jina_amini_clip.png
   :width: 300


.. figure:: images/clip_hommage_jina.png
   :width: 300

   https://twitter.com/KurdistanAu/status/1579509737808613376?s=20&t=_rCZjgIHvxGb4q3z_3YN9g


Le groupe féminin kurde, Hedar Rûbar a sorti 1 clip en hommage à Jina
Amini & à la révolte féminine en Iran

lien https://www.youtube.com/watch?v=rt_imL4y820

#EndIranRegime #LetUsTalk #TwitterKurds #مهسا_امینی #MahsaAmini #opiran
#IranRevolution2022 #IranProrests2022 #JinJiyanAzadi #SayHerName
