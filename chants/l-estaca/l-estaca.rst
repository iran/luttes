.. index::
   ! pair: Chant ; L'estaca
   ! L'estaca

.. _l_estaca:

===================================================================================================
**L'estaca** "le pieu" est une chanson catalane composée par le chanteur Lluís Llach en 1968
===================================================================================================

- https://fr.wikipedia.org/wiki/L%27Estaca
- http://www.lluisllach.fr/chansons/lluis-llach-l-estaca/

L'Estaca (trad. litt. : "le pieu") est une chanson catalane composée par 
le chanteur Lluís Llach en 1968.

Composée durant la dictature du général Franco en Espagne, **c'est un cri à 
l'unité d'action pour se libérer de l'oppression, pour atteindre la liberté**. 

D'abord symbole de la lutte contre l'oppression franquiste en Catalogne, 
elle est devenue un symbole de la lutte pour la liberté.

Extrêmement populaire en Catalogne aujourd'hui, au point d'être considérée 
comme partie du folklore populaire, elle a aussi connu un destin international. 

Elle a eu plusieurs interprétations différentes et a été traduite en plus 
de cinquante langues — français, occitan, basque, corse, allemand, polonais, 
espéranto, etc. 


Paroles
===============

Les paroles évoquent, en prenant la métaphore d'une corde attachée à un pieu 
(estaca en catalan), le combat des hommes pour la liberté. 

La scène se passe à l'aube, tandis que le narrateur de la chanson se remémore 
les paroles d'une conversation entre grand-père Siset (avi Siset) et lui. 

Il demande au grand-père Siset : 

**Ne voyez-vous pas le pieu auquel nous  sommes tous liés ? 
Si nous ne pouvons pas nous en défaire, nous ne pourrons jamais avancer** 

::

    No veus l'estaca a on estem tots lligats? 
    Si no podem desfer-la mai no podrem caminar 

D'après Siset, seule une action commune peut apporter la liberté : 

**Si nous tirons tous, il va tomber, si je tire fort vers ici, et que tu tires 
fort par là, il est certain qu'il tombe, tombe, tombe, et nous pourrons nous 
libérer** 

::

     Si estirem tots, ella caurà, si jo estiro fort per aquí i tu l'estires 
     fort per allà, segur que tomba, tomba, tomba, i ens podrem alliberar

L'interlocuteur de grand-père Siset insiste sur la difficulté du combat pour 
la liberté, qui ne demande pas de répit et des efforts : 

**Mais, Siset, ça fait longtemps déjà, mes mains à vif sont écorchées, et 
alors que mes forces me quittent, il est plus large et plus haut**

::

    Però, Siset, fa molt temps ja, les mans se'm van escorxant, i quan la força 
    se me'n va ella és més ampla i més gran).

L'idée d'une nécessaire prise de conscience collective pour obtenir la liberté 
clôt la chanson. 

Dans la dernière strophe, une fois grand-père Siset mort, son interlocuteur 
devient responsable de la diffusion des idées de liberté et de lutte auprès 
des nouvelles générations : 

**Et quand passent d'autres valets, je lève la tête pour chanter le dernier 
chant de Siset, le dernier qu'il m'ait appris**

::

    I mentre passen els nous vailets, estiro el coll per cantar el darrer 
    cant d'en Siset, el darrer que em va ensenyar. 



Paroles en français
==========================

::

    L’estaca (Le pieu)

    Le grand-père Siset me parlait ainsi
    Tôt le matin au portail
    tandis qu’attendant le soleil,
    nous regardions passer les charettes
    Siset, ne voit tu pas le pieu
    On nous sommes tous attachés,
    Si nous ne nous détachons pas
    Jamais nous ne pourrons nous libérer…
    Si nous tirons tous il tombera
    Et il ne peut plus tenir trés longtemps
    Sûr qu’il tombera , tombera, tombera,
    Bien vermoulu comme il doît être déjà.

    Si je tire fort de mon côté,
    Et que tu tires fort du tien,
    Sûr qu’il tombera, tombera, tombera,
    Et nous pourrons nous délivrer.
    Mais, Siset, il y a longtemps déjà
    que l’on s’ écorche les mains
    Et quand la force m’abandonne
    Il semble bien plus large et plus grand qu’avant.

    Certainement qu’il est tout pourri,
    Pourtant, Siset, il pèse tant!
    Et parfois la force me manque.
    Alors, chante moi encore ta chanson!
    Si je tire fort de mon côté,
    Et que tu tires fort du tien,
    Sûr qu’il tombera, tombera, tombera,
    Et nous pourrons nous délivrer.
    On n’entend plus le vieux Siset
    Un mauvais vent l’a emporté.
    Qui sait où il est passé?
    Et je reste seul au portail.
    Et quand passent des jeunes,
    Je tends le cou pour chanter
    Le dernier chant de Siset
    Le dernier qu’il m’ait appris.
    Si je tire fort de mon côté,
    Et que tu tires fort du tien,
    Sûr qu’il tombera, tombera, tombera,
    Et nous pourrons nous délivrer.

Paroles en catalan
========================

::

    L’avi Siset em parlava
    de bon matí al portal
    mentre el sol esperàvem
    i els carros vèiem passar.

    Siset, que no veus l’estaca
    on estem tots lligats?
    Si no podem desfer-nos-en
    mai no podrem caminar!

    Si estirem tots, ella caurà
    i molt de temps no pot durar,
    segur que tomba, tomba, tomba
    ben corcada deu ser ja.

    Si jo l’estiro fort per aquí
    i tu l’estires fort per allà,
    segur que tomba, tomba, tomba,
    i ens podrem alliberar.

    Però, Siset, fa molt temps ja,
    les mans se’m van escorxant,
    i quan la força se me’n va
    ella és més ampla i més gran.

    Ben cert sé que està podrida
    però és que, Siset, pesa tant,
    que a cops la força m’oblida.
    Torna’m a dir el teu cant:

    L’avi Siset ja no diu res,
    mal vent que se l’emportà,
    ell qui sap cap a quin indret
    i jo a sota el portal.

    I mentre passen els nous vailets
    estiro el coll per cantar
    el darrer cant d’en Siset,
    el darrer que em va ensenyar.

Versions
==============

La chanson L'Estaca connaît, depuis sa création, une longue histoire de 
traductions et de réinterprétations. 

Elle a notamment été traduite en "corse, en basque, en breton, en occitan, 
en niçois, et reprise par de nombreux artistes, comme le collectif musical 
réuni autour du groupe Zebda sur l'album Motivés". 


Le magazine musical catalan Enderrock, qui a publié un grand reportage sur 
l'aventure de la célèbre chanson de Llach, recense pas moins de 250 versions 
différentes, dans plus de dix-sept langues différentes. 

Symbole de la lutte contre les pouvoirs autoritaires, elle a été chantée dans 
de nombreux mouvements de contestation. 

Le syndicat Solidarność fait de la version polonaise Mury, écrite et interprétée 
par Jacek Kaczmarski, son hymne contre le régime communiste de la République 
Populaire de Pologne. 

Cette traduction a servi de modèle à une traduction en biélorusse qui accompagne, 
en 2020, les manifestations contre le "président-dictateur" Alexandre Loukachenko. 

En arabe dialectal tunisien, c'est la version Dima Dima, écrite par Yasser Jradi, 
qui est interprétée par la chanteuse Emel Mathlouthi (qui représenta la Tunisie 
lors de la remise du Prix Nobel de la Paix aux dirigeants de la transition 
démocratique tunisienne) lors de manifestations de rue pendant la Révolution 
de Jasmin. 

La version russe, Steny (Стены, Les murs, 2012) interprétée par le groupe 
Arkadiy Kots lors de rassemblements anti-Poutine, a connu un succès retentissant 
en Russie à la suite de l'arrestation des membres du groupe.

La chanson a été adaptée en français par Marc Robine sous le titre Le Pieu 
(L'Estaque) en 1999. 
