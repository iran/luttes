.. index::
   pair: La potence ; Toomaj Salehi

.. _potence_toomaj:

======================================
**La potence** de Toomaj Salehi
======================================

- https://x.com/JJR_JJR_JJR/status/1784196674920812765
- https://www.amnesty.fr/peine-de-mort-et-torture/actualites/iran-2023-executions-record-guerre-contre-la-drogue
- https://www.youtube.com/@ToomajSalehi/videos


#FreeToomaj #StopExecutionsInIran #FemmeVieLiberte

Message des JJR
==================

La dictature iranienne frappe encore l'expression de la liberté et de la révolte.

Rendez-vous ce dimanche à 15h dimanche 28 avril 2024 place de la Bastille
à Paris pour défendre Toomaj Salehi !

.. figure:: images/affiche.webp


Le tribunal révolutionnaire d'Ispahan a condamné mercredi 24 avril 2024 le rappeur
Toomaj Salehi à mort, pour "corruption sur Terre", délit légitimant les
executions de nombreux·ses opposant·es au régime de Khamenei.

Toomaj Salehi a 33 ans, il a été emprisonné dans des conditions inhumaines à
plusieurs reprises pour son soutien au mouvement "Femme, Vie Liberté".
Il a toujours soutenu dans ses textes engagés les manifestations déclenchées
par l'exécution de Mahsa Amini.

Cette condamnation intervient dans un moment où le régime intensifie sa
répression avec 853 personnes executées en Iran en 2023, soit une augmentation
de 48% des condamnations à mort en un an
(voir le `Rapport d'Amnesty International <https://www.amnesty.fr/peine-de-mort-et-torture/actualites/iran-2023-executions-record-guerre-contre-la-drogue>`_ ⤵️

Nous apprenons cette nouvelle avec beaucoup de tristesse, et invitons tous
nos soutiens à rejoindre les rassemblements pour la libération de Toomaj Salehi.
Les conditions d'existence des Iranien·nes, dans cette théocratie autoritaire
qui bride toute liberté et massacre tout élan de vie et de poésie, nous bouleversent,
mais l'espoir de sauver Toomaj de la mort et de la prison ne peut pas quitter nos coeurs.

- https://x.com/JJR_JJR_JJR/status/1784196690318082434

Nous (JJR) souhaitons honorer sa parole et son travail en traduisant des passages
d'une de ses chansons, qui s'appelle طناب دار (Tanab-e-Dar) qu'on pourrait
traduire par "La potence" :


La Potence ار (Tanab-e-Dar)
===============================

.. youtube:: PAR2Uo-vmgY

::

    Ma voix résonne dans la rue,
    si bien que si vous me réduisez au silence,
    ce sera vain

    nous avons semé des millions de graines
    prêtes à jouer leur  rôle,
    à se disséminer sans ombre,
    comme vos poteaux de diffusion,

    vos épouvantails suspendus à leur extrémité,
    chacun d'eux sur les poteaux électriques

    Ni ligne,
    ni tendance,
    ni aile,
    je n'appartiens à aucun parti,
    je suis made in Iran,
    immortel,
    mon cadavre ne vous permettra pas
    de déformer l'histoire

    Même si vous nous tuez,
    même si vous nous coupez en morceaux,
    même si vous nous brûlez,
    nous fleurirons partout,
    je suis un parmi des millions.

    Même si vous m'enflammez,
    si vous me faites fondre
    avec de la chaux, de l'acide,
    même si vous me brisez entièrement,
    même si vous me coupez en morceaux
    et que vous enterrez
    chaque morceau dans le sol,
    je germerai à chaque fois,
    vous ne pourrez même plus me compter

    Le noeud coulant,
    le noeud coulant,
    le noeud coulant,
    je t'embrasserai avec honneur,
    notre voix sera une balle
    dans le cœur de cette forteresse,
    et nous nous reverrons
    de l'autre côté de ce mur

    Camarade, mets la clé ensanglantée
    dans la serrure de la liberté,
    je te prendrai dans mes bras
    avec grand honneur,
    notre voix sera une balle
    dans le cœur de cette forteresse,
    et nous nous reverrons
    de l'autre côté de ce mur.
