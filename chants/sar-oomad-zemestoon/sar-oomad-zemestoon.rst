.. index::
   pair: Chant ; Sar Oomad Zemestoon (L'Hiver Est Fini)
   pair: Chant ; L'Hiver Est Fini (Sar Oomad Zemestoon)
   ! Sar Oomad Zemestoon (L'Hiver Est Fini)

.. _sar_oomad_zemestoun:

============================================================================
🇦🇲 "Sar Oomad Zemestoon" (L'Hiver Est Fini), chanson d'origine arménienne
============================================================================

- https://jinhaagency.com/en/editor-s-pick/song-of-the-day-tara-tiba-sar-oomad-zemestoon-2190
- https://www.youtube.com/watch?v=CGaQx4tRJF8&ab_channel=ziruh%7BEvrenselM%C3%BCzik%7D


**Sar Oomad Zemestoon** (L'hiver est terminé) de la chanteuse iranienne Tara Tiba 
est une très ancienne chanson d'amour arménienne.

La chanson a été popularisée pendant la révolution iranienne de 1979 par les 
Fedayin Khalgh ("Les Sacrifices du Peuple"), un groupe marxiste laïc.

Elle a finalement été appropriée par la révolution dans son ensemble. 
 
Depuis de nombreux chanteurs ont chanté cette chanson. 

Définition de criduchoeur
=================================

- https://criduchoeur.jimdofree.com/r%C3%A9pertoire/sar-omad-zemestoun/contexte/

C'est un hymne de la révolution iranienne de 1979 contre le shah d'Iran. 

La mélodie reprend un chant traditionnel arménien et les paroles réécrites 
en persan sont généralement attribuées au poète Saeed Sultanpour (1940-1981) 
surnommé « le poète de la révolution ». 

Cette chanson a été popularisée par le groupe marxiste Fedayeen Khalgh 
(litt. « Les sacrifices du peuple », l'un des mouvements marxistes les plus 
importants des années 1970 en Iran, composé d'ouvrier·es et d'étudiant·es). 

Les paroles, métaphoriques, relateraient l'opération Siakhal, une opération 
de guérilla ayant eu lieu le 8 février 1971 contre une gendarmerie pour libérer 
un étudiant arrêté pour ses activités militaires ; à la suite de laquelle 
treize guérilleros furent exécutés. 

Malgré la récupération islamiste de la révolution, ce chant est resté un 
hymne à l'espoir et à la liberté et a été repris notamment lors du soulèvement 
du Mouvement vert en 2009. 

Cette révolte, contestant l'élection du parti conservateur, a donné lieu à 
une forte répression policière, de nombreux assassinats et exécutions de 
manifestant·es et l'assignement à résidence du candidat d'opposition. 

La musique traditionnelle étant critiquée par les autorités religieuses, 
elle est devenue en elle-même un élément central de la contestation.



Paroles en français
===============================
 
- https://criduchoeur.jimdofree.com/r%C3%A9pertoire/sar-omad-zemestoun/paroles/#:~:text=Traduction-,l'hiver%20touche%20a%20sa%20fin,-%2C%20le%20printemps%20refleurit
 
::
 
    L'hiver touche à sa fin, le printemps refleurit
    La fleur rouge du soleil est revenue et la nuit s'est enfuie
    Les montagnes sont couvertes de tulipes, les tulipes sont éveillées
    Dans les montagnes, ils sèment des tournesols, fleur après fleur après fleur
    Dans les montagnes, son cœur s'est éveillé
    Il apporte des fleurs, du pain et des fusils
    Dans sa poitrine (jân*, jân, jân),
    il y a une forêt d'étoiles (jân, jân)

    L'hiver touche à sa fin, le printemps refleurit
    La fleur rouge du soleil est revenue et la nuit s'est enfuie
    Un sourire lumineux sur ses lèvres, son cœur est enflammé par l'émotion
    Sa voix jaillit comme une source et sa mémoire est comme un cerf dans la forêt lointaine 


Paroles en persan
==========================

- https://criduchoeur.jimdofree.com/r%C3%A9pertoire/sar-omad-zemestoun/paroles/#:~:text=avec%20voix%20basse%20%3A-,sar%20omad%20zemestoun,-%2C%20shekofteh%20bah%C3%A2roun


::

    Sar omad zemestoun, shekofteh bahâroun
    Basse :                      jân, jân                      jân, jân
    
    Gol-é-sorkh-é-khorshid bâz oumad o shab | shod | gorizoun (x2)
    Kouhâ lâleh zâran, lahlehâ bidâran
    Basse :                 jân, jân               jân, jân
    Tou kouhâ dâran gol gol gol aftâbo mikâran (x2)

    Tou-yé kouhestoun, delesh bidâreh
    Tofang o gol o gandoum, dâreh miyareh

    Tou-yé sinesh, jân jân jân (x2)
    Yé jangal setâreh dâreh, jân jân, yé jangal setâreh dâreh (x2)
    Sar omad zemestoun, shekofteh bahâroun
    
    Basse :                      jân, jân                      jân, jân
    
    Gol-é-sorkh-é-khorshid bâz oumad o shab | shod | gorizoun (x2)
    Labesh khandeh-yé-nour, delesh chooleh-yé-shour

    Basse :                       jân, jân                        jân, jân

    Sedâch cheshmeh o yâ|desh a|hou-yé-jangal-é-dour (x2) 


Paroles en anglais
========================

::

    “The winter has come to an end,
    the spring has blossomed
    The red flower of the sun has risen once again,
    the night has escaped
    The mountains are covered with tulips,
    the tulips are awake
    They are planting sunshine in the mountains…”


