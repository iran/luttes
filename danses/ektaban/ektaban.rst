.. index::
   pair: Danses ;  Ektaban

.. _danse_ektaban_2023_03_08:

=====================================================================================================
**2023-03-08 Danse des 5 filles d'Ektaban**
=====================================================================================================

- https://www.youtube.com/watch?v=2EDWiMANebo (Danse des filles d'Ekbatan, Téhéran, 8 mars 2023)

- http://nitter.smnz.de/arminarefi/status/1633785524300259330#m

PLUS BEAU MESSAGE d’#Iran à l’occasion de la journée internationale de
la femme.

Cheveux au vent et nombril à l’air, cinq habitantes du quartier Ekbatan
de Téhéran entament une chorégraphie sur Calm Down de Rema, comme partout
ailleurs dans le monde, sauf que c’est interdit ici.


- http://nitter.smnz.de/arminarefi/status/1634233970861719562#m

WANTED. Les 5 Iraniennes qui ont émerveillé le monde en dansant à l’occasion
de la journée internationale des droits de la femme sont recherchées par
les services de sécurité en #Iran qui examinent les caméras de leur
quartier d’Ekbatan de Téhéran et en interrogent les gardiens.
