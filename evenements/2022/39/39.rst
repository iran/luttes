
.. _semaine_39_iran_2022:

============================================================
Semaine 39 Du lundi 26 septembre au lundi 3 octobre 2022
============================================================

♀️✊ 2e rassemblement samedi 1er octobre 2022 à Grenoble place Félix Poulat à 14h30 **Femme, Vie, Liberté #mahsa_amini 📣  Portons la voix de l'Iran, rassemblement de solidarité avec les femmes Iraniennes #Grenoble #NousSommesLeursVoix #OpIran #مهساامینی** 📣
=======================================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_10_01`


**Vendredi 30 septembre 2022 Vendredi sanglant ("Bloody Friday") Massacre de plus de 90 personnes au Balouchistan #Balochistan**
=====================================================================================================================================

- :ref:`massacre_balouchistan_2022_09_30`


Jeudi 29 septembre 2022 Communiqué | Soutien de l'EHESS (L’École des hautes études en sciences sociales) à la mobilisation de la société iranienne
=======================================================================================================================================================

- :ref:`ehess_2022_09_29`
