
.. _semaine_48_iran_2022:

================================================================
Semaine 48 Du lundi 28 novembre au lundi 5 décembre 2022
================================================================


♀️✊ ⚖️ 🌄  11e rassemblement samedi 3 décembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarite avec le peuple iranien et aux kurdes durement réprimé·e·s en Iran** #Grenoble #MahsaAmini #MahsaJinaAmini 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_12_03`


- :ref:`ali_rakhshani_mohammad_rakhshani_2022_12_02`
- :ref:`blast_un_peuple_se_leve_2022_11_29`
