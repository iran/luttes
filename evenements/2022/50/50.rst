
.. _semaine_50_iran_2022:

=============================================================
Semaine 50 du lundi 12 décembre au lundi 19 décembre 2022
=============================================================


♀️✊ ⚖️ 13e rassemblement samedi 17 décembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarité avec le peuple iranien, libération des prisonniers politiques et annulation des condamnations à mort** 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_12_17`

Vendredi 16 décembre 2022 **Le régime se venge et punit les manifestants, je reste absolument horrifié, choqué et indigné**
=================================================================================================================================

- :ref:`javad_rehman_2022_12_16`


Mercredi 14 décembre 2022 **Expulsion de l'Iran de la Commission de la femme de l'ONU (CSW)**
================================================================================================

- :ref:`expulsion_com_femmes_2022_12_14`

Mercredi 14 décembre 2022 **Lettre de soutien des médecins français**
========================================================================

- :ref:`soutien_medecins_2022_12_14`


Mardi 13 décembre 2022 **La doctoresse Aida Rostami, enlevée, torturée et assassinée**
=========================================================================================

- :ref:`aida_rostami`


Lundi 12 décembre 2022 **Le régime des ayatollahs a déjà exécuté par pendaison deux protestataires #Mohsen_Shekari et #Majidreza_Rahnavard**
===============================================================================================================================================

- :ref:`executions_2022_12_12`


Dimanche 11 décembre 2022 **Soirée iranienne à Lans-en-Vercors organisée par le cinéma Le Clap**
===================================================================================================

- :ref:`isere_iran_2022_12_11`
