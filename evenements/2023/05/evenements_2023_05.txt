

.. {ref} in ["local", "list"]
.. {path} in ["images", "{year}/{week}/images"]


.. _shervin_hajipour_2023_02_05_{{ref}}:

2023-02-05 #ShervinHajipour Grammy de la meilleure chanson pour le changement social **Baraye**
====================================================================================================

- :ref:`baraye`
- https://twitter.com/LettresTeheran/status/1622448963767996416#m

La chanson « Baraye » de @HajipourShervin devenue l’hymne du soulèvement
en Iran a gagné le Grammy de la meilleure chanson pour le changement social.

- https://twitter.com/LettresTeheran/status/1622458997151346689#m

Silencieux sur les réseaux sociaux depuis son arrestation et sa libération
Shervin Hajipour a réagi à cette victoire sur Instagram en publiant
simplement :« Nous avons gagné »


- https://nitter.manasiwibi.com/arminarefi/status/1622624558657200128#m

PREMIÈRE DAME des États-Unis, Jill Biden a prononcé dimanche 5 février
les mots « Woman, life, freedom », devise de la révolte en #Iran, en
remettant à l’artiste iranien Shervin Hajipour le Grammy Award de la
meilleure chanson pour un changement social pour « Baraye » (« Pour »).



- https://mastodon.social/@ScottLucas/109817389149036838

#ShervinHajipour, who faces imprisonment over "Baraye", wins US Grammy
for song which has become an anthem of #IranProtests2023

“For dancing in the streets/For the fear we feel when we kiss/For Women, Life, Freedom"



2023-02-05 Deux sœurs, deux journalistes, deux combattantes Elnaz Mohammadi arrêtée ce matin
==============================================================================================

- https://nitter.manasiwibi.com/FaridVahiid/status/1622171537519398912#m

Elaheh Mohammadi, qui avait couvert les funérailles de #MahsaAmini est
emprisonnée à la prison d’Evin de Téhéran depuis le 29 sep.

Sa sœur jumelle, Elnaz (à gauche), également journaliste, vient d’être
arrêtée ce matin ! #Iran


- https://twitter.com/LettresTeheran/status/1622160912311271424#m

Selon le mari de #ElahehMohammadi la journaliste emprisonnée depuis plus
de 4 mois, la sœur jumelle d’Elaheh, #ElnazMohammadi journaliste également,
est arrêtée aujourd’hui.


2023-02-04 #Environmental_crime and destruction of #Bankol_Oak_forests in #Ilam for oil and gas exploration
===============================================================================================================

- https://eldritch.cafe/@sanaz/109808673241890130

#Environmental_crime and destruction of #Bankol_Oak_forests in #Ilam for
oil and gas exploration.

In the heart of #Zagros #forests in the northern part of Ilam, in Bankol
district, the Iranian regime is destroying the #Oak_forests.

#Environment and is building a #gas_field.
The citizens of #Karzan near the #Bankol forest are spontaneously trying
to stop this project through a #campaign and with the help of #environmental
activists, because the construction of this project will result in the
pollution of groundwater and the destruction of a large part of #agricultural land,
in addition to the death of all #trees and the loss of all animal species.

- https://eldritchcafe.files.fedi.monster/media_attachments/files/109/808/664/160/224/219/original/456385d653529b21.mp4


2023-02-04 The German Foreign Ministry urged its citizens including dual Iranian-German nationals to avoid visiting Iran and leave the country
==============================================================================================================================================

The German Foreign Ministry has, in its latest Iran travel advisory, urged
its citizens including dual Iranian-German nationals to avoid visiting
Iran and leave the country if they are already there due to a serious
risk of arbitrary arrest and long-term prison sentences.


♀️✊ ⚖️ 20e rassemblement samedi 4 février 2023 à Grenoble **place Félix Poulat** à 14h30 **Arrêt des exécutions, annulation des condamnations à mort libération des prisonniers politiques, expulsion de l'ambassadeur d'iran** 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2023_02_04`


2023-02-03 **Manifestations à #Zahedan et #Sanandaj**
=======================================================

- https://masthead.social/@NaderTeyf/109801400634765310

Les Baloutches sont descendus dans la rue ce vendredi pour la dix-huitième
semaine consécutive. Les manifestants ont scandé contre le régime et son
guide suprême à #Zahedan.

UNE FOULE s’est rassemblée vendredi 3 février à Zahedan, dans le sud-est
de l’#Iran, pour une nouvelle journée de contestation en marge de la
prière hebdomadaire organisée dans la mosquée Makki de la ville.

« Je tuerai ! Je tuerai ! Celui qui a tué mon frère ! », scande-t-elle.


Les Kurdes ont aussi manifesté à l'autre bout du pays, à #Sanandaj, pour
exiger la libération de tous les prisonniers politiques, entre autres,
les Baloutches.

**Le mouvement #Femme_Vie_Liberté a renforcé la solidarité entre les peuples
en #Iran et cela déplaît beaucoup aux mollahs qui essayent encore de
diviser pour régner**.


Ce matin une manifestation a eu lieu à #Sanandaj pour exiger la libération
des prisonniers politiques.
Ce soir, un groupe de jeunes y barrent une rue et scandent:"Mort au dictateur"
et "Mort à Khamenei".

Le Kurdistan et le Sistan et Baloutchestan sont deux provinces en #Iran
où le mouvement #Femme_Vie_Liberté n'a jamais cessé depuis le 16 septembre
et cela malgré une répression importante, avec des centaines de morts et
plusieurs milliers d'arrestations.




2023-02-03 **"Iran : sur la piste de l'or des mollahs"**
===========================================================================================================================

- https://www.parismatch.com/actu/international/iran-sur-la-piste-de-lor-des-mollahs-221692
- https://piped.kavin.rocks/nMBw6b3b31Q

ENQUÊTE. Affolée par la révolte, la République islamique exfiltre en
catastrophe le trésor des gardiens de la révolution.

A l’intérieur du terminal quasi désert de l’aéroport de Mehrabad, en
principe fermé à cette heure, des hommes en noir remplissent les soutes
de plusieurs Airbus de la compagnie Mahan Air.

Hassan, employé à la logistique, les observe. «Les types étaient des
pasdarans, les gardiens de la révolution. Leurs sacs devaient peser de
70 à 100 kilos et les avions n’étaient inscrits sur aucun registre.

Des membres d’équipage m’ont raconté qu’ils desservaient des pays proches.
Quand je leur ai demandé pourquoi ils décollaient en pleine nuit, ils
m’ont dit que si je posais des questions, je risquais d’avoir des problèmes.
Je n’ai pas insisté mais je suis certain qu’ils chargeaient de l’argent.»

..

2023-02-03 **GRANDE NOUVELLE. Le cinéaste iranien Jafar Panahi libéré sous caution après sept mois de prison**
=================================================================================================================


GRANDE NOUVELLE. Le cinéaste iranien Jafar Panahi libéré sous caution
après sept mois de prison

les mots du réalisateur iranien Jafar Panahi à sa sortie de prison, à
Téhéran "il y a tellement de monde derrière moi (dans la prison d'Evin),
c'est plein d'étudiants, de professeurs, d'ouvriers, comment je pourrais
être heureux ce soir?"


.. figure:: {{ path }}/jafar_panahi.png
   :align: center

2023-02-03  **Comprendre «Femme, Vie, Liberté» Les interventions sont en ligne**
=====================================================================================

- :ref:`etude_2023_01_16`

- https://soundcloud.com/user-897145586/sets/femme-vie-liberte

Comprendre « Femme, Vie, Liberté » `Les interventions sont en ligne <https://soundcloud.com/user-897145586/sets/femme-vie-liberte>`_


Nobel laureate Ebadi says Iran's **'revolutionary process' is irreversible**
=================================================================================

- https://www.reuters.com/world/middle-east/nobel-laureate-ebadi-says-irans-revolutionary-process-is-irreversible-2023-02-03/


DUBAI, Feb 3 (Reuters) - Iranian Nobel Peace Prize laureate Shirin Ebadi
said the death in custody of a young Iranian Kurdish woman last year has
sparked an irreversible "revolutionary process" that would eventually
**lead to the collapse of the Islamic Republic**.

GROWING ANGER
-----------------

Ebadi, speaking in a phone interview from London, said the state's use of
deadly violence will deepen anger felt by ordinary Iranians about the
clerical establishment because the their grievances remain unaddressed.

"The protests have taken a different shape, but they have not ended,"
Ebadi told Reuters in a phone interview from London.

With deepening economic misery, chiefly because of U.S. sanctions over
Tehran's disputed nuclear work, many Iranians are feeling the pain of
galloping inflation and rising joblessness.

Inflation has soared to over 50%, the highest level in decades.
Youth unemployment remains high with over 50% of Iranians being pushed
below the poverty line, according to reports by Iran's Statistics Centre.

The crackdown has stoked diplomatic tensions at a time when talks to
revive Tehran's 2015 nuclear deal with world powers are at a standstill.

The United States and its Western allies have slapped sanctions on Iranian
authorities and entities for their involvement in the crackdown and other
human rights abuses.

**To force Iran's clerical establishment from power, Ebadi said the West
should take "practical steps" such as downgrading its political ties
with Iran by recalling its ambassadors from Tehran, and should avoid
reaching any agreement with the Islamic Republic, including the nuclear deal**.


Mercredi 1er février 2023 Jafar Panahi entame une grève de la faim sèche
====================================================================================

- :ref:`jafar_panahi_2023_02_01`

- https://nitter.manasiwibi.com/arminarefi/status/1621063734155255809#m


EN PRISON depuis le 11 juillet 2022, l’illustre cinéaste iranien Jafar Panahi
a annoncé dans une lettre divulguée par sa famille qu’il entamait à compter
du 1er fevrier 2023 une grève de la faim sèche pour protester contre
les « traitements illégaux et inhumains » du pouvoir judiciaire


2023-02-03 #ElmiraRahamani violoncelliste de l’orchestre philharmonique d’Ispahan et de confession bahaï est en détention arbitraire
========================================================================================================================================

- https://eldritch.cafe/@sanaz/109797818214543806

#ElmiraRahamani violoncelliste de l’orchestre philharmonique d’Ispahan
et de **confession bahaï** est en détention arbitraire depuis le 16 janvier 2023.

Selon une source proche elle est accusée d’« incitation à l’émeute et à
la guerre » et « propagande contre le système ».

La minorité bahaï est très violemment réprimée par le régime islamique
qui ne reconnaît pas cette religion. Le simple fait d’être bahaï suffit
pour être emprisonné en Iran

.. figure:: {{ path }}/elmira_rahamani.jpeg
   :align: center


2023-02-03 #FreeToomaj
===========================

- http://nitter.fdn.fr/Baboobabounette/status/1621458822681296896#m

Ils et elles sont des milliers de prisonniers politiques iraniens, torturés,
violés, victimes d'aveux forcés pour avoir demandé la liberté.

Parmi eux @OfficialToomaj rappeur et héros national. Cela fait 97 jours
qu’il est à l’isolement et encourt la peine de mort #FreeToomaj


2023-02-03 **Sauvez Farhad Meysami**
=====================================

- https://www.change.org/p/sauvez-dr-farhad-meysami

IRAN. Farhad Meysami, médecin & militant emprisonné pour avoir soutenu
ls droits des femmes, est en grève de la faim depuis 4 mois. 1 pétition
appelle à sauver Meysami devenu squelettique.

#SaveFarhadMeysami #JinJiyanAzadi


