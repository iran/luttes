.. index::
   ! Aïda Asgharzadeh

.. local in ["local", "list"]
.. images in ["images", "{year}/{week}/images"]



2023-05-03 🇫🇷 **La France persiste à vouloir expulser vers l’Iran**
=========================================================================

- https://www.amnesty.fr/refugies-et-migrants/actualites/la-france-persiste-a-vouloir-expulser-vers-liran

Depuis le mois d’avril 2023, nous avons eu connaissance de plusieurs cas
lors desquels les autorités françaises ont rendu des décisions d’expulsion
vers l’Iran et placé des personnes iraniennes en centres de rétention.

Et ce, malgré les risques de persécutions auxquelles les personnes seraient
exposées sur place.

Avec la Cimade et le Collectif Iran Justice, nous dénonçons ces décisions
illégales et interpelons le ministre de l’Intérieur, Gérald Darmanin.

...

Alors que le ministre de l’Intérieur, Gérald Darmanin, déclarait le 25
janvier 2023 devant le Sénat : « Il ne s’agit donc pas pour la France de
renvoyer vers l’Iran des ressortissants.

Nous n’en renvoyons plus vers ce pays, d’ailleurs » et « Quoi qu’il en soit,
je puis vous assurer que nous n’expulserons personne vers l’Iran »,
l’administration continue de notifier des décisions d’expulsion vers
l’Iran mettant en danger la vie de ces personnes ainsi que celle de leurs
familles.

Nous demandons au ministre de l’Intérieur que des consignes claires soient
envoyées aux préfets afin qu’il soit mis immédiatement fin à la notification
de décisions d’expulsion et de placement en rétention de personnes qui
présentent des craintes de persécution ou de mauvais traitement en cas
de renvoi dans leur pays d’origine.

Ces personnes, qui ne peuvent légalement pas être expulsées, doivent
obtenir la protection de la France ou à défaut un titre de séjour.

Signataires 
--------------

- La Cimade
- Amnesty International France
- Collectif Iran Justice

2023-05-03 **Le pari fou d’un écrivain français en Iran : "L'usure d'un monde"**
====================================================================================

- https://www.lepoint.fr/culture/le-pari-fou-d-un-ecrivain-francais-en-iran-03-05-2023-2518842_3.php
- :ref:`lusure_dun_monde`

Malgré les avertissements, François-Henri Désérable s’est rendu en République
islamique en plein mouvement de révolte et en revient avec un livre unique.

- https://twitter.com/Les_CitadElles/status/1654022709288869888#m

A lire par F-H Désérable.
" Début novembre, à Téhéran, la moitié des filles de moins de trente ans
sortaient sans le voile .. les agents de la police des moeurs étaient
dépassés par l'ampleur, la durée du mouvement..."


2023-05-02 🇮🇷 **Le régime iranien tue une alpiniste kurde près de Téhéran**
===============================================================================

- https://kurdistan-au-feminin.fr/2023/05/02/iran-le-regime-iranien-tue-une-alpiniste-kurde-pres-de-teheran/

IRAN – L’alpiniste kurde, Mahsa Zarin Cheng a été assassinée sous la torture
au ministère iranien de l’Information et son corps jeté au pied d’une
montagne près de Téhéran, signalent plusieurs médias kurdes qui citent
l’ONG des droits humains Kurdistan Human Rights Network (KHRN).

La militante Mahsa Zarin Cheng, 32 ans, qui a participé aux manifestations
anti-régime déclenchées parle meurtre de Jina Mahsa Amini en septembre 2022
à Téhéran et qui aurait été détenue et torturée à plusieurs reprises
pendant les manifestations, a disparu il y a quelques jours alors qu’elle
était partie en escalade avec des amis. Le même jour, les forces du régime
iranien ont fait une descente au domicile de l’athlète et ont confisqué
ses effets personnels.

Aucune déclaration officielle n’a été faite à la famille. Le 1er mai, le
corps de l’athlète a été retrouvé au pied d’une montagne à Téhéran.

Dans un communiqué sur le sujet, l’Association des droits de l’homme du
Kurdistan a annoncé que l’athlète avait été tuée et que sa famille avait
été menacée par les renseignements iraniens pour qu’elle ne parle pas
du meurtre de l’athlète qu’ils ont déguisé en « chute d’une hauteur
en grimpant ».

Mahsa Zarin Cheng a été enterrée au cimetière de Behesht Reza, à Ilam,
sous le siège des forces armées gouvernementales.