
.. list in ["local", "list"]
.. 2023/24/images in ["images", "{year}/{week}/images"]



2023-06-18 9 mois après la mort de #MahsaAmini‌ les Iraniennes continuent leur combat #FemmeVieLiberte
========================================================================================================

- http://nitter.unixfox.eu/Arlette_Zilberg/status/1670316986126020610#m


9 mois après la mort de #MahsaAmini‌ les Iraniennes continuent leur combat #FemmeVieLiberte.
La révolution continue, sous d'autres formes. #IranRevolution


2023-06-18 Le régime politico-religieux en #Iran s'est mis à dos les jeunes iraniennes qui veulent renverser ce régime. Il les enlève, les emprisonne
==========================================================================================================================================================

- http://nitter.unixfox.eu/Les_CitadElles/status/1670514999477825538#m

Pfttt ! Le régime politico-religieux en #Iran s'est mis à dos les jeunes
iraniennes qui veulent renverser ce régime.

Il les enlève, les emprisonne.

Alors ce sont les mères qui manifestent ! Ce régime ne pourra emprisonner
tout 1 peuple. Il tombera ! #IranRevolution #IranianGirls


2023-06-17 Dix des étudiant.e.s qui protestaient contre l’application obligatoire du voile par un un sit-in lancé le 14 juin à la faculté d’Art de l’université de Téhéran ont été enlevées
=================================================================================================================================================================================================

- https://kurdistan-au-feminin.fr/2023/06/17/iran-kidnapping-de-10-etudiants-a-la-faculte-dart-de-luniversite-de-teheran/

IRAN – Dix des étudiant.e.s qui protestaient contre l’application obligatoire
du voile par un un sit-in lancé le 14 juin à la faculté d’Art de l’université
de Téhéran ont été enlevées ce matin par des individus en uniforme sur
le campus universitaire.

On ne sait pas où se trouvent actuellement les étudiant.e.s kidnappé.e.s.



2023-06-14 **La situation en Iran et dans l’exil iranien (1)**
====================================================================


- https://oclibertaire.lautre.net/spip.php?article3823

La situation en Iran et dans l’exil iranien (1)

Interview d’un camarade iranien

mercredi 14 juin 2023, par Courant Alternatif

Nous avons réalisé en janvier 2023 une interview de B, camarade libertaire
iranien vivant à Lyon Nous l’avons complété récemment.

Nous vous en présentons ici la première partie. La suite paraîtra dans le numéro d’été.

Issu d’un milieu pauvre, B. a pu entrer à l’université publique d’Ispahan
qui était gratuite à l’époque. Il milité dans un éphémère groupe communiste
« Liberté et égalité ».

Après plusieurs arrestations il s’est exilé en Turquie.
En 2013, arrivé en France il rejoint le Parti de Gauche, devenu LFI,
qu’il quitte en 2019 tout en fréquentant le groupe trotskiste « l’Étincelle ».

Il est membre du collectif de la librairie libertaire la Gryffe et a
traduit en iranien « Qu’est-ce que la propriété ? » de Proudhon, et
« Protestations devant les libertaires du présent et du futur sur les
capitulations de 1937 ».

Les débuts de la révolte

Le déclencheur c’est la mort de Masha Amini, jeune femme kurde iranienne en visite à Téhéran, arrêtée le mardi 13 septembre par la police des mœurs et déclarée morte à l’hôpital 3 jours plus tard. Elle avait été battue à mort.
Il y avait déjà eu des morts dans de précédents mouvements de contestation du régime, comme celui autour des vendeurs ambulants qui ont été tabassés et massacrés dans la rue en 2020, mais il n’y avait jamais eu de réaction comme ce qui s’est passé après la mort de Masha.
C’est comme si le couvercle de 43 années de colère intériorisée par les iraniens contre le régime venait de sauter.
Cela a déclenché une dynamique nouvelle extrêmement déterminée dans la société iranienne.
Ainsi, le 16 septembre au soir même du décès, il y a eu une première confrontation avec la police devant l’hôpital où le corps de Masha reposait. La manifestation dénonçait les circonstances de sa mort (meurtre nié par la police) et les mensonges officiels sur les problèmes de santé qui auraient causé sa mort.
Dès la 2ème semaine les étudiants ont entamé une grande grève. Le régime a fermé les universités et des centaines d’étudiants ont été arrêtés et sont encore emprisonnés.

Pendant 3 mois le mouvement a occupé la rue dans tout le pays, mais après 30 000 emprisonnements, de nombreux blessés (nombre inconnu), 600 morts dans la rue, une dizaine de condamnations à mort et 5 exécutions capitales, les manifestations ont beaucoup diminué voire disparu. La force de manifester dans la rue a été détruite/réprimée, les gens restent chez eux car on ne peut pas manifester tous les jours, on souffre et on s’épuise, comme dans le mouvement des Gilets jaunes.
Mais d’autres formes d’actions existent : des réunions, des actions artistiques comme la poésie, des clips, des slogans…
Et, pour nous qui sommes en exil, quand on écoute impuissant, les nouvelles et qu’on apprend qu’un tel est mort, un autre arrêté, cela détruit ta santé mentale. À part pleurer et aller manifester nous nous sentons dans une position d’incapacité à faire bouger les lignes.
Peut-on parler de révolution avec ce mouvement ?

Les cycles de confrontation avec l’État sont de plus en plus réguliers, C’est quasiment tous les ans. C’est pour ça qu’on peut dire qu’on est dans une époque révolutionnaire. C’est une révolution sur l’aspect social, l’aspect économique, et sur certaines libertés. Il y a 43 ans de colères, sur différents sujets. Économiquement, l’Iran pourtant 2ème réserve de gaz et producteur de pétrole, est en difficulté parce que le système est corrompu. Sur les libertés individuelles et les droits fondamentaux, on est en face d’un régime islamiste qui interdit tout. Aller prendre une bière en terrasse sans finir en prison, c’est un rêve pour la jeunesse iranienne, on ne se rend pas compte à quel point.
La révolution actuelle est une révolution sociale profonde : elle va changer les rapports entre les gens, dans la société, les relations femmes hommes, les libertés pour les minorités, etc. C’est pourquoi cette révolution va être longue.
Il y a de tout dans cette révolution, mais on ne sait pas qui va arriver à traduire tout ça et donner un programme politique. J’espère que ce sera la gauche, mais pour qu’elle arrive à ça il faudra changer beaucoup de choses.
La contre-révolution – les royalistes – veulent juste changer les dirigeants. La droite acceptera les libertés individuelles, mais on aura un problème au point de vue économique avec eux ; c’est pourquoi les liens avec les syndicats sont tellement important, et beaucoup d’amis sont en train de travailler avec eux. Le syndicalisme est interdit en Iran. Toute réunion de travailleurs est considérée comme une organisation terroriste, donc ça se fait en cachette.
C’est une révolution qui a commencé, qui va peut-être durer. Là, ça fait plus de trois mois que ça continue, et j’ai aucun doute que les iraniens vont gagner d’une manière ou d’une autre, et faire tomber ce régime.
C’est héroïque ce qu’ont fait les iraniens. Je ne suis pas nationaliste mais ce mouvement m’a rendu fier d’être iranien. C’est un peuple en révolte, un grand peuple révolté qui veut changer la société.
Dans de nombreuses familles, il y a un mort, un prisonnier. À la mi-mai 3 personnes ont été encore exécutées et leurs familles qui protestaient, emprisonnées. La colère, la haine et la vengeance sont le moteur de beaucoup de personnes en Iran aujourd’hui. Vu comment fonctionne la société iranienne, cela va provoquer beaucoup de règlement de comptes quand le régime tombera.
Le tabou qu’il y avait sur la religion a sauté. En quoi est-ce important ?

L’appel du muezzin à 5 h du matin, pour nous c’est quelque chose d’horrible.

Des amis touristes français m’ont dit qu’ils trouvaient ça magnifique.
Mais pour nous, ça veut dire que quelqu’un va être exécuté, parce que
c’est à ce moment qu’ont lieu les exécutions dans les prisons.

Selon les règles islamiques, c’est après la prière qu’on peut tuer, pas avant.

Pour nous, il n’y avait pas mieux pour détruire l’islam que ce gouvernement
islamiste, et je peux le remercier sur cet aspect.

L’Islam a mis dix siècles pour s’imposer partout en Iran et devenir une
religion d’état. Ce sont les Safavides qui, au 16ème siècle ont imposé
le chiisme comme religion d’état pour contrer la menace de l’empire ottoman.

Avant ça, il n’y avait pas de règles strictes : l’islam s’était diffusé,
mais la population était assez libre et dans la pratique et les iraniens
n’ont **jamais accepté l’islam comme dans les autres pays musulmans**.

Dans la société il a toujours existé des mouvements de résistance.
En 1906 il y a eu une tentative de monarchie constitutionnelle, qui donnait
le pouvoir à l’assemblée et actait une séparation de l’église et de l’État.
Mais ça n’a duré que trois mois, sous les attaques du clergé chiite.

Avant la révolution, l’islam était respecté, mais l’arrivée au pouvoir
des islamistes a montré une autre face de la religion.

Durant la révolte de 2022-2023, une photo montre des collégiennes faisant
des doigts d’honneur à un portrait d’ayatollah ; cette photo montre que
le régime a détruit tout respect envers la religion.

Elle montre que l’Iran ne peut plus être un pays religieux. C’est fini.

Marx disait à propos d’un livre d’Hegel que tout ce qu’on considérait
comme sacré allait s’évaporer dans l’air ; ce qu’on croit ne jamais
pouvoir changer disparaîtra un jour ou l’autre.

Maintenant on fait tomber les turbans des mollahs.
Des mosquées sont attaquées.
Les femmes sortent sans voile et se coupent les cheveux.
Le musée Khomeiny a été incendié.

Tout ce qui était autrefois sacré a été détruit.

C’est le message de cette photo : des jeunes filles contestent l’autorité
religieuse et y mettent fin. On peut être optimiste pour l’avenir et
la liberté en Iran.

Sur ce sujet il y a aussi des problèmes générationnels, entre parents et
enfants. On trouve une vidéo d’un jeune qui brûle un coran chez lui et
dit aux parents « voyez, je brûle le Coran et rien ne se passe ».

Avec des amis on a fait un travail d’analyse du Coran verset par verset.

Mais jamais je ne traduirai ça en français, c’est trop dangereux.
Ici en France c’est compliqué de parler de l’islam.
Pour nous autres iraniens c’est facile, comme on vient d’un pays de
culture musulmane : on sait qu’on est contre.


.. _decouverte_lithium_2023_06_12_list:

2023-06-12 **La découverte d’une énorme réserve de lithium faite en Iran inquiète Israël et l’Occident**
==============================================================================================================

- https://www.jforum.fr/la-decouverte-faite-en-iran-inquiete-israel-et-loccident.html
- http://desinfos.com/la-revue-des-infos/la-decouverte-faite-en-iran-inquiete-israel-et-l-occident.html


**La divulgation d’une énorme réserve de lithium, contenant 8,5 millions
de tonnes, devrait rehausser le profil de l’Iran dans le monde**.

« Cette prise de position risque de modifier le rapport de force et le
rapport de force régional en sa faveur »

Le renforcement géopolitique de l’Iran ces derniers temps est un fait accompli.

Après une déconnexion pendant plusieurs années, les Iraniens ont renoué
des relations diplomatiques avec les pays du Golfe, menés par l’Arabie Saoudite,
et il semble que malgré la pression américaine et les sanctions qui lui
sont imposées, il réalise des progrès significatifs dans le développement
du programme nucléaire – et la chute du régime n’est pas en vue.

Maintenant, Téhéran a une autre raison de sourire. La position économique
et l’influence géopolitique de l’Iran devraient bientôt se renforcer
considérablement.

Tout cela sur fond de découverte d’un immense gisement de lithium,
contenant 8,5 millions de tonnes – le deuxième plus gros gisement au
monde après le Chili – situé dans le district de Hamden à l’ouest du pays.

Non seulement cela représente environ 10 % des réserves mondiales
totales d' »or blanc », qui sont actuellement estimées à environ 89
millions de tonnes, mais la découverte – qui pourrait sauver l’économie
iranienne et éliminer apparemment l’efficacité des sanctions à son
encontre – est un  » billet gagnant ».

On s’attend à ce qu’il fasse basculer l’équilibre régional des pouvoirs
et donne à l’Iran une puissance et un statut géopolitique économique
sans précédent.

Cette découverte pourrait affecter considérablement les marchés mondiaux
de l’énergie et des mines, ainsi que l’industrie des batteries et des
véhicules électriques dans le monde.

Tous ces éléments, à leur tour, devraient entraîner des changements
profonds dans l’économie et la politique mondiales, ramenant le centre
de gravité vers le Moyen-Orient, et influençant la carte des relations
et des intérêts sur la scène internationale.

Un haut fonctionnaire du ministère iranien de l’Industrie et des Mines a
affirmé qu’il s’agit de la plus grande réserve de lithium au monde située
en dehors de l’Amérique du Sud (l’Argentine, la Bolivie et le Chili sont
responsables de 58 % des ressources mondiales) et la deuxième après la
réserve en Chili, qui contient 9,2 millions de tonnes de lithium.

Ce minéral est considéré comme le « joyau de la couronne » en raison de
son énorme valeur et de son importance pour la production de batteries
pour diverses industries électroniques, notamment les voitures électriques,
les ordinateurs portables et les téléphones portables, les panneaux
solaires et les drones, ainsi qu’à des fins de stockage d’énergie.

**La découverte du réservoir en Iran est un changement significatif dans
les règles du jeu**.

Le Dr Hochberg Marom, expert en géopolitique, en terrorisme mondial et
en crises internationales , affirme que « non seulement l’influence de
l’Iran pourrait devenir considérablement plus forte et même éclipser
celle des pays du Golfe, en mettant l’accent sur les principaux producteurs
mondiaux de pétrole, dirigés par l’Arabie saoudite.

Saoudite et les Emirats, cette position peut changer en sa faveur les
rapports de force et les rapports de force régionaux. »

Une étude de 2021 publiée par le cabinet de conseil McKinsey montre que
la demande croissante de véhicules électriques a provoqué une croissance
fulgurante en quelques années de 550 % des prix du lithium.

Diverses sources financières occidentales prédisent même que le marché
mondial du lithium, qui s’élevait à 7,3 milliards de dollars en 2022,
devrait doubler et atteindre 16,3 milliards de dollars en 2030.


Sur l'important gisement de lithium (aussi appelé "or blanc") découvert
en Iran (premières annonces en mars 2023 et ci-dessous juin 2023)

- https://lentrepreneur.co/actualites/international/petrole-du-21e-siecle-liran-decouvre-un-important-gisement-de-lithium-04032023 (20 juin 2023)
- https://www.france-irak-actualite.com/2023/06/la-decouverte-d-un-immense-champ-de-lithium-en-iran-va-changer-les-rapports-de-force.html (14 juin 2023)

  "L’expert israélien en géopolitique, Anat Hochberg Marom, a déclaré au
  journal israélien Maariv que la découverte du réservoir de lithium iranien
  pourrait entraîner un changement dans l’équilibre régional des pouvoirs,
  et accorder à Téhéran une position inédite aux niveaux géopolitique
  et économique.

  ...

  «Ainsi, ce champ ne se limite pas seulement au fait qu’il contient 10%
  des réserves mondiales d’or blanc, qui sont actuellement estimées à 89
  millions de tonnes, mais à la découverte – qui peut sauver l’économie
  iranienne et annuler ostensiblement l’efficacité des sanctions qui lui
  ont été imposées.
  Donc, il s’agit d’un Joker gagnant», explique le quotidien israélien,
  citant l’expert en géopolitique, Anat Hochberg Marom.

  Et de poursuivre : «Cette découverte pourrait avoir un impact significatif
  sur les marchés mondiaux de l’énergie et des mines, ainsi que sur les
  industries mondiales des batteries et des voitures électriques.

  Tout cela devrait entraîner des changements profonds dans l’économie
  et la politique mondiales, déplaçant le centre de gravité vers le Moyen-Orient».

  Ce minéral est considéré comme le «joyau de la couronne», en raison de
  son énorme valeur et de son importance dans la production de batteries
  pour diverses industries électroniques, notamment les voitures électriques,
  les ordinateurs portables, les téléphones portables, les panneaux solaires
  et les drones, ainsi qu’à des fins de stockage d’énergie.

  «Ainsi, il est probable, selon le journal, que cela non seulement renforce
  considérablement l’influence de l’Iran, mais l’emporte également sur
  l’influence des États du Golfe, dont les principaux producteurs mondiaux
  de pétrole, à savoir l’Arabie saoudite et les Émirats arabes unis.

  Cette situation risque de modifier le rapport de force régional en
  faveur de Téhéran».

  Et de conclure : «cela renforcera la position de négociation de l’Iran
  dans le cadre de diverses alliances et accords économiques et sécuritaires,
  et modifiera les rapports de force entre les différents acteurs au Moyen-Orient».





