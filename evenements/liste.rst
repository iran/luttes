

================================================================================
**Quelques événements depuis la mort de Mahasa Amini le 16 septembre 2022**
================================================================================

**Semaine 26 du lundi 26 juin 2023 au dimanche 2 juillet 2023**

.. include:: 2023/26/evenements_2023_26_list.txt


**Semaine 25 du lundi 19 juin 2023 au dimanche 25 juin 2023**

.. include:: 2023/25/evenements_2023_25_list.txt


**Semaine 24 du lundi 12 juin 2023 au dimanche 18 juin 2023**

.. include:: 2023/24/evenements_2023_24_list.txt


**Semaine 23 du lundi 5 juin 2023 au dimanche 11 juin 2023**

.. include:: 2023/23/evenements_2023_23_list.txt


**Semaine 22 du lundi 29 mai 2023 au dimanche 4 juin 2023**

.. include:: 2023/22/evenements_2023_22_list.txt


**Semaine 21 du lundi 22 mai 2023 au dimanche 28 mai 2023**

.. include:: 2023/21/evenements_2023_21_list.txt

**Semaine 20 du lundi 15 mai 2023 au dimanche 21 mai 2023**

.. include:: 2023/20/evenements_2023_20_list.txt


**Semaine 19 du lundi 8 mai 2023 au dimanche 14 mai 2023**

.. include:: 2023/19/evenements_2023_19_list.txt


**Semaine 18 du lundi 1er mai 2023 au dimanche 7 mai 2023**

.. include:: 2023/18/evenements_2023_18_list.txt


**Semaine 17 du lundi 24 avril 2023 au dimanche 30 avril 2023**

.. include:: 2023/17/evenements_2023_17_list.txt


**Semaine 16 du lundi 17 avril 2023 au dimanche 23 avril 2023**

.. include:: 2023/16/evenements_2023_16_list.txt


**Semaine 15 du lundi 10 avril 2023 au dimanche 16 avril 2023**

.. include:: 2023/15/evenements_2023_15_list.txt


**Semaine 14 du lundi 3 avril 2023 au dimanche 9 avril 2023**

.. include:: 2023/14/evenements_2023_14_list.txt


**Semaine 13 du lundi 27 mars 2023 au dimanche 2 avril 2023**

.. include:: 2023/13/evenements_2023_13_list.txt


**Semaine 12 du lundi 20 mars 2023 au dimanche 26 mars 2023**

.. include:: 2023/12/evenements_2023_12_list.txt


**Semaine 11 du lundi 13 mars 2023 au dimanche 19 mars 2023**

.. include:: 2023/11/evenements_2023_11_list.txt


**Semaine 10 du lundi 6 mars 2023 au dimanche 12 mars 2023**

.. include:: 2023/10/evenements_2023_10_list.txt


**Semaine 09 du lundi 27 février 2023 au dimanche 5 mars 2023**

.. include:: 2023/09/evenements_2023_09_list.txt


**Semaine 08 du lundi 20 février 2023 au dimanche 26 février 2023**

.. include:: 2023/08/evenements_2023_08_list.txt


**Semaine 07 du lundi 13 février 2023 au dimanche 19 février 2023**

.. include:: 2023/07/evenements_2023_07_list.txt


**Semaine 06 du lundi 6 février 2023 au dimanche 12 février 2023**

.. include:: 2023/06/evenements_2023_06_list.txt

**Semaine 05 du lundi 30 janvier 2023 au dimanche 5 février 2023**

.. include:: 2023/05/evenements_2023_05_list.txt


2023-01-28 **Les arrestations arbitraires d'adolescents se poursuivent en Iran**
=====================================================================================

Arbitrary arresting teenagers continues by #Iran

::

    Jan.28 four Baluchi citizens violently beaten & taken away by plainclothes
    agents in #Zahedan Families unclear of their locations!

::

    quatre citoyens baloutches violemment battus et emmenés par des agents en civil


.. figure:: ../repression/arrestations/2023/01/28/images/mostafa_shahuzehi.png
   :align: center

   Mostafa Shahuzehi-14 (TL)


.. figure:: ../repression/arrestations/2023/01/28/images/yasin_shahuzehi.png
   :align: center

   Yasin Shahuzehi-15 (TR)


.. figure:: ../repression/arrestations/2023/01/28/images/erfan_barahui.png
   :align: center

   Erfan Barahui-14 (BL)


.. figure:: ../repression/arrestations/2023/01/28/images/uthman_rakhshani.png
   :align: center

   Uthman Rakhshani-19 (BR)




♀️✊ ⚖️ 19e rassemblement samedi 28 janvier 2023 à Grenoble **place Félix Poulat** à 14h30 **Arrêt des exécutions, annulation des condamnations à mort libération des prisonniers politiques, expulsion de l'ambassadeur d'iran** 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2023_01_28`


.. figure:: ../actions/2023/01/28/images/annonce_rassemblement.png
   :align: center
   :width: 300

.. figure:: ../actions/2023/01/28/images/un_samedi_4_manifs.png
   :align: center
   :width: 600

   https://www.ici-grenoble.org/article/un-samedi-quatre-manifs


.. figure:: ../actions/2023/01/28/images/ici_grenoble_2023_01_28.png
   :align: center

   https://www.ici-grenoble.org/agenda

Texte d'espoir
--------------

La rupture entre du peuple et le pouvoir est totale et irréversible.

Le régime n’a plus les moyens économiques nécessaires pour répondre,
même partiellement, aux demandes sociales, et une partie de la classe
dominante n’est plus solidaire avec le Guide et ses positions extrêmes.

Le régime et son appareil de répression sont affaiblis.
Une fracture à l’intérieur de l’appareil de l’état est plus que visible
Par conséquent, l’émergence de nouvelles vagues de protestations et de
grèves est inévitable.

À cela s’ajoute une crise écologique, surtout une crise de l’eau
disponible, qui a déjà provoqué des émeutes dans un passé récent.
Et aussi une crise idéologique et morale due à la corruption systémique et
au pillage des ressources du pays par une poignée des puissants.
**Une liste non exhaustive !**

Le mouvement de révolte en cours, même affaibli, voire défait momentanément,
est appelé à se redresser.

On entre dans le cinquième mois de la révolte. La baisse récente d’intensité
des protestations ne doit pas tromper. Les protestations prennent d’autres
formes plus sur la base sociale et économique et social.
Comme la grève des titulaires du secteur pétrolier le 17 janvier 2023.

Le feu sous les cendres est en train d’œuvrer . Nous sommes plus que
jamais au seuil d’une révolution.



Vendredi 27 janvier 2023 **«Sommes-nous heureux ? Nous sommes anéantis…» Lettre de Tahereh Saeedi à son mari emprisonné, Jafar Panahi**
=============================================================================================================================================

- :ref:`panahi_2023_01_27`


**Nous sommes anéantis…**

La semaine dernière, nous avons été informés qu’il serait libéré dans une
semaine. Nous étions heureux à nouveau. Une semaine a passé, et Jafar
n’est toujours pas avec nous. Cela fait exactement 200 jours maintenant.

Nous sommes désespérés…

La libération de Jafar est en totale conformité avec leurs propres lois,
mais ils sont au-dessus des lois, sans aucun respect pour la loi.

Si en dépit de tout cela, vous nous demandez comment nous allons, et si
nous sommes heureux, nous vous répondrons que nous sommes heureux, et
que nous allons bien. Mais ne nous croyez pas sur parole.»

.. figure:: ../actions/2023/01/27/images/jafar_panahi_2010.png
   :align: center
   :width: 300



2023-01-25 **Rencontre avec le maire de Grenoble** #Grenoble #IranRevolution  #FemmeVieLiberte
==================================================================================================

- :ref:`maire_grenoble_2023_01_25`

🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)



"J'ai rencontré hier les associations iraniennes locales pour réaffirmer
la solidarité et le soutien de Grenoble au mouvement Femme Vie Liberté
du peuple iranien. Nous nous tenons à leurs côtés dans ce combat pour
la liberté !"

Source: https://singapore.unofficialbird.com/EricPiolle/status/1618531693442510849#m

.. figure:: ../actions/2023/01/25/images/mairie_grenoble.jpeg
   :align: center
   :width: 800



♀️✊ ⚖️ 18e rassemblement samedi 21 janvier 2023 à Grenoble **place Félix Poulat** à 14h30 **Arrêt des exécutions, annulation des condamnations à mort libération des prisonniers politiques, expulsion de l'ambassadeur d'iran** 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2023_01_21`


.. figure:: ../actions/2023/01/21/images/annonce_rassemblement.png
   :align: center
   :width: 300


Vendredi 20 janvier 2023 **Iran - Soutien au soulèvement « Femme, Vie, Liberté » - Non aux exécutions capitales !**
=========================================================================================================================

- :ref:`mediapart_2023_01_20`

**Les peuples d’Iran doivent être maîtres de leur destin**

Dans ce contexte et face au spectre d'une révolution politique et sociale
en Iran, les dirigeants des grandes puissances œuvrent, plus ou moins
discrètement, à la constitution d'un Conseil de transition rassemblant
tous les courants de l’opposition de la droite iranienne, dont les
monarchistes.

**Ces courants, libéraux sur le plan économique et autoritaires sur le plan
politique, sont à l’opposé des dynamiques des mobilisations et des
aspirations sociales et démocratiques qui s’expriment en Iran.**

Du coup d’État de 1953 organisé par la CIA et les services secrets britanniques
contre le gouvernement Mossadegh et sa politique de nationalisation du pétrole,
en passant par la conférence de Guadeloupe en 1979 où les chefs d'Etat
de France, d’Allemagne, de Grande Bretagne et des États-Unis ont accéléré
le départ en exil du Chah et l’avènement de Khomeiny, **les grandes puissances
ont toujours agi, sans surprise, en faveur de leurs propres intérêts
contre ceux des peuples d’Iran**.

A l’opposé des solutions imposées de l’extérieur, nous défendons une véritable
campagne de solidarité internationale avec toutes celles et ceux qui se
mobilisent en Iran pour en finir avec la République Islamique.


**Être à la hauteur de la détermination et du courage du peuple iranien**

L'issue du soulèvement en cours sera déterminante pour les peuples de la
région et du monde.
**Il est donc de notre responsabilité, à la mesure de nos moyens, d’aider
le soulèvement « Femme, Vie, Liberté » à réaliser ses aspirations émancipatrices.**

En effet, **la machine à répression qu’est la République Islamique ne sera
pas brisée sans une puissante campagne internationale et sans une mobilisation
forte des opinions mondiales**.

.. include:: ../revendications/revendications_2023_03_07.txt


Vendredi 20 janvier 2023 **Appel au rassemblement de soutien aux otages francais en iran le 28 janvier 2023** #FreeIranOstages
===================================================================================================================================

- :ref:`otages_2023_01_20`

.. include:: ../repression/otages/liste_2023_02_09.txt

La LDH appelle au rassemblement de soutien le 28 janvier 2023, à 14h00, à Paris

**Actuellement, sept Français sont détenus en Iran pour des raisons fallacieuses**.

Détenus de façon arbitraire, pour certains accusés d’espionnage par la
République islamique d’Iran, ils sont privés des droits les plus élémentaires,
à commencer par le droit à une instruction judiciaire et un procès dignes
de ces noms.

Nous, familles et proches des otages français détenus en Iran, souhaitons
tout d’abord alerter sur le sort injuste et les conditions de détention
inhumaines qui leur sont infligés.

Privés de contacts avec leurs proches depuis des mois et placés à
l’isolement pour certains, la santé physique et psychologique des otages
français se dégrade.

Nous demandons instamment leur libération et leur rapatriement, puisqu’ils
sont innocents et accusés à tort.

C’est pourquoi, nous vous invitons à vous joindre à notre rassemblement
symbolique et pacifique le samedi 28 janvier, à 14h00, sur le parvis des
Droits de l’Homme à Paris, en soutien aux otages français détenus en Iran.

Familles et comités de soutien de Fariba Adelkhah, Benjamin Brière et
Cécile Kohler



Jeudi 19 janvier 2023 OTAGES FRANÇAIS détenus en #Iran, MANIFESTANTS IRANIENS ciblés en #France et MENACES DE MORT contre les journalistes de @Charlie_Hebdo
===============================================================================================================================================================

- :ref:`otages_2023_01_19`

OTAGES FRANÇAIS détenus en #Iran, MANIFESTANTS IRANIENS ciblés en #France
et MENACES DE MORT contre les journalistes de @Charlie_Hebdo_, la France
est devenue la nouvelle cible privilégiée de la République islamique.


.. figure:: ../actions/2023/01/19/images/le_point.png
   :align: center
   :width: 300



Lundi 16 janvier 2023 **RASSEMBLEMENT D’AMPLEUR de manifestants iraniens ce lundi 16 janvier devant le parlement européen à Strasbourg**
=================================================================================================================================================

- :ref:`strasbourg_2023_01_16`


RASSEMBLEMENT D’AMPLEUR de manifestants iraniens ce lundi 16 janvier 2023
devant le parlement européen à Strasbourg dans le but de pousset le
Conseil européen à placer les Gardiens de la révolution iraniens sur la
liste des organisations terroristes de l’UE.


.. figure:: ../actions/2023/01/16/images/dessin_strasbourg_shah_d_iran.jpg
   :align: center


Lundi 16 janvier 2023 **La tour Eiffel affiche le slogan Femme Vie Liberté en soutien au peuple iranien**
=============================================================================================================

- :ref:`tour_eiffel_2023_01_16`

.. figure:: ../actions/2023/01/16/images/femme_vie_liberte.png
   :align: center
   :width: 300

Ce soir, la Tour #Eiffel s’illumine du slogan : #FemmeVieLiberté

A mes sœurs, à mes frères d’#Iran, nous vous adressons ce message :
votre lutte pour la liberté est notre lutte à tous.

#Paris est et restera à vos côtés.

#StopExecutionsInIran
#JinJiyanAzadi


♀️✊ ⚖️ 17e rassemblement samedi 14 janvier 2023 à Grenoble **place Félix Poulat** à 14h30 **Arrêt des exécutions, annulation des condamnations à mort liberation des prisonniers politiques, expulsion de l'ambassadeur d'iran** 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2023_01_14`

.. figure:: ../actions/2023/01/14/images/annonce_rassemblement.png
   :align: center
   :width: 300

.. figure:: ../actions/2023/01/14/images/stop_pendaison_2.jpeg
   :align: center
   :width: 300

.. figure:: ../actions/2023/01/14/images/marvarid.jpeg
   :align: center
   :width: 300


**Vidéos peertube**

- https://sepiasearch.org/search?search=grenoble+iran

Discours de Z.
--------------------

- https://www.orion-hub.fr/w/5xEYhhbxbgnwmQwLJE79WN


Discours de S.
--------------------------

- https://www.orion-hub.fr/w/3cc3PRNFTRC9XABCRh3T81


Poème lu par Z.
----------------------

- https://www.orion-hub.fr/w/9AvWsUZPigwfWEqqEesHFD

::

    Nazli les tulipes ont éclot
    Et le printemps sourit
    Derrière la fenêtre de la maison
    Le vieux jasmin fleurit


    N'hésite pas
    Ne te confronte pas
    A cette mort dans l'effroi
    Mieux vaut vivre que disparaître
    Surtout au printemps.

     Nazli parle Nazli
    Nazli ne parlait pas
    Nazli était une étoile
    Qui un moment,  ici
    A brillé puis est partie
    Elle a serré les dents de rage
    Fatiguée endolori, elle est partie

    Nazli parle Nazli
    Nazli ne parlait pas
    Nazli était une pensée
    Qui a fleuri pour annoncer
    Que l'hiver sera fini
    De l'obscurité elle sortie
    Dans le sang s'est effondré et partie


Annonce de la fin de la manifestation par S.
-------------------------------------------------------

- https://www.orion-hub.fr/w/73mb8f9GrH2DxvQVjUg99d

Dimanche 15 janvier 2023 RASSEMBLEMENT solidarité pour l'iran 🌹 Paris Trocadero
====================================================================================

- :ref:`paris_2023_01_15`

.. figure:: ../actions/2023/01/15/images/annonce_2023_01_15.png
   :align: center
   :width: 300


Samedi 14 janvier 2023 2023-01-14 **Cris de rage contre le régime iranien**
===============================================================================

- :ref:`cris_de_rage_2023_01_14`

- https://www.ledevoir.com/societe/777916/cris-de-rage-contre-le-regime-iranien

.. figure:: ../actions/2023/01/14/images/cris_de_rage.png
   :align: center


Les hurlements, rauques et déchirants, pulvérisent le calme de ce samedi
après-midi d’hiver.

Au pied des bureaux de Radio-Canada, à Montréal, une dizaine de manifestants
hurlent leur colère contre le régime iranien, dans une tentative de mobiliser
l’attention médiatique sur le sujet.

Pendant cinq longues minutes, les personnes présentes, d’origine iranienne
et majoritairement des femmes, lancent des cris de rage.

La scène est angoissante. L’une d’entre elles se prostre, agenouillée
sur un carton ; d’autres fondent en larme. Le titre de la performance ?
« La colère est intraduisible. »

« Nous sommes fâchés et nous crions. Il n’y a pas besoin de traduction
pour ça », articule Soorena Noorai. La voix saisie par l’émotion, les
larmes s’agglutinant au coin des yeux, le Canado-Iranien raconte comment
il s’est senti pendant les cinq minutes qu’a duré la performance, à
laquelle il a participé.

« C’était comme si j’entendais les prisonniers en Iran. »

Depuis le meurtre de Mahsa Amini au mois de septembre dernier, la colère
des Iraniens et Iraniennes est palpable. La jeune femme a été battue à
mort puisqu’elle ne portait pas son hidjab au goût de la police des moeurs
locale.
Depuis, des milliers de ses concitoyens se sont révoltés, parfois au péril de leur vie.

L’organisme Iran Human Rights estime le nombre de personnes tuées dans
la répression des manifestants à 458, tandis que l’Organisation des
Nations Unies parle de 14 000 arrestations.

« [Le changement] va passer uniquement par le renversement du régime »,
martèle Mina Favar, Montréalaise née et ayant grandi en Iran.
« On ne veut plus l’islam politique, continue-t-elle. Ce qui se passe,
c’est une révolution séculaire et féministe. » Comme quelques dizaines
d’autres personnes, la jeune femme a assisté à la courte performance,
à quelques mètres de l’entrée de la nouvelle Maison de Radio-Canada,
au coin des rues René-Lévesque et Papineau.

« Nous protestons contre la passivité des médias et des instances nationales
et internationales qui ne couvrent pas comme il faut la situation en Iran »,
explique l’un des organisateurs quant au choix du lieu pour tenir la
performance. Celui-ci a préféré préserver l’anonymat « pour des raisons de sécurité ».

Après quelques minutes hautes en émotion passées à chanter des slogans,
comme « Femme, Vie, Liberté », les manifestants se dispersent lentement
dans le froid hivernal.

« La colère n’a pas besoin de mots, poursuit l’organisateur.
On peut l’exprimer par le cri. »



Samedi 14 janvier 2023 **L'Iran et l'Arabie saoudite s'apprêtent à rouvrir leurs ambassades respectives**
===============================================================================================================

- :ref:`arabie_saoudite_2023_01_14`

- https://www.lapresse.ca/international/moyen-orient/2023-01-13/le-chef-de-la-diplomatie-iranienne-espere-une-normalisation-avec-l-arabie-saoudite.php


(Beyrouth) Le ministre iranien des Affaires étrangères a exprimé l’espoir
d’une normalisation prochaine des relations de son pays avec l’Arabie saoudite,
lors d’une visite à Beyrouth au cours de laquelle il a rencontré le chef
du puissant Hezbollah pro-iranien.


Lors d’une conférence de presse, Hussein Amir-Abdollahian a également
salué le rapprochement en cours entre la Turquie et la Syrie, où il doit
est attendu samedi selon le quotidien progouvernemental syrien Al-Watan.

Un dialogue entre l’Iran chiite et l’Arabie saoudite sunnite avait été
amorcé à Bagdad, mais la dernière réunion remonte à avril 2022.

Le ministre iranien a brièvement rencontré son homologue saoudien en
marge d’une réunion régionale en Jordanie en décembre dernier.

« Nous accueillons favorablement un retour à des relations normales avec
l’Arabie saoudite, qui aboutirait à une réouverture des bureaux de
représentation ou des ambassades à Téhéran et Riyad, dans le cadre du
dialogue entre les deux pays qui doit se poursuivre », a déclaré M. Abdollahian.

Ses déclarations interviennent alors que l’Iran, secoué par une vague
de manifestations, accuse ses « ennemis » menés par les États-Unis,
d’attiser les protestations.

En novembre, Téhéran avait appelé Riyad à changer son comportement « inamical »
et menacé les voisins de l’Iran, dont l’Arabie saoudite, de représailles
contre toute tentative de déstabilisation du pays.

Les deux poids lourds de la région ont rompu leurs liens depuis 2016 et
soutiennent des parties rivales dans plusieurs conflits dans la région,
notamment au Yémen.
L’Iran a une influence prépondérante en Irak et au Liban et soutient
militairement et politiquement le régime en Syrie.


Vendredi 13 janvier 2023 **international consensus over designating Iran’s IRGC as a terrorist organization**
====================================================================================================================

- :ref:`irgc_terrorist_2023_01_13`


ICYMI: As international consensus over designating Iran’s :term:`IRGC` as a
terrorist organization is growing, over 100 members of the European
Parliament call for proscribing the Guards in its entirety.


Mardi 12 janvier 2023 **Révolte féministe qui se déroule en Iran et ses conséquences pour les droits des femmes**
===================================================================================================================

- :ref:`femmes_2023_01_12`
- https://open.spotify.com/episode/5Ymll2QpImnloIl7ttEfyf?si=52a6caa48a8d4d46&nd=1

Depuis le mois de septembre 2022, les différentes manifestations en Iran
ont été au cœur de l’actualité, sensibilisant la communauté internationale
aux importantes violations des droits des femmes ayant lieu au sein du
pays.

Bien que l’élection d’Ebrahim Raïssi à la présidence de l’État, ainsi
que la mise en place de ses mesures liberticides, aient été décrites
comme le point de départ de la révolte féministe en Iran, cet évènement
n’a fait qu’embraser la relation déjà très tendue entre la population
iranienne et le gouvernement iranien à la politique très conservatrice.

Afin d'explorer les spécificités de ces soulèvements sans précédents et
leurs conséquences structurelles, nous avons la chance d’avoir avec nous
aujourd’hui, Asal Bagheri, docteure en Sémiologie, spécialiste du
cinéma iranien et enseignante-chercheuse à l'université Cergy Paris.


Mercredi 11 janvier 2023 **Details of 121 Kurdish people killed during Mahsa (Jina) Amini protests in Iran**
================================================================================================================

- :ref:`morts_kurdes_2023_01_11`


.. figure:: ../actions/2023/01/11/images/121_morts.png
   :align: center


Details of 121 Kurdish people killed during Mahsa (Jina) Amini protests in Iran

According to Iran Human Rights, a total of 481 people have been killed
so far during the Mahsa (Jina) Amini protests across Iran, including in
Iran’s Kurdish region.

KHRN data of confirmed deaths of Kurdish people shows that 121 Kurds
have been killed by Iranian security forces during the protests.

**KHRN has verified this information via multiple sources**.


Mardi 10 janvier 2023 **Les avocats du monde entier, et aujourd’hui ceux du barreau de ROUEN, se tiennent aux côtés de leurs consœurs et de leurs confrères iraniens**
===========================================================================================================================================================================

- :ref:`soutiens_avocats_2023_01_10`


.. figure:: ../actions/2023/01/10/images/compte_twitter.png
   :align: center
   :width: 300

   https://nitter.manasiwibi.com/Iranjustice1401

**Annonce**

- https://nitter.manasiwibi.com/Iranjustice1401/status/1612823147731980289#m

Nous, avocats membres du collectif Iran Justice, appelons les Confrères
partout dans le monde à soutenir nos Confrères iraniens par une courte
vidéo individuelle, collective, ou une photo, pour nous tenir à leur cotés.

La profession d’avocat internationalement solidaire.

.. figure:: ../actions/2023/01/10/images/annonce_2023_01_10.png
   :align: center
   :width: 500



Lundi 9 janvier 2023 **Le pape François condamne l’Iran pour son recours à la peine de mort contre des manifestants**
======================================================================================================================================

- :ref:`pape_francois_2023_01_09`

.. figure:: ..//actions/2023/01/09/images/pape_francois.png
   :align: center
   :width: 300

   https://fr.wikipedia.org/wiki/Fran%C3%A7ois_(pape)



CSDHI – Le pape François a condamné pour la première fois, lundi 9 janvier 2023,
**la peine de mort** et l’exécution de manifestants par l’Iran dans son
traditionnel discours du Nouvel An aux diplomates.

Il a déclaré que la **guerre en Ukraine** était « un crime contre dieu et **l’Humanité.»**

Le souverain pontife a fait ces remarques dans un discours aux diplomates
accrédités auprès du Vatican, son aperçu au début de la nouvelle année
qui est désormais connu de manière informelle comme son discours sur
**«l’état du monde»**.

Son discours de huit pages en italien, lu devant les représentants de la
plupart des 183 pays accrédités auprès du Vatican, a passé en revue toutes
les zones de conflit du monde, y compris celles d’Afrique, du Moyen-Orient
et d’Asie.

Toutefois, la principale nouveauté du discours prononcé dans la salle
des Bénédictions du Vatican a été **la rupture du silence sur les troubles
qui secouent l’Iran depuis la mort, en septembre dernier, d’une Iranienne
kurde de 22 ans, Mahsa Amini, en garde à vue**.

**«Le droit à la vie est également menacé là où la peine de mort continue
d’être imposée, comme c’est le cas ces jours-ci en Iran, à la suite des
récentes manifestations réclamant un plus grand respect de la dignité
des femmes»**, a-t-il ajouté.

:ref:`Quatre manifestants ont été exécutés <executions>` dans le cadre de la vague de protestations
populaires dans la République islamique.

**«La peine de mort ne peut être employée pour une prétendue justice d’État,
car elle ne constitue pas un moyen de dissuasion et ne rend pas justice
aux victimes, mais ne fait qu’alimenter la soif de vengeance»**, a déclaré François.

**Il a ensuite réitéré son appel à mettre fin à la peine de mort dans le
monde entier**, affirmant qu’elle est « toujours inadmissible car elle
porte atteinte à l’inviolabilité et à la dignité de la personne».

Le pape François a déclaré que de nombreux pays ne respectaient que du bout des
lèvres les engagements qu’ils avaient pris en matière de droits humains
et il a **appelé au respect des femmes, affirmant qu’elles étaient encore
largement considérées comme des citoyens de second rang, soumises à la
violence et aux abus.**

**« Il est inacceptable qu’une partie d’un peuple soit exclue de l’éducation,
comme cela arrive aux femmes afghanes »**, a-t-il déclaré.

François a évoqué « la guerre en Ukraine, avec son cortège de morts et
de destructions, avec ses attaques contre les infrastructures civiles
qui font que des vies sont perdues non seulement par des tirs et des
actes de violence, mais aussi par la faim et le froid glacial ».

Il a ensuite immédiatement cité une constitution du Vatican, affirmant
que «tout acte de guerre visant à la destruction aveugle de villes
entières ou de vastes zones avec leurs habitants est un crime contre
dieu et **l’Humanité** qui **mérite une condamnation ferme et sans équivoque.»**

Faisant référence à la crise des missiles de Cuba en 1962, il a déclaré :
« Malheureusement, aujourd’hui aussi, la menace nucléaire est élevée,
et le monde ressent à nouveau la peur et l’angoisse. »

Le pape a réitéré son appel à une interdiction totale des armes nucléaires,
**affirmant que même leur possession pour des raisons de dissuasion est «immorale.»**

Lundi 9 janvier 2023 **La petite communauté de juifs d’Iran (environ 10 000 personnes) se sent en insécurité**
==================================================================================================================

- :ref:`juives_juifs_iran_2023_01_09`

Le citoyen juif d’Iran, El Nathan Massih Isrealian, arrêté fin octobre
après le soulèvement populaire, a été libéré aujourd’hui sous caution
selon @hra_news.

La petite communauté de juifs d’Iran (environ 10 000 personnes) se sent
en insécurité suite à la répression du soulèvement populaire, étant donné
le passif très hostile du régime envers elle.

Dès le début du soulèvement le 22 septembre 2022, l’association des juifs
d’Iran a demandé aux pratiquants de ne pas venir aux synagogues après
17h jusqu’à nouvel ordre.


Lundi 9 janvier **List of 109 Protesters at Risk of Execution, Death Penalty Charges or Sentences; At Least 481 Protesters Killed**
======================================================================================================================================


:ref:`list_protesters_2023_01_09`


Iran Human Rights (IHRNGO); January 9, 2023:


.. figure:: ../images/logo_ihrngo.png
   :align: center
   :width: 300

**At least 481 people including 64 children and 35 women**, have been killed
by security forces in the current nationwide protests.

The death toll increase relates to recently verified cases from the first
two months of the protests.

Furthermore, **at least 109 protesters are currently at risk of execution,
death penalty charges or sentences**.

This is a minimum as most families are under pressure to stay quiet,
**the real number is believed to be much higher**.

Publishing this report, Iran Human Rights draws Iranian civil society
and the international community’s attention to the **intensification of
repression through arbitrary arrests, physical torture, sexual assault
and rape in detention and the mass issuance of sentences**.

There have been enough cases reported throughout the country to conclude
that it is not merely isolated incidents but a **systematic policy by the
government**.

It also expresses its serious concern about the widespread arrests taking
place in Sistan and Baluchistan province which according to official
reports, has reached 100 people.
Many of those arrested were branded as “illegal aliens” and as many
Baluch citizens are deprived of having birth certificates, they are
subject to double the government oppression.

Director Mahmood Amiry Moghaddam said: “Many lesser known protesters,
especially in Sistan and Baluchistan province are under torture and at
risk of death penalty sentences.
**Saving their lives through mass campaigns and international pressure,
must be one the main priorities**.

New punitive measures should be taken against repressive organisations
like the :term`IRGC` and the Office of Khamenei in response to the
recent executions, along with increasing protests and campaigns inside
and outside the country, which can be effective steps to save their lives.”

According to information obtained by Iran Human Rights, at least 481
people including 64 children have been killed by security forces in the
nationwide protests so far.

**Of the 64 children, nine were girls**.

They were all under 18 years of age, but have not all been verified
through document evidence. Iran Human Rights is working to obtain confirmation
of their ages.

The aforementioned numbers only relate to protests on the streets.

The protesters executed and those that have died under suspicious circumstances
(include alleged suicides) shortly after release, are not included in
these statistics.


♀️✊ ⚖️ 16e rassemblement samedi 7 janvier 2023 à Grenoble **place Félix Poulat** à 14h30 **Solidarité avec le peuple iranien; Femme Vie Liberté; En mémoire des victimes du vol PS752**
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2023_01_07`

.. figure:: ../actions/2023/01/07/images/annonce_rassemblement.jpeg
   :align: center
   :width: 300

.. figure:: ../actions/2023/01/07/images/maryvonne_matheoud.jpeg
   :align: center
   :width: 300


.. figure:: ../actions/2023/01/07/images/performance.png
   :align: center


Discours de Z. sur le prix Simone de Beauvoir 2022
---------------------------------------------------------

- https://www.orion-hub.fr/w/83KeQdokNddb2zdWjsaLBD

Discours de S.
---------------------------------------------------------

- https://www.orion-hub.fr/w/vXRxrtu3FRXWtQYaq8VxAq


Samedi 7 janvier 2023 : exécution de **Mohammad Hosseini** et **Seyed Mohammad Mehdi Karami**
==============================================================================================

- :ref:`mohammad_mehdi_karami_2023_01_07`
- :ref:`seyed_mohammad_hosseini_2023_01_07`


Mohammad Mehdi Karami |MehdiKarami|
--------------------------------------

.. figure:: ../repression/executions/2023/01/07/mohammad-mehdi-karami/images/mohammad_mehdi_karami.png
   :align: center
   :width: 300


.. figure:: ../repression/executions/2023/01/07/mohammad-mehdi-karami/images/mohammad_mehdi_karami_medailles.png
   :align: center
   :width: 300

Il n’avait que 22 ans. Champion de Karaté, il avait été condamné à mort
le 4 décembre 2022 pour avoir tué un milicien bassidji le 3 novembre à l’issue
d’un simulacre de procès.

Il a été pendu ce samedi 7 janvier 2023 à l’aube en #Iran.



#SeyedMohammadHosseini |MohammadHosseini|
-----------------------------------------------

.. figure:: ../repression/executions/2023/01/07/seyed-mohammad_hosseini/images/seyed_mohammad_hosseini.png
   :align: center
   :width: 300


.. figure:: ../repression/executions/2023/01/07/seyed-mohammad_hosseini/images/seyed_mohammad_hosseini_2.png
   :align: center
   :width: 300

Il s'appelait #SeyedMohammadHosseini. Extorqués sous la torture, ses aveux
lui ont valu d’être condamné à mort le 4 décembre 2022 pour avoir tué
un milicien bassidji en marge d’une manifestation le 3 novembre à Karaj
(ouest de Téhéran). Il a été pendu ce samedi 7 janvier à l’aube



Jeudi 5 janvier Fermeture de l'**Institut français de recherche en Iran (IFRI)**
==================================================================================

- :ref:`ifri_2023_01_05`

L'Iran a décidé de fermer jeudi 5 janvier 2023 l'Institut français
de recherche en Iran (IFRI, spécialisé dans l'archéologie et les sciences
humaines) en réponse aux caricatures contre l'ayatollah Khamenei publiées
mercredi par Charlie_Hebdo


Mercredi 4 janvier le Prix Simone-de-Beauvoir pour la liberté des femmes 2023 est décerné aux femmes iraniennes, à la mémoire de Mahsa Amini
===============================================================================================================================================

- :ref:`simone_de_beauvoir_2023_01_04`


.. figure:: ../actions/2023/01/04/images/page_sdb.png
   :align: center
   :width: 300

   https://www.prixsimonedebeauvoir.com/

Le Prix Simone-de-Beauvoir pour la liberté des femmes 2023 est décerné
aux femmes iraniennes, à la mémoire de Mahsa Amini.

Il sera remis le lundi 9 janvier 2023, à 11h00, à la Maison de l’Amérique
latine, 217, boulevard Saint-Germain, Paris VII.

Le prix Simone de Beauvoir pour la liberté des femmes a été fondé par
Julia Kristeva en 2008, à l’issue du colloque international organisé à
Paris pour le centenaire de Simone de Beauvoir (9 janvier 1908- 14 avril 1986).

Julia Kristeva, la fondatrice, a présidé le prix de 2008 à 2011.
Josyane Savigneau lui succède, puis Sihem Habchi en 2017.
Depuis octobre 2019, Sylvie Le Bon de Beauvoir est présidente, et Pierre
Bras est Délégué général du jury.

Le prix, doté, est remis chaque année le 9 janvier, jour de la naissance
de Simone de Beauvoir.

Ce prix est décerné à une personne ou une association, à une œuvre ou une
action qui, partout dans le monde - dans tous les domaines, droit, travail,
éducation, recherche, littérature, vie quotidienne, militantisme... - défend
et fait progresser la liberté des femmes, jamais définitivement acquise.

Communiqué de presse
----------------------

- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
- 🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)


« Femme, Vie, Liberté ! » Cet appel résonne dans les rues en Iran et à
travers le monde depuis la mort de Mahsa Amini, le 16 septembre 2022 à
Téhéran.

L’étudiante de 22 ans, originaire du Kurdistan, avait été arrêtée trois
jours plus tôt par la « police des mœurs » parce que quelques boucles
de cheveux s’échappaient de son voile.
Accusée par les policiers de « port de vêtements inappropriés », elle est
emprisonnée, rouée de coups, sombre dans le coma et meurt à l’hôpital.

La nouvelle de sa mort déclenche un mouvement insurrectionnel sans
équivalent. Des femmes enlèvent leur voile en public, se coupent les
cheveux, manifestent dans les rues.
Parti du refus de porter le voile, le mouvement de révolte des femmes
contre la dictature islamique est rejoint par les jeunes Iraniens à travers
tout le pays. Il ne recule pas malgré la terreur, les tortures, les viols,
les condamnations à mort et les pendaisons.

Quand à son arrivée au pouvoir en 1979 l’ayatollah Khomeiny avait rendu
le voile obligatoire, les Iraniennes étaient descendues par milliers dans
les rues.
Simone de Beauvoir avait soutenu leur révolte. En 2009, le prix Simone-de-Beauvoir
pour la liberté des femmes a été attribué, sur proposition de l’écrivaine
Chahla Chafiq, alors membre du jury, au collectif «One MillionSignatures »
qui avait lancé une pétition pour faire supprimer les lois discriminatoires
envers les femmes en Iran.
Il s’agissait d’une campagne audacieuse et originale pour « faire avancer l’égalité ».

La poétesse iranienne Simin Behbahani avait reçu le prix, à Paris, au nom
de ce Collectif

Chahla Chafiq, qui participe à cette remise du prix 2023, est l’autrice
du Rendez-vous iranien de Simone de Beauvoir (2019). Dans cet essai,
elle montre l’influence actuelle de la pensée de Simone de Beauvoir dans
la lutte des Iraniennes contre l’oppression culturelle et religieuse de
l’Etat islamique.

Le cri de révolte **Femme, Vie, Liberté !** est repris par des dizaines
de milliers de manifestantes et manifestants. La révolte des femmes
devient le combat de toute une génération pour la liberté : c’est
cette extension révolutionnaire que le jury soutient en attribuant son
**prix 2023 aux Iraniennes, à la mémoire de Mahsa Amini**.

Après la cérémonie, un DEBAT aura lieu à 18h **Femme, Vie, Liberté : Iran, une révolution existentielle ?**

Auditorium du Monde
67-69, avenue Pierre Mendès-France – 75013 Paris
Avec Virginie Larousse, journaliste au Monde, Chahla Chafiq, écrivaine
et sociologue, Fawzia Zouari, présidente du parlement des écrivaines
francophones, Farzaneh Milani, écrivaine et universitaire d’origine
iranienne et Sedef Ecer, romancière et dramaturge franco-turque.

Lundi 2 janvier **Greve de la faim de 15 femmes dans la prison de Katchoï**
===============================================================================

- :ref:`greve_faim_2023_01_02`


.. figure:: ../actions/2023/01/02/images/les_15_femmes.jpg
   :align: center
   :width: 500

Depuis hier elles ne mangent rien et elles ne boivent rien.

Une grève de la faim incompatible avec l’état de santé de la plupart de
ces 15 femmes détenues à la prison de Katchoï. C’est pour réprimer le
mouvement #FemmeVieLiberté qu’elles sont emprisonnées.

Say their names👇🏼

#FatemehJamalpour #NiloufarKerdouni #FatemehNazarinejad #AnsiehMousavi
#JasmineHajmirza #NiloufarShakeri #ArmitaAbbasi #ElhamModaressi #FatemehHarbi
#SomayeMasoumi #HamidehZeraie #MarziehMirghasem #MaedehSohrabi #FatemehMosallah
#ShahrzadDerakhshan


♀️✊ ⚖️ 15e rassemblement samedi 31 décembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarité avec le peuple iranien annulation des condamnations à mort, hommage à Mohammad Moradi** 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_12_31`

.. figure:: ../actions/2022/12/31/images/annonce_rassemblement.png
   :align: center
   :width: 300


.. figure:: ../actions/2022/12/31/images/mohammad_moradi.jpeg
   :align: center
   :width: 600


Mercredi 28 décembre 2022 **Arrestation de Miryoussef Younesi, le père de Ali Younesi**
========================================================================================

- :ref:`miryoussef_younesi_2022_12_28`


.. figure:: ../repression/arrestations/2022/12/28/images/ali_et_miryoussef_younesi.png
   :align: center
   :width: 400


- https://nitter.manasiwibi.com/arminarefi/status/1608366645403082753#m

NON CONTENT d’avoir arrêté et condamné à 16 ans de prison au mois d’avril 2022
Ali Younesi, étudiant de 22 ans de la prestigieuse université Sharif de
Téhéran, le régime iranien vient d’arrêter son père Miryoussef Younesi
mercredi 28 décembre 2022 chez lui à Shahroud (nord de l’#Iran).



Mardi 27 décembre 2022 **Suicide de Mohammad Moradi à Lyon**
=============================================================

- :ref:`mohammed_moradi_2022_12_27`


.. figure:: ../actions/2022/12/27/images/mohammad_moradi.png
   :align: center
   :width: 300


- https://kurdistan-au-feminin.fr/2022/12/28/lyon-suicide-kurde-iran/

LYON – Mohammad Moradi, un jeune Kurde d’Iran étudiant à Lyon, s’est suicidé
le 26 décembre 2022 en se jetant dans le Rhône en guise de protestation contre
la répression sanglante menée par les mollahs iraniens depuis le début
des protestations anti-régime. « La police attaque les gens [en Iran],
on a perdu beaucoup de fils […]

Mardi 27 décembre 2022 **Sara Khadem et Atousa Pourkashyan se sont présentées au championnat du monde au Kazakhstan sans voile**
========================================================================================================================================

- :ref:`sara_khadem_et_atousa_pourkashyan_2022_12_27`

#womanlifefreedom #sarakhadem #atousapourkashiyan #mahsaamini #iranrevolution

.. figure:: ../actions/2022/12/27/images/sara_khadem_et_atousa_pourkashyan.png
   :align: center
   :width: 300

Sara Khadem et Atousa Pourkashyan, maîtres d’échecs iraniennes, se sont
présentées hier au championnat du monde au Kazakhstan sans voile.

Les femmes iraniennes sont obligées de le porter dans des contextes
internationaux en sport.
On le sait, les représailles peuvent être terribles.

Mais le regard de Sara Khadem raconte le courage plus que la peur, celui
que donne la certitude de faire ce que l’on doit.


♀️✊ ⚖️ 14e rassemblement samedi 24 décembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarité avec le peuple iranien, libération des prisonniers politiques et annulation des condamnations à mort** 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_12_24`

.. figure:: ../actions/2022/12/24/images/affiche_rassemblement.png
   :align: center
   :width: 300

ce samedi 24 décembre 2022 cela fait donc 3 mois 1 semaine et 1 jour que
Jina a été assassinée. (99e jour)

.. figure:: ../actions/2022/12/24/images/paix_au_rojava.jpeg
   :align: center
   :width: 400


.. figure:: ../actions/2022/12/24/images/fusion_emotion.jpeg
   :align: center
   :width: 400

2022-12-22 **Report: Targeted and Rare Crackdown of Human Rights Defenders in Iran Protests**  #IranProtest2022
================================================================================================================

- :ref:`iran_hr_2022_12_22`
- https://iranhr.net/media/files/HRD_2022_Eng_2bMr713.pdf


Iran Human Rights (IHRNGO); December 22, 2022: Human rights defenders have
always been under repression by the Islamic Republic.

However, their targeted and systematic repression has dramatically increased
in recent years. In its 2019/2020 Human Rights Defenders report, Iran Human Rights
documented 54 human rights defenders which doubled in 2021 with more
than 100 human rights defenders.

This upward trend continued in 2022, reaching a peak with the current
report documenting 218 human rights defenders who have been arrested and
harassed in less than three months.
While the Islamic Republic is considered one the biggest repressive states
in the world, this report shows that the targeted crackdown on human rights
defenders, was unprecedented following the outbreak of nationwide protests,
even for the Islamic Republic.

Iran Human Rights Director, Mahmood Amiry-Moghaddam said: “Three months
have passed since the beginning of the nationwide protests in Iran.
Changing the political system is the core protest demand but the demands
of those who take to the streets at the risk to their lives, is even
greater than a change of system.

The people of Iran have stood up to claim all the rights millions have
been deprived of for decades. All protesters who took to the streets
are themselves human rights defenders according to the UN definition.

Human rights defenders play an important role in the **‘Woman, Life, Liberty’ revolution.”**


This report shows that the Islamic Republic’s response to any human rights
activism is violent crackdowns, and that the situation of human rights
defenders has significantly deteriorated compared to last year.

Many activists were unlawfully arrested “preemptively” at their homes
and locations other than protests.
Activists like Golrokh Irayi-Ebrahimi, Milad Fadayi, Saba Sherdoost and
Majid Tavakoli.

A significant number of defenders also suffer from illnesses. Arash Sadeghi
was arrested despite suffering from a rare form of cancer.
Rapper Toomaj Salehi is facing charges that carry the death penalty for
his protest songs.

There are also teachers who were arrested or faced reprisals for refusing
to hand over names of protesting/striking students and pupils, with one
losing her life due to a heart attack from the pressure.

Lawyers willing to represent protesters and other human rights defenders
have been particularly targeted at an unprecedented rate with at least
46 subjected arrests or legal action.

Iran Human Rights calls on civil society worldwide to support their
colleagues who are fighting for their fundamental rights under such
difficult circumstances in Iran.

Artists, Bar Associations, workers’ unions, journalists, women’s activists
and others can help save their lives by being their voices.
We call on well-known figures to take coordinated action by each following
up on the situation of one of the people listed in this report, and to
be their voice in interviews and public appearances.

Many of the human rights defenders are behind bars in dangerous conditions;
international pressure and raising the political cost of the repression,
is the only way to reduce the pressure on them.



Mercredi 21 décembre 2022 **L'appel des 19 20 et 21 décembre 2022**
====================================================================

- :ref:`appel_2022_12_21`

**La ville kurde de #Kamyaran est en grève**

- https://masthead.social/@NaderTeyf/109551776809842792

Au troisième jour de l'appel à la grève et à la manifestation, la ville
kurde de #Kamyaran est en grève. Un rapport de netblocks.org montre que
le régime a pratiquement coupé Internet au Kurdistan et les informations
passent difficilement.

Les lycéennes de #Kermanchah et #Ahvaz (le clip) ont manifesté ce matin.

On entend dans cette vidéo:

- "Mort au dictateur",
- "Liberté, liberté, liberté"
- et "Mauvais et voyeur, c'est toi/Femme libre, c'est moi."

#Mahsa_Amini

Mercredi 21 décembre 2022 **Contre la répression en Iran : Résolution de l’Assemblée des délégué-e-s CGAS (Genève)**
=======================================================================================================================

- :ref:`solidarite_suisse_2022_12_21`


.. figure:: ../actions/2022/12/21/images/cgas_geneve.jpg
   :align: center

   http://www.cgas.ch/SPIP/?lang=fr


Contre la répression en Iran : Résolution de l’Assemblée des délégué-e-s CGAS
-------------------------------------------------------------------------------

Depuis quatre mois, une large part des iranien.ne.s occupent les rue dans
l’ensemble du pays. Initié par des centaines de milliers de femmes, ce
mouvement entraîne aujourd’hui la majorité de la population de l’ Iran.

Il vise à se débarrasser d’un régime, celui des mollahs, autoritaire,
liberticide, assassin.

Durant l’année qui se termine, plus de 500 personnes ont été exécutées
en Iran.
Encore ces jours, des manifestantes et manifestants sont pendu.e.s en
public, sans procès, coupables d’avoir, par leurs déclarations, par leur
participation aux manifestations, osé défier le régime.

Après les femmes, après les étudiant.e.s, ce sont aujourd’hui les salariées
et les salariés qui se mettent en grève. Elles, ils, méritent toute la
solidarité du mouvement syndical à l’échelle mondiale.

En Suisse, le récent congrès de l’Union syndicale suisse a acclamé une
position que la CGAS fait sienne et qui :

- Affirme la solidarité totale du mouvement syndical avec les mouvements
  populaires en Iran et avec leurs revendications démocratiques,
  économiques et sociales ;
- Condamne le climat de terreur instauré par la répression -exercée
  notamment à coups d’exécutions sommaires- par les autorités iraniennes ;
- Exige du gouvernement fédéral :

    - l’expulsion du personnel diplomatique de la république islamique d’Iran et
    - la saisie des avoirs directs et indirects des responsables iraniens
      déposés en Suisse ;

- S’engage à fournir dans la mesure de ses possibilités de l’aide matérielle
  aux grévistes en Iran ;
- Appelle ses membres à participer aux moments de mobilisation sur la
  Place des Nations qu’organise régulièrement la communauté iranienne à Genève.


Mardi 20 décembre 2022 **L'appel des 19 20 et 21 décembre 2022**
====================================================================

- :ref:`appel_2022_12_20`


Des villes kurdes sont en grève
------------------------------------

- https://masthead.social/@NaderTeyf/109545490270897430

Au deuxième jour, des villes kurdes de #Sanandaj, #Saghez (`le clip <https://cdn.masto.host/mastheadsocial/media_attachments/files/109/545/487/405/992/477/original/b4da869cb7a589ba.mp4>`_) et
#Kamyaran sont en grève. Une administration de contrôle du régime menace
les commerçants de fermer définitivement leurs magasins s'ils continuent
le mouvement.

#Mahsa_Amini

Meurtres de Hossein Saïdi, Modjahed Kourkour et Mahmoud Ahmadi
----------------------------------------------------------------

- https://masthead.social/@NaderTeyf/109548134009960971

Des combattants de la liberté de la ville d'#Izeh qui savaient être
poursuivis par la police politique des mollahs en #Iran se sont
réfugiés dans un village près de cette ville.

La police a finalement trouvé leur cachette. Elle a attaqué la maison
par des armes semi-lourdes de guerre et a tué 3 d'entre eux:
Hossein Saïdi, Modjahed Kourkour et Mahmoud Ahmadi.

Deux autres ont été arrêtés.
#Mahsa_Amini

Cette année est l'année du sang/Seyed Ali (Khamenei) est renversé
-------------------------------------------------------------------

- https://masthead.social/@NaderTeyf/109547980685807024

Deux manifestations sont signalées à #Machhad et #Ispahan.

Ici dans le quartier populaire Salsabil de #Téhéran le jeune précise la
date du jour et les autres scandent:

- "Mort au dictateur"
- et "Cette année est l'année du sang/Seyed Ali (Khamenei) est renversé."

#Mahsa_Amini


Lundi 19 décembre 2022 **L'appel des 19 20 et 21 décembre 2022**
====================================================================

- :ref:`appel_2022_12_19`

- https://masthead.social/@NaderTeyf/109541736693286231

Une vidéo montre l'attaque des forces de répression dans une station du
métro de #Téhéran contre les gens qui scandaient:"Nous ne voulons pas
de régime islamique."

Celle-ci concerne la ville de :ref:`#Racht <racht>` ce soir et le slogan simple et
continu de **Liberté, liberté, liberté.**



Lundi 19 décembre 2022 **Campagne internationale de solidarité avec la lutte des femmes iraniennes !**
========================================================================================================

- :ref:`cnt_so_2022_12_19`
- https://cnt-so.org/wp-content/uploads/2022/12/Solidarite-Iran-Dec-22.pdf

.. figure:: ../actions/2022/12/19/images/solidarite_iran.png
   :align: center
   :width: 300

   https://cnt-so.org/wp-content/uploads/2022/12/cnt_so_soutien_iranien_nes.png

Débuté il y a plus de trois mois, le mouvement de révolte populaire en Iran
contre la dictature cléricale et son cortège de misère, continue malgré
une répression féroce du régime qui a fait des centaines de victimes et
provoqué l’incarcération de milliers d’autres dont certain-nes sont
condamné-es à mort et risquent l’exécution rapidement.

**La CNT-SO salue la lutte exemplaire des femmes qui refusent la main-mise
de la dictature cléricale sur leurs corps et leurs vies et sont à
l’avant-garde du mouvement populaire qui déferle sur ce pays.**

**La CNT-SO appelle à la solidarité internationaliste avec ce mouvement
révolutionnaire en particulier face à la répression**.


La Confédération CNT-SO apporte son soutien aux manifestants-es iranien-nes

Notre Confédération exprime sa totale solidarité à la révolte des femmes
iraniennes, aux grèves qui s’organisent.
**Nous faisons nôtre les revendications de la lutte du peuple iranien :
le refus de l’oppression des femmes par le pouvoir théocratique, la fin
de la dictature, la liberté d’expression, d’organisation, de grève,
de manifestation.**

EN IRAN COMME AILLEURS : NON À L’OPPRESSION, NON À L’EXPLOITATION !
VIVE LA LUTTE DES FEMMES ET DE LA CLASSE OUVRIÈRE ! SOLIDARITÉ INTERNATIONALE !

Campagne internationale de solidarité

Ecrire:

En Iran, les autorités ont déjà exécuté deux manifestants. 26 personnes
risquent de subir le même sort dans les jours à venir.
Ces exécutions doivent cesser et toute condamnation à mort doit être
annulée #StopExecution StopExecutionsInIran !

Responsable du pouvoir judiciaire,

Gholamhossein Mohseni Ejei c/o Ambassade d’Iran auprès de l’Union européenne,
Avenue Franklin Roosevelt No. 15, 1050 Bruxelles. Belgique.
Copie à : Ambassade de la Rép. Islamique d’Iran, 4 avenue d’Iéna -75116 Paris



♀️✊ ⚖️ 13e rassemblement samedi 17 décembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarité avec le peuple iranien, libération des prisonniers politiques et annulation des condamnations à mort** 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_12_17`


.. figure:: ../actions/2022/12/17/images/annonce_rassemblement.png
   :align: center
   :width: 300


.. figure:: ../actions/2022/12/17/images/chant_fin_de_manif.png
   :align: center
   :width: 300



.. figure:: ../actions/2022/12/17/images/pancartes_dans_la_neige.png
   :align: center
   :width: 300



Vendredi 16 décembre 2022 **Le régime se venge et punit les manifestants, je reste absolument horrifié, choqué et indigné**
=================================================================================================================================

- :ref:`javad_rehman_2022_12_16`

Depuis qu'il est en poste, Javaid Rehman, rapporteur spécial de l’ONU sur
les droits de l’homme en Iran, n'a pas été autorisé à aller dans le pays.

Un pays où, chaque jour, ont lieu des "exécutions sommaires, arbitraires
et illégales", témoigne-t-il.

**Javaid Rehman est rapporteur spécial de l’ONU sur les droits de l’homme
en Iran**.

Ce professeur de droit, qui enseigne à la Brunel University de Londres,
**dénonce la violence de la répression et s’alarme de la dérive du régime**
depuis l’arrivée au pouvoir à Téhéran du président ultraconservateur Ebrahim Raïssi.

Il y a trois mois jour pour jour (16 septembre 2022), la mort d'une jeune Kurde de 22 ans,
Mahsa Amini, faisait basculer le pays dans une nouvelle période de troubles.

Comme lors des manifestations de 2009 et 2019, le régime des mollahs a
choisi la manière forte pour mater la protestation. Les morts se comptent
par centaines, plus de 450 personnes, les arrestations par milliers.

Téhéran a également recours aux exécutions pour tenter de dissuader les
jeunes Iraniens de descendre dans la rue. Et tout semble indiquer que la
répression va se poursuivre tant que le mouvement de contestation durera.


Mercredi 14 décembre 2022 **Expulsion de l'Iran de la Commission de la femme de l'ONU (CSW)**
================================================================================================

- :ref:`expulsion_com_femmes_2022_12_14`

Le Conseil économique et social de l'ONU (Ecosoc) a voté ce mercredi 14
décembre 2022 l'expulsion de l'Iran de la **Commission de la femme de
l'ONU (CSW)** en raison de la répression des manifestations par la
République islamique depuis septembre.

L'Ecosoc, qui chapeaute la CSW, s'est prononcé à 29 voix pour (16 abstentions
et huit contre) pour sortir Téhéran de cette commission pour le reste
de son mandat jusqu'en 2026, suite à l'adoption d'une résolution présentée
par les États-Unis, qui n'ont pas de relations diplomatiques avec l'Iran
depuis 1980 et qui était portée depuis deux mois par une **pétition internationale**.

LA République islamique d’#Iran exclue ce mercredi 14 décembre de la
Comission de l’ONU sur le statut des femmes par 29 voix pour, 8 voix
contre et 16 abstentions.

Décision votée par les 54 membres du Conseil économique et social de
l’ONU sur proposition des États-Unis.

.. figure:: ../actions/2022/12/14/images/les_votes.png
   :align: center
   :width: 400


Mercredi 14 décembre 2022 **Lettre de soutien des médecins français**
========================================================================

- :ref:`soutien_medecins_2022_12_14`

Lettre de soutien des médecins français
---------------------------------------------

Les droits de l’homme, l’enseignement et la pratique médicale sont
en danger en Iran.

Depuis près de trois mois et suite au décès d’une jeune femme de
22 ans, Mahsa (Jina) Amini, le monde universitaire iranien, à l’instar
de la société tout entière, est le lieu de manifestations quotidiennes
réprimées dans une violence inouïe.

Le peuple iranien tente d’exprimer son rejet de la dictature en place et
des conditions politiques, culturelles et économiques qu’elle impose
par la force.

**Or, les cris pacifiques de protestation ont été, à chaque fois, réprimés
dans le sang**.

La situation actuelle se singularise par le rôle pivot des femmes, des
étudiants et des lycéens avec des arrestations touchant une majorité
de personnes de moins de 25 ans et au moins 45 personnes en
dessous de 18 ans.

Les universités sont quotidiennement les lieux de répression de manifestations.
Les étudiants sont arrêtés au sein même de ces lieux sacrés de savoir
et de formation.

Les ambulances sont utilisées pour transporter les personnels de la
répression. Pire encore, les blessés sont traqués jusque dans les
services d’urgences où ils sont privés de soins et arrêtés.

Les médecins sont menacés et forcés d’établir des certificats en
camouflant l’origine des blessures et des décès.

De nombreuses arrestations ont eu lieu et des condamnations à mort
ou des longues peines de prison viennent d’être prononcées.
D’après plusieurs sources concordantes, plusieurs médecins refusant d’être
complices seraient arrêtés. Les secours et les soins les plus
élémentaires aux blessés seraient volontairement empêchés ou
retardés.

**Ainsi, les lieux de soins et les universités ne sont plus les enceintes
inviolables qu’elles devraient être**.

À la suite d’un procès expéditif par un tribunal et à l’encontre du
respect des droits élémentaires de l’homme, une condamnation à
mort a été récemment prononcée contre un collègue médecin
radiologue, le Dr Hamid Ghareh Hasanlou arrêté au cours d’une
manifestation. Sa femme, technicienne de laboratoire, a été
condamnée à 25 ans de prison.

Treize autres condamnations à mort ont été prononcées contre de jeunes
manifestants. L’un d’eux Mohsen Shekari, âgé de 23 ans, a été exécuté
le 9 décembre.

En tant que médecins, universitaires, alertés par nos collègues
français d'origine iranienne, nous ne pouvons rester silencieux face à
ces exactions que nous condamnons et dénonçons avec la plus
grande fermeté.

Au-delà du soutien que nous témoignons au peuple iranien, nous protestons
contre les pressions exercées sur les médecins et enseignants dans
l’exercice de leur profession et contre les arrestations massives et
arbitraires des étudiants.

**Nous demandons la suspension immédiate de toutes les condamnations à mort.**


- Pr Didier Samuel Président de la conférence des doyens des facultés de médecine
- Dr Thierry Godeau Président de la conférence des présidents de CME des centres hospitaliers
- Pr Rémi Salomon Président de la conférence des présidents de CME des centres hospitalo-universitaires


Lettre de soutien des médecins français sur la place Félix Poulat
-------------------------------------------------------------------

La vidéo sur peertube: https://www.orion-hub.fr/w/sqxUNqd7wN72QT5XNE9JLA

.. figure:: ../actions/2022/12/17/images/discours_medecin.png
   :align: center

   :ref:`soutien_medecins_2022_12_14`



Mardi 13 décembre 2022 **La doctoresse Aida Rostami, enlevée, torturée et assassinée**
=========================================================================================

- :ref:`aida_rostami`

.. figure:: ../repression/assassinats/2022/12/13/aida_rostami/images/aida_rostami.png
   :align: center
   :width: 300


La docteuresse #AidaRostami, qui venait en aide aux manifestants blessés
dans le quartier Ekbatan de Téhéran, a été enterrée par ses proches à
Gorgan, dans le nord de l’#Iran.

IRAN Aida Rostami, 1 doctoresse de Téhéran qui aidait ls blessés ds protestations,
a été enlevée par les forces du régime.

**Elle est morte sous la torture: visage écrasé, 1 bras cassé & 1 œil crevé**

#مجیدرضا_رهنورد
#StopExecutionsInIran
#JinaAmini
#IranRevolution
#IranProtests



Lundi 12 décembre 2022 **Le régime des ayatollahs a déjà exécuté par pendaison deux protestataires #Mohsen_Shekari et #Majidreza_Rahnavard**
===============================================================================================================================================

- :ref:`executions_2022_12_12`
- https://masthead.social/@NaderTeyf/109507031220180266

Le régime des ayatollahs a déjà exécuté par pendaison deux protestataires,
:ref:`#Mohsen_Shekari <mohsen_shekari_2022_10_08>` et :ref:`#Majidreza_Rahnavard <majid_reza_rahnavard_2022_12_12>`

Les étudiant.es multiplient leurs rassemblements contre la peine de mort
partout en #Iran, comme à la faculté de médecin à #Tabriz ce matin.


Dimanche 11 décembre 2022 **Soirée iranienne à Lans-en-Vercors organisée par le cinéma Le Clap**
===================================================================================================

- :ref:`isere_iran_2022_12_11`


.. figure:: ../actions/2022/12/11/images/soiree_iranienne.png
   :align: center


.. figure:: ../actions/2022/12/11/images/zara_et_zoreh.png
   :align: center

   A droite, Madame Zoreh Baharmast, présidente de la ligue des droits de l'Homme en Iran et à gauche Zara jeune iranienne



Au moment où le peuple iranien manifeste contre le régime qui l’oppresse,
Le Clap vous propose une soirée autour de deux films iraniens qui sortent
en France cet automne:

- :ref:`juste une nuit <juste_une_nuit>`
- :ref:`aucun ours <aucun_ours>`



Avant-propos
-------------------

Film iranien de Ali Asgari
Avec Sadaf Asgari, Ghazal Shojaei, Babak Karimi… | 2022 | 1h26 | VOSTF

Séance suivie d’un échange avec de jeunes iraniennes et Zoreh Baharmast,
présidente de la section grenobloise de la Ligue des droits de l’homme.


Description
----------------

Fereshteh doit cacher son bébé illégitime pendant une nuit à ses parents
qui lui rendent une visite surprise.

Son amie Atefeh l’aide. Elles se lancent dans une odyssée au cours de
laquelle elles doivent soigneusement choisir qui sont leurs alliés.

Aucun ours
------------

Jafar Panahi était libre de mouvement dans son pays au moment du tournage
de **Aucun Ours**, mais interdit de filmer.

Il est incarcéré depuis juillet 2022 pour s’être insurgé contre l’arrestation
de deux de ses confrères cinéastes :

- Mohammad Rasoulof (Le diable n’existe pas, Un homme intègre)
- et Mostafa Aleahmad.


Description
===========

Dans un village iranien proche de la frontière, un metteur en scène est
témoin d’une histoire d’amour tandis qu’il en filme une autre.

La tradition et la politique auront-elles raison des deux ?

♀️✊ ⚖️ 🌄  12e rassemblement samedi 10 décembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarité avec le peuple iranien, libération des prisonniers politiques et annulation des condamnations à mort** 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_12_10`


.. figure:: ../actions/2022/12/10/images/annonce_2022_12_10.jpg
   :align: center
   :width: 400


Jeudi 8 décembre 2022 **L'exécution de #MohsenShekari ce matin a profondément bouleversé les gens en #Iran** |MohsenShekari|
===============================================================================================================================

- :ref:`assassinat_mohsen_shekari_2022_10_08`


.. figure:: ../repression/executions/2022/12/08/mohsen-shekari/images/mohsen_shekari.jpg
   :align: center
   :width: 300

Il n'avait que participé à une barricade et blessé légèrement un bassidji
selon le pouvoir judiciaire islamique lui-même.



Mercredi 7 décembre 2022 **La journée de l'étudiant**, le troisième jour des grèves continue dans les universités
====================================================================================================================

- :ref:`appel_iran_2022_12_07`

'appel des 5, 6 et 7 décembre

Le 7 décembre 1953 le régime du Chah a tué 3 étudiants à #Téhéran.

Le régime des mollahs a tout fait pour arrêter le mouvement étudiant
depuis 11 semaines. Les étudiant.es manifestent aujourd'hui dans de
nombreuses universités en #Iran.

Les forces de répression ont attaqué certaines protestations.

Ici à l'université d'Allameh Tabataba'i, les étudiant.es scandent entre
autres:"Étudiant est éveillé/Il déteste le totalitarisme" et
"Étudiants, ouvriers/Unité, unité".

- https://cdn.masto.host/mastheadsocial/media_attachments/files/109/472/126/765/182/727/original/28ddbff74bc1ab24.mp4


Mardi 6 décembre 2022 Le deuxième jour des grèves continue dans les universités + pétrochime et ciment
=========================================================================================================


- :ref:`appel_iran_2022_12_06`


Les ouvriers ont fait plus de 4000 grèves l'année dernière en #Iran alors
que faire grève est interdit.

Deux grèves ouvrières importantes de ce 6 décembre:

- ☑️ Grève des ouvriers de la pétrochimie de #Sanandaj depuis hier,
  surtout pour augmentation de salaire et suppression des sociétés sous-traitantes;
- ☑️ Grève des ouvriers du Ciment Sepahan de #Ispahan.
  Les grévistes ont expulsé trois directeurs de cette entreprise gérée
  par les Pasdaran.

Un bilan partiel des manifestations et grèves en #Iran pour la journée du 5 décembre 2022
==============================================================================================

- :ref:`actions_iran_2022_12_05`

Un bilan partiel des manifestations et grèves en #Iran pour la journée du 5 décembre 2022

L'appel des 5, 6 et 7 décembre

Un bilan partiel des manifestations et grèves en #Iran pour la journée du 5 décembre 2022:

🔴 Manifestations:

- ✔️ Dans 13 quartiers de #Téhéran et plusieurs stations du métro;
- ✔️ Dans 19 villes de tous les coins du pays;
- ✔️ Dans 13 universités également partout dans le pays.

🔴 Grèves sans précédent depuis 43 ans:

- ✔️ Dans 24 endroits commerciaux différents de Téhéran;
- ✔️ Dans 60 villes aux quatre coins cardinaux du pays.

⬇️ La caserne des bassidjis incendiée à #Arak


♀️✊ ⚖️ 🌄  11e rassemblement samedi 3 décembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarite avec le peuple iranien et aux kurdes durement réprimé·e·s en Iran** #Grenoble #MahsaAmini #MahsaJinaAmini 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_12_03`


.. figure:: ../actions/2022/12/03/images/annonce_rassemblement_2022_12_03.png
   :align: center
   :width: 400



2022-12-  Every Day In Iran: New Execution Verdicts
==========================================================

- :ref:`ali_rakhshani_mohammad_rakhshani_2022_12_02`

.. figure:: ../repression/executions/2022/12/02/images/ali_rakhshani_mohammad_rakhshani.png
   :align: center


Ali Rakhshani and Mohammad Rakhshani, two 15 years old from Balochistan
of Iran, are sentenced to death.

**Wake up! take action today and stop the brutal regime of Iran.**


Mardi 29 novembre 2022 Blast **Iran : un peuple se lève contre un gouvernement à l'agonie**
===============================================================================================

- :ref:`blast_un_peuple_se_leve_2022_11_29`

.. figure:: ../actions/2022/11/29/images/golshiteh_farahani_les_choses_vont_sarranger.png
   :align: center


Interrogée par le quotidien 20 minutes pour savoir si la situation en
Iran allait s’arranger, la comédienne iranienne Golshiteh Farahani a
répondu qu’elle pensait que oui .

Cela ne signifiait pas qu’elle espérait que les choses se calment.

Bien au contraire. Pendant 39 ans (son âge), elle avouait avoir toujours
répondu non . Mais là, c’était oui, un grand oui .

J’ai l’impression que le gouvernement islamique est devenu une fourmi
ridicule et blessée qui a perdu toute crédibilité, un monstre qui saigne.
Et même si c’est une question de temps, même s’il saigne longtemps, il va
finalement mourir parce que cette génération de la jeunesse a arraché
sa colonne vertébrale.

Il ne peut plus marcher ajoutait-elle avant de conclure: Il va ramper
un moment puis s’écrouler. Les Iraniens ne le laisseront pas se relever .

Il était temps que Blast se saisisse de la révolution iranienne .



♀️✊ ⚖️ 🌄  10e rassemblement samedi 26 novembre 2022 à Grenoble **place Félix Poulat** à 14h30 **Solidarite avec le peuple iranien et aux kurdes durement réprimé·e·s en Iran, Syrie, Irak et Turquie** #Grenoble #MahsaAmini #MahsaJinaAmini #Kurdistan 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_11_26`


.. figure:: ..//actions/2022/11/26/images/annonce_rassemblement_2022_11_26.png
   :align: center
   :width: 300


🌄 Dimanche 27 novembre 2022 à 14h30 **débat avec Berivan Firat porte-parole des relations extérieures du Conseil Démocratique kurde en France (CDK-F)**
==============================================================================================================================================================================

- :ref:`debat_rojava_2022_11_27`


Débat avec :ref:`Berivan Firat <kurdistan_luttes:berivan_firat>` porte-parole des relations extérieures du Conseil Démocratique kurde en France (CDK-F)
(:ref:`Agit Polat <kurdistan_luttes:agit_polat>` était initialement annoncé) 🌄

Les Kurdes du Rojava mettent-ils réellement en application le confédéralisme
démocratique avec pour piliers la démocratie directe, l’émancipation
des femmes, l’écologie, et **l’inclusion de toutes les composantes ethniques
et religieuses de la société ?**

Organisé par :ref:`l’association iséroise des amis des Kurdes (AIAK)  <kurdistan_luttes:aiak>`.


Maison de Quartier Fernand Texier
161 avenue Ambroise Croizat
Saint-Martin-d'Hères


♀️ Vendredi 25 novembre 2022 Femme Vie Liberte **Journée internationale pour l'élimination de la violence à l'égard des femmes**
=====================================================================================================================================


- :ref:`femme_vie_liberte_2022_11_25`

.. figure:: ../actions/2022/11/25/images/femme_vie_liberte.png
   :align: center
   :width: 400


Il y a cinq ans, le mouvement #MeToo, fondé par l’activiste Tarana Burke
en 2006, a pris de l’ampleur et déclenché une mobilisation mondiale
créant un sentiment d’urgence en matière de prévention et d’élimination
de la violence à l’égard des femmes et des filles.

Depuis lors, une prise de conscience et un élan sans précédent ont vu le
jour grâce au travail acharné des activistes de base, des défenseuses
et défenseurs des droits humains des femmes et des défenseuses et défenseurs
des droits des survivantes dans le monde entier pour prévenir et éliminer
la violence à l’égard des femmes et des filles.

Parallèlement, il y a eu une recrudescence des mouvements anti-droits,
y compris des groupes antiféministes, ce qui a entraîné un rétrécissement
de l’espace de la société civile, une réaction hostile envers les
organisations de défense des droits des femmes et une augmentation
des attaques contre les défenseuses des droits humains, les défenseurs
des droits des femmes et les activistes des droits humains.

Soutenir et financer des organisations de défense des droits des femmes
et des mouvements féministes solides et autonomes est crucial pour
mettre fin à la violence à l’égard des femmes et des filles.

C’est pourquoi le thème de la campagne Tous UNiS pour 2022 appellera à
un soutien renforcé envers l’activisme pour prévenir la violence à
l’égard des femmes et des filles : « Tous UNiS ! L’activisme pour
l’élimination de la violence à l’égard des femmes et des filles ! »


Jeudi 24 novembre 2022 **L'ONU ouvre une enquête sur la répression en Iran**
================================================================================


- :ref:`onu_iran_2022_11_24`


Le Conseil des droits de l’homme de l’ONU a décidé jeudi, malgré l’opposition
de Téhéran et de Pékin , d’ouvrir une enquête internationale sur la répression
des manifestations en Iran après la mort de Mahsa Amini, pour rassembler
des preuves des violations et éventuellement poursuivre les responsables.

Et c’est en plein débat devant le Conseil, que l’agence de presse iranienne
Fars a annoncé l’arrestation du célèbre footballeur Voria Ghafouri,
accusé d’avoir “insulté et sali la réputation de l’équipe nationale (Team Melli)
et de s’être livré à de la propagande” contre l’Etat.

Réunis d’urgence à l’initiative de l’Allemagne et de l’Islande, les 47
Etats membres de l’instance onusienne la plus élevée en matière de droits
humains ont décidé de nommer une équipe d’enquêteurs de haut niveau pour
faire la lumière sur toutes les violations de ces droits liées à la
répression des manifestations.

Le texte a été voté à une majorité légèrement plus large qu’attendue,
malgré une manoeuvre dilatoire de dernière minute de Pékin : 25 pays
ont voté oui, six non (Arménie, Chine, Cuba, Erythrée, Pakistan et Venezuela)
et 16 se sont abstenus.

“Les responsables iraniens ne pourront pas perpétrer cette violente
répression dans l’anonymat; la communauté internationale les regarde”,
a réagi l’ambassadrice américaine Michèle Taylor.


“Le courage et la détermination des manifestants nous obligent”, a tweeté
la représentation française auprès de l’ONU, saluant également la création
de ce mécanisme d’enquête.

L’ONG Amnesty International s’est félicitée d‘“une résolution historique”
qui marque “une étape importante vers la fin de l’impunité !”, tandis
que Lucy McKernan, de Human Rights Watch, qualifiait la décision du
Conseil d‘“étape bienvenue”.


“Changement inévitable” ¶

Pendant les débats, le Haut-Commissaire de l’ONU aux droits de l’homme,
Volker Türk, dont la demande de visite en Iran est restée jusqu’à présent
lettre morte, a appelé Téhéran à “cesser” son “usage inutile et
disproportionné de la force”.

“La situation actuelle est intenable”, a prévenu M. Türk, qui réclame
“un moratoire sur la peine de mort” et demande que le gouvernement “
s’engage dans un processus de réformes car le changement est inévitable”.



🌄 Mercredi 23 novembre 2022 Rassemblement appelé par L'AIAK en solidarité avec les **kurdes d'Iran de Syrie d'Irak et de Turquie**
=====================================================================================================================================

- :ref:`soutien_kurdistan_2022_11_23`

Rassemblement appelé par L'AIAK (Association iséroise des amis des kurdes)
mercredi 23 novembre 2022 à 18h à Félix Poulat en solidarité avec les
Kurdes d'Iran, de Syrie, d'Irak et de Turquie.

Cela fait suite à la répression féroce par la République islamique d'Iran,
aux  bombardements des camps des oppositions en Irak et des bombardements
turcs sur les régions kurdes en Syrie.


Mardi 22 novembre 2022 Projection du film **Hitch : une histoire iranienne** de Chowra Makaremi
================================================================================================================

- :ref:`hitch_2022_11_22`

« Iran, 1988. Des milliers de prisonniers politiques, enfermés depuis
les lendemains de la révolution de 1979, sont massacrés.

Ces derniers des révolutionnaires opposés à Khomeini sont liquidés dans
le plus grand secret. Parmi elles et eux, Fatemeh Zarei, ma mère.
Tandis que l’État iranien nie toujours ses crimes et s’efforce encore
aujourd’hui d’en effacer les traces, le film part en quête des lieux,
des objets et des gestes qui permettront de dénouer le silence, là où
seul l’intime reste en témoignage d’une politique. »



♀️✊ ⚖️ 9e rassemblement samedi 19 novembre 2022 à Grenoble **place aux Herbes** à 14h30 **Le temps est venu ... rejoignez le mouvement mondial de la révolution** #NovembreSanglant #BloodyNovember ⁦ #ps752justice #Grenoble 📣
==============================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_11_19`


Vendredi 18 novembre 2022 **L’avocate iranienne Nasrin Sotoudeh lauréate du prix Robert-Badinter**
=====================================================================================================

- :ref:`nasrin_sotoudeh_2022_11_18`
- https://www.ecpm.org/lavocate-iranienne-nasrin-sotoudeh-laureate-du-prix-robert-badinter/


.. figure:: ../actions/2022/11/18/images/article_pnm.png
   :align: center
   :width: 600


Cette justice d’élimination, cette justice d’angoisse et de mort, décidée
avec sa marge de hasard, nous la refusons .
Jamais ces paroles de Robert Badinter, ancien garde des sceaux, n’auront
mieux prouvé leur justesse que parmi les 1500 délégués venus de 128 pays
à Berlin du 15 au 18 novembre 2022 à l’occasion du 8e Congrès mondial
contre la peine de mort.

Au vu de la situation que la population iranienne endure du fait de la
répression sanglante (au moins 416 morts dont 51 enfants) menée par le
régime de la République islamique depuis la mort de Mahsa Amini le 16 septembre 2022,
les congressistes ont attribué pour a première fois le Prix Robert-Badinter
à Nasrin Sotoudeh, avocate iranienne et militante des droits de l’homme .

Christine Taubira, l’ancienne garde des Sceaux, a remis ce prix à Mahmood
Amiry-Moghaddam , directeur de l’ONG Iran Human Rights , l’avocate iranienne
n’ayant pu faire le déplacement.

L’Iran détient le record du pays qui a le taux d’exécutions par habitant
le plus élevé : durant les dix premiers mois de 2022, 448 personnes ont
été exécutées. En 2021, 333 exécutions ont eu lieu.

Plus de 5000 personnes personnes sont actuellement détenues dans les
couloirs de la mort. Et plus de 15000 manifestants ont été arrêtés dont
6 ont été condamnés à mort.

L’Iran figure parmi les 5 pays qui exécutent le plus dans le monde avec
la Chine, l’Egypte, l’Arabie Saoudite et la Syrie.

Pour Raphaël Chenuil-Hazan, directeur général d’Ensemble contre la peine
de mort (ECPM), ONG organisatrice du congrès, le but (du congrès) est de
créer le moment politique pour porter la question de l’abolition de la
peine de mort auprès des acteurs politiques de pays qui ont toujours
la peine de mort.

Il s’est félicité des signaux venant du contient africain annonçant
que, d’ici quelques années, l’Afrique sra un continent quasiment ou
entièrement abolitionniste.

Si les congerssistes ont fêté à Berlin le 20e anniversaire de la Coalition
mondiale contre la peine de mort, il reste à poursuivre le combat abolitionniste
contre cette “pratique cruelle” . Car malgré les énormes avancées obtebues
en 2022, 52 Etats restent à ce jour rétentionnistes et 30000 personnes
se trouvent dans les coulors de la mort.

Alors que 108 Etats sont désormais abolitionnistes pour tous les crimes,
8 autres le sont pour les seuls crimes de droit commun et 29 pays observent
un moratoire sur les exécutions.

La lettre de Nasrin Sotoudeh
-----------------------------------

Honorables organisateurs et participants du Congrès mondial contre la
peine de mort

Avec tous mes respects,

J’ai été informée ces derniers jours que je faisais partie des quatre
candidats au Grand Prix du Public Robert-Badinter.

Quelle belle opportunité de faire entendre nos voix au monde entier et
surtout à votre prestigieux congrès à l’heure où les condamnations à
mort jettent une ombre sur notre jeunesse contestataire en Iran.

Je ne sais pas encore avec qui j’ai l’honneur d’être nominée mais sans
aucun doute, ils méritent tous plus ce prix que moi. Je vais d’ailleurs
leur demander, ainsi qu’à tous les participants au congrès, de m’aider.

Moi, Nasrin Sotoudeh, avocate et prisonnière politique en Iran, je
demande au monde entier et à ce congrès d’être les yeux et les oreilles
des Iraniens en ces jours difficiles. Des jours où des jeunes gens qui
ont utilisé leur droit légal de participer aux manifestations “Femme, Vie, Liberté”
sont injustement condamnés à mort.

En changeant le comportement du gouvernement à l’égard des manifestants
et en empêchant le système judiciaire d’exécuter les manifestants, on
contribuera à abolir la peine de mort dans l’Iran de demain.

Ces jours-ci, l’opinion publique iranienne est plus proche que jamais
de l’abolition de la peine de mort et s’aligne sur les objectifs de la
communauté mondiale des droits de l’homme, qui visent à instaurer une vie
fondée sur la paix, la justice et le droit.

Par conséquent, en tant qu’institution civile fiable, nous vous demandons
de nous aider à instaurer la paix, la justice et l’abolition de la peine de mort ;
que votre soutien et votre alliance pour faire entendre la voix des Iraniens
en faveur de la justice soit le début de la fin de la peine de mort.

Avec mes meilleures salutations

Nasrin Sotoudeh Iran, Téhéran Novembre 2022


Semaine du 14 au 20 novembre 2022
======================================

- https://blogs.mediapart.fr/237870/blog/201122/une-semaine-dactualite-sur-liran


♀️✊ 8e rassemblement samedi 12 novembre 2022 à Grenoble place Félix Poulat à 14h30 **Manifestation solidaire avec les luttes d’émancipation en Iran** #Grenoble #NousSommesLeursVoix  #مهساامینی📣
=====================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_11_12`


♀️✊ 7e rassemblement samedi 5 novembre 2022 à Grenoble place Félix Poulat à 14h30 **Manifestation solidaire avec les luttes d’émancipation en Iran** #Grenoble #NousSommesLeursVoix  #مهساامینی📣
=====================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_11_05`


- https://www.lepoint.fr/monde/iran-il-ne-reste-pas-beaucoup-de-temps-a-vivre-au-regime-04-11-2022-2496509_24.php#xtor=CS2-239


ENTRETIEN. Historien et chercheur à Stanford, **Abbas Milani** explique pourquoi
la République islamique est plus fragilisée par la révolte en cours qu’il
n’y paraît.



♀️✊ 6e rassemblement **Chaine humaine pour soutenir la révolution en Iran** le samedi 29 octobre 2022 à 15h place Félix Poulat, #Grenoble #NousSommesLeursVoix
====================================================================================================================================================================

- :ref:`iran_grenoble_2022_10_29`


.. figure:: ../actions/2022/10/29/images/femme_vie_liberte.png
   :align: center
   :width: 300

Appel international: créons une chaîne humaine en solidarité avec la lutte
du peuple iranien.

Rassemblement le samedi 29 octobre à 15h place Félix Poulat organisé par
le **Collectif Iran solidarité** avec le soutien de:

- **La voix de l’Iran**, comité de Grenoble.
- LDH Iran,
- `LDH Grenoble Métropole <- https://www.grenoble.fr/association/129060/69-ligue-des-droits-de-l-homme-et-du-citoyen.htm>`_
- Cercle Laique



♀️✊ 5e rassemblement samedi 22 octobre 2022 à Grenoble place Félix Poulat à 14h30 **Manifestation solidaire avec les luttes d’émancipation en Iran** #Grenoble #NousSommesLeursVoix  #مهساامینی📣
=====================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_10_22`


2022-10-22 **Freedom Rally for Iran, Soldarität mit den Protestierenden im Iran** #Mahsa_Amini #Berlin #Iran
===========================================================================================================================

- :ref:`freedom_iran_2022_10_22`

Rassemblement européen de plus de 80 000 personnes à Berlin

.. figure:: ../actions/2022/10/22/berlin/images/berlin_80_000.png
   :align: center


♀️✊ 4e rassemblement samedi 15 octobre 2022 à Grenoble place Félix Poulat à 14h30 **Manifestation solidaire avec les luttes d’émancipation en Iran** #Grenoble #NousSommesLeursVoix  #مهساامینی📣
=====================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_10_15`


Samedi 15 octobre 2022 **Incendie dans la prison d'Evin**
============================================================

- :ref:`evin_incendie_2022_10_15`

L’incendie de la prison d’Evin est survenu le 15 octobre 2022 à partir
d’environ 10h0 heure locale lorsqu’une série d’incidents, dont un incendie,
des explosions et des combats à l’arme automatique, s’est produite dans
la prison d’Evin à Téhéran, en Iran, se poursuivant jusqu’au petit matin
du 16 octobre 2022.

L’agence de presse Fars a déclaré que quatre “explosions massives” ont
eu lieu après que l’IRNA a affirmé que l’incendie avait été éteint.

Un témoin du NYT a déclaré avoir entendu des coups de feu automatiques
et des explosions “énormes” vers 21h0, heure locale.

Selon le Centre pour les droits de l’homme en Iran (en), une “fusillade”
a eu lieu dans la prison d’Evin à 22h0 heure locale dans la nuit du 15 octobre.

Des vidéos partagées sur les réseaux sociaux le 15 octobre montraient de
la fumée s’élevant de la prison. Des coups de feu répétés ainsi que des
chants antigouvernementaux pouvaient également être entendus dans les vidéos.

Des explosions provenant de la prison ont été entendues aux premières
heures du 16 octobre.

Amirdaryoush Youhaei, un résident local, a déclaré : « Il y a eu une
énorme sirène, puis onze grosses explosions et des tirs de mitrailleuses
qui ne se sont pas arrêtés. […] C’était comme si nous regardions un
film de guerre. »



♀️✊ 3e rassemblement samedi 8 octobre 2022 à Grenoble place Félix Poulat à 14h30 **Femme, Vie, Liberté #mahsa_amini 📣 Rassemblement de solidarité avec la révolte du peuple iranien pour la liberté #Grenoble #NousSommesLeursVoix  #مهساامینی** 📣
=====================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_10_08`


.. figure:: ../actions/2022/10/08/images/affiche_manif_2022_10_08.png
   :align: center
   :width: 300


lundi 10 octobre **Grève dans la pétrochimie** (**Assalouyeh**, Abadan, etc)
=======================================================================================

- :ref:`greve_petrochimie_2022_10_10`

Après les universités, les lycées et d’autres secteurs, c’est maintenant
l’un des piliers de l’économie iranienne qui se mobilise.

Appel à la mobilisation et à la grève aujourd’hui à Assalouyeh , le cœur
de l’industrie pétrochimique en #Iran. #MahsaAmini


Samedi 8 octobre 2022 |Anonymous| PIRATAGE Le JT de 21 h de la chaîne d’info continue de la télévision d’Etat piraté par la **Justice d’Ali**
================================================================================================================================================

- :ref:`piratage_iran_2022_10_08`

PIRATAGE. Le JT de 21 h de la chaîne d’info continue de la télévision
d’Etat piraté par la Justice d’Ali qui place, à côté du portrait de
l’ayatollah Khamenei, ceux de 4 femmes tuées dans la répression

- :ref:`#MahsaAmini <mahsa_jina_amini>` |JinaAmini|
- :ref:`#HadisNajafi <hadis_najafi>` |HadisNajafi|
- :ref:`#NikaShakarami <nika_shakarami>` |NikaShakarimi|
- et :ref:`#SarinaEsmaeilzadeh <sarina_esmaeilzadeh>` |SarinaEsmaeilzadeh|


Samedi 8 octobre 2022 Appel pour soutenir la rébellion courageuse des femmes iraniennes, et du peuple, demandant des libertés politiques et des droits égaux
=================================================================================================================================================================

- :ref:`soutien_femme_2022_10_08`

 Article de Libération ¶

Des intellectuels dont Etienne Balibar, Ludivine Bantigny, Phillipe Descola
lancent un appel pour soutenir la rébellion courageuse des femmes iraniennes,
et du peuple, demandant des libertés politiques et des droits égaux.

Les événements faisant suite à l’assassinat de Mahsa Amini, 22 ans, par
la police des mœurs iranienne le 13 septembre 2022 justifient l’indignation
et le plus large soutien international.


Mercredi 5 octobre 2022  **Angela Davis apporte son soutien aux manifestants en Iran**
============================================================================================

- :ref:`soutien_angela_davis`


Angela Davis apporte son soutien aux manifestants en Iran.


**Mardi 4 octobre 2022 Assassinat de Nagihan Akarsel  l'une des autrices du slogan JinJiyanAzadî (kurdistan Irak) #NagihanAkarsel**
=========================================================================================================================================

- :ref:`nagihan_akarsel_2022_10_04`


.. figure:: ../repression/assassinats/2022/10/04/nagihan_akarsel/nagihan_akarsel.png
   :align: center

Our friend Nagihan Akarsel, Jineolojî Academy member and Jineolojî Journal
Editorial Board member was martyred as a result of an armed attack in
Sulaymaniyah at 9.30 this morning.

With anger and rage, we condemn this assassination, which is one of the
recent political murders carried out by fascist and hegemonic powers
against all women in resistance around the world.

Le jeudi 29 décembre 2022, le nom de l’assassin de Nagihan Akarsel
(assassinée de 11 balles dans le cord le corps le 4 octobre 2022 a
Souleimani au Kurdistan du Sud - Irak) et son lien avec la Turquie a
été révélé par Zubeyir Aydar , membre du KNK (Conseil National du Kurdistan).

Il s’agit de Ismail Peker , un homme turc né à Ankara.

Aydar a déclaré que celui-ci avait confessé avoir été payé et envoyé pour
assassiner Nagihan Akarsel, militante de la Jineolojî et du mouvement
des femmes kurdes.



**Mardi 4 octobre 2022 Annonce de l'assassinat de Nika Shakarami 16 ans #NikaShakarami #نیکا_شاکرمی** |NikaShakarimi|
========================================================================================================================

- :ref:`nika_shakarami_2022_10_04`

#NikaShakarami⁩ |NikaShakarimi| avait 17 ans lorsqu’elle a disparu pendant la manifestation
pour ⁦#MahsaAmani⁩ Une semaine après, les forces de sécurité ont rendu son
corps. Son nez et son crâne étaient brisés.



Mardi 4 octobre 2022 Pas de nouvelles de **Morvarid Ayaz**, chercheuse à @SorbonneParis1 et mère d’une fillette de 5 ans #MorvaridAyaz
==================================================================================================================================================

- :ref:`marvarid_ayaz_2022_10_04`


.. figure:: ../repression/arrestations/2022/09/morvarid_ayaz/images/marvarid_ayaz.png
   :align: center

Morvarid Ayaz, sociologue et docteure associée au Groupe Sociétés, Religions,
Laïcités (GSRL, EPHE/PSL-CNRS, Paris), a été arrêtée le 21 septembre 2022
à Rasht au nord-ouest de l’Iran.

Convoquée au commissariat de “Farzaneh” pour “donner certaines explications,
sans plus de précisions” selon son époux Arash Naimian, elle a été arrêtée,
emprisonnée, puis transférée quelques jours plus tard “de la prison de
Lakan de Rasht à Téhéran, mais aujourd’hui, personne ne sait où elle se
trouve exactement”, précise son époux.



Mardi 4 octobre 2022 France Déclaration intersyndicale ✊ **Solidarité avec les manifestant.e.s d'Iran** ✊
=================================================================================================================

- :ref:`declaration_intersyndicale_2022_10_04`

.. figure:: ../actions/2022/10/04/iran_declaration_intersyndicale/intersyndicale.png
   :align: center


Nous condamnons fermement la répression envers les manifestant.e.s.

Nous appelons le gouvernement iranien à libérer immédiatement et
inconditionnellement l’ensemble des manifestant.es.s détenu.e.s, ainsi
que les défenseur·e·s des droits humains, les syndicalistes, les militant.e.s
étudiant.e s, les journalistes, etc.

Nous soutenons notamment :

- le droit essentiel des femmes à disposer de leurs corps,
- l’abrogation de la loi rendant obligatoire le port du Hijab ainsi que
  toutes les lois phallocratiques en vigueur.

Paris, le 4 octobre 2022


Mardi 4 octobre 2022  Shervin Hajipour libéré le 4 octobre 2022
========================================================================

- :ref:`liberation_shervin_hajipour`

Shervin Hajipour a été libéré le jeudi 4 octobre 2022

Le chanteur iranien Shervin Hajipour , auteur de la chanson Barayé (Pour)
qui accuse la corruption généralisée et célèbre l’espoir de changement,
a été arrêté jeudi 29 septembre 2022 chez lui, à Téhéran. Postée sur
Instagram, sa chanson a été écrite à partir de tweets postés à la suite
de la mort de Mahsa/Jina Amini.

Elle a été vue plus de quarante millions de fois et elle est rapidement
devenue un symbole de la révolution en marche en Iran .


Lundi 3 octobre 2022 Rendre à Mahsa Amini son vrai prénom **Jîna** et aux femmes kurdes la maternité du slogan **Femme, Vie, Liberté**
===========================================================================================================================================

- :ref:`vrai_prenom_jina_2022_10_03`


1) Mahsa Amini s’appelle en réalité Jina Amini , mais comme l’état civil
iranien refuse d’enregistrer les prénoms kurdes, la famille a dû choisir
un prénom persan pour tout ce qui est administratif.

Mais presque personne dans l’entourage proche ne connaissait le prénom
Mahsa qui était destiné au régime iranien. D’ailleurs, sur sa tombe, sa
mère l’appelait Jina et jamais Mahsa …

2) Le deuxième point également très important à clarifier est le slogan
Femme, Vie, Liberté (en kurde: Jin, Jiyan, Azadî), un slogan fruit de la
lutte de libération du mouvement kurde, pour ne pas le nommer, le PKK
(organisation classée terroriste par l’Occident à la demande de la Turquie
qui colonise une très grande partie du Kurdistan).

Alors, il faut rendre à César ce qui est à César et à Jina juste son
prénom tandis qu’aux femmes kurdes la maternité du slogan Femme, Vie, Liberté ,
fruit de décennies de lutte contre le colonialisme et le patriarcat au Kurdistan.




♀️✊ 2e rassemblement samedi 1er octobre 2022 à Grenoble place Félix Poulat à 14h30 **Femme, Vie, Liberté #mahsa_amini 📣  Portons la voix de l'Iran, rassemblement de solidarité avec les femmes Iraniennes #Grenoble #NousSommesLeursVoix #OpIran #مهساامینی** 📣
=======================================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_10_01`


.. figure:: ../actions/2022/10/01/femme_vie_liberte_portons_la_voix_de_l_iran.png
   :align: center
   :width: 300

**Vendredi 30 septembre 2022 Vendredi sanglant ("Bloody Friday") Massacre de plus de 90 personnes au Balouchistan #Balochistan**
=====================================================================================================================================

- :ref:`massacre_balouchistan_2022_09_30`

Iran's security forces have killed at least 82 Baluchi protesters and
bystanders in Zahedan, Sistan & Baluchistan province.

Those killed include 3 children. **Bloody Friday**, 30 September 2022, marked
the deadliest day on record since protests began 3 weeks ago


Jeudi 29 septembre 2022 Communiqué | Soutien de l'EHESS (L’École des hautes études en sciences sociales) à la mobilisation de la société iranienne
=======================================================================================================================================================

- :ref:`ehess_2022_09_29`


L’EHESS apporte sa solidarité totale à la communauté universitaire iranienne,
aux étudiantes et aux étudiants, aux collègues, et plus largement à la
société civile iranienne tout entière, engagée dans le combat pour le
droit des femmes et la défense des libertés civiles.
Elle condamne fermement la répression violente qui les touche.

Alors que les morts se comptent déjà par dizaines, et que les arrestations
arbitraires de milliers d’activistes se multiplient, alors que l’accès à
Internet a été coupé pour tenter d’entraver la mobilisation, l’EHESS
appelle à l’expression d’une solidarité sans réserve de l’ensemble de la
communauté universitaire internationale, à la hauteur du courage manifesté
par les femmes et les hommes d’Iran, par les manifestantes et les manifestants,
par les grévistes qui risquent leur vie aujourd’hui.


♀️✊ 1er rassemblement samedi 24 septembre 2022 à Grenoble place Félix Poulat à 14h30 **rassemblement de solidarité avec les femmes en Iran et en hommage à Mahsa Jina Amini, #Grenoble #JinaMahsaAmini #MasahAmini #Masah_Amini #OpIran #مهساامینی**
==========================================================================================================================================================================================================================================================

- :ref:`iran_grenoble_2022_09_24`

.. figure:: ../actions/2022/09/24/images/appel_au_rassemblement.png
   :align: center
   :width: 300

Samedi 24 septembre 2022  **Appel de Hawzhin Azeez**, Parlez pour Jina Mahsa Amini et pour toutes les femmes du monde
===========================================================================================================================

- :ref:`appel_de_hawzhin_azeez`

L’activiste kurde, Hawzhin Azeez appelle chaque femme et homme à élever
la voix pour les femmes et la planète que le système capitaliste phallocrate
détruit depuis des siècles.


|JinaAmini| Mahsa Jîna Amini (assassinée le vendredi 16 septembre 2022 à Téhéran)
==========================================================================================

- :ref:`jina_amini_2022_09_16`

Jîna est kurde et signifie femme ; cependant, le nom kurde est interdit,
c’est pourquoi ses parents ont été contraints de lui donner un autre nom
officiel à l’époque. C’est pourquoi Jîna était appelée « Mahsa » par les
autorités, ce qui vient du vieux persan et signifie « comme la lune »
ou «la lune».

Le mardi 13 septembre 2022, Jîna et son frère se rendaient de leur ville
natale à Téhéran lorsqu’ils ont été arrêtés par la brigade des mœurs.
Ils voulaient arrêter Jîna à cause de sa tenue «non islamique».

Sa tenue «non islamique» comportait une partie de ses cheveux sous le hijab.
Elle a donc été emmenée dans un commissariat.
Après quelques heures, Jîna est tombée dans le coma puis a été emmenée
à l’hôpital, où elle est finalement décédée le 16 septembre 2022.


Alors que les autorités officielles parlaient de « problèmes cardiaques
soudains » et plus tard aussi de maladies antérieures, le public a compris
assez rapidement ce qui avait dû se passer.

Jîna avait déjà été battue et torturée par plusieurs policiers sur le
chemin du commissariat et cela a continué au commissariat. Les gens le
savaient parce que des milliers d’autres avaient vécu cela auparavant,
parce que le frère de Jîna a dû lui-même être témoin des premiers passages
à tabac et parce que quelques jours plus tard, un groupe de hackers via
« Iran International», un média de l’opposition iranienne en exil, a fait
scanner les scanner et images du dossier de Jîna qui ont été divulgués
montrant clairement qu’elle est morte d’un traumatisme crânien.

Le gouvernement iranien a immédiatement tenté de dissimuler ce qui s’était
passé. Les autorités policières ont exigé que les parents de Jîna enterrent
le corps de leur fille à huis clos le soir même.
Cependant, ils ont refusé et ne l’ont enterrée que le lendemain matin
à Seqiz, où le soulèvement populaire en Iran a commencé.



Arrestation de Jina Mahsa Amini |JinaAmini| à Téhéran le mardi 13 septembre 2022
======================================================================================

- :ref:`arrestation_jina_2022_09_13`
