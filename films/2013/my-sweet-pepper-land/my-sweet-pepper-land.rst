.. index::
   ! My sweet pepper land

.. _my_sweet_pepper_land:

==========================
**My sweet pepper land**
==========================

- https://fr.wikipedia.org/wiki/My_Sweet_Pepper_Land

.. figure:: images/my-sweet-pepper-land_villeneuve.png
   :align: center


:Dates de sortie:

    22 mai 2013 (festival de Cannes 2013), 9 avril 2014 (sortie nationale)

My Sweet Pepper Land est un film dramatique franco-germano-kurde coécrit
et réalisé par Hiner Saleem, sorti en 2013.


Synopsis
==============

Baran, ancien résistant contre Saddam Hussein et en faveur de l'indépendance
kurde, retrouve sa vieille mère après une longue période éloigné d'elle.

Celle-ci organise aussitôt de ridicules rencontres avec des jeunes filles
dans le but de le marier au plus vite. Il fuit alors pour échapper à cette
emprise familiale et accepte un poste de policier au commissariat d'un
village isolé au nord du Kurdistan irakien, près de la frontière turque.

Son prédécesseur a été assassiné, mais il va se donner pour digne mission
de lutter contre la corruption et d'affirmer son autorité et celle de
l'État pour que le pays retrouve la paix et la sécurité.

Il se trouve très vite confronté à Aziz Aga, chef tribal mafieux qui règne
en maître sur la région, jusqu'à tenir la justice sous sa coupe.

Aziz affirme à Baran que la loi ancestrale clanique ne peut être soumise
à la loi de l'État. Les hommes de main d'Aziz Aga viennent menacer Baran
à plusieurs reprises.

Dans le même temps, Govend, jeune femme très instruite qui a vécu dans un
milieu plus urbain, obtient l'accord de son père, malgré la réticence
brutale et le machisme paternaliste de ses nombreux frères, pour devenir
l'institutrice du même village du nord du Kurdistan.
Tout y est à construire en matière d'éducation. Mais sa venue au village
suscite bientôt la méfiance car son aspect et son comportement trop
progressistes sont jugés incompatibles avec les traditions locales.

Aziz Aga et ses adjoints, règnent sur la région par la terreur.

Ils en veulent autant à l'institutrice qu'au policier, qui cherche à
mettre son nez dans leurs petits trafics de drogue et de médicaments.
Ils font courir la rumeur selon laquelle les deux nouveaux arrivants
enfreignent les bonnes mœurs par une liaison amoureuse alors qu'ils ne
sont pas mariés.
Baran et Govend ne font pourtant que se rencontrer et échanger quelques
impressions sur la musique, se soutenant l'un l'autre, alors que naît
un sentiment entre eux.

Les villageois, se sentant salis par ce supposé déshonneur, décident de
retirer leurs enfants de l'école.
Alors qu'Aziz Aga et ses miliciens entreprennent d'éliminer Baran, Govend,
désespérée et dans une impasse, se décide à quitter le village, mais
revient finalement dans les bras de Baran.

En représailles d'une attaque, un groupe de résistantes kurdes décime le
groupe d'Aziz Aga.
Les frères de Govend essayent de la forcer à revenir vers leur père.
En vain.
