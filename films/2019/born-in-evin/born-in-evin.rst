.. index::
   pair: Born in Evin; Documentaire
   pair: Born in Evin; Maryam Zaree
   ! Born in Evin
   ! Maryam Zaree


.. _born_in_evin:

============================================================
**Born in Evin** par Maryam Zaree
============================================================

- https://www.on-tenk.com/fr/documentaires/politique/born-in-evin
- :ref:`born_in_evin_2023_04_25`

.. figure:: images/maryam_zaree.png
   :align: center

   Maryam Zaree

Résumé
============

La cinéaste et actrice Maryam Zaree enquête sur les circonstances de sa
naissance, en Iran, dans l’une des prisons politiques les plus tristement
célèbres au monde.


Description
============

Maryam Zaree est née en 1983 à Téhéran, Iran.

Fuyant les persécutions politiques, sa mère l'emmène en Allemagne
lorsqu'elle a deux ans.

Elle a grandi à Francfort-sur-le-Main et a étudié la comédie dans la
célèbre école de cinéma Konrad-Wolf à Potsdamm-Babelsberg.
Elle a tenu les rôles principaux dans une douzaine de longs métrages, a
travaillé pour le théâtre et la télévision et a été récompensée pour
ses performances.

En 2018, elle a reçu un "Grimme Preis" pour sa performance dans la série
télévisée "4 Blocks".

Sa première pièce de théâtre en tant qu'autrice, "Kluge Gefühle" a reçu
le prix du Stückemarkt de Heidelberg et a été représentée sur les scènes
de plusieurs théâtres. "Born in Evin" est son premier long métrage.


L'avis de Tënk
=================

Bien que ce film traite d’un sujet profondément politique, et en même
temps très personnel, Maryam Zaree parvient à employer une narration
caractérisée par l’humour ainsi que par un grand sérieux dans le traitement
des conséquences du traumatisme que nombre de protagonistes ont subi.

La réalisatrice suit sa propre histoire pour mettre fin au silence, qui
l’a accompagnée toute sa vie, à propos du lieu et des circonstances de
sa naissance.

Elle rencontre d’autres survivants, parle à des experts et recherche
d’autres enfants nés dans la même prison qu’elle.
Elle tente de trouver des réponses à ses questions personnelles et politiques.
Et sur son chemin, elle réalise plus d’une fois l’importance considérable
qu’il y a à briser le silence et à mettre des mots sur les événements passés.

Luc-Carolin Ziemann
Programmatrice, autrice et formatrice cinéma

