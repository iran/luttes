.. index::
   ! Aucun ours

.. _aucun_ours:

==========================
**Aucun ours**
==========================

- https://leclapvercors.fr/aucun-ours/
- https://cinemaleclub.com/film--306537.html

.. figure:: images/aucun_ours.png
   :align: center


Avant propos
===============

Jafar Panahi était libre de mouvement dans son pays au moment du tournage
de **Aucun Ours**, mais interdit de filmer.

Il est incarcéré depuis juillet 2022 pour s’être insurgé contre l’arrestation
de deux de ses confrères cinéastes :

- Mohammad Rasoulof (Le diable n’existe pas, Un homme intègre)
- et Mostafa Aleahmad.


Description
===========

Dans un village iranien proche de la frontière, un metteur en scène est
témoin d’une histoire d’amour tandis qu’il en filme une autre.

La tradition et la politique auront-elles raison des deux ?



