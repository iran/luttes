.. index::
   ! Juste une nuit

.. _juste_une_nuit:

==========================
**Juste une nuit**
==========================

- https://leclapvercors.fr/juste-une-nuit/
- https://www.lemonde.fr/culture/article/2022/11/16/juste-une-nuit-l-epopee-d-une-mere-courage-en-iran_6150171_3246.html


.. figure:: images/juste_une_nuit.png
   :align: center


Avant-propos
=============

Film iranien de Ali Asgari
Avec Sadaf Asgari, Ghazal Shojaei, Babak Karimi… | 2022 | 1h26 | VOSTF

Séance suivie d’un échange avec de jeunes iraniennes et Zoreh Baharmast,
présidente de la section grenobloise de la Ligue des droits de l’homme.


Description
===========

Fereshteh doit cacher son bébé illégitime pendant une nuit à ses parents
qui lui rendent une visite surprise.

Son amie Atefeh l’aide. Elles se lancent dans une odyssée au cours de
laquelle elles doivent soigneusement choisir qui sont leurs alliés.


