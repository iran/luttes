.. index::
   ! Les ombres persanes


.. _les_ombres_persanes:

==========================
**Les ombres persanes**
==========================

- https://fr.wikipedia.org/wiki/Les_Ombres_persanes


Synopsis
============

À Téhéran, un homme et une femme découvrent par hasard qu’un autre couple
leur ressemble trait pour trait.

Passé le trouble et l’incompréhension va naître une histoire d’amour... et de manipulation.
