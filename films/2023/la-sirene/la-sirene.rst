.. index::
   ! La sirène


.. _la_sirene:

==========================
**La sirène**
==========================

- https://www.lebleudumiroir.fr/critique-la-sirene/

Synopsis
============

1980, sud de l’Iran. Les habitants d’Abadan résistent au siège des Irakiens.

Omid, 14 ans, a décidé de rester sur place mais tandis que l’étau se
resserre, il va tenter de sauver ceux qu’il aime en fuyant la ville à
bord d’un bateau abandonné.


Un film d'animation bouleversant sur la guerre Iran-Irak, sur les conséquences
de ce conflit terrible, aujourd'hui encore.
(https://www.ici-grenoble.org/evenement/cinema-la-sirene-sur-la-guerre-oubliee-iran-irak).

.. figure:: images/affiche.png
   :align: center
