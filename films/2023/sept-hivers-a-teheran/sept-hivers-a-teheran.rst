.. index::
   ! Sept hivers à Téhéran
   pair: Reyhaneh ; Jabbari
   ! Reyhaneh Jabbari


.. _sept_hivers_a_teheran:

============================================================
**Sept hivers à Téhéran** #MahsaAmini #ReyhanehJabbari
============================================================

- https://www.ici-grenoble.org/evenement/documentaire-choc-sept-hivers-a-teheran-sur-les-droits-des-femmes-en-iran

Description
============


Alors que les révoltes populaires sont horriblement réprimées en Iran,
projection du documentaire choc de Steffi Niederzoll, "Sept hivers à Téhéran".

En 2007 à Téhéran, Reyhaneh Jabbari, 19 ans, poignarde l’homme sur le
point de la violer.

Elle est accusée de meurtre et condamnée à mort. A partir d’images filmées
clandestinement, Sept hivers à Téhéran montre le combat de la famille
pour tenter de sauver Reyhaneh, devenue symbole de la lutte pour les droits
des femmes en Iran.

Attention film exceptionnel mais bouleversant.


La production : nourfilms
============================

- https://www.nourfilms.com/
- https://twitter.com/nourfilms\_/rss

.. figure:: images/twitter_nour_films.png
   :align: center

   https://twitter.com/nourfilms\_, https://twitter.com/nourfilms\_/rss


Ont participé au documentaire
================================

Shole Pakravan
----------------

.. toctree::
   :maxdepth: 3

   shole_pakravan/shole_pakravan


Steffi Niederzoll
--------------------

- https://twitter.com/niederzoll
- https://twitter.com/niederzoll/rss

.. figure:: images/steffi_niederzoll.png
   :align: center

   https://twitter.com/niederzoll, https://twitter.com/niederzoll/rss


.. _zar_amir_ebrahimi:

**Zar Amir Ebrahimi**, la "voix" de Reyhaneh Jabbari
-------------------------------------------------------

- https://twitter.com/Zar_Amir/rss


Commentaires
==============

- https://twitter.com/shauraix/status/1639354084384976897#m

Au @lecafedesimages, la projection en avant-première du film "Sept hivers
à Téhéran", en présence de Shole Pakravan (mère de la jeune Reyhanneh Jabbari),
fait salle comble #Iran

- https://twitter.com/AmnestyAixenPce/status/1639019867893538816#m

Mardi, Salle comble au ciméma #LeMazarin pour l'avant première du film
#SeptHiversATéhéran, soutenu par #Amnesty, l'@ACAT_France et @AssoECPM.

Un débat fort instructif a suivi avec la mère de #ReyhanahJabbari dont
le destin est tracé dans ce film et un militant d'Amnesty.


- https://twitter.com/fsionneau/status/1640430032911728641#m
- https://www.nouvelobs.com/cinema/20230327.OBS71433/la-mort-en-direct-de-reyhaneh-27-ans-executee-pour-avoir-tue-son-violeur.html

« "Sept Hivers à Téhéran" n’est pas un spectacle : c’est un verdict contre
cette terre des hommes, terre d’inhumanité.»


2023-03-15 https://twitter.com/AssoECPM/status/1636046921432285184#m
------------------------------------------------------------------------------

Rendez-vous le jeudi 23/03 à l'UGC Ciné Cité Les Halles pour l'avant-première
du documentaire multi primé Sept Hivers à Téhéran, distribué par @nourfilms_ !

Avec la participation exceptionnelle de l'actrice franco-iranienne @Zar_Amir
et de @chenuilhazan, directeur d'ECPM. J-7 !


Sept hivers à Téhéran: le procès qui annonce la révolte
----------------------------------------------------------

- https://twitter.com/Figaro_Culture/status/1640749637454757889#m
- https://www.lefigaro.fr/cinema/sept-hivers-a-teheran-le-proces-qui-annonce-la-revolte-20230328


CRITIQUE - En salle ce mercredi, Sept hivers à Téhéran relate le combat
de la famille de Reyhaneh Jabbari, pendue en 2014, après une instruction biaisée.

En Iran, l’hiver est la saison de la pendaison des femmes. Reyhaneh Jabbari,
27 ans, aura passé sept hivers en prison avant d’être exécutée, en 2014.

Son crime? Avoir poignardé à mort l’homme qui tentait de la violer.

Pour raconter l’histoire de Reyhaneh Jabbari, la réalisatrice allemande
Steffi Niederzoll mêle des films de famille, des images tournées clandestinement,
des enregistrements téléphoniques et des lettres écrites par Reyhaneh à
sa famille en détention.

Elles sont lues par l’actrice iranienne Zar Amir Ebrahimi, récompensée
à Cannes (2022) pour Les Nuits de Mashhad et exilée à Paris.

On découvre une adolescente de 14 ans joyeuse, choyée par une mère aimante
et un père progressiste.

La mère, Shole, témoigne depuis Berlin, où elle a trouvé refuge avec les
deux sœurs de Reyhaneh. Le père, Fereydoon, privé de passeport, ne peut
quitter Téhéran. Ils se souviennent de leur fille adorée. ...


Cinéma : "Sept hivers à Téhéran" ou le **combat des femmes**
==============================================================


- https://www.radiofrance.fr/franceculture/podcasts/bienvenue-au-club/sept-hivers-a-teheran-le-combat-des-femmes-1611456

Retour sur l'affaire Reyhaneh Jabbari, du nom de cette jeune fille condamnée
à la pendaison en Iran pour avoir tué l'homme qui tentait de la violer.

Grâce à la rencontre de la famille de la jeune fille, Steffi Niederzoll,
notre invitée, a réalisé le documentaire "Sept hivers à Téhéran".

Avec

    Steffi Niederzoll Réalisatrice et actrice

La jeune Iranienne Reyhaneh Jabbari a été condamnée à mort en Iran pour
avoir mortellement poignardé l'homme qui tentait de la violer.

Grâce à la famille de la jeune fille, qui lui a notamment donné des vidéos
et des lettres datant de l'époque du procès, la réalisatrice et actrice
Steffi Niederzoll a réalisé Sept hivers à Téhéran, en salle le 29 mars 2023.

Un documentaire poignant sur un procès truqué, et la preuve que la justice
ne s'applique pas de la même manière selon que l'on soit un homme ou une
femme en Iran.

Merci à Louise Degenhardt pour sa traduction simultanée.

**Sept hivers à Téhéran : "Essentiel et indispensable."** ⭐️⭐️⭐️⭐️⭐️
===========================================================================================

- https://twitter.com/nourfilms\_/status/1639965455933751296#m
- https://www.unificationfrance.com/article76340.html

::

    "Essentiel et indispensable." ⭐️⭐️⭐️⭐️⭐️
    Merci Unification France

Sept hivers à Téhéran est un excellent documentaire qui revient sur une
affaire qui a défrayé la chronique en Iran et au niveau international.

En 2007, une jeune femme de 19 ans, Reyhaneh Jabbari, tue l’homme qui
essayait de la violer.

Mais dans ce pays où les lois sont écrites par les hommes et où les personnes
n’ont pas le droit de se défendre, surtout si elles sont des femmes, elle
est condamnée à mort.
Seul le pardon de la famille de celui qui l’a agressé peut lui permettre
de vivre, car c’est la loi du talion, œil pour œil dent pour dent, qui est appliquée.

La réalisatrice Steffi Niederzoll a rencontré la famille de la jeune femme
et a eu accès à de très nombreux documents : vidéos, enregistrements audio
et photographies, qui lui permettent de retracer les 7 hivers qu’elle a passé en prison.

Elle a, de plus, eu accès à des vidéos se déroulant en Iran à cette période
qui permettent d’illustrer la narration de l’affaire.
Ces images étant parfois interdites, et faisant planer la prison sur
ceux qui les ont envoyées, cela explique la présence de nombreux Anonymes
dans le générique final.

Ce sont les lettres, les enregistrements audio et le journal intime de
la jeune femme qui permettent de mettre des mots sur les images montrées.
C’est l’actrice Zar Amir Ebrahimiqui les prononce lorsque les témoignages
audio de Reyhaneh Jabbari ne sont pas disponibles.

Des interviews récentes de la mère, des deux sœurs et du père de la jeune
femme permettent aussi d’apporter un certain éclairage sur ces événements
qui ont bouleversé l’Iran et le monde.

Sans compter que sa mère, Shole Pakravan, s’est engagée très intensément
dans l’interdiction de la peine de mort et aide activement les familles
à éviter que leurs enfants ne soient exécutés.
Ce qui lui a valu de devoir s’exiler en Allemagne avec ses deux filles,
alors que son mari est empêché de quitter le pays et subi de nombreuses
répercussions.

Le documentaire est particulièrement palpitant et poignant à suivre.

Il a une immense répercussion aujourd’hui avec la révolution iranienne
démarrée le 16 septembre 2022 suite à la mort de Mahsa Amini, « Femme Vie Liberté ! »,
qui voit de nombreux jeunes gens être condamnés à mort et parfois exécutés
dans la foulée.

En plus de cette histoire tragique, ce sont les mots de cette femme
brillante qui résonnent en nous. En effet, cette dernière a découvert
un autre monde au cœur de la prison et a entrepris de se faire porte-parole
de la voix de celles qui s’y trouvent enfermées, parfois parce qu’elles
ont dû se prostituer pour survivre.

Au travers des quelques cas présentés, c’est la justice à deux vitesses
d’un pays qui est montrée, celle concernant différemment les hommes, et
les femmes.
Car malgré des preuves, les éléments en faveur de l’inculpé sont régulièrement
écartés, voire détruits, alors que la torture permet d’obtenir des aveux
faux sur lesquels se base le procureur.

L’œuvre est donc d’autant plus nécessaire, qu’elle donne la parole à
ceux que l’on veut faire taire et propose le portrait d’une femme forte
et généreuse qui s’est aussi battue pour celles qui n’avaient pas une
famille aussi aimante qu’elle à ses côtés.

Sept hivers à Téhéran est un excellent documentaire proposant des images
d’une grande force et permettant la rencontre d’une famille investie qui
est plongée en plein cauchemar et qui veut utiliser son histoire pour
montrer au monde que la mort d’un individu n’impacte pas que lui et
que la peine de mort devrait être abolie définitivement dans le monde entier.

Shole Pakravan a aussi écrit un livre sur cette histoire, Wie man ein
Schmetterling wird, avec la réalisatrice Steffi Niederzoll.

**Il est sorti en Allemagne et devrait sans doute être traduit en France.**

Ne passez donc pas à côté de ce documentaire remarquable qui reste
longtemps en mémoire.

Essentiel et indispensable.

