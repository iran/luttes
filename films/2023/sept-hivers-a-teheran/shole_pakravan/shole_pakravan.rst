.. index::
   pair: Sept hivers à Téhéran; Shole Pakravan
   pair: Reyhaneh Jabbari; Shole Pakravan
   ! Shole Pakravan

.. _shole_pakravan:

========================================================
**Shole Pakravan**, la maman de Reyhaneh Jabbari
========================================================

- https://twitter.com/pakravan_shole
- https://twitter.com/pakravan_shole/rss


.. figure:: images/pakravan_shole.png
   :align: center

   https://twitter.com/pakravan_shole, https://twitter.com/pakravan_shole/rss


La lutte contre la peine de mort
=======================================

- https://www.ecpm.org/
- https://www.ecpm.org/ecpm-partenaire-du-documentaire-sept-hivers-a-teheran/


📢Zoom sur le cycle scolaire dédié à la #peinedemort en Iran📢

2️⃣ témoins abolitionnistes iranien·nes, Shole Pakravan et Taimoor Aliassi
ont accompagné les équipes d'ECPM

.. figure:: images/pakravan_shole_contre_la_peine_de_mort.png
   :align: center

   https://twitter.com/AssoECPM/status/1641377618032308224#m


Shole Pakravan : « Lorsqu’un pays souffre, sa douleur se répand dans le monde entier »
==========================================================================================

- https://www.ecpm.org/ecpm-partenaire-du-documentaire-sept-hivers-a-teheran/
- https://www.instagram.com/p/CpiYxBcs_Nm/


Pourquoi tenez-vous à partager votre histoire ?
----------------------------------------------------

Ma fille m’a demandé, dans une dernière lettre, de la laisser partir en
paix.

Elle me disait qu’elle allait voyager avec le vent, tout autour du monde.
J’ai fait en sorte que cela arrive, vous allez comprendre comment.

Elle m’avait demandé de donner ses organes. Ce n’était pas possible.

Elle m’a aussi demandé de pardonner toutes les personnes impliquées dans
sa condamnation et son exécution. Je n’ai pas réussi à m’y résoudre.

En revanche, le livre que j’ai écrit, et ce film, 7 hivers à Téhéran,
sont deux ailes qui lui permettront de voyager avec le vent, de par le monde,
comme elle le souhaitait.

Vous savez, la souffrance de ma fille a pris fin mais la mienne et celle
de notre famille est encore vive.
Nous la portons dans nos chairs. Je veux que les gens prennent conscience
que ce n’est pas seulement une personne qui meurt dans une exécution : sa
famille meurt avec elle.

Les pays occidentaux devraient considérer que la peine de mort en Iran
les concerne eux-aussi.

**L’humanité est liée, nous sommes responsables les un·es des autres, nous
partageons la même planète.
Lorsqu’un pays souffre, sa douleur se répand dans le monde entier**.

Les gens doivent savoir que certain·es de leurs pairs traînent cette
lourde peine jusqu’à leur dernier souffle.

Pouvez-vous expliquer l’importance du pardon en Iran lorsque quelqu’un est condamné à la loi du Talion ?
-----------------------------------------------------------------------------------------------------------

L’Iran suit la loi islamique. C’est-à-dire qu’il existe deux modèles de
condamnation à mort : d’une part pour les crimes liés à la drogue, à l’homosexualité,
au fait de renier la religion musulmane, etc., et d’autre part le
Qisas : œil pour œil, dent pour dent.

Quand une personne commet un meurtre, le droit islamique ordonne de tuer
l’assassin·e sauf si la famille de la victime lui accorde son pardon.

Cela vient de la Charia.


Malheureusement, le fils de la victime n’a pas accordé le pardon à votre fille ; qui est-il pour vous désormais ?
----------------------------------------------------------------------------------------------------------------------

Je l’aurais appelé fils s’il avait accordé son pardon à ma fille et permis
de la libérer mais il a appuyé sur le bouton permettant de déclencher la
pendaison.
Je ne l’aime pas, bien sûr, mais je ne cherche pas à me venger de lui, je
ne souhaite pas sa mort, car je sais que même si je tuais l’humanité toute
entière ça ne me ramènerais pas Reyhaneh.

**Les manifestations en Iran depuis la mort de Mahsa Amini perdurent. La jeunesse iranienne vous donne-t-elle des espoirs de paix ?**

Pour être honnête, je ne crois pas. J’aimerais y croire. Aujourd’hui,
je suis extrêmement inquiète pour l’avenir.

J’ai vu des manifestant·es dans les pays occidentaux, à Bruxelles et à
Londres notamment, brandir des cordes de pendaison et des pancartes « tuez les Mollahs »
pendant des manifestations contre le régime iranien.

Le Chah d’Iran, Reza Pavlavi, a réuni un comité pour combattre la République
islamique d’Iran.
Je respecte les oppositions au régime en vigueur, bien sûr, mais cela
m’inquiète de voir que le prochain régime continuera sans doute d’exécuter.

Je suis une militante, j’ai vu ce que l’exécution fait aux gens, à leurs
proches. Bien sûr que les Mollahs ont perpétré des actes terribles mais
ce dont nous avons besoin, c’est de la justice, pas des exécutions.


Comment pouvons-nous soutenir la communauté iranienne ?
-------------------------------------------------------------

Les sanctions contre l’Iran ont fonctionné mais le système judiciaire
continue d’émettre des ordres d’exécution tous les jours.

**Ces sanctions n’aident pas la population**. Il faudrait punir le système
judiciaire lui-même, sanctionner les juges qui prononcent la peine de mort,
les gardes qui transfèrent les condamné·es sur le lieu de leur exécution.

Quel message voulez-vous faire passer ?
-----------------------------------------------

Chaque cellule de mon corps est pleine de haine contre les bourreaux.

Tout le monde doit savoir que les familles ne sont pas coupables, que nous
sommes des êtres humains et que nous ne devrions pas avoir à porter le
poids de cette peine dans notre chair et dans notre âme.

Je supplie chacun·e d’entre vous d’aider les jeunes qui ont manifesté
suite à la mort de Jina Mahsa Amini et se trouvent maintenant dans le
couloir de la mort.

Je vous en supplie, faites tout ce qui est en votre pouvoir, passez à l’action,
signez des pétitions, soyez leurs voix sur les réseaux sociaux, écrivez
à vos parlementaires, à vos gouvernements et demandez-leur d’aider les
personnes condamnées à mort en Iran.

Après chaque projection du film, on vient me voir et parfois, ce sont des
gens qui ont aussi souffert d’injustices criantes.

Le film leur donne le courage de les dénoncer.

Pour moi, ça veut dire beaucoup de voir que le film libère la parole des
femmes du monde entier.

Le cinéma, le théâtre, l’art en général peuvent faire une différence dans le monde.


