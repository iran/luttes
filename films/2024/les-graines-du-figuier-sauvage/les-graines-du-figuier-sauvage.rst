.. index::
   pair: Film ; Les Graines du figuier sauvage (2024, Mohammad Rasoulof)


.. _figuier_sauvage_2024:

==============================================================================================
2024 **Les Graines du figuier sauvage (The Seed of the Sacred Fig)** par Mohammad Rasoulof
==============================================================================================

- https://fr.wikipedia.org/wiki/Mohammad_Rasoulof


Synopsis
==================

- https://fr.wikipedia.org/wiki/Les_Graines_du_figuier_sauvage

L'histoire se concentre sur Iman, sa femme et ses deux filles. 

Iman est un fonctionnaire honnête, juriste qui, au bout de vingt ans, a 
récemment été nommé enquêteur au tribunal révolutionnaire de Téhéran par 
l'entremise d'un de ses collègues. 

Avec son nouveau statut, plus valorisant, Iman découvre qu'on attend de lui 
qu'il avalise les condamnations à mort décidées par le procureur, sans même 
étudier les dossiers. 
À la signature de son engagement, il lui est remis un pistolet pour sa 
protection et celle de sa famille, pistolet qu'il range tous les soirs dans 
sa table de nuit.

À la même période, les manifestations nationales du mouvement Femme, Vie, Liberté 
contre le port obligatoire du hijab gagnent en ampleur et ses filles y sont 
indirectement mêlées. 
Ainsi, lorsque sa femme et ses deux filles aident une étudiante blessée lors 
d'une charge de police, par ailleurs seule amie de sa fille aînée, les trois 
décident de garder l'incident secret pour Iman. 

Au tribunal, on conseille à ce dernier de cacher ses activités à ses amis et 
à sa famille, par crainte de pressions qui pourraient être exercées sur lui.

La vie d'Iman commence alors à basculer dans la méfiance et la suspicion. 
Il réprimande ses filles pour leurs sensibilités féministes qu'il considère 
être de la propagande des ennemis de l'Iran.

Un matin, le pistolet d'Iman n'est plus dans le tiroir où il le range chaque 
soir. 
Cette perte pourrait le conduire en prison et mettre un terme à son ascension 
sociale. 
Il devient suspicieux envers les trois femmes du foyer, pensant que l'une 
d'elles l'a pris et lui ment. 

