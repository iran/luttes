.. index::
   ! Alborz

.. _alborz:

========================================
Province d'Alborz
========================================

.. figure:: ../images/provinces.png
   :width: 500

.. figure:: images/alborz.png
   :align: center



.. toctree::
   :maxdepth: 3

   villes/villes
