.. index::
   ! Karadj

.. _karadj:
.. _karaj:

========================================
**Karadj (Karaj, Karadsch)**
========================================


.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/alborz.png
   :width: 500



.. figure:: images/karadj.png
   :align: center



Définition wikipedia
===========================

- https://fr.wikipedia.org/wiki/Karadj


Karadj ou Karadsch (en persan : كرج / Karaj [kæˈɾædʒ]) est une ville
d'Iran située dans la province d'Alborz au pied des monts Elbourz,
30 km à l'ouest de Téhéran.

La croissance de sa population est très rapide


La base de l'économie de Karaj est sa proximité avec Téhéran.

Sa position entre la Mer Caspienne et la capitale en fait un nœud
important des transports : la ville est connectée par autoroute et
chemin de fer avec Téhéran et Qazvin (à 100 km au nord-ouest), et
le réseau du métro de Téhéran arrive jusqu'à Karadj.

Evenements
===========

- :ref:`mehdi_karami`
