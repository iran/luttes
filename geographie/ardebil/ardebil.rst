.. index::
   ! Ardebil

.. _province_ardebil:

========================================
Province de Ardebil
========================================

- https://fr.wikipedia.org/wiki/Ardebil_(province)

.. figure:: ../images/provinces.png
   :width: 500

.. figure:: images/ardebil.png
   :align: center


Ardabil (en azéri : Ərdəbil, en persan : اردبیل), également appelé Ardebil
et anciennement Artavil, est une province d'Iran située dans l'Azerbaïdjan
iranien sur la côte ouest de la mer Caspienne.

Créée en 1993 par détachement de la province d'Azerbaïdjan-Oriental, elle
est à l'origine de la dynastie safavide.


.. toctree::
   :maxdepth: 3

   villes/villes
