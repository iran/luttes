.. index::
   ! Ardebil

.. _ville_ardebil:

========================================
**Ardebil**
========================================

- https://fr.wikipedia.org/wiki/Ardebil

.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/ardebil.png
   :width: 500



.. figure:: images/ardebil.png
   :align: center



Définition wikipedia
===========================

- https://fr.wikipedia.org/wiki/Ardebil

Ardabil (en persan : اردبیل), également Ardebil, anciennement Artavil, est
une ville historique de nord-ouest de l'Iran et la capitale de la province
d'Ardabil.

Sa population est estimée à 650 000 habitants, en majorité azéris.

La ville est réputée pour sa tradition de fabrication de soie et de
tapis. C'est ici que se trouve la tombe de Cheikh Safî ad-Dîn, qui donna
son nom à la dynastie safavide.


Géographie
=============

La ville d'Ardabil se trouve à 400 km au nord-ouest de Téhéran et à 50 
km à l'ouest de la mer Caspienne.
À proximité de cette mer et de la république d'Azerbaïdjan, la ville a
une grande importance politique et économique.

Ardabil est sise dans une plaine ouverte, à 1 500 m d'altitude, juste à
l'est du mont Savalan (4 811 m), où souffle un vent froid jusqu'à la
fin du printemps.
Les hivers sont très froids, avec des températures tombant à −25 °C, et
les étés sont tempérés, avec des températures qui ne dépassent guère 35 °C.

Le lac Neor se situe au sud-est de la ville.

