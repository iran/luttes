.. index::
   pair: Province; Azerbaïdjan occidental
   ! Azerbaïdjan occidental

.. _azerbaidjan_occidental:

========================================
Province d'Azerbaïdjan occidental
========================================

.. figure:: ../images/provinces.png
   :width: 500

.. figure:: images/azerbaidjan_occidental.png
   :align: center



L'Azerbaïdjan-Occidental ou Azerbaïdjan de l'Ouest (azerbaïdjanais :
Qərbi Azərbaycan ostanı ou persan : آذربایجان غربی Azarbāyejān-e-Qarbi)
est une des trente et une provinces de l'Iran et constitue l'une des
provinces de l'Azerbaïdjan iranien.

Elle couvre une superficie de 39 487 km2, ou 43 660 km2 en incluant le
lac d'Ourmia.

La province compte une population de plus de 3 000 000 habitants (2011).

Sa capitale est Ourmia.


.. toctree::
   :maxdepth: 3

   villes/villes
