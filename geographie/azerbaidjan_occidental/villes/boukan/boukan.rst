.. index::
   ! Boukan

.. _boukan:

================================================
**Boukan (Bokan, Bukan, Bokan, ou Bowkān)**
================================================

- https://fr.wikipedia.org/wiki/Boukan


.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/azerbaidjan_occidental.png
   :width: 500



.. figure:: images/boukan.png
   :align: center



Définition wikipedia
===========================

**Boukan** (en persan : بوكان, en kurde : Bokan ; aussi romanisée en Bukan,
Bokan, ou Bowkān) est une ville en Azerbaïdjan-Occidental dans le nord-ouest
de l'Iran comptant environ 213 000 habitants.


Économie
=============

Boukan est la troisième plus grande ville industrielle en Azerbaïdjan-Occidental.
