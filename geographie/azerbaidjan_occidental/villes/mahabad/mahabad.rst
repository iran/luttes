.. index::
   ! Mahabad

.. _mahabad:

========================================
**Mahabad (Mehabad)**
========================================

- https://fr.wikipedia.org/wiki/Mahabad


.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/azerbaidjan_occidental.png
   :width: 500



.. figure:: images/mahabad.png
   :align: center



Définition wikipedia
===========================

Mahabad (en kurde : مەھاباد, en persan : مهاباد, Mehabad) est une ville de la
province d'Azerbaïdjan occidental en Iran.

Elle a été dans l'antiquité le centre du royaume des Mannéens.

En 1946, elle fut la capitale de l'éphémère État kurde, la république de
Mahabad, proclamée le 22 janvier sur la place Tchahar Tcheragh (en)1.

La ville et sa région sont très pauvres.


En 2014, à l'occasion d'un débat sur la création de 10 nouvelles provinces,
le ministre de l'intérieur Abdolreza Rahmani Fazli (en) propose de créer
une province de « Kurdistan du Nord » avec Mahabad pour capitale, par
division de l'Azerbaïdjan occidental, et d'étendre les droits culturels
de la minorité kurde d'Iran.

Mais cette proposition se heurte à une forte opposition des Azéris, la
minorité ethnique la plus nombreuse d'Iran, à la fois parce quelle divise
une de leurs provinces et parce qu'elle rappelle la tentative
indépendantiste de la `république de Mahabad en 1946 <https://fr.wikipedia.org/wiki/R%C3%A9publique_de_Mahabad>`_

