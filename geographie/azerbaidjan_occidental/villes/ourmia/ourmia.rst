.. index::
   ! Ourmia
   ! Oroumieh
   ! Urmiya

.. _ourmia:

========================================
**Ourmia (Oroumieh, Urmiya)**
========================================

- https://fr.wikipedia.org/wiki/Ourmia

.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/azerbaidjan_occidental.png
   :width: 500



.. figure:: images/ourmia.png
   :align: center



Définition wikipedia
===========================

Oroumieh [ɔʁumjɛ], (persan : ارومیه, azéri : Urmiya, kurde : Wurmê, syriaque : ܘܪܡܝܐ ; appelée ر
ضائیه, Rezaiyeh sous les Pahlavi) est une ville du nord-ouest de l'Iran,
capitale de la province de l'Azerbaïdjan occidental.

Au recensement de 2012, sa population s'élève à 1 265 721 habitants
pour 700 000 ménages.

La ville se trouve dans une plaine fertile située à 1 330 mètres d'altitude,
sur la rive ouest du lac d'Oroumieh, à proximité de la frontière turque.

Oroumieh est en 2007 la dixième ville plus peuplée d'Iran.

Sa population est majoritairement composée d'Azéris , aux côtés de minorités
Kurdes, assyriennes et arméniennes.




