
.. _villes_azerbaidjan_occidental:

========================================
Villes d'Azerbaïdjan occidental
========================================

.. toctree::
   :maxdepth: 3


   boukan/boukan
   ourmia/ourmia
   mahabad/mahabad
