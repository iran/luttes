.. index::
   pair: Province; Azerbaïdjan oriental
   ! Azerbaïdjan oriental

.. _azerbaidjan_oriental:

========================================
Province d'Azerbaïdjan oriental
========================================

.. figure:: ../images/provinces.png
   :width: 500

.. figure:: images/azerbaidjan_oriental.png
   :align: center



L'Azerbaïdjan oriental ou Azerbaïdjan de l'Est (Şərqi Azərbaycan Ostanı
en azéri, آذربایجان شرقی, Āzarbāyjān-e Šarqi en persan) est une province
d'Iran faisant partie de l'Azerbaïdjan iranien.

Elle est située au nord-ouest du pays, à la frontière de l'Arménie et
de la république d'Azerbaïdjan. Sa capitale est Tabriz.


.. toctree::
   :maxdepth: 3

   villes/villes
