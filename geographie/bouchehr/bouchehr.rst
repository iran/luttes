.. index::
   pair: Province ; Bouchehr
   ! Bouchehr

.. _province_bouchehr:

========================================
Province de Bouchehr
========================================

- https://fr.wikipedia.org/wiki/Province_de_Bouchehr

.. figure:: ../images/provinces.png
   :width: 500

.. figure:: images/bouchehr.png
   :align: center


La province de Bouchehr (en persan : استان بوشهر / Ostân-e Bušehr) est
une des 31 provinces d'Iran.

Elle est située dans le sud du pays le long du golfe Persique.

Sa capitale est Bouchehr. La province possède 10 départements : Assalouyeh,
Bouchehr, Dachtestan, Dachti, Deilam, Deir, Djam, Kangan, Guenaveh, et
Tanguestan.

En 1996, la province avait une population d'approximativement 744 000 habitants.



.. toctree::
   :maxdepth: 3

   villes/villes
