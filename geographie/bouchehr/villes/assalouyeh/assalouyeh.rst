.. index::
   ! Assalouyeh

.. _assalouyeh:

========================================
**Assalouyeh**
========================================


.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/bouchehr.png
   :width: 500



.. figure:: images/assalouyeh.png
   :align: center



Définition wikipedia
===========================

- https://fr.wikipedia.org/wiki/Assalouyeh


Asalouyeh aussi orthographié Assalouyeh ou Assaluyeh, et parfois précédé
par le mot bandar, signifiant port) est une ville du sud de l'Iran,
dans la province de Bushehr.

Située sur les rives du Golfe Persique, à quelque 270 km au sud-est
de la capitale provinciale, Bushehr.

Asalouyeh a été choisie comme site pour accueillir les infrastructures
de la Zone économique spéciale pour l'énergie de Pars (PSEEZ) car
c'est le point terrestre le plus proche du plus grand champ gazier
du monde, South Pars et la ville disposait d'un aéroport, agrandi
en 2003 et d'un port en eaux profondes.



