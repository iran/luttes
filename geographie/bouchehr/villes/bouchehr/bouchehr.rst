.. index::
   pair: Ville ; Bouchehr
   ! Bouchehr

.. _ville_bouchehr:

========================================
**Bouchehr**
========================================


.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/bouchehr.png
   :width: 500



.. figure:: images/bouchehr.png
   :align: center



Définition wikipedia
===========================

- https://fr.wikipedia.org/wiki/Bouchehr


Bouchehr (en persan : بوشهر / Bušehr) est une ville portuaire iranienne,
capitale de la province de ‌Bouchehr.

Elle est située sur la côte du golfe Persique. En 2010, une centrale
nucléaire se trouve sur son territoire.



