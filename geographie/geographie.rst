
.. _geographie_iran:

=====================
Géographie
=====================


.. figure:: images/provinces.png
   :align: center

   https://en.wikipedia.org/wiki/Provinces_of_Iran

.. toctree::
   :maxdepth: 3

   alborz/alborz
   ardebil/ardebil
   azerbaidjan_oriental/azerbaidjan_oriental
   azerbaidjan_occidental/azerbaidjan_occidental
   bouchehr/bouchehr
   golestan/golestan
   guilan/guilan
   ilam/ilam
   kermanchah/kermanchah
   khouzistan/khouzistan
   kurdistan_iranien/kurdistan_iranien
   kurdistan_province/kurdistan_province
   mazandaran/mazandaran
   sistan_et_baloutchistan/sistan_et_baloutchistan
   teheran/teheran


