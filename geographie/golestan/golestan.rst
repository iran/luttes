.. index::
   ! Golestan

.. _golestan:

========================================
Province du Golestan
========================================

- https://fr.wikipedia.org/wiki/Province_du_Golestan

.. figure:: ../images/provinces.png
   :width: 500

.. figure:: images/golestan.png
   :align: center


Le Golestan (en persan : گلستان / Golestân) est une des 31 provinces d'Iran,
située au nord-est du pays.

Sa capitale est Gorgan.


.. toctree::
   :maxdepth: 3

   villes/villes
