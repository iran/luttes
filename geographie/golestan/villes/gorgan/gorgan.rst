.. index::
   ! Gorgan

.. _gorgan:

========================================
**Gorgan**
========================================

- https://fr.wikipedia.org/wiki/Gorgan

.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/golestan.png
   :width: 500



.. figure:: images/gorgan.png
   :align: center



Définition wikipedia
===========================

- https://fr.wikipedia.org/wiki/Gorgan


Gorgan (persan : Gorgān, گرگان, arabe : Jurjān, جرجان) est une ville du
nord-est de l'Iran, située à 400 km de Téhéran, à l'extrémité sud-est
de la mer Caspienne.

C'est la capitale de la province du Golestan.

