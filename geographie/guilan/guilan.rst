.. index::
   ! Guilan

.. _guilan:

========================================
Province de Guilan
========================================

- https://fr.wikipedia.org/wiki/Guilan_(province)

.. figure:: ../images/provinces.png
   :width: 500

.. figure:: images/guilan.png
   :align: center


Le Guilan (en persan : استان گیلان / Ostân-e Gilân [osˌtɒːne giːˈlɒːn] ; aussi
transcrit Gilan, en sachant qu'en persan la consonne گ / g se prononce [g])
est une des trente-et-une provinces d'Iran.

Elle était connue pendant l'Antiquité comme une partie de l'Hyrcanie.

Sa population est d'environ 2 millions d'habitants et sa superficie est
de 14 700 km².

Le Guilan se trouve à l'ouest du Mazandéran, le long de la mer Caspienne.
Sa capitale est Racht.

Les autres villes d'importance de la province sont : Astara, Astaneh-ye Achrafiyeh,
Roudsar, Langroud, Souma'eh Sara, Talech, Fouman, Massouleh et Lahidjan.

Le port principal de la province est Bandar-e Anzali (précédemment Bandar-e Pahlavi).


.. toctree::
   :maxdepth: 3

   villes/villes
