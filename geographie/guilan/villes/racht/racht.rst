.. index::
   ! Racht

.. _racht:

========================================
**Racht**
========================================

- https://fr.wikipedia.org/wiki/Racht

.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/guilan.png
   :width: 500



.. figure:: images/racht.png
   :align: center



Définition wikipedia
===========================

- https://fr.wikipedia.org/wiki/Racht


Racht ou Rasht (en persan : رشت, Rašt) est la capitale de la province
de Guilan au nord-ouest de l'Iran.
C'est un lieu commercial majeur entre le Caucase, la Russie et l'Iran
grâce au port de Bandar-e Anzali.

Racht est aussi un centre touristique majeur.


Géographie et climat
=========================

Racht est nichée entre le littoral de la mer Caspienne et les pentes de
la chaîne de l'Elbourz sur un des bras du Sefid Roud. Elle est située à
317 km par la route au nord-ouest de Téhéran et à environ 30 km de la
Mer Caspienne.

Racht bénéficie d'une variante humide du climat de type méditerranéen.

Le temps est souvent pluvieux et venteux. Un vent de nord-est souffle
de la Caspienne. Racht reçoit environ 1 000-1 200 mm de précipitations
annuelles sous forme de pluie.

Les précipitations sont particulièrement abondantes en hiver et en automne.
Octobre, le mois le plus arrosé, reçoit 230 mm de précipitations en moyenne.

Par contre, le mois de juin, le mois le plus sec, ne reçoit que 38 mm
de précipitations. La température tombe rarement en dessous de 0 °C.
