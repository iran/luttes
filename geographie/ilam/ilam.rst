.. index::
   ! Ilam

.. _ilam:

==========================================================================
Province d'**Ilam**
==========================================================================

- https://fr.wikipedia.org/wiki/Province_d%27Ilam
- https://en.wikipedia.org/wiki/Ilam_province

Ilam est une des trente et une provinces d'Iran.

Elle est située dans le sud-ouest du pays, à la frontière de l'Irak.

Sa capitale est la ville d'Ilam. Sa superficie est de 20 133 km2.

Les villes de la province sont : Ilam, Mehran, Dehloran, Dareh Shahr,
Shirvan Va Chardavol, Arakvaz, Malek Shahi, Aivan et Abdanan.

La province est entourée du Khuzestan au sud, du Lorestan à l'est, de la
Province de Kermanshah au nord et de l'Irak à l'ouest avec qui elle a
425 kilomètres de frontière commune.

La population de la province est approximativement de 545 000 habitants
(estimation 2005).

Géographie
============

La province d'Ilam est une des régions les plus chaudes d'Iran.

Cependant, les régions montagneuses au nord et au nord-est sont relativement
fraîches. Les précipitations annuelles moyennes dans la province sont de 578 mm.

En 1996, la température maximum absolue a été de 38 °C en août et la
température minimum de 0,4 °C a été enregistrée en février.
Le nombre de jours connaissant le gel en hiver est de 27


.. figure:: ../images/provinces.png
   :width: 500

.. figure:: images/ilam.png
   :align: center

