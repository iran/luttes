.. index::
   pair: Province; Kermanchah
   ! Kermanchah

.. _province_kermanchah:

============================================
**Province de Kermanchah** (ex Bakhtaran)
============================================

.. figure:: ../images/provinces.png
   :width: 500

.. figure:: images/kermanchah.png
   :align: center


La province de Kermanchah (Kirmaşan en kurde, كرمانشاه en persan) est une
des 31 provinces d'Iran.

Elle est située dans l'ouest du pays, à la frontière avec l'Irak.

Entre 1979 et les années 1990, elle était connue sous le nom de Bakhtaran.

Sa capitale, Kermanshah (34° 18′ N, 47° 04′ E) est située dans le centre
de la partie occidentale de l’Iran. La population de la ville est de
690 000 personnes.

La ville est située sur les pentes de la montagne Kooh-e Sefid, qui est
le sommet le plus célèbre dans la région de Kermanshah.
La ville s'étend sur plus de 10 km en longueur, le long de la rivière
Sarab et de la vallée du même nom.
L'altitude de la ville est de 1 420 m au-dessus du niveau de la mer.

La distance entre Kermanshah et Téhéran est de 525 km. Elle est le centre
des échanges commerciaux d'une région qui produit des céréales, du riz,
des légumes, des fruits, des huiles végétales, mais aussi de nombreuses
industries comme des raffineries de pétrole et de sucre, de la farine,
du ciment, du textile, etc.

L'aéroport est situé au nord-est de la ville et la distance à vol d'oiseau
jusqu'à Téhéran est de 413 km.




.. toctree::
   :maxdepth: 3

   villes/villes
