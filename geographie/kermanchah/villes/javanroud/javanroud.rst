.. index::
   ! Javanroud

.. _ville_javanroud:

========================================
**Javanroud**
========================================


.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/kermanchah.png
   :width: 500



.. figure:: images/javanroud.png
   :align: center



Définition wikipedia
===========================

- https://en.wikipedia.org/wiki/Javanrud

Javanrud (Kurdish: جوانڕۆ, romanized: Ciwanro; Persian: جوانرود; also Romanized
as Javānrūd; also known as Qal‘a Jūanrūd, Qal‘eh Jūānrūd, and Qal‘eh-ye Javānrūd,
all meaning "Fort Javanrud", and Jūānrū)[2] is a city in Javanrud County,
Kermanshah Province, Iran.

At the 2006 census, its population was 43,104, comprising 9,591 families.

The city of Javanrud is located 79 kilometers north of Kermanshah and
is 1300 meters above sea level.



