.. index::
   ! Kermanchah

.. _ville_kermanchah:

========================================
**Kermanchah (ex Bakhtaran)**
========================================


.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/kermanchah.png
   :width: 500



.. figure:: images/kermanchah.png
   :align: center



Définition wikipedia
===========================

- https://fr.wikipedia.org/wiki/Kermanchah

Kermanchah ou Kermanshah (kurde : Kirmaşan, persan : کرمانشاه, Kermânšâh;
également dénommée Bakhtaran, Bākhtarān, Kermānshāhān ou Qahremānshahr)
est la capitale de la province du même nom dans l'ouest de l'Iran.

Elle est située à 521 km de Téhéran et à environ 80 km de la frontière
irakienne, au pied des monts Zagros.

Les habitants sont en majorité des Kurdes des différentes tribus, dont
la plupart se sont sédentarisés après la Seconde Guerre mondiale.

Ils parlent le dialecte méridional du kurde. La majorité des Kurdes dans
cette ville sont chiites.

C'est la neuvième ville la plus peuplée et l'une des plus grandes villes
d'Iran et la ville la plus importante de la région centrale de l'ouest
de l'Iran.


