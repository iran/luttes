.. index::
   ! Khouzistan

.. _khouzistan:

========================================
Province du Khouzistan
========================================

- https://fr.wikipedia.org/wiki/Khouzistan

.. figure:: ../images/provinces.png
   :width: 500

.. figure:: images/khouzistan.png
   :align: center


Le Khouzistan, Khuzestan ou Khouzestan (en persan : خوزستان) est une des
31 provinces d'Iran.

Elle est située au sud-ouest du pays, aux confins de l'Irak et du
Golfe Persique.

Sa capitale est Ahvaz et elle couvre une surface de 62 238 km2.

Les autres villes principales de la province sont Behbahan, :ref:`Abadan <abadan>`,
Andimechk, Khorramshahr, Bandar-e Emam, Dezfoul, Chouchtar, Omidiyeh (en),
Izeh, Baghmalek, Bandar-e Mahchahr, Dasht-e Azadegan (en),
Ramhormoz, Shadegan (en), Chouch, Masjed Soleiman, Aghadjari,
Île Minu (en) et Hoveyzeh.



.. toctree::
   :maxdepth: 3

   villes/villes
