.. index::
   pait: Ville ; Abadan
   ! Abadan

.. _abadan:

========================================
**Abadan**
========================================


.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/khouzistan.png
   :width: 500



.. figure:: images/abadan.png
   :align: center



Définition wikipedia
===========================

- https://fr.wikipedia.org/wiki/Abadan


Abadan (en persan : آبادان) est une ville de la province du Khuzistan en
Iran, dans une île de l'Arvand rud.

La population de la ville d'après le recensement de 2006 était de
217 987 habitants.
