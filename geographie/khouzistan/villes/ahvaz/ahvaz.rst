.. index::
   ! Ahvaz

.. _ahvaz:

========================================
**Ahvaz**
========================================


.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/khouzistan.png
   :width: 500



.. figure:: images/ahvaz.png
   :align: center



Définition wikipedia
===========================

- https://fr.wikipedia.org/wiki/Ahvaz


Ahvaz (en persan : اهواز) est une ville d'Iran située sur les bords de la
rivière Karun au milieu de la province du Khuzestan dont elle est la
capitale.

Elle a une élévation de 20 mètres par rapport au niveau de la mer.

Ahvaz est classée ville la plus polluée aux particules en suspension
(PM10) du monde par l'OMS.
