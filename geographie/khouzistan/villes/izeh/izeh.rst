.. index::
   pait: Ville ; Izeh
   ! Izeh

.. _izeh:

========================================
**Izeh**
========================================


.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/khouzistan.png
   :width: 500



.. figure:: images/izeh.jpeg
   :align: center



Définition wikipedia
===========================

- https://fr.wikipedia.org/wiki/Izeh


Izeh est une ville du Khuzestan en Iran, capitale du district d'Izeh
située au nord est du site de Suse.

La ville s'est appelée Ayapir à la période élamite, puis Izaj ou Idhaj
puis Mâlamîr.


Evenements
===========

- :ref:`kian_pirfalak`
