.. index::
   ! Kurdistan iranien

.. _kurdistan_iranien:
.. _rojhilat:

==========================================================================
**Kurdistan iranien (Rojhellatî Kurdistan, Rojhelat en français)**
==========================================================================

- https://fr.wikipedia.org/wiki/Kurdistan_iranien

Description
============

Le Kurdistan iranien (en kurde : Rojhellatî Kurdistan), aussi appelé
Rojhelat en français : « l'Orient », est le nom officieux des régions
d'Iran habitées par les Kurdes.

Il partage des frontières avec l'Irak et la Turquie.

Il compte 10 millions d’habitants, ce qui représente 12 % de la population
iranienne. Sa capitale est :ref:`Mahabad <mahabad>`.

Il comprend la province iranienne du Kurdistan. Ils parlent une langue
Iranienne : "kurmandji, sorani, gurani", ce qui fait qu'ils sont Iraniens
de culture kurde, comme toutes les autres ethnies iraniennes présentes
sur le plateau Iranien.

En effet, la plupart des mots kurdes ont des origines iraniennes, et la
langue régionale (surtout le Sorani) est compréhensible par un Iranien
parlant une autre langue régionale, ou le farsi, encore plus si cet Iranien
provient des régions capsiennes (Nord ouest de l'Iran).

Présentation
==============

Le Kurdistan iranien inclut des parties de la province d'Azerbaïdjan
de l'ouest, du Kordestan, de la province de Kermanshah et de la province d'Ilam.

Les Kurdes forment la majorité de la population de cette région qui est
estimée à 10 millions (soit 12 % de la population), principalement sunnites,
contrairement à la majorité de la population iranienne musulmane chiite.

Cette région est la partie orientale du grand espace géo-culturel appelé
Kurdistan.



.. figure:: images/kurdistan_iranien.png
   :align: center

