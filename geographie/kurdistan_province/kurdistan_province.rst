.. index::
   pair: Parêzgey ; Kurdistan
   pair: Parêzgey ; Kurdistan
   pair: Géographie ; Kurdistan
   ! Kurdistan province
   ! Kordestan

.. _kurdistan:
.. _kurdistan_province:

==========================================================================
**Province du Kurdistan** (**Ostan-e Kordestan**, Parêzgey Kurdistan)
==========================================================================

- https://en.wikipedia.org/wiki/Kurdistan_province
- https://fr.wikipedia.org/wiki/Province_du_Kurdistan


La province du Kurdistan (en persan : استان کردستان, Ostan-e Kordestan ;
en kurde : Parêzgey Kurdistan) est une des 31 provinces d'Iran, à ne pas
confondre avec la région plus grande du Kurdistan iranien.

Sa capitale est Sanandaj.

La superficie de la province est de 28 817 km2, ce qui ne représente
qu'un huitième des régions habitées par des Kurdes en Iran (Kurdistan iranien).

La province est située dans l'ouest de l'Iran et est entourée par l'Irak
à l'ouest, la province de l'Azerbaïdjan de l'ouest au nord, la province
de Zanjan au nord-est et la province de Kermanshah au sud.

Les villes principales de la province sont: Marivan, Baneh, Saqqez,
Qorveh (en), Bijar, Kamyaran (en) et Diwandarreh (en).


.. figure:: ../images/provinces.png
   :width: 500

.. figure:: images/kurdistan.png
   :align: center

.. toctree::
   :maxdepth: 3

   villes/villes
