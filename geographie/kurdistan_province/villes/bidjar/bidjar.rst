.. index::
   pair: Ville; Bidjâr
   ! Bidjar
   ! Bîcar

.. _bidjar:

========================================
**Bidjâr (Bijar, Bîcar)**
========================================

.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/kurdistan.png
   :width: 500


.. figure:: images/bidjar.png
   :align: center


Description wikipedia
========================

- https://fr.wikipedia.org/wiki/Bidjar


Bidjar (ou Bijar ; en persan : بيجار / Bidjâr ; en kurde : Bîcar) est une
ville de la province du Kordestan en Iran.

Elle avait une population estimée à 55 019 habitants en 2006. Elle fait
partie du comté de Bidjar.

Les tapis noués à la main de la région de Bijar sont connus pour leur qualité

Les ruines d'un château connu sous le nom du château de Changiz se
trouvent à une vingtaine de kilomètres de la ville, sur la route qui
la relie à Sananda.


Evenements
===========

- :ref:`mehdi_karami`

