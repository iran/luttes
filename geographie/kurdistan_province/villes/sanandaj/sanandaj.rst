.. index::
   pair: Ville; Sanandaj
   ! Sanandaj

.. _sanandaj:

===============================================
**Sanandaj (Senna, Sine [səna] en kurde)**
===============================================

.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/kurdistan.png
   :width: 500



.. figure:: images/sanandaj.png
   :align: center


Description wikipedia
========================

- https://fr.wikipedia.org/wiki/Sanandaj


**Sanandadj** ou **Senna** (en kurde : Sine / سنه prononcé : [səna] Écouter; en
persan : سنندج / Sanandaj) est la capitale de la **province iranienne du
Kurdistan**.

Située dans la partie occidentale de l'Iran, à la frontière de l'Irak,
la ville de Sanandadj est à 512 km de Téhéran, à 1 480 m au-dessus du
niveau de la mer.

La population de Sanandadj est **principalement kurde**, mais la ville
accueille aussi des minorités **arménienne et juive**.

En 2011, Sanandadj est classée troisième ville présentant l'air le plus
pollué au monde par l'OMS.

Histoire
----------

Le nom **Sanandadj** est la forme arabe de **Sena Dej** (**Sine Dij**
en langue kurde), qui signifie **la citadelle de Sena**.

La ville est aujourd'hui communément appelée **Sinne** ou **Senna** en kurde.


