.. index::
   pair: Ville; Saqqez

.. _saqqez:

========================================
**Saqqez**
========================================

.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/kurdistan.png
   :width: 500



.. figure:: images/saqqez.png
   :align: center


Description wikipedia
========================

- https://fr.wikipedia.org/wiki/Saqqez


Saqqez (en persanسقز ; en kurde سەقز, Saghez, Saqqiz ou Sakiz) est la
capitale du département de Saqqez, dans la province iranienne du Kurdistan.

Histoire
----------

Les anciens ancêtres kurdes iraniens ont vécu à Seqiz et ses environs
depuis environ 1000 av. J.-C. Vers le milieu du VIIe siècle av. J.-C.,
les Scythes, menés par Bartatua atteignent le sommet de leur puissance
en Asie occidentale.

La région de Saqqez était leur centre politique. Pendant la période
préislamique, Seqiz et les régions avoisinantes forment un petit pays
connu sous le nom de Sagapeni, qui est censé être lié au nom des
anciens iraniens Sakas (Scythes) à partir duquel le nom de la ville
est dérivé.


Evénements
=============
