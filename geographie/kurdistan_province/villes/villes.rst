
.. _villes_kurdistan:

===============================================================
Villes
===============================================================

.. figure:: ../../images/provinces.png
   :width: 500

.. figure:: ../images/kurdistan.png
   :align: center



.. toctree::
   :maxdepth: 3

   bidjar/bidjar
   sanandaj/sanandaj
   saqqez/saqqez
