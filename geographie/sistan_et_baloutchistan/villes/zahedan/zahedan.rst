.. index::
   ! Zahedan

.. _zahedan:

===============================================================
**Zahedan**
===============================================================


.. figure:: ../../../images/provinces.png
   :width: 500

.. figure:: ../../images/sistan_baloutchistan.png
   :align: center

.. figure:: images/zahedan.png
   :align: center


Définition Wikipedia
========================

- https://fr.wikipedia.org/wiki/Zahedan


Zahedan (en persan : زاهدان, Zâhedân) est une ville d'Iran, capitale de
la province de Sistan-et-Baloutchistan.

La majorité des habitants de Zahedan font partie de l'ethnie Baloutche
et parlent le baloutche.

Les monuments historiques importants de Zahedan sont une mosquée sunnite
appelée "Mosquée Makki" et un temple sikh.


Evénements
===========

- :ref:`massacre_balouchistan_2022_09_30`
