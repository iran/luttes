.. index::
   pair: Province; Téhéran

.. _province_teheran:

========================================
Province de Téhéran
========================================

.. figure:: ../images/provinces.png
   :width: 500

.. figure:: images/teheran.png
   :align: center


La province de Téhéran (persan : تهران) est une des 31 provinces d'Iran.

Elle couvre une surface de 18 909 km² et est située au nord du plateau
central de l'Iran.
La province a des frontières communes avec le Mazandaran au nord,
la province de Qom au sud, la province de Semnan à l'est et la province
d'Alborz à l'ouest.

La métropole de Téhéran n'est pas seulement la capitale de la province
mais aussi la capitale de l'Iran.

Depuis juin 2010, la province compte 11 départements, 37 municipalités
et 1 358 villages.

La province a gagné de l'importance quand Téhéran a été proclamée capitale
de la dynastie Qajare en 1778.
Aujourd'hui Téhéran est une des 20 plus grandes municipalités du monde
en taille et en population.




.. toctree::
   :maxdepth: 3

   villes/villes
