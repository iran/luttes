.. index::
   ! Téhéran

.. _ville_teheran:

========================================
**Téhéran**
========================================


.. figure:: ../../../images/provinces.png
   :width: 500


.. figure:: ../../images/teheran.png
   :width: 500



.. figure:: images/teheran.png
   :align: center



Définition wikipedia
===========================

- https://fr.wikipedia.org/wiki/T%C3%A9h%C3%A9ran


Téhéran (/te.e.ʁɑ̃/ ; en persan : تهران, Tehrān, /tʰehˈɾɒn/ Écouter) est la
capitale et la plus grande ville de l'Iran.
Située dans le nord du pays, au pied des monts Elbourz, la ville donne
son nom à la province dont elle est également la capitale.

Téhéran a vu sa population multipliée par 40 depuis qu'elle est devenue
la capitale à la suite du changement de dynastie de 1786.

En 2015, la ville compte environ 9 millions d'habitants, et l'agglomération
plus de 15 millions.
La ville possède un métro (actuellement avec 7 lignes) et un dense
réseau autoroutier.

Cette croissance très importante de Téhéran est principalement due à
l'amélioration des conditions de vie ainsi qu'à l'attraction exercée
sur les habitants des provinces.
Elle a connu une forte accélération à partir de 1974, à la suite de la
forte hausse du prix du pétrole lors du premier choc pétrolier.
Les banlieues de la ville ont alors crû très rapidement ; finalement la
pression immobilière a eu raison de la politique de développement
urbain fixée en 1969.

Téhéran accueille près de la moitié de l'activité industrielle du pays :
industrie automobile, équipements électriques et électroniques, armement,
textiles, sucre, ciment et produits chimiques.

La ville et son bazar sont le pôle de commercialisation des tapis et
meubles produits dans l'ensemble du pays.

