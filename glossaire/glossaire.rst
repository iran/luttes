.. index::
   ! Glossaire

.. _glossaire:

=====================
Glossaire
=====================

.. glossary::



   Âzâdi (Persan, Kurde)
   Freedom (Anglais)
   Libertà (Italien)
       :ref:`Liberté <femmes_vie_liberte>`

   IRGC
       Islamic Revolutionary Guard Corps

   Zan (Persan)
   Jin (Kurde)
   Woman (Anglais)
   Donna (Italien)
       :ref:`Femme <femmes_vie_liberte>`

   Zendegi (Persan)
   Jîyan (Kurde)
   Life (Anglais)
   Vita (Italien)
       :ref:`Vie <femmes_vie_liberte>`
