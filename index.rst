
.. raw:: html

   <a rel="me" href="https://kolektiva.social/@iranluttes"></a>

.. ⚖️
.. 🔥
.. ☠️
.. ☣️
.. 🪧
.. 🇺🇳 unicef
.. 🇫🇷
.. 🇧🇪
.. 🇬🇧
.. 🇺🇸 🇨🇦
.. 🇨🇭
.. 🇩🇪
.. 🌍 ♀️
.. 🇮🇷 🇺🇦
.. 📣
.. 💃
.. 🎻

.. un·e

|FluxWeb| `RSS <https://iran.frama.io/luttes/rss.xml>`_


.. _luttes_iran:
.. _iran_luttes:

==========================================================
🇮🇷 **Luttes en Iran** ♀️, nous sommes leurs voix 📣
==========================================================

.. https://addons.mozilla.org/fr/firefox/addon/libredirect/

- http://iran.frama.io/linkertree

.. figure:: images/logo_mahsa_amini.png
   :align: center
   :width: 300

   https://www.youtube.com/watch?v=rt_imL4y820 (Clip d'hommage à Jina Mahsa Amini)

.. toctree::
   :maxdepth: 6

   revendications/revendications
   militantes/militantes
   universitaires/universitaires      
   actions/actions
   analyses/analyses
   repression/repression
   evenements/evenements

.. toctree::
   :maxdepth: 3

   slogans/slogans
   chants/chants
   danses/danses
   films/films
   livres/livres
   videos/videos

.. toctree::
   :maxdepth: 6

   soutiens/soutiens
   organisations/organisations

.. toctree::
   :maxdepth: 4

   geographie/geographie
   sites/sites

.. toctree::
   :maxdepth: 3


   tools/tools
   glossaire/glossaire

