.. index::
   pair: Negar Djavadi; Désorientale (2016-08)

.. _desorientale_2016:

============================================================
2016-08 **Désorientale** de Negar Djavadi |NegarDjavadi|
============================================================

- https://www.lianalevi.fr/catalogue/desorientale/


Auteure : Négar Djavadi |NegarDjavadi|
=============================================

- https://www.lianalevi.fr/auteur/negar-djavadi/


Negar Djavadi naît en Iran en 1969 dans une famille d’intellectuels opposants 
au Shah puis à Khomeiny. 

Elle a onze ans lorsqu’elle arrive clandestinement en France, à Paris, où elle
réside toujours. 

Diplômée de l’INSAS, une école de cinéma bruxelloise, elle travaille plusieurs 
années derrière la caméra avant de se consacrer à l’écriture de scénarios. 

En 2016, Désorientale, son premier roman est un succès de librairie unanimement 
salué, traduit en une dizaine de langues. 

Son deuxième roman, Arène, paru en août 2020, a été salué par la critique et 
les libraires (Prix Millepages 2020). Les droits audiovisuels ont été cédés. 


Description
===============

Si nous étions en Iran, cette salle d’attente d’hôpital ressemblerait à un 
caravansérail, songe Kimiâ. 
Un joyeux foutoir où s’enchaîneraient bavardages, confidences et anecdotes 
en cascade. 

Née à Téhéran, exilée à Paris depuis ses dix ans, Kimiâ a toujours essayé de 
tenir à distance son pays, sa culture, sa famille. 

Mais les djinns échappés du passé la rattrapent pour faire défiler l’étourdissant 
diaporama de l’histoire des Sadr sur trois générations: 

- les tribulations des  ancêtres, 
- une décennie de révolution politique, 
- les chemins de traverse de l’adolescence, l’ivresse du rock, 
  le sourire voyou d’une bassiste blonde…

Une fresque flamboyante sur la mémoire et l’identité; un grand roman sur 
l’Iran d’hier et la France d’aujourd’hui.
