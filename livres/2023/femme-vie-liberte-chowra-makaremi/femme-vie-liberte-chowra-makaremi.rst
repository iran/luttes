.. index::
   pair: Femme ! Vie ! Liberté ! Échos d'un soulèvement révolutionnaire en Iran; Livre


.. _livre_femme_vie_makaremi_2023:

====================================================================================================
**Femme ! Vie ! Liberté ! Échos d'un soulèvement révolutionnaire en Iran** de Chowra Makaremi
====================================================================================================

- :ref:`luttes_2023:livre_femme_vie_satrapi_2023`
