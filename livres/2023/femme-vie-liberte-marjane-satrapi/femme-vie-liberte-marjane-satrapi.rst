.. index::
   pair: Femme Vie Liberté (Marjane Satrapi) ; Livre


.. _livre_femme_vie_satrapi_2023:

====================================================================================================
Bande dessinée **Femme Vie Liberté** de Marjane Satrapi
====================================================================================================

- :ref:`luttes_2023:livre_femme_vie_makaremi_2023`
