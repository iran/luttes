.. index::
   pair: L'usure d'un monde; Livre
   pair: François-Henri Désérable; Ecrivain


.. _ref_lusure_dun_monde:

================================================================================
**L'usure d'un monde** de François-Henri Désérable
================================================================================

- :ref:`luttes_2023:lusure_dun_monde`
