.. index::
   pair: La dernière place; Livre
   pair: La dernière place; PS752
   pair: Négar Djavadi; Ecrivaine


.. _ref_la_derniere_place:

====================================================================================================
**La dernière place** de Négar Djavadi #PS752
====================================================================================================

- :ref:`luttes_2023:la_derniere_place`
