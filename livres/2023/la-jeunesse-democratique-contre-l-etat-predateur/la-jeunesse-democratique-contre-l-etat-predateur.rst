.. index::
   pair: La jeunesse démocratique contre l'Etat predateur; Livre


.. _la_jeunesse_democratique_contre_l_etat_predateur:

================================================================================
**La jeunesse démocratique contre l'Etat predateur** de Farhad Khorokhavar
================================================================================

- https://www.fauves-editions.fr/livre-iran_la_jeunesse_democratique_contre_l_etat_predateur_farhad_khosrokhavar-9791030204667-75939.html

.. figure:: images/la_jeunesse_democratique_contre_l_etat_predateur.png
   :align: center

   https://www.fauves-editions.fr/livre-iran_la_jeunesse_democratique_contre_l_etat_predateur_farhad_khosrokhavar-9791030204667-75939.html


La mort de Mahsa Amini, le 16 septembre 2022, a provoqué en Iran une
vague de contestation sans précédent, devenue depuis insurrection.

Venu fissurer et souligner la déconnexion totale du régime avec sa société,
ce mouvement de révolte a révélé l'illégitimité d'un État de non-droit
et le refus de sa jeunesse à ployer sous son joug.

Prisonnier d'une vision théocratique, anti-occidentale, antimoderne et
antidémocratique, sans ancrage dans la réalité culturelle du pays, l'Iran
gronde de griefs aussi politiques et économiques qu'écologiques.

Sans compter, sur le plan anthropologique, cette « joie de vivre » que
revendique le mouvement, en complète contradiction avec une conception
mortifère de l'existence qui promeut le martyre comme idéal de vie.

Cependant, en dépit de la répression massive qui s'abat sur la société
iranienne depuis plus de quatre décennies, malgré la main mise totale
du régime sur les institutions et les médias, force est de reconnaître
l'échec de la République islamique à embrigader et asservir la société,
notamment la jeune génération.

Éclairant le mouvement à la lumière d'une nouvelle subjectivité iranienne,
de l'évolution de la République islamique et, enfin, de ses échecs,
Farhad Khosrokhavar décrypte les particularités d'un mouvement où la
jeunesse – en particulier les femmes – est à l'avant-garde.

Une analyse critique et détaillée des composantes du système pour comprendre
le paradoxe de l'Iran : une société de plus en plus sécularisée et un
État de moins en moins en mesure d'en comprendre l'évolution.

Date de publication : 30 janvier 2023
Broché - format : 13,5 x 21,5 cm • 266 pages
Langue : français
ISBN : 979-10-302-0466-7
EAN13 : 9791030204667
EAN PDF : 9791030204674
EAN ePUB : 9791030204681


