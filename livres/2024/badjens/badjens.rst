.. index::
   pair: Livre ; badgens (2024)
   pair: delphine Minoui ; badgens (2024)

.. _badgens_2024:

======================================================================================================
2024-08 **badgens** de delphine Minoui
======================================================================================================

- https://www.seuil.com/ouvrage/badjens-delphine-minoui/9782021541724

.. figure:: images/recto.webp

Description
========================


« Bad-jens : mot à mot, mauvais genre. En persan de tous les jours: espiègle ou effrontée. »

Chiraz, automne 2022. Au cœur de la révolte « Femme, Vie, Liberté », une Iranienne 
de 16 ans escalade une benne à ordures, prête à brûler son foulard en public. 

Face aux encouragements de la foule, et tandis que la peur se dissipe peu à peu, 
le paysage intime de l’adolescente rebelle défile en flash-back : sa naissance 
indésirée, son père castrateur, son smartphone rempli de tubes frondeurs, 
ses copines, ses premières amours, son corps assoiffé de liberté, et ce code 
vestimentaire, fait d’un bout de tissu sur la tête, dont elle rêve de s’affranchir. 

Et si dans son surnom, Badjens, choisi dès sa naissance par sa mère, se trouvait 
le secret de son émancipation ? 

De cette transformation radicale, racontée sous forme de monologue intérieur, 
Delphine Minoui livre un bouleversant roman d’apprentissage où les mots claquent 
pour tisser un nouveau langage, à la fois tendre et irrévérencieux, à l’image 
de cette nouvelle génération en pleine ébullition.

D’origine iranienne, lauréate du prix Albert-Londres et grand reporter au Figaro, 
Delphine Minoui couvre depuis vingt-cinq ans l’actualité du Proche et Moyen-Orient. 

Publiés au Seuil, ses récits empreints de poésie, Je vous écris de Téhéran 
et Les Passeurs de livres de Daraya (Grand Prix des lectrices ELLE), ont connu 
un immense succès et ont été traduits dans une dizaine de langues.
