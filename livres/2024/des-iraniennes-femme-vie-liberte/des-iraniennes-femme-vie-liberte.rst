.. index::
   pair: Livre ; Des iraniennes, femme vie liberté (2024)

.. _des_iraniennes_2024:

======================================================================================================
2024-09 **Des iraniennes, femme vie liberté 1979-2024** 
======================================================================================================

- https://www.desfemmes.fr/litterature/des-iraniennes/


.. figure:: images/recto_exemple.webp

.. figure:: images/recto_exemple_2.webp


Description
========================

Un livre politique pour documenter la lutte des femmes en Iran.

Le mouvement "Femme, vie, liberté" né en Iran en septembre 2022 embrase 
le pays et expose les Iraniennes à une répression extrêmement violente : 
meurtres, arrestations arbitraires, tortures, conditions de détention inhumaines… 

Contre la misogynie et l’autoritarisme, les protestations et manifestations 
initiées par les Iranien·nes sont relayées dans le monde entier. 

Ce soulèvement fait écho à celui de mars 1979, où des milliers de femmes se 
sont mobilisées à Téhéran pour défendre leurs droits. 
À ce moment-là déjà, à l’initiative d’Antoinette Fouque, des militantes du 
MLF-Psychanalyse et Politique s’étaient rendues sur place pour documenter 
cette révolte, main dans la main avec elles.

Ce livre collectif, alliant les époques (1979, 2022) et les pays (France et Iran), 
s’inscrit dans la chaîne de solidarité qui soutient le combat des femmes iraniennes. 

Il comprend des archives inédites et des entretiens avec des militantes 
d’hier et d’aujourd’hui ; il restitue et transmet le témoignage des luttes 
passées et présentes.

Plus que jamais, il est nécessaire de se rallier au cri "Femme ! Vie ! Liberté !".

"En tant que femme, et comme des millions d’autres femmes iraniennes, j’ai
toujours été confrontée à l’enfermement de la culture patriarcale, au pouvoir 
religieux et autoritaire, aux funestes lois discriminatoires et oppressives, 
et à toutes sortes de restrictions dans tous les domaines de ma vie. […] 

La triste vérité, au fond, est que le gouvernement autoritaire, misogyne et 
religieux de la République islamique nous a volé notre vie." 

Nargess Mohammadi, prix Nobel de la paix. 2023

Cet ouvrage a été réalisé par des militantes du MLF-Psychanalyse et Politique 
et de l’Alliance des femmes pour la démocratie, avec la collaboration de 
militantes iraniennes du mouvement "Femme, vie, liberté" et de toutes 
celles qui ont témoigné de 1979 à 2024 de leur combat.

Les bénéfices de la vente de ce livre seront reversés à des associations de 
femmes iraniennes en lutte pour la démocratie et la défense de leurs droits.

Un QR code, imprimé à l’intérieur de l’ouvrage permet d’accéder à un document 
d’une grande valeur historique et politique, le film Mouvement de libération 
des femmes iraniennes : année zéro (1979, couleur, 13 mn, production 
Des femmes filment) à voir en cliquant sur `ce lien https://vimeo.com/985618085 Mouvement de libération des femmes iraniennes, année zéro, 1979 <https://vimeo.com/985618085>`_


La presse en parle
======================

Chirinne Ardakani, avocate : "Il faut faire inscrire dans le droit international la notion d’apartheid de genre". Le Nouvel Obs, 29 novembre 2024
-----------------------------------------------------------------------------------------------------------------------------------------------------

Chirinne Ardakani, avocate : "Il faut faire inscrire dans le droit international 
la notion d’apartheid de genre". Le Nouvel Obs, 29 novembre 2024


Le livre regorge d’archives bouleversantes, qui immortalisent les manifestations de mars 1979 et septembre 2022. Le Point, 5/11/2024
-----------------------------------------------------------------------------------------------------------------------------------------

Étudiante dévêtue en Iran : "Ahou Daryaei, tu n’es pas seule !" 
La cinéaste Sepideh Farsi Farsi et l’avocate Chirinne Ardakani commentent le 
geste inouï de leur compatriote. 
Le livre regorge d’archives bouleversantes, qui immortalisent les manifestations 
de mars 1979 et septembre 2022. Le Point, 5/11/2024

La très singulière histoire du féminisme en Iran réclame d’être prise en compte par qui veut comprendre ce qui se joue dans les conduites sociales –  ou "anti-sociales" – et "sécuritaires" En Attendant Nadeau, 17/12/2024
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://www.en-attendant-nadeau.fr/2024/12/17/nous-etions-aussi-rebelles-lune-que-lautre-iran/

La très singulière histoire du féminisme en Iran réclame d’être prise en compte 
par qui veut comprendre ce qui se joue dans les conduites sociales – 
ou "anti-sociales" – et "sécuritaires" [du pays]. En Attendant Nadeau, 17/12/2024

Interview de Chirinne Ardakani, co-autrice du livre. RFI, 16/09/2024
-------------------------------------------------------------------------------

Malgré la répression et les violences des autorités, les femmes iraniennes 
continuent de résister et de se mobiliser contre le régime. Interview de 
Chirinne Ardakani, co-autrice du livre. RFI, 16/09/2024


Chirinne Ardakani, co-autrice du livre. France Inter, 15/09/2024
-----------------------------------------------------------------------


- https://www.radiofrance.fr/franceinter/podcasts/l-invite-de-8h20/l-invite-de-8h20-du-week-end-du-dimanche-15-septembre-2024-2623719

"Ces femmes refusent de plier" : le combat des Iraniennes se poursuit deux ans après. 
interview de Chirinne Ardakani, co-autrice du livre. France Inter, 15/09/2024

Deux ans après le début du mouvement "Femme, vie, liberté", l'athlète 
Marjan Jangjoo, grimpeuse iranienne arrivée en février 2023 en France, 
la journaliste Mariam Pirzadeh, ancienne correspondante en Iran et 
l'avocate Chirinne Ardakani, évoquent la situation des femmes en Iran.

Le mouvement "Femme, vie, liberté", né il y a deux ans après la mort de 
Mahsa Jina Amini, étudiante Kurde iranienne qui avait été arrêtée par la 
police des mœurs pour ne pas avoir porté correctement le hijab, continue 
son combat pour l'émancipation des femmes en Iran. 

"Non seulement la résistance est toujours là, mais elle est incarnée par 
le quartier des femmes" de la prison d'Evin, explique l'avocate Chirinne Ardakani, 
venue avec une lettre de la Prix Nobel de la Paix Narges Mohammadi (voir ci-dessous) 
qui y est emprisonnée ainsi qu'un enregistrement clandestin de chants des détenues. 

"Ce qu'on entend là, ce sont des femmes qui sont organisées entre elles, 
qui continuent de donner de la voix, des voix puissantes, malgré la prison. 

Ces femmes refusent de plier ", commente Chirinne Ardakani.


Un ouvrage fort que l’on ne referme jamais tout à fait, tant il nous invite  à garder les yeux ouverts sur la situation des femmes dans le monde entier Les Inrocks, 12/09/2024 
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Un ouvrage fort que l’on ne referme jamais tout à fait, tant il nous invite 
à garder les yeux ouverts sur la situation des femmes dans le monde entier. 
Les Inrocks, 12/09/2024

La Cause Littéraire, 21/11/2024
------------------------------------
 
Ce livre a le mérite d’analyser du point de vue des femmes la situation 
politique et sociale de l’Iran, l’apport des Iraniennes aux grandes manifestations 
qui se sont déroulées dans leur pays et la répression féroce dont elles ont 
été victimes. La Cause Littéraire, 21/11/2024

Mise en lumière des figures courageuses et héroïques qui refusent la résignation et la soumission Combat le media, 19/10/2024
---------------------------------------------------------------------------------------------------------------------------------

À travers cet ouvrage historique, […] mise en lumière des figures courageuses 
et héroïques qui refusent la résignation et la soumission. 
Leur combat de chaque instant est un véritable cri d’amour à la liberté, 
la vie, et la démocratie. Combat le media, 19/10/2024

Cet ouvrage remarquablement documenté et illustré, permet de prendre la mesure du courage et de la force de résistance des femmes iraniennes Viabooks, 15/09/2024
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

Cet ouvrage remarquablement documenté et illustré, permet de prendre la mesure 
du courage et de la force de résistance des femmes iraniennes. Viabooks, 15/09/2024

L’ouvrage donne à lire de nombreux témoignages recueillis en 1979 et pour certains complétés en 2023 Le Monde Libertaire, 30/09/2024
-----------------------------------------------------------------------------------------------------------------------------------------

L’ouvrage donne à lire de nombreux témoignages recueillis en 1979 et pour 
certains complétés en 2023. 
Il est aussi augmenté par des extraits des journaux féministes publiés en
France. Le Monde Libertaire, 30/09/2024

Articles 2024
================

2024-12-07 nous-etions-aussi-rebelles-lune-que-lautre-iran
---------------------------------------------------------------

- https://www.en-attendant-nadeau.fr/2024/12/17/nous-etions-aussi-rebelles-lune-que-lautre-iran/


Dans un tel contexte, l’ouvrage Iraniennes. Femme, Vie, Liberté (1979-2024), 
réalisé par l’équipe MLF-Psychanalyse et Politique d’Antoinette Fouque et 
l’Alliance des femmes pour la démocratie, en collaboration avec des militantes 
de "Femme, Vie, Liberté", peut prêter à confusion et n’être pas pleinement reçu. 

Il clarifie pourtant, en historicisant le mouvement iranien, son lien avec 
l’Occident et sa dimension mondialiste, en faisant archive dans un sens plus 
classique, donnant accès aux récits et documents (y compris iconographiques) 
relatifs à la grande manifestation du 8 mars 1979, qui fut un tournant mais 
pas un acte de naissance. Un court chapitre consacré à la "révolte des femmes 
iraniennes depuis le XIXe siècle" rappelle qu’en 1911 300 femmes armées 
s’étaient rendues au parlement pour s’opposer à un ultimatum russe, cachant 
leur arme sous leur tchador ; qu’en 1921, la Journée du 8 mars avait été 
célébrée par un groupe d’Iraniennes militant pour la scolarisation des femmes 
au nord-ouest du pays, qui furent mises en prison par le Shah, comme quatre 
ans plus tard toutes les femmes engagées dans des activités politiques. 

En plus des tracts et télex témoignant des échanges franco-iraniens, ce 
memorandum reprend des extraits de numéros de l’hebdomadaire 
Des Femmes en mouvements, de novembre 1979 à juin 1982. 

La revue fait office de vigie des répressions et appelle à la solidarité des 
luttes dans un sens marxiste : le refus iranien de la loi des mollahs succède 
à celui du "viol émancipateur de l’impérialisme américain". 

L’ensemble montre les liens entre le mouvement iranien et les féminismes 
français et américain des années 1970, le rôle moteur de l’Américaine 
Kate Millett, autrice de La politique du mâle (1970), qui publia cette 
année-là Going to Iran et entraîna à Téhéran Simone de Beauvoir, et la 
déclinaison occidentale d’un féminisme qui se voulait mondial, 
"ni oriental ni occidental, la liberté des femmes est mondiale". 

Ce à quoi Khomeyni avait répliqué par avance : "le 8 mars est un slogan de l’Ouest".

La publication de ces télex, messages de soutien et actes de présence peut 
faire songer à un rappel de prérogatives, comme si le vieux MLF se projetait 
dans l’actuel mouvement iranien au nom du passé. 

Mais le volume fait entendre la voix des Iraniennes, de manière particulièrement 
forte dans la deuxième partie, consacrée au mouvement de septembre 2022 et 
à l’état présent du système carcéral. 

On y trouve sept lettres de prisonnières politiques de la prison d’Evin, 
certaines inédites, écrites par des femmes enfermées depuis 2019 ou 2022, 
ou bien davantage, jusqu’à quinze ans. 

L’une, signée par sept détenues, évoque les tortures en particulier mentales, 
dont celle de confronter les détenues à l’attente et la mise en scène de 
leur propre exécution, et la menace de mort qui pèse sur celles accusées 
d’"espionnage" et d’"entrée en guerre contre Dieu et corruption de la terre" – 
charge la plus grave, qui s’abat entre autres sur les militantes écologistes.

Le dossier attire l’attention sur ce fait méconnu et tragique. 

La criminalisation de la lutte pour l’environnement, croissante aussi dans 
les démocraties, est en Iran un problème brûlant. 

Or les femmes jouent dans cette mobilisation un rôle moteur, car ce sont 
elles qui ont en charge les tâches de la vie quotidienne et de la survie, 
comme faire boire de l’eau saine aux enfants : c’est aussi le sens du mot 
"vie" dans le slogan. 

Les femmes kurdes sont aussi doublement ciblées. 

L’une a été condamnée à la prison à perpétuité en 2008 pour "crime de belligérance" 
après avoir dit, dans un lycée de filles, l’importance de la Journée internationale 
du 8 mars. 

Plusieurs fois incarcérée depuis 1998, à nouveau depuis 2021, la militante 
des droits de l’homme Narges Mohammadi, qui vient d’être temporairement 
libérée pour raisons médicales, dit la douleur de la vie volée par la série 
des "murs" auxquels se heurtent les femmes : empêchement professionnel, 
censure, exclusion administrative…

Mais, comme pour l’environnement, "Femme, Vie, Liberté" vise la réappropriation 
de la vie par tous : "Si vous regardez attentivement la société iranienne,
dit-elle, vous verrez que chaque individu, à tout moment de sa vie et en tout 
lieu, est coupable du désir de vivre", et risque d’en mourir. 

Précieux sont aussi les quatre longs entretiens inédits avec des artistes, 
avocates, militantes des droits de l’homme, exilées en France, qui redisent 
cet empêchement de vivre mais aussi la formidable vitalité du mouvement et 
sa créativité. 

Chirinne Ardakani 
--------------------------

Chirinne Ardakani parle à ce sujet d’ "épiphanie militante" et insiste sur 
le rôle majeur des femmes dans la production intellectuelle liée à cette lutte, 
alors que beaucoup d’hommes restent "empêtrés dans cette violence virile". 

L’un des entretiens fait parler Sorour Kasmai
---------------------------------------------------

- https://www.instagram.com/sorourkasmai/

Née à Téhéran en 1962, elle a fui en 1983 son pays pour la France où, devenue 
romancière dans les deux langues, elle dirige la collection "Horizons persans" 
aux éditions Actes Sud. 

Elle dit la nécessité d’une historiographie longue de la lutte des femmes 
en Iran, remonte aux années 1910-1930, s’arrête sur la campagne 
"Un million de signatures" pour l’élimination des lois discriminatoires (1997-2006), 
qui a mis durablement à l’ordre du jour l’égalité entre les sexes, et sur le 
tournant de 1979-1983, quand la gauche anti-shah accepta les slogans islamistes 
au nom de la priorité anti-impérialiste, seul moyen pour les étudiants de 
se lier au "peuple", mais il s’y laissait voir une "espèce de satisfaction 
inconsciente misogyne". 

Ce consentement aggrava la mutation de la culture populaire, parfois son 
renversement : ceux qui au nom de Dieu incendièrent les cinémas étaient 
les mêmes que ceux qui s’y rendaient au sortir des mosquées.


Chirinne Ardakani participe à une rencontre Femme, vie, liberté au Salon des livres féministes “Délivrées !” salle Agora
==============================================================================================================================

.. figure:: images/recto_2024_11_09.webp

Chirinne Ardakani participe à une rencontre Femme, vie, liberté au Salon 
des livres féministes “Délivrées !” salle Agora.

Chirinne Ardakani avocate et militante active au sein du collectif Iran Justice, 
témoigne dans le livre Des Iraniennes, Femme, vie liberté 1979-2024 
(Des femmes-Antoinette Fouque, 2024).

Ce livre collectif, alliant les époques (1979, 2022) et les pays (France et Iran), 
s’inscrit dans la chaîne de solidarité qui soutient le combat des femmes 
iraniennes. 

Il comprend des archives inédites et des entretiens avec des militantes 
d’hier et d’aujourd’hui ; il restitue et transmet le témoignage des luttes 
passées et présentes. 

Plus que jamais, il est nécessaire de se rallier au cri « Femme ! Vie ! Liberté ! ».

Salon des livres féministes “Délivrées !” : Une quarantaine auteur.ices, 
artistes, cinéastes, éditeur.ices, viendront parler de leurs arts durant 4 jours 
de rencontres, masterclass, signatures, projections, performances, …


#livre #essai #rencontre #conference #iran #droitsdesfemmes #liberté 
#femmevieliberté #editionsdesfemmes #antoinettefouque @salon_delivrees 
@espacedesfemmesaf @librairie_des_femmes @femmevieliberte @from___iran 
@nedadiran @hura_mirshekari @chirinne_ardakani @sorourkasmai @sepideh_farsi 
@matrimoine_litterature_gpairel @un_cafe_un_livre @horizonetinfini 
@parlons_non_fiction @passagedesplumes @la_femme_affamee_de_lectures 
@la_malle_a_livres @mademoisellelit @mademoisellemoncoeur @hanael_parks 
@ally_lit_des_livres @ingrid.hill.bouquine @tempsdelecture @lovelitt_and_coffee 
@jelislanuit @sarahfouilloux @alinesirba


