.. index::
   pair: Livre; L'impossible Kurdistan - Du Rêve Inachevé À L'Assassinat Du Leader Kurde Ghassemlou (2024-04)

.. _livre_2024_04_ghassemlou:

==================================================================================================
2024-04 **L'impossible Kurdistan - Du Rêve Inachevé À L'Assassinat Du Leader Kurde Ghassemlou**
==================================================================================================

- https://www.institutkurde.org/activites_culturelles/event-642/
- https://www.librairiesindependantes.com/product/9782262103903/

Description
==============

Le combat d`Abdul Rahman Ghassemlou pour la liberté des Kurdes.

Le 13 juillet 1989, le leader kurde Abdul Rahman Ghassemlou est assassiné à
Vienne par des tueurs à gages de l`ayatollah Khomeini.

Pourquoi ce meurtre a-t-il été orchestré ? En quoi Ghassemlou constituait-il
une menace pour la République islamique ?

Cet acte terroriste aurait-il pu être évité ?

Dans cette biographe originale aux accents d`enquête politique, la journaliste
d`investigation Carol Prunhuber répond à toutes ces questions et à beaucoup
d`autres.

Ce faisant, elle dresse le portrait d`un homme exceptionnel, passionné,
cultivé et obsédé par un idéal : créer un Kurdistan autonome au sein d`un
Iran démocratique.

En effet, fier d`être kurde et iranien, Ghassemlou lutte toute sa vie pour
regrouper les Kurdes – répartis principalement entre la Turquie, l`Iran,
l`Irak et la Syrie – dans un même État. Secrétaire général du Parti démocratique
du Kurdistan d`Iran (PDKI) de 1973 à 1989, il devient une figure incontournable
de la résistance aux régimes autoritaires iraniens – celui du shah Mohammad
Reza Pahlavi d`abord, puis celui de l`ayatollah Khomeini dès 1979.

Dans cet ouvrage enfin traduit en français, Carol Prunhuber raconte ce rêve
inachevé, cette lutte pour la liberté et cette vie fauchée.

Spécialiste du monde kurde et proche de Ghassemlou, l`auteure nous emmène,
de Téhéran à Vienne, au cœur d`un univers fascinant et méconnu.

Alors que la République islamique d`Iran continue de perturber et de défier
l`Occident, L`Impossible Kurdistan est un ouvrage essentiel pour comprendre
le Moyen-Orient d`hier et d`aujourd`hui.


Interview
============

- :ref:`iran_luttes_2024:prunhuber_2024_04_02`
