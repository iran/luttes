.. index::
   pair: Livre ; La face cachée des mollahs (Emmanuel RAZAVI, 2024-01)

.. _mollahs_2024_01:

======================================================================================================
2024-01 **La face cachée des mollahs** par Emmanuel RAZAVI
======================================================================================================

- https://www.editionsducerf.fr/librairie/livre/20452/La-face-cachee-des-mollahs

Emmanuel Razavi
=================

Spécialiste du Moyen-Orient et grand reporter pour les rédactions de Paris Match,
Franc-Tireur ou encore Politique internationale, Emmanuel Razavi est l’auteur
de plusieurs documentaires et ouvrages sur les filières liées à l’islamisme.

.. figure:: images/couverture.webp
   :width: 600

Description
========================

- https://www.editionsducerf.fr/librairie/livre/20452/La-face-cachee-des-mollahs

Et si l’Iran était aujourd’hui la première organisation criminelle au monde ?
Le Corps des gardiens de la révolution islamique, un cartel de narcotrafiquants ?
En quoi les mollahs forment-ils une véritable mafia ?
Comment la République islamique a-t-elle étendu son emprise délétère, notamment
sur l’Europe, en finançant des groupes terroristes avec l’argent de la drogue ?

Des slogans de la révolution khomeiniste aux circuits du blanchiment d’argent sale,
Emmanuel Razavi met au jour la face cachée des mollahs et expose les rouages
de leur système mortifère.

Pasdarans, caïds, pilotes, dealers, mais aussi agents, peshmergas, avocats
et victimes : c’est en investigateur qu’il nous dévoile cet empire mondial du mal.

Un livre informé, implacable, indispensable



Articles 2024
=================


2024-01-21 Iran : révélations sur la face mafieuse du régime des mollahs
----------------------------------------------------------------------------------

- https://www.radiofrance.fr/franceculture/podcasts/signes-des-temps/la-republique-islamique-d-iran-quel-avenir-pour-le-regime-8183839

Résultat d'une année d'enquête clandestine et dangereuse, le livre
"La face cachée des mollahs" du grand reporter Emmanuel Razavi révèle
la dimension mafieuse et criminelle du régime de Téhéran.
Un livre à lire pour comprendre les réalités du Moyen-Orient derrière la
façade religieuse.

En Iran aujourd’hui, le mouvement Femme vie liberté est toujours victime
d’une répression terrible, des jeunes filles sont battues, violées et tuées
presque chaque jour simplement parce qu’elles refusent de porter le voile
islamique.
On marie des fillettes de 12 ans avec des hommes de 50, et alors que
l’adultère est passible de la peine de mort, les religieux ont institutionnalisé
le mariage provisoire qui peut durer d’une heure à 99 ans, baptisé le Sigheh,
véritable couverture légale à la pratique de la prostitution.

Ce que l'on sait moins, c'est l'économie noire qui assure à base de trafic
de drogue, d'armes et de femmes la survie financière des dirigeants, en particulier
des Gardiens de la révolution islamique qui contrôlent aujourd'hui 60 %
de l'économie du pays.
À l'heure où le régime des mollahs principal soutien du Hamas à Gaza lance
une série de frappes en Syrie, en Irak et au Pakistan, faisant monter
d’un cran la tension déjà très forte dans la région depuis le 7 octobre, à
l'heure où l’ONU choisit honteusement Téhéran pour présider la conférence
internationale sur le désarmement nucléaire qui doit se tenir à partir du 18 mars,
il faut lire ce livre formidable pour comprendre les enjeux clandestins
derrière la politique visible.


Marc Weitzmann reçoit le grand reporter Emmanuel Razavi alors que paraît
La face cachée des mollahs : l’enquête exclusive sur la république islamique
d’Iran aux éditions du Cerf.

Le grand reporter Emmanuel Razavi rappelle le fonctionnement totalitariste
de la République islamique d'Iran. Il poursuit, soulignant l'aberration que
l'Iran préside prochainement au mois de mars la conférence internationale
sur le désarmement nucléaire.

Il relate le sort réservé à la jeunesse emprisonnée, avec le viol systématique
qui brise la volonté.
Emmanuel Razavi évoque la relation historique entre les services secrets
russes et iraniens et leur actuelle coopération.
Il poursuit sur le corps des gardiens de la Révolution, milice paramilitaire
au fonctionnement mafieux.

Emmanuel Razavi analyse : "l'Iran représente une menace, une réelle menace
bien sûr pour Israël, et pour l'ensemble des démocraties occidentales.
Une menace par ses schémas fondateurs qui sont anti-occidentaux, antisémites,
mais aussi par son alignement avec des pays comme la Russie qui nous met
aussi en danger actuellement."

