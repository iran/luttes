.. index::
   pair: Livre ; Nous n'avons pas peur

.. _pas_peur_2024_03_04:

======================================================================================================
2024-03 **Nous n’avons pas peur**
======================================================================================================

.. figure:: images/couverture.webp
   :width: 600

Description
========================

- https://editionsdufaubourg.fr/livre/nous-navons-pas-peur

En écho au mouvement **Femme, Vie, Liberté**, 16 femmes iraniennes livrent
ici leurs témoignages. Ces voix s’élèvent parfois depuis l’exil, parfois
depuis des cellules de prison.

Elles parlent d’une vie sans droits contrôlée par la police des mœurs,
d’humiliations, de mise sous tutelle et de détresse économique.

Mais aussi d’une nouvelle génération, d’une révolution que plus rien ne
pourra arrêter, de libertés qui se gagnent pas à pas et de l’incroyable
résilience du peuple iranien.

Leurs textes sont bouleversants, remplis de larmes et porteurs d’espoir.

Leur bravoure est une leçon d’humanité.

Que ce livre soit leur porte-parole !

Avec les témoignages de :

- Golshifteh Farahani,
- Ghazal Abdollahi,
- Parastou Forouhar,
- Shohreh Bayat,
- Shila Behjat,
- Ani,
- Nargess Eskandari-Grünberg,
- Fariba Balouch,
- Rita Jahanforuz,
- Jasmin Shakeri,
- Shirin Ebadi,
- Masih Alinejad,
- Narges Mohammadi,
- Nazanin Boniadi,
- Nasrin Sotoudeh,
- Leily.

Traduit de l'allemand par Mathilde Ramadier, sauf pour le témoignage de
Golshifteh Farahani, recueilli par Sophie Caillat.


Golshifteh Farahani Sur Arte TV
======================================

- https://www.arte.tv/fr/videos/119214-001-A/golshifteh-farahani-et-les-femmes-iraniennes-defient-les-mollahs/


Dans **Nous n’avons pas peur**, publié aux éditions du Faubourg, l'actrice
Golshifteh Farahani est l’une des seize voix recueillies par les journalistes
Natalie Amiri et Düzen Tekkal.

Des témoignages de femmes iraniennes qui racontent leurs luttes et leurs espoirs,
depuis l’exil ou la détention.
