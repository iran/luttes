.. index::
   pair: Livre ; Torture blanche (2024)
   pair: Narges Mohammadi ; Torture blanche (2024)

.. _torture_blanche_2024:

======================================================================================================
2024-03 **Torture blanche** de Narges Mohammadi
======================================================================================================

- https://www.albin-michel.fr/torture-blanche-9782226492678

.. figure:: images/recto.webp

:Préface de: Shirin Ebadi
:Traducteur: Didier Ausan
:Postface de: Shannon Woodcock


Description
========================


"Le 16 novembre 2021, pour la douzième fois de ma vie, j'ai été arrêtée et 
pour la quatrième fois condamnée à être placée en cellule d'isolement. 
Cette fois, mon chef d'accusation, vous le tenez entre vos mains : c'est ce livre, 
Torture blanche. 
On m'enfermera encore. Mais je continuerai de me battre jusqu'à ce que les 
droits humains et la justice règnent dans mon pays." 

Narges Mohammadi

Infatigable militante des droits humains, Narges Mohammadi, prix Nobel de 
la Paix 2023, livre depuis la prison de Zanjan où elle est incarcérée le 
tableau terrifiant du traitement infligé par la République islamique d'Iran 
aux prisonnières politiques et aux militantes, soumises à la "torture blanche", 
une peine d'isolement total. 

Les entretiens qu'elle a menés, dans des conditions particulièrement dangereuses, 
avec treize détenues constituent un témoignage de résistance unique et un acte 
de courage qui s'inscrit dans le sillage du mouvement "Femme, vie, liberté"

