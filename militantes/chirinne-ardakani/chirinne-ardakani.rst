.. index::
   ! Chirinne Ardakani

.. _chirinne_ardakani:

===============================================
**Chirinne Ardakani** |ChirinneArdakani|
===============================================

- https://www.iran-justice.fr/
- https://www.instagram.com/chirinne_ardakani/
- https://www.instagram.com/iranjusticeorg/
- https://fr.linkedin.com/in/chirinne-ardakani-9b989751
- https://normandiepourlapaix.fr/personnes-structures/ardakani

.. figure:: images/chirinne_ardakani.png

Biographie
=============

Chirinne Ardakani est avocate. 

Elle exerce en droit pénal et en droits des étrangers et défend, en particulier, 
les dissidents politiques à travers le monde dont la militante féministe et 
anti-peine de mort **Narges Mohammadi récipiendaire du prix Nobel de la paix**. 

Résolument attachée aux droits de la Défense et aux droits humains, notamment 
des personnes exilées, elle conçoit sa pratique comme un outil concret de 
mobilisation en faveur des luttes démocratiques et des libertés politiques. 

En septembre 2022, en réaction au meurtre de Jina Mahsa Amini, kurde iranienne 
par la police des mœurs iranienne, elle fonde l’association Iran Justice. 

Rassemblant avocats, juristes et militants des droits humains, le collectif 
inventorie, documente et qualifie les exactions du régime iranien dans l’objectif 
de traduire, un jour, ses auteurs en justice, pour que cesse l'impunité des 
crimes d'Etat en Iran. 

Rompu au contentieux stratégique, Iran justice est à l’origine de plusieurs 
actions judiciaires, dont plusieurs plaintes visant des dignitaires iraniens 
et de saisines du groupe de travail sur la détention arbitraire de l’ONU. 

Iran Justice plaide pour la reconnaissance internationale et la criminalisation 
de l’apartheid de genre pour en finir avec la ségrégation légale et les 
persécutions qui sévissent dans certains états du monde à raison du genre ou 
de l’orientation sexuelle. 

Militante pour les droits des femmes, Chirinne Ardakani est par ailleurs 
co-autrice de l’ouvrage collectif "Les iraniennes, de 1979 à 2024" paru en 
septembre 2024 (édition des femmes Antoinette Fouque), livre politique qui 
retrace les luttes des mouvements féministes iraniens à travers les époques.
 
Présidente de Iran Justice, Avocate de Narges Mohammadi, Prix Nobel de la 
paix 2023 actuellement emprisonnée en Iran


Biographie 2
================

Chirinne Ardakani est avocate et défend, en particulier, les dissidents 
politiques à travers le monde dont la militante féministe et anti-peine 
de mort Narges Mohammadi récipiendaire du prix Nobel de la paix ainsi que 
des otages arbitrairement détenus par les régimes autoritaires.

En septembre 2022, en réaction au meurtre de Jina Mahsa Amini, kurde iranienne 
par la police des mœurs iranienne, Chirinne Ardakani fonde l’association 
Iran justice. 

Rassemblant avocats, juristes et militants des droits humains, le collectif 
inventorie, documente et qualifie les exactions du régime iranien dans 
l’objectif de traduire, un jour, ses auteurs en justice, pour que cesse 
l’impunité des crimes d’État en Iran.

Iran Justice plaide pour la reconnaissance internationale et la criminalisation 
de l’apartheid de genre pour en finir avec la ségrégation légale et les 
persécutions qui sévissent dans certains états du monde à raison du genre 
ou de l’orientation sexuelle.

#iran #femmes #droitsdesfemmes #nolandssong @chirinne_ardakani @iranjusticeorg

Biographie 3
================

- https://www.youtube.com/watch?v=w1RTSsCl-38

« Jîna Mahsa Amini meurt parce que c'est une femme qui refuse de se soumettre à la loi patriarcale »

Chirinne nait et grandit en France et l’Iran n’est pour elle que le pays de 
ses parents où elle se rend régulièrement en vacances. Étudiante en droit, 
militante, et engagée à gauche, celle qui deviendra avocate du droit des 
étrangers  découvre son « iranité » au gré des décisions politiques qui la 
renvoient à  son double identité : la déchéance de nationalité en France (04:18), 
ou le « muslim ban » aux Etats-Unis (07:08). 

Profitant de la relative ouverture du pays suite à la signature de l’accord 
sur le nucléaire iranien, elle décide de se rendre en 2018 et en 2019 en 
Iran pour la première fois sans sa mère, et investit sa double nationalité, 
sa double culture avec une émotion et une conviction politique nouvelle. (08:46).
  
Avec le mouvement Femme Vie liberté, la révolution féministe des iraniennes 
devient alors pleinement son combat. 


Articles/interventions en 2025
======================================

- :ref:`arthaud_grenoble:chirinne_ardakani_2025_03_22`

Articles/interventions en 2024
======================================

- https://www.youtube.com/watch?v=ahjXSAxkON4

.. youtube:: ahjXSAxkON4


Octobre 2024
------------------

- https://www.instagram.com/p/DAqguiBIequ/


Articles 2023
==========================

- https://www.lemonde.fr/m-le-mag/article/2023/03/23/en-france-la-diaspora-iranienne-brandit-l-arme-du-droit-contre-le-regime-des-mollahs_6166620_4500055.html
