.. index::
   ! Le Cercle persan

.. _cercle_pezrsan:

===================
Le Cercle persan
===================

- https://www.instagram.com/lecerclepersan/

Est une association Franco-Persane/Afghane à but non lucratif fondée afin de 
promouvoir les échanges socioculturels entre la France et le monde persan
