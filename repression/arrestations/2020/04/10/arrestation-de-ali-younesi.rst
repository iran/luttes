.. index::
   ! Ali Younesi

.. _ali_younesi_2020_04_10:

=====================================================
2020-04-10 **Arrestation de Ali Younesi**
=====================================================

- https://www.iranpoliticalprisoners.org/post/ali-younesi
- https://iranpolitprisoners.wixsite.com/website/post/ali-younesi

- :ref:`miryoussef_younesi_2022_12_28`

.. figure:: images/ali_younesi.png
   :align: center


Iranian university student, Ali Younesi of Sharif University was arbitrarily
detained by the Islamic Revolutionary Guard Corps on 10 April 2020.

As of July 2020, he has yet to be released and reports state that he has
contracted the coronavirus in prison.


After almost a month of secrecy, and following student protests at Sharif
University, the Iranian regime's judiciary finally stated on 5th May
that Ali Younesi, and a fellow colleague Amir Hossain Moradi, were detained.

Amir Hossein Moradi disappeared on April 10, and Ali Younesi was brought
home in the evening of the same day by authorities linked to the Islamic
Revolutionary Guard Corps, with injuries and torture marks. He was later
detained and held at Iran's notorious Evin prison.

Ali Younesi, who is only 20 years old, has achieved many awards in his
line of study. Ali won the gold medal of the 12thI nternational Olympiad
on Astronomy and Astrophysics, held in China in 2018.

Earlier, he had won the silver and gold medals of the National Astronomy
Olympiad in 2016 and 2017.

