.. index::
   pair: Arrestation ; Morvarid Ayaz
   pair: Morvarid ; Ayaz
   ! Morvarid Ayaz

================================================================================================
**Morvarid Ayaz**, chercheuse à @SorbonneParis1 et mère d’une fillette de 5 ans #MorvaridAyaz
================================================================================================

.. #BeHerVoice #MorvaridAyaz #IranProtests2022 #IranianWomenLivesMatter


2022-10-10 Le GSRL soutient Morvarid Ayaz
=============================================

- https://www.gsrl-cnrs.fr/ (Groupe Sociétés, Religions, Laïcités)
- https://twitter.com/LaboGSRL
- https://twitter.com/LaboGSRL/status/1579394774855217153?s=20&t=aB5rqob7iMyb3PMxqz7PaA
 
Morvarid Ayaz, sociologue et docteure associée au Groupe Sociétés, Religions,
Laïcités (GSRL, EPHE/PSL-CNRS, Paris), a été arrêtée le 21 septembre 2022
à Rasht au nord-ouest de l’Iran.

Convoquée au commissariat de "Farzaneh" pour "donner certaines explications,
sans plus de précisions" selon son époux Arash Naimian, elle a été arrêtée,
emprisonnée, puis transférée quelques jours plus tard "de la prison de
Lakan de Rasht à Téhéran, mais aujourd’hui, personne ne sait où elle se
trouve exactement", précise son époux.

Malgré les demandes répétées de son avocat, la caution pour sa libération
conditionnelle a été refusée.
"Je suis très inquiet pour elle" conclut-il. Mère d’une fille de 5 ans,
elle vit depuis 2017 en Iran, et n’est membre d’aucune organisation politique.

 
Morvarid Ayaz a soutenu en 2017 une thèse de doctorat à l’École Pratique
des Hautes Études sous la direction de Jean-Paul Willaime, intitulée :
**La perception de l’islam d’Iran du point de vue de la sociologie de la
connaissance de l’Iran contemporain**.


Nous, ses collègues du Groupe Sociétés, Religions, Laïcités, demandons
aux autorités iraniennes sa libération immédiate.



.. _marvarid_ayaz_2022_10_04:

2022-10-04 **Toujours pas de nouvelles de Morvarid Ayaz**, chercheuse à @SorbonneParis #BeHerVoice #MorvaridAyaz @sup_recherche @education_gouv @PapNdiaye
===============================================================================================================================================================

- https://twitter.com/PerrineCoet/status/1577314434564820993?s=20&t=uBmjnbwo1kkGn8dDMPzrCA

Thèse de Morvarid Ayaz, disparue ces derniers jours en Iran: Sociologie
de la connaissance du chiisme dans l'espace des savoirs sur l'Iran en
France (1947-2010) "Dans l’espace des savoirs sur l’Iran, les énoncés
concernant sa dimension religieuse ne sont pas rares." #BeHerVoice


2022-10-01 Tweet de Jul
============================

Morvarid Ayaz. Mon amie, chercheuse à @SorbonneParis1 et mère d’une
fillette de 5 ans. Arrêtée en Iran il y a plusieurs jours et transférée
dans un endroit secret.

Parlez d’elle. #behervoice #opIran #womenlifefreedom

@sup_recherche @education_gouv @PapNdiaye

.. figure:: images/marvarid_ayaz.png
   :align: center

   https://twitter.com/jul_auteur/status/1576221540223692801?s=20&t=e8maBfy3jEMKxJtbPC4I4A



