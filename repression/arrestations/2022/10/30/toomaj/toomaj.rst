.. index::
   pair: Toomaj; Arrestation
   ! Toomaj
   ! Toomaj Salehi

.. _toomaj_arrestation:
.. _toomaj:

=====================================================================================================
2022-10-30 **Arrestation de Toomaj Salehi** #freetoomaj "Pesare Azadi" (Le fils de la liberté)
=====================================================================================================

- https://fr.wikipedia.org/wiki/Toomaj_Salehi


.. figure:: images/toomaj.png
   :align: center


Introduction
============

Toomaj ou Tomaj, de son vrai nom **Toomaj Salehi** (persan: توماج صالحی) est un
rappeur iranien engagé très populaire.

Dans ses textes, il évoque les problèmes de la société iranienne et les
mobilisations contre la République islamique. Il a été arrêté, une première
fois, le 12 septembre 2021, puis relâché sous caution.

Dans le cadre du  mouvement de protestation qui a suivi la mort de Masha
Amini à l'issue de son arrestation par la police des mœurs en septembre 2022,
Toomaj est devenu l'une des voix de la contestation contre la répression
mise en œuvre par le régime, encourageant la population, dans des vidéos
postées sur ses comptes Instagram et Twitter, à descendre dans la rue
pour protéger les manifestants attaqués par les forces de sécurité.

**Il est arrêté, à nouveau, le 30 octobre 2022.**

Ses proches craignent qu'il soit victime de torture et qu'il soit condamné
à mort par le régime islamique.



Biographie
===================

Toomaj est né le **3 décembre 1990** à Chaharmaha. Il est originaire de la
tribu des Bakhtiari, un peuple nomade iranien dont le territoire couvre
la partie méridionale des chaînes du Zagros, entre Ispahan et Ahvaz.

Au sujet de son prénom, Toomaj a précisé dans une interview qu'il s'agit
de son vrai prénom qui est d'origine turque. C'est le nom d'une montagne
célèbre et il signifie aussi "tempête".

Quand il était enfant, sa famille et lui déménagent à Shahin Shahr, un
quartier d'Ispahan où il a grandi.
La situation financière de sa famille s'est progressivement détériorée
à la suite de l'emprisonnement de son père, arrêté en raison de ses
activités politiques.

Toomaj a fait des études de mécanique et de design industriel dans deux
universités. Il a ensuite travaillé plusieurs années avec son père dans
l'atelier de conception et de production de pièces industrielles et
médicales que son père a ouvert, une fois sorti de prison.

Toomaj débute dans le rap en 2016 alors qu'il est âgé de 26 ans.


Liens
======

- :ref:`iran_luttes_2024:cgt_2024_05_07`
- :ref:`akrami_iran:toomaj_2023_09_22`
- :ref:`akrami_iran:toomaj_2023_02_25`
- :ref:`akrami_iran:toomaj_2022_12_03`
