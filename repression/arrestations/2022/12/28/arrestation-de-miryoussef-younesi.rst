.. index::
   ! Miryoussef Younesi

.. _miryoussef_younesi_2022_12_28:

============================================================================
2022-12-28 **Arrestation de Miryoussef Younesi, le père de Ali Younesi**
============================================================================

- :ref:`ali_younesi_2020_04_10`


.. figure:: images/ali_et_miryoussef_younesi.png
   :align: center


- https://nitter.manasiwibi.com/arminarefi/status/1608366645403082753#m

NON CONTENT d’avoir arrêté et condamné à 16 ans de prison au mois d’avril
Ali Younesi, étudiant de 22 ans de la prestigieuse université Sharif de
Téhéran, le régime iranien vient d’arrêter son père Miryoussef Younesi
mercredi 28 décembre chez lui à Shahroud (nord de l’#Iran).


- https://nitter.poast.org/IranHrm/status/1608152878606159874#m


Father of Iranian international astronomy Olympiad gold winner - 2018
#Ali_Younesi has been detained.

MirYousef Younesi, was detained this morning in Shahrud, Semnan Province.
Six plainclothes agents arrested him while he was at his parents home.


- https://nitter.manasiwibi.com/HRANA_English/status/1608165875198169089#m

Today, December 28, security forces arrested #MirYousef_Younesi, the
father of imprisoned student #Ali_Younesi, in #Shahrud, Semnan Province.

Ali Younesi, a Sharif University of Technology student, is currently
serving his sentence in Evin Prison.

- https://nitter.bird.froth.zone/OrgIAC/status/1608131639166525443#m

Father of #Ali_Younesi, Miryousef Younesi, was arrested earlier today in
city of Shahrud. Ali is an internationally award winning student of
Sharif University who was sentenced to 16 yrs on false charges.

#IranProtests #IranRevolution @UNHumanRights @StateDRL @AmnestyIran @hrw

