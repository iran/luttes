
.. _paniz_karami_2023_01_28:

=====================================================================================
2023-01-28 **La blogueuse musicale iranienne #PanizKarami a été arrêtée**
=====================================================================================

- https://nitter.poast.org/IranHrm/status/1620315043186475008#m


::

    Iranian music blogger #PanizKarami was arrested Jan 10, 2023
    #Iran's security forces selectively took the 25-YO from Tehran and
    she has been charged with 'rioting in cyberspace.'
    Like many other protest detainees, she had to put forward a heavy
    bail to be released until trial day

::

    Les forces de sécurité iraniennes ont arrêtée arbitrairement la jeune
    femme de 25 ans à Téhéran et  elle a été inculpée pour "émeute dans le cyberespace".
    Comme beaucoup d'autres détenus protestataires, elle a dû avancer une lourde
    caution pour être libérée jusqu'au jour du procès


.. images/paniz_karami.png
   :align: center
