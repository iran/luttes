
.. _arrestations_zahedan_2023_01_28:

=====================================================================================
2023-01-28 **Les arrestations arbitraires d'adolescents se poursuivent en Iran**
=====================================================================================

Arbitrary arresting teenagers continues by #Iran

::

    Jan.28 four Baluchi citizens violently beaten & taken away by plainclothes
    agents in #Zahedan Families unclear of their locations!

::

    quatre citoyens baloutches violemment battus et emmenés par des agents en civil



.. figure:: images/mostafa_shahuzehi.png
   :align: center

   Mostafa Shahuzehi-14 (TL)


.. figure:: images/yasin_shahuzehi.png
   :align: center

   Yasin Shahuzehi-15 (TR)


.. figure:: images/erfan_barahui.png
   :align: center

   Erfan Barahui-14 (BL)


.. figure:: images/uthman_rakhshani.png
   :align: center

   Uthman Rakhshani-19 (BR)

