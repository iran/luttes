.. index::
   pair: Arrestations; 2023-04-30

.. _arrestations_iran_2023_04_30:

===========================================================================================================
2023-04-30 L’arrestation à la veille du 1er mai de 9 militants de droits sociaux et syndicalistes
===========================================================================================================

- http://nitter.smnz.de/LettresTeheran/status/1652590600925970437#m

L’arrestation à la veille du 1er mai 2023 de 9 militants de droits sociaux et
syndicalistes.

Reihaneh Ansarinejad, Asal Mohammadi, Anisha Asadollahi, Kamiyar Fakour,
Hirad Pirbadaghi, Hassan Ebrahimi, Sarvenaz Ahmadi, Aldouz Hashemi et
Jaleh Rohzadeh, ont été arrêtés par les forces de sécurité et ont été
transférés à la prison d’Evin.


.. figure:: images/les_9_militants.png
   :align: center
