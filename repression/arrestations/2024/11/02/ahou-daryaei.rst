.. index::
   pair: Arrestation ; Ahou Daryaei (2024-11-02)

.. _arrestation_ahou_daryaei_2024_11_02:

==========================================
2024-11-02 **Arrestation d'Ahou Daryaei**
==========================================

- https://fr.wikipedia.org/wiki/Ahou_Daryaei

La jeune femme de l’université des sciences et de la recherche 
(en farsi : دخترعلوم و تحقیقات), possiblement nommée Ahou Daryaei 
(en farsi : آهو دریایی, Āhū Daryāyī, prononcé ɒːˈhuː dæɾjɒːˈjiː) — est une 
doctorante iranienne de 30 ans en littérature française à l’Université 
islamique Azad de Téhéran.

Le 2 novembre 2024, après avoir été harcelée par la milice islamiste Basij, 
qui l'aurait prise à partie pour non-port du hidjab, obligatoire dans ce 
pays, et lui aurait déchiré des vêtements ; elle aurait réagi en se déshabillant 
partiellement et en s'asseyant dans la cour de l'université pour protester. 

Son acte audacieux a fait d'elle un symbole de la résistance contre les lois 
strictes du code vestimentaire iranien et l'application du hijab obligatoire, 
deux ans après la mort de Mahsa Amini pour les mêmes raisons.

À la suite de son acte de résistance, elle a été arrêtée par la milice de 
la république islamique d'Iran. 

Sa localisation et son état de santé demeurent incertains, certains rapports 
suggèrent qu'elle pourrait être détenue dans un établissement psychiatrique, 
ce qui suscite des préoccupations internationales quant à son traitement. 

En Iran, les autorités ont une pratique documentée de qualifier de mentalement 
instables les femmes qui s'opposent aux lois sur le hijab, afin de réprimer 
les dissidences. 

