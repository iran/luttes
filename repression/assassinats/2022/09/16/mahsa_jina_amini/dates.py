"""Calcul du nombre de jours depuis la mort de MasahAmini
"""

import pendulum
mort_de_jina_amini = pendulum.datetime(2022,9,16, 11)
# from_day = pendulum.now()
from_day = pendulum.datetime(2022,12,1, 15)
duree = from_day - mort_de_jina_amini
print(f"Mort de JinaAmini:{mort_de_jina_amini.to_datetime_string()}\nHeures:{duree.in_hours()}\nJours:{duree.in_days()}\nSemaines:{duree.in_weeks()=}\n{duree.in_words()}")
