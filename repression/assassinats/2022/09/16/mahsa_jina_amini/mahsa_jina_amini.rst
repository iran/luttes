
.. index::
   pair: Jina ; Amini
   pair: Jîna ; Emînî
   ! Mahsa Jina Amini
   ! Jîna Emînî


.. _mahsa_jina_amini:
.. _jina_amini:
.. _jina_amini_2022_09_16:

============================================================================================================================================================================================================
**Mahsa Jîna Amini** (née le 21 septembre 1999, assassinée le 16 septembre 2022 à Téhéran) ##JinaMahsaAmini #MasahAmini #MasahAmini #Masah_Amini  #OpIran #مهساامینی
============================================================================================================================================================================================================

- https://hengaw.net/en/news/zhina-amini-died-due-to-severe-injuries

.. #NousSommesLeursVoix #zhinaAmini #JinaAmini #MasahAmini #MasahAmini #Masah_Amini #Jîna_Emînî #Jina_emini #OpIran #مهساامینی
.. #EndIranRegime #IranProtests #LetUsTalk  #WhiteWednesdays #TwitterKurds #مهسا_امینی #Rojhilat #JinaMahsaAmini #opiran
.. #EndIranRegime #IranProtests #LetUsTalk  #WhiteWednesdays #TwitterKurds #مهسا_امینی #Rojhilat #MahsaAmini #OpIran
.. #IranRevolution #IranProtests2022 #ZanZendegiÂzâdi #JinJiyanAzadî #WomenLifeFreedom #FemmesVieLiberte
.. #NousSommesLeursVoix #Masah_Amini  #OpIran #مهساامینی #IranRevolution #IranProtests2022

.. figure:: images/jina_emini_kurde_photo.png
   :align: center


.. figure:: images/mahsa_amini_en_persan.png
   :align: center
   :width: 300

   Masah Amini, #مهساامینی


La femme aux deux noms : Jîna Amini
=====================================

- https://kurdistan-au-feminin.fr/2022/09/24/le-kurdistan-oriental-terreau-de-soulevements-populaires/

:term:`Jîna <Jin (Kurde)>` Amini était une jeune femme kurde de 22 ans originaire de la ville
kurde orientale de Seqiz (Saqqez). Cependant, elle est appelée «Mahsa Amini»
sur les réseaux sociaux et la plupart des rapports actuels sur la situation
en Iran.

.. figure:: images/saqqez.png
   :align: center

   Saqqez (en persanسقز ; en kurde سەقز, Saghez, Saqqiz ou Sakiz) est la capitale du département de Saqqez, dans la province iranienne du Kurdistan. , https://fr.wikipedia.org/wiki/Saqqez

C’est un signe de la politique anti-kurde que l’Iran mène depuis des décennies.

**Jîna** est kurde et signifie femme ; cependant, le nom kurde est interdit,
c’est pourquoi ses parents ont été contraints de lui donner un autre nom
officiel à l’époque. C’est pourquoi Jîna était appelée « Mahsa » par les
autorités, ce qui vient du vieux persan et signifie « comme la lune » ou «la lune».

Le mardi 13 septembre 2022, Jîna et son frère se
rendaient de leur ville natale à Téhéran lorsqu’ils ont été arrêtés par
la brigade des mœurs. Ils voulaient arrêter Jîna à cause de sa tenue
«non islamique».

Sa tenue «non islamique» comportait une partie de ses cheveux sous le
hijab. Elle a donc été emmenée dans un commissariat.
Après quelques heures, Jîna est tombée dans le coma puis a été emmenée
à l’hôpital, où elle est finalement décédée le 16 septembre 2022.

Alors que les autorités officielles parlaient de « problèmes cardiaques
soudains » et plus tard aussi de maladies antérieures, le public a
compris assez rapidement ce qui avait dû se passer.

Jîna avait déjà été battue et torturée par plusieurs policiers sur le
chemin du commissariat et cela a continué au commissariat. Les gens le
savaient parce que des milliers d’autres avaient vécu cela auparavant,
parce que le frère de Jîna a dû lui-même être témoin des premiers
passages à tabac et parce que quelques jours plus tard, un groupe de
hackers via « Iran International», un média de l’opposition iranienne
en exil, a fait scanner les scanner et images du dossier de Jîna qui
ont été divulgués montrant clairement qu’elle est morte d’un traumatisme
crânien.

Le gouvernement iranien a immédiatement tenté de dissimuler ce qui
s’était passé.
Les autorités policières ont exigé que les parents de Jîna enterrent
le corps de leur fille à huis clos le soir même. Cependant, ils ont
refusé et ne l’ont enterrée que le lendemain matin à Seqiz, où le soulèvement
populaire en Iran a commencé.

Début des soulèvements populaires
-------------------------------------

Le 17 septembre 2022, les soulèvements de Seqiz (Saqqez) ont commencé,
qui se sont rapidement propagés à d’autres villes kurdes orientales
telles que Ûrmiye (Urmia), Serdeşt (Sardasht) et Sînê (Sanandaj).

Les jeunes femmes ont dominé ces soulèvements depuis le début.

Les deux :ref:`slogans centraux <slogans>` qui résonnent dans les rues de presque toutes
les villes d’Iran depuis des jours sont :ref:`Jin, Jiyan, Azadî (Femmes, vie, liberté) <femmes_vie_liberte>`
et :ref:`Bimre dîktator (Mort au dictateur) <mort_au_dictateur>`.

Au début, l’État iranien a désespérément essayé de vendre sa version de
la mort de Jîna comme la vérité. Ainsi, le père de Jîna devrait être
obligé d’expliquer que sa fille souffrait en fait de maladies antérieures.

**Cependant, il n’a pas cédé aux pressions des autorités iraniennes**.

**Une muselière a également été imposée aux médecins traitants**. Ils n’ont
pas été autorisés à dire un mot sur les circonstances de la mort au public.

Ensuite, les responsables de l’État font circuler le mythe habituel selon
lequel les soulèvements populaires sont contrôlés de l’étranger.

Mais les gens du pays n’achètent plus cela à l’État.

Trop de personnes sont en prison et condamnées à la peine de mort, en
grande partie parce que leurs opinions politiques diffèrent de celles
au pouvoir en Iran ; trop de personnes disparaissent sans laisser de
traces, trop de femmes sont torturées chaque jour et l’oppression
des minorités en Iran est trop grande.



Say her name Jina Amini, #JinaMahsaAmini, Woman, Life, Freedom
------------------------------------------------------------------

- https://twitter.com/KurdistanAu/status/1573996395652071424?s=20&t=iA8CBOSiOy890yMMe0Zogw

.. figure:: images/say_her_name.png
   :align: center

   Say her name Jina Amini, #JinaMahsaAmini Woman, Life, Freedom


Le 21 septembre, Jina Mahsa Amini aurait eu 23 ans. Les mollahs l'ont tuée le 16 septembre, l'empêchant de souffler ses 23 bougies qui embrasent tout l'Iran depuis...
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://twitter.com/KurdistanAu/status/1573968143231377408?s=20&t=iA8CBOSiOy890yMMe0Zogw
- https://kurdistan-au-feminin.fr/2022/09/25/le-21-septembre-jina-mahsa-amini-aurait-eu-23-ans-les-mollahs-lont-tuee-le-16-septembre-lempechant-de-souffler-ses-23-bougies-qui-embrasent-tout-liran-depuis/

Le 21 septembre, Jina Mahsa Amini aurait eu 23 ans. Les mollahs l'ont
tuée le 16 septembre 2022, l'empêchant de souffler ses 23 bougies qui
embrasent tout l'Iran depuis...

.. figure:: images/jina_emini_kurde.png
   :align: center

   Le 21 septembre, Jina Mahsa Amini aurait eu 23 ans, https://twitter.com/KurdistanAu/status/1573968143231377408?s=20&t=iA8CBOSiOy890yMMe0Zogw


.. figure:: images/mahsa_amini_hopital.png
   :align: center
   :width: 600

.. _parents_jina_amini:

Les parents de Jina Mahsa Amini
----------------------------------

- :ref:`niloofar_hamedi`

.. figure:: images/mahsa_amini_hopital_parents.png
   :align: center
   :width: 600

   Parents of Mahsa Amini in Kasra Hospital in Iranian capital Tehran (:ref:`Niloofar Hamedi <niloofar_hamedi>`) https://www.middleeasteye.net/news/iran-mahsa-amini-journalist-broke-news-jail

.. figure:: images/tweet_sur_jina.png
   :align: center

   https://twitter.com/chowmak/status/1571229379606323208?s=20&t=THQuX7a2q36lRn3BIWWTJw

Commentaire de Masih Alinejad le vendredi 30 septembre 2022
---------------------------------------------------------------

A new photo of #MahsaAmini who’s tragic death by hijab police has united
the world to stand up against tyranny.

She is looking at us with a beautiful smile, without knowing that her
name became a symbol of resistance to end gender apartheid regime.

#مهسا_امینی
@SamRasoulpour


.. figure:: images/mahsa_amini_1.png
   :align: center
   :width: 600

   https://twitter.com/AlinejadMasih/status/1575895751900553216?s=20&t=roQ9myRjz3EW5EnQ9CgmBQ

2022-10-03 Rendre à Mahsa Amini son vrai prénom **Jîna** et aux femmes kurdes la maternité du slogan **Femme, Vie, Liberté**
------------------------------------------------------------------------------------------------------------------------------------

- :ref:`vrai_prenom_jina_2022_10_03`


- https://twitter.com/KurdistanAu/status/1579562710790213632?s=20&t=wZMviB-wNRN0RfdgtM5T0g



Les meutriers et meutrières de Jina Mahsa Amini
==================================================

- https://www.iranintl.com/en/202210025867


.. figure:: images/les_meurtriers_et_meurtrieres.png
   :align: center


Jîna Amini en habits traditionnels kurdes
===========================================

Jîna Amini en habits traditionnels kurdes...

.. figure:: images/jina_amini_habit_kurde.png
   :align: center
   :width: 600


#EndIranRegime #LetUsTalk #TwitterKurds #مهسا_امینی #MahsaAmini #opiran
#IranRevolution2022 #IranProrests2022 #JinJiyanAzadi #SayHerName #Rojhilat #Iran

Manifestations de soutien à Grenoble
=====================================

- :ref:`iran_grenoble_2022_11_12`
- :ref:`iran_grenoble_2022_11_05`
- :ref:`iran_grenoble_2022_10_29`
- :ref:`freedom_iran_2022_10_22`
- :ref:`iran_grenoble_2022_10_15`
- :ref:`iran_grenoble_2022_10_08`
- :ref:`iran_grenoble_2022_10_01`
- :ref:`iran_grenoble_2022_09_24`
