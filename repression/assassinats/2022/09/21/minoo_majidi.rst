
.. index::
   pair: Minoo ; Majidi
   pair: Kermanshah; Minoo Majidi
   ! Minoo Majidi
   ! Cut it out
   ! Marco Melgrati


.. _minoo_majidi:

================================================================================
**Minoo Majidi tuée le 21 septembre 2022 dans la ville kurde de Kermanshah**
================================================================================


Si les gens comme moi n'y vont pas, alors qui ?
===================================================

.. figure:: images/minoo_majidi.png
   :align: center

   https://twitter.com/KurdistanAu/status/1574289097320972295?s=20&t=p-nKec4xmqeMzdfSUtngJQ


Si les gens comme moi n'y vont ps, alors qui?

J'irai pour qu'au moins notre jeunesse soit ps tuée,
Minoo Majidi, 1 femme kurde tuée à Kirmashan par le régime iranien
lors ds #IranProtests

#JinJiyanAzadi
#JinamahsaAmini #mahsaAmini
#EndIranRegime #LetUsTalk #WhiteWednesdays


La fille de #Minoo_Majidi
===========================

IMAGE FORTE. La fille de #Minoo_Majidi, une manifestante tuée le 21 septembre dans
la ville kurde de Kermanshah, s’est rasée les cheveux qu’elle tient dans
sa main gauche en se recueillant sur la tombe de sa mère.

Ce geste est un des symboles des femmes défiant le régime en #Iran


.. figure:: images/fille_minoo_majidi.png
   :align: center

   https://twitter.com/arminarefi/status/1575905955577204736?s=20&t=VZg307Dq5J0uPVk0hdLXuA


.. _cut_it_out:

Cut it out
------------

- https://twitter.com/Melgratillustr
- https://twitter.com/Melgratillustr/status/1575427238849581056?s=20&t=roQ9myRjz3EW5EnQ9CgmBQ

Illustration by Italian artist Marco Melgrati (https://twitter.com/Melgratillustr) .  Entitled "Cut it out"

.. figure:: images/couper_les_cheveux.png
   :align: center
   :width: 400

   https://twitter.com/ksadjadpour/status/1575565453355020302?s=20&t=OMYHXy4r4Gp6yOth3s-eOQ

Commentaire Masih Alinejad
------------------------------

- https://twitter.com/AlinejadMasih/status/1575844779736199168?s=20&t=roQ9myRjz3EW5EnQ9CgmBQ

Her mother Minoo Majidi  was killed by the Iranian regime while protesting
the tragic death of #MahsaAmini.
Now she stands at her mother's gravesite, unveiled herself and in her
left hand she holds the hair she cut from her head.

This is the true face of iranian freedom fighters.


- https://twitter.com/AlinejadMasih/status/1575844779736199168?s=20&t=roQ9myRjz3EW5EnQ9CgmBQ
