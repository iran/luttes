
.. index::
   pair: Sarina ; Esmaeilzadeh
   ! Sarina Esmaeilzadeh


.. _sarina_esmaeilzadeh:

======================================================================================================================================================================
**Sarina Esmaeilzadeh tuée le 23 septembre 2022 dans la ville kurde de Karaj  #SarinaEsmaeilzadeh #سارینا_اسماعیل_زاده#** #SarinaEsmailzadeh #NeverForgetNeverForgive
======================================================================================================================================================================

- https://en.wikipedia.org/wiki/Death_of_Sarina_Esmailzadeh

Listen to #SarinaEsmaeilzadeh, 16-year-old, full of life & hope
===================================================================


.. figure:: images/sarina.png
   :align: center

   https://twitter.com/FaridVahiid/status/1578127675058958336?s=20&t=nM5NTHlmR-IMYYpLVaq0lg


Listen to #SarinaEsmaeilzadeh, 16-year-old, full of life & hope.

She was killed on Sep 23 in Karaj as a result of baton blows, like many
other young protesters who lost their lives for speaking their minds.

These voices will not be silenced.

#MahsaAmini #مهسا_امینی #IranProtests


2023-09-23
=============

- :ref:`akrami_iran:sarina_2023_09_22`
