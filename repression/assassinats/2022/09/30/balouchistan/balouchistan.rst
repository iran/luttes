.. index::
   pair: Massacre ; Balouchistan
   pair; Vendredi ; Sanglant
   ! Vendredi sanglant
   ! Balouchistan
   ! Bloody Friday


.. _massacre_balouchistan_2022_09_30:
.. _bloody_friday_2022_09_30:

=======================================================================================================================
**2022-09-30 Vendredi sanglant ("Bloody Friday") Massacre de plus de 90 personnes au Balouchistan #Balochistan**
=======================================================================================================================

- :ref:`sistan_balutchistan`
- :ref:`zahedan`


Iran: At least 82 Baluchi protesters and bystanders killed in bloody crackdown
===================================================================================

https://www.amnesty.org/en/latest/news/2022/10/iran-at-least-82-baluchi-protesters-and-bystanders-killed-in-bloody-crackdown/

.. figure:: images/82_people_killed.png
   :align: center

   https://twitter.com/AmnestyIran/status/1578003075184447489?s=20&t=k-wIPvjBQOB3OHPOhB8buw

Iran's security forces have killed at least 82 Baluchi protesters and
bystanders in Zahedan, Sistan & Baluchistan province.

Those killed include 3 children. **Bloody Friday**, 30 September 2022, marked
the deadliest day on record since protests began 3 weeks ago


Tweet de Cahîda Dêrsi
==========================

- https://twitter.com/dersi4m/status/1576149515400118272?s=20&t=9tyXtvL9ApwTMYJ_SA5O8w

Yesterday at least 42 Baloch protesters were killed and 192 others were
wounded after the Iranian regime forces opened fire at unarmed protesters
in #Zahedan city, western of #Balochistan

https://rojnews.news/ku/yen-din/rexistina-mafe-mirovan-li-belucistane-42-kes-hatin-kustin/ #TwitterKurds



Tweet de Kurdistan Au Féminin
====================================


- https://twitter.com/KurdistanAu/status/1576271080942993408?s=20&t=Ilt20K6fn0XZOTf5h21t_Q

IRAN. Le parti kurde d'opposition, KOMALA condamne le massacre commis
par le régime iranien hier au #Balochistan

#EndIranRegime #LetUsTalk #JînaAmînî #TwitterKurds #مهسا_امینی #MahsaAmini
#opiran #IranRevolution #Rojhilat #Kurdistan #JinJiyanAzadi


