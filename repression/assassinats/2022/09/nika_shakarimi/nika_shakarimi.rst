.. index::
   pair: Nika ; Shakarami
   ! Nika Shakarami

.. _nika_shakarami:

===================================================================================
**Nika Shakarami 16 ans #NikaShakarami #نیکا_شاکرمی** #NeverForgetNeverForgive
===================================================================================

- https://fr.wikipedia.org/wiki/Nika_Shakarami
- https://en.wikipedia.org/wiki/Killing_of_Nika_Shakarami


Repose en paix, fille de l’#Iran. Nous ne t’oublierons pas. #NikaShakarmi
==============================================================================

.. figure:: images/repose_en_paix.png
   :align: center

   https://twitter.com/FaridVahiid/status/1578068084040122370?s=20&t=nM5NTHlmR-IMYYpLVaq0lg


Assassinée, la veille de son 17e anniversaire. Morte pour la liberté
=======================================================================

Un jour, ils devront rendre des comptes. #Iran #NikaShakrami


.. figure:: images/repose_en_paix.png
   :align: center

   https://twitter.com/FaridVahiid/status/1578071458680651779?s=20&t=nM5NTHlmR-IMYYpLVaq0lg


Les dernières vidéos de #Nika_Shakarami
==========================================

- https://twitter.com/GhadakpourN/status/1579372115920576512?s=20&t=reCQS6ustdoYxsEFeiFLtg


Ici elle brûle son foulard…Les dernières vidéos de #Nika_Shakarami .
Nika 17 ans, a disparu la même nuit.

Après 10 jours, le gouvernement a remis son corps à sa mère et a déclaré
que Nika s'était suicidé. #Mahsa_Amini



Tweet de Farid Vahiid
========================


.. figure:: images/nika_shakarami.png
   :align: center
   :width: 300

   https://twitter.com/FaridVahiid/status/1575871363432075264?s=20&t=OMYHXy4r4Gp6yOth3s-eOQ

Disparue depuis huit jours, le corps sans vie de **Nika Shakarami** a été
rendu à ses parents. Nouvelle victime de la répression en #Iran, Nika a
reçu de **nombreux coups à la tête** selon plusieurs sources.

Elle n’avait que 17 ans. Morte pour la liberté. #MahsaAmini


.. _nika_shakarami_2022_10_04:

2022-10-04 Les forces de sécurité rendent le corps de Nika Shakarami #NikaShakarami #نیکا_شاکرمی
======================================================================================================

#NikaShakarami⁩ avait 17 ans lorsqu’elle a disparu pendant la manifestation pour ⁦#MahsaAmani⁩
Une semaine après, les forces de sécurité ont rendu son corps. Son nez et son crâne étaient brisés.

⁧#مهسا_امینی⁩ ⁧#نیکا_شاکرمی⁩ ⁦#NousSommesLeursVoix


.. figure:: images/nika_shakarami_video.png
   :align: center

   https://twitter.com/AntonStruve/status/1577253361539317761?s=20&t=XDz0OE7uG7GOyBGz7Lh-pQ


2023-09-22
=============

- :ref:`akrami_iran:nika_shakarami_2023_09_22`

