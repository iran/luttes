.. index::
   ! Asra Panahi

===============================================================================================
2022-10-12 **Asra Panahi** battue à mort à Ardebil par les forces dites de """sécurité"""
===============================================================================================

.. figure:: images/asra_panahi.png
   :align: center

Annonces
==========

- https://twitter.com/AntonStruve/status/1581949528134938624?s=20&t=2GWcCwlQM2ISyMGvGMPb2A


#AsraPanahi est décédée mercredi après avoir été battue à mort à Ardabil
par les forces de sécurité pour avoir scandé le slogan “Femme, Vie, Liberté”.

Elle avait refusé avec d’autres lycéennes de rejoindre un rassemblement
pro-régime.

#MahsaAmini #مهسا_امینی #Iran

