
=========================================================
2022-11-14 Au moins 326 morts dans les manifestations
=========================================================


- https://fr.news.yahoo.com/conseil-droits-humains-l-onu-114729503.html


Au moins 326 morts dans les manifestations


ONU - Le Conseil a indiqué qu’une session exceptionnelle consacrée à
« la détérioration de la situation des droits humains » en Iran serait
organisée le 24 novembre 2022 à Genève

Une réunion tenue en urgence face à la situation. Le Conseil des droits
de l’homme des Nations unies a annoncé lundi qu’il tiendrait une session
le 24 novembre 2022 à Genève sur l’Iran, secoué par des semaines de
manifestations réprimées par les autorités.

Cette décision a été prise après une requête vendredi des ambassadeurs
d’Allemagne et d’Islande aux Nations unies. Le soutien de 16 des 47
Etats membres du Conseil des droits humains - plus d’un tiers - est
nécessaire pour convoquer une session spéciale en plus des trois sessions
régulières organisées chaque année.


Pour le moment, 44 pays, dont 17 membres du Conseil, ont approuvé la
demande germano-islandaise, selon l’instance. Cette initiative intervient
après huit semaines de manifestations en Iran, déclenchées par la mort
de Mahsa Amini, 22 ans, après son arrestation pour avoir enfreint les
règles vestimentaires très strictes, inspirées par la charia, qui
s’appliquent aux femmes dans le pays.

Au moins 326 personnes ont été tuées lors de la répression des manifestations,
selon l’ONG Iran Human Rights (IHR) basée à Oslo. Les protestations ont
évolué en un vaste mouvement contre la théocratie qui dirige l’Iran
depuis la chute du shah en 1979.

