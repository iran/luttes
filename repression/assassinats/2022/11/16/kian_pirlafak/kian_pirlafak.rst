.. index::
   pair: Kian ; Pirfalak
   ! Kian Pirfalak

.. _kian_pirfalak:

==========================================================
2022-11-16 Assassinat de **Kian Pirfalak** (10 ans)
==========================================================


.. figure:: images/kian_pirfalak_bis.jpeg
   :align: center


.. figure:: images/kian_pirfalak_image.jpeg
   :align: center

.. figure:: images/sang_kilian_pirfalak.jpeg
   :align: center


.. figure:: images/ici_grenoble_kilian.png
   :align: center

   :ref:`#KianPirfalak <kian_pirfalak>` |KianPirfalak|, https://video.blast-info.fr/w/bbSWztuvnDD441nU2B2GTo?start=57m1s


Liens
======

- :ref:`marjan_2023:maman_kilian`

Le 16 novembre 2022, #Kian_Pirfalak, 10 ans a été tué à #Izeh par balles
=========================================================================

Source: https://masthead.social/@NaderTeyf/109364479223159136

Le 16 novembre 2022, #Kian_Pirfalak, 10 ans a été tué à #Izeh par balles.

Sa mort a entraîné une grande émotion non seulement partout en #Iran
mais aussi au-delà des frontières.
Le régime des mollahs a prétendu que ce sont des terroristes qui l'ont tué.
Les obsèques de Kian ont eu lieu ce matin.

Zeynab Molaïrad, la mère de Kian, a confirmé que ce sont bien les forces
de répression du régime qui ont fusillé son fils.
Les gens présents ont scandé contre le régime. #Mahsa_Amini



https://nitter.grimneko.de/AlinejadMasih/status/1593260397628473348#m
======================================================================

Iran faced outrage after the security forces killed 10 Yr old boy,
#KianPirfalak in the city of :ref:`Izeh <izeh>`

Family kept his body in ice at their home, fearing that the government
would steal his body.

I call on the international community to stop the killing of more children
in Iran.


- https://nitter.manasiwibi.com/arminarefi/status/1617548293768359948#m

ELLE AVAIT ACCUSÉ la République islamique d’avoir tué son fils de 9 ans
#Kian_Pirfalak, abattu mercredi 16 novembre par des hommes armés à Izeh
(sud de l’#Iran). #Zeinab_Molaei_Rad vient de se voir signifier qu’elle
était suspendue de son métier d’enseignante jusqu’à nouvel ordre

.. figure:: images/mere_de_kilian.png
   :align: center
