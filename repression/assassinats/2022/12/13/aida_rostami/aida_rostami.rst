.. index::
   ! Aida Rostami


.. _aida_rostami:

=================================================================================
**La doctoresse Aida Rostami, enlevée, torturée et assassinée** #AidaRostami
=================================================================================

.. figure:: images/aida_rostami.png
   :align: center


.. figure:: images/aida_rostami_2.png
   :align: center

.. figure:: images/aida_rostami_3.png
   :align: center


- https://twitter.com/KurdistanAu/status/1603803309730471937#m

La docteuresse #AidaRostami, qui venait en aide aux manifestants blessés
dans le quartier Ekbatan de Téhéran, a été enterrée par ses proches à
Gorgan, dans le nord de l’#Iran.

IRAN Aida Rostami, 1 doctoresse de Téhéran qui aidait ls blessés ds protestations,
a été enlevée par les forces du régime.

**Elle est morte sous la torture: visage écrasé, 1 bras cassé & 1 œil crevé**

#مجیدرضا_رهنورد
#StopExecutionsInIran
#JinaAmini
#IranRevolution
#IranProtests


- https://mastodon.social/@Anonkurd/109524489338604740

She was a doctor!

Doctor Aida Rostami,who treated protesters,was murdered by the regime

Before being killed, she was tortured. Her left eye was removed and her left hand was broken.

This is the true face the monster regime killing children, innocent demonstrators and doctors.
#IranRevolution
#Iran


The Islamic Republic should be treated like #ISIS
======================================================

.. https://www.deepl.com/translator

- https://iranwire.com/en/politics/111407-tehran-doctor-tortured-and-killed-for-treating-wounded-protesters/


.. figure:: images/aida_rostami_4.png
   :align: center

En anglais
-------------

The Islamic Republic should be treated like #ISIS: Dr. Aida Rostami, 36,
was treating protesters in Ekbatan and other neighborhoods of Tehran until
she disappeared earlier this month. Her tortured body was handed over
to her family in #Iran a day later.

Iranians wounded by the security forces during the ongoing nationwide
protests avoid treatment in the country’s increasingly unsafe hospitals
for fear of being detained, tortured, prosecuted or killed.

The medics ready to brave the risks to help the injured are treating
protesters in their offices, the demonstrators’ homes or elsewhere.

Aida Rostami, 36, was one of them. She was treating protesters in Ekbatan
and other western neighborhoods of Tehran until she disappeared earlier
this week. Her tortured body was handed over to her family a day later.

**After taking care of several wounded protesters in Ekbatan on December 12,
Rostami realized she was running out of some medical items such as sterile
gas**, a source close to the family told IranWire.
The doctor left a protester’s house to get the things she needed **and
was never seen alive again**.

A local police station called the family the next morning, announcing
that Rostami had died in a car crash overnight. They were told to pick
up her body at a morgue.

**The IranWire source said the car accident was "definitely" not the cause
of her death**.

"The medical examiner told her family that they were ordered not to
reveal the true cause of Aida's death. They said that she did not die
in a car accident, they killed her."

The security officers allowed the relatives to see the body, but only
after they insisted.

"It is not possible that when you are driving and you get an accident,
both of your hands would break, your lower torso gets bruised, and
your eye completely comes out."

The family unsuccessfully requested the police to show them Rostami’s
vehicle and the spot where the alleged car accident occurred.

Rostami was buried in :ref:`Gorgan <gorgan>`, where the family is originally from.

And Ekbatan lost another doctor.


En français
--------------

Les Iraniens blessés par les forces de sécurité lors des manifestations
nationales en cours évitent de se faire soigner dans les hôpitaux du pays,
de moins en moins sûrs, de peur d'être détenus, torturés, poursuivis ou tués.

Les médecins prêts à braver les risques pour aider les blessés soignent
les manifestants dans leurs bureaux, à leur domicile ou ailleurs.

Les médecins prêts à braver les risques pour aider les blessés soignent
les manifestants dans leurs bureaux, au domicile des manifestants ou ailleurs.

Aida Rostami, 36 ans, était l'une d'entre eux. Elle soignait les manifestants
à Ekbatan et d'autres quartiers ouest de Téhéran jusqu'à ce qu'elle
disparaisse en début de semaine. Son corps torturé a été remis à sa famille
un jour plus tard.

Après avoir pris soin de plusieurs manifestants blessés à Ekbatan le 12 décembre 2022,
Rostami s'est rendu compte qu'elle était à court de certains articles médicaux
comme le gaz stérile, a déclaré une source proche de la famille à IranWire.
Le docteur a quitté la maison d'un manifestant pour obtenir les choses
dont elle avait besoin et n'a plus jamais été revue vivante.

Un poste de police local a appelé la famille le lendemain matin, annonçant
que Rostami était morte dans un accident de voiture pendant la nuit.
On leur a dit de venir chercher son corps à la morgue.

La source d'IranWire a déclaré que l'accident de voiture n'était
"certainement" pas la cause de sa mort.

"Le médecin légiste a dit à sa famille qu'on leur avait ordonné de ne pas
révéler la véritable cause de la mort d'Aïda. Ils ont dit qu'elle n'était
pas morte dans un accident de voiture, ils l'ont tuée."

Les agents de sécurité ont autorisé les proches à voir le corps, mais seulement
après qu'ils aient insisté.

"Il n'est pas possible que lorsque vous conduisez et que vous avez un accident,
que tes deux mains se cassent, que tu aies des contusions au bas du torse
et que votre œil s'arrache complètement."

La famille a demandé sans succès à la police de leur montrer le véhicule
de Rostami et l'endroit où l'accident de voiture présumé s'est produit.

Rostami a été enterré à :ref:`Gorgan <gorgan>`, d'où la famille est originaire.

Et Ekbatan a perdu un autre médecin.


Les habitants de #Gorgan lui ont rendu un hommage mardi 20 décembre 2022
==========================================================================

- https://masthead.social/@NaderTeyf/109546384656316944

#Aïda_Rostami était une médecin de 36 ans qui soignait clandestinement
les blessés de manifestations. Il y a 7 jours, le régime des ayatollahs
en #Iran a annoncé sa mort accidentel alors que son corps montre plusieurs
marques de torture. Les habitants de #Gorgan lui ont rendu un hommage
aujourd'hui (mardi 20 décembre 2022) en scandant des slogans contre le régime

- https://cdn.masto.host/mastheadsocial/media_attachments/files/109/546/353/227/684/599/original/ca2b4a98abe73360.mp4


The young women are abducted, raped and their bodies found somewhere.
======================================================================

- https://eldritch.cafe/@sanaz/109553642575664663

The young doctor #Ayda_Rostami was kidnapped on 2022-12-12 after her shift
at the hospital On 13.12 the family was notified by the police that
they could collect her body.

It is a systematic attack of Iranian regime against the youth and against
the women. While the young men are sentenced and executed, the young women
are abducted, raped and their bodies found somewhere.

The regime is aware that the execution of a young woman has a deep political
dimension and would cause a great outcry, so the murders are carried out secretly.

