
.. _condamnations_a_mort_2022_11_13:

=======================================================================================
2022-11-13 Premières condamnations à mort, pétion initiée par Raheleh Behzadi Suède
=======================================================================================

- https://www.change.org/p/stop-execution-of-iranian-protesters



The Iranian court and parliament have issued and confirmed the execution
of seven people in connection with recent protests against the dictatorship
and corruption of the Iranian government.

The people are as follows 

- Mohammad Ghobadloo
- :ref:`Saman Seydi <saman_seydi>`
- Mohsen Reza zade Gharaghloo
- Saeed Shirazi
- Mohammad Broghni
- Abolfazl Mehri Hossein Hajiloo
- Manouchehr Mehmannavaz 

These sentences were handed down on vaguely defined national security
charges, and the defendants not only lacked access to lawyers but were
tortured or mistreated by authorities in detention.

People involved in protests are sentenced to death in unfair trials, not
the security forces who use force and kill hundreds of protesters with bullets.

We call on all the people of the world, human rights organizations,
international courts, students, universities, celebrities, and politicians
to take immediate action to abolish the recent death.

We call on governments, politicians, and international organizations to
guarantee a fair trial for those charged with recognized crimes.

Saman Seydi
===========

.. toctree::
   :maxdepth: 3

   saman_seydi/saman_seydi
