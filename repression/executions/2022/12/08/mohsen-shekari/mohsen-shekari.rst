.. index::
   pair: Assassinat ; Mohsen Shekari
   pair: Exécution ; Mohsen Shekari
   ! Mohsen Shekari

.. _assassinat_mohsen_shekari_2022_10_08:
.. _mohsen_shekari_2022_10_08:

===============================================================================================================================
Jeudi 8 décembre 2022 **L'exécution de #MohsenShekari ce matin a profondément bouleversé les gens en #Iran** |MohsenShekari|
===============================================================================================================================

.. figure:: images/mohsen_shekari.jpg
   :align: center


.. figure:: images/mohsen_shekari_cordes.jpg
   :align: center


L'exécution de #Mohsen_Shekari ce matin a profondément bouleversé les gens en #Iran.
========================================================================================

- https://masthead.social/@NaderTeyf/109479314150427497
- https://cdn.masto.host/mastheadsocial/media_attachments/files/109/479/312/436/130/349/original/ec96438b7c0453f1.mp4



Il n'avait que participé à une barricade et blessé légèrement un bassidji
selon le pouvoir judiciaire islamique lui-même.

Il a été arrêtée sur l'avenue Sattar Khan de #Téhéran.

Une manifestation s'y forme ce soir.

#Mahsa_Amini


Le régime islamique a exécuté ce matin à #Téhéran un jeune de 23 ans. #Mohsen_Shekari
======================================================================================

- https://masthead.social/@NaderTeyf/109477267939837020

Le régime islamique a exécuté ce matin à #Téhéran un jeune de 23 ans. #Mohsen_Shekari aurait participé à une barricade sur l'avenue Sattar Khan et blessé un bassidji le 25 septembre, selon le pouvoir judiciaire. La première audience a eu lieu le 1er novembre. Mohsen n'a pas eu un avocat de son choix.
La justice des mollahs est connue pour des procès inéquitables et expéditifs. Elle a exécuté quelque 5000 prisonniers politiques en été 1988 avec des procès qui n'ont duré que 5 minutes.

