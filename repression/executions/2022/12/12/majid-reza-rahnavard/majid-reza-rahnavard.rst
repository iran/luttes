.. index::
   pair: Exécution ; Majid Reza Rahnavard
   ! Majid Reza Rahnavard


.. _majid_reza_rahnavard_2022_12_12:

==================================================================
Lundi 12 décembre 2022 : exécution de **Majid Reza Rahnavard**
==================================================================

.. figure:: images/majid_reza_rahnavard.jpg
   :align: center


- https://masthead.social/@NaderTeyf/109502028610953778

Après la pendaison du deuxième protestataire, #Majidreza_Rahnavard, des
manifestants du quartier Haft Hoz de #Téhéran ont scandé:"C'est le dernier
message, si tu exécute, ça sera l'insurrection" et "Ils ont pris Majidreza,
ils ont rendu son cadavre."

#Mahsa_Amini


- https://nitter.manasiwibi.com/arminarefi/status/1602254142185082881#m

EN EXÉCUTANT #MajidRezaRahnavard, 2e manifestant pendu en 4 jours, qui
plus est en public cette fois, le régime iranien entend fair peur à ses
opposants. Pas sûr que cela les dissuade à battre à nouveau le pavé.



- https://nitter.manasiwibi.com/arminarefi/status/1603506216889733120#m


QUI EST CAPABLE de faire ça ? Qq minutes avant son exécution, le
manifestant #Majidreza_Rahnavard déclare lundi 12 décembre à la télévision
iranienne d’État qu’il ne veut ni Coran, ni prière après son exécution
mais de la « joie ».

Ceux-là devront un jour rendre des comptes #Iran

.. figure:: images/yeux_bandes.png
   :align: center
