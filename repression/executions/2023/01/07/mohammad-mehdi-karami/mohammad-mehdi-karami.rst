.. index::
   pair: Mohammad Mehdi ; Karami
   pair: Exécution ; Mohammad Mehdi Karami
   ! Mohammad Mehdi Karami
   ! Mehdi Karami


.. _mehdi_karami:
.. _mohammad_mehdi_karami_2023_01_07:

==============================================================================================================
Samedi 7 janvier 2023 : exécution de **Mohammad Mehdi Karami (Mihemed Mehdî Keremî) #MohammadMehdiKarami**
==============================================================================================================

- https://iran-hrm.com/2022/12/16/mohammad-mehdi-karami-karate-champion-risks-his-life/

Autre assassiné le même jour #SeyedMohammadHoseini |MohammadHosseini|
==========================================================================

- :ref:`mohammad_hosseini`


Mohammad Mehdi Karami |MehdiKarami|
=========================================

.. figure:: images/mohammad_mehdi_karami.png
   :align: center


.. figure:: images/mohammad_mehdi_karami_medailles.png
   :align: center

Il n’avait que 22 ans. Champion de Karaté, il avait été condamné à mort
le 4 décembre 2022 pour avoir tué un milicien bassidji le 3 novembre à l’issue
d’un simulacre de procès.

Il a été pendu ce samedi 7 janvier 2023 à l’aube en #Iran.


.. figure:: images/les_parents.png
   :align: center

   Les parents. Mashallah Karami (son père)

- https://nitter.manasiwibi.com/FaridVahiid/status/1611701471980015616#m

« Papa, ils ont prononcé les peines, je suis condamné à mort… mais ne le
dis pas à maman. »
Mehdi Karami a été exécuté ce matin en #Iran. #MahsaAmini

- https://twitter.com/KurdistanAu/status/1611729358380482561#m


Mihemed Mehdî Keremî, 1 Kurde de :ref:`Bîcar <bidjar>`, 22 ans, exécuté ce matin

Lorsqu'il a été condamné à mort, il a dit à son père; "Ma peine est la
peine de mort, mais s'il te plaît, ne le dis pas à maman"

#IRAN #ROJHILAT #KURDISTAN #JeSuisKurde #IranRevoIution #StopExecutionsInIran


- https://nitter.manasiwibi.com/Clem_Autain/status/1611654537911177217#m

Je viens d'apprendre que mon filleul #MohammadMehdiKarami, 22 ans, à été
exécuté ce matin par le gouvernement iranien. Colère, tristesse immense
et pensées affligées à ses proches.

@EmmanuelMacron votre silence vis-à-vis du régime est inadmissible.

#IranRevolution2023


- https://nitter.poast.org/IranHrm/status/1611771308206968832#m

#MohammadMehdiKarami never had lawyer during trials, says #HumanRights
attorney @Mhaghasi1
The Supreme Court & Chief Justice agreed my presence but head of court
claimed, only a lawyer from his department has right of entry.
Iran's Supreme Court should have rejected the vote.

Article de iran-hrm
-----------------------

- https://iran-hrm.com/2022/12/16/mohammad-mehdi-karami-karate-champion-risks-his-life/

Mehdi was a Kurdish citizen from :ref:`Bijar <bidjar>`, who came to live
in :ref:`Karaj <karaj>`, close to Tehran with family.
Deprived of even #Iran's standards, right to last visitation.
Twitter posts-related to Iranian state systems, had indicated such a crime at night Jan.6, 2023.


Mohammad Mehdi Karami, 22, is a Karate champion and has several
championship medals.

Mohammad Mehdi Karami has been identified by Iran’s Judiciary as the front-row
defendant in the case of Ruhollah Ajmian, a Basij Force member who was
killed during the protests.

He was found dead on November 3, 2022, in Karaj, during a ceremony
commemorating the 40th day after state security forces killed Hadis Najafi.

Mr. Karami was arrested by security forces on November 5, 2022, and beaten
so hard he fell unconscious to the ground. Government forces thought he
was dead and threw his “body” outside the Nazarabad Court.
But as they were leaving, they realized he was still alive. After taking
him back inside, they broke his rib under torture and injured his hands
and head.

Mr. Karami received no medical care for the sufferings.
Mohammad Mehdi Karami informed of the government forces threatening to
sexually harass him during his detention time. He said, “they threaten
to rape me every day.”
Mohammad Mehdi Karami was tried on November 30, 2022, and sitting on
his left and right side were two individuals. He was made to say what
was given to him on a paper by the individuals. Mohammad Mehdi Karami
was identified in court as the front-row defendant and sentenced to death
on charges of “Corruption on Earth” by committing crimes against national
security.
He was also charged with attacking a Basij member, assembly and collusion
against national security, and posting action calls to strike against
the state on cyberspace.

Mashallah Karami, Mohammad Mehdi Karami’s father, told a news agency in
Tehran that his son’s “public defender” had not answered any of their
calls in the past week. The lawyer had refused to even give his office
address. Mashallah Karami’s said that the Judiciary informed him about
being able to appoint a lawyer.
But after introducing Mohammad Hossein Aghasi, the Judicial authorities
did not grant Mr. Aghasi access to the case.

Mashallah Karami, a vendor, said that the person in charge of his son’s
case had told him, would it satisfy you if I were to question the judgment
of God and the Prophet? This was his respond to Mashallah Karami, after
he requested for a sentence other than the death penalty to be given to
his son.Mashallah Karami says his son denied committing any crime and
told him, believe me dad, I swear to God, I did not do any of it.

Mehdi Karami swore to his father’s calloused hands but added that in
order to be sentenced to death, there is no need for evidence or document.
They only need you to confess. Mehdi Karami asks, should they not be
investigating the details?

Mashallah Karami says his son informed him in a phone call that he had
received the death sentence from Judicial authorities in prison on
Wednesday, December 7, 2022. In an interview with a daily in Tehran,
Mashallah Karami said, he called us and said, dad, they have given us
the sentences. I have been sentenced to death. My son cried and said,
don’t tell Mom anything. Mashallah Karami is a vendor in Nazarabad,
Alborz Province. He sells paper towels and has two children.

One is Mohammad Mehdi and the other child disabled and remains at home.
Mashallah Karami says his wife is very attached to Mehdi. He says my son
is an athlete and always tried to earn honor.
If something happens to Mehdi, our lives will end too. I have raised him
with hard labor.

According to Mashallah Karami, his son has not accepted his charges and
has been sentenced to death upon forced “confession.”

Confessions that are made under physical and psychological pressure.
Human Rights Monitor has warned repeatedly during recent weeks that
detained protesters in Iran are under pressure and torture to make forced
confessions against themselves.

These detainees are denied access to a lawyer of their choice.
Given these circumstances, the proceedings are “unfair,” and the sentences
handed down to them are “illegal.”

On Tuesday, December 13, 2022, several German lawmakers assumed political
support for young Iranian detainees at risk of execution.

Helge Limburg, Member of the German Bundestag announced that he has taken
over political sponsorship of Mohammad Mehdi Karami. In a tweet, he
describes the possible execution of Mohammad Mehdi Karami by the regime
in Iran to be a Judicial killing.

