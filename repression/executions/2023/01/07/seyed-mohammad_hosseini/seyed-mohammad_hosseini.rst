.. index::
   pair: Seyed Mohammad ; Hosseini
   pair: Mohammad ; Hosseini
   pair: Exécution ; Seyed Mohammad Hosseini
   ! Mohammad Hosseini
   ! Seyed Mohammad Hosseini


.. _mohammad_hosseini:
.. _mohammad_hosseini_2023_01_07:
.. _seyed_mohammad_hosseini_2023_01_07:

===================================================================
Samedi 7 janvier 2023 : exécution de **Seyed Mohammad Hosseini**
===================================================================

39 ans ouvrier dans une usine de taille de pierre.


Autre assassiné le même jour #MohammadMehdiKarami  |MehdiKarami|
====================================================================

- :ref:`mehdi_karami`



Seyed Mohammad Hosseini |MohammadHosseini|
=================================================

.. figure:: images/seyed_mohammad_hosseini.png
   :align: center


.. figure:: images/seyed_mohammad_hosseini_2.png
   :align: center


Iran has hanged two protesters #MohammadHosseini and #MohammadMehdiKarami,
both charged with "corruption on earth" & accused of killing a member
of the paramilitary Basij force.


The authorities must not be allowed to end innocent lives.
#SeyedMohammadHoseini
#MohammadHosseini



- https://nitter.manasiwibi.com/Benjam1Lucas/status/1611725910436368385#m

Mon filleul #MohammadHosseini a été cruellement exécuté ce matin.

Nous ne pouvons plus fermer les yeux sur la situation des iraniens qui
se battent pour la démocratie. @EmmanuelMacron, il faut continuer (? NDLR)  la
pression internationale pour éviter d’autres morts.

#StopExecutionInIran


- https://nitter.manasiwibi.com/arminarefi/status/1611756537847971842#m

IL S’APPELAIT #SeyedMohammadHosseini. Extorqués sous la torture, ses aveux
lui ont valu d’être condamné à mort le 4 décembre 2022 pour avoir tué
un milicien bassidji en marge d’une manifestation le 3 novembre à Karaj
(ouest de Téhéran). Il a été pendu ce samedi 7 janvier à l’aube


- https://nitter.manasiwibi.com/SafaiDarya/status/1612056823720206336#m

Mohamad's friends wanted to get his body for burial, but they don’t give
the body. They ask for first-class family while they know he was an orphan.

They want to bury him secretly in an unknown place.

We are all his relatives.We can’t allow this.

🖤
#Seyed_Mohammad_Hosseini



- https://nitter.poast.org/IranHrm/status/1612117312768999424#m

.. figure:: images/seyed_mohammad_hosseini_3.png
   :align: center


#SeyedMohammadHoseini had lost both his parents. The day of his arrest in
the surrounding of Beheshte Sakineh Cemetery in Karaj, he visiting their
graves as he always did on Thursdays.

No one has been able to reclaim his executed body for burial.
#StopExecutionsInIran
