.. index::
   pair: Mohayeddin  ; Ebrahimi
   pair: Exécution ; Mohayeddin Ebrahimi
   ! Mohayeddin Ebrahimi

.. _mohayeddin_ebrahimi_2023_03_17:

===========================================================================================================
Vendredi 17 mars 2023 : **exécution de MohayeddinEbrahimi** #MohayeddinEbrahimi |MohayeddinEbrahimi|
===========================================================================================================

- https://iranhr.net/en/articles/5782/


ran Human Rights (IHRNGO); March 16, 2023: Kurdish political prisoner,
Mohiyedin Ebrahimi was executed in Urmia Central Prison.

Iran Human Rights strongly condemns Mohiyedin Ebrahimi’s execution and
current execution wave.

Director, Mahmood Amiry-Moghaddam said: “Mohiyedin Ebrahimi was sentenced
to death without due process and fair trial rights by the Revolutionary
Court and his execution is a violation of national and international law.

Like more than 140 people executed in 2023 so far, Mohiyedin was one of
the low-cost victims of the Islamic Republic’s execution machine, the
sole purpose of which is to terrorise society and prevent protests.

Ali Khamenei and the judiciary under his command must be held accountable
for these crimes.”

“The international community must react to the wave of arbitrary executions
in Iran.
Their silence is a green light for the continuation of the executions,” he added.

43-year-old Mohiyedin Ebrahimi was arrested by IRGC forces on 3 November
2017.
He was shot in the leg at the time of arrest. He was sentenced to death
on charges of baghy (armed rebellion) through membership of the Kurdistan
Democratic Party of Iran by Branch Two of the Urmia Revolutionary Court
in October 2018 which was overturned by Branch 16 of the Supreme Court.

However, he was re-sentenced to death by the same Revolutionary Court branch.

His three lawyers, Messrs Muzayen, Alizadeh and Tataei stress that Mohiyedin
Ebrahimi was working as a kolbar due to poverty and unemployment and
that he should not be charged with carrying weapons or anti-government
armed operations.



- https://twitter.com/LettresTeheran/status/1636646649140215810#m

URGENT - #MohayeddinEbrahimi prisonnier politique kurde iranien de 43 ans,
père de deux enfants handicapés, a été pendu en secret, ce matin à l’aube,
à la prison d’Uormia.


.. figure:: images/mohayeddin_ebrahimi.png
   :align: center



- http://nitter.smnz.de/Hengaw_English/status/1636617033667207171#m

Today at dawn, #Mahyaddin_Ibrahimi, a political prisoner from Oshnoviyeh, was executed.
Hengaw will publish an additional report.

Friday, March 17, 2023

#JinaAmini #Kurdistan #IranianRevolution
