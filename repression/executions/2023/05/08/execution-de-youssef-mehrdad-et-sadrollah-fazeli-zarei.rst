
.. index::
   pair: Exécutions ; Lundi 8 mai 2023
   pair: Libre-penseur ; Youssef Mehrdad
   pair: Libre-penseur ; Sadrollah Fazeli Zarei


.. _yousef_sadrollah_2023_05_08:
.. _yousef_2023_05_08:
.. _sadrollah_2023_05_08:

=================================================================================================================================================
Lundi 8 mai 2023 **Pendaisons de deux héros libres-penseurs Youssef Mehrdad et Sadrollah Fazeli Zarei #YoussefMehrdad #SadrollahFazeliZarei**
=================================================================================================================================================

- https://www.msn.com/fr-ch/actualite/other/deux-hommes-ex%C3%A9cut%C3%A9s-en-iran-pour-blasph%C3%A8me/ar-AA1aUmEg#image=1
- http://cnt-ait.info/2023/05/09/republique-execution/

.. figure:: images/yousef_mehrad.png
   :align: center

   Youssef Mehrdad


.. figure:: images/fazeli_zare.png
   :align: center

   Sadrollah Fazeli Zarei


Youssef Mehrdad et Sadrollah Fazeli Zarei ont été accusés de gérer des
comptes de médias sociaux «dédiés à l'athéisme et à la profanation des
lieux saints», a rapporté l'agence de presse judiciaire iranienne Mizan
citée par BBC News.

L'avocat de M. Mehdad avait insisté sur le fait qu'il était innocent et
que sa condamnation était injuste.


Les deux hommes avaient été arrêtés en 2020 et accusés de gérer une chaîne
Telegram intitulée «Critique de la superstition et de la religion», selon
l'agence de presse iranienne Human Rights Activists News Agency (HRANA).

Ils ont été détenus à l'isolement pendant les deux premiers mois et n'ont
pas eu accès à un avocat.

En 2021, le tribunal pénal d'Arak a reconnu MM. Mehrad et Fazeli-Zare
coupables de blasphème et les a condamnés à mort, a ajouté HRANA.

Ils ont également été condamnés à six ans de prison pour avoir «dirigé
des groupes agissant contre la sécurité nationale».

Toujours selon la BBC, la Cour suprême a rejeté leurs appels contre les
verdicts et a confirmé leurs condamnations à mort plus tard dans l'année,
a déclaré Mizan, ajoutant que les deux hommes avaient «clairement avoué
leurs crimes».

199 exécutions en 2023

Les groupes de défense des droits de l'homme affirment que les tribunaux
iraniens sont régulièrement loin de garantir des procès équitables et
qu'ils utilisent comme preuves de faux aveux obtenus sous la torture.

«L'exécution de Yousef et de Sadrollah pour "insulte au Prophète" n'est
pas seulement un acte cruel commis par un régime médiéval, c'est aussi
une grave insulte à la liberté d'expression», a déclaré Mahmood Amiry-Moghaddam,
directeur de l'association Iran Human Rights, basée en Norvège.

Voici la nouvelle «stratégie» de l'Iran pour forcer les femmes à porter le voile

L'application de la peine de mort en Iran est critiquée depuis des années
par les défenseurs des droits de l'homme.

Les exécutions pour blasphème sont toutefois extrêmement rares dans ce
pays dominé par des religieux chiites. Comme l'organisation de défense
des droits de l'homme Hengaw l'a annoncé il y a quelques jours, 199
prisonniers ont déjà été exécutés en Iran cette année. (ats/cru)


.. figure:: images/la_pendaison.png
   :align: center
   :width: 300

   http://cnt-ait.info/2023/05/09/republique-execution/


.. figure:: images/affiche_cnt_ait.png
   :align: center
   :width: 500

   http://cnt-ait.info/2023/05/09/republique-execution/


.. _yousef_sadrollah_2023_05_17:

2023-05-17 Mort à la République d’exécution d’Iran ! (Rassemblement Paris et Toulouse, samedi 20 Mai 2023), à la mémoire de deux héros libres-penseurs, Yousef Mehrad et Sadrollah Fazeli Zare
================================================================================================================================================================================================

- http://cnt-ait.info/2023/05/17/mort-republique-execution/
- http://cnt-ait.info/category/inter/iran/farsi/

Rassemblement, Samedi 20 mai, 14h30
Statue du Chevalier de la Barre, Square Nadar, Montmartre

(1 Rue Saint-Éleuthère, 75018 Paris )

A Toulouse, rassemblement Samedi 20 mai, 16h00, place du Salin

Le 16 septembre 2022, une révolution a débuté en Iran.

Les femmes, la jeunesse, la population se sont levées pour exiger la
liberté de vivre, tout simplement, sans obligation morale archaïque, en
parfaite égalité entre les hommes et les femmes.

La réponse de la République islamique à cette explosion de liberté est
brutale et féroce.

En plus de la répression sauvage contre les manifestations, la peine de mort
tourne à plein régime. Au moins 290 personnes ont été exécutées depuis
le 1er janvier 2023, soit en moyenne plus de 10 par semaine.

Sur le podium de la barbarie, la République Islamique d’Iran est à la
seconde place, après la Chine Communiste.

En Iran on peut être condamné à mort pour avoir participé à des manifestations,
comme celles qui se déroulent depuis la mort en détention de Mahsa Amini,
arrêtée pour port « inapproprié » du voile islamique.

Quel courage que démontrent ces femmes qui osent défier le pouvoir autocratique
qui les étouffe !

On peut aussi y être condamné à mort pour avoir osé critiquer la religion.

**Le 8 mai 2023, Yousef Mehrad et Sadrollah Fazeli Zare ont été pendus pour ce motif.**


.. figure:: images/yousef_mehrad.png
   :align: center

   Youssef Mehrdad


.. figure:: images/fazeli_zare.png
   :align: center

   Sadrollah Fazeli Zarei


Ils avaient été arrêtés et emprisonnés en 2020 pour avoir créé un groupe
Télégramme intitulé « Critique de la superstition et de la religion »,
sur lequel entre autre apparaissait une vidéo d’un Coran brûlé ainsi
que des propos remettant en cause la religion.

Le 10 mai 2023, deux jours après cette exécution, **dans un immense bras d’honneur
à l’Humanité, la République islamique d’Iran a été désignée par l’ONU pour
présider pour 2023 le Forum social du Conseil des droits de l’homme de l’ONU,
qui – comble de l’ironie – est consacré cette année au thème « de la contribution
de la technologie et de l’innovation à la promotion des droits de l’homme ».**
(Voir :ref:`onu_2023_05_10_local`)

**Tout cela avec le silence assourdissant, sinon l’aval, des «gouvernements
« démocratiques »  du monde entier.**

Pourtant la lutte pour la liberté de pensée est indissociable de la lutte
pour la justice sociale. Pierre-Joseph Proudhon distinguait trois formes
d’aliénation : la Religion (aliénation de la raison) ; l’État (aliénation de la volonté)
et la propriété (aliénation du corps).

Les trois formes d’auto-aliénation sont liées et connectées les unes aux
autres comme trois anneaux imbriqués, et la libération humaine n’est pas
possible qu’en se libérant des trois.

L’exécution ignoble de ces deux héros de la libre pensée nous rappelle
celle du `Chevalier de la Barre <https://fr.wikipedia.org/wiki/Fran%C3%A7ois-Jean_Lefebvre_de_La_Barre>`_, un jeune homme qui en 1766 avait lui
aussi été accusé de profanation d’objets religieux et de propos
blasphématoires.

Après avoir été torturé, il fut amené à l’échafaud, la corde au cou,
portant dans le dos une pancarte sur laquelle était écrit « impie, blasphémateur
et sacrilège exécrable ». L’indignation devant ce crime odieux alluma
le feu de la Raison et des Lumières qui devait déboucher quelques années
plus tard sur la Révolution française.

Espérons que la mémoire de ces deux héros, qui osèrent braver la dictature
des Mollahs pour exprimer leur liberté de pensée, allumera les brasiers
qui demain balayeront le régime théocratique iranien.

Pour honorer les combattantes et combattants de la Liberté, d’hier et
d’aujourd’hui, nous vous donnons rendez-vous le Samedi 20 mai 2023 à 14h30
au Square Nadar, sur la butte Montmartre, haut lieu de la Commune de Paris,
autre grande célébration de la Liberté et de l’Egalité.

Mort à la république de l’exécution مرگ بر جمهوری اعدام

CNT-AIT (anarchosyndicalisme !) contact@cnt-ait.info http://cnt-ait.info

=======

A Toulouse, le rassemblement est organisé à 16h, place du Salin (palais de justice)
devant la plaque en Mémoire de `Vanini <https://fr.wikipedia.org/wiki/Giulio_Cesare_Vanini>`_, exécuté à Toulouse le 9 février 1619
par le Parlement des Capitouls de Toulouse, pour blasphème, impiété,
athéisme et mœurs contre natures.

Il fut condamné à avoir la langue arrachée, à être étranglé puis brûlé.


