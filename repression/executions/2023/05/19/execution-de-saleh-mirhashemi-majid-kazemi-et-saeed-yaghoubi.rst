
.. index::
   pair: Exécutions ; Vendredi 19 mai 2023


.. _pendaison_2023_05_19:
.. _pendaisons_2023_05_19:

========================================================================================================================================================
Vendredi 19 mai 2023 **Pendaisons de Saleh Mirhashemi, Majid Kazemi et Saeed Yaghoubi #SalehMirhashemi, #MajidKazemi, and #SaeedYaghoubi**
========================================================================================================================================================

#Iran #IranProtests #DoNotLetThemKillUs #SalehMirhashemi, #MajidKazemi, and #SaeedYaghoubi


- https://nitter.bird.froth.zone/ICHRI/status/1659540638658879489#m

Executed today after a sham trial in Iran
Saeed Yaghoubi, 37, Saleh Mirhashemi, 36, and Majid Kazemi, 30
"They flogged the soles of my feet."
"They took me to a place where there were no cameras and told me to drop my pants."
"I begged them not to, said I would accept all the charges."


- https://nitter.poast.org/chowmak/status/1659483464771444736#m


A Isphahan, les manifestants condamnés à mort #SaeedYaghoobi #SalehMirhashemi
et #Majid_Kazemi ont été pendus à l'aube.
Hier, les autorités iraniennes avaient publié un communiqué disant que
leur exécution n'était pas prévue, pour prévenir les rassemblements devant la prison.



2023-05-19 Iran Human Rights (IHRNGO); May 19, 2023: Protesters Saleh Mirhashemi,
Majid Kazemi and Saeed Yaghoubi were executed this morning, according to
the Judiciary’s Mizan news agency.

Iran Human Rights previously warned of their imminent executions and urged
the international community to do all in its power to save them.

Director, Mahmood Amiry-Moghaddam said: “The execution of the three protesters
are extrajudicial killings that **Iranian authorities, particularly Supreme
Leader Ali Khamenei, must be held accountable for**.

Unless the Iranian authorities are met with serious consequences by the
international community, hundreds of protester lives will be taken by
their killing machine.”

“These executions are meant to prolong the Islamic Republic's rule and
only a high political cost can stop more protester executions,” he added.

According to Mizan, the Judiciary’s news agency, protesters Saleh Mirhashemi,
Majid Kazemi and Saeed Yaghoubi were executed on 19 May.

The report does not specify the exact location of the executions but they
were held in Isfahan Central Prison.

They were sentenced to death for charges of moharebeh (enmity against god)
by the Isfahan Revolutionary Court on 9 January in relation to protests
in Isfahan on 25 November 2022.

Saleh, Majid and Saeed were tortured to force self-incriminating confessions
which were aired, to testify against each other and to take part in a
reconstruction of the alleged crime scene where they were forced to repeat
the scenario as was dictated to them.

In Majid Kazemi’s case, his two brothers were also arrested and used to
pressure him into confessing to the false charges.

Their death penalty sentences were upheld by the Supreme Court on 9 May
and their videos of their torture-tainted confessions and testimonies
began airing on 11 May.

On 13 May, their families and lawyers were informed that their sentences
would be carried out within days. The next day, the protesters’ families
were joined by protesters who stayed outside Isfahan Central Prison all night.

On 17 May, the families of the protesters called on the public to join
them outside the prison that night after they were granted last family
visits.

The Judiciary issued a statement, denying that their executions were due
to take place. Yet, they were executed in the early hours of 19 May.

For questions and comments, please contact:
Mahmood Amiry-Moghaddam
Director of IHRNGO
Tel: +47 91742177
mail@iranhr.net




.. figure:: images/saleh_saeed_majid.png
   :align: center


.. toctree::
   :maxdepth: 3

   saleh_mirhashemi/saleh_mirhashemi
   majid_kazemi/majid_kazemi
   saeed_yaghoubi/saeed_yaghoubi
