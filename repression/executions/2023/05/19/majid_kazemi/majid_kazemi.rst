
.. index::
   pair: Assassiné ; Majid Kazemi (2023-05-19)


.. _majid_kazemi_2023_05_19:

========================================================================
Vendredi 19 mai 2023 **Pendaison de Majid Kazemi #MajidKazemi**
========================================================================

.. figure:: images/majid_kazemi.png
   :align: center


.. figure:: images/majid_kazemi_artisan.png
   :align: center


2023-05-19 Le cousin du manifestant exécuté ce matin #MajidKazemi raconte comment la famille est empêchée de faire le deuil sur la tombe de Majid
=====================================================================================================================================================

- nitter.unixfox.eu/FreeMajidKazemi/status/1659538228423327744#m

Le cousin du manifestant exécuté ce matin #MajidKazemi raconte comment
la famille est empêchée de faire le deuil sur la tombe de Majid par les
miliciens basidjis qui n’arrêtent pas de ricaner et provoquer la famille.


#MajidKazemi clamait son innocence depuis la prison et dénonçait les tortures
inhumaines qu’il a subi pour faire des aveux forcés.

**Ce jeune artisan de 31 ans a été pendu ce matin par la dictature des mollahs.**


2023-05-23
=============

- https://nitter.net/HDS67/status/1661112370993700882#m

Pour obliger Majid à avouer un meurtre qu'il n'avait pas commis, ils ont
arrêté son frère Hossein et l'ont torturé sous ses yeux.

Majid a accepté de signer tout ce qu'ils voulaient.

Ils l'ont tué sur la base de ses faux aveux, puis ont arrêté ses deux frères.
#IranRevolution

