
.. index::
   pair: Assassiné ; Saeed Yaghoubi (2023-05-19)


.. _saeed_yaghoubi_2023_05_19:

========================================================================
Vendredi 19 mai 2023 **Pendaison de Saeed Yaghoubi #SaeedYaghoubi**
========================================================================



.. figure:: images/saeed_yaghoubi.png
   :align: center


- http://nitter.unixfox.eu/LettresTeheran/status/1659468686187458560#m

#SaeedYaghoobi s’occupait tout seul de sa mère de 81 ans et son père de 85 ans.

Il a été pendu ce matin par le régime mortifère des mollahs pour le seul
crime d’avoir manifesté.
