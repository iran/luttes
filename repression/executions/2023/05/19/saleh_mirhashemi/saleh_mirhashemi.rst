
.. index::
   pair: Assassiné ; Saleh Mirhashemi (2023-05-19)

.. _saleh_mirhashemi_2023_05_19:

========================================================================
Vendredi 19 mai 2023 **Pendaison de Saleh Mirhashemi #SalehMirhashemi**
========================================================================


.. figure:: images/saleh_mirhashemi.png
   :align: center



2023-05-23
=============

- https://nitter.net/Azadi4iranParis/status/1660769768381489152#m

La mère de Saleh Mirhashemi, manifestant exécuté par pendaison samedi dernier,
hospitalisée elle dit « Oh, Saleh, faites le descendre… ». Son fils Saleh
ainsi que Majid Kazemi et Saeed Yaghoubi ont été pendus après des aveux
obtenus sous la torture. #IranRevoIution #MahsaAmini
