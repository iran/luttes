.. index::
   pair: Pendaison; Mohammad Ghobadlou (2024-01-23)

.. _mohammad_ghobadlou:

========================================================================
2024-01-23 **Pendaison de Mohammad Ghobadlou** |MohammadGhobalu|
========================================================================

- https://fr.wikipedia.org/wiki/Mohammad_Ghobadlou
- http://iranhr.net/en/rss/
- http://iranhr.net/en/articles/6521/


Annonce
==========

- :ref:`iran_luttes_2024:execution_mohammad_ghobadlou`

Contexte
===========

Mohammad Ghobadlou (persan : محمد قبادلو) né en 2000, est un citoyen
iranien exécuté le 23 janvier 2024 pour sa participation aux manifestations
consécutives à la mort de Mahsa Amini en 2022, et condamné à mort pour
Moharebeh : faire la guerre contre Dieu.

Mohammad Ghobadlou et Mohammad Boroughani, également condamné à mort,
deviennent rapidement de nouveaux visages d'indignation en Iran contre
les exécutions politiques de manifestants.

En 2022, d'importantes manifestations font suite à la mort de Mahsa Amini,
une Kurde iranienne, décédée le 16 septembre 2022 après avoir été
détenue par la « police de la moralité ». Des milliers de manifestants
sont arrêtés et des dizaines sont inculpés de délits tels que Moharebeh
: faire la guerre contre Dieu, ou Mofsed-e-filarz : corruption des mœurs,
qui sont passibles de la peine de mort en Iran.


Procès et condamnation
==========================

Le 29 octobre 2022, se tient son procès présidé par **Abolqasem Salavati
surnommé « le juge de la mort »**.

Le tribunal déboute les deux avocats de
Mohammad Ghobadlou et désigne un avocat du « centre d'écoute judiciaire de la
République islamique ». Son propre avocat s'oppose fermement à la tenue du
procès : de son incarcération jusqu'au jour de l'audience, il n'a accès à
aucun document relatif à son dossier.

L'accusation ne présente aucune photo, aucun dossier de jurisprudence médicale
ni de documents courants.
Il est cependant condamné pour les chefs d'accusation d'« inimitié contre Dieu
Moharebeh (« faire la guerre contre Dieu ») ou Mofsed-e-filarz (« corruption
des mœurs »)

Le régime le prive également des médicaments visant à traiter son trouble
bipolaire1.

Le 24 décembre 2022, la Cour suprême du régime islamique annonce qu'elle
confirme la condamnation à mort de Mohammad Ghobadlou après avoir rejeté
son appel7.

L'avocat de Ghobadlou annonce le 26 août 2023 qu'à la suite de la confirmation
par 50 psychiatres iraniens de la nécessité d'un réexamen plus approfondi
du cas de Ghobadlou en raison de sa maladie mentale, la première chambre de
la Cour suprême avait annulé la condamnation à mort et renverrait l'affaire
à l'une des branches du tribunal pénal 1 de Téhéran.

Malgré cela, la procédure d'exécution est maintenue et Mohammad Ghobadlou
est exécuté le 23 janvier 2024.


Articles sur iranhr.net
==================================

- http://iranhr.net/en/rss/
- http://iranhr.net/en/articles/6521/
