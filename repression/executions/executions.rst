.. index::
   ! Exécutions

.. _executions:

=========================================
Exécutions
=========================================

- :ref:`nasrin_sotoudeh_2022_11_18`

.. figure:: gibet.png
   :align: center

Stop execution of iranian protesters
=====================================

- https://www.change.org/p/stop-execution-of-iranian-protesters


#MohsenShekari |MohsenShekari|  #MajidRezaRahnavard |MajidRezaRahnavard|
#MehdiKarami |MehdiKarami|  #MohammadHosseini |MohammadHosseini|


.. figure:: images/say_their_4_names.png
   :align: center

.. toctree::
   :maxdepth: 6

   2024/2024
   2023/2023
   2022/2022

