Exécutés suite à des événements survenus après e 16 septembre 2022

- :ref:`mohsen_shekari_2022_10_08` #MohsenShekari |MohsenShekari|
- :ref:`majid_reza_rahnavard_2022_12_12` #MajidRezaRahnavard |MajidRezaRahnavard|
- :ref:`mohammad_mehdi_karami_2023_01_07` #MehdiKarami |MehdiKarami|
- :ref:`mohammad_hosseini_2023_01_07` #MohammadHosseini |MohammadHosseini|
- :ref:`saeed_yaghoubi_2023_05_19` #SaeedYaghoub |SaeedYaghoubi|
- :ref:`saleh_mirhashemi_2023_05_19` #SaeedYaghoub |SalehMirhashemi|
- :ref:`majid_kazemi_2023_05_19` #MajidKazemi |MajidKazemi|
