.. index::
   pair: Otage ; Benjamin Brière
   ! Benjamin Brière

.. _benjamin_briere:

===========================================================================================================================
**Benjamin Brière, #BenjaminBriere**  |BenjaminBriere| détenu depuis mai 2020 #freeben **libéré le vendredi 12 mai 2023**
===========================================================================================================================

- https://change.org/freeben
- https://www.change.org/p/emmanuelmacron-lib%C3%A9rons-benjamin-bri%C3%A8re-en-gr%C3%A8ve-de-la-faim-dans-une-prison-iranienne-freeben
- https://www.instagram.com/liberonsbenjamin/

.. figure:: images/benjamin_briere.png
   :align: center


Je m’appelle Blandine. Mon frère Benjamin Brière |BenjaminBriere| est détenu dans la prison
de Vakilabad à Mashhad dans le Nord Est de l'Iran, depuis maintenant 20 mois.

Il est accusé d'espionnage alors qu’il voyageait simplement en touriste.

Nous avons tout fait pour alerter les autorités, mais depuis 20 mois,
Benjamin n’a toujours pas vu de juge.
Le 25 décembre dernier, il a entamé une grève de la faim pour dénoncer
les maltraitances dont il est victime. Il a franchi cette étape car les
autorités ne lui ont pas laissé appeler ses proches pour Noël, ses courriers
ne lui ont pas été transmis.

C’est une maltraitance de trop. Nous devons absolument répondre à son appel au secours.

Benjamin est détenu depuis mai 2020, date à laquelle il a été arrêté
alors qu’il visitait un parc naturel. Il est aujourd'hui accusé d'espionnage
et de propagande par le système islamique, pour avoir pris des photos
avec son drône de loisirs, et avoir posé la question « pourquoi le voile
est obligatoire en Iran, alors qu’il est facultatif dans d’autres pays musulmans? »
sur les réseaux sociaux.

**Aujourd’hui il est pris en otage par les autorités iraniennes**.

Il est détenu dans l'illégalité, la justice iranienne n'a toujours pas
décidé quel tribunal allait le juger, 20 mois après son arrestation.
Les Iraniens savent que Benjamin n'a rien fait. C’est une monnaie d'échange.

Il se sent aujourd’hui abandonné et interpelle, à travers sa grève de la
faim, les autorités iraniennes et françaises. Il dénonce l'absurdité de
la situation qui lui fait vivre depuis 20 mois un véritable enfer.

Il met sa santé, sa vie en jeu parce qu'il ne lui reste plus que ça pour
protester contre les atteintes aux droits de l'homme qu'il subit.

Aujourd’hui, avec la famille et les proches de Benjamin, nous avons
besoin de vous pour faire réagir le gouvernement français. Signez notre
pétition dès aujourd’hui pour faire libérer Benjamin des geôles iraniennes.

Un grand merci pour votre soutien. Ensemble, ramenons Benjamin à la maison.

Blandine Brière, soeur de Benjamin

#FreeBen



Historique
============

2023-05-12 Le touriste Benjamin Brière et le consultant Bernard Phelan sont sortis de prison et ont pu quitter la République islamique ce vendredi. La fin d’un long calvaire
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://www.lepoint.fr/monde/deux-otages-francais-liberes-d-iran-12-05-2023-2519869_24.php


2023-02-05 #BenjaminBrière 27 ans, otage français du régime iranien entame une grève de la faim
--------------------------------------------------------------------------------------------------

- https://twitter.com/LettresTeheran/status/1622716346030075904#m

#BenjaminBriere|  #BenjaminBrière 27 ans, otage français du régime iranien entame une grève
de la faim pour protester contre sa détention arbitraire, déclare sa soeur
« La seul arme avec laquelle il puisse lutter ».

