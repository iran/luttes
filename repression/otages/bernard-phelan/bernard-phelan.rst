.. index::
   pair: Otage ; Bernard Phelan
   ! Bernard Phelan

.. _bernard_phelan:

========================================================================================
**Bernard Phelan, #BernardPhelan** |BernardPhelan| **libéré le vendredi 12 mai 2023**
========================================================================================

#FreeIranOstages #BernardPhelan

- https://www.lepoint.fr/monde/bernard-phelan-prisonnier-en-iran-est-dans-un-etat-de-sante-critique-18-01-2023-2505320_24.php


.. figure:: images/bernard_phelan.png
   :align: center


«  C'est une question de jours » : il est urgent que Téhéran libère Bernard
Phelan, dont l'état de santé est désormais critique depuis qu'il a entamé
une grève de la soif pour protester contre sa détention arbitraire en Iran,
a souligné mercredi sa sœur dans un entretien avec l'Agence France-Presse.

De nationalité irlandaise et française, le sexagénaire a été arrêté
le 3 octobre 2022. Il était alors en voyage dans le cadre de ses activités
de « consultant en Iran pour un tour-opérateur », raconte Caroline Massé-Phelan.

C'était peu après le déclenchement de manifestations de masse en Iran
pour dénoncer la mort de Mahsa Amini, une jeune Iranienne de 22 ans décédée
à la suite de son arrestation par la police des mœurs pour violation
présumée du code vestimentaire pour les femmes.


Plusieurs Français arrêtés

Depuis, plusieurs dizaines d'Occidentaux, dont sept Français, ont été
arrêtés.
Leurs soutiens les décrivent comme des innocents utilisés par les Gardiens
de la révolution comme leviers dans les relations de l'Iran avec l'Occident.

Téhéran et les grandes puissances ont longtemps tenté, en vain, de
ressusciter un accord international de 2015 qui vise à garantir le caractère
civil du programme nucléaire iranien. Téhéran est accusé, malgré ses démentis,
de chercher à se doter de l'arme atomique.

« Le voyage de mon frère était prévu de longue date », explique Caroline
Massé-Phelan, précisant que celui-ci était parti le 17 septembre 2022,
soit le lendemain du décès tragique de Mahsa Amini.

Il n'avait alors pas de crainte particulière, confie sa sœur, qui n'a pas
pu avoir de contact direct avec lui depuis son arrestation.
La famille a commencé à s'inquiéter quand elle ne l'a pas vu revenir de son voyage.

Détenu dans une cellule de la prison de :ref:`Vakilabad <vakilabad>` à Mashhad, la deuxième
plus grande ville d'Iran, Bernard Phelan, 64 ans, souffre de problèmes
cardiaques et d'une pathologie aux os nécessitant une prise en charge
médicale.

Face à des autorités iraniennes inflexibles sur les demandes répétées
des autorités françaises et irlandaises de le libérer, il a entamé une
grève de la faim le jour de l'An avant de refuser tout liquide cette
semaine, conduisant à une dégradation rapide de son état de santé.


« Seules armes »
===================

Une source diplomatique a indiqué à l'AFP que Bernard Phelan montrait
« de graves signes d'épuisement physique et psychologique ».

La grève de la faim et de la soif mettent en péril sa vie, mais
« ce sont les seules armes » dont il dispose, souligne Caroline Massé-Phelan.

C'est aussi sur l'insistance de son frère qu'elle a décidé de rendre public son cas.

« C'est un innocent au milieu de je ne sais quelle histoire, qui adorait
l'Iran, qui a 64 ans, qui est malade, qui veut juste rentrer chez lui »,
dit-elle.

« Je pense qu'il fait partie d'un groupe d'Européens emprisonnés pour
des raisons politiques […] dont je ne connais rien », « nous n'avons rien
à voir dans cette histoire », insiste-t-elle.

« Ce sont des gens innocents qui sont utilisés comme sorte de pions
dans des histoires qui vont au-delà de notre compréhension ».
Le temps presse, observe-t-elle, alors que ses conditions de détention,
avant même la grève de la faim et de la soif, étaient déjà difficiles
à supporter. Outre la promiscuité avec les autres détenus, les températures
nocturnes sont glaciales dans une cellule sans vitres aux fenêtres.

Bernard Phelan est en contact deux fois par jour avec la cellule de crise
et de soutien du quai d'Orsay « qui transmet les messages de sa famille »,
a indiqué la source diplomatique française.
Mais les demandes de communication directe avec la famille ont toutes
été refusées par les autorités iraniennes.

Bernard Phelan n'a reçu sa première visite consulaire française que le 9 janvier 2023,
après des demandes répétées, a également expliqué la source diplomatique.

Son père a écrit à l'ambassade d'Iran en Irlande. Sans succès jusqu'à
présent, mais sa sœur veut garder l'espoir d'une libération pour raisons humanitaires.


