.. index::
   pair: Otage ; Cecile Kohler
   ! Cecile Kohler

.. _cecile_kohler:

===================================================================================
**Cecile Kohler** détenue depuis le 7 mai 2022 #FreeCecileKohler |CecileKohler|
===================================================================================

- https://libertepourcecile.com
- https://www.change.org/p/pour-la-lib%C3%A9ration-de-c%C3%A9cile-jacques-et-olivier-otages-en-iran (Pour la libération de Cécile, Jacques et Olivier, otages en Iran)
- https://libertepourcecile.com/pages/nous-aider.html
- https://www.instagram.com/liberte_pour_cecile/

::

    - https://twitter.com/FreeCecile_
    - https://nitter.poast.org/FreeCecile_/rss

#FreeIranOstages #CecileKohler
#freececile #freejacquesparis #freethemall

.. figure:: images/cecile_kohler.png
   :align: center


Introduction
=============

Présentée comme une syndicaliste, accusée à ce titre d’espionnage par l’Iran,
Cécile Kohler est d’abord une brillante enseignante agrégée de lettres
modernes partie découvrir l’ancienne Perse.
Cécile Kohler est détenue en Iran depuis le 8 mai 2022.


Yvelines : Cécile Kohler, professeure de français syndiquée FO, est détenue en Iran
========================================================================================

- https://actu.fr/ile-de-france/carrieres-sur-seine_78124/yvelines-cecile-kohler-professeure-de-francais-syndiquee-fo-est-detenue-en-iran_51000023.html

Yvelines : Cécile Kohler, professeure de français syndiquée FO, est détenue en Iran

Cécile Kohler, professeure au lycée Les Pierre-Vives de Carrières-sur-Seine
(Yvelines), est détenue en Iran depuis le 8 mai 2022. Explications.

Elle n’est pas rentrée de son voyage en Iran. Depuis la reprise des cours,
début mai, une professeure de français du lycée Les Pierres Vives de
Carrières-sur-Seine (Yvelines) est absente.

Cécile Kohler est détenue en Iran avec son mari. Tous les deux ont été
arrêtés à l’aéroport de Khomeini, probablement le 8 mai 2022, à l’issue d’un
voyage personnel durant les vacances de Pâques.
Ils étaient arrivés le 29 avril 2022. Ils auraient notamment visité Téhéran,
Kashan et Ispahan.

Selon nos informations, la direction de l’établissement aurait participé
à donner l’alerte. « Comme elle n’avait pas repris le travail, cette
même direction a informé sa hiérarchie », précise notre source.

Du côté du rectorat, aucune information ne fuite sur cet événement.

Engagée syndicale
--------------------

Cécile Kohler est professeure, agrégée de Lettres Modernes.

Depuis 2011 , elle est également engagée au niveau syndical, en tant que
représentante de la section SNFOLC au lycée (Syndicat national Force
ouvrière des lycées et collèges).

A compter de 2016, elle a été chargée des relations internationales pour
la Fédération de l’enseignement, de la culture et de la formation
professionnelle Force ouvrière (FNEC FP-FO).

Pour l’heure, les raisons de l’arrestation de Cécile Kohler et de son
mari restent floues.
Le mercredi 11 mai 2022, le ministre iranien des Renseignements avait
annoncé l’interpellation, en précisant que les « deux Européens étaient
accusés d’être venus déstabiliser la République islamique ».


Pétitions pour Cécile Kohler et Jacques Paris
=========================================================

- https://libertepourcecile.com/pages/communiques/petition-jacques-paris-cecile-kohler-otages-en-iran.html


Articles 2025
===============

- :ref:`arthaud_grenoble:otages`
- https://www.lalsace.fr/politique/2025/01/21/porter-la-voix-et-le-nom-de-cecile-kohler-au-parlement-europeen
- https://france3-regions.francetvinfo.fr/grand-est/bas-rhin/strasbourg-0/detention-de-cecile-kohler-en-iran-les-proches-des-otages-recus-par-le-parlement-europeen-3094789.html
- https://www.francebleu.fr/infos/international/la-soeur-de-cecile-kohler-demande-au-parlement-europeen-de-faire-pression-sur-l-iran-5280231
- https://www.francebleu.fr/infos/international/l-alsacienne-cecile-kohler-detenue-en-iran-dans-des-conditions-tres-preoccupantes-selon-la-prix-nobel-narges-mohammadi-8625646

Articles 2024
===============

- :ref:`crha_2024:iran_solidarite_2024_05_11`

Articles 2023
===============


2023-09-17 Iran: les détenus français Cécile Kohler et Jacques Paris face à un possible procès pour espionnage
-------------------------------------------------------------------------------------------------------------------

- https://www.rfi.fr/fr/monde/20230917-iran-les-d%C3%A9tenus-fran%C3%A7ais-c%C3%A9cile-kohler-et-jacques-paris-face-%C3%A0-un-possible-proc%C3%A8s-pour-espionnage


Cela fait près de 500 jours que l’enseignante et syndicaliste française
Cécile Kohler est détenue en Iran avec son compagnon Jacques Paris.

Mardi 12 septembre 2023, la justice iranienne a annoncé la fin de l’enquête
à leur encontre, ouvrant la voie à leur éventuel procès pour espionnage,
mais sans indiquer de date.

La justice iranienne a indiqué mardi que l'enquête sur deux Français
arrêtés en 2022 en Iran, l'enseignante Cécile Kohler et son compagnon
Jacques Paris, était terminée, ouvrant la voie à un éventuel procès pour
« espionnage ». « Un homme et une femme, tous deux citoyens français,
ont été arrêtés pour espionnage contre l'Iran », a déclaré le porte-parole
du ministère de la Justice, Massoud Satayshi, au cours d'une conférence
de presse.

« Leur cas a été transmis au tribunal compétent après une enquête approfondie.
Une mise en accusation a été prononcée », a-t-il ajouté, sans donner de
détails.

« Leurs avocats ont examiné à deux reprises leur dossier et, lorsque de
nouvelles informations seront disponibles, nous les communiquerons »,
a poursuivi le porte-parole.

Très peu de communication avec les deux détenus
+++++++++++++++++++++++++++++++++++++++++++++++++++=

Cécile Kohler, enseignante de français et syndicaliste, et Jacques Paris,
retraité, ont été arrêtés le 7 mai 2022 alors qu'ils faisaient du tourisme
en Iran.
Leurs proches ont à plusieurs reprises organisé des rassemblements en
France pour réclamer leur libération et dénoncer leurs conditions de
détention « extrêmement difficiles » à la prison d'Evin de Téhéran.

Noémie, la sœur de Cécile Kohler et porte-parole du comité de soutien
« Liberté pour Cécile » est d’autant plus inquiète, qu’elle et sa famille
ont eu très peu de nouvelles de Cécile depuis son arrestation, explique
notre correspondante à Strasbourg, Wyloën Munhoz-Boillot.
Leur dernier échange téléphonique remonte au 23 août 2023.

« Depuis mars, elle a l’autorisation de nous appeler de manière très
aléatoire, toutes les cinq à six semaines.
Ce sont des appels très courts, de moins de 10 minutes, de très mauvaise
qualité.
Donc, on a du mal à dialoguer et elle est sous haute surveillance.
Donc, en fait, on n’en sait pas beaucoup de ses conditions de détention
réelles.

Ces appels consistent principalement à nous donner nous des nouvelles
de la famille parce qu’elle en a besoin pour tenir.
Elle, elle, essaie de nous rassurer, elle nous dit : "Je tiens le coup,
ne vous en faites pas, ça va". Mais on sent qu’elle essaie de nous
rassurer et on sait, d’après les témoignages d’autres prisonniers, que
ce sont des conditions quand même extrêmement rudes.
Donc, on s’attend au pire. »


Des dizaines d'autres étrangers aussi détenus en Iran
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Outre Cécile Kohler et Jacques Paris, deux autres Français sont toujours
détenus en Iran, le consultant :ref:`Louis Arnaud <louis_arnaud>`, arrêté le 28 septembre 2022
à Téhéran, et un :ref:`autre dont l'identité n'a jamais été rendue publique <otage_x>`.

« Rien ne justifie la détention » des « ressortissants français dans les
prisons et dans des conditions inadmissibles en Iran », a déclaré le
président Emmanuel Macron dans un discours le 28 août.

D'autres pays européens et des défenseurs des droits humains accusent
Téhéran de détenir des dizaines d'étrangers dans une stratégie de chantage.

Un autre Français, :ref:`Benjamin Brière <benjamin_briere>`, et un ressortissant franco-irlandais,
:ref:`Bernard Phelan <bernard_phelan>`, ont été libérés en mai pour
« raisons humanitaires ».


.. _soutien_cecile_jacques_2023_05_17:

2023-05-17 **Communiqué de l’union syndicale des enseignants iraniens en soutien à Cécile Kohler et Jacques Paris**
-------------------------------------------------------------------------------------------------------------------------

- http://nitter.unixfox.eu/LettresTeheran/status/1658745105153511428#m


A l’occasion de l’anniversaire de l’arrestation du couple d’enseignants
français Cécile Kohler et Jacques Paris ainsi que des enseignants iraniens
dans le même dossier, l’union syndicale des enseignants iraniens publie un
communiqué en soutien à ces deux otages d’état et demande leur libération
sans délai, voici un extrait :

« l'année dernière Mme Cécile Kohler et sa compagne M. Jacques Paris,
ont eu une rencontre très amicale et informelle avec un groupe d'enseignants
lors d'un voyage touristique en Iran. Ces deux français qui s'étaient
rendus en Iran avec un visa touristique et avec l'autorisation du ministère
des Affaires étrangères ont été arrêtés et sont détenus dans le quartier
de sécurité 209 depuis cette date.

Dans un acte éhonté et illégal, la télé d’état, en collusion avec les
forces de sécurité, a publié les aveux forcés de ces deux enseignants
dans une émission télévisée.

Il est évident pour tout le monde que les aveux télévisés n'ont jamais
eu et n'auront jamais de validité légale. Les forces de sécurité à la hâte
et avant d'interroger les enseignants et ouvriers arrêtés, dans une
action illégale dans les tout premiers jours de l'arrestation, en réalisant
un clip, ont tenté d'attribuer les protestations des enseignants aux
deux enseignants français.

Au bout de six mois, ils tentent à nouveau avec un nouveau clip d'attribuer
le soulèvement populaire de septembre à la direction de ces deux enseignants.

Tout le monde sait que ces histoires sont l'œuvre des scénaristes des
institutions de sécurité et sont sans fondement...»

Article du jeudi 16 février 2023
----------------------------------------

- :ref:`cecile_kohler_2023_02_16_local`
- https://www.lepoint.fr/monde/otage-francaise-en-iran-ma-soeur-est-coupee-du-monde-16-02-2023-2508940_24.php#xtor=CS2-239
- https://nitter.manasiwibi.com/arminarefi/status/1626483293787983872#m

|CecileKohler| ELLE S'APPELLE Cécile Kohler et a 38 ans. Professeur de lettres, elle
s'est rendue en #Iran en avril 2022 avec son ami Jacques Paris.

Elle et lui sont détenus depuis 9 mois dans la funeste prison Evin
de Téhéran.

Sa soeur tire la sonnette d'alarme .


Article du 29 juin 2022
--------------------------------

- https://fr.timesofisrael.com/deux-iraniens-detenus-pour-avoir-travaille-avec-un-couple-francais-arrete-en-mai/

Deux Iraniens ayant travaillé comme interprètes pour un couple de Français
arrêté en Iran en mai sont détenus eux aussi depuis plus d’un mois, a
alerté mardi la mère de l’un d’eux dans un message vidéo.

Cécile Kohler, une responsable syndicale dans l’enseignement français et
son conjoint Jacques Paris ont été arrêtés le 7 mai alors qu’ils faisaient
du tourisme en Iran pendant les vacances de Pâques, selon une source syndicale
française, tandis que Téhéran les a accusés de chercher à « déstabiliser » le pays.

Anisha Asadollahi et son mari, Keyvan Mohtadi, qui travaillaient comme
interprètes pour le couple, sont eux aussi détenus depuis plus d’un mois
et demi, selon la mère de Mme Asadollahi.

Ils ont été arrêtés le 9 mai lors d’un raid policier à leur domicile à
Téhéran, affirme la mère, qui n’a pas donné son prénom, dans un message
vidéo publié par la radio Zamaneh, basée aux Pays-Bas.

« Cela fait 48 jours que ma fille est en détention, dont 33 passés à
l’isolement. Son mari Keyvan connaît le même sort », dit-elle dans ce
message, dont une version a été publiée par le Centre pour les droits
humains en Iran (CHRI), basé à New York.

« Pourquoi ? Pour avoir été les interprètes des deux Français ? »,
demande-t-elle, avant d’appeler à leur « libération immédiate et
inconditionnelle ». « La prison n’est pas pour les écrivains et les traducteurs »,
ajoute-t-elle.

Le couple français a été accusé de rencontrer des représentants de
syndicats d’enseignants iraniens et de tenter de provoquer des troubles.

Le ministère français des Affaires étrangères avait dénoncé des arrestations
« sans fondement » et appelé à leur libération immédiate.

