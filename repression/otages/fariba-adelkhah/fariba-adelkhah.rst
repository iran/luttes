.. index::
   pair: Otage ; Fariba Adelkhah
   ! Fariba Adelkhah

.. _fariba_adelkhah:

======================================================================================================================================================
**Fariba Adelkhah, #FaribaAdelkhah** |FaribaAdelkhah| libérée le 10 février 2023 mais **assignée à résidence**; en france depuis le 17 octobre 2023
======================================================================================================================================================

- https://fr.wikipedia.org/wiki/Fariba_Adelkhah

#FreeIranOstages #FaribaAdelkhah


.. figure:: images/fariba_adelkhah.png
   :align: center


.. _fariba_adelkhah_2023_10_18:

2023-10-18 La chercheuse franco-iranienne Fariba Adelkhah, retenue en Iran depuis 2019, de retour en France
============================================================================================================

- https://www.france24.com/fr/info-en-continu/20231018-%F0%9F%94%B4-la-chercheuse-franco-iranienne-fariba-adelkhah-d%C3%A9tenue-en-iran-de-retour-en-france-annonce-son-comit%C3%A9-de-soutien

La chercheuse franco-iranienne Fariba Adelkhah est rentrée en France mardi
après quatre ans de captivité en Iran, a annoncé mercredi son comité de
soutien.

Anthropologue réputée, directrice de recherche au Centre de recherches
internationales de Sciences Po à Paris, Fariba Adelkhah avait été arrêtée
en juin 2019 avec un autre chercheur français, son compagnon Roland Marchal,
qui a lui été libéré en 2020.


La fin du calvaire pour Fariba Adelkhah. La chercheuse franco-iranienne
arrêtée en Iran en 2019 pour atteinte à la sécurité nationale, puis libérée
en février dernier mais empêchée de quitter le territoire, est de retour
en France, a annoncé mercredi 18 octobre l'Institut d'études politiques
de Paris.

"Depuis mardi, Fariba Adelkhah est enfin de retour en France.
Elle a été accueillie à son arrivée à l'aéroport par Béatrice Hibou,
présidente de son comité de soutien, et Mathias Vicherat, directeur
de Sciences Po", a indiqué un communiqué de l'école.

"Après quatre ans et demi de privation de liberté, me voici de retour
en France", a déclaré l'universitaire dans un communiqué de son comité
de soutien, remerciant "du fond du cœur la diplomatie française" et
tous ceux qui ont contribué à sa libération.

"Désormais, tout cela est derrière moi.

Ce qui reste, ce sont tous ces gestes d'amitié et d'engagement, ces
mobilisations de connus et d'inconnus (...).
Et évidemment, ce que le comité de soutien a su faire au-delà de mon cas,
et pendant plus de quatre ans, par fidélité au principe de la liberté scientifique."

Quatre Français restent détenus en Iran
-----------------------------------------------

Spécialiste du chiisme et de l'Iran post-révolutionnaire à Sciences Po,
Fariba Adelkhah avait été arrêtée en 2019 puis condamnée en 2020 à cinq
ans de prison pour atteinte à la sécurité nationale, ce que ses proches
ont toujours farouchement nié.

Elle avait été libérée en février mais n'avait pas été autorisée à quitter le pays.

Quatre Français restent détenus en Iran : Cécile Kohler et Jacques Paris,
arrêtés le 7 mai 2022, "lors d'un séjour touristique" selon leurs proches,
Louis Arnaud, un voyageur 36 ans, ainsi qu'un autre Français dont
l'identité n'a jamais été rendue publique.

"C'est un grand soulagement pour sa famille et tous ses proches", a
indiqué mercredi le ministère français des Affaires étrangères dans
un communiqué. Mais la France "demande la libération immédiate" de tous
ses ressortissants encore emprisonnés par la République islamique.

Plusieurs dizaines d'Occidentaux sont détenus par la République islamique,
décrits par leurs soutiens comme des individus innocents utilisés comme
leviers de négociation.


2023-02-10 #FaribaAdelkhah la détenue franco-iranienne est libérée de la prison d’Evin ce soir selon plusieurs sources
============================================================================================================================

- https://twitter.com/LettresTeheran/status/1624107983381663745#m
- :ref:`fariba_adelkhah_2023_02_10_local`

#FaribaAdelkhah la détenue franco-iranienne est libérée de la prison d’Evin
ce soir selon plusieurs sources

Selon l’un des avocats de #FaribaAdelkhah sa libération est définitive.
Cependant il n’est pas encore clair si elle peut quitter le pays. Fariba
a été condamnée en 2020 à 5 ans de prison pour « complot contre la sécurité nationale »,
elle vient d’être « graciée ».


Wikipedia
=============

Fariba Adelkhah, née le 25 avril 1959 à Téhéran, est une anthropologue
franco-iranienne. Directrice de recherche à l'Institut d'études politiques
de Paris, elle est arrêtée en Iran en juin 2019, accusée d'espionnage,
emprisonnée pendant plus d'un an puis libérée avec un bracelet électronique.

Elle est à nouveau incarcérée à la prison d'Evin le 12 janvier 2022


Article de janvier 2022
==========================

Fariba Adelkhah a été arrêté arrêtée en juin 2019 et condamnée en mai 2020
à cinq ans de prison pour atteintes à la sécurité nationale.
En détention à domicile, elle a récemment été réincarcérée, selon son comité de soutien.


La chercheuse franco-iranienne Fariba Adelkhah, qui est retenue en Iran
depuis 2019 et qui avait été placée en résidence surveillée en 2020, a
été de nouveau incarcérée à Téhéran, a annoncé mercredi son comité de
soutien à Paris. Informée de la situation, le quai d’Orsay a immédiatement
demandé sa « libération immédiate ».

« Nous apprenons avec stupeur et indignation la réincarcération dans la
prison d’Evin de Fariba Adelkhah », a annoncé ce comité dans un communiqué,
dénonçant des agissements « cyniques » du pouvoir iranien qui utiliserait
le cas de la chercheuse « selon des fins extérieures ou intérieures qui
demeurent opaques ».
