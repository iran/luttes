.. index::
   pair: Otage ; Jacques Paris
   ! Jacques Paris

.. _jacques_paris:

=====================================================================================
**Jacques Paris** détenu depuis le 7 mai 2022 #FreeJacquesParis |JacquesParis|
=====================================================================================

- https://www.change.org/p/pour-la-lib%C3%A9ration-de-c%C3%A9cile-jacques-et-olivier-otages-en-iran (Pour la libération de Cécile, Jacques et Olivier, otages en Iran)
- https://libertepourjacques.com/
- :ref:`cecile_kohler`

.. figure:: images/jacques_paris.png
   :align: center

.. figure:: images/jacques_paris_2.png
   :align: center


Articles 2025
===============

- :ref:`arthaud_grenoble:otages`

Articles 2024
===============

- :ref:`crha_2024:iran_solidarite_2024_05_11`

