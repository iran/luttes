.. index::
   pair: Otage ; Louis Arnaud
   ! Louis Arnaud

.. _louis_arnaud:

===============================================================================================================
**Louis Arnaud** détenu depuis le 28 septembre 2022 #FreeLouisArnaud  |LouisArnaud|  (libéré le 12 juin 2024)
===============================================================================================================

- https://twitter.com/freelouisarnaud
- https://nitter.poast.org/freelouisarnaud/rss
- :ref:`louis_arnaud_2023_01_26`
- https://www.lepoint.fr/monde/le-combat-d-une-mere-pour-liberer-son-fils-emprisonne-en-iran-28-02-2023-2510269_24.php

.. figure:: images/louis_arnaud.png

.. figure:: images/louis_arnaud_2.png


Pétition
==========

- https://www.change.org/p/lib%C3%A9rez-louis-arnaud-voyageur-d%C3%A9tenu-en-iran-depuis-le-28-septembre-2022

Nous lançons cet appel du coeur afin de venir en aide à Louis Arnaud, notre ami,
notre frère, notre fils.

Louis est une personne ordinaire qui a tout mis en oeuvre pour réaliser un
rêve qui l’habitait depuis toujours : voyager sur la route de la soie par voie
terrestre et maritime.

Sensible à la préservation de notre planète, Louis a fait le choix délibéré
d'éviter l'avion, affirmant ainsi son engagement écologique lors de son périple.

Louis est un voyageur passionné, qui a toujours cherché à comprendre et à
apprécier les différentes cultures du monde. Il est parti en juillet 2022 avec
un esprit d'ouverture et de respect, loin de toute volonté de perturbation ou
de provocation.

Après avoir traversé l’Italie, la Grèce et la Turquie, Louis passera par la
Géorgie puis l'Arménie avant d’entrer en République islamique d'Iran où il
sera arrêté.

Depuis le 28 septembre 2022, Louis est détenu dans la prison d’Evin à
Téhéran. Il a été arrêté en compagnie d'une Italienne, d'un Polonais et
d’une Iranienne rencontrés sur les routes, sans que rien n’ai pu leur être
officiellement reproché.

Tous ont été libérés. À l’exception de Louis.

Les mois passent et il se sent complètement démuni face à la situation.

Par la présente, nous interpellons notre gouvernement et sa diplomatie, afin
qu’ils soutiennent leurs efforts pour sa prochaine libération. Avant qu’il
n’en reste définitivement meurtri…

De toute évidence, nous craignons qu'il subisse des dommages psychologiques et
physiques irréversibles. Il a besoin de notre aide.

Ne laissons pas un innocent en prison.

❤️ Aidez Louis en signant et en partageant cette pétition le plus largement
possible.

Votre signature représente beaucoup. Elle renforce nos attentes envers les
autorités afin qu’elles réagissent rapidement. De plus, ellemontre à Louis
et sa famille qu’ils ne sont pas seuls dans cette épreuve.

Un immense merci pour votre soutien.

Ensemble, ramenons Louis à la maison.

Articles 2024
===============

- :ref:`crha_2024:iran_solidarite_2024_05_11`
