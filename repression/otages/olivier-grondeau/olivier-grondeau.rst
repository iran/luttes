.. index::
   pair: Otages ; Olivier Grondeau

.. _otage_x:
.. _olivier_grondeau:

===================================================================================================
**Olivier Grondeau** détenu depuis le 12 octobre 2022  |OlivierGrondeau|
===================================================================================================

- https://www.change.org/p/pour-la-lib%C3%A9ration-de-c%C3%A9cile-jacques-et-olivier-otages-en-iran
- https://piaille.fr/@libertepourolivier
- https://linktr.ee/libertepourolivier
- https://libertepourolivier.fr/
- https://www.instagram.com/libertepourolivier/

Olivier Grondeau
=====================

- :ref:`cecile_kohler` |CecileKohler|
- :ref:`jacques_paris` |JacquesParis|


Olivier Grondeau fait partie des 3 otages français (:ref:`Cecile Kohler <cecile_kohler>` |CecileKohler|
et :ref:`Jacques Paris <jacques_paris>` |JacquesParis|)  détenus arbitrairement en Iran.

 
**Olivier Grondeau est détenu à la prison d'Evin depuis le 12 octobre 2022**.


Appel à l'aide d'Olivier Grondeau depuis la prison d'Evin
===========================================================

- https://libertepourolivier.fr/appel-a-l-aide
- https://libertepourolivier.fr/20250113_Appel_a_l-aide_Olivier.mp3

Cliquer sur https://libertepourolivier.fr/20250113_Appel_a_l-aide_Olivier.mp3 
pour écouter l'appel à l'aide d'Olivier Grondeau depuis la prison d'Evin.

Articles 2025
================

- :ref:`arthaud_grenoble:otages`
- https://www.theguardian.com/world/2025/jan/17/iran-knows-my-son-is-innocent-says-mother-of-french-man-held-in-evin-jail
- :ref:`iran_luttes_2025:newsletter_olivier_grondeau_2025_01_15`
- https://www.telerama.fr/debats-reportages/apres-voir-lu-la-lettre-d-olivier-grondeau-detenu-en-iran-on-n-ecoutera-plus-barbara-de-la-meme-facon-7023887.php
- :ref:`iran_luttes_2025:olivier_grondeau_2025_01_13`
- https://www.brut.media/fr/videos/france/politique/jean-noel-barrot-sur-brut-il-est-retenu-en-otage
- https://www.france.tv/france-5/c-a-vous/saison-16/6861376-detenu-en-iran-le-francais-olivier-grondeau-brise-le-silence-la-story-c-a-vous.html
- https://www.lemonde.fr/international/article/2025/01/13/olivier-grondeau-francais-detenu-en-iran-sort-de-l-anonymat-et-confie-son-epuisement_6495655_3210.html
- https://www.francebleu.fr/infos/international/temoignage-je-suis-innocent-olivier-grondeau-touriste-francais-est-detenu-en-iran-depuis-plus-de-deux-ans-5144992
- https://www.rfi.fr/fr/podcasts/invit%C3%A9-international/20250113-il-est-loin-de-tout-ce-dont-on-l-accuse-t%C3%A9moigne-la-m%C3%A8re-d-olivier-grondeau-d%C3%A9tenu-en-iran
