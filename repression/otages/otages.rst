.. index::
   ! Otages français

.. _otages_francais:

=========================================
Otages français
=========================================

- https://www.change.org/p/pour-la-lib%C3%A9ration-de-c%C3%A9cile-jacques-et-olivier-otages-en-iran (Pour la libération de Cécile, Jacques et Olivier, otages en Iran)

.. figure:: images/petition_otages.png

.. toctree::
   :maxdepth: 3

   benjamin-briere/benjamin-briere
   bernard-phelan/bernard-phelan
   cecile-kohler/cecile-kohler
   fariba-adelkhah/fariba-adelkhah
   jacques-paris/jacques-paris
   louis-arnaud/louis-arnaud
   olivier-grondeau/olivier-grondeau   
