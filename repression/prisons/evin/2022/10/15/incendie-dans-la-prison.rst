.. index::
   pair: Evin; 2022-10-15

.. _evin_incendie_2022_10_15:

============================================================
Samedi 15 octobre 2022 **Incendie dans la prison d'Evin**
============================================================

- https://fr.wikipedia.org/wiki/Incendie_de_la_prison_d%27Evin

L'incendie de la prison d'Evin est survenu le 15 octobre 2022 à partir
d'environ 22h0 heure locale lorsqu'une série d'incidents, dont un
incendie, des explosions et des combats à l'arme automatique, s'est
produite dans la prison d'Evin à Téhéran, en Iran, se poursuivant
jusqu'au petit matin du 16 octobre 2022.


L'agence de presse Fars a déclaré que quatre "explosions massives" ont
eu lieu après que l'IRNA a affirmé que l'incendie avait été éteint.

Un témoin du NYT a déclaré avoir entendu des coups de feu automatiques
et des explosions "énormes" vers 21h00, heure locale.

Selon le Centre pour les droits de l'homme en Iran (en), une "fusillade"
a eu lieu dans la prison d'Evin à 22h00 heure locale dans la nuit du
15 octobre 2022.

Des vidéos partagées sur les réseaux sociaux le 15 octobre montraient
de la fumée s'élevant de la prison. Des coups de feu répétés ainsi
que des chants antigouvernementaux pouvaient également être entendus
dans les vidéos.

Des explosions provenant de la prison ont été entendues aux premières
heures du 16 octobre 2022.

Amirdaryoush Youhaei, un résident local, a déclaré : « Il y a eu une
énorme sirène, puis onze grosses explosions et des tirs de mitrailleuses
qui ne se sont pas arrêtés. [...] C'était comme si nous regardions un
film de guerre. »

.. figure:: images/evin_en_feu.png
   :align: center
