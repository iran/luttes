.. index::
   pair: Prison ; Evin
   ! Evin

.. _evin:

=========================================
Evin
=========================================

- https://fr.wikipedia.org/wiki/Prison_d%27Evin


Origines
============

La prison d'Evin est construite en 1972, sous le règne du dernier shah
d'Iran, Mohammad Reza Pahlavi.

Elle se situe dans le quartier d'Evin, au nord de la capitale iranienne,
Téhéran, au pied des monts Alborz. C'est là que se trouvait la maison de
Seyyed Zia'eddin Tabatabai, premier ministre de l'Iran dans les années 1920.

La prison inclut alors une cour pour les exécutions, une cour de promenade
et des quartiers séparés pour les prisonniers hommes et femmes.

La prison est alors sous le contrôle de la SAVAK, service de sécurité
intérieure et de renseignement du régime. Elle a une capacité de 320
prisonniers (dont 20 dans les cellules d'isolement et 300 dans deux
grandes cellules collectives).

En 1977, la prison est agrandie, afin d'accueillir jusqu'à 1 500 prisonniers,
dont au moins 100 dans les cellules d'isolement.
En effet, la prison d’Evin a alors supplanté la prison de Qasr dans
l’accueil des prisonniers politiques.

La prison reçoit alors des prisonniers prestigieux. Peuvent y être enfermé
des opposants communistes ou des sympathisants de gauche, mais aussi des
religieux proches de Rouhollah Khomeini, tels que Mahmoud Taleghani et
Hossein Ali Montazeri.


Evenements
============

.. toctree::
   :maxdepth: 6

   2022/2022

