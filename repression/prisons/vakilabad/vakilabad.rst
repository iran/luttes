.. index::
   pair: Prison ; Vakilabad
   ! Vakilabad

.. _vakilabad:

=========================================
Vakilabad
=========================================

- https://en.wikipedia.org/wiki/Vakilabad_Prison


Vakilabad Prison (زندان وکیل‌آباد), also called Central Prison of Mashhad (زندان مرکزی مشهد),
is a prison in Iran, located in the city of Mashhad in the northeast
of the country.

The prison has reportedly been the site of hundreds of secret executions
carried out by the Islamic Republic of Iran government.

