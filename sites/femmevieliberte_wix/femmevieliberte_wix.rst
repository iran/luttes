.. index::
   pair: Site; FemmeVieLiberte
   ! FemmeVieLiberte

.. _femmevieliberte_wix:

======================================================================================================================
**Femme Vie Liberté, Soutien à la révolution iranienne en cours**
======================================================================================================================

- https://femmevieliberte.wixsite.com/my-site
- https://www.instagram.com/bibpourtous/
- femmevieliberte@mailo.com

.. figure:: images/logo.png
   :align: center


.. figure:: images/logo_2.png
   :align: center

.. figure:: images/instagram.png
   :align: center


Qui sommes-nous ?
======================

​Nous sommes un collectif de bibliothécaires et actrices/acteurs de la
culture mobilisé pour rendre visible l’incroyable révolution féministe,
sociale, sociétale et culturelle en cours en Iran.

Avec plusieurs collectifs d’Iranien·nes en lutte, de citoyen·nes solidaires,
nous constatons que les médias « mainstream » ne couvrent plus qu’à de
rares exceptions cette révolution qui nous concerne pourtant toutes et
tous.

Alors qu’à ce jour [7 décembre 2022] une répression inouïe fait rage
(plus de 475 personnes, dont 64 enfants, tuées depuis le 16 septembre
et 18742 personnes arrêtées et passibles de peine de mort), la révolution
contre la dictature se poursuit, plus inventive que jamais, dans tout le
pays, et toute la société.

Ils et elles appellent à l’aide et il y a urgence : le régime commence
à exécuter les premiers manifestants.

Soyons leur voix!


