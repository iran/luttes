.. index::
   pair: Site; FemmeVieLiberte
   ! FemmeVieLiberte

.. _femmevieliberte_wordpress:

======================================================================================================================
Collectif national **FemmeVieLiberté => ATTENTION compromission INADMISSIBLE avec l'extrême droite !!!**
======================================================================================================================

- https://femmevieliberte.wordpress.com/
- grenoble@femmevieliberte.info

Compromission avec l'extrême droite
======================================

Des photos avec Ciotti en première page de leur site. **C'est inadmissible**.


.. figure:: images/logo_site.png
   :align: center

   https://femmevieliberte.wordpress.com/


.. toctree::
   :maxdepth: 3

   qui-sommes-nous/qui-sommes-nous
   nos-revendications/nos-revendications



Comment nous contacter ?
============================

Un moyen simple de contacter les organisateurs de rassemblements dans ces
ville de la france:


.. figure:: images/contact.png
   :align: center

- grenoble@femmevieliberte.info


