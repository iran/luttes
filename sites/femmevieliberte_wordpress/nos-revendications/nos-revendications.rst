
.. _revendications_femmevieliberte:

===========================================
Revendications FemmeVieLiberte
===========================================


- Fermeture de l’ambassade d’Iran à Paris.
- Fin complète des relations diplomatiques avec l’Iran.
- Blocage de tous les biens du régime à l’étranger.
- Sanction de l’ensemble du CGRI et pas seulement de certains de ses membres.
- Sanction de tous les responsables du gouvernement et du régime iraniens.
- Expulsion les ambassadeurs étrangers d’Iran.
