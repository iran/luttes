
========================
Qui sommes nous ?
========================

- https://fr.wikipedia.org/wiki/Lion_solaire

- Nous sommes des centaines d’individus qui ont aidé bénévolement à organiser
  des événements de “Femme Vie Liberte” chacun dans nos villes.
  Nous sommes maintenant en contact les uns avec les autres
- Nous ne sommes affiliés à aucune opinion politique, ni de droite ni de gauche **(=> donc de droite voire d'extrêmedroite) comme d'habitude, NDLR)**
- Nous ne sommes ni séparatistes ni Modjahed ni monarchiste
- Nous ne recevons aucun soutien financier d’aucune organisation.
- Le drapeau `Le lion solaire <https://fr.wikipedia.org/wiki/Lion_solaire>`_, souvent utilisé par les participants des
  manifestations, n’est pas un symbole du monarchisme ou du Pahlavi.
  Il est lié à l’histoire, à la culture et à l’identité de l’Iran.

