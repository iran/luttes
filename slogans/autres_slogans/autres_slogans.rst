
.. _autres_slogans:

=====================
📣 Autres slogans 📣
=====================


Liberté pour l'Iran/Justice pour l'Iran
=========================================

::

    Liberté pour l'Iran/Justice pour l'Iran



Je tuerai ! Je tuerai ! Celui qui a tué ma soeur !
==========================================================

- https://nitter.manasiwibi.com/arminarefi/status/1592517491380871169#m

::

    Je tuerai ! Je tuerai ! Celui qui a tué ma soeur !

"Je tuerai ! Je tuerai ! Celui qui a tué ma soeur !", scandent des écolières
à la sortie des cours ce mardi 15 novembre 2022 dans le sud de Téhéran,
en référence aux 25 femmes tuées en #Iran dans les manifestations qui ont
suivi la mort de #MahsaAmini pour un voile mal porté.


Liberté, Liberté, Liberté
==============================

::

    Azadi ! (Liberté) Azadi ! Azadi !

LE CENTRE COMMERCIAL « City Center » d’Ispahan, dans le centre de l’#Iran,
pris d’assaut par des manifestants qui scandent en chœur « Azadi ! (Liberté) Azadi ! Azadi ! »,
à l’occasion des commémorations des 3 ans des tueries de novembre 2019
(des 100aines de manifestants tués)


- https://nitter.manasiwibi.com/arminarefi/status/1593493350098903041#m

FÊTE ET/OU RÉVOLTE ce jeudi 17 novembre 2022 dans la ville d’Abdanan, dans
l’ouest de l’#Iran, où des contestataires exultent autour d’un brasier
géant en répétant : « Azadi ! (Liberté) Azadi ! Azadi ! », au 62e jour
de contestation après la mort de #MahsaAmini pour un voile mal porté


Bassidjis, Pasdaran/Notre Daech, c'est vous
==============================================

::

    Bassidjis, Pasdaran/Notre Daech, c'est vous


Source: https://masthead.social/@NaderTeyf/109409135432927467

Un des slogans que l'on entend souvent dans les manifestations contre le
régime des mollahs en #Iran ces jours-ci est:"Bassidjis, Pasdaran/Notre Daech, c'est vous".

Le groupe Lafarge finançait Daech en Syrie et le groupe franco-italien
Cheddite arme les mollahs.



Mort à [l’ayatollah] Khamenei !
==================================

- https://nitter.manasiwibi.com/arminarefi/status/1592522372523118593#m

::

    Mort à [l’ayatollah] Khamenei !


LES UNIVERSITÉS grondent à nouveau en #Iran, ici la faculté des Sciences
et de la Culture de Téhéran, où des étudiants scandent en chœur ce
mardi 15 novembre 2022 « Mort à [l’ayatollah] Khamenei ! », du nom du
guide suprême et plus haute autorité politique du pays. #MahsaAmini


Khameneii, Khameneii, dégage degage de l'Iran
==============================================

::

    Khameneii, Khameneii, dégage degage de l'Iran



Cette année est l'année du sang/Seyed Ali (Khamenei) est renversé
=========================================================================

::

    Cette année est l'année du sang/Seyed Ali (Khamenei) est renversé


Source: https://masthead.social/@NaderTeyf/109406257963502259

Il y a des quartiers de #Téhéran où la répression aussi féroce soit-elle
n'a pas dissuadé les gens à sortir et à manifester pratiquement tout le
temps contre le régime en #Iran.

La cité Ekbatan en est un, le quartier Tchitgar en est un autre où ce
soir encore les gens scandent: "Cette année est l'année du sang/Seyed Ali (Khamenei) est renversé."
#Mahsa_Amini



Le sang qui coule dans nos veines est bu par notre Guide suprême !
======================================================================

::

    Le sang qui coule dans nos veines est bu par notre Guide suprême !


«LE SANG qui coule dans nos veines est bu par notre Guide suprême ! »,
scandent ce mardi 15 novembre 2022 des étudiantes de l’université des Sciences
et de la Culture de Téhéran, qui dénoncent ainsi la répression sanglante
ordonnée par l’ayatollah Khamenei, + haute autorité en #Iran



Foulard ou pas c'est mon choix
=================================

::

    Foulard ou pas c'est mon choix



Down with dictator
=====================

::

    Down with dictator



Le gouvernement d'Iran, tue les jeunes et les enfants
==========================================================

::

    Le gouvernement d'Iran, tue les jeunes et les enfants


République islamique, on veut pas on veut pas
================================================

::

    République islamique, on veut pas on veut pas


La Liberté pour l'Iran
==========================

::

    La Liberté pour l'Iran

Laïcité pour l'Iran
======================

::

    Laïcité pour l'Iran


Libérez, Les prisonniers, politiques
=======================================

::

    Libérez, Les prisonniers, politiques


Zahedan  kurdistan le cœur battant de l'Iran
=============================================

::

    Zahedan  kurdistan le cœur battant de l'Iran


Libérez Toumadj Salehi
=========================

::

    Libérez Toumadj Salehi


Libérez Saman
================

::

    Libérez Saman


Liberez Hossein Ronaghi
===========================

::

    Libérez Hossein Ronaghi


Solidarité,  internationale
==============================

::

    Solidarité,  internationale


Solidarité avec l'Iran
=========================

::

    Solidarité avec l'Iran


So so so solidarité avec les femmes du monde entier
=========================================================

::

    So so so solidarité avec les femmes du monde entier


Iran Ukraine solidarité
===========================

::

    Iran Ukraine solidarité


République meurtrière, on veut pas on veut pas
=================================================

::

    République meurtrière, on veut pas on veut pas


Expulsez l'ambassadeur de l'Iran
=================================

::

    Expulsez l'ambassadeur de l'Iran


Negociez pas avec les mollahs
================================

::

    Negociez pas avec les mollahs


République Islamique, il est temps de partir
===============================================

::

    République Islamique, il est temps de partir



Même si on est loin on vous soutient
===========================================

::

    Même si on est loin on vous soutient

.. _slogan_bloody_november:

Cette année est celle du sang, Seyed-Ali [Khamenei] sera renversé! (#NovembreSanglant #BloodyNovember)
========================================================================================================

::

    Cette année est celle du sang, Seyed-Ali [Khamenei] sera renversé!


UNE FOULE IMPORTANTE est réunie ce mardi 15 novembre devant la station de
métro Shariati, dans le nord de Téhéran, scandant à l’unisson :
« Cette année est celle du sang, Seyed-Ali [Khamenei] sera renversé! »,
pour les 3 ans des tueries de novembre 2019 (des centaines de morts).

Bande son ... C'est une alerte rouge. Le temps de la #Révolution est arrivé
==============================================================================

- https://masthead.social/@NaderTeyf/109353096787956192

Les initiatives populaires dans la lutte contre le régime des mollahs
ne manquent pas en #Iran.

Une bande-son passe en ce moment dans plusieurs villes.
On l'a entendue ce matin dans le quartier Cheikh Bahaï de #Téhéran.

Une voix de femme dit ceci::

    L'injustice est flagrante et l'oppresseur commet encore des crimes.
    Compatriote, si tu ne te lèves pas aujourd'hui, l'oppression frappera
    aussi à ta porte.

C'est une alerte rouge. Le temps de la #Révolution est arrivé." #Mahsa_Amini



