.. index::
   ! Femme, Vie, Liberté
   pair: Zan, زن,; Femme
   pair: Zendegi, زندگی ; Vie
   pair: Âzâdi, آزادی  ; Liberté
   ! Jin Jîyan, Azadî (kurde)
   pair: Jin ; Femme
   pair: Jîyan ; Vie
   ! Woman, Life, Freedom (anglais)
   ! FemmeVieLiberte
   ! ZanZendegiÂzâdi
   ! JinJiyanAzadî
   ! WomanLifeFreedom

.. _femmes_vie_liberte:
.. _femme_vie_liberte:
.. _jin_jiyan_azadi:

====================================================================================================================
🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 ) => Femme, Vie, Liberté
====================================================================================================================

- https://www.mediapart.fr/journal/international/290922/iran-femmes-vie-liberte-c-est-un-projet-politique
- https://www.youtube.com/watch?v=KKVqOaOL_YA (Iran : « Femmes, vie, liberté, c'est un projet politique »)


🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)
🇪🇸 Mujer Vida Libertad (Espagnol)

📣 Portons la voix de l'Iran 📣
#mahsa_amini #JinaMahsaAmini #Grenoble #NousSommesLeursVoix  #OpIran #مه#ساامینی

#mahsa_amini #JinaMahsaAmini #NousSommesLeursVoix #IranProtests   #مه#ساامینی
##NikaShakarami #NagihanAkarsel


.. _jin_jiyan_azadi_ref:

🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî  #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
========================================================

**Jin Jîyan, Azadî (Kurde)**

.. figure:: images/jin_jiyan_azadi.png
   :align: center
   :width: 500

Historique
------------

.. toctree::
   :maxdepth: 3

   historique/historique


.. _woman_life_freedom:

.. _zan_zendegi_azadi:

🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan) #ZanZendegiÂzâdi 🇮🇷
==============================================================================

- :ref:`femme_vie_liberte_2022_09_24`


**Zan, زن, Zendegi, زندگی, Âzâdi, آزادی  (Persan,  🇮🇷)**

.. figure:: images/tissu_avec_zan_zendegi_azadi_en_persan.png
   :align: center
   :width: 500


🇫🇷 Femme, Vie, Liberté  #FemmeVieLiberte (Français,  🇫🇷)
================================================================

**Femme, Vie, Liberté**


.. _donna_vita_liberta:

🇮🇹  Donna, Vita, Libertà (Italien, 🇮🇹 )
================================================

Donna, Vita, Libertà = femme, vie, liberté

ROME. Manifestation de solidarité avec la révolte populaire en Iran

#EndIranRegime #LetUsTalk #JînaAmînî #TwitterKurds #مهسا_امینی #MahsaAmini
#opiran #IranRevolution #Rojhilat #Kurdistan #JinJiyanAzadi


.. figure:: images/donna_vita_liberta.png
   :align: center
   :width: 500

   https://twitter.com/KurdistanAu/status/1576154754299764738?s=20&t=3LM6735AGlXde8hoYDbH8w


🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
================================================================

**Woman, Life, Freedom (Anglais)**


La gare centrale de la ville de Rotterdam aux Pays-Bas

#JinaMahsaAmini
#JinJiyanAzadi (#FemmeVieLiberte)
#TwitterKurds

#ژن_ژیان_ئازادی
#زن_زندگی_آزادی

#مهسا_امینی #MahsaAmini

.. figure:: images/zan_zendegi_azadi.png
   :align: center
   :width: 500

   https://twitter.com/KurdistanAu/status/1575564442699706368?s=20&t=yyACzU44T9RtmBFNsH4AsA

En hébreu et arabe à Jérusalem
===================================

.. figure:: images/femme_vie_liberte_hebreu_jerusalem.png
   :align: center

