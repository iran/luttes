.. index::
   pair: Historique ; Femme Vie Liberté

.. _historique_femmes_vie_liberte:

=================================================================
**Historique** (origine kurde du slogan **Femme Vie Liberté**)
=================================================================


Commentaire de elifxeyal
============================

- https://twitter.com/elifxeyal/status/1576507905926664193?s=20&t=kZvWYZXPWehuZKBJTu3cyw


.. figure:: images/elif_sarican.png
   :align: center

   https://twitter.com/elifxeyal/status/1576507905926664193?s=20&t=kZvWYZXPWehuZKBJTu3cyw

Dear Sisters,

When first declared on **8th March 2006**, the Kurdish Women’s Freedom Movement
always intended **Jin Jîyan Azadî** to be an universal slogan of active struggle
for women everywhere. 🧵 (1)


There is beauty in its spread and its translation into many languages
however, **the roots of Jin Jîyan Azadî must be defended** as a call to
action for women to mobilise and come alive against colonialism and
patriarchal capitalism in Kurdistan and beyond.


Its a political ideology for a free, democratic and ecological life.

Its the resurrection of Kurdish women and society. Its the call to action
for World Women’s Democratic Confederalism.

While chanting Jin Jîyan Azadî on the streets or on social media **it
must be against all colonial forces from The Islamic Republic of Iran to
the Turkish State & beyond**.

Long Live International Solidarity ✌🏽✊🏽♥️

Follow @Womenweaving2 for info on the Berlin conference, 5/6Nov.


Rendre à Mahsa Amini son vrai prénom **Jina** et aux femmes kurdes la maternité du slogan **Femme, Vie, Liberté**
=======================================================================================================================

- :ref:`vrai_prenom_jina_2022_10_04`
- :ref:`vrai_prenom_jina_2022_10_03`
