.. index::
   ! Mort au dictateur
   ! MarG bar dictator (Persan)
   ! Merg ber dîktator (kurde)

.. _mort_au_dictateur:

============================================
📣  Mort au dictateur (français,  🇫🇷 )
============================================


**Mort au dictateur (français,  🇫🇷)**


.. _marg_bar_dictator:

🇮🇷 MarG bar dictator (Persan,  🇮🇷)
========================================

**MarG bar dictator**  مرگ بر دیکتاتور


.. _merg_ber_dictator:

🏴󠁩󠁲󠀱󠀶 Merg ber dîktator (kurde, 🏴󠁩󠁲󠀱󠀶 )
==========================================

**Merg ber dîktator**
