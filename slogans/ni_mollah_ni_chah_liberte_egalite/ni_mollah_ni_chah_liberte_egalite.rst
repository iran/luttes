.. index::
   ! Femme, Vie, Liberté

.. _ni_mollah_ni_chah:

====================================================================================================================
**Ni Mollah Ni Chah, Liberté Égalité !**
====================================================================================================================

- :ref:iran_cinquieme_mois_2023_01_25`


::

    Les grèves revendicatives des travailleurs continuent, mais n’apparaissent pas
    politisées, même si des communiqués de solidarité viennent des activistes et
    de syndicats embryonnaires. Il faut noter la présence des services de sécurité
    dans l’enceinte des usines et des grandes unités du travail, surtout dans le
    secteur pétrochimique, celle-ci s’est renforcée, ainsi qu’une première vague
    d’arrestations : il y a trois mois 350 arrestations en une journée ont touché
    les militants de ce secteur à la suite de la première grève ouvertement politique.

    D’un autre côté, il semble que pour le moment, du fait de la militarisation de
    la répression au Kurdistan, le centre de gravité de la protestation
    s’est déplacé vers la province de Sistan-Baloutchistan, où de nouveaux slogans
    liant les revendications sociales à la question des libertés se sont multipliés
    dans les grandes manifestations de vendredi.
    Le plus connu étant **Ni Mollah Ni Chah, Liberté Égalité !**
