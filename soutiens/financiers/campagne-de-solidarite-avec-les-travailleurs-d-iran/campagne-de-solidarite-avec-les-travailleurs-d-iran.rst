.. index::
   pair: Solidarité ; Campagne

.. _campagne_solidarite:

===============================================================================
**Campagne de solidarité avec les travailleurs d'Iran (Solidarité Iran)**
===============================================================================

- http://www.iran-echo.com/soutien_fr.html

Le meurtre de Jina Mahsa Amini le 16 septembre 2022, assassinée par
la police des mœurs, pour un voile mal ajusté a déclenché un soulèvement
populaire inédit par son ampleur, sa profondeur et sa durée.

En moins de 48h, le mot d'ordre "Femme, Vie, Liberté" s'est propagé dans
tous le pays, et dans le monde entier, comme une traînée de poudre.

Il marque une rupture radicale avec la République Islamique.

Les manifestantes et manifestants réclament inlassablement la chute de
la République Islamique. Ce mouvement de contestation radical rassemble
les femmes, la jeunesse, les minorités nationales et les travailleurs
dans un rejet radical du régime capitaliste, théocratique et misogyne
des mollahs. Le soulèvement s'ancre dans la durée et touche plus de 160
villes petites et grandes. Face à cela, le pouvoir a enclenché une
répression féroce.

Plus de 500 morts, des milliers de blessés, plus de 19000 prisonniers et
de disparus, des enlèvements, des tortures et des viols...la barbarie
de ce régime n'a pas de limite. Le pouvoir judiciaire prononce des
condamnations lourdes à l'encontre des manifestants, certains sont
condamnés à mort et exécutés dans la foulée.

Dans ce contexte, l'entrée en lutte de la classe ouvrière constitue un
enjeu central dans l'issue de l'affrontement en cours.
Les travailleurs qui se lancent dans des actions grévistes sont sévèrement
réprimés.

Arrêtés, ils sont aussi licenciés, doivent faire face aux tortures et à
des frais de justice exorbitants.
Afin de les soutenir et de contribuer à la prise en charge des frais de
justice et d'avocats, Solidarité Socialiste avec les Travailleurs en Iran
lance une campagne de solidarité financière. Il s'agit de développer une
solidarité internationaliste concrète avec celles et ceux qui luttent
courageusement dans des conditions difficiles.



Pour envoyer des dons : merci d'indiquer la mention "Solidarité Iran"

Les dons peuvent être versés sous forme de chèques (en euros seulement
et payables en France), par virements bancaires directement au compte de
notre association, et via Helloasso ou PayPal.

Chèques
==========

Les chèques en euros seulement et payables en France à l’ordre d’ESSF
doivent être envoyés à :

ESSF
2, rue Richard-Lenoir
93100 Montreuil
France


Banque
=============

Crédit lyonnais
Agence de la Croix-de-Chavaux (00525)
10 boulevard Chanzy
93100 Montreuil
France ESSF, compte n° 445757C

Références bancaires nationales (RIB) :
Banque : 30002
Indicatif : 00525
N° de compte : 0000445757C
Clé : 12
Compte au nom de : ESSF

Coordonnées bancaires internationales :
IBAN : FR85 3000 2005 2500 0044 5757 C12
BIC / SWIFT : CRLYFRPP
Compte au nom de : ESSF

Paypal : vous pouvez aussi transférer vos dons via Paypal
==============================================================

https://www.paypal.com/donate/?token=IWfKmLSeEC_VyeQ_b-tD3p8-njtO5sDM8_7gr1ssbabSeCXVnjw4UpQStlGcO7YJgKBBvm&country.x=FR&locale.x=FR]


HelloAsso
==============

HelloAsso : vous pouvez aussi transférer vos dons via HelloAsso Faire un don à Europe solidaire sans frontières

Faire un don à `Europe solidaire sans frontières <https://www.helloasso.com/associations/europe-solidaire-sans-frontieres/formulaires/1/widget>`_
(https://www.helloasso.com/associations/europe-solidaire-sans-frontieres/formulaires/1/widget)





