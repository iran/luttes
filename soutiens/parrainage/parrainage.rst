.. index::
   ! Parrainage

.. _parrainage:

=====================
Parrainage
=====================


ACTION CITOYENNE.Face au manque de réaction des autorités politiques
face à la répression sanglante en #Iran, + de 100 Parlementaires (députés,
sénateurs) français et européens ont décidé de parrainer un manifestant
condamné à (ou qui risque) la peine de mort.

Contact @CArdakani

