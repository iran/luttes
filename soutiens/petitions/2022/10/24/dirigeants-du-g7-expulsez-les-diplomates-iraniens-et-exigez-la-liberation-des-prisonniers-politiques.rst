.. index::
   pair: Petition ; Dirigeants du G7, expulsez les diplomates iraniens et exigez la libération des prisonniers

.. _petitions_2022_11_24_liberez_prisonniers:

=============================================================================================
Dirigeants du G7, expulsez les diplomates iraniens et exigez la libération des prisonniers
=============================================================================================


- https://www.change.org/p/dirigeants-du-g7-expulsez-les-diplomates-iraniens-et-exigez-la-lib%C3%A9ration-des-prisonniers-politiques


Aux honorables ministres des Affaires étrangères du Royaume-Uni, de la
France, de l’Allemagne, de l’Italie, du Japon, du Canada et des États-Unis,
soit le groupe des nations du G7 :

Par la présente, nous, les membres fondateurs d’Iranians for Justice
and Human Rights, ainsi que d’autres cosignataires, exprimons notre plus
profonde inquiétude concernant les derniers développements survenus
en Iran au cours des cinq dernières semaines.

La brutale répression et les actes de violence perpétrés par de nombreuses
forces du régime islamique iranien en réaction aux manifestations qui ont
suivi l’assassinat de Mahsa Amini par le régime islamique ont entraîné
d’innombrables décès, blessures et incarcérations.

Le régime islamique iranien a de toute évidence l’intention de poursuivre
et d’intensifier les actes de violence inhumaine contre ses propres citoyens.

Le 15 octobre, des rapports troublants et des preuves vidéo ont fait
état de fusillades et d’un incendie dans la prison d’Evin, lieu tristement
célèbre où sont incarcérés de nombreux prisonniers.

Nous craignons que la vie de nombreux prisonniers d’opinion (y compris,
mais sans s’y limiter, des prisonniers politiques, des activistes civiques,
des artistes, des représentants de groupes ethniques et religieux et
des syndicalistes) et de citoyens ayant la double nationalité détenus
dans cette prison et dans d’autres prisons à travers l’Iran soit gravement menacée.

Par conséquent, nous vous demandons :

(1) d’exiger avec force et sans équivoque la libération de tous les
    prisonniers d’opinion en Iran ; et

(2) de désigner sans délai les ambassadeurs ou autres représentants du
    régime islamique en poste dans votre pays, que ce soit dans les
    ambassades ou dans les autres institutions internationales, comme
    personæ non gratæ et d’ordonner leur expulsion en signe de protestation
    contre le traitement illégal et inhumain des manifestants en Iran.

