.. index::
   pair: Petition ; Expulsion de l'ambassadeur d'Iran et la fermeture de l’ambassade d'Iran en France

.. _petition_2022_11_27_expulsion_ambassadeur:

=============================================================================================
**Expulsion de l'ambassadeur d'Iran et la fermeture de l’ambassade d'Iran en France**
=============================================================================================

- https://petitions.assemblee-nationale.fr/initiatives/i-1095?locale=fr

Le régime de l’Iran dévoile actuellement au monde entier son ignominie.

Nul ne peut plus ignorer que ce régime islamique est sanguinaire.
Au prétexte fallacieux de la religion et dans un silence politique mortel,
ils exécutent sauvagement leurs propres citoyens, jeunes filles, femmes,
hommes, et même enfants, au beau milieu de la rue et sans pitié.

Les iraniens et les iraniennes voulaient pacifiquement faire entendre
leurs voix. Mais leur patience a atteint ses limites, ils n’en peuvent
plus et les manifestations sont devenues révolution.

Les iraniens rejettent ce régime et ses lois injustes.

Ils ne veulent plus du Régime Islamique pour l’Iran.

Ces femmes et ces hommes, écœurés du meurtre de Mahsa Amini qui avait
pour seul tort d’avoir mal ajusté son voile - aux yeux de la police des mœurs -
ont demandé à ce que les femmes ne soient plus forcées à porter le voile,
à ce qu’elles puissent exister dans l’espace public en tant qu’êtres libres,
ainsi que dans l’espace social et dans l’espace juridique.

Il est inconcevable que la France, qui défend sa laïcité, son héritage
révolutionnaire, son égalité et sa parité, la France qui dit placer la
protection des femmes contre les violences dans ses priorités, puisse à
ce jour maintenir des relations diplomatiques avec la République Islamique
actuellement en place en Iran.

Ainsi, pour apporter notre soutien aux courageuses jeunes filles iraniennes
dans leur combat, pour défendre les droits de l’Homme et pour condamner
le meurtre de Mahsa Amini et des 304 autres personnes, dont 47 enfants,
ainsi que plus de 14000 arrestations (au 9 novembre 2022 selon les
statistiques de l'Organisation mondiale des droits de l'homme "Amnesty International"
et l’ONU), nous demandons :

1. que l’ambassadeur d’Iran en France soit expulsé,
2. que l’ambassade soit fermée, aussi longtemps que le régime qu’elle
   représente au cœur de la France, porte ces valeurs d’inégalités,
   d’obscurantisme et de violence.
