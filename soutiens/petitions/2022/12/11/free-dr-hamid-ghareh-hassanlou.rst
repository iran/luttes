.. index::
   ! Hamid Ghareh-Hassanlou

.. _hamid_ghareh_hassanlou:

================================================
2022-12-11 Free Dr Hamid Ghareh-Hassanlou
================================================

- https://www.change.org/p/free-dr-hamid-ghareh-hassanlou?

.. figure:: images/hamid_ghareh_hassanlou.png
   :align: center

Dear all,

We are writing this petition to seek your urgent help regarding the dire
situation of Dr Hamid Ghare-Hassanlou and his wife Mrs Farzaneh Ghare-Hassanlou..

Dr. Hamid Ghare Hasanloo (Radiologist) and his wife Farzaneh Ghare Hasanloo
were arrested in Karaj Iran 5 weeks ago by Islamic republic guards.

Their crime was attending the funeral of Hadis Najafi young, a girl who
 was killed in a peaceful protest in Karaj a few days earlier.

Their home was raided by secret police and they were arrested following
a brutal beating witnessed by their teenage daughters.

They have been tortured and beaten in order to obtain false confessions
of crimes they have not committed, a routine practice by the security forces in Iran.

The relentless beating has resulted in multiple rib fractures for
Dr Ghareh-Hassanlou exacerbating his chronic heart and lung condition.


Dr Ghare-Hassanlou is a compassionate physician with more than 26 years
of experience as a radiologist engaged in charitable activities such
as building schools in poor and underserved communities of Iran.

His kind-hearted and generous wife has been equally active in humanitarian
activities. 

The City prosecutor office has refused to allow the accused individuals
to retain their own lawyers, using “national security” as a pretence
and soon declared them to be the enemy of the government punishable by death.

This unjust process has also been influenced by Hasanloo’s affiliations
with Sufism which is apparently now a subject for discrimination and prosecution.

We urge you to employ every possible measure to save the lives of these
innocent individuals, be their voices so we can collectively help
them gain their well-deserved freedom.

We thank you in advance for your urgent attention to this matter.
