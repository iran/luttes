.. index::
   ! Amir Nasr-Azadani

.. _amir_nasr_azadani:

==========================================================
2022-12-19 Stoppez l'exécution d'Amir Nasr-Azadani
==========================================================

- https://www.change.org/p/stoppez-l-ex%C3%A9cution-d-amir-nasr-azadani-fifacom-un

.. figure:: images/amir_nasr_azadani.png
   :align: center

   Amir Nasr-Azadani

Le footballeur iranien Amir Nasr-Azadani pourrait être exécuté.

Selon "Iranwire", Amir Nasr-Azadani (26 ans) a été condamné à mort pour
"trahison" après avoir manifesté son soutien aux droits humains et
défendu les droits des femmes dans son pays.

Le syndicat international des joueurs Fifpro dénonce l'affaire : "Nous
sommes solidaires d'Amir et demandons que sa peine soit immédiatement levée".

Signez cette pétition pour que le régime iranien mette fin à l'exécution
du footballeur Amir Nasr-Azadani.
