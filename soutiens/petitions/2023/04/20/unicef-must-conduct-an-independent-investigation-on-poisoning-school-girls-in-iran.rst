.. index::
   pair: Pétion; Unicef (2023-04-20)

.. _petition_unicef_2023_04_20:

=====================================================================================================
2023-04-20 **UNICEF Must Conduct an Independent Investigation on Poisoning School Girls in Iran**
=====================================================================================================

- https://www.change.org/p/unicef-must-conduct-an-independent-investigation-on-poisoning-school-girls-in-iran

.. figure:: images/fillette_avec_masque.png
   :align: center


Hundreds of girls have been poisoned by gas in schools in over 15 cities
in Iran.

The Iranian government has refused to shut down schools to investigate
the mass poisoning of girls and some reports shows the head mistresses
have stopped girls from leaving school.

So far the government has not done a thorough investigation to find out
the root of these attacks or the physical harm it can cause, nor they
have provided security for these schools to protect the students.

**School girls have had a significant role in Woman Life Freedom movement
and the recent women uprising in Iran**.

They became very vocal  demanding their basic rights and showed their
discontent against compulsory hijab.

The state cracked down on schools and beat many school girls inside the
schools, and sent many to correction behaviour centres.

Many of us believe the recent poisoning is in connection with the school
girls activities against the state.

The fact that the Iranian government has not taken any actions to protect
the students implies their involvement with the mass poisoning.


According to UNICEF

Every child has rights, whatever their ethnicity, gender, religion, language,
abilities or any other status.

The United Nations Convention on the Rights of the Child (UNCRC) forms
the basis for all our work, including our campaigning.

**As the only organisation working for children recognised by the Convention,
we are passionate about protecting the rights of every child**.

Articles 28 and 29 focus on a child’s right to an education and on the
quality and content of education.

Article 28 says that “State Parties recognise the right of children to
education” and “should take all appropriate measures to ensure that school
discipline is administered in a manner consistent with the child’s human dignity.


UNICEF has the responsibility to keep ALL CHILDREN safe regardless of
their gender, sex and religion.
Iranian girls are not safe in the very place they are supposed to be kept
safe, schools.

The Iranian government has failed to protect them; therefore we are
demanding UNICEF to step in and conduct a thorough and independent
investigation on the nature of gas poisoning and the physical and
psychological harm they can cause as well as finding out who is behind
poisoning girls.

