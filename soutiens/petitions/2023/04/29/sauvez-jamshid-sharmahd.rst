
=================================================================
2023-04-29 **Sauvez Jamshid Sharmahd !**
=================================================================

- https://www.change.org/p/stop-l-ex%C3%A9cution-de-mohana-kameli-lib%C3%A9rez-la


.. figure:: images/jamshid_sharmahd.png
   :align: center


Nous lançons un appel désespéré à la communauté internationale et vous
demandons de sauver une vie humaine.

La vie de Jamshid Sharmahd, citoyen allemand, retenu en otage par la
République islamique d'Iran depuis 18 mois, est en grave danger.

Lors d'un voyage entre l'Allemagne et l'Inde, Jamshid Sharmahd a dû faire
une escale de trois jours à l'aéroport de Dubaï en août 2020.

Là, il a perdu le contact avec sa famille.

La famille a appris quelques jours plus tard par les médias d'État iraniens
que M. Sharmahd avait été enlevé par les services secrets de la République
islamique et emmené en Iran.

Jamshid Sharmahd est un militant politique qui n'a cessé de critiquer
la République islamique d'Iran.
Selon sa fille, Gazelle Sharmahd, son père est en isolement depuis 555
jours et n'a pas été en mesure de contacter sa famille, ni d'avoir accès
à un avocat indépendant ou de contacter sa famille et ses proches en Iran.

Le samedi 5 février, les médias d'État iraniens ont annoncé que son
procès commencerait le lendemain.
La famille n'a appris l'ouverture du procès que par les médias.

Le 6 février, le procès de Jamshid Sharmahd s'est ouvert. Il doit être
considéré comme un spectacle politique, car il est partiellement diffusé
par les médias d'État iraniens.

La famille est extrêmement inquiète : Jamshid Sharmahd est gravement
émacié et "n'est plus lui-même".
Il comparaît devant le Tribunal révolutionnaire 15, présidé par le juge
Abolqasem Salavati, connu comme le "juge de la mort".

Il est accusé de "corruption sur terre", ce qui signifie la peine de mort
dans le système juridique médiéval de la République islamique d'Iran.

Le juge Salavati est connu pour imposer la peine de mort. La dernière
victime était Ruhollah Zam. Le journaliste Zam a également été enlevé
en Iran et pendu dans ce pays en décembre 2020.

Il est de la plus haute importance que les gouvernements du monde entier
agissent immédiatement pour empêcher l'assassinat planifié de M. Sharmahd.

Nous demandons instamment que cette affaire soit considérée comme une
priorité absolue, qu'une équipe de gestion de crise soit mise en place
et que les gouvernements du monde entier épuisent toutes les voies politiques
et diplomatiques pour faire pression sur la République islamique d'Iran.

Une condamnation générale de la peine de mort et un appel au respect des
droits de l'homme ne suffisent pas.
Nous vous demandons instamment de prendre des mesures immédiates, sérieuses
et durables pour sauver la vie de ce citoyen allemand de 66 ans et de sa
famille de cet acte horrible.

Avec nos salutations distinguées
