
=================================================================
2023-04-29 **Stop l'exécution de Mohana Kameli ! Libérez-la !**
=================================================================

- https://www.change.org/p/stop-l-ex%C3%A9cution-de-mohana-kameli-lib%C3%A9rez-la


Mohana Kameli est une Iranienne de 24 ans qui a été la voix de son peuple
lors des récentes manifestations en Iran, contre 44 ans d'oppression et
d'injustice sociopolitique, culturelle et économique de la part du régime
des Mollahs.

Elle a manifesté dans les rues de Téhéran et a inscrit sur les murs de
la ville des slogans contre le dictateur.

Lors d'une manifestation en septembre 2022, elle a été gravement blessée
au bas de la jambe par une arme à feu des gardiens de la révolution et,
plus tard, elle a été renversée par un motocycliste, ce qui lui a causé
de graves blessures à la tête et lui a cassé le bras.

Néanmoins, Mohana a continué à participer activement aux manifestations
à Téhéran jusqu'au 12 décembre 2022, date à laquelle les agents des
gardiens de la révolution, après s'être fait passer pour des employés
de la centrale électrique, ont fait irruption de manière violente et
brutale dans la maison sans aucune explication, ont kidnappé Mohana
et ont disparu.

La mère de Mohana, malade, est décédée trois jours plus tard des suites
du choc.
Son père était déjà décédé en 2018, il y a quatre ans.

Environ un mois plus tard, les proches ont appris que Mohana se trouvait
dans la prison de "Waramin", une petite ville près de Téhéran, et ont pu
lui rendre visite.

Malgré ses blessures, Mohana a été torturée physiquement et mentalement
à plusieurs reprises pendant son séjour en prison.
Après sa visite, ses proches ont fait part de l'état de santé catastrophique
de Mohana sur les réseaux sociaux.

Sa blessure ouverte par balle était gravement enflammée et elle souffrait
de douleurs intenses et de poussées de fièvre.
Après la publication de son état de santé critique en prison, les gardiens
ont transféré Mohana à l'hôpital Taleghani, l'hôpital universitaire de
l'Université Shahid Beheshti, le 12 janvier 2023.

La nouvelle de son transfert à l'hôpital a suscité le désir de nombreuses
personnes à Téhéran de rendre visite à Mohana Kameli à l'hôpital.
Cependant, les forces de sécurité ont empêché les visiteurs et ont ramené
Mohana en prison après deux opérations, qui ont eu lieu les 15 et 16 janvier
et ont duré au total plus de 10 heures, contre l'avis des médecins traitants,
qui estimaient que le processus de guérison était en danger.

Pendant son traitement à l'hôpital, ses mains ont été menottées au lit
malgré son plâtre.

Le soutien apporté à Mohana par le célèbre footballeur iranien Ali Karimi
a contribué à la popularité de Mohana auprès du public, de sorte que le
régime corrompu des Mulla s'est inquiété et a tenté de tromper le public
et de nier l'existence d'une personne nommée Mohana Kameli en construisant
et en distribuant de fausses vidéos. L'intention du régime des Mulla
dans cette tromperie est de cacher au public le nombre réel de victimes
ou leurs exécutions.

Dans l'ombre de la propagation du doute et de la distraction en public,
Mohana a été condamnée à mort pour guerre contre Dieu ("Muharebe") lors
d'un procès d'urgence à huis clos.

Mohana n'a pas été autorisée à exercer son droit à un avocat.

La condamnation à mort a été prononcée le 21 janvier.

Mohana a maintenant besoin de notre aide de toute urgence.

Lorsque sa mère, inquiète, lui a demandé ce qui se passerait si elle
était arrêtée, elle a répondu : "J'ai des milliers de sœurs et de frères
qui me sortiront de là. "

Aidez-nous à faire de l'espoir de Mohana une réalité !

Soyez ses sœurs et ses frères ! Bien sûr, cette pétition concerne en
premier lieu les Iraniens, mais elle est également soutenue par toutes
les personnes de la communauté mondiale qui aiment les droits,
indépendamment de leur sexe, de la couleur de leur peau, de leur
appartenance ethnique, de leur nation ou de leur religion !

Ce n'est qu'ensemble que nous pourrons prendre position contre l'arbitraire
du régime des mollahs.

Faites comprendre au dictateur iranien que non seulement les Iraniens,
mais aussi la communauté internationale, observent attentivement ce que
les dirigeants iraniens font subir à leur propre peuple.

Aidez-nous à sauver Mohana de la prison et à lui redonner une vie normale !

Nous vous remercions.

Les parents et amis de Mohana
