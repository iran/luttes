.. index::
   pair: Pétition; Appel pour soutenir le combat des femmes et des peuples d’Iran pour la démocratie (2023-05-19)


.. _petition_fr_2023_05_19:

=====================================================================================================
2023-05-19 **Appel pour soutenir le combat des femmes et des peuples d’Iran pour la démocratie**
=====================================================================================================

- https://www.change.org/p/appel-pour-soutenir-le-combat-des-femmes-et-des-peuples-d-iran-pour-la-d%C3%A9mocratie


Lettre ouverte au Président de la République,

à la Première Ministre et à la Ministre des Affaires Etrangères

Depuis l’assassinat de Jina Mahsa Amini, jeune femme Kurde par la brigade
de moralité en septembre 2022, l’Iran est secoué par un mouvement de révolte
au premier rang duquel se trouvent les femmes et la jeunesse iranienne.

Pour mettre fin à la dictature théocratique qui opprime son peuple depuis
déjà 44 ans et accéder à la démocratie, cette révolte a besoin du plus
vaste soutien international, de toutes les forces féministes, démocratiques,
syndicales, etc.

Il est plus que temps : les gouvernements occidentaux doivent prendre leurs
responsabilités et dénonçer la répression sanglante qui s’abat sur ce
mouvement social.

Nous demandons donc au gouvernement français de :

- Briser le silence sur ce qui se passe en Iran
- Convoquer l’ambassadeur d’Iran et lui faire part de sa préoccupation
  concernant les violences utilisées contre les manifestant·es et les
  arrestations systématiques des opposants-tes, les exécutions sommaires
- Ne plus céder à la « Diplomatie d’otage » pratiquée depuis des années par l’Etat iranien
- Réduire au minimum les relations diplomatiques avec le régime islamique en Iran
- Demander la libération des prisonniers politiques, l’arrêt immédiat
  des exécutions et l'annulation des peines existantes
- Utiliser son poids et sa position au sein du Conseil de sécurité des
  Nations unies pour faire pression sur le gouvernement iranien afin de
  l’obliger à accepter l'entrée en Iran de Javaid Rehman, Rapporteur spécial
  des Nations Unies sur les droits de l'Homme en Iran ainsi que de donner
  suite à la mission d’enquête du Conseil des droits de l’Homme sur la
  répression en Iran
- Utiliser son poids et sa position au sein de l’Union européenne pour
  mettre « les Gardiens de la révolution » sur la liste européenne des
  organisations terroristes
- Sanctionner les dirigeants du régime et bloquer leurs biens et capitaux
  ainsi que ceux des dignitaires du régime basés en France
- Délivrer des laisser-passer ou des visas, reconnaître sans condition
  le statut de réfugié aux iranien·nes qui sont obligé-es de quitter
  l’Iran pour fuir la répression dont ils sont victimes.

1ers signataires :

ANSARI Irène, Présidente de la Ligue des Femmes Iraniennes pour la Démocratie (LFID)

VERGIAT Marie-Christine, Vice-Présidente de la Ligue des droits de l'Homme (LDH)

ZOUGHEBI Henriette, conseillère régionale honoraire et Présidente de la,Coopérative des Idées 93

et
ASSASSI Eliane, sénatrice de Seine-Saint-Denis

CHOQUET Catherine, Présidente de la Fédération 93 de la LDH

Collectif Attac Sud

DAENINCKX Didier, écrivain

DELARBRE Jean-Michel, membre du Comité national de la LDH

DELLAC Dominique, Vice-Présidente du Conseil départemental de Seine-Saint-Denis

GAY Fabien, sénateur de Seine-Saint-Denis

GARRIDO Raquel, députée de Seine-Saint-Denis

GUEGUEN CARUSO Rozenn, militante de la LDH, membre du Bureau fédéral Ile de France

KELOUA HACHI Fatiha, députée de Seine-Saint-Denis

LADJALI Cécile, autrice

LORCA Alexis, Maire-adjoint de Montreuil

MADJIDI Maryam, autrice, scénariste et réalisatrice

MENHOUDJ Halima, Maire-adjointe de Montreuil

MEYER Gilles, citoyen, militant de la LDH

PEU Stéphane, député de Seine-Saint-Denis

PRIGL d'ONDEL Stéphanie, citoyenne

ROUSSILLON Marie-Edith, metteur en scène et directrice de production -  spectacles vivants.

