.. index::
   pair: Syndicats ; 2023-05-01

.. _soutiens_syndicats_2023_05_01:

==============================================================================================================================
2023-05-01 **Message du Collectif syndical français aux ‎travailleuses et travailleurs d’Iran‎ à l’occasion du 1er mai 2023**
==============================================================================================================================

.. figure:: images/logos_syndicats.png
   :align: center


Depuis plusieurs années, nos organisations syndicales CFDT, CGT, FSU, Solidaires,
UNSA, ‎soutiennent ensemble vos combats pour la défense de vos droits et
de vos libertés.

Nous ‎connaissons les conditions dans lesquelles vous menez cette lutte,
en particulier, en raison de la répression très forte qui est ‎exercée
par le pouvoir iranien, et les conditions de travail et de vie très
difficiles du ‎peuple iranien.‎ 

Le droit de constituer librement des organisations syndicales et de s’associer
dans des ‎organisations représentant les travailleuses et les travailleurs
qui soient réellement indépendantes du pouvoir, sont des aspirations
légitimes et pleinement reconnues par le droit ‎international.

Nous soutenons votre exigence que l’Etat iranien se soumette enfin à ce
droit fondamental, auquel il a souscrit en étant membre de l’Organisation
Internationale du Travail !‎ ‎‎

Le 30 avril 2022, à la veille de la journée internationale des travailleurs/euses,
un état de siège non déclaré régnait à Téhéran et d'autres grandes villes.

**Rasoul BODAGI, Jafar EBRAHIMI, Mohammad HABIBI et Ali Akbar BAGHANI, tous
membres de l’Association des enseignant.e.s ont été arrêtés**.

Ensuite Réza SHAHABI, Hassan SAÏDI et Davoud RAZAVI, membres du Syndicat
des Travailleurs/euses de la Régie du Transport de Téhéran et sa banlieue
(Vahed) ainsi que Keyvan MOHTADI et Anisha ASSADOLLAHI leurs traducteurs
ont été arrêtés.

Ils ont tous été condamnés à de lourdes peines d'emprisonnement sous des
allégations fallacieuses.

Leur seul "crime" est d'avoir voulu exercer leur activité syndicale conformément
au droit international régi par les Conventions fondamentales de l'OIT
dont le droit syndical est un élément central.

Notre Collectif syndical pour l’Iran suit avec une grande préoccupation
l’évolution de leurs conditions de détention, et en particulier les
conditions pénitentiaires inhumaines auxquelles ils sont soumis.

L’aggravation de l'état de santé de Réza SHAHABI et Davood RAZAVI nous
préoccupe sérieusement. Selon le médecin de la prison où ils sont détenus,
Il est urgent de transférer Réza SHAHABI dans un hôpital civil hors du
milieu carcéral pour une intervention chirurgicale urgente, mais les
autorités iraniennes refusent obstinément cela.

A l’occasion du 1er mai, journée internationale de solidarité, nous vous
renouvelons ‎notre soutien sans faille dans l’éprouvant combat que vous
menez pour la liberté de ‎toutes et tous, pour la dignité des travailleuses
et des travailleurs, pour le respect des ‎Droits Humains‎‎.

Nous  condamnons avec force cette répression insupportable.


Confédération française démocratique du travail (CFDT)
Confédération générale du travail (CGT)
Fédération syndicale unitaire (FSU)
Union syndicale Solidaires
Union nationale des syndicats autonomes (UNSA)
