"""
Utilisation de Jinja2 pour fabriquer les bons paramètres


A partir d'un texte , on fabrique 2 fichiers .rst, la différence entre les
2 étant juste l'emplacement des images (.. figure:: images/)

"""
from __future__ import print_function

import sys
from dataclasses import dataclass
import pendulum
from jinja2 import Environment, FileSystemLoader



@dataclass
class Options:
    """Les options

    --help
        pour avoir de l'aide

    --year
        l'année

    --week
        la semaine

    Exemple d'appels
    =================

    python fabrique.py --year 2023 --week 6
    python fabrique.py --year 2023 --week 5
    """

    def __init__(self):
        self.year: int = None
        self.week: int = None

    def __str__(self):
        chaine = f"{self.year=} {self.week=}"
        return chaine

def produce_write_rst_file(template, *, options: Options, ref: str, path: str):
    """
    ref: str
        in ["local", "list"]
    """

    file_name =  f"templates/evenements_{options.year}_{options.week}_{ref}.txt"
    output = template.render(
        path=path,
        ref=ref,
    )
    with open(file_name, "w") as file_evt:
        file_evt.write(output)



def trt_template(options: Options):
    """
    Les images sont stockées sous evenements/year/images
    """
    file_loader = FileSystemLoader("templates")
    env = Environment(loader=file_loader)
    file_template = f"evenements_{options.year}_{options.week}.txt"
    template = env.get_template(file_template)
    produce_write_rst_file(template, options=options, ref="local", path="images")
    produce_write_rst_file(template, options=options, ref="list", path=f"{options.year}/{options.week}/images")


if __name__ == "__main__":
    """Point d'entrée du script Python."""
    options = Options()
    options.current_day = pendulum.now().start_of("day")
    for i, argument in enumerate(sys.argv):
        # print("%d %s" % (i, argument))
        if argument in ["--year"]:
            options.year = str(sys.argv[i + 1])
        elif argument in ["--week"]:
            options.week = int(sys.argv[i + 1])
            options.week=f"{options.week:02}"
        elif argument in ["--help"]:
            print(Options.__doc__)
            exit(0)

    print(f"{options}")
    trt_template(options)
