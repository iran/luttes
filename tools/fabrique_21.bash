#!/bin/bash
set -x

python fabrique.py --year 2023 --week 21

cp templates/evenements_2023_21_* ../evenements/2023/21
