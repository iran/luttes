#!/bin/bash
set -x

python fabrique.py --year 2023 --week 22

cp templates/evenements_2023_22_* ../evenements/2023/22
