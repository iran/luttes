#!/bin/bash
set -x

python fabrique.py --year 2023 --week 23

cp templates/evenements_2023_23_* ../evenements/2023/23
