#!/bin/bash
set -x

python fabrique.py --year 2023 --week 24

cp templates/evenements_2023_24_* ../evenements/2023/24
