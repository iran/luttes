#!/bin/bash
set -x

python fabrique.py --year 2023 --week 25

cp templates/evenements_2023_25_* ../evenements/2023/25
