#!/bin/bash
set -x

python fabrique.py --year 2023 --week 26

cp templates/evenements_2023_26_* ../evenements/2023/26
