#!/bin/bash
# set the Personal Access Token GITLAB_TOKEN before
set -e

for id in `glab pipeline list --sort asc -P 3| awk '{print substr($3,2)}'| tail -n+2`
do
    glab pipeline delete $id
done
