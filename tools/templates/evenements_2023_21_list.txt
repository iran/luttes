
.. list in ["local", "list"]
.. 2023/21/images in ["images", "{year}/{week}/images"]


.. _sara_salemi_2023_05_26_list:

2023-05-26 **14e Congrès Fédération de l'Éducation, de la Recherche et de la Culture Motion - Soutien à Sara Sélami militante iranienne** 🇮🇷
=================================================================================================================================================

- https://www.ferc-cgt.org/14e-congres-motion-soutien-a-sara-selami-militante-iranienne
- :ref:`sarah_selami`

Pendant notre débat de congrès sur le respect des Droits humains, la
démocratie syndicale, la lutte contre toute forme de violence, pour la
fraternité et la solidarité internationale, les congressistes ont apporté
leur soutien plein et entier à la camarade iranienne, Sara Sélami, qui
a présenté le mouvement syndical et la situation socio-politique iranienne
au 53e congrès de la CGT.

Pour rappel : au 52e congrès le représentant du syndicat indépendant du
Transport de Téhéran, faisant partie de la délégation iranienne, une fois
rentré en Iran a été emprisonné, puis libéré et de nouveau emprisonné et
condamné à 6 ans de prison pour son activité syndicale.

Au 53e congrès, dans le cadre de la solidarité internationale, :ref:`Sara Sélami <sarah_selami_cgt_2023_03_30>`
est intervenue pour présenter la situation difficile de la société iranienne.
Elle a été soigneusement choisie par « l’association Solidarité socialiste
avec les travailleurs en Iran » qui collabore avec le collectif intersyndical
français de soutien aux travailleurs et travailleuses en Iran, dont la CGT est membre.

:ref:`Sara Sélami <sarah_selami>`, réfugiée politique, opposante active au régime islamique et
ancrée dans la lutte des classes a présenté la situation politique et sociale,
les activités des syndicats indépendants, du mouvement « Femmes, Vie, Liberté »
et de la répression féroce du régime contre les manifestant·es.

:ref:`Mais une partie de son intervention au 53e congrès mettant en cause la FSM
(Fédération Syndicale Mondiale) <sarah_selami_cgt_2023_03_30>` n’a pas plu à 5 organisations de la CGT
qui en sont adhérentes (Fédérations Chimie et Commerce, UD 13, 82 et 94).

Ces organisations ont envoyé un courrier au Secrétaire général de la FSM
demandant des « éléments de réponse circonstanciés » pour répondre aux
« calomnies » contre la FSM.
Elles précisent même que ces calomnies « ne peuvent rester impunies ».

**Ce courrier minable**, dénonçant la camarade iranienne dans un contexte
politique et social iranien où les opposant·es au régime islamique sont
traqué·es en Iran mais aussi à l’extérieur, ils et elles sont kidnapé·es
et ramené·es en Iran pour exécution (comme le binational irano-suédois
kidnappé en Turquie et exécuté par pendaison en Iran début mai 2023).

Ce courrier met donc en danger la vie de notre camarade iranienne qui est
intervenue au 53e congrès de la CGT.

Nous apportons notre soutien plein et entier à notre camarade iranienne
Nous condamnons le courrier de ces 5 organisations.
Nous exigeons une explication sur cette démarche de dénonciation d’une
militante et de sa mise en danger.

Saint Pierre de Quiberon, 26 mai 2023



2023-05-23  🚨Rdv mardi 23 mai à 17h00 devant l’ambassade d’Iran à Paris !
============================================================================

🚨Rdv demain, mardi 23 mai à 17h00 devant l’ambassade d’Iran à Paris !

Depuis le début d’année le régime tortionnaire des mollahs a exécuté près
de 218 opposants politiques !

Les prisonniers sont sans défense, sans avocat, et soumis à des tortures. #Iranrevolution #Mahsa_Amini


2023-05-22  **[URGENT] Manifestation contre les exécutions en Iran**
========================================================================

lundi 22 à 18h, République

- https://www.agendamilitant.org/Manifestation-contre-les-executions-en-Iran-3005.html #Iran #Femme #Vie #Liberté

Manifestation organisée par des collectifs de gauche et féministes avec
les prisonnier·es, les condamné·es à mort et les familles des exécuté·es
en Iran.

Toutes les exécutions sont politiques !

Depuis le mois de janvier, la République islamique des bourreaux exécute
en moyenne 10 personnes par semaine.

La machine à tuer de la République islamique tourne à plein régime et il
est urgent de nous retrouver pour montrer notre solidarité avec les
prisonnier·es, les condamné·es à mort et les familles des exécuté·es,
et notre acharnement à défendre avec rage et ferveur la vie et la lutte.


De la place de la République à Fontaine des Innocents (Châtelet)
Manifestation organisée par :
Collectif Roja Paris
Collectif de soutien à la lutte du peuple en Iran Paris
Féminists 4 Jina Paris


2023-05-22 PROFANÉE. La plaque en verre érigée sur la tombe de Jina #MahsaAmini dans sa ville natale de Saghez, dans l’ouest de l’#Iran, a été vandalisée
===========================================================================================================================================================


- http://nitter.smnz.de/arminarefi/status/1660553704129413120#m

PROFANÉE. La plaque en verre érigée sur la tombe de Jina #MahsaAmini
dans sa ville natale de Saghez, dans l’ouest de l’#Iran, a été vandalisée
a alerté dimanche 21 mai son frère sur Instagram.

« Même la plaque de verre de ta tombe les embête ! », s’est offusqué Ashan Amini.