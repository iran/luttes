
.. list in ["local", "list"]
.. 2023/22/images in ["images", "{year}/{week}/images"]



2023-06-01 **Lettre ouverte Reza Shahabi, chauffeur de bus en Iran, et membre du Conseil d'administration du Syndicat des travailleurs/euses de la compagnie de bus de Téhéran et sa banlieue (VAHED)** #ILC2023
=========================================================================================================================================================================================================================

- https://laboursolidarity.org/fr/n/2697/lettre-ouverte-reza-shahabi-chauffeur-de-bus-en-iran-et-membre-du-conseil-d039administration-du-syndicat-des-travailleurseuses-de-la-compagnie-de-bus-de-teheran-et-sa-banlieue-vahed?fbclid=IwAR11hAkelfUDGM4DgVfjD3PXAOc_Sjb50CN-yt07MNhJMztBPmIPxhqjh8U


Je vous salue chaleureusement

Je suis Reza Shahabi, chauffeur de bus en Iran, et membre du Conseil
d'administration du Syndicat des travailleurs/euses de la compagnie de
bus de Téhéran et sa banlieue (VAHED).

Je vous adresse mes salutations de derrière les murs et les barreaux de
la prison d'Evin.

Depuis 2004, c'est-à-dire depuis la reprise d'activité de notre syndicat,
mes collègues et moi-même avons été arrêté.es, licencié.es, emprisonné.es
et torturé.es à de nombreuses reprises.

De 2010 à 2017, je suis allé en prison pour avoir revendiqué des droits,
et lutté pour une vie humaine et digne.

Et cela, suite à des accusations mensongères.

En prison, j'ai subi une opération au cou et au dos, à cause des coups
et de la torture.

En mai 2022, la répression s'est abattue contre des militant-es en raison de :

- leurs activités syndicales,
- leur lutte pour les droits des salarié.es,
- leurs protestations contre le niveau misérable des salaires, ainsi que
  pour l'accès des salarié.es aux moyens de subsistance, au logement,
  aux soins de santé, à l'enseignement ...
- des rencontres et échanges de vues après la fête internationale du
  travail de 2022, entre d'une part deux syndicalistes enseignants
  français.es, et d'autre part des iranien.es (syndicalistes, retraité.es,
  écrivain.es, femmes, étudiant.es ...).

Comme par le passé, nous avons dû faire face à des faux témoignages,
ainsi que d'accusations fictives et mensongères.

Actuellement des militant.es sont  soit en prison, soit en attente de condamnation :

- Reza Shahabi, Dawood Razavi, Hassan Saeedi du Syndicat des
  travailleurs/euses de la compagnie de bus de Téhéran et sa banlieue,
- Keyvan Mohtadi et Anisha Asdolahi, enseignant.es d'anglais et
  traducteur/trices du Syndicat du syndicat VAHED,
- Reyhane Ansarinejad, militante syndicale,
- Jaleh Roohzad, enseignante militante à la retraite,
- Rasool Bodaghi, Jafar Ebrahimi, Mohammad Habibi, Masoud Nikkhah,
  Eskander Lotfi, Sha'ban Mohammadi, membres de syndicats de l'enseignement,
-  ainsi qu'un certain nombre de sympathisant.es et d'enfants de salarié.es.

De plus, alors que nous sommes en prison depuis plus d'un an, de nouvelles
accusations ont été portées contre Hassan Saeidi, Keyvan Mohtadi et
moi-même le 29 mai 2023.

Camarades et cher.es collègues,

Depuis sa mise en place, le régime au pouvoir dans notre pays a eu recours
à une répression ouverte et brutale contre l'ensemble du monde du travail.

Sa première mesure envers celui-ci a été de supprimer de la législation
les notions de syndicat et d'organisation syndicale indépendante.

Il les a remplacés par des organisations inféodées à l'Etat telles que
la Maison des Travailleurs, le Conseil islamique du Travail, et
l'Assemblée des Représentant.es des Travailleurs/euses.

Tous ces organismes opèrent sous la supervision et la direction du gouvernement.
Ils agissent comme une police secrète au sein des usines et les ateliers,
et font en sorte que les protestations des travailleurs/euses ne soient
pas entendues.

Ils identifient les travailleurs/euses qui manifestent et les signalent
aux forces de sécurité ou à la direction. Par exemple en province, dans
le secteur des autobus, les Conseils du travail islamiques ont dénoncé
aux institutions régionales sécuritaires les chauffeurs participant à
des manifestations.

Sur ordre des forces de sécurité, iIs dénoncent également, aux soi-disant
"Comités de Discipline du Travail", les travailleurs/euses protestataire
revendiquant leurs droits et qui manifestent.
Ces salarié.es sont ensuite congédié.es sur ordre des autorités sécuritaires,
comme chez nous à la Compagnie de bus de Téhéran et sa banlieue.

Nous faisons nous-mêmes partie des travailleurs/euses ainsi licencié.es.

Nous avons été licencié.es, arrêté.es et emprisonné.es par les forces de
sécurité.

**D'une part pour avoir organisé un syndicat indépendant, conformément
aux conventions 87 et 98 de l'OIT**.

D'autre part en raison de nos revendications pour une vie juste, la mise
en œuvre des nouvelles classifications et grilles de salaires, une juste
reconnaissnce de la pénibilité des postes de travail, etc.

Nous avons été licencié.es sur la base de rapports mensongers de membres
du Conseil Islamique du Travail et de la Maison des travailleurs.

Et maintenant nous sommes à nouveau emprisonné.es pour les mêmes raisons.

Cher.es ami.es,

De soi-disant syndicalistes iranien.nes assistent également à votre conférence.

Ils/elles ne sont pas les représentant.es légitimes des travailleurs/euses
d'Iran.
Ils/elles n'ont en effet pas été choisies dans le cadre d'un processus
démocratique sans ingérence des forces de sécurité et des employeurs,
mais sous la crainte d'être licencié.es par la direction.
Ces personnes n'ont pas été élues, ou alors ce vote était fictif.

**Ces soit-disant syndicalistes font tout ce que l'employeur et le
gouvernement leur demandent**.

Et cela uniquement dans leur propre intérêt et pour profiter de privilèges
et de facilités mises à leur disposition par le pouvoir.

Ils/elles n'ont jamais été du côté des travailleurs/euses, au contraire,
ils/elles ont agi en permanence contre leurs intérêts.
Ce n'est un secret pour personne qu'ils/elles sont un instrument entre
les mains des employeurs et de l'appareil de sécurité du gouvernement.

Ils/elles constituent une « machine à signature » pour confirmer les
licenciements et justifier des procédures judiciaires répressives contre
les militant.es de la classe ouvrière.

La douleur des travailleurs/euses d'Iran est grande et dépasse l'entendement.

Chers collègues, notre message final, est le suivant :

Le pouvoir iranien signe des conventions et pactes internationaux de l'OIT,
y compris des conventions fondamentales qui lient tous les États membres.

Non seulement il ne les applique pas, mais il met en place des conditions
plus dures et plus inhumaines pour l'ensemble des salarié.es, hommes et
femmes, dans tous les ateliers, écoles, hôpitaux, sociétés d’intérim et
entreprises du secteur privé.

De plus, la politique spéculative et rentière du pouvoir a créé une
inflation galopante entraînant un faible pouvoir d'achat des salarié.es.

Et même lorsqu'il/elle a un emploi, le/la salarié.e ne peut pas pas payer
certaines dépenses mensuelles telles que son loyer.

De plus, en cas de protestation, la peur d'être licencié.e, emprisonné.e
et fouetté.e s'ajoute à ces conditions inhumaines.

Non seulement les travailleurs/euses n'ont pas voix au chapitre dans les
sommets mondiaux concernant le salariat, mais ils/constatent que des
positions importantes dans ces institutions sont occupées par les
responsables mêmes de leur vie misérable.

Le pouvoir n'autorise pas la formation de syndicats et d'organisations
indépendantes.

**Il fait du droit de grève un crime contre la sécurité d'Etat.**

Il n'autorise pas l'élection de représentant.es des salarié.es dans un
cadre démocratique. Chaque fois que la voix de la protestation ouvrière
s'élève, il répond par des licenciements, des emprisonnements, le fouet
et la torture.

Ce même appareil sécuritaire envoie ses propres représentant.es,
étiquetté.es comme syndicalistes, dans les forums internationaux,
y compris les réunions de l'OIT.

Est-il possible pour la même personne de rester secrétaire général de
la "Maison des travailleurs" pendant près de trois décennies, et en même
temps d'être député pendant 6 mandats.

Et cela alors que l'existence de filtres électoraux rigides et sélectifs
est connu de tous/toutes ? Et pendant tout ce temps, le Parlement a pris
d'innombrables décisions contre les  travailleurs/euses.

Qu'en est-il des membres de la Maison des travailleurs et des Conseils
islamiques qui ont joué un rôle direct dans la répression de l'assemblée
générale du personnel de la compagnie d'autobus de Téhéran ?

Ils considèrent que servir le pouvoir constitue pour eux un honneur et
un devoir.
Mais comment est-il possible de prétendre qu'ils/elles peuvent être
simultanément représentant.es des travailleurs/euses d'Iran ?

Comment peut-on faire confiance à ces personnes qui auraient été
pressentie.es pour être élu.es au Conseil d'Administration  de l'OIT,
ses comités ou groupes  de travail ?

Je demande à chaque syndicaliste indépendant.e présent.e au sommet de
l'OIT de prendre en compte les demandes des travailleurs/euses opprimé.es
et démuni.es d'Iran, qui tentent de survivre dans des conditions
inégalitaires avec des possibilités minimales pour agir.

Au cours de la période récente, le monde a clairement été témoin des
événements dramatiques concernant l'oppression, l'exploitation et
l'utilisation d'une violence infinie contre les travailleurs/euses,
les jeunes, les femmes et les minorités de ce pays.

Des accusations répétées ont été lancées contre nous, telles que celle
d'atteinte à la sécurité de l'Etat ou d'autres accusations mensongères.

Il n'est pas possible de laver les mains sanglantes de ces personnes.

Nous vous demandons de vous tenir aux côtés des travailleurs/euses d'Iran :

- De ne pas accepter la présence de ces agents de la sécurité d’Etat qui
  ont été envoyé.es à l'assemblée de l'OIT en tant que représentant.es
  des travailleurs/euses d'Iran et qui auraient été pressenties pour y
  occuper des places au sein du Conseil d'administration,  ses comités
  ou groupes  de travail,
- D'exiger leur départ,
- De demander également ouvertement la libération de toutes et tous les
  travailleurs/euses et syndicalistes emprisonné.es,

- D'exiger la pleine mise en œuvre des conventions de l'Organisation
  internationale du travail, notamment en ce qui concerne le droit de
  grève, d'organisation, de négociation collective et de liberté d'association .


En solidarité,
Reza Shahabi,
Prison d'Evin, Téhéran

2023-06-01 **(Texte intersyndical) Rassemblement à Genève le vendredi 9 juin contre la répression en Iran** #ILC2023
==========================================================================================================================


- https://solidaires.org/sinformer-et-agir/actualites-et-mobilisations/internationales/texte-intersyndical-rassemblement-a-geneve-le-vendredi-9-juin-contre-la-repression-en-iran/

A l’occasion de la Conférence annuelle de l'OIT (Organisation internationale du travail),
des organisations syndicales françaises et suisses s’associent au
rassemblement devant le siège des Nations Unies à Genève, le vendredi 9 juin 2023
pour protester contre la répression en Iran, et notamment l’emprisonnement
des nombreux militants syndicalistes et de droits des travailleurs, ainsi
que deux syndicalistes français, par le régime islamique




2023-06-01 Pour la 37ème semaine consécutive, une foule de manifestants est descendue dans la rue à Zahedan
=============================================================================================================

Pour la 37ème semaine consécutive, une foule de manifestants est descendue
dans la rue à Zahedan (sud-est de l’#Iran) en marge de la grande prière
hebdomadaire.

« Je tuerai ! Je tuerai celui qui a tué mon frère ! », ont scandé ce
vendredi 2 juin les protestataires.


2023-06-01 En Iran, le régime islamique a pendu 142 personnes au mois de mai, un  record absolu
======================================================================================================

En Iran, le régime islamique a pendu 142 personnes au mois de mai, un
record absolu.

Depuis début de l’année 2023, 307 personnes ont été exécutées soit une
hausse de 76%.

Le régime islamique exécute massivement pour propager la peur et terroriser
la population afin d’éviter le renversement du régime.


2023-05-31 Mohseni Ejei affirme lors d’un discours : « Les manifestants qui doivent être exécutés seront exécutés, sans tergiversations »
============================================================================================================================================

- http://nitter.unixfox.eu/LettresTeheran/status/1663930389843279872#m

Mohseni Ejei, le chef de l’autorité judiciaire du régime islamique, affirme
lors d’un discours : « Les manifestants qui doivent être exécutés seront
exécutés, sans tergiversations ». Le régime islamique a exécuté 7 manifestants
et des dizaines d’autres sont dans le couloir de la mort.

Il a augmenté également de façon exponentielle l’exécution des prisonniers
de droit commun dans l’objectif politique de terroriser la population et
se venger du soulèvement populaire.


2023-05-31 Pour une société « d'égalité et de liberté », nous soutenons les Iranien·nes
===========================================================================================

-  https://blogs.mediapart.fr/les-invites-de-mediapart/blog/310523/pour-une-societe-degalite-et-de-liberte-nous-soutenons-les-iranien-nes

« Seul le peuple iranien est capable d’établir les éléments fondateurs
d’une société nouvelle et moderne » : en écho à une déclaration signée
par vingt organisations impliquées dans le soulèvement populaire en Iran,
un large collectif d'universitaires, de syndicalistes et de militants
internationaux manifeste son soutien à cette proposition d'alternative.


:ref:`Nous exprimons notre soutien à la déclaration parue en Iran le 15 février 2023 <revendications_iran_2023_02_15_texte>`
(reproduite ci-dessous).

Cette déclaration émane de vingt organisations de terrain impliquées dans
le soulèvement actuel : syndicats indépendants du pouvoir, associations
de défense des droits des salarié.es, associations de femmes, de retraité.es,
d'étudiant.es et des droits humains.

Cette déclaration ne se limite pas dénoncer la répression sanglante et le
despotisme religieux (ou non-religieux d'hier).

Elle propose simultanément une ébauche d'alternative.



2023-05-31 **La journaliste #NiloofarHamedi a été jugée par le juge de la mort Salavati**
=============================================================================================

- http://nitter.unixfox.eu/LettresTeheran/status/1663557271597752320#m

La journaliste #NiloofarHamedi a été jugée par le juge de la mort Salavati,
aujourd’hui, après 9 mois de détention arbitraire.

Son crime ? Avoir fait son travail après l’assassinat de Jina Mahsa Amini
en se rendant à l’hôpital.

Elle avait notamment posté cette photo des parents de Jina après son décès
sur Twitter. Selon la réquisition, elle risque jusqu’à 15 ans de prison.
