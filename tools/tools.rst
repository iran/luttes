
.. ffmpeg -i baraye.mp4 -vcodec libx265 -crf 28 output.mp4

.. _outils:

=====================
Outils
=====================


Signal
========

People in Iran wanting to use Signal for Android
---------------------------------------------------

- https://twitter.com/signalapp/status/1574119171641647109?s=20&t=q6-xMx-CgGFh6Ct5jkZkPQ

People in Iran wanting to use Signal for Android: try downloading it from
the Play Store first. This is the easiest option.
If this doesn’t work, you can try getting it from our site, here: https://signal.org/android/apk/ #IRanASignalProxy #MahsaAmini #OpIran

- https://signal.org/android/apk/


If you can't reach our site directly, we created the email: getsignal@signal.org
------------------------------------------------------------------------------------

Email us here, and we will automatically respond with a link directing
you to the APK where you can download Signal for Android.
Please note that this inbox is not monitored.  #opiran #MahsaAmini


- https://twitter.com/signalapp/status/1574467355752554496?s=20&t=zgyCYsRrfUI8D7vygjVjZA


Snowflake ?
============

- https://snowflake.torproject.org/

Snowflake est un système pour contrecarrer la censure sur Internet.

Les personnes victimes de censure peuvent utiliser Snowflake pour accéder
à Internet. Leur connexion passe par des mandataires Snowflake qui sont
exploités par des bénévoles.

Pour de plus amples renseignements sur le fonctionnement de Snowflake,
consultez notre wiki de documentation (page en anglais).

There are numerous tools available, such as Snowflake, that "transform"
Internet activity, each using a different technique.
Some redirect Internet traffic to appear to be coming from popular
cloud providers like Microsoft Azure and Amazon Web Services.
Others scramble Internet traffic in order to make it appear completely random.

It therefore becomes costly for censors to consider blocking such
circumvention tools since it would require blocking large parts of
the Internet in order to achieve the initial targeted goal.


https://twitter.com/phagafaga/status/1573668385812013056?s=20&t=tyTIqu7qI9yj_g1Zx8anzg
------------------------------------------------------------------------------------------


If you wanna help, download the snowflake extension here https://snowflake.torproject.org
and if you keep it enabled with your laptop and browser on, it allows
for some Iranian users to circumvent internet censorship. Share please

#Mahsa_Amini #مهسا_امینی #ژینا_امینی

https://snowflake.torproject.org/


ProtoVPN
==========

- https://twitter.com/ProtonVPN/status/1573298232976883714?s=20&t=QmAOkMFb3-OQN_gX3n7hwg


We stand for freedom around the world. That's why we provide our services
for free, and we're here to support our user community in Iran.

Don't hesitate to send us a DM at @ProtonSupport or download our free VPN here: https://protonvpn.com/free-vpn/.

