.. index::
   pair: Video ; Annonce de la réation du parc Jina Mahsa Amini
   pair: Video ; Emmanuel Carroz

.. _videos_2023_09:

=====================
2023-09
=====================

.. _video_annonce_square_jina_amini:

2023-09-16 Annonce de la création du square Jina Mahsa Amini par Emmanuel Carroz
===================================================================================

- :ref:`square_jina_amini_2023_10_03`
- :ref:`iran_grenoble_2023_09_16`
- :ref:`luttes_2023:jardin_mahsa_jina_amini_2023_09_16`

.. youtube:: 61rVaWKIxYc



Emmanuel Carroz
-------------------

- https://www.grenoble.fr/1823-emmanuel-carroz.htm

Adjoint Mémoire, Migrations et Coopérations internationales, Europe
